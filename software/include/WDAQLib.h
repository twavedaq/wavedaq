#include "DAQLib.h"
#include "WDBLib.h"
#include <cmath>
#include <list>
#include <iostream>
#include <fstream>

#ifndef WDAQLIB_H
#define WDAQLIB_H

#include "WDLib.h"

//classes in this file
class WDAQPacketData;
class WDAQWdbPacketData;
class WDAQDRSPacketData;
class WDAQADCPacketData;
class WDAQTDCPacketData;
class WDAQTRGPacketData;
class WDAQScaPacketData;
class WDAQDummyPacketData;
class WDAQTcbPacketData;

class WDAQBoardEvent;
class WDAQWdbEvent;
class WDAQTcbEvent;

class WDAQEvent;

class WDAQPacketCollector;
class WDAQTCBReader;
class WDAQEventBuilder;
class WDAQWorker;
class WDAQEventWriter;

//forward declarations
class WDTCB;

#define EOE 1
#define SOE 2
#define EOT 4
#define SOT 8

#define WD2_BOARD_ID 0
#define TCB_BOARD_ID 1

typedef struct {
   char           bank_name[4];
   unsigned int   time_stamp;
   unsigned int   event_number;
   unsigned char  trigger_information[8];
   unsigned short temperature;
   unsigned short reserved;
} TCB_FRAME_HEADER;

//WDAQ Packet Data - class for UDP DAQ packets 
class WDAQPacketData{
   public:
   //informations from WDAQ packet header
   unsigned char    mProtocolVersion;
   unsigned char    mBoardType;
   unsigned char    mBoardRevision;
   unsigned short   mBoardId;
   unsigned short   mCrateId;
   unsigned short   mSlotId;
   unsigned short   mPacketNumber;
   unsigned char    mDataType;
   unsigned char    mWDAQFlags;
   unsigned short   mPayloadLength;
   unsigned short   mDataOffset; 
   //informations from board specific headers
   unsigned int     mEventNumber;
   unsigned short   mTriggerNumber;
   unsigned char    mTriggerType;
   unsigned short   mSerialTriggerData;  
   //Set properties according to UDP event header
   void SetEventHeaderInfo(WDAQ_FRAME_HEADER *);

   //idea to how to handle board event merging across different board types
   void AddToBoardEvent(WDAQBoardEvent *e) {
      HeaderToBoardEvent(e);
      AddDataToBoardEvent(e);
   };

   virtual ~WDAQPacketData() { };

   protected:
   //Set WDAQBoardEvent Header
   void HeaderToBoardEvent(WDAQBoardEvent *e);
   //To be overwritten by derived classes
   virtual void AddDataToBoardEvent(WDAQBoardEvent *e) { };
};

//WDAQ WDB Packet Data - class for WDB UDP DAQ packets 
class WDAQWdbPacketData: public WDAQPacketData{
   //informations from packet header
   public:
   unsigned char    mADC;
   unsigned char    mChannel;
   unsigned int     mTxEnable;
   unsigned short   mZeroSuppressionMask;
   unsigned char    mWDBFlags;
   unsigned char    mTriggerSource;//serve?
   unsigned char    mBitsPerSample;//serve?
   unsigned short   mSamplesPerEventPerChannel;//serve?
   unsigned long    mTimeStamp;
   unsigned short   mTriggerCell;
   unsigned int     mSamplingFrequency;
   float            mTemperature;
   unsigned short   mDacOFS;
   unsigned short   mDacROFS;
   unsigned short   mFrontendSettings;
   
   //Set properties according to UDP event header
   void SetWdbHeaderInfo(WD_FRAME_HEADER *);

   //merge this packet information in given board event, to be implemented according to data
   virtual void AddDataToBoardEvent(WDAQBoardEvent *e) { };

   virtual ~WDAQWdbPacketData() { };
};

//WDAQ DRS Packet Data -  derived packet class to host DRS data
class WDAQDRSPacketData : public WDAQWdbPacketData{
public:
   float data[1024];

   //Add packet info to given Board Event
   void AddDataToBoardEvent(WDAQBoardEvent *e);

   ~WDAQDRSPacketData() { };
};

//WDAQ ADC Packet Data -  derived packet class to host ADC data
class WDAQADCPacketData : public WDAQWdbPacketData{
public:
   unsigned short data[1024];

   //Add packet info to given Board Event
   void AddDataToBoardEvent(WDAQBoardEvent *e);

   ~WDAQADCPacketData() { };
};

//WDAQ TDC Packet Data -  derived packet class to host TDC data
class WDAQTDCPacketData : public WDAQWdbPacketData{
public:
   unsigned char data[512];

   //Add packet info to given Board Event
   void AddDataToBoardEvent(WDAQBoardEvent *e);

   ~WDAQTDCPacketData() { };
};

//WDAQ TRG Packet Data -  derived packet class to host TRG data
class WDAQTRGPacketData : public WDAQWdbPacketData{
public:
   unsigned long data[512];

   //Add packet info to given Board Event
   void AddDataToBoardEvent(WDAQBoardEvent *e);

   ~WDAQTRGPacketData() { };
};

//WDAQ Scaler Packet Data -  derived packet class to host scaler
class WDAQScaPacketData : public WDAQWdbPacketData{
public:
  unsigned long data[WD_N_SCALER];

   //Add packet info to given Board Event
   void AddDataToBoardEvent(WDAQBoardEvent *e);

   ~WDAQScaPacketData() { };
};

//WDAQ Dummy Packet Data -  derived packet class for fully zero suppressed board
class WDAQDummyPacketData : public WDAQWdbPacketData{
public:

   //Add packet info to given Board Event
   void AddDataToBoardEvent(WDAQBoardEvent *e);

   ~WDAQDummyPacketData() { };
};

//WDAQ TCB Packet Data - class for TCB UDP DAQ packets 
class WDAQTcbPacketData: public WDAQPacketData{
   public:
   //informations from packet header
   std::array<char,4> mBankName;
   unsigned int       mTimeStamp;
   float              mTemperature;
   
   unsigned int     data[1024];//update this to match max event size
   //Set properties according to UDP event header
   void SetTcbHeaderInfo(TCB_FRAME_HEADER *);

   //merge this packet information in given board event, to be implemented according to data
   void AddDataToBoardEvent(WDAQBoardEvent *e);
};

//WDAQ board event - Board Event class
class WDAQBoardEvent {
public:
   //from WDAQ header 
   unsigned char    mBoardType;
   unsigned char    mBoardRevision;
   unsigned short   mBoardId;
   unsigned short   mCrateId;
   unsigned short   mSlotId;
   unsigned short   mWDAQFlags;

   //event status
   bool             mEndFlagReceived;
   bool             mStartFlagReceived;
   int              mPacketsReceived;
   int              mFirstPacket;
   int              mLastPacket;

   bool IsComplete();
   WDAQBoardEvent(WDAQPacketData* pkt);
};

//WDB board event - WDB Board Event class
class WDAQWdbEvent : public WDAQBoardEvent {
public:
   //from WDB header 
   unsigned int     mSamplingFrequency;
   unsigned short   mWDBFlags;
   unsigned char    mTriggerSource;//serve?
   unsigned long    mTimeStamp;
   unsigned short   mDacOFS;
   unsigned short   mDacROFS;
   float            mTemperature;

   //channel or data depenant
   unsigned short   mFrontendSettings[WD_N_CHANNELS];
   unsigned int     mTriggerCell[WD_N_CHANNELS];
   unsigned int     mDrsTxEnable;
   unsigned int     mAdcTxEnable;
   unsigned int     mTdcTxEnable;
   unsigned int     mTrgTxEnable;
   unsigned short   mDrsZeroSuppressionMask;
   unsigned short   mAdcZeroSuppressionMask;
   unsigned short   mTdcZeroSuppressionMask;
   
   float            mDrsU[WD_N_CHANNELS][1024];
   unsigned short   mAdcU[WD_N_CHANNELS-2][2048];
   unsigned char    mTdc[WD_N_CHANNELS-2][512];
   unsigned long    mTrg[512];
   unsigned long    mScaler[WD_N_SCALER];

   //event status
   bool             mVCalibrated;
   bool             mDrsHasData[WD_N_CHANNELS];
   int              mDrsByteNumber[WD_N_CHANNELS];
   bool             mAdcHasData[WD_N_CHANNELS];
   int              mAdcByteNumber[WD_N_CHANNELS];
   bool             mTdcHasData[WD_N_CHANNELS];
   int              mTdcByteNumber[WD_N_CHANNELS];
   bool             mTrgHasData;
   bool             mScalerHasData;
   int              mTrgByteNumber;

   float GetRange() {
      return (int)((0.68 - (mDacOFS/65535.0*2.5))*100)/100.0;
   };
   
   WDAQWdbEvent(WDAQPacketData* pkt);
};

class WDAQTcbBank {
public:
   std::vector<unsigned int> data;
   std::array<char,4> fName;

   WDAQTcbBank(std::array<char,4> name, unsigned long length){
      fName = name;
      data.reserve(1000);//should be the max bank size to be matched with WDAQTcbPacket
      data.resize(length);
   }
};

//TCB board event - TCB Board Event class
class WDAQTcbEvent : public WDAQBoardEvent {
public:
   //from TCB header 
   unsigned long    mTimeStamp;
   float            mTemperature;

   //data
   std::map<std::array<char,4>,WDAQTcbBank*>     mBanks;

   WDAQTcbEvent(WDAQPacketData* pkt);
   ~WDAQTcbEvent(){
      for(auto i: mBanks)
         delete i.second;
      mBanks.clear();
   }
};

//WDAQ Event - global DAQ event
class WDAQEvent {
public:
   unsigned int     mEventNumber;
   unsigned short   mTriggerNumber;
   unsigned short   mTriggerType;
   unsigned short   mSerialTriggerData;

   //map (BoardType -> map(BoardSerial -> Board))
   std::map<unsigned char, std::map<unsigned short, WDAQBoardEvent *>> fBoard;
   
   void AddPacket(WDAQPacketData* pkt);
   int IsComplete();
   
   //constructor
   WDAQEvent(WDAQPacketData* pkt);

   //destructor
   ~WDAQEvent();

}; 

//---------- THREAD implementation -------
//Packet Collector - Thread to collect packets
#define WDAQ_UDP_PROTOCOL_VERSION  7

class WDAQPacketCollector: public DAQServerThread{
   DAQBuffer<WDAQPacketData> *fBuf;

   //statistics
   unsigned long fNPackets;
   unsigned long fDroppedPackets;

   void Begin();

   void GotData(int size, unsigned char* dataptr);
   void PushPacket(WDAQPacketData*);
   
   void End();

   public:
   WDAQPacketCollector(DAQBuffer<WDAQPacketData> *buf, int nBoards=-1): DAQServerThread(nBoards*4*1024*1024){ //  4*1MB/Board
      fBuf = buf;
      fNPackets = 0;
      fDroppedPackets = 0;
   }
};

// temporary thread to read TCB
class WDAQTCBReader: public DAQThread{
   DAQBuffer<WDAQPacketData> *fBuf;
   WDTCB *fBoard;

   void Begin();

   void Loop();
   
   void End();

   public:
   WDAQTCBReader(DAQBuffer<WDAQPacketData> *buf, WDTCB *board){
      fBuf = buf;
      fBoard = board;
   }
};

//Event builder - Thread that build events from packets
class WDAQEventBuilder : public DAQThread{
   DAQBuffer<WDAQPacketData> *fSource;
   DAQBuffer<WDAQEvent> *fDestination;
   //structure to group together packets belonging to same event
   std::map<int, WDAQEvent*> fEvents;

   //statistics
   unsigned long fBuildedEvent;
   unsigned long fDroppedEvent;
   unsigned long fOldEvent;
   int           fNBoards;
   //flags
   bool          fNotBuilding;
   bool          fDropping;

   void Begin();

   void Loop();

   void End();

   public:
   WDAQEventBuilder(DAQBuffer<WDAQPacketData> *source, DAQBuffer<WDAQEvent> *destination, int nNBoards){
      fSource = source;
      fDestination = destination;
      fNBoards = nNBoards;
      
      fBuildedEvent = 0;
      fDroppedEvent = 0;
      fOldEvent = 0;
      fNotBuilding = false;
      fDropping = false;
   }

   //building state getters
   bool GetIsNotBuilding() const { return fNotBuilding; }
   bool GetIsDropping() const { return fDropping; }
};

//Event worker - Thread that calibrate events
class WDAQWorker : public DAQThread{
   DAQBuffer<WDAQEvent> *fSource;
   DAQBuffer<WDAQEvent> *fDestination;

   //map for time bins
   std::map<int, VCALIB*> fVCalib;

   //statistics
   unsigned long fNEvent;
   unsigned long fDroppedEvent;

   //single board calibration
   void calibrateBoard(WDAQWdbEvent *ev);

   void Begin();

   void Loop();

   void End();

   public:
   void AddVoltageCalibration(int id, VCALIB *calib){
         fVCalib[id] = calib;
   }

   WDAQWorker(DAQBuffer<WDAQEvent> *source, DAQBuffer<WDAQEvent> *destination){
      fSource = source;
      fDestination = destination;
   }

   ~WDAQWorker(){
   }

};


//Event writer - Thread that writes event to file
class WDAQEventWriter : public DAQThread{
   DAQBuffer<WDAQEvent> *fSource;

   //map for time bins
   std::map<int, TCALIB*> fTCalib;

   //vetor to hold tcb numbers
   std::vector<int> fTCBList;

   //output file data
   std::string fFileName;
   std::ofstream fFile;
   unsigned int fRunNumber;

   //statistics
   unsigned long fNEvent;
   unsigned long fLastEvent;

   //event per file
   unsigned int fEventsPerFile;

   void Begin();

   void Loop();

   void End();

   //write run header to file
   void WriteRunHeader();

   //return current file name inserting runnumber
   std::string GetFileName();

   public:
   void AddTimeCalibration(int id, TCALIB *calib){
         fTCalib[id] = calib;
   }
   void AddTcbName(int id){
         fTCBList.push_back(id);
   }

   WDAQEventWriter(DAQBuffer<WDAQEvent> *source, std::string file, unsigned int eventsPerFile = 0, unsigned int startRunNumber = 0){
      fSource = source;
      fFileName = file;
      fEventsPerFile = eventsPerFile;
      fRunNumber = startRunNumber;

      fNEvent = 0;
      fLastEvent = 0;
   }

   ~WDAQEventWriter(){
   }

};

#endif
