# README #

This repository is for the WaveDREAM system and the associated firmware and software.

### What is this repository for? ###

* Drs4 based REAdout Module (WaveDREAM) II board firmware
* WaveDREAM II C library to access this module
* Crate Management Board (CMB) firmware for C8051F121 micro controller
* Trigger Concentrator Board (TCB) firmware
* Data Concentrator Board (DCB) firmware
* Auxiliary System for clock and trigger distribution
* Schematics for WD2, CMB, TCB, CMB and Ancillary System
* Documentations, data sheets etc.
* Documentation [Wiki](https://bitbucket.org/twavedaq/wavedaq/wiki/Home)

### How do I get set up? ###

* To compile the WD2 library under Mac OSX and Linux, execute in the wavedaq/software directory:
 
    * mkdir build
    * cd build
    * cmake ..
    * make install

This will put the "wds" executable under wavedaq/software/bin and some libraries under wavedaq/software/lib

### Who do I talk to? ###

* Stefan Ritt <stefan.ritt@psi.ch>
* Luca Galli <luca.galli@pi.infn.it>
