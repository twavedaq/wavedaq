---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic SPI Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.07.2014 14:58:31
--
--  Description :  Generic implementation of an SPI Master Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity spi_master is
  generic (
    CGN_MAX_NUM_BYTES  : integer := 1; -- allowable values: 1,2,3,4
    CGN_NUM_SS_BITS    : integer := 1;
    CGN_SCK_RATIO      : integer := 4; -- allowable values: 2,4,8,10,12...
    CGN_MULTI_MASTER   : integer := 0;
    CGN_HAS_RX_FIFO    : integer := 1;
    CGN_HAS_TX_FIFO    : integer := 1
  );
  port (
    -- SPI interface
    MOSI_O             : out std_logic;
    MISO_I             : in  std_logic;
    SCK_O              : out std_logic;
    SS_N_O             : out std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);

    -- Configuration
    NUM_BITS_I         : in  std_logic_vector(5 downto 0);
    CPOL_I             : in  std_logic; -- 0=positive _/"\_ , 1=negative "\_/"
    CPHA_I             : in  std_logic; -- 0=first edge centered on data, 1=second edge centered on data
    LSB_FIRST_I        : in  std_logic;
    LOOP_I             : in  std_logic;
    MANUAL_SLAVE_SEL_I : in  std_logic;
    SLAVE_SEL_N_I      : in  std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);

    -- Status
    -- FIFO control and status
    RX_FIFO_RESET_I    : in  std_logic;
    RX_FIFO_AEMPTY_O   : out std_logic;
    RX_FIFO_EMPTY_O    : out std_logic;
    RX_FIFO_RE_I       : in  std_logic;
    RX_FIFO_DATA_O     : out std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0);
    TX_FIFO_RESET_I    : in  std_logic;
    TX_FIFO_AFULL_O    : out std_logic;
    TX_FIFO_FULL_O     : out std_logic;
    TX_FIFO_WE_I       : in  std_logic;
    TX_FIFO_DATA_I     : in  std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0);

    RESET_I            : in  std_logic;
    CLK_I              : in  std_logic
  );
end spi_master;

architecture behavioral of spi_master is

  constant C_DIV_COUNTER_SIZE : integer := 5;
  constant C_COUNTER_LOAD     : std_logic_vector(C_DIV_COUNTER_SIZE-1 downto 0) := CONV_STD_LOGIC_VECTOR( (CGN_SCK_RATIO/2)-1 , C_DIV_COUNTER_SIZE);

  -- states of state machine
  type type_state is (idle, load, sync, select_slave, start, wait_tx_edge_noshift, wait_tx_edge, wait_rx_edge, stop);
  signal state            : type_state := idle;

  signal sck_positive     : std_logic := '0';
  signal sck_rising_edge  : std_logic := '0';
  signal sck_falling_edge : std_logic := '0';
  signal clk_div_count    : std_logic_vector(C_DIV_COUNTER_SIZE-1 downto 0) := (others => '0');
  signal edge_count       : std_logic_vector(6 downto 0);
  
  signal mosi             : std_logic := '0';
  signal miso             : std_logic := '0';
  signal slave_select     : std_logic := '1';

  signal input_shift_reg  : std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0) := (others => '0');
  signal output_shift_reg : std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0) := (others => '0');

  -- internal rx fifo signals
  signal rx_fifo_full   : std_logic := '0';
  signal rx_fifo_afull  : std_logic := '0';
  signal rx_fifo_we     : std_logic := '0';
  signal rx_fifo_wdata  : std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0) := (others => '0');

  -- internal tx fifo signals
  signal tx_fifo_aempty : std_logic := '0';
  signal tx_fifo_empty  : std_logic := '0';
  signal tx_fifo_re     : std_logic := '0';
  signal tx_fifo_rdata  : std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0) := (others => '0');

  component spi_fifo
    generic (
      CGN_NUM_BYTES : integer := 1; -- allowable values: 1,2,3,4
      CGN_HAS_FIFO  : integer := 1
    );
    port (
      WRITE_DATA_I   : in  std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0);
      WRITE_EN_I     : in  std_logic;
      READ_DATA_O    : out std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0);
      READ_EN_I      : in  std_logic;
      FULL_O         : out std_logic;
      ALMOST_FULL_O  : out std_logic;
      ALMOST_EMPTY_O : out std_logic;
      EMPTY_O        : out std_logic;
      RESET_I        : in  std_logic;
      CLK_I          : in  std_logic
    );
  end component;

begin

  rx_fifo_or_reg : spi_fifo
    generic map(
      CGN_NUM_BYTES => CGN_MAX_NUM_BYTES,
      CGN_HAS_FIFO  => CGN_HAS_RX_FIFO
    )
    port map(
      WRITE_DATA_I   => rx_fifo_wdata,
      WRITE_EN_I     => rx_fifo_we,
      READ_DATA_O    => RX_FIFO_DATA_O,
      READ_EN_I      => RX_FIFO_RE_I,
      FULL_O         => rx_fifo_full,
      ALMOST_FULL_O  => rx_fifo_afull,
      ALMOST_EMPTY_O => RX_FIFO_AEMPTY_O,
      EMPTY_O        => RX_FIFO_EMPTY_O,
      RESET_I        => RX_FIFO_RESET_I,
      CLK_I          => CLK_I
    );

  rx_data_mux : process(input_shift_reg, LSB_FIRST_I, NUM_BITS_I)
  begin
    rx_fifo_wdata <= (others=>'0');
    if LSB_FIRST_I = '1' then
      rx_fifo_wdata(CONV_INTEGER(NUM_BITS_I)-1 downto 0) <= input_shift_reg((CGN_MAX_NUM_BYTES*8)-1 downto (CGN_MAX_NUM_BYTES*8)-CONV_INTEGER(NUM_BITS_I));
    else
      rx_fifo_wdata(CONV_INTEGER(NUM_BITS_I)-1 downto 0) <= input_shift_reg(CONV_INTEGER(NUM_BITS_I)-1 downto 0);
    end if;
  end process rx_data_mux;

  tx_fifo_or_reg : spi_fifo
  generic map(
    CGN_NUM_BYTES => CGN_MAX_NUM_BYTES,
    CGN_HAS_FIFO  => CGN_HAS_TX_FIFO
  )
  port map(
    WRITE_DATA_I   => TX_FIFO_DATA_I,
    WRITE_EN_I     => TX_FIFO_WE_I,
    READ_DATA_O    => tx_fifo_rdata,
    READ_EN_I      => tx_fifo_re,
    FULL_O         => TX_FIFO_FULL_O,
    ALMOST_FULL_O  => TX_FIFO_AFULL_O,
    ALMOST_EMPTY_O => tx_fifo_aempty,
    EMPTY_O        => tx_fifo_empty,
    RESET_I        => TX_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  mosi <= output_shift_reg(0) when LSB_FIRST_I = '1' else output_shift_reg(CONV_INTEGER(NUM_BITS_I)-1);
  -- Loopback mode
  miso <= mosi when LOOP_I = '1' else MISO_I;

  slave_select_mask : process(MANUAL_SLAVE_SEL_I, SLAVE_SEL_N_I, slave_select)
    variable i : integer;
  begin
    if MANUAL_SLAVE_SEL_I = '1' then
      SS_N_O <= SLAVE_SEL_N_I;
    else
      for i in CGN_NUM_SS_BITS-1 downto 0 loop
        SS_N_O(i) <= SLAVE_SEL_N_I(i) or slave_select;
      end loop;
    end if;
  end process slave_select_mask;

  control_fsm : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RESET_I = '1' then
        clk_div_count <= (others => '0');
        edge_count <= (others => '0');
        tx_fifo_re    <= '0';
        sck_positive  <= '0';
        if CGN_MULTI_MASTER = 1 then
          MOSI_O <= 'Z';
        else
          MOSI_O <= mosi;
        end if;
        state      <= idle;
      else

        -- clock edge counter
        if edge_count > 0 and clk_div_count = 0 then
          edge_count <= edge_count-1;
        end if;

        -- clock divide counter
        if clk_div_count = 0 then
          clk_div_count <= C_COUNTER_LOAD;
        else
          clk_div_count <= clk_div_count - 1;
        end if ;

        if CGN_MULTI_MASTER = 1 then
          MOSI_O <= 'Z';
        else
          MOSI_O <= mosi;
        end if;

        tx_fifo_re   <= '0';
        rx_fifo_we   <= '0';
        if CPOL_I = '0' then
          SCK_O <= sck_positive;
        else
          SCK_O <= not sck_positive;
        end if;

        case state is
        when idle =>
        -- deselect slave wait for action
          slave_select <= '1';
          edge_count <= (others => '0');
          if tx_fifo_empty = '0' then
            tx_fifo_re <= '1';
            state <= load;
          end if;
        when load =>
        -- select slave and load data into tx register
          output_shift_reg <= tx_fifo_rdata;
          state <= sync;
        when sync =>
          edge_count <= NUM_BITS_I & '0';
          if clk_div_count = 0 then
            slave_select <= '0';
            MOSI_O <= mosi;
            if CPHA_I = '1' then
              state <= wait_tx_edge_noshift;
            else
              state <= wait_rx_edge;
            end if;
          end if;
        when wait_tx_edge_noshift =>
        -- wait for transmit clock edge and shift output sreg with edge
          MOSI_O <= mosi;
          if clk_div_count = 0 then
            sck_positive <= not sck_positive;
            state <= wait_rx_edge;
          end if;
        when wait_tx_edge =>
        -- wait for transmit clock edge and shift output sreg with edge
          MOSI_O <= mosi;
          if clk_div_count = 0 then
            if edge_count > 0 then
              sck_positive <= not sck_positive;
              if LSB_FIRST_I = '1' then
                output_shift_reg <= '0' & output_shift_reg((CGN_MAX_NUM_BYTES*8)-1 downto 1);
              else
                output_shift_reg <= output_shift_reg((CGN_MAX_NUM_BYTES*8)-2 downto 0) & '0';
              end if;
              state <= wait_rx_edge;
            else
              state <= stop;
            end if;
          end if;
        when wait_rx_edge =>
        -- wait for receive clock edge and shift input sreg with edge
          MOSI_O <= mosi;
          if clk_div_count = 0 then
            if edge_count > 0 then
              sck_positive <= not sck_positive;
              if LSB_FIRST_I = '1' then
                input_shift_reg <= miso & input_shift_reg((CGN_MAX_NUM_BYTES*8)-1 downto 1);
              else
                input_shift_reg <= input_shift_reg((CGN_MAX_NUM_BYTES*8)-2 downto 0) & miso;
              end if;
              state <= wait_tx_edge;
            else
              state <= stop;
            end if;
          end if;
        when stop =>
        -- store received data and return to idle state
          slave_select <= '1';
          if clk_div_count = 0 then
            rx_fifo_we <= '1'; -- no full-check => eat or die
            state <= idle;
          end if;
        when others =>
          state <= idle;
        end case;
      end if;
    end if;
  end process control_fsm;

end behavioral;
