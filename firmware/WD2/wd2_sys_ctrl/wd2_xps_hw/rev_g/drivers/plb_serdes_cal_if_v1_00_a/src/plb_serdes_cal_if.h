/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_serdes_cal_if_v1_00_a/src/plb_serdes_cal_if.h
* Version:           1.00.a
* Description:       plb_serdes_cal_if Driver Header File
* Date:              Thu Sep 11 15:51:50 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef PLB_SERDES_CAL_IF_H
#define PLB_SERDES_CAL_IF_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 * -- SLV_REG2 : user logic slave module register 2
 * -- SLV_REG3 : user logic slave module register 3
 * -- SLV_REG4 : user logic slave module register 4
 * -- SLV_REG5 : user logic slave module register 5
 * -- SLV_REG6 : user logic slave module register 6
 * -- SLV_REG7 : user logic slave module register 7
 * -- SLV_REG8 : user logic slave module register 8
 * -- SLV_REG9 : user logic slave module register 9
 * -- SLV_REG10 : user logic slave module register 10
 * -- SLV_REG11 : user logic slave module register 11
 * -- SLV_REG12 : user logic slave module register 12
 * -- SLV_REG13 : user logic slave module register 13
 * -- SLV_REG14 : user logic slave module register 14
 * -- SLV_REG15 : user logic slave module register 15
 * -- SLV_REG16 : user logic slave module register 16
 * -- SLV_REG17 : user logic slave module register 17
 */
#define PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET (0x00000000)
#define PLB_SERDES_CAL_IF_SLV_REG0_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000000)
#define PLB_SERDES_CAL_IF_SLV_REG1_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000004)
#define PLB_SERDES_CAL_IF_SLV_REG2_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000008)
#define PLB_SERDES_CAL_IF_SLV_REG3_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x0000000C)
#define PLB_SERDES_CAL_IF_SLV_REG4_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000010)
#define PLB_SERDES_CAL_IF_SLV_REG5_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000014)
#define PLB_SERDES_CAL_IF_SLV_REG6_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000018)
#define PLB_SERDES_CAL_IF_SLV_REG7_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x0000001C)
#define PLB_SERDES_CAL_IF_SLV_REG8_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000020)
#define PLB_SERDES_CAL_IF_SLV_REG9_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000024)
#define PLB_SERDES_CAL_IF_SLV_REG10_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000028)
#define PLB_SERDES_CAL_IF_SLV_REG11_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x0000002C)
#define PLB_SERDES_CAL_IF_SLV_REG12_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000030)
#define PLB_SERDES_CAL_IF_SLV_REG13_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000034)
#define PLB_SERDES_CAL_IF_SLV_REG14_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000038)
#define PLB_SERDES_CAL_IF_SLV_REG15_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x0000003C)
#define PLB_SERDES_CAL_IF_SLV_REG16_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000040)
#define PLB_SERDES_CAL_IF_SLV_REG17_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000044)

/**************************** Type Definitions *****************************/


/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_SERDES_CAL_IF register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_SERDES_CAL_IF device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PLB_SERDES_CAL_IF_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PLB_SERDES_CAL_IF_mWriteReg(BaseAddress, RegOffset, Data) \
 	Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a PLB_SERDES_CAL_IF register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_SERDES_CAL_IF device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 PLB_SERDES_CAL_IF_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_SERDES_CAL_IF_mReadReg(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_SERDES_CAL_IF user logic slave registers.
 *
 * @param   BaseAddress is the base address of the PLB_SERDES_CAL_IF device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 * 	void PLB_SERDES_CAL_IF_mWriteSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Value)
 * 	Xuint32 PLB_SERDES_CAL_IF_mReadSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_SERDES_CAL_IF_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG0_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg1(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG1_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg2(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG2_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg3(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG3_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg4(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG4_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg5(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG5_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg6(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG6_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg7(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG7_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg8(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG8_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg9(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG9_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg10(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG10_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg11(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG11_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg12(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG12_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg13(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG13_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg14(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG14_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg15(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG15_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg16(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG16_OFFSET) + (RegOffset), (Xuint32)(Value))
#define PLB_SERDES_CAL_IF_mWriteSlaveReg17(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG17_OFFSET) + (RegOffset), (Xuint32)(Value))

#define PLB_SERDES_CAL_IF_mReadSlaveReg0(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG0_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg1(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG1_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg2(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG2_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg3(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG3_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg4(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG4_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg5(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG5_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg6(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG6_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg7(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG7_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg8(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG8_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg9(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG9_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg10(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG10_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg11(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG11_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg12(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG12_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg13(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG13_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg14(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG14_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg15(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG15_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg16(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG16_OFFSET) + (RegOffset))
#define PLB_SERDES_CAL_IF_mReadSlaveReg17(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_SERDES_CAL_IF_SLV_REG17_OFFSET) + (RegOffset))

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the PLB_SERDES_CAL_IF instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus PLB_SERDES_CAL_IF_SelfTest(void * baseaddr_p);

#endif /** PLB_SERDES_CAL_IF_H */
