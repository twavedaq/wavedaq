/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_wd2_reg_bank_v1_00_a/src/plb_wd2_reg_bank.h
* Version:           1.00.a
* Description:       plb_wd2_reg_bank Driver Header File
* Date:              Thu Aug 14 12:00:46 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef PLB_WD2_REG_BANK_H
#define PLB_WD2_REG_BANK_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**************************** Type Definitions *****************************/


/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_WD2_REG_BANK register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_WD2_REG_BANK device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PLB_WD2_REG_BANK_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PLB_WD2_REG_BANK_mWriteReg(BaseAddress, RegOffset, Data) \
 	Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a PLB_WD2_REG_BANK register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_WD2_REG_BANK device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 PLB_WD2_REG_BANK_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_WD2_REG_BANK_mReadReg(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_WD2_REG_BANK user logic memory (BRAM).
 *
 * @param   Address is the memory address of the PLB_WD2_REG_BANK device.
 * @param   Data is the value written to user logic memory.
 *
 * @return  The data from the user logic memory.
 *
 * @note
 * C-style signature:
 * 	void PLB_WD2_REG_BANK_mWriteMemory(Xuint32 Address, Xuint32 Data)
 * 	Xuint32 PLB_WD2_REG_BANK_mReadMemory(Xuint32 Address)
 *
 */
#define PLB_WD2_REG_BANK_mWriteMemory(Address, Data) \
 	Xil_Out32(Address, (Xuint32)(Data))
#define PLB_WD2_REG_BANK_mReadMemory(Address) \
 	Xil_In32(Address)

/************************** Function Prototypes ****************************/


/**
 *
 * Run a self-test on the driver/device. Note this may be a destructive test if
 * resets of the device are performed.
 *
 * If the hardware system is not built correctly, this function may never
 * return to the caller.
 *
 * @param   baseaddr_p is the base address of the PLB_WD2_REG_BANK instance to be worked on.
 *
 * @return
 *
 *    - XST_SUCCESS   if all self-test code passed
 *    - XST_FAILURE   if any self-test code failed
 *
 * @note    Caching must be turned off for this function to work.
 * @note    Self test may fail if data memory and device are not on the same bus.
 *
 */
XStatus PLB_WD2_REG_BANK_SelfTest(void * baseaddr_p);

#endif /** PLB_WD2_REG_BANK_H */
