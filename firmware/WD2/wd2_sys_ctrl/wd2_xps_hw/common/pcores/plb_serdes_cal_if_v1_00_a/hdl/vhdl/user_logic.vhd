---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  PLB ISERDES CALIBRATION INTERFACE
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.09.2014 16:07:54
--
--  Description :  Interface for reading ISERDES deserialized data in order to
--                 calibrate the IDELAY.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Thu Sep 11 15:51:48 2014 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    CGN_SERDES_DWIDTH              : integer              := 12;
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 11
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    ISERDES_DATA0_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA1_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA2_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA3_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA4_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA5_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA6_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA7_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_FRAME_I                : in  std_logic_vector(CGN_SERDES_DWIDTH-1 downto 0);
    ISERDES_DATA_VALID_I           : in  std_logic;
    ISERDES_ALIGNED_I              : in  std_logic;
    ISERDES_HALFWORD_I             : in  std_logic;
    ISERDES_BITSLIP_I              : in  std_logic;
    ISERDES_DIV_CLK_I              : in  std_logic;
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  COMPONENT async_fifo_4x16
    PORT (
      rst : IN STD_LOGIC;
      wr_clk : IN STD_LOGIC;
      rd_clk : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(3 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(3 DOWNTO 0);
      full : OUT STD_LOGIC;
      overflow : OUT STD_LOGIC;
      empty : OUT STD_LOGIC
    );
  END COMPONENT;

  COMPONENT async_fifo_112x16
    PORT (
      rst : IN STD_LOGIC;
      wr_clk : IN STD_LOGIC;
      rd_clk : IN STD_LOGIC;
      din : IN STD_LOGIC_VECTOR(111 DOWNTO 0);
      wr_en : IN STD_LOGIC;
      rd_en : IN STD_LOGIC;
      dout : OUT STD_LOGIC_VECTOR(111 DOWNTO 0);
      full : OUT STD_LOGIC;
      empty : OUT STD_LOGIC;
      underflow : OUT STD_LOGIC
    );
  END COMPONENT;

  constant C_PLB2ADC_FIFO_WIDTH : integer := 4;
  constant C_ADC2PLB_FIFO_WIDTH : integer := 9*CGN_SERDES_DWIDTH+4;
  constant C_ZEROS              : std_logic_vector(C_SLV_DWIDTH-CGN_SERDES_DWIDTH-1 downto 0) := (others=>'0');

  signal plb2adc_fifo_wr_en     : std_logic := '0';
  signal plb2adc_fifo_dout      : std_logic_vector(C_PLB2ADC_FIFO_WIDTH-1 downto 0) := (others=>'0');
  signal plb2adc_fifo_rd_en     : std_logic := '0';
  signal plb2adc_fifo_full      : std_logic := '0';
  signal plb2adc_fifo_overflow  : std_logic := '0';
  signal plb2adc_fifo_empty     : std_logic := '0';

  signal adc2plb_fifo_wr_en     : std_logic := '0';
  signal adc2plb_fifo_dout      : std_logic_vector(C_ADC2PLB_FIFO_WIDTH-1 downto 0) := (others=>'0');
  signal adc2plb_fifo_full      : std_logic := '0';
  signal adc2plb_fifo_empty     : std_logic := '0';
  signal adc2plb_fifo_underflow : std_logic := '0';

  signal adc2plb_data           : std_logic_vector(8*CGN_SERDES_DWIDTH-1 downto 0) := (others=>'0');
  signal adc2plb_frame          : std_logic_vector(1*CGN_SERDES_DWIDTH-1 downto 0) := (others=>'0');
  signal adc2plb_ctrl           : std_logic_vector(3 downto 0) := (others=>'0');

  signal soft_rst               : std_logic := '0';
  signal fifo_rst               : std_logic := '0';
  signal word_count             : std_logic_vector(3 downto 0) := (others=>'0');

  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg3                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg4                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg5                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg6                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg7                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg8                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg9                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg10                      : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel              : std_logic_vector(0 to 10);
  signal slv_reg_read_sel               : std_logic_vector(0 to 10);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;

begin

  --USER logic implementation added here

  -- input from adc interface
  input_ffs : process(ISERDES_DIV_CLK_I)
  begin
    if rising_edge(ISERDES_DIV_CLK_I) then
      adc2plb_data  <= ISERDES_DATA7_I &
                       ISERDES_DATA6_I &
                       ISERDES_DATA5_I &
                       ISERDES_DATA4_I &
                       ISERDES_DATA3_I &
                       ISERDES_DATA2_I &
                       ISERDES_DATA1_I &
                       ISERDES_DATA0_I;
      adc2plb_frame <= ISERDES_FRAME_I;
      adc2plb_ctrl  <= ISERDES_DATA_VALID_I &
                       ISERDES_ALIGNED_I &
                       ISERDES_HALFWORD_I &
                       ISERDES_BITSLIP_I;
    end if;
  end process input_ffs;

  adc_if_to_plb_core_fifo_inst : async_fifo_112x16
  PORT MAP (
    rst                 => fifo_rst,
    wr_clk              => ISERDES_DIV_CLK_I,
    rd_clk              => Bus2IP_Clk,
    din( 95 downto   0) => adc2plb_data,
    din(107 downto  96) => adc2plb_frame,
    din(111 downto 108) => adc2plb_ctrl,
    wr_en               => adc2plb_fifo_wr_en,
    rd_en               => slv_reg0(14),
    dout                => adc2plb_fifo_dout,
    full                => adc2plb_fifo_full,
    empty               => adc2plb_fifo_empty,
    underflow           => adc2plb_fifo_underflow
  );

  -- Status information
  slv_reg1(0 to 11)  <= X"000";
  slv_reg1(14)       <= plb2adc_fifo_full;
  slv_reg1(15)       <= adc2plb_fifo_empty;
  slv_reg1(16 to 27) <= X"000";
  slv_reg1(28 to 31) <= word_count;

  -- save underflow or overflow until reset
  underflow_overflow_sr_ff : process(Bus2IP_Clk)
  begin
    if rising_edge(Bus2IP_Clk) then
      if soft_rst = '1' then
        slv_reg1(12) <= '0';
        slv_reg1(13) <= '0';
      else
        if plb2adc_fifo_overflow = '1' then
          slv_reg1(12) <= '1';
        end if;
        if adc2plb_fifo_underflow = '1' then
          slv_reg1(13) <= '1';
        end if;
      end if;
    end if;
  end process underflow_overflow_sr_ff;

  -- DATA
  slv_reg2  <= C_ZEROS & adc2plb_fifo_dout(1*CGN_SERDES_DWIDTH-1 downto 0);                   -- DATA0
  slv_reg3  <= C_ZEROS & adc2plb_fifo_dout(2*CGN_SERDES_DWIDTH-1 downto 1*CGN_SERDES_DWIDTH); -- DATA1
  slv_reg4  <= C_ZEROS & adc2plb_fifo_dout(3*CGN_SERDES_DWIDTH-1 downto 2*CGN_SERDES_DWIDTH); -- DATA2
  slv_reg5  <= C_ZEROS & adc2plb_fifo_dout(4*CGN_SERDES_DWIDTH-1 downto 3*CGN_SERDES_DWIDTH); -- DATA3
  slv_reg6  <= C_ZEROS & adc2plb_fifo_dout(5*CGN_SERDES_DWIDTH-1 downto 4*CGN_SERDES_DWIDTH); -- DATA4
  slv_reg7  <= C_ZEROS & adc2plb_fifo_dout(6*CGN_SERDES_DWIDTH-1 downto 5*CGN_SERDES_DWIDTH); -- DATA5
  slv_reg8  <= C_ZEROS & adc2plb_fifo_dout(7*CGN_SERDES_DWIDTH-1 downto 6*CGN_SERDES_DWIDTH); -- DATA6
  slv_reg9  <= C_ZEROS & adc2plb_fifo_dout(8*CGN_SERDES_DWIDTH-1 downto 7*CGN_SERDES_DWIDTH); -- DATA7
  -- CTRL & FRAME
  slv_reg10 <= X"0000" & adc2plb_fifo_dout(C_ADC2PLB_FIFO_WIDTH-1 downto 8*CGN_SERDES_DWIDTH);

  -- output to adc interface
  plb_core_to_adc_if_fifo_inst : async_fifo_4x16
  PORT MAP (
    rst      => fifo_rst,
    wr_clk   => Bus2IP_Clk,
    rd_clk   => ISERDES_DIV_CLK_I,
    din      => slv_reg0(28 to 31),
    wr_en    => plb2adc_fifo_wr_en,
    rd_en    => plb2adc_fifo_rd_en,
    dout     => plb2adc_fifo_dout,
    full     => plb2adc_fifo_full,
    overflow => plb2adc_fifo_overflow,
    empty    => plb2adc_fifo_empty
  );

  plb2adc_wren_gen : process(Bus2IP_Clk)
  begin
    plb2adc_fifo_wr_en <= '0';
    -- If it can be written to the FIFO
    -- generate a write enable pulse
    if plb2adc_fifo_full = '0' and slv_reg0(28 to 31) /= "0000" then
      plb2adc_fifo_wr_en <= '1';
    end if;
  end process plb2adc_wren_gen;

  -- control signal generation
  soft_rst <= slv_reg0(15);
  fifo_rst <= soft_rst or Bus2IP_Reset;

  -- counter controlling the write to the adc2plb fifo
  word_counter : process(ISERDES_DIV_CLK_I)
  begin
    if rising_edge(ISERDES_DIV_CLK_I) then
      adc2plb_fifo_wr_en <= '0';
      plb2adc_fifo_rd_en <= '0';
      if word_count = 0 then
        -- if the counter is not running and
        -- there is a new load value in the fifo
        if plb2adc_fifo_empty = '0' then
          plb2adc_fifo_rd_en <= '1';
          word_count         <= plb2adc_fifo_dout;
        end if;
      elsif adc2plb_fifo_full = '0' then
        adc2plb_fifo_wr_en <= '1';
        word_count         <= word_count-1;
      end if;
    end if;
  end process word_counter;

  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  -- 
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  -- 
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  -- 
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 10);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 10);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4) or Bus2IP_WrCE(5) or Bus2IP_WrCE(6) or Bus2IP_WrCE(7) or Bus2IP_WrCE(8) or Bus2IP_WrCE(9) or Bus2IP_WrCE(10);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3) or Bus2IP_RdCE(4) or Bus2IP_RdCE(5) or Bus2IP_RdCE(6) or Bus2IP_RdCE(7) or Bus2IP_RdCE(8) or Bus2IP_RdCE(9) or Bus2IP_RdCE(10);

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin
    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
      else
        -- default
        slv_reg0(0 to 15)  <= (others=>'0');
        -- If it can be written to the FIFO
        -- reset number of words requested and
        -- generate a write enable pulse
        if plb2adc_fifo_wr_en = '1' then
          slv_reg0(16 to 31) <= (others=>'0');
        end if;
        case slv_reg_write_sel is
          when "10000000000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when others => null;
        end case;
      end if;
    end if;
  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1, slv_reg2, slv_reg3, slv_reg4, slv_reg5, slv_reg6, slv_reg7, slv_reg8, slv_reg9, slv_reg10 ) is
  begin
    case slv_reg_read_sel is
      when "10000000000" => slv_ip2bus_data <= slv_reg0;
      when "01000000000" => slv_ip2bus_data <= slv_reg1;
      when "00100000000" => slv_ip2bus_data <= slv_reg2;
      when "00010000000" => slv_ip2bus_data <= slv_reg3;
      when "00001000000" => slv_ip2bus_data <= slv_reg4;
      when "00000100000" => slv_ip2bus_data <= slv_reg5;
      when "00000010000" => slv_ip2bus_data <= slv_reg6;
      when "00000001000" => slv_ip2bus_data <= slv_reg7;
      when "00000000100" => slv_ip2bus_data <= slv_reg8;
      when "00000000010" => slv_ip2bus_data <= slv_reg9;
      when "00000000001" => slv_ip2bus_data <= slv_reg10;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

end IMP;
