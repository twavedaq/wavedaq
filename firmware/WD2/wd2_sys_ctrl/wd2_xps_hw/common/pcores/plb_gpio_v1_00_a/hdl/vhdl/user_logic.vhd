------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Wed Oct 20 15:11:22 2010 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here
library plb_gpio_v1_00_a;
use plb_gpio_v1_00_a.gpio_single_register_set;

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------
    CGN_P0_HAS_INPUT :  integer := 0; CGN_P0_HAS_OUTPUT :  integer := 0; CGN_P0_PORT_WIDTH :  integer := 32; CGN_P0_HAS_BITOP :  integer := 0;
    CGN_P1_HAS_INPUT :  integer := 0; CGN_P1_HAS_OUTPUT :  integer := 0; CGN_P1_PORT_WIDTH :  integer := 32; CGN_P1_HAS_BITOP :  integer := 0;
    CGN_P2_HAS_INPUT :  integer := 0; CGN_P2_HAS_OUTPUT :  integer := 0; CGN_P2_PORT_WIDTH :  integer := 32; CGN_P2_HAS_BITOP :  integer := 0;
    CGN_P3_HAS_INPUT :  integer := 0; CGN_P3_HAS_OUTPUT :  integer := 0; CGN_P3_PORT_WIDTH :  integer := 32; CGN_P3_HAS_BITOP :  integer := 0;
    CGN_P4_HAS_INPUT :  integer := 0; CGN_P4_HAS_OUTPUT :  integer := 0; CGN_P4_PORT_WIDTH :  integer := 32; CGN_P4_HAS_BITOP :  integer := 0;
    CGN_P5_HAS_INPUT :  integer := 0; CGN_P5_HAS_OUTPUT :  integer := 0; CGN_P5_PORT_WIDTH :  integer := 32; CGN_P5_HAS_BITOP :  integer := 0;
    CGN_P6_HAS_INPUT :  integer := 0; CGN_P6_HAS_OUTPUT :  integer := 0; CGN_P6_PORT_WIDTH :  integer := 32; CGN_P6_HAS_BITOP :  integer := 0;
    CGN_P7_HAS_INPUT :  integer := 0; CGN_P7_HAS_OUTPUT :  integer := 0; CGN_P7_PORT_WIDTH :  integer := 32; CGN_P7_HAS_BITOP :  integer := 0;
    CGN_P8_HAS_INPUT :  integer := 0; CGN_P8_HAS_OUTPUT :  integer := 0; CGN_P8_PORT_WIDTH :  integer := 32; CGN_P8_HAS_BITOP :  integer := 0;
    CGN_P9_HAS_INPUT :  integer := 0; CGN_P9_HAS_OUTPUT :  integer := 0; CGN_P9_PORT_WIDTH :  integer := 32; CGN_P9_HAS_BITOP :  integer := 0;
    CGN_P10_HAS_INPUT : integer := 0; CGN_P10_HAS_OUTPUT : integer := 0; CGN_P10_PORT_WIDTH : integer := 32; CGN_P10_HAS_BITOP : integer := 0;
    CGN_P11_HAS_INPUT : integer := 0; CGN_P11_HAS_OUTPUT : integer := 0; CGN_P11_PORT_WIDTH : integer := 32; CGN_P11_HAS_BITOP : integer := 0;
    CGN_P12_HAS_INPUT : integer := 0; CGN_P12_HAS_OUTPUT : integer := 0; CGN_P12_PORT_WIDTH : integer := 32; CGN_P12_HAS_BITOP : integer := 0;
    CGN_P13_HAS_INPUT : integer := 0; CGN_P13_HAS_OUTPUT : integer := 0; CGN_P13_PORT_WIDTH : integer := 32; CGN_P13_HAS_BITOP : integer := 0;
    CGN_P14_HAS_INPUT : integer := 0; CGN_P14_HAS_OUTPUT : integer := 0; CGN_P14_PORT_WIDTH : integer := 32; CGN_P14_HAS_BITOP : integer := 0;
    CGN_P15_HAS_INPUT : integer := 0; CGN_P15_HAS_OUTPUT : integer := 0; CGN_P15_PORT_WIDTH : integer := 32; CGN_P15_HAS_BITOP : integer := 0;
    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 128
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    -- ADD USER PORTS ABOVE THIS LINE ------------------
    IO_PORT0_I                      : in  std_logic_vector(0 to CGN_P0_PORT_WIDTH-1);
    IO_PORT0_O                      : out std_logic_vector(0 to CGN_P0_PORT_WIDTH-1);
    IO_PORT0_T                      : out std_logic_vector(0 to CGN_P0_PORT_WIDTH-1);
    IO_PORT1_I                      : in  std_logic_vector(0 to CGN_P1_PORT_WIDTH-1);
    IO_PORT1_O                      : out std_logic_vector(0 to CGN_P1_PORT_WIDTH-1);
    IO_PORT1_T                      : out std_logic_vector(0 to CGN_P1_PORT_WIDTH-1);
    IO_PORT2_I                      : in  std_logic_vector(0 to CGN_P2_PORT_WIDTH-1);
    IO_PORT2_O                      : out std_logic_vector(0 to CGN_P2_PORT_WIDTH-1);
    IO_PORT2_T                      : out std_logic_vector(0 to CGN_P2_PORT_WIDTH-1);
    IO_PORT3_I                      : in  std_logic_vector(0 to CGN_P3_PORT_WIDTH-1);
    IO_PORT3_O                      : out std_logic_vector(0 to CGN_P3_PORT_WIDTH-1);
    IO_PORT3_T                      : out std_logic_vector(0 to CGN_P3_PORT_WIDTH-1);
    IO_PORT4_I                      : in  std_logic_vector(0 to CGN_P4_PORT_WIDTH-1);
    IO_PORT4_O                      : out std_logic_vector(0 to CGN_P4_PORT_WIDTH-1);
    IO_PORT4_T                      : out std_logic_vector(0 to CGN_P4_PORT_WIDTH-1);
    IO_PORT5_I                      : in  std_logic_vector(0 to CGN_P5_PORT_WIDTH-1);
    IO_PORT5_O                      : out std_logic_vector(0 to CGN_P5_PORT_WIDTH-1);
    IO_PORT5_T                      : out std_logic_vector(0 to CGN_P5_PORT_WIDTH-1);
    IO_PORT6_I                      : in  std_logic_vector(0 to CGN_P6_PORT_WIDTH-1);
    IO_PORT6_O                      : out std_logic_vector(0 to CGN_P6_PORT_WIDTH-1);
    IO_PORT6_T                      : out std_logic_vector(0 to CGN_P6_PORT_WIDTH-1);
    IO_PORT7_I                      : in  std_logic_vector(0 to CGN_P7_PORT_WIDTH-1);
    IO_PORT7_O                      : out std_logic_vector(0 to CGN_P7_PORT_WIDTH-1);
    IO_PORT7_T                      : out std_logic_vector(0 to CGN_P7_PORT_WIDTH-1);
    IO_PORT8_I                      : in  std_logic_vector(0 to CGN_P8_PORT_WIDTH-1);
    IO_PORT8_O                      : out std_logic_vector(0 to CGN_P8_PORT_WIDTH-1);
    IO_PORT8_T                      : out std_logic_vector(0 to CGN_P8_PORT_WIDTH-1);
    IO_PORT9_I                      : in  std_logic_vector(0 to CGN_P9_PORT_WIDTH-1);
    IO_PORT9_O                      : out std_logic_vector(0 to CGN_P9_PORT_WIDTH-1);
    IO_PORT9_T                      : out std_logic_vector(0 to CGN_P9_PORT_WIDTH-1);
    IO_PORT10_I                     : in  std_logic_vector(0 to CGN_P10_PORT_WIDTH-1);
    IO_PORT10_O                     : out std_logic_vector(0 to CGN_P10_PORT_WIDTH-1);
    IO_PORT10_T                     : out std_logic_vector(0 to CGN_P10_PORT_WIDTH-1);
    IO_PORT11_I                     : in  std_logic_vector(0 to CGN_P11_PORT_WIDTH-1);
    IO_PORT11_O                     : out std_logic_vector(0 to CGN_P11_PORT_WIDTH-1);
    IO_PORT11_T                     : out std_logic_vector(0 to CGN_P11_PORT_WIDTH-1);
    IO_PORT12_I                     : in  std_logic_vector(0 to CGN_P12_PORT_WIDTH-1);
    IO_PORT12_O                     : out std_logic_vector(0 to CGN_P12_PORT_WIDTH-1);
    IO_PORT12_T                     : out std_logic_vector(0 to CGN_P12_PORT_WIDTH-1);
    IO_PORT13_I                     : in  std_logic_vector(0 to CGN_P13_PORT_WIDTH-1);
    IO_PORT13_O                     : out std_logic_vector(0 to CGN_P13_PORT_WIDTH-1);
    IO_PORT13_T                     : out std_logic_vector(0 to CGN_P13_PORT_WIDTH-1);
    IO_PORT14_I                     : in  std_logic_vector(0 to CGN_P14_PORT_WIDTH-1);
    IO_PORT14_O                     : out std_logic_vector(0 to CGN_P14_PORT_WIDTH-1);
    IO_PORT14_T                     : out std_logic_vector(0 to CGN_P14_PORT_WIDTH-1);
    IO_PORT15_I                     : in  std_logic_vector(0 to CGN_P15_PORT_WIDTH-1);
    IO_PORT15_O                     : out std_logic_vector(0 to CGN_P15_PORT_WIDTH-1);
    IO_PORT15_T                     : out std_logic_vector(0 to CGN_P15_PORT_WIDTH-1);
    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  constant NR_OF_PORTS : integer := 16;
  
  type reg_parameter is array(0 to NR_OF_PORTS-1) of integer;
  
  constant C_HAS_INPUT  : reg_parameter := (CGN_P0_HAS_INPUT,  CGN_P1_HAS_INPUT,  CGN_P2_HAS_INPUT,  CGN_P3_HAS_INPUT,  CGN_P4_HAS_INPUT,  CGN_P5_HAS_INPUT,  CGN_P6_HAS_INPUT,  CGN_P7_HAS_INPUT,  CGN_P8_HAS_INPUT,  CGN_P9_HAS_INPUT,  CGN_P10_HAS_INPUT,  CGN_P11_HAS_INPUT,  CGN_P12_HAS_INPUT,  CGN_P13_HAS_INPUT,  CGN_P14_HAS_INPUT,  CGN_P15_HAS_INPUT);
  constant C_HAS_OUTPUT : reg_parameter := (CGN_P0_HAS_OUTPUT, CGN_P1_HAS_OUTPUT, CGN_P2_HAS_OUTPUT, CGN_P3_HAS_OUTPUT, CGN_P4_HAS_OUTPUT, CGN_P5_HAS_OUTPUT, CGN_P6_HAS_OUTPUT, CGN_P7_HAS_OUTPUT, CGN_P8_HAS_OUTPUT, CGN_P9_HAS_OUTPUT, CGN_P10_HAS_OUTPUT, CGN_P11_HAS_OUTPUT, CGN_P12_HAS_OUTPUT, CGN_P13_HAS_OUTPUT, CGN_P14_HAS_OUTPUT, CGN_P15_HAS_OUTPUT);
  constant C_PORT_WIDTH : reg_parameter := (CGN_P0_PORT_WIDTH, CGN_P1_PORT_WIDTH, CGN_P2_PORT_WIDTH, CGN_P3_PORT_WIDTH, CGN_P4_PORT_WIDTH, CGN_P5_PORT_WIDTH, CGN_P6_PORT_WIDTH, CGN_P7_PORT_WIDTH, CGN_P8_PORT_WIDTH, CGN_P9_PORT_WIDTH, CGN_P10_PORT_WIDTH, CGN_P11_PORT_WIDTH, CGN_P12_PORT_WIDTH, CGN_P13_PORT_WIDTH, CGN_P14_PORT_WIDTH, CGN_P15_PORT_WIDTH);
  constant C_HAS_BITOP  : reg_parameter := (CGN_P0_HAS_BITOP,  CGN_P1_HAS_BITOP,  CGN_P2_HAS_BITOP,  CGN_P3_HAS_BITOP,  CGN_P4_HAS_BITOP,  CGN_P5_HAS_BITOP,  CGN_P6_HAS_BITOP,  CGN_P7_HAS_BITOP,  CGN_P8_HAS_BITOP,  CGN_P9_HAS_BITOP,  CGN_P10_HAS_BITOP,  CGN_P11_HAS_BITOP,  CGN_P12_HAS_BITOP,  CGN_P13_HAS_BITOP,  CGN_P14_HAS_BITOP,  CGN_P15_HAS_BITOP);

  signal ports_in  : std_logic_vector(0 to NR_OF_PORTS*C_SLV_DWIDTH-1) := (others => '0');
  signal ports_out : std_logic_vector(0 to NR_OF_PORTS*C_SLV_DWIDTH-1) := (others => '0');
  signal ports_tri : std_logic_vector(0 to NR_OF_PORTS*C_SLV_DWIDTH-1) := (others => '0');

  signal reg_read_ack  : std_logic_vector(0 to NR_OF_PORTS-1);
  signal reg_write_ack : std_logic_vector(0 to NR_OF_PORTS-1);

  -- Function for Reduction OR
  function or_reduce(l : std_logic_vector) return std_logic is
    variable v : std_logic := '0';
  begin
    for i in l'range loop v := v or l(i); end loop;
    return v;
  end;
  -- End of Function

  signal slv_ip2bus_data                : std_logic_vector(0 to NR_OF_PORTS*C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;


begin

  REGISTER_SETS : for i in 0 to NR_OF_PORTS-1 generate
  begin
    INPUTS_OR_OUTPUTS : if C_HAS_INPUT(i) = 1 or C_HAS_OUTPUT(i) = 1 generate
    begin
      SINGLE_REGSET_INSTANCE : entity plb_gpio_v1_00_a.gpio_single_register_set
      generic map
      (
        CGN_HAS_INPUT  => C_HAS_INPUT(i),
        CGN_HAS_OUTPUT => C_HAS_OUTPUT(i),
        CGN_HAS_BITOP  => C_HAS_BITOP(i),
        CGN_PORT_WIDTH => C_PORT_WIDTH(i),
        CGN_DATA_WIDTH => C_SLV_DWIDTH
      )
      port map
      (
        I_DATA    => Bus2IP_Data,
        O_DATA    => slv_ip2bus_data(i*C_SLV_DWIDTH to (i+1)*C_SLV_DWIDTH-1),
        I_BE      => Bus2IP_BE,
        I_REG_WEN => Bus2IP_WrCE(i*8 to i*8+7),
        I_REG_REN => Bus2IP_RdCE(i*8 to i*8+7),
        O_RD_ACK  => reg_read_ack(i),
        O_WR_ACK  => reg_write_ack(i),
        IO_PORT_I => ports_in(i*C_SLV_DWIDTH to i*C_SLV_DWIDTH+C_PORT_WIDTH(i)-1),
        IO_PORT_O => ports_out(i*C_SLV_DWIDTH to i*C_SLV_DWIDTH+C_PORT_WIDTH(i)-1),
        IO_PORT_T => ports_tri(i*C_SLV_DWIDTH to i*C_SLV_DWIDTH+C_PORT_WIDTH(i)-1),
        I_RESET   => Bus2IP_Reset,
        I_CLK     => Bus2IP_Clk
      );
    end generate INPUTS_OR_OUTPUTS;

    NO_INPUTS_OR_OUTPUTS : if C_HAS_INPUT(i) = 0 AND C_HAS_OUTPUT(i) = 0 generate
    begin
      reg_read_ack(i)  <= '0';
      reg_write_ack(i) <= '0';
    end generate NO_INPUTS_OR_OUTPUTS;


  end generate REGISTER_SETS;

  -- Port assignments
  ports_in( 0*C_SLV_DWIDTH to  0*C_SLV_DWIDTH+C_PORT_WIDTH(0) -1)  <= IO_PORT0_I;
  ports_in( 1*C_SLV_DWIDTH to  1*C_SLV_DWIDTH+C_PORT_WIDTH(1) -1)  <= IO_PORT1_I;
  ports_in( 2*C_SLV_DWIDTH to  2*C_SLV_DWIDTH+C_PORT_WIDTH(2) -1)  <= IO_PORT2_I;
  ports_in( 3*C_SLV_DWIDTH to  3*C_SLV_DWIDTH+C_PORT_WIDTH(3) -1)  <= IO_PORT3_I;
  ports_in( 4*C_SLV_DWIDTH to  4*C_SLV_DWIDTH+C_PORT_WIDTH(4) -1)  <= IO_PORT4_I;
  ports_in( 5*C_SLV_DWIDTH to  5*C_SLV_DWIDTH+C_PORT_WIDTH(5) -1)  <= IO_PORT5_I;
  ports_in( 6*C_SLV_DWIDTH to  6*C_SLV_DWIDTH+C_PORT_WIDTH(6) -1)  <= IO_PORT6_I;
  ports_in( 7*C_SLV_DWIDTH to  7*C_SLV_DWIDTH+C_PORT_WIDTH(7) -1)  <= IO_PORT7_I;
  ports_in( 8*C_SLV_DWIDTH to  8*C_SLV_DWIDTH+C_PORT_WIDTH(8) -1)  <= IO_PORT8_I;
  ports_in( 9*C_SLV_DWIDTH to  9*C_SLV_DWIDTH+C_PORT_WIDTH(9) -1)  <= IO_PORT9_I;
  ports_in(10*C_SLV_DWIDTH to 10*C_SLV_DWIDTH+C_PORT_WIDTH(10)-1)  <= IO_PORT10_I;
  ports_in(11*C_SLV_DWIDTH to 11*C_SLV_DWIDTH+C_PORT_WIDTH(11)-1)  <= IO_PORT11_I;
  ports_in(12*C_SLV_DWIDTH to 12*C_SLV_DWIDTH+C_PORT_WIDTH(12)-1)  <= IO_PORT12_I;
  ports_in(13*C_SLV_DWIDTH to 13*C_SLV_DWIDTH+C_PORT_WIDTH(13)-1)  <= IO_PORT13_I;
  ports_in(14*C_SLV_DWIDTH to 14*C_SLV_DWIDTH+C_PORT_WIDTH(14)-1)  <= IO_PORT14_I;
  ports_in(15*C_SLV_DWIDTH to 15*C_SLV_DWIDTH+C_PORT_WIDTH(15)-1)  <= IO_PORT15_I;

  IO_PORT0_O  <= ports_out( 0*C_SLV_DWIDTH to  0*C_SLV_DWIDTH+C_PORT_WIDTH(0) -1);
  IO_PORT1_O  <= ports_out( 1*C_SLV_DWIDTH to  1*C_SLV_DWIDTH+C_PORT_WIDTH(1) -1);
  IO_PORT2_O  <= ports_out( 2*C_SLV_DWIDTH to  2*C_SLV_DWIDTH+C_PORT_WIDTH(2) -1);
  IO_PORT3_O  <= ports_out( 3*C_SLV_DWIDTH to  3*C_SLV_DWIDTH+C_PORT_WIDTH(3) -1);
  IO_PORT4_O  <= ports_out( 4*C_SLV_DWIDTH to  4*C_SLV_DWIDTH+C_PORT_WIDTH(4) -1);
  IO_PORT5_O  <= ports_out( 5*C_SLV_DWIDTH to  5*C_SLV_DWIDTH+C_PORT_WIDTH(5) -1);
  IO_PORT6_O  <= ports_out( 6*C_SLV_DWIDTH to  6*C_SLV_DWIDTH+C_PORT_WIDTH(6) -1);
  IO_PORT7_O  <= ports_out( 7*C_SLV_DWIDTH to  7*C_SLV_DWIDTH+C_PORT_WIDTH(7) -1);
  IO_PORT8_O  <= ports_out( 8*C_SLV_DWIDTH to  8*C_SLV_DWIDTH+C_PORT_WIDTH(8) -1);
  IO_PORT9_O  <= ports_out( 9*C_SLV_DWIDTH to  9*C_SLV_DWIDTH+C_PORT_WIDTH(9) -1);
  IO_PORT10_O <= ports_out(10*C_SLV_DWIDTH to 10*C_SLV_DWIDTH+C_PORT_WIDTH(10)-1);
  IO_PORT11_O <= ports_out(11*C_SLV_DWIDTH to 11*C_SLV_DWIDTH+C_PORT_WIDTH(11)-1);
  IO_PORT12_O <= ports_out(12*C_SLV_DWIDTH to 12*C_SLV_DWIDTH+C_PORT_WIDTH(12)-1);
  IO_PORT13_O <= ports_out(13*C_SLV_DWIDTH to 13*C_SLV_DWIDTH+C_PORT_WIDTH(13)-1);
  IO_PORT14_O <= ports_out(14*C_SLV_DWIDTH to 14*C_SLV_DWIDTH+C_PORT_WIDTH(14)-1);
  IO_PORT15_O <= ports_out(15*C_SLV_DWIDTH to 15*C_SLV_DWIDTH+C_PORT_WIDTH(15)-1);

  IO_PORT0_T  <= ports_tri( 0*C_SLV_DWIDTH to  0*C_SLV_DWIDTH+C_PORT_WIDTH(0) -1);
  IO_PORT1_T  <= ports_tri( 1*C_SLV_DWIDTH to  1*C_SLV_DWIDTH+C_PORT_WIDTH(1) -1);
  IO_PORT2_T  <= ports_tri( 2*C_SLV_DWIDTH to  2*C_SLV_DWIDTH+C_PORT_WIDTH(2) -1);
  IO_PORT3_T  <= ports_tri( 3*C_SLV_DWIDTH to  3*C_SLV_DWIDTH+C_PORT_WIDTH(3) -1);
  IO_PORT4_T  <= ports_tri( 4*C_SLV_DWIDTH to  4*C_SLV_DWIDTH+C_PORT_WIDTH(4) -1);
  IO_PORT5_T  <= ports_tri( 5*C_SLV_DWIDTH to  5*C_SLV_DWIDTH+C_PORT_WIDTH(5) -1);
  IO_PORT6_T  <= ports_tri( 6*C_SLV_DWIDTH to  6*C_SLV_DWIDTH+C_PORT_WIDTH(6) -1);
  IO_PORT7_T  <= ports_tri( 7*C_SLV_DWIDTH to  7*C_SLV_DWIDTH+C_PORT_WIDTH(7) -1);
  IO_PORT8_T  <= ports_tri( 8*C_SLV_DWIDTH to  8*C_SLV_DWIDTH+C_PORT_WIDTH(8) -1);
  IO_PORT9_T  <= ports_tri( 9*C_SLV_DWIDTH to  9*C_SLV_DWIDTH+C_PORT_WIDTH(9) -1);
  IO_PORT10_T <= ports_tri(10*C_SLV_DWIDTH to 10*C_SLV_DWIDTH+C_PORT_WIDTH(10)-1);
  IO_PORT11_T <= ports_tri(11*C_SLV_DWIDTH to 11*C_SLV_DWIDTH+C_PORT_WIDTH(11)-1);
  IO_PORT12_T <= ports_tri(12*C_SLV_DWIDTH to 12*C_SLV_DWIDTH+C_PORT_WIDTH(12)-1);
  IO_PORT13_T <= ports_tri(13*C_SLV_DWIDTH to 13*C_SLV_DWIDTH+C_PORT_WIDTH(13)-1);
  IO_PORT14_T <= ports_tri(14*C_SLV_DWIDTH to 14*C_SLV_DWIDTH+C_PORT_WIDTH(14)-1);
  IO_PORT15_T <= ports_tri(15*C_SLV_DWIDTH to 15*C_SLV_DWIDTH+C_PORT_WIDTH(15)-1);

  slv_write_ack <= or_reduce(reg_write_ack);
  slv_read_ack  <= or_reduce(reg_read_ack);
  IP2Bus_WrAck  <= slv_write_ack;
  IP2Bus_RdAck  <= slv_read_ack;

  IP2BUS_DATA_MUX : process(Bus2IP_WrCE, slv_ip2bus_data)
  begin
    IP2Bus_Data <= (others => '0'); -- default
    for i in 0 to NR_OF_PORTS-1 loop
      --if Bus2IP_WrCE(i*8 to i*8+7) /= 0 then
      if reg_read_ack(i) = '1' then
        IP2Bus_Data <= slv_ip2bus_data(i*C_SLV_DWIDTH to (i+1)*C_SLV_DWIDTH-1);
      end if;
    end loop;
  end process IP2BUS_DATA_MUX;

  IP2Bus_Error <= '0';

end IMP;
