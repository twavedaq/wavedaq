library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;

entity gpio_single_register_set is
  generic
  (
    CGN_HAS_INPUT    : integer := 0;
    CGN_HAS_OUTPUT   : integer := 0;
    CGN_HAS_BITOP    : integer := 0;
    CGN_PORT_WIDTH   : integer := 32;
    CGN_DATA_WIDTH   : integer := 32
  );
  port
  (
    I_DATA           : in  std_logic_vector(0 to CGN_DATA_WIDTH-1);
    O_DATA           : out std_logic_vector(0 to CGN_DATA_WIDTH-1);
    I_BE             : in  std_logic_vector(0 to CGN_DATA_WIDTH/8-1);
    I_REG_WEN        : in  std_logic_vector(0 to 7);
    I_REG_REN        : in  std_logic_vector(0 to 7);
    O_RD_ACK         : out std_logic;
    O_WR_ACK         : out std_logic;
    IO_PORT_I        : in  std_logic_vector(0 to CGN_PORT_WIDTH-1);
    IO_PORT_O        : out std_logic_vector(0 to CGN_PORT_WIDTH-1);
    IO_PORT_T        : out std_logic_vector(0 to CGN_PORT_WIDTH-1);
    I_RESET          : in  std_logic;
    I_CLK            : in  std_logic
  );

end entity gpio_single_register_set;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture behavioral of gpio_single_register_set is

  constant C_EN_OUT               : std_logic_vector(0 to 7) := "10000000";
  constant C_EN_IN                : std_logic_vector(0 to 7) := "01000000";
  constant C_EN_SETOUT            : std_logic_vector(0 to 7) := "00100000";
  constant C_EN_CLROUT            : std_logic_vector(0 to 7) := "00010000";
  constant C_EN_TRI               : std_logic_vector(0 to 7) := "00001000";
  constant C_EN_SETTRI            : std_logic_vector(0 to 7) := "00000010";
  constant C_EN_CLRTRI            : std_logic_vector(0 to 7) := "00000001";

  signal out_reg                       : std_logic_vector(0 to CGN_PORT_WIDTH-1) := (others => '0');
  signal tri_reg                       : std_logic_vector(0 to CGN_PORT_WIDTH-1) := (others => '1');
  signal out_reg_ce                    : std_logic_vector(0 to CGN_PORT_WIDTH-1);
  signal tri_reg_ce                    : std_logic_vector(0 to CGN_PORT_WIDTH-1);

  -- Function for Reduction OR
  function or_reduce(l : std_logic_vector) return std_logic is
    variable v : std_logic := '0';
  begin
    for i in l'range loop v := v or l(i); end loop; --'
    return v;
  end;
  -- End of Function

begin

  O_RD_ACK <= or_reduce(I_REG_REN);
  O_WR_ACK <= or_reduce(I_REG_WEN);

  -- Output Register
  OUTPUT_REGISTER : if CGN_HAS_OUTPUT = 1 generate
  begin

    OUTPUT_REGISTER_PROCESS : process(I_CLK)
    begin
      if rising_edge(I_CLK) then
        if I_RESET = '1' then
          out_reg <= (others => '0');
        else
          if I_REG_WEN = C_EN_OUT then -- write output register
            for i in 0 to CGN_PORT_WIDTH-1 loop
              if out_reg_ce(i) = '1' then
                out_reg(i) <= I_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH);
              end if;
            end loop;
          end if;
          -- Implementing the bitoperations with a "generate" statement results
          -- in confusing code. Probably the corresponding hardware is removed
          -- anyway by the synthesis tool if things are implemented as is.
          if CGN_HAS_BITOP = 1 then -- Only do bit operation hardware if required
            if I_REG_WEN = C_EN_SETOUT then -- set output register bits
              for i in 0 to CGN_PORT_WIDTH-1 loop
                if out_reg_ce(i) = '1' then
                  out_reg(i) <= out_reg(i) or I_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH);
                end if;
              end loop;
            elsif I_REG_WEN = C_EN_CLROUT then -- clear output register bits
              for i in 0 to CGN_PORT_WIDTH-1 loop
                if out_reg_ce(i) = '1' then
                  out_reg(i) <= out_reg(i) and not(I_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH));
                end if;
              end loop;
            end if; -- I_REG_WEN
          end if; -- CGN_HAS_BITOP
        end if; -- I_RESET
      end if; -- rising_edge(I_CLK)
    end process OUTPUT_REGISTER_PROCESS;

    -- Generate clock enable for every FF in the otuput register
    OUT_REG_CE_PROCESS : process(I_REG_WEN, I_BE) is
    begin
      if I_REG_WEN = C_EN_OUT or I_REG_WEN = C_EN_SETOUT or I_REG_WEN = C_EN_CLROUT then
        for i in 0 to CGN_PORT_WIDTH-1 loop
          out_reg_ce(i) <= I_BE((i-CGN_PORT_WIDTH+CGN_DATA_WIDTH)/8);
        end loop;
      else
        out_reg_ce <= (others => '0');
      end if;
    end process OUT_REG_CE_PROCESS;

    -- Assign outputs
    IO_PORT_O <= out_reg;

  end generate OUTPUT_REGISTER;



  -- Tristate Register (only generate if required)
  TRISTATE_REGISTER : if CGN_HAS_OUTPUT = 1  and CGN_HAS_INPUT = 1 generate
  begin

    TRISTATE_REGISTER_PROCESS : process(I_CLK)
    begin
      if rising_edge(I_CLK) then
        if I_RESET = '1' then
          tri_reg <= (others => '1');
        else
          if I_REG_WEN = C_EN_TRI then -- write tristate register
            for i in 0 to CGN_PORT_WIDTH-1 loop
              if tri_reg_ce(i) = '1' then
                tri_reg(i) <= I_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH);
              end if;
            end loop;
          end if;
          -- Implementing the bitoperations with a "generate" statement results
          -- in confusing code. Probably the corresponding hardware is removed
          -- anyway by the synthesis tool if things are implemented as is.
          if CGN_HAS_BITOP = 1 then -- Only do bit operation hardware if required
            if I_REG_WEN = C_EN_SETTRI then -- set tristate register bits
              for i in 0 to CGN_PORT_WIDTH-1 loop
                if tri_reg_ce(i) = '1' then
                  tri_reg(i) <= tri_reg(i) or I_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH);
                end if;
              end loop;
            elsif I_REG_WEN = C_EN_CLRTRI then -- clear tristate register bits
              for i in 0 to CGN_PORT_WIDTH-1 loop
                if tri_reg_ce(i) = '1' then
                  tri_reg(i) <= tri_reg(i) and not(I_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH));
                end if;
              end loop;
            end if; -- I_REG_WEN
          end if; -- CGN_HAS_BITOP
        end if; -- I_RESET
      end if; -- rising_edge(I_CLK)
    end process TRISTATE_REGISTER_PROCESS;

    TRI_REG_CE_PROCESS : process(I_REG_WEN, I_BE) is
    begin
      if I_REG_WEN = C_EN_TRI or I_REG_WEN = C_EN_SETTRI or I_REG_WEN = C_EN_CLRTRI then
        for i in 0 to CGN_PORT_WIDTH-1 loop
          tri_reg_ce(i) <= I_BE((i-CGN_PORT_WIDTH+CGN_DATA_WIDTH)/8);
        end loop;
      else
        tri_reg_ce <= (others => '0');
      end if;
    end process TRI_REG_CE_PROCESS;

    -- Assign tristates
    IO_PORT_T <= tri_reg;

  end generate TRISTATE_REGISTER;



--  READ_PROCESS : process(I_REG_REN) -- , IO_PORT_I, out_reg, tri_reg)
--  begin
--    O_DATA <= (others => '1');
--    case I_REG_REN is
--      when X"40" => --C_EN_IN =>
--        O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= X"a1a1";
--      when X"80" => --C_EN_OUT =>
--        O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= X"b2b2";
--      when X"08" => --C_EN_TRI =>
--        O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= X"c3c3";
--      when others =>
--        O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= (others => '0');
--    end case;
--  end process READ_PROCESS;

  READ_PROCESS : process(I_REG_REN, IO_PORT_I, out_reg, tri_reg)
  begin
    O_DATA <= (others => '0');
    case I_REG_REN is
      when C_EN_IN =>
        -- Implementing the input read operation with a "generate" statement results
        -- in confusing code. Probably the corresponding hardware is removed
        -- anyway by the synthesis tool if things are implemented as is.
        if CGN_HAS_INPUT = 1 then
          for i in 0 to CGN_PORT_WIDTH-1 loop
            O_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH) <= IO_PORT_I(i);
          end loop;
        else
          O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= (others => '0');
        end if;
      when C_EN_OUT =>
        -- Implementing the output read operation with a "generate" statement results
        -- in confusing code. Probably the corresponding hardware is removed
        -- anyway by the synthesis tool if things are implemented as is.
        if CGN_HAS_OUTPUT = 1 then
          for i in 0 to CGN_PORT_WIDTH-1 loop
            O_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH) <= out_reg(i);
          end loop;
        else
          O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= (others => '0');
        end if;
      when C_EN_TRI =>
        -- Implementing the tristate read operation with a "generate" statement results
        -- in confusing code. Probably the corresponding hardware is removed
        -- anyway by the synthesis tool if things are implemented as is.
        if CGN_HAS_OUTPUT = 1 then
          for i in 0 to CGN_PORT_WIDTH-1 loop
            O_DATA(i-CGN_PORT_WIDTH+CGN_DATA_WIDTH) <= tri_reg(i);
          end loop;
        else
          O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= (others => '0');
        end if;
      when others =>
        O_DATA(CGN_DATA_WIDTH-CGN_PORT_WIDTH to CGN_DATA_WIDTH-1) <= (others => '0');
    end case;
  end process READ_PROCESS;

end architecture behavioral;
