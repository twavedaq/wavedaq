--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : counter.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  2017.01.05
--
--  Description : simple generic counter prepared for timing optimization
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity counter is
  generic
  (
    CGN_TOTAL_WIDTH : integer := 32;
    CGN_LSB_WIDTH   : integer := 2;
    CGN_DIR         : string  := "UP" -- options: "DOWN"
  );
  port
  (
    COUNT_I  : in  std_logic_vector(CGN_TOTAL_WIDTH-1 downto 0);
    COUNT_O  : out std_logic_vector(CGN_TOTAL_WIDTH-1 downto 0);
    LOAD_I   : in   std_logic;
    ENABLE_I : in   std_logic;
    CLK_I    : in   std_logic
  );
end counter;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of counter is

  constant C_LSB_ALL_ONES  : std_logic_vector(CGN_LSB_WIDTH-1 downto 0) := (others=>'1');
  constant C_LSB_ALL_ZEROS : std_logic_vector(CGN_LSB_WIDTH-1 downto 0) := (others=>'0');

  signal count_lsbs : std_logic_vector(CGN_LSB_WIDTH-1 downto 0) := (others=>'0');
  signal count_msbs : std_logic_vector(CGN_TOTAL_WIDTH-1 downto CGN_LSB_WIDTH) := (others=>'0');

begin

  COUNT_O <= count_msbs & count_lsbs;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if LOAD_I = '1' then
        count_msbs <= COUNT_I(CGN_TOTAL_WIDTH-1 downto CGN_LSB_WIDTH);
        count_lsbs <= COUNT_I(CGN_LSB_WIDTH-1 downto 0);
      elsif ENABLE_I = '1' then
        if CGN_DIR = "UP" then
          count_lsbs <= count_lsbs + 1;
          if count_lsbs = C_LSB_ALL_ONES then
            count_msbs <= count_msbs + 1;
          end if;
        else
          count_lsbs <= count_lsbs - 1;
          if count_lsbs = C_LSB_ALL_ZEROS then
            count_msbs <= count_msbs - 1;
          end if;
        end if;
      end if;
    end if;
  end process;

end behavioral;
