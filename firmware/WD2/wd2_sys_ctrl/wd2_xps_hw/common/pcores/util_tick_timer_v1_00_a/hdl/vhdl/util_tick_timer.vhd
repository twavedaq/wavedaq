---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Tick Timer
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  11.01.2018 11:01:22
--
--  Description :  Generic Tick Timer.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

-- Add timing constraints to ignore cross clock domain paths
-- between microblaze and daq:
-- TIMESPEC TS_UC_TO_DAQ = FROM "TG_UC" TO "TG_DAQ" TIG;
-- TIMESPEC TS_DAQ_TO_UC = FROM "TG_DAQ" TO "TG_UC" TIG;


entity util_tick_timer is
  generic (
    CGN_TICK_TIME_NS  : real :=  1.0e9; -- ns
    CGN_CLK_PERIOD_NS : real := 12.5    -- ns
  );
  port (
    TICK_UC_O  : out std_logic;
    TICK_DAQ_O : out std_logic;
    RST_UC_I   : in  std_logic;
    CLK_UC_I   : in  std_logic;
    CLK_DAQ_I  : in  std_logic
  );
end util_tick_timer;

architecture behavioral of util_tick_timer is
  
  constant C_TIMER_LOAD     : integer := integer(CGN_TICK_TIME_NS/CGN_CLK_PERIOD_NS) - 1;
  constant C_TIMER_SIZE     : integer := log2ceil(C_TIMER_LOAD);
  constant C_TIMER_LOAD_SLV : std_logic_vector(C_TIMER_SIZE-1 downto 0) := CONV_STD_LOGIC_VECTOR(C_TIMER_LOAD, C_TIMER_SIZE);

  signal count_msbs : std_logic_vector(C_TIMER_SIZE-1 downto 2) := (others=>'0');
  signal count_lsbs : std_logic_vector(1 downto 0) := (others=>'0');
  signal count      : std_logic_vector(C_TIMER_SIZE-1 downto 0) := (others=>'0');
  signal tick       : std_logic := '0';
  signal tick_s0    : std_logic := '0';
  signal tick_s1    : std_logic := '0';
  signal tick_s2    : std_logic := '0';
  
begin
  
  TICK_UC_O  <= tick;
  TICK_DAQ_O <= tick_s1 and not tick_s2;
  
  count <= count_msbs & count_lsbs;
  
  counter : process(CLK_UC_I)
  begin
    if rising_edge(CLK_UC_I) then
      if RST_UC_I = '1' or count = 0 then
        count_msbs <= C_TIMER_LOAD_SLV(C_TIMER_SIZE-1 downto 2);
        count_lsbs <= C_TIMER_LOAD_SLV(1 downto 0);
      else
        count_lsbs <= count_lsbs - 1;
        if count_lsbs = C_LSB_ALL_ZEROS then
          count_msbs <= count_msbs - 1;
        end if;
      end if;
    end if;
  end process;

  tick_generator : process(CLK_UC_I)
  begin
    if rising_edge(CLK_UC_I) then
      if count = 0 then
        tick <= '1';
      else
        tick <= '0';
      end if;
    end if;
  end process;

  
  sync_ffs : process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      tick_s0 <= tick;
      tick_s1 <= tick_s0;
      tick_s2 <= tick_s1;
    end if;
  end process;

end architecture behavioral;