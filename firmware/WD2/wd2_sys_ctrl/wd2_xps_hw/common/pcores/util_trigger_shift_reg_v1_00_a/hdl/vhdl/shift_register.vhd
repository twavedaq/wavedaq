---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Simple Shift Register
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  16.09.2014 10:24:30
--
--  Description :  Generic shift register.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity shift_register is
  generic (
    CGN_NR_OF_BITS : integer := 8;
    CGN_DIRECTION  : integer := 0 -- Specifies data direction 0="LSB first" or 1="MSB first"
  );
  port (
    PDATA_I : in  std_logic_vector(CGN_NR_OF_BITS-1 downto 0);
    SDATA_I : in  std_logic;
    PDATA_O : out std_logic_vector(CGN_NR_OF_BITS-1 downto 0);
    SDATA_O : out std_logic;
    LOAD_I  : in  std_logic;
    EN_I    : in  std_logic;
    CLK_I   : in  std_logic
  );
end shift_register;

architecture behavioral of shift_register is

  signal shift_reg : std_logic_vector(CGN_NR_OF_BITS-1 downto 0) := (others=>'0');

begin

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if LOAD_I = '1' then
        shift_reg <= PDATA_I;
      elsif EN_I = '1' then
        if CGN_DIRECTION = 1 then
          shift_reg <= shift_reg(CGN_NR_OF_BITS-2 downto 0) & SDATA_I;
        else -- LSB first
          shift_reg <= SDATA_I & shift_reg(CGN_NR_OF_BITS-1 downto 1);
        end if;
      end if;
    end if;
  end process;

  -- serial output:    vvv MSB-first vvv                                       vvv LSB-first
  SDATA_O <= shift_reg(CGN_NR_OF_BITS-1) when CGN_DIRECTION = 1 else shift_reg(0);
  -- parallel output:
  PDATA_O <= shift_reg;

end architecture behavioral;