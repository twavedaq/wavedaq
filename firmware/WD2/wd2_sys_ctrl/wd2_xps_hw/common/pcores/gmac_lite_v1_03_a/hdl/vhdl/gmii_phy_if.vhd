--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : .vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    interface to external PHY
--    remember to set constaints:
--
--    ############################################################
--    # External GMII Constraints                                #
--    ############################################################
--    INST "*gmii_phy_rxd*"                  IOB = force;
--    INST "*gmii_phy_rx_dv"                 IOB = force;
--    INST "*gmii_phy_rx_er"                 IOB = force;
--
--    INST "*gmii_phy_txd*"                  IOB = force;
--    INST "*gmii_phy_tx_en"                 IOB = force;
--    INST "*gmii_phy_tx_er"                 IOB = force;
--
--    Use "SLEW = FAST" on output pins
--    SLEW = FAST
--
--
--    ############################################################
--    # The following are required to maximize setup/hold        #
--    ############################################################
--    # Changed to add Drive strength and INST Name
--    INST "fpga_0_Soft_TEMAC_GMII_TXD_0_pin_?_OBUF"  SLEW = FAST;
--    INST "fpga_0_Soft_TEMAC_GMII_TX_EN_0_pin_OBUF"  SLEW = FAST;
--    INST "fpga_0_Soft_TEMAC_GMII_TX_ER_0_pin_OBUF"  SLEW = FAST;
--    INST "fpga_0_Soft_TEMAC_GMII_TX_CLK_0_pin_OBUF" SLEW = FAST;
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library unisim;
use unisim.vcomponents.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity gmii_phy_if is
--  generic
--  (
--    C_NUM_REG                      : integer              := 8
--  );
  port
  (
    -- GMII PHY signals
    GMII_PHY_TXD_O              : out std_logic_vector(7 downto 0);
    GMII_PHY_TX_EN_O            : out std_logic;
    GMII_PHY_TX_ER_O            : out std_logic;
    GMII_PHY_TX_CLK_O           : out std_logic;

    GMII_PHY_RXD_I              : in  std_logic_vector(7 downto 0);
    GMII_PHY_RX_DV_I            : in  std_logic;
    GMII_PHY_RX_ER_I            : in  std_logic;
    GMII_PHY_RX_CLK_I           : in  std_logic;

    -- GMII user signals
    GMII_USER_TXD_I              : in  std_logic_vector(7 downto 0);
    GMII_USER_TX_EN_I            : in  std_logic;
    GMII_USER_TX_ER_I            : in  std_logic;
    GMII_USER_TX_CLK_I           : in  std_logic;

    GMII_USER_RXD_O              : out std_logic_vector(7 downto 0);
    GMII_USER_RX_DV_O            : out std_logic;
    GMII_USER_RX_ER_O            : out std_logic;
    GMII_USER_RX_CLK_O           : out std_logic
  );
end entity gmii_phy_if;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of gmii_phy_if is

  signal clk_ibufg        : std_logic;
  signal clk_gmii_rx      : std_logic;

  signal gmii_phy_rxd     : std_logic_vector(7 downto 0);
  signal gmii_phy_rx_dv   : std_logic;
  signal gmii_phy_rx_er   : std_logic;

  signal gmii_phy_txd     : std_logic_vector(7 downto 0);
  signal gmii_phy_tx_en   : std_logic;
  signal gmii_phy_tx_er   : std_logic;

  signal clk_gmii_rx_180  : std_logic;

begin

  ------------------------------------------------------------------------------
  -- RX
  ------------------------------------------------------------------------------

  ---------------------------------------------------
  -- CLK input buffer
  ---------------------------------------------------

  ibufg_inst : IBUFG
  port map
  (
    I => GMII_PHY_RX_CLK_I,
    O => clk_ibufg
  );

  bufg_inst : BUFG
  port map
  (
    I => clk_ibufg,
    O => clk_gmii_rx
  );


  ---------------------------------------------------
  -- register gmii data inputs
  -- tbd: check timing, maybe use idelay
  ---------------------------------------------------

  process(clk_gmii_rx) is
  begin
    if rising_edge(clk_gmii_rx) then
      gmii_phy_rxd    <= GMII_PHY_RXD_I;
      gmii_phy_rx_dv  <= GMII_PHY_RX_DV_I;
      gmii_phy_rx_er  <= GMII_PHY_RX_ER_I;
    end if;
  end process;

  GMII_USER_RXD_O    <= gmii_phy_rxd;
  GMII_USER_RX_DV_O  <= gmii_phy_rx_dv;
  GMII_USER_RX_ER_O  <= gmii_phy_rx_er;
  GMII_USER_RX_CLK_O <= clk_gmii_rx;

  ------------------------------------------------------------------------------
  -- TX
  ------------------------------------------------------------------------------

  process(GMII_USER_TX_CLK_I) is
  begin
    if rising_edge(GMII_USER_TX_CLK_I) then
      gmii_phy_txd    <= GMII_USER_TXD_I;
      gmii_phy_tx_en  <= GMII_USER_TX_EN_I;
      gmii_phy_tx_er  <= GMII_USER_TX_ER_I;
    end if;
  end process;


  clk_gmii_rx_180 <= not GMII_USER_TX_CLK_I;

  ODDR2_inst : ODDR2
  generic map
  (
    DDR_ALIGNMENT => "NONE",  -- Sets output alignment to "NONE", "C0", "C1"
    INIT          => '0',     -- Sets initial state of the Q output to '0' or '1'
    SRTYPE        => "SYNC"   -- Specifies "SYNC" or "ASYNC" set/reset
  )
  port map
  (
    Q  => GMII_PHY_TX_CLK_O,  -- 1-bit output data
    C0 => GMII_USER_TX_CLK_I, -- 1-bit clock input
    C1 => clk_gmii_rx_180,    -- 1-bit clock input
    CE => '1',                -- 1-bit clock enable input
    D0 => '1',                -- 1-bit data input (associated with C0)
    D1 => '0',                -- 1-bit data input (associated with C1)
    R  => '0',                -- 1-bit reset input
    S  => '0'                 -- 1-bit set input
  );


  GMII_PHY_TXD_O    <= gmii_phy_txd;
  GMII_PHY_TX_EN_O  <= gmii_phy_tx_en;
  GMII_PHY_TX_ER_O  <= gmii_phy_tx_er;


-- obuf_inst : OBUF
-- generic map
-- (
--   DRIVE      => 12,
--   IOSTANDARD => "DEFAULT",
--   SLEW       => "FAST")
-- port map
-- (
--   O => O,   -- Buffer output (connect directly to top-level port)
--   I => I    -- Buffer input
-- );

end architecture behave;
