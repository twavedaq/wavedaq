--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

-- common definition of patterns

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;


package common is

TYPE PatternType IS ARRAY (31 downto 0 ) OF  STD_LOGIC_VECTOR(31 downto 0);
end common;

package body common is
 
end common;
