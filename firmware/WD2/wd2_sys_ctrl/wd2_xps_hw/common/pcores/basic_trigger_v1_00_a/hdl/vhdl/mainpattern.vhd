
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Marco Francesconi
-- 
-- Create Date:    16:54:27 05/26/2016 
-- Design Name: 
-- Module Name:    mainpattern_vhd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: here there is a pattern-based trigger, 
--				mainly intended for BGO Cosmic RUN
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.OR_REDUCE;
use IEEE.STD_LOGIC_MISC.AND_REDUCE;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity mainpattern is
  Generic(
    CGN_NR_OF_PATTERNS : integer := 32;
    CGN_PATTERN_WIDTH  : integer := 32
  );
  Port(
    CLK_I         : in  std_logic;
    RUNMODE_I     : in  std_logic; -- enables trigger generation
    PATTERNS_I    : in  std_logic_vector(CGN_NR_OF_PATTERNS*CGN_PATTERN_WIDTH-1 downto 0); -- patterns_i (array of 32 vectors of 32 bits)
    PATTERN_ENA_I : in  std_logic_vector (CGN_NR_OF_PATTERNS-1 downto 0); -- pattern bit enable (equivalent to set pattern(i)=X"00000000")
    COMP_I        : in  std_logic_vector (15 downto 0); -- comparator input
    TRIG_O        : out std_logic --trigger output
    );
end mainpattern;

architecture Behavioral of mainpattern is 

  type pattern_type is array (CGN_NR_OF_PATTERNS-1 downto 0 ) OF  std_logic_vector(CGN_PATTERN_WIDTH-1 downto 0);
  signal patterns     : pattern_type := (others=>(others=>'0'));          -- patterns
--  signal patternmask  : pattern_type := (others=>(others=>'0'));          -- pattern mask
  signal reg_comp_p     : std_logic_vector (15 downto 0) := (others=>'0');  -- input comparator register
  signal reg_comp_n     : std_logic_vector (15 downto 0) := (others=>'0');  -- input comparator register
  signal patternmatch_p : std_logic_vector (31 downto 0) := (others=>'0');  -- pattern matched register
  signal patternmatch_n : std_logic_vector (31 downto 0) := (others=>'0');  -- pattern matched register
  signal patternmatch   : std_logic_vector (31 downto 0) := (others=>'0');  -- pattern matched register
  signal int_trg_p      : std_logic := '0';                                 -- fired once we have a pattern match
  signal int_trg_n      : std_logic := '0';                                 -- fired once we have a pattern match

begin

  -- Truth table:
  --
  -- ch   = channel
  -- ptrn = pattern number
  -- c  = COMP_I(ch)
  -- pp = Pattern(ch)(ptrn)
  -- pn = Pattern(ch)(ptrn+15)
  -- o  = patternmatch(ch)
  --
  -- c  pp pn   o
  -- 0  0  0    1
  -- 0  0  1    1
  -- 0  1  0    0
  -- 0  1  1    0
  -- 1  0  0    1
  -- 1  0  1    0
  -- 1  1  0    1
  -- 1  1  1    0

  -- check pattern matching for enabled ones, then store the result in PATTERNMATCH array
  g_pattern : for i in 0 to CGN_NR_OF_PATTERNS-1 generate

  -- mapping of the patterns from large vector to specific type
    patterns(i) <= PATTERNS_I((i+1)*CGN_PATTERN_WIDTH-1 downto i*CGN_PATTERN_WIDTH);

    process(PATTERN_ENA_I, COMP_I, patterns)
    begin
      if PATTERN_ENA_I(i) = '0' or patterns(i) = 0 then
        patternmatch(i) <= '0';
      else
        patternmatch(i) <= and_reduce(    (not(COMP_I) and not(patterns(i)(15 downto  0)))
                                       or (    COMP_I  and not(patterns(i)(31 downto 16))) );
      end if;
    end process;

  end generate g_pattern;

  -- enables trigger generation only if RUNMODE_I = '1'
  --     maybe can be moved to the main trigger logic
  --     by design RUNMODE_I should be set to something like "not BUSY"
  TRIG_O <= or_reduce(patternmatch) when RUNMODE_I = '1' else '0';

end Behavioral;


