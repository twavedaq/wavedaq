---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Trigger Linking Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  24.11.2015 13:05:08
--
--  Description :  Linkage of independent trigger channels for WD2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity trigger_linkage is
  generic (
    CGN_NO_CHANNELS     : integer   := 32 -- Nr of inputs from comparators
  );
  port (
    -- Trigger Signals
    LOCAL_TRIG_I       : in  std_logic_vector(CGN_NO_CHANNELS-1 downto 0);
    EXT_TRIG_BPL_I     : in  std_logic;
    EXT_TRIG_MCX_I     : in  std_logic;
    AUTO_TRIG_I        : in  std_logic;
    TRIG_O             : out std_logic;
    -- Trigger Configuration
    CFG_ENABLE_TRIG_I  : in  std_logic;
    CFG_AUTO_TRIG_EN_I : in  std_logic;
    CFG_CHANNEL_AND_I  : in  std_logic_vector(CGN_NO_CHANNELS-1 downto 0);
    CFG_CHANNEL_OR_I   : in  std_logic_vector(CGN_NO_CHANNELS-1 downto 0);
    CFG_EXT_TRIG_AND_I : in  std_logic;
    CFG_EXT_TRIG_OR_I  : in  std_logic
  );
end trigger_linkage;

architecture behavioral of trigger_linkage is

  constant C_CFG_ZEROS    : std_logic_vector(CGN_NO_CHANNELS-1 downto 0) := (others=>'0');

  signal trigger_or       : std_logic := '0';
  signal trigger_and      : std_logic := '0';
  signal logical_or_term  : std_logic_vector(CGN_NO_CHANNELS downto 0) := (others=>'0');
  signal logical_and_term : std_logic_vector(CGN_NO_CHANNELS downto 0) := (others=>'0');

  signal auto_trigger     : std_logic := '0';
  
begin

  -- Generate Trigger Terms
  mask_inst : process(LOCAL_TRIG_I, EXT_TRIG_BPL_I, EXT_TRIG_MCX_I, CFG_CHANNEL_OR_I, CFG_CHANNEL_AND_I, CFG_EXT_TRIG_OR_I, CFG_EXT_TRIG_AND_I)
  begin
    -- Local Trigger Terms
    for i in CGN_NO_CHANNELS-1 downto 0 loop
      logical_or_term(i)  <= LOCAL_TRIG_I(i) and CFG_CHANNEL_OR_I(i);
      logical_and_term(i) <= LOCAL_TRIG_I(i) or not CFG_CHANNEL_AND_I(i);
    end loop;
    -- External Trigger Terms
    logical_or_term(CGN_NO_CHANNELS)  <= (EXT_TRIG_BPL_I or EXT_TRIG_MCX_I) and CFG_EXT_TRIG_OR_I;
    logical_and_term(CGN_NO_CHANNELS) <= (EXT_TRIG_BPL_I or EXT_TRIG_MCX_I) or not CFG_EXT_TRIG_AND_I;
  end process mask_inst;

  auto_trigger <= AUTO_TRIG_I when CFG_AUTO_TRIG_EN_I = '1' else '0';

  -- Reduce OR/AND triggers
  trigger_or  <= OR_REDUCE(logical_or_term) or auto_trigger;
  trigger_and <= AND_REDUCE(logical_and_term) when ((CFG_CHANNEL_AND_I /= C_CFG_ZEROS) or (CFG_EXT_TRIG_AND_I = '1'))
                                              else '0';

  -- Link OR/AND triggers
  TRIG_O <= ( trigger_or or trigger_and ) when ( CFG_ENABLE_TRIG_I = '1' ) else '0';

end architecture behavioral;
