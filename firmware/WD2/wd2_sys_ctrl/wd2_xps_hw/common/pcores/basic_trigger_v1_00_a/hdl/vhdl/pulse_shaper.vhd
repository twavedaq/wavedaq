---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Pulse Shaper
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid, Stefan Ritt
--  Created :  17.03.2017 13:31:20
--
--  Description :  Simple pulse shaper with asynchronous set and synchronous reset.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity pulse_shaper is
  generic (
    CGN_LENGTH_BITS : integer := 3
  );
  port (
    -- Trigger Signals
    PULSE_I        : in  std_logic;
    PULSE_O        : out std_logic;
    PULSE_LENGTH_I : in  std_logic_vector(CGN_LENGTH_BITS-1 downto 0);
    CLK_I          : in  std_logic
  );
end pulse_shaper;

architecture behavioral of pulse_shaper is

  signal pulse_ff       : std_logic := '1';
  signal pulse_ff_rst   : std_logic := '0';
  signal pw_delay       : std_logic_vector(2**CGN_LENGTH_BITS-1 downto 0) := (others=>'0');
  signal pulse_i_sync   : std_logic_vector(2 downto 0) := (others=>'0');
  signal pulse_o_sync   : std_logic_vector(2 downto 0) := (others=>'0');

begin

--  pulse_ff_inst : process(PULSE_I, pulse_ff_rst)
--  begin
--    if PULSE_I = '1' then
--      pulse_ff <= '1';
--    elsif  pulse_ff_rst = '1' then
--      pulse_ff <= '0';
--    end if;
--  end process pulse_ff_inst;

  pulse_ff_inst : process(PULSE_I, CLK_I)
  begin
    if PULSE_I = '1' then
      pulse_ff <= '1';
    elsif rising_edge(CLK_I) then
      if pulse_ff_rst = '1' then
        pulse_ff <= '0';
      end if;
    end if;
  end process pulse_ff_inst;

  process(CLK_I, PULSE_I, pulse_ff)
  begin
    pulse_o_sync(0) <= pulse_ff;
    pulse_i_sync(0) <= PULSE_I;
    if rising_edge(CLK_I) then
      for s in 0 to 1 loop
        pulse_i_sync(s+1) <= pulse_i_sync(s);
        pulse_o_sync(s+1) <= pulse_o_sync(s);
      end loop;
      if pulse_ff_rst = '1' then
        -- reset trigger pulse
          pulse_o_sync(2 downto 1) <= (others=>'0');
      end if;
    end if;
  end process;

  -- delay line
  process(CLK_I, pulse_o_sync)
  begin
    pw_delay(0) <= pulse_o_sync(2);
    if rising_edge(CLK_I) then
      if pulse_ff_rst = '1' then
        pw_delay(2**CGN_LENGTH_BITS-1 downto 1) <= (others=>'0');
      else
        for s in 2**CGN_LENGTH_BITS-2 downto 0 loop
          pw_delay(s+1) <= pw_delay(s);
        end loop;
      end if;
    end if;
  end process;

  -- delay line mux
  pulse_ff_rst <= pw_delay(CONV_INTEGER(PULSE_LENGTH_I)) and not(pulse_i_sync(2));

  PULSE_O <= pulse_ff;

end architecture behavioral;
