---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Tap for DCDL with HFN Input
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.04.2016 12:33:40
--
--  Description :  Tap for Digitally Controlled Delay Line using a High Fanout Network
--                 at the input.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

--    ############################################################
--    # External Constraints                                     #
--    ############################################################
--    INST "*_iob_ff*"   IOB = force;

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity dcdl_4tap is
  Generic
  (
    CGN_TAP_TYPE : string := "LUT LATCH AS LOGIC"  -- options: "LUT ONLY", "CARRY"
  );
  Port
  (
    TAP_CASCADE_I : in  std_logic;
    TAP_I         : in  std_logic_vector(3 downto 0);
    TAP_SELECT_I  : in  std_logic_vector(3 downto 0);
    TAP_CASCADE_O : out std_logic
  );
end dcdl_4tap;

architecture structural of dcdl_4tap is

  signal lut_to_latch : std_logic_vector(3 downto 0);
  signal tap_link     : std_logic_vector(4 downto 0);

  attribute RLOC : string;
--  attribute RLOC of lut_to_latch : signal is "X0Y0";

begin

  LUT_LATCH_IMPLEMENTATION : if CGN_TAP_TYPE = "LUT LATCH AS LOGIC" generate

    tap_link(0)   <= TAP_CASCADE_I;
    TAP_CASCADE_O <= tap_link(4);

    LUTS_IN_SLICE : for i in 0 to 3 generate
      attribute RLOC of LUT3_inst    : label is "X0Y0";
      attribute RLOC of AND2B1L_inst : label is "X0Y0";
--      attribute RLOC of tap_link     : signal is "X0Y0";
    begin
      LUT3_inst : LUT3
      generic map (
        INIT => X"CA")
      port map (
        O  => lut_to_latch(i), -- LUT general output
        I0 => tap_link(i),     -- LUT input
        I1 => TAP_I(i),        -- LUT input
        I2 => TAP_SELECT_I(i)  -- LUT input
      );
      
      AND2B1L_inst : AND2B1L
      port map (
        O   => tap_link(i+1),   -- LATCH-GATE output
        DI  => lut_to_latch(i), -- LATCH-GATE input (high active)
        SRI => '0'              -- LATCH-GATE input (low active)
      );
    end generate LUTS_IN_SLICE;

  end generate LUT_LATCH_IMPLEMENTATION;


  LUT_ONLY_IMPLEMENTATION : if CGN_TAP_TYPE = "LUT ONLY" generate

    tap_link(0)   <= TAP_CASCADE_I;
    TAP_CASCADE_O <= tap_link(4);

    LUTS_IN_SLICE : for i in 0 to 3 generate
      LUT3_inst : LUT3
      generic map (
        INIT => X"CA")
      port map (
        O  => tap_link(i+1),  -- LUT general output
        I0 => tap_link(i),    -- LUT input
        I1 => TAP_I(i),       -- LUT input
        I2 => TAP_SELECT_I(i) -- LUT input
      );
    end generate LUTS_IN_SLICE;

  end generate LUT_ONLY_IMPLEMENTATION;


  CARRY_TYPE_IMPLEMENTATION : if CGN_TAP_TYPE = "CARRY" generate

    tap_link(0)   <= TAP_CASCADE_I;
    TAP_CASCADE_O <= tap_link(4);
  
    CARRY4_inst : CARRY4
    port map (
      CO     => tap_link(4 downto 1), -- 4-bit carry out (3:0)
      O      => open,                 -- 4-bit carry chain XOR data out (3:0)
      CI     => tap_link(0),          -- 1-bit carry cascade input
      CYINIT => '0',                  -- 1-bit carry initialization
      DI     => TAP_I,                -- 4-bit carry-MUX data in (3:0)
      S      => TAP_SELECT_I          -- 4-bit carry-MUX select input (3:0)
    );

  end generate CARRY_TYPE_IMPLEMENTATION;

end structural;

