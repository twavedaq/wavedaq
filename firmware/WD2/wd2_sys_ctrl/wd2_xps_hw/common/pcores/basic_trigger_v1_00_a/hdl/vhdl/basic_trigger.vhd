---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Basic Trigger Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  19.10.2015 15:42:08
--
--  Description :  Simple basic trigger unit. Reduces and forwards the comparator signals
--                 from the front end to the OSERDES lines going to the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library basic_trigger_v1_00_a;
use basic_trigger_v1_00_a.trigger_linkage;
use basic_trigger_v1_00_a.hfnin_dcdl;
use basic_trigger_v1_00_a.mainpattern;
use basic_trigger_v1_00_a.trigger_iserdes;
use basic_trigger_v1_00_a.pulse_shaper;
use basic_trigger_v1_00_a.auto_trigger;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity basic_trigger is
  generic (
    CGN_NR_OF_PATTERNS : integer   := 32;
    CGN_DELAY_SEL_BITS : integer   := 9;
    CGN_COMP_DIFF_IN   : integer   := 0;  -- 1 = diff input, 0 = SE input
    CGN_DIFF_OUT       : integer   := 0;  -- 1 = diff output, 0 = SE output
    CGN_PULSE_WIDTH    : integer   := 4;  -- Minimum output pulse width in clock cycles (max is one more)
    CGN_NR_OF_COMP     : integer   := 16; -- Nr of inputs from comparators
    CGN_TRG_BUS_WIDTH  : integer   := 8
  );
  port (
    -- Trigger Signals
    COMP_BUS_P_I              : in  std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    COMP_BUS_N_I              : in  std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    EXT_TRIG_BPL_P_I          : in  std_logic;
    EXT_TRIG_BPL_N_I          : in  std_logic;
    EXT_TRIG_MCX_I            : in  std_logic;
    EXT_TRIG_MCX_O            : out std_logic;
    EXT_TRIG_MCX_OUT_EN_I     : in  std_logic;
    EXT_TRIG_O                : out std_logic;
    TRIG_NO_DELAY_O           : out std_logic;
    TRIG_O                    : out std_logic;
    COMP_TO_TRG_O             : out std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    -- Trigger Configuration
    CFG_PATTERNS_I            : in  std_logic_vector(CGN_NR_OF_PATTERNS*CGN_NR_OF_COMP*2-1 downto 0); -- patterns_i (array of 32 vectors of 32 bits)
    CFG_LCL_PATTERN_ENA_I     : in  std_logic_vector(CGN_NR_OF_PATTERNS-1 downto 0); -- pattern bit enable (equivalent to set pattern(i)=X"00000000")
    CFG_TRIG_SCHEME_SEL_I     : in  std_logic_vector(1 downto 0);
    CFG_ENABLE_TRIG_I         : in  std_logic;
    CFG_SHAPER_ENABLE_I       : in  std_logic;
    CFG_MASK_TRIG_I           : in  std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    CFG_FALLING_EDGE_I        : in  std_logic;
    CFG_CHANNEL_AND_I         : in  std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    CFG_CHANNEL_OR_I          : in  std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    CFG_EXT_TRIG_AND_I        : in  std_logic;
    CFG_EXT_TRIG_OR_I         : in  std_logic;
    CFG_TRIG_DELAY_I          : in  std_logic_vector(CGN_DELAY_SEL_BITS-1 downto 0);
    CFG_BYPASS_DELAY_I        : in  std_logic;
    CFG_TRIG_PULSE_LENGTH_I   : in  std_logic_vector(2 downto 0);
    CFG_AUTO_TRIGGER_ENABLE_I : in  std_logic;
    CFG_AUTO_TRIGGER_PERIOD_I : in  std_logic_vector(31 downto 0);
    -- TDC
    TRG_TDC_DATA_O            : out std_logic_vector(8*CGN_NR_OF_COMP-1 downto 0);
    TRG_TDC_BIT_CLK_I         : in  std_logic;
    TRG_TDC_DIV_CLK_I         : in  std_logic;
    TRG_TDC_PLL_LOCKED_I      : in  std_logic;
    TRG_TDC_LOCKED_O          : out std_logic;
    TRG_TDC_PLL_RST_I         : in  std_logic;
    -- Triggered Channels within window
    ACTIVE_CH_O               : out std_logic_vector(CGN_NR_OF_COMP-1 downto 0);
    ACTIVE_CH_WINDOW_I        : in  std_logic_vector(7 downto 0);
    ACTIVE_CH_POLARITY_I      : in  std_logic; -- 1 = positive, 0 = inverted
    -- Clock
    SYS_CLK_I                 : in  std_logic;
    CLK_I                     : in  std_logic
  );
end basic_trigger;

architecture behavioral of basic_trigger is

  constant C_DIV              : integer := CGN_NR_OF_COMP/CGN_TRG_BUS_WIDTH;
  constant C_PATTERNS_WIDTH   : integer := CGN_NR_OF_PATTERNS*CGN_NR_OF_COMP*2;
  
  type trigger_sync_type is array (CGN_NR_OF_COMP-1 downto 0) of std_logic_vector(3 downto 0);
  signal trigger_sync         : trigger_sync_type := (others=>(others=>'0'));
  
  signal cfg_lcl_pattern_ena  : std_logic_vector(CGN_NR_OF_PATTERNS-1 downto 0) := (others=>'0'); 

  signal cfg_lcl_trig_scheme_sel : std_logic_vector(1 downto 0) := (others=>'0');

  signal cfg_mask_trig           : std_logic_vector(CGN_NR_OF_COMP-1 downto 0)  := (others=>'0');

  signal comp_bus                : std_logic_vector(CGN_NR_OF_COMP-1 downto 0)  := (others=>'0');
  signal comp_bus_pn             : std_logic_vector(CGN_NR_OF_COMP-1 downto 0)  := (others=>'0');
  signal comp_bus_s              : std_logic_vector(CGN_NR_OF_COMP-1 downto 0)  := (others=>'0');
  signal trigger_ff              : std_logic_vector(CGN_NR_OF_COMP-1 downto 0)  := (others=>'0');
  signal trigger_ff_rst          : std_logic_vector(CGN_NR_OF_COMP-1 downto 0)  := (others=>'0');
  signal cfg_trig_delay          : std_logic_vector(CGN_DELAY_SEL_BITS-1 downto 0) := (others=>'0');
  signal external_trigger_bpl    : std_logic := '0';
  signal ext_trig_mcx_out_en     : std_logic := '0';

  signal cfg_enable_trig         : std_logic := '0';
  signal cfg_falling_edge        : std_logic := '0';
  signal cfg_channel_and         : std_logic_vector(CGN_NR_OF_COMP-1 downto 0) := (others=>'0');
  signal cfg_channel_or          : std_logic_vector(CGN_NR_OF_COMP-1 downto 0) := (others=>'0');
  signal cfg_ext_trig_and        : std_logic := '0';
  signal cfg_ext_trig_or         : std_logic := '0';
  signal cfg_bypass_delay        : std_logic := '0';
  signal cfg_auto_trig_en        : std_logic := '0';

  signal trigger                 : std_logic := '0';
  signal trigger_out             : std_logic := '0';
  signal simple_trigger          : std_logic := '0';
  signal pattern_trigger_local   : std_logic := '0';
  signal cfg_patterns            : std_logic_vector(C_PATTERNS_WIDTH-1 downto 0) := (others=>'0'); 

  signal auto_trigger            : std_logic := '0';
  signal auto_trigger_rst        : std_logic := '0';

  signal active_ch_polarity      : std_logic := '0';
  signal active_ch_window        : std_logic_vector(7 downto 0) := (others=>'0');
  
  type srl_delay_type is array (CGN_NR_OF_COMP-1 downto 0) of std_logic_vector(4 downto 0);
  signal srl_delay               : srl_delay_type := (others=>(others=>'0'));
  type srl_delay_addr_type is array (3 downto 0) of std_logic_vector(3 downto 0);
  signal srl_delay_addr          : srl_delay_addr_type := (others=>(others=>'0'));
  signal cfg_trig_pulse_length   : std_logic_vector(2 downto 0) := (others=>'0');

begin

  cdc_sync_sys_to_trig_cfg_0 : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  19,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => CLK_I,
    PORT_A_I(0) => CFG_ENABLE_TRIG_I,
    PORT_A_I(1) => CFG_FALLING_EDGE_I,
    PORT_A_I(2) => CFG_EXT_TRIG_AND_I,
    PORT_A_I(3) => CFG_EXT_TRIG_OR_I,
    PORT_A_I(4) => CFG_BYPASS_DELAY_I,
    PORT_A_I(5) => ACTIVE_CH_POLARITY_I,
    PORT_A_I(6) => CFG_AUTO_TRIGGER_ENABLE_I,
    PORT_A_I(7) => EXT_TRIG_MCX_OUT_EN_I,
    PORT_A_I(15 downto  8) => ACTIVE_CH_WINDOW_I,
    PORT_A_I(18 downto 16) => CFG_TRIG_PULSE_LENGTH_I,
    PORT_B_O(0) => cfg_enable_trig,
    PORT_B_O(1) => cfg_falling_edge,
    PORT_B_O(2) => cfg_ext_trig_and,
    PORT_B_O(3) => cfg_ext_trig_or,
    PORT_B_O(4) => cfg_bypass_delay,
    PORT_B_O(5) => active_ch_polarity,
    PORT_B_O(6) => cfg_auto_trig_en,
    PORT_B_O(7) => ext_trig_mcx_out_en,
    PORT_B_O(15 downto  8) => active_ch_window,
    PORT_B_O(18 downto 16) => cfg_trig_pulse_length
  );
  
  
  cdc_sync_sys_to_trig_cfg_1 : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_NR_OF_COMP,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_CHANNEL_AND_I,
    PORT_B_O => cfg_channel_and
  );
  
  cdc_sync_sys_to_trig_cfg_2 : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_NR_OF_COMP,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_CHANNEL_OR_I,
    PORT_B_O => cfg_channel_or
  );
  
  cdc_sync_sys_to_trig_cfg_3 : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_NR_OF_COMP,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_MASK_TRIG_I,
    PORT_B_O => cfg_mask_trig
  );



  cdc_sync_sys_to_trigger_delay : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_DELAY_SEL_BITS,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => CLK_I,
    PORT_A_I => CFG_TRIG_DELAY_I,
    PORT_B_O => cfg_trig_delay
  );

  cdc_sync_sys_to_trig_cfg_patterns : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  C_PATTERNS_WIDTH,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => CLK_I,
    PORT_A_I => CFG_PATTERNS_I,
    PORT_B_O => cfg_patterns
  );

  cdc_sync_sys_to_trig_cfg_pattern_en : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_NR_OF_PATTERNS,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => CLK_I,
    PORT_A_I => CFG_LCL_PATTERN_ENA_I,
    PORT_B_O => cfg_lcl_pattern_ena
  );

  cdc_sync_sys_trig_scheme_sel : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  2,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => CLK_I,
    PORT_A_I => CFG_TRIG_SCHEME_SEL_I,
    PORT_B_O => cfg_lcl_trig_scheme_sel
  );

  comp_bus_ibufds_inst : IBUFDS
  generic map (
    DIFF_TERM => TRUE,
    IBUF_LOW_PWR => TRUE,
    IOSTANDARD => "LVDS_25")
  port map (
    O  => external_trigger_bpl,
    I  => EXT_TRIG_BPL_P_I,
    IB => EXT_TRIG_BPL_N_I
  );
  EXT_TRIG_O <= external_trigger_bpl or EXT_TRIG_MCX_I;

  pw_gen_inst : for i in CGN_NR_OF_COMP-1 downto 0 generate

    comp_diff_in_buffer : if CGN_COMP_DIFF_IN = 1 generate
      comp_bus_ibufds_inst : IBUFDS
      generic map (
        DIFF_TERM => TRUE,
        IBUF_LOW_PWR => TRUE,
        IOSTANDARD => "LVDS_25")
      port map (
        O  => comp_bus(i),
        I  => COMP_BUS_P_I(i),
        IB => COMP_BUS_N_I(i)
      );
    end generate comp_diff_in_buffer;

    comp_se_in_buffer : if CGN_COMP_DIFF_IN = 0 generate
      comp_bus(i) <= COMP_BUS_P_I(i);
    end generate comp_se_in_buffer;

    comp_bus_pn(i) <= '0'         when cfg_mask_trig(i) = '0' else
                      comp_bus(i) when cfg_falling_edge = '0' else
                      not comp_bus(i);

    pulse_shaper_inst : entity basic_trigger_v1_00_a.pulse_shaper
      generic map (
      CGN_LENGTH_BITS => 3
    )
      port map (
      -- Trigger Signals
      PULSE_I        => comp_bus_pn(i),
      PULSE_O        => trigger_ff(i),
      PULSE_LENGTH_I => cfg_trig_pulse_length,
      CLK_I          => CLK_I
    );

    comp_bus_s(i)    <= trigger_ff(i) when CFG_SHAPER_ENABLE_I = '1' else comp_bus_pn(i);
    COMP_TO_TRG_O(i) <= comp_bus_s(i);

  end generate pw_gen_inst;

  pattern_trigger_local_inst : entity basic_trigger_v1_00_a.mainpattern
    generic map(
      CGN_NR_OF_PATTERNS => CGN_NR_OF_PATTERNS,
      CGN_PATTERN_WIDTH  => 2*CGN_NR_OF_COMP
    )
    port map(
      CLK_I         => CLK_I,
      RUNMODE_I     => cfg_enable_trig,
      PATTERNS_I    => cfg_patterns,
      PATTERN_ENA_I => cfg_lcl_pattern_ena,
      COMP_I        => comp_bus_s,
      TRIG_O        => pattern_trigger_local
    );

  trigger_linkage_inst : entity basic_trigger_v1_00_a.trigger_linkage
    generic map
    (
      CGN_NO_CHANNELS => CGN_NR_OF_COMP
    )
    port map
    (
      LOCAL_TRIG_I       => comp_bus_s,
      EXT_TRIG_BPL_I     => external_trigger_bpl,
      EXT_TRIG_MCX_I     => EXT_TRIG_MCX_I,
      AUTO_TRIG_I        => auto_trigger,
      TRIG_O             => simple_trigger,
      CFG_ENABLE_TRIG_I  => cfg_enable_trig,
      CFG_AUTO_TRIG_EN_I => cfg_auto_trig_en,
      CFG_CHANNEL_AND_I  => cfg_channel_and,
      CFG_CHANNEL_OR_I   => cfg_channel_or,
      CFG_EXT_TRIG_AND_I => cfg_ext_trig_and,
      CFG_EXT_TRIG_OR_I  => cfg_ext_trig_or
    );

  -- select trigger type
  trigger <= pattern_trigger_local when (cfg_lcl_trig_scheme_sel(1) = '1') else simple_trigger;

  asynch_delay_inst : entity basic_trigger_v1_00_a.hfnin_dcdl
  generic map
  (
    CGN_DELAY_SEL_BITS => CGN_DELAY_SEL_BITS,
    CGN_TAP_TYPE       => "LUT LATCH AS LOGIC"
  )
  port map
  (
    IN_I      => trigger,
    DELAY_I   => CFG_TRIG_DELAY_I,
    BYPASS_I  => cfg_bypass_delay,
    OUT_O     => trigger_out
  );
  EXT_TRIG_MCX_O <= trigger_out when ext_trig_mcx_out_en = '1' else '0';
  TRIG_O         <= trigger_out;

--  synch_delay_inst : entity basic_trigger_v1_00_a.synchronous_delay
--  generic map
--  (
--    CGN_TAP_MULTIPLIER    => 20
--  )
--  port map
--  (
--    DELAY_I   => cfg_trig_delay(0),
--    SIG_IN_I  => trigger,
--    SIG_OUT_O => TRIG_O,
--    CLK_I     => CLK_I
--  );

  auto_trigger_rst <= not(cfg_auto_trig_en);

  auto_trigger_inst : entity basic_trigger_v1_00_a.auto_trigger
  port map
  (
    LOAD_VALUE_I => CFG_AUTO_TRIGGER_PERIOD_I,
    TIMER_UP_O   => auto_trigger,
    RST_I        => auto_trigger_rst,
    CLK_I        => CLK_I
  );

  TRIG_NO_DELAY_O <= trigger;

  trigger_iserdes_inst : entity basic_trigger_v1_00_a.trigger_iserdes
  Generic map
  (
    CGN_NR_OF_CHANNELS => CGN_NR_OF_COMP,
    CGN_RATIO          => 8
  )
  Port map
  (
    SERIAL_TRIGGER_I     => comp_bus,
    PARALLEL_TRIGGER_O   => TRG_TDC_DATA_O,
    BIT_CLK_I            => TRG_TDC_BIT_CLK_I,
    DIV_CLK_I            => TRG_TDC_DIV_CLK_I,
    PLL_LOCKED_I         => TRG_TDC_PLL_LOCKED_I,
    LOCKED_O             => TRG_TDC_LOCKED_O,
    RESET_I              => TRG_TDC_PLL_RST_I,
    ACTIVE_CH_O          => ACTIVE_CH_O,
    ACTIVE_CH_WINDOW_I   => active_ch_window,
    ACTIVE_CH_POLARITY_I => active_ch_polarity
  );

end architecture behavioral;
