---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Asynchronous Delay
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid, Stefan Ritt
--  Created :  24.11.2015 15:32:12
--
--  Description :  Synchronous delay with LUTs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity synchronous_delay is
  generic (
    --CGN_TAP_MULTIPLIER    : integer := 1;
    --CGN_DELAY_VALUE_WIDTH : integer := 8
	CGN_TAP_MULTIPLIER    : integer := 20
  );
  port (
    -- Trigger Signals
    --DELAY_I   : in  std_logic_vector(CGN_DELAY_VALUE_WIDTH-1 downto 0);
    DELAY_I   : in  std_logic;
    SIG_IN_I  : in  std_logic;
    SIG_OUT_O : out std_logic;
    CLK_I     : in  std_logic
  );
end synchronous_delay;

architecture behavioral of synchronous_delay is

  signal delay_line : std_logic_vector(CGN_TAP_MULTIPLIER-1 downto 0) := (others=>'0');

begin

  -- buffer chain delay for hardware trigger
  delay_line(0) <= SIG_IN_I;

  delay_ffs : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
	  for i in 1 to CGN_TAP_MULTIPLIER-1 loop
	    delay_line(i) <= delay_line(i-1);
	  end loop;
    end if;
  end process;
  
  -- select delayed hardware trigger
  SIG_OUT_O <= delay_line(0) when DELAY_I = '0' else delay_line(CGN_TAP_MULTIPLIER-1);
  --SIG_OUT_O <= delay_line(CONV_INTEGER(DELAY_I));

end architecture behavioral;
