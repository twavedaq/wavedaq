---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  DCDL with HFN Input
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.04.2016 12:29:31
--
--  Description :  Digitally Controlled Delay Line using a High Fanout Network
--                 at the input.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

--    ############################################################
--    # External Constraints                                     #
--    ############################################################
--    INST "*_iob_ff*"   IOB = force;

library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VComponents.all;

library basic_trigger_v1_00_a;
use basic_trigger_v1_00_a.dcdl_4tap;

entity hfnin_dcdl is
  Generic
  (
    CGN_DELAY_SEL_BITS : integer := 9;
    CGN_TAP_TYPE       : string  := "LUT LATCH AS LOGIC"  -- options: "LUT ONLY", "CARRY"
  );
  Port
  (
    IN_I     : in  STD_LOGIC;
    DELAY_I  : in  STD_LOGIC_VECTOR (CGN_DELAY_SEL_BITS-1 downto 0);
    BYPASS_I : in  STD_LOGIC;
    OUT_O    : out STD_LOGIC
  );
end hfnin_dcdl;

architecture behavioral of hfnin_dcdl is

  component dcdl_4tap is
    Generic
    (
      CGN_TAP_TYPE : string := "LUT LATCH AS LOGIC"
    );
    Port
    (
      TAP_CASCADE_I : in  std_logic;
      TAP_I         : in  std_logic_vector(3 downto 0);
      TAP_SELECT_I  : in  std_logic_vector(3 downto 0);
      TAP_CASCADE_O : out std_logic
    );
  end component;

  signal dly_tap_in        : std_logic;
  signal dly_tap_in_vector : std_logic_vector(3 downto 0);
  signal dly_chain         : std_logic_vector((2**CGN_DELAY_SEL_BITS)/4 downto 0);
  signal tap_sel           : std_logic_vector(2**CGN_DELAY_SEL_BITS-1 downto 0);

  attribute RLOC : string;
--  attribute RLOC of dly_chain : signal is "X1Y0";

begin

  OUT_O <= IN_I when BYPASS_I = '1' else dly_chain((2**CGN_DELAY_SEL_BITS)/4);

  -- Using a BUFH saves precious BUFG resources
  -- while leading to a higher initial delay
  BUFG_delay_hfn_inst : BUFG
  --BUFH_delay_hfn_inst : BUFH
  port map (
    O => dly_tap_in,
    I => IN_I
  );

  process(DELAY_I)
  begin
    tap_sel <= (others=>'0');
    tap_sel(2**CGN_DELAY_SEL_BITS-CONV_INTEGER(DELAY_I)-1) <= '1';
  end process;

  dly_chain(0) <= '0';

  dly_tap_in_vector <= dly_tap_in & dly_tap_in & dly_tap_in & dly_tap_in;

  tap_instances : for tap4 in 0 to (2**CGN_DELAY_SEL_BITS)/4-1 generate
    constant C_SLICES_PER_COLUMN : integer := 16;
--    constant C_Y_OFFSET          : integer := tap4/C_SLICES_PER_COLUMN;
--    constant C_X_OFFSET_EVEN     : integer := tap4 mod C_SLICES_PER_COLUMN;
--    constant C_X_OFFSET_ODD      : integer := abs( (tap4 mod C_SLICES_PER_COLUMN) - C_SLICES_PER_COLUMN + 1 ) ;
--    constant C_X_OFFSET          : integer := ((C_Y_OFFSET+1) mod 2)*C_X_OFFSET_EVEN + (C_Y_OFFSET mod 2)*C_X_OFFSET_ODD;
    constant C_X_OFFSET          : integer := tap4/C_SLICES_PER_COLUMN;
    constant C_Y_OFFSET_EVEN     : integer := tap4 mod C_SLICES_PER_COLUMN;
    constant C_Y_OFFSET_ODD      : integer := abs( (tap4 mod C_SLICES_PER_COLUMN) - C_SLICES_PER_COLUMN + 1 ) ;
    constant C_Y_OFFSET          : integer := ((C_X_OFFSET+1) mod 2)*C_Y_OFFSET_EVEN + (C_X_OFFSET mod 2)*C_Y_OFFSET_ODD;
    constant rloc_str : string := "X" & integer'image(C_X_OFFSET) & "Y" & integer'image(C_Y_OFFSET);
    attribute RLOC of dcdl_tap_inst: label is rloc_str;
    --attribute RLOC of dcdl_tap_inst : label is "X1Y0";
  begin
    dcdl_tap_inst : dcdl_4tap
    Generic Map
    (
      CGN_TAP_TYPE  => CGN_TAP_TYPE
    )
    Port Map
    (
      TAP_CASCADE_I => dly_chain(tap4),
      TAP_I         => dly_tap_in_vector,
      TAP_SELECT_I  => tap_sel((tap4+1)*4-1 downto tap4*4),
      TAP_CASCADE_O => dly_chain(tap4+1)
    );
  end generate tap_instances;

end behavioral;

