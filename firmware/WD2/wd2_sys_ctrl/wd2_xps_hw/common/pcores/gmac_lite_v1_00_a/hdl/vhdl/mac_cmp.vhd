--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : mac_cmp.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    compare mac address to MAC_I or broadcast.
--    MAC_ERROR_O stays set until beginning of new frame.
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity mac_cmp is
  port
  (
    CLK_I        : in  std_logic;
    RESET_I      : in  std_logic;
    DATA_I       : in  std_logic_vector(7 downto 0);
    DATA_VALID_I : in  std_logic;
    MAC_I        : in  std_logic_vector(47 downto 0);
    MAC_ERROR_O  : out std_logic
  );
end mac_cmp;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behaviour of mac_cmp is

  signal data_r : std_logic_vector( 7 downto 0);
  signal mac_rx : std_logic_vector(47 downto 0);

  type state_type is (S_IDLE, S_START, S_MAC_0, S_MAC_1, S_MAC_2, S_MAC_3, S_MAC_4, S_MAC_5, S_MAC_CMP);
  signal state : state_type;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

begin

  mac_proc: process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if (RESET_I = '1') then
        MAC_ERROR_O <= '0';
        state       <= S_IDLE;
      else

        -- register data
        data_r <= DATA_I;

        case state is
          when S_IDLE =>       -- wait for inter frame gap
                               if (DATA_VALID_I = '0') then
                                 state <= S_START;
                               end if;

          when S_START =>      -- wait for start of new frame
                               if (DATA_VALID_I = '1') then
                                 MAC_ERROR_O <= '0';
                                 state <= S_MAC_0;
                               end if;

          when S_MAC_0 =>      mac_rx(47 downto 40)   <= data_r;
                               state <= S_MAC_1;

          when S_MAC_1 =>      mac_rx(39 downto 32)  <= data_r;
                               state <= S_MAC_2;

          when S_MAC_2 =>      mac_rx(31 downto 24) <= data_r;
                               state <= S_MAC_3;

          when S_MAC_3 =>      mac_rx(23 downto 16) <= data_r;
                               state <= S_MAC_4;

          when S_MAC_4 =>      mac_rx(15 downto 8) <= data_r;
                               state <= S_MAC_5;

          when S_MAC_5 =>      mac_rx(7 downto 0) <= data_r;
                               state <= S_MAC_CMP;

          when S_MAC_CMP =>    if ((mac_rx /= MAC_I) and (mac_rx /= x"ffffffffffff")) then
                                 MAC_ERROR_O <= '1';
                               end if;
                               state <= S_IDLE;

          when others =>       state <= S_IDLE;
        end case;

      end if;
    end if;
  end process;

end architecture behaviour;
