--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll8_to_gmii.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    local link to gmii interface
--  Note:
--    frame may not be interrupted once it has started,
--    i.e. LL_SRC_RDY_N_I must no go hight until fram is finished !!!
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;

library unisim;
use unisim.VComponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll8_to_gmii is
  port
  (
      CLK_I               : in std_logic;
      RESET_I             : in std_logic;
      IFG_COUNT_I         : in std_logic_vector(7 downto 0);

      -- ll interface
      LL_DATA_I           : in std_logic_vector(7 downto 0);
      LL_SOF_N_I          : in std_logic;
      LL_EOF_N_I          : in std_logic;
      LL_SRC_RDY_N_I      : in std_logic;
      LL_DST_RDY_N_O      : out std_logic;

  -- xgmii to xaui interface

      GMII_TXD_O          : out std_logic_vector(7 downto 0);
      GMII_TX_EN_O        : out std_logic;
      GMII_TX_ER_O        : out std_logic
  );
end ll8_to_gmii;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of ll8_to_gmii is

  constant C_PADDING      : std_logic_vector(7 downto 0) := "00000000"; -- 0x00
  constant C_PREAMBLE     : std_logic_vector(7 downto 0) := "01010101"; -- 0x55
  constant C_SFD          : std_logic_vector(7 downto 0) := "11010101"; -- 0xD5 Start Frame Delimiter

  signal ll_data_r        : std_logic_vector(7 DOWNTO 0);

  signal ifg_count        : std_logic_vector (7 downto 0);
  signal byte_count       : std_logic_vector (5 downto 0) := (others =>'0');
  signal ll_dst_rdy_n     : std_logic;

  signal crc_data         : std_logic_vector (7 downto 0);
  signal crc_data_reg     : std_logic_vector (7 downto 0);

  signal crc_reset        : std_logic;

  signal crc_calc         : std_logic;
  signal crc_use_data_reg : std_logic;
  signal crc              : std_logic_vector(31 downto 0);

  type states is (S_IDLE, S_PREAMBLE, S_SFD, S_DATA, S_PADDING, S_CRC_0, S_CRC_1, S_CRC_2, S_CRC_3, S_IFG);
  signal state : states;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

begin


  crc32_d8_inst : entity psi_3205_v1_00_a.crc32_d8
  port map
  (
    CLK_I        => CLK_I,
    RESET_I      => crc_reset,
    DATA_I       => crc_data,
    DATA_VALID_I => crc_calc,
    CRC_O        => crc,
    CRC_VALID_O  => open
  );

  LL_DST_RDY_N_O <= ll_dst_rdy_n;

  crc_data    <= crc_data_reg when (crc_use_data_reg = '1') else LL_DATA_I ;

  fsm: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then

      if RESET_I = '1' then
        state            <= S_IDLE;
        GMII_TXD_O       <= C_PREAMBLE;
        GMII_TX_EN_O     <= '0';
        ll_dst_rdy_n     <= '1';
        crc_reset        <= '1';
        crc_calc         <= '0';
        crc_use_data_reg <= '0';
--        byte_count     <= (others =>'0');
      else

         -- counter for preamble and minmum frame size
        if byte_count > 0 then
          byte_count <= byte_count - 1;
        end if;

        case state is
          when S_IDLE =>       GMII_TXD_O       <= C_PREAMBLE;
                               crc_reset        <= '1';
                               crc_calc         <= '0';
                               crc_use_data_reg <= '0';
                               byte_count <= "000110";
                               if (LL_SOF_N_I = '0' and LL_SRC_RDY_N_I = '0') then
                                 crc_reset      <= '0';
                                 ifg_count      <= IFG_COUNT_I;
                                 ll_data_r      <= LL_DATA_I;
                                 ll_dst_rdy_n   <= '1';
                                 GMII_TX_EN_O   <= '1';
                                 state <= S_PREAMBLE;
                               else
                                 GMII_TX_EN_O   <= '0';
                                 ll_dst_rdy_n   <= '0';
                               end if;

          when S_PREAMBLE =>   GMII_TXD_O     <= C_PREAMBLE;
                               GMII_TX_EN_O   <= '1';
                               ll_dst_rdy_n   <= '1';
                               if byte_count = 0 then
                                 crc_data_reg     <= ll_data_r;
                                 crc_use_data_reg <= '1';
                                 crc_calc         <= '1';
                                 GMII_TXD_O       <= C_SFD;
                                 state <= S_SFD;
                               end if;

          when S_SFD =>        GMII_TXD_O     <= ll_data_r;
                               GMII_TX_EN_O   <= '1';
                               ll_dst_rdy_n   <= '0';
                               crc_use_data_reg <= '0';
                               byte_count     <= "111010"; -- minimum frame size: 64 - 2(counts) - 4(crc)
                               state <= S_DATA;

          when S_DATA =>       GMII_TXD_O     <= LL_DATA_I;
                               GMII_TX_EN_O   <= '1';
                               ll_dst_rdy_n   <= '0';
                               if (LL_EOF_N_I='0' and LL_SRC_RDY_N_I ='0') then
                                 ll_dst_rdy_n <= '1';
                                 if (byte_count = 0) then
                                   crc_calc <= '0';
                                   state <= S_CRC_0;
                                 else
                                   crc_data_reg      <= C_PADDING;
                                   crc_use_data_reg  <= '1';
                                   state <= S_PADDING;
                                 end if;
                               end if;
                               -- check error: if LL_SRC_RDY_N_I /= '0' or  LL_SOF_N_I = '0' set GMII_TX_ER_O

          when S_PADDING =>    GMII_TXD_O     <= C_PADDING;
                               GMII_TX_EN_O   <= '1';
                               if (byte_count = 0) then
                                 crc_use_data_reg <= '0';
                                 crc_calc         <= '0';
                                 state <= S_CRC_0;
                               end if;


          when S_CRC_0 =>      GMII_TXD_O     <= crc(7 downto 0);
                               GMII_TX_EN_O   <= '1';
                               state <= S_CRC_1;

          when S_CRC_1 =>      GMII_TXD_O     <= crc(15 downto 8);
                               GMII_TX_EN_O   <= '1';
                               state <= S_CRC_2;

          when S_CRC_2 =>      GMII_TXD_O     <= crc(23 downto 16);
                               GMII_TX_EN_O   <= '1';
                               state <= S_CRC_3;

          when S_CRC_3 =>      GMII_TXD_O     <= crc(31 downto 24);
                               GMII_TX_EN_O   <= '1';
                               crc_reset      <= '1';
                               state <= S_IFG;

          when S_IFG =>        ifg_count <= ifg_count - 1;
                               GMII_TX_EN_O   <= '0';
                               GMII_TXD_O     <= C_PREAMBLE;
                               crc_reset      <= '1';
                               if ifg_count = 0 then
                                 state         <= S_IDLE;
                                 ll_dst_rdy_n  <= '0';
                               end if;

          when others =>       state <= S_IDLE;
        end case;
      end if;
    end if;

  end process fsm;

  GMII_TX_ER_O <= '0';  -- not used

end architecture behave;

