--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : .vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    gmii to local link  interface
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;

library unisim;
use unisim.vcomponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity gmii_to_ll8 is
  port
  (
    -- gmii interface
    GMII_RX_CLK_I        : in  std_logic;
    GMII_RXD_I           : in  std_logic_vector(7 downto 0);
    GMII_RX_DV_I         : in  std_logic;
    GMII_RX_ER_I         : in  std_logic;

    -- ll interface
    LL8_DATA_O           : out std_logic_vector(7 downto 0);
    LL8_SOF_N_O          : out std_logic;
    LL8_EOF_N_O          : out std_logic;
    LL8_SRC_RDY_N_O      : out std_logic;
    LL8_DST_RDY_N_I      : in  std_logic;

    -- MAC filter
    MAC_FILTER_I         : in  std_logic;
    MAC_I                : in  std_logic_vector(47 downto 0);

    -- ctrl signals
    RESET_I              : in  std_logic
  );
end gmii_to_ll8;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of gmii_to_ll8 is

  component mac_cmp is
  port
  (
    CLK_I        : in  std_logic;
    RESET_I      : in  std_logic;
    DATA_I       : in  std_logic_vector(7 downto 0);
    DATA_VALID_I : in  std_logic;
    MAC_I        : in  std_logic_vector(47 downto 0);
    MAC_ERROR_O  : out std_logic
  );
  end component;

  ------------------------------------------------------------------------------

  constant C_PREAMBLE     : std_logic_vector(7 downto 0) := "01010101"; -- 0x55
  constant C_SFD          : std_logic_vector(7 downto 0) := "11010101"; -- 0xD5 Start Frame Delimiter

  signal gmii_rxd_r       : std_logic_vector(7 downto 0);
  signal gmii_rx_valid    : std_logic;

  signal ll8_data_out     : std_logic_vector(7 downto 0);
  signal ll8_sof_n        : std_logic;
  signal ll8_eof_n        : std_logic;
  signal ll8_src_rdy_n    : std_logic;


  signal ll8_us_dst_rdy_n : std_logic;
  signal ll8_us_pkt_ok    : std_logic;
  signal ll8_us_pkt_err   : std_logic;

  signal crc_reset        : std_logic;
  signal crc_calc         : std_logic;
  signal crc_valid        : std_logic;

  signal mac_error        : std_logic;
  signal mac_cmp_reset    : std_logic;

  type state_type is (S_IDLE, S_START, S_PREAMBLE, S_SFD, S_SOF, S_DATA, S_EOF, S_FRAME_OK, S_FRAME_ERR);
  signal state : state_type;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

begin

  ---------------------------------------------------
  -- Calcalute Frame Check Sequence (CRC32)
  ---------------------------------------------------

  crc32_d8_inst : entity psi_3205_v1_00_a.crc32_d8
  port map
  (
    CLK_I        => GMII_RX_CLK_I,
    RESET_I      => crc_reset,
    DATA_I       => gmii_rxd_r,
    DATA_VALID_I => crc_calc,
    CRC_O        => open,
    CRC_VALID_O  => crc_valid
  );


  ---------------------------------------------------
  -- compare MAC address
  ---------------------------------------------------

  mac_cmp_inst : mac_cmp
  port map
  (
    CLK_I        => GMII_RX_CLK_I,
    RESET_I      => mac_cmp_reset,
    DATA_I       => gmii_rxd_r,
    DATA_VALID_I => crc_calc,
    MAC_I        => MAC_I,
    MAC_ERROR_O  => mac_error
  );

  mac_cmp_reset <= RESET_I or not MAC_FILTER_I;

--  crc_calc <= ((not LL_SRC_RDY_N_I) and (not ll_dst_rdy_n)) or crc_padding;
--  crc_data <= LL_DATA_I when (crc_padding = '0') else C_PADDING;


  ---------------------------------------------------

  gmii_rx_valid <= '1' when ((GMII_RX_DV_I = '1') and (GMII_RX_ER_I = '0')) else '0';

  state_logic: process (GMII_RX_CLK_I) is
  begin
    if rising_edge(GMII_RX_CLK_I) then

      ll8_sof_n     <= '1';
      ll8_eof_n     <= '1';
      ll8_src_rdy_n <= '1';

      ll8_us_pkt_ok  <= '0';
      ll8_us_pkt_err <= '0';


--      gmii_rxd_r    <= gmii_rxd;
--      ll8_data_out  <= gmii_rxd_r;
--      ll8_data_out  <= (others => '0');

      if (RESET_I = '1') then
        crc_reset <= '1';
        crc_calc  <= '0';
        state     <= S_IDLE;
      else
        case state is
          when S_IDLE =>       -- wait for inter frame gap
                               crc_reset <= '1';
                               crc_calc  <= '0';
                               if (gmii_rx_valid = '0') then
                                 state <= S_START;
                               end if;

          when S_START =>      -- wait for start of new frame
                               if (gmii_rx_valid = '1') then
                                 if (GMII_RXD_I = C_PREAMBLE) then
                                   state <= S_PREAMBLE;
                                 else
                                   state <= S_IDLE;
                                 end if;
                               end if;

          when S_PREAMBLE =>   crc_reset <= '0';
                               if (gmii_rx_valid /= '1') then
                                 state <= S_IDLE;
                               elsif (GMII_RXD_I = C_SFD) then
                                 state <= S_SFD;
                               elsif (GMII_RXD_I /= C_PREAMBLE) then
                                 state <= S_IDLE;
                               end if;

          when S_SFD   =>
                               gmii_rxd_r   <= GMII_RXD_I;
                               ll8_data_out <= gmii_rxd_r;
                               crc_calc     <= '1';

                               if (gmii_rx_valid /= '1') then
                                 state <= S_IDLE;
                               else
                                 state <= S_SOF;
                               end if;


          when S_SOF   =>
                               gmii_rxd_r   <= GMII_RXD_I;
                               ll8_data_out <= gmii_rxd_r;

                               if (gmii_rx_valid /= '1') then
                                 state <= S_IDLE;
                               else
                                 ll8_sof_n     <= '0';
                                 ll8_src_rdy_n <= '0';
                                 state <= S_DATA;
                               end if;

          when S_DATA =>
                               gmii_rxd_r   <= GMII_RXD_I;
                               ll8_data_out <= gmii_rxd_r;

                               ll8_src_rdy_n <= '0';

                               if (GMII_RX_DV_I = '0') then
                                 ll8_eof_n <= '0';
                                 state <= S_EOF;
                               end if;

                               if (GMII_RX_ER_I = '1') then
                                 ll8_eof_n <= '0';
                                 state <= S_FRAME_ERR;
                               end if;

          when S_EOF =>        crc_calc  <= '0';
                               if (crc_valid = '0') then
                                 state <= S_FRAME_ERR;
                               elsif (mac_error = '1') then
                                 state <= S_FRAME_ERR;
                               else
                                 state <= S_FRAME_OK;
                               end if;

          when S_FRAME_OK =>   ll8_us_pkt_ok  <= '1';
                               state <= S_IDLE;

          when S_FRAME_ERR =>  ll8_us_pkt_err <= '1';
                               state <= S_IDLE;

          when others =>       state <= S_IDLE;
        end case;
      end if;
    end if;
  end process state_logic;

--
--  if (ll8_src_rdy_n = '0' and ll8_us_dst_rdy_n = '1' ) then
--
--  end if;
--

  ------------------------------------------------------------------------------

  ll8_packet_fifo_inst : entity psi_3205_v1_00_a.ll8_packet_fifo
  generic map
  (
    CGN_ADDR_WIDTH => 11
  )
  port map
  (
    CLK_I               => GMII_RX_CLK_I,
    RESET_I             => RESET_I,

    -- ll us interface
    LL8_I_DATA_I       => ll8_data_out,
    LL8_I_SOF_N_I      => ll8_sof_n,
    LL8_I_EOF_N_I      => ll8_eof_n,
    LL8_I_SRC_RDY_N_I  => ll8_src_rdy_n,
    LL8_I_DST_RDY_N_O  => ll8_us_dst_rdy_n,
    LL8_I_PKT_OK_I     => ll8_us_pkt_ok,
    LL8_I_PKT_ERR_I    => ll8_us_pkt_err,

    -- ll ds interface
    LL8_O_DATA_O       => LL8_DATA_O,
    LL8_O_SOF_N_O      => LL8_SOF_N_O,
    LL8_O_EOF_N_O      => LL8_EOF_N_O,
    LL8_O_SRC_RDY_N_O  => LL8_SRC_RDY_N_O,
    LL8_O_DST_RDY_N_I  => LL8_DST_RDY_N_I
  );


end architecture behave;
