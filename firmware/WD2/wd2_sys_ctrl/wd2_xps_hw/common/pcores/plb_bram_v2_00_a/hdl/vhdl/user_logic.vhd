--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : user_logic.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  block ram written from PLB and read from user port
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;

library unisim;
use unisim.vcomponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    CGN_RD_DATA_WIDTH             : integer := 32;
    CGN_RD_ADDR_WIDTH             : integer := 9;
    CGN_BYTE_SWAP_EN              : boolean := true;

    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_AWIDTH                   : integer              := 32;
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_MEM                      : integer              := 1
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here

    BRAM_RD_CLK_I                 : in  std_logic;
    BRAM_RD_EN_I                  : in  std_logic;
    BRAM_RD_ADDR_I                : in  std_logic_vector(CGN_RD_ADDR_WIDTH-1 downto 0);
    BRAM_RD_DATA_O                : out std_logic_vector(CGN_RD_DATA_WIDTH-1 downto 0);

    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS                      : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW                     : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  function log2 (val: INTEGER) return natural is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function Log2;

  constant C_WR_ADDR_WIDTH     : integer := log2((2**CGN_RD_ADDR_WIDTH) * CGN_RD_DATA_WIDTH  / 32);

  signal mem_address           : std_logic_vector(C_WR_ADDR_WIDTH-1 downto 0);

  signal mem_read_enable       : std_logic;
  signal mem_read_enable_dly1  : std_logic;
  signal mem_read_req          : std_logic;
 -- signal mem_ip2bus_data       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_read_ack_dly1     : std_logic;
  signal mem_read_ack_dly2     : std_logic;
  signal mem_read_ack          : std_logic;
  signal mem_write_ack         : std_logic;

--  signal bram_byte_we          : std_logic_vector(3 downto 0);

  signal data_b_zero           : std_logic_vector(CGN_RD_DATA_WIDTH-1 downto 0);

  signal mem_read_data         : std_logic_vector(31 downto 0);
  signal mem_write_data        : std_logic_vector(31 downto 0);
  
  signal bram_di               : std_logic_vector(31 downto 0);
  signal bram_do               : std_logic_vector(31 downto 0);
  signal bram_wraddr           : std_logic_vector(C_WR_ADDR_WIDTH-1 downto 0);
  signal bram_wren             : std_logic;
  signal bram_en               : std_logic;

begin

  --USER logic implementation added here

  ------------------------------------------
  -- Example code to access user logic memory region
  --
  -- Note:
  -- The example code presented here is to show you one way of using
  -- the user logic memory space features. The Bus2IP_Addr, Bus2IP_CS,
  -- and Bus2IP_RNW IPIC signals are dedicated to these user logic
  -- memory spaces. Each user logic memory space has its own address
  -- range and is allocated one bit on the Bus2IP_CS signal to indicated
  -- selection of that memory space. Typically these user logic memory
  -- spaces are used to implement memory controller type cores, but it
  -- can also be used in cores that need to access additional address space
  -- (non C_BASEADDR based), s.t. bridges. This code snippet infers
  -- 1 256x32-bit (byte accessible) single-port Block RAM by XST.
  ------------------------------------------

  mem_read_enable <= ( Bus2IP_CS(0) ) and Bus2IP_RNW;
  mem_read_ack    <= mem_read_ack_dly2;
  mem_write_ack   <= ( Bus2IP_CS(0) ) and not(Bus2IP_RNW);
  mem_address     <= Bus2IP_Addr(C_SLV_AWIDTH-C_WR_ADDR_WIDTH-2 to C_SLV_AWIDTH-3);

  -- implement single clock wide read request
  mem_read_req    <= mem_read_enable and not(mem_read_enable_dly1);
  BRAM_RD_REQ_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_enable_dly1 <= '0';
      else
        mem_read_enable_dly1 <= mem_read_enable;
      end if;
    end if;

  end process BRAM_RD_REQ_PROC;

  -- this process generates the read acknowledge 1 clock after read enable
  -- is presented to the BRAM block. The BRAM block has a 1 clock delay
  -- from read enable to data out.
  BRAM_RD_ACK_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_ack_dly1 <= '0';
        mem_read_ack_dly2 <= '0';
      else
        mem_read_ack_dly1 <= mem_read_req;
        mem_read_ack_dly2 <= mem_read_ack_dly1;
      end if;
    end if;

  end process BRAM_RD_ACK_PROC;


  ram_dp_generic_inst :entity psi_3205_v1_00_a.ram_dp
  generic map
  (
    CGN_DATA_WIDTH_A  => 32,
    CGN_ADDR_WIDTH_A  => C_WR_ADDR_WIDTH,
    CGN_READ_PORT_A   => true,
    CGN_WRITE_PORT_A  => true,
    CGN_READ_FIRST_A  => false,
    CGN_USE_OUTREG_A  => true,

    CGN_DATA_WIDTH_B  => CGN_RD_DATA_WIDTH,
    CGN_ADDR_WIDTH_B  => CGN_RD_ADDR_WIDTH,
    CGN_READ_PORT_B   => true,
    CGN_WRITE_PORT_B  => false,
    CGN_READ_FIRST_B  => true,
    CGN_USE_OUTREG_B  => false,

    CGN_RAM_STYLE     => "block"  -- {auto|block|distributed|pipe_distributed|block_power1|block_power2}
  )
  port map
  (
    PA_CLK_I       => Bus2IP_Clk,
    PA_ADDR_I      => bram_wraddr,
    PA_EN_I        => bram_en,
    PA_DATA_O      => bram_do,
    PA_WR_EN_I     => bram_wren,
    PA_DATA_I      => bram_di,

    PB_CLK_I       => BRAM_RD_CLK_I,
    PB_ADDR_I      => BRAM_RD_ADDR_I,
    PB_EN_I        => BRAM_RD_EN_I,
    PB_DATA_O      => BRAM_RD_DATA_O,
    PB_WR_EN_I     => '0',
    PB_DATA_I      => data_b_zero

  );

  data_b_zero  <= (others => '0');

  mem_write_data   <= Bus2IP_Data;

  gen_byte_swap: if (CGN_BYTE_SWAP_EN) generate
  begin 
    bram_di       <= psi_3205_v1_00_a.functions.byte_swap(mem_write_data);
    mem_read_data <= psi_3205_v1_00_a.functions.byte_swap(bram_do);
  end generate;

  gen_no_byte_swap: if (not CGN_BYTE_SWAP_EN) generate
  begin 
    bram_di        <= Bus2IP_Data;
    mem_read_data  <= bram_do;  
  end generate;

  
  bram_wraddr           <= mem_address(C_WR_ADDR_WIDTH-1 downto 0);
  bram_wren             <= not(Bus2IP_RNW) and Bus2IP_CS(0);
  bram_en               <= bram_wren or mem_read_req;

  -- only 32 bit write supported
--  bram_byte_we(0)    <= Bus2IP_BE(0);
--  bram_byte_we(1)    <= Bus2IP_BE(1);
--  bram_byte_we(2)    <= Bus2IP_BE(2);
--  bram_byte_we(3)    <= Bus2IP_BE(3);

  -- no read back
--  mem_ip2bus_data <= (others => '0');

-- implement Block RAM read mux
--   MEM_IP2BUS_DATA_PROC : process( bram_do, Bus2IP_CS ) is
--   begin
-- 
--     case Bus2IP_CS is
--       when "1" => mem_ip2bus_data <= bram_do;
--       when others => mem_ip2bus_data <= (others => '0');
--     end case;
-- 
--   end process MEM_IP2BUS_DATA_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= mem_read_data when mem_read_ack = '1' else (others => '0');
  IP2Bus_WrAck <= mem_write_ack;
  IP2Bus_RdAck <= mem_read_ack;
  IP2Bus_Error <= '0';

end IMP;
