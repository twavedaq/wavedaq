---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Toggle FlipFlop
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  17.10.2017 15:53:00
--
--  Description :  Toggle FlipFlop.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_t_ff is
  port (
    T_FF_O : out std_logic;
    CLK_I  : in  std_logic
  );
end util_t_ff;

architecture behavioral of util_t_ff is

  signal ff : std_logic := '0';

begin

  T_FF_O <= ff;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      ff <= not(ff);
    end if;
  end process;

end architecture behavioral;