------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Tue Apr 14 12:36:13 2015 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------
library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;
--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    CGN_IP_HEADER_SEL_WIDTH : integer :=  6;
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 5
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    SERDES_DIV_CLK_I               : in  std_logic;
    PACKAGER_BUSY_I                : in  std_logic;
    TRANSPARENT_TRIGGER_O          : out std_logic;
    DATA_VALID_I                   : in  std_logic;
    DRS_CTRL_BUSY_I                : in  std_logic;
    HEADER_SEL_O                   : out std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  -- states of state machine
  type type_tf_ctrl_state is (sleep, trigger_sample, sample, wait_next);
  signal ctrl_state          : type_tf_ctrl_state := sleep;
  attribute fsm_encoding : string;
  attribute fsm_encoding of ctrl_state : signal is "one-hot";

  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0            : std_logic_vector(0 to C_SLV_DWIDTH-1); -- Trigger register
  signal slv_reg1            : std_logic_vector(0 to C_SLV_DWIDTH-1); -- Nr of samples register
  signal slv_reg2            : std_logic_vector(0 to C_SLV_DWIDTH-1); -- Nr of frames register
  signal slv_reg3            : std_logic_vector(0 to C_SLV_DWIDTH-1); -- Current frame register
  signal slv_reg4            : std_logic_vector(0 to C_SLV_DWIDTH-1); -- Header select register
  signal slv_reg_write_sel   : std_logic_vector(0 to 4);
  signal slv_reg_read_sel    : std_logic_vector(0 to 4);
  signal slv_ip2bus_data     : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack        : std_logic;
  signal slv_write_ack       : std_logic;

  signal frame_counter       : std_logic_vector(31 downto 0) := (others=>'0');
  --signal total_frame_counter : std_logic_vector(15 downto 0) := (others=>'0');
  --signal sample_counter      : std_logic_vector(31 downto 0) := (others=>'0');
  signal trigger_aquisition  : std_logic := '0';
  signal trigger_reset       : std_logic := '0';
  signal clr_reset           : std_logic := '0';
  signal reset               : std_logic := '0';
  signal sample_counter_init : std_logic_vector(31 downto 0) := (others=>'0');
  signal frame_counter_limit : std_logic_vector(31 downto 0) := (others=>'0');
  signal drs_ctrl_busy_s        : std_logic := '0';

begin

  --USER logic implementation added here

  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  -- 
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  -- 
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                    "10000"   C_BASEADDR + 0x00
  --                    "01000"   C_BASEADDR + 0x04
  --                    "00100"   C_BASEADDR + 0x08
  --                    "00010"   C_BASEADDR + 0x0C
  --                    "00001"   C_BASEADDR + 0x10
  -- 
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 4);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 4);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3) or Bus2IP_RdCE(4);

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
        slv_reg1 <= X"000007FF"; -- default for nr of samples 1024
        slv_reg2 <= X"00000001"; -- default nr of frames 1
        --slv_reg3 <= (others => '0'); -- read only
        slv_reg4 <= (others => '0');
      else
        -- default
        slv_reg0(0 to 29) <= (others => '0');
        if trigger_reset = '1' then
          slv_reg0(31) <= '0';
        end if;
        if clr_reset = '1' then
          slv_reg0(30) <= '0';
        end if;
        ----------
        case slv_reg_write_sel is
          when "10000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "01000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg1(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00100" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg2(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00010" => null;
--            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
--              if ( Bus2IP_BE(byte_index) = '1' ) then
--                slv_reg3(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
--              end if;
--            end loop;
          when "00001" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg2(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1, slv_reg2, slv_reg3 ) is
  begin

    case slv_reg_read_sel is
      when "10000" => slv_ip2bus_data <= slv_reg0;
      when "01000" => slv_ip2bus_data <= slv_reg1;
      when "00100" => slv_ip2bus_data <= slv_reg2;
      when "00010" => slv_ip2bus_data <= slv_reg3;
      when "00001" => slv_ip2bus_data <= slv_reg4;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  serdes_to_plb_clk : cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 34,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => SERDES_DIV_CLK_I,
    CLK_B_I => Bus2IP_Clk,
    PORT_A_I(31 downto 0) => frame_counter,
    PORT_A_I(32) => trigger_aquisition,
    PORT_A_I(33) => reset,
    PORT_B_O(31 downto 0) => slv_reg3,
    PORT_B_O(32) => trigger_reset,
    PORT_B_O(33) => clr_reset
  );

  plb_to_serdes_clk : cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 66,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => Bus2IP_Clk,
    CLK_B_I => SERDES_DIV_CLK_I,
    PORT_A_I(31 downto  0) => slv_reg1,
    PORT_A_I(63 downto 32) => slv_reg2,
    PORT_A_I(64) => slv_reg0(31),
    PORT_A_I(65) => slv_reg0(30),
    PORT_B_O(31 downto  0) => sample_counter_init,
    PORT_B_O(63 downto 32) => frame_counter_limit,
    PORT_B_O(64) => trigger_aquisition,
    PORT_B_O(65) => reset
  );

  data_valid_ff : process(SERDES_DIV_CLK_I)
  begin
    if rising_edge(SERDES_DIV_CLK_I) then
      drs_ctrl_busy_s <= DRS_CTRL_BUSY_I;
    end if;
  end process data_valid_ff;

  test_frame_ctrl_fsm : process(SERDES_DIV_CLK_I)
  begin
    if rising_edge(SERDES_DIV_CLK_I) then
      TRANSPARENT_TRIGGER_O <= '0';

      if reset = '1' then
        ctrl_state <= sleep;
      else
        case ctrl_state is
          when sleep =>
            frame_counter <= (others=>'0');
            if DRS_CTRL_BUSY_I = '0' and PACKAGER_BUSY_I = '0' and trigger_aquisition = '1' then
              ctrl_state <= trigger_sample;
            end if;
            
          when trigger_sample =>
            TRANSPARENT_TRIGGER_O <= '1';
            if DRS_CTRL_BUSY_I = '1' then
              frame_counter <= frame_counter + 1;
              ctrl_state <= sample;
            end if;

          when sample =>
            if DRS_CTRL_BUSY_I = '0' and drs_ctrl_busy_s = '1' then -- falling edge of busy
              if frame_counter = frame_counter_limit then
                ctrl_state <= sleep;
              else
                ctrl_state <= wait_next;
              end if;
            end if;

          when wait_next =>
            if DRS_CTRL_BUSY_I = '0' and PACKAGER_BUSY_I = '0' then
              ctrl_state <= trigger_sample;
            end if;

          when others =>
            null;
        end case;
      end if;
    end if;
  end process test_frame_ctrl_fsm;

  HEADER_SEL_O     <= slv_reg4(0 to CGN_IP_HEADER_SEL_WIDTH-1);

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

end IMP;
