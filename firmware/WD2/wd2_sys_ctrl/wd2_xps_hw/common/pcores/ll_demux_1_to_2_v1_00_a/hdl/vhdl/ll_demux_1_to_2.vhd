--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_demux_1_to_2.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  24.03.2016 12:30:39
--
--  Description :  
--    local link demultiplexer
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_demux_1_to_2 is
  generic
  (
    CGN_LL_DATA_WIDTH     : integer := 32;
    CGN_LL_REM_WIDTH      : integer := 2;
    CGN_SIDE_DATA_WIDTH   : integer := 1
  );
  port
  (
    -- Ports
    SELECT_I            : in  std_logic;
    CLK_I               : in  std_logic;
    RESET_I             : in  std_logic;

    -- Local Link D
    LL_D_I_DATA_I       : in  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_D_I_SOF_N_I      : in  std_logic;
    LL_D_I_EOF_N_I      : in  std_logic;
    LL_D_I_REM_I        : in  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0) := (others => '0');
    LL_D_I_SRC_RDY_N_I  : in  std_logic;
    LL_D_I_DST_RDY_N_O  : out std_logic;
    LL_D_I_SIDE_DATA_I  : in  std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0) := (others => '0');

    -- Local Link QA
    LL_QA_O_Data_O       : out std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_QA_O_SOF_N_O      : out std_logic;
    LL_QA_O_EOF_N_O      : out std_logic;
    LL_QA_O_REM_O        : out std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_QA_O_SRC_RDY_N_O  : out std_logic;
    LL_QA_O_DST_RDY_N_I  : in  std_logic;
    LL_QA_O_SIDE_DATA_O  : out std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0);

    -- Local Link QB
    LL_QB_O_Data_O       : out std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_QB_O_SOF_N_O      : out std_logic;
    LL_QB_O_EOF_N_O      : out std_logic;
    LL_QB_O_REM_O        : out std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_QB_O_SRC_RDY_N_O  : out std_logic;
    LL_QB_O_DST_RDY_N_I  : in  std_logic;
    LL_QB_O_SIDE_DATA_O  : out std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0)
  );
end ll_demux_1_to_2;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of ll_demux_1_to_2 is

  signal  transmitting   :   std_logic := '0';
  signal  dst_select     :   std_logic := '0';

begin

  -- De-Mux
  LL_D_I_DST_RDY_N_O  <= LL_QA_O_DST_RDY_N_I when dst_select = '0' else LL_QB_O_DST_RDY_N_I;

  LL_QA_O_Data_O      <= LL_D_I_DATA_I;
  LL_QA_O_SOF_N_O     <= LL_D_I_SOF_N_I or dst_select;
  LL_QA_O_EOF_N_O     <= LL_D_I_EOF_N_I or dst_select;
  LL_QA_O_REM_O       <= LL_D_I_REM_I;
  LL_QA_O_SRC_RDY_N_O <= LL_D_I_SRC_RDY_N_I;
  LL_QA_O_SIDE_DATA_O <= LL_D_I_SIDE_DATA_I;

  LL_QB_O_Data_O      <= LL_D_I_DATA_I;
  LL_QB_O_SOF_N_O     <= LL_D_I_SOF_N_I or not dst_select;
  LL_QB_O_EOF_N_O     <= LL_D_I_EOF_N_I or not dst_select;
  LL_QB_O_REM_O       <= LL_D_I_REM_I;
  LL_QB_O_SRC_RDY_N_O <= LL_D_I_SRC_RDY_N_I;
  LL_QB_O_SIDE_DATA_O <= LL_D_I_SIDE_DATA_I;

  -- Do not change destination selection while transmitting
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RESET_I = '1' then
        transmitting <= '0';
      else
        if LL_D_I_SOF_N_I = '0' then
          transmitting <= '1';
        elsif LL_D_I_EOF_N_I = '0' then
          transmitting <= '0';
        end if;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if transmitting = '0' and LL_D_I_SOF_N_I = '1' then
        dst_select <= SELECT_I;
      end if;
    end if;
  end process;

-------------------------------------------------------------------------------
END behavior;

