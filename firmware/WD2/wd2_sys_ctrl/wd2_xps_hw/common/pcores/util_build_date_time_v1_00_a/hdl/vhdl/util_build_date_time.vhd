---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Build Date and Time Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  03.11.2016 12:14:34
--
--  Description :  Provide build date and time to firmware.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity util_build_date_time is
  generic (
    CGN_DATE_YEAR     : integer := 0;
    CGN_DATE_MONTH    : integer := 1;
    CGN_DATE_DAY      : integer := 1;
    CGN_TIME_HOUR     : integer := 0;
    CGN_TIME_MINUTE   : integer := 0;
    CGN_TIME_SECOND   : integer := 0
  );
  port (
    BUILD_YEAR_O   : out std_logic_vector(15 downto 0);
    BUILD_MONTH_O  : out std_logic_vector( 7 downto 0);
    BUILD_DAY_O    : out std_logic_vector( 7 downto 0);
    BUILD_HOUR_O   : out std_logic_vector( 7 downto 0);
    BUILD_MINUTE_O : out std_logic_vector( 7 downto 0);
    BUILD_SECOND_O : out std_logic_vector( 7 downto 0)
  );
end util_build_date_time;

architecture behavioral of util_build_date_time is

begin

  BUILD_YEAR_O   <= conv_std_logic_vector(CGN_DATE_YEAR,   16);
  BUILD_MONTH_O  <= conv_std_logic_vector(CGN_DATE_MONTH,   8);
  BUILD_DAY_O    <= conv_std_logic_vector(CGN_DATE_DAY,     8);
  BUILD_HOUR_O   <= conv_std_logic_vector(CGN_TIME_HOUR,    8);
  BUILD_MINUTE_O <= conv_std_logic_vector(CGN_TIME_MINUTE,  8);
  BUILD_SECOND_O <= conv_std_logic_vector(CGN_TIME_SECOND,  8);

end architecture behavioral;