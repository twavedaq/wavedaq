library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_MISC.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity adc_frame is
generic
(
  CGN_ON_CHIP_TERM       : boolean := TRUE;
  CGN_NUM_SERIAL_BITS    : integer := 12
);
Port
( 
  -- ADC Bitclock divided by 3
  ADC_HALFWORD_DIV_CLK_I : IN  std_logic;
  ADC_BIT_CLK_I          : IN std_logic;
  ADC_BIT_CLK_N_I        : IN std_logic;
  ADC_SERDES_STROBE_I    : IN std_logic;

  -- ADCLK (Frame Signal)  same alignment as data
  ADC_ADCLK_P_I          : IN  std_logic;
  ADC_ADCLK_N_I          : IN  std_logic;

  ADC_DIV_CLK_EXT_I      : IN  std_logic;

  RESET_I                : IN  std_logic;
  EN_I                   : IN  std_logic;
  BITSLIP_EN_I           : IN  std_logic;

  FRAME_O                : OUT std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  MSB_REG_CE_O           : OUT std_logic;
  BITSLIP_O              : OUT std_logic;
  ALIGNED_O              : OUT std_logic
);
end adc_frame;


architecture Behavioral of adc_frame is

  constant ALL_ONES          : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0) := (others => '1');
  constant ALL_ZEROS         : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0) := (others => '0');
  constant TARGET_FRAME_WORD : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0)   := ALL_ONES & ALL_ZEROS;

  signal aligned              : std_logic;
  signal aligned_s            : std_logic;

  signal adc_adclk           : std_logic := '0';

  signal iserdes_q           : std_logic_vector(7 downto 0);

  signal bitslip_en          : std_logic := '0';

  signal msbs_aligned        : std_logic := '0';

  signal serdes_data_out     : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal halfword_s          : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal frame_msbs          : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal frame_lsbs          : std_logic_vector(CGN_NUM_SERIAL_BITS/2-1 downto 0);
  signal frame_vec           : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  signal frame               : std_logic_vector(CGN_NUM_SERIAL_BITS-1 downto 0);
  signal bitslip_delay       : std_logic_vector(3 downto 0) := (others => '0');
  signal msb_reg_ce          : std_logic := '0';
  
  signal slave_shiftout      : std_logic := '0';
  signal icascade            : std_logic := '0';

  attribute RLOC : string;

  attribute RLOC of aligned_s: signal is "X0Y0";
  attribute RLOC of ALIGNED_O: signal is "X1Y0";
  
begin

---------------------------------------------------------------------------------------------------
-- adclock clock input differential buffer
---------------------------------------------------------------------------------------------------

  adc_lclk_ibufds : IBUFDS
  generic map 
  (
    DIFF_TERM   => CGN_ON_CHIP_TERM,
    IOSTANDARD  => "LVDS_25"
  )
  port map
  (
    I  => ADC_ADCLK_P_I,
    IB => ADC_ADCLK_N_I,
    O  => adc_adclk 
  );

  -- Instantiate the serdes primitive
  ----------------------------------
  -- declare the iserdes
  iserdes2_master : ISERDES2
   generic map (
     BITSLIP_ENABLE => TRUE,
     DATA_RATE      => "SDR",
     DATA_WIDTH     => CGN_NUM_SERIAL_BITS/2,
     INTERFACE_TYPE => "RETIMED", --"NETWORKING_PIPELINED", --"NETWORKING",
     SERDES_MODE    => "MASTER")
   port map (
     Q1         => iserdes_q(3), -- 4. bit in time
     Q2         => iserdes_q(2), -- 5. bit in time
     Q3         => iserdes_q(1), -- 6. bit in time
     Q4         => iserdes_q(0), -- 7. bit in time (MSB @ AD9637)
     SHIFTOUT   => icascade,
     INCDEC     => open,
     VALID      => open,
     BITSLIP    => bitslip_en,       -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.
                                  -- The amount of bitslip is fixed by the DATA_WIDTH selection.
     CE0        => EN_I,   -- 1-bit Clock enable input
     CLK0       => ADC_BIT_CLK_I, -- 1-bit IO Clock network input. Optionally Invertible. This is the primary clock
                                   -- input used when the clock doubler circuit is not engaged (see DATA_RATE
                                   -- attribute).
     CLK1       => '0',--ADC_BIT_CLK_N_I, -- 1-bit Optionally invertible IO Clock network input. Timing note: CLK1 should be
                                   -- 180 degrees out of phase with CLK0.
     CLKDIV     => ADC_HALFWORD_DIV_CLK_I,                        -- 1-bit Global clock network input. This is the clock for the fabric domain.
     D          => adc_adclk, -- 1-bit Input signal from IOB.
     IOCE       => ADC_SERDES_STROBE_I,                       -- 1-bit Data strobe signal derived from BUFIO CE. Strobes data capture for
                                                      -- NETWORKING and NETWORKING_PIPELINES alignment modes.
  
     RST        => RESET_I,        -- 1-bit Asynchronous reset only.
     SHIFTIN    => slave_shiftout,
  
  
    -- unused connections
     FABRICOUT  => open,
     CFB0       => open,
     CFB1       => open,
     DFB        => open);
  
  iserdes2_slave : ISERDES2
   generic map (
     BITSLIP_ENABLE => TRUE,
     DATA_RATE      => "SDR",
     DATA_WIDTH     => CGN_NUM_SERIAL_BITS/2,
     INTERFACE_TYPE => "RETIMED", --"NETWORKING_PIPELINED", --"NETWORKING",
     SERDES_MODE    => "SLAVE")
   port map (
    Q1         => iserdes_q(7), -- ignored
    Q2         => iserdes_q(6), -- 1. bit in time (MSB @ AD9637)
    Q3         => iserdes_q(5), -- 2. bit in time
    Q4         => iserdes_q(4), -- 3. bit in time
    SHIFTOUT   => slave_shiftout,
    BITSLIP    => bitslip_en,      -- 1-bit Invoke Bitslip. This can be used with any DATA_WIDTH, cascaded or not.
                                -- The amount of bitslip is fixed by the DATA_WIDTH selection.
    CE0        => EN_I,   -- 1-bit Clock enable input
    CLK0       => ADC_BIT_CLK_I, -- 1-bit IO Clock network input. Optionally Invertible. This is the primary clock
                                  -- input used when the clock doubler circuit is not engaged (see DATA_RATE
                                  -- attribute).
    CLK1       => '0',--ADC_BIT_CLK_N_I, -- 1-bit Optionally invertible IO Clock network input. Timing note: CLK1 should be
                                  -- 180 degrees out of phase with CLK0.
    CLKDIV     => ADC_HALFWORD_DIV_CLK_I,                        -- 1-bit Global clock network input. This is the clock for the fabric domain.
    D          => '0',            -- 1-bit Input signal from IOB.
    IOCE       => ADC_SERDES_STROBE_I,   -- 1-bit Data strobe signal derived from BUFIO CE. Strobes data capture for
                                  -- NETWORKING and NETWORKING_PIPELINES alignment modes.
  
    RST        => RESET_I,       -- 1-bit Asynchronous reset only.
    SHIFTIN    => icascade,
    -- unused connections
    FABRICOUT  => open,
    CFB0       => open,
    CFB1       => open,
    DFB        => open);

  bitslip_proc: process (ADC_HALFWORD_DIV_CLK_I)
  begin
    if rising_edge(ADC_HALFWORD_DIV_CLK_I) then
      
      if (aligned = '0') and (bitslip_delay = 0) and (BITSLIP_EN_I = '1') then
        bitslip_en    <= '1';
        bitslip_delay <= (others => '1');
      else
        bitslip_en    <= '0';
        if (bitslip_delay /= 0) then
          bitslip_delay <= bitslip_delay - 1;
        end if;
      end if;

     end if;
  end process;

  serdes_data_out <= iserdes_q(CGN_NUM_SERIAL_BITS/2-1 downto 0);

  -- msb/lsb store
  process(ADC_HALFWORD_DIV_CLK_I)
  begin
    if rising_edge(ADC_HALFWORD_DIV_CLK_I) then
      halfword_s <= serdes_data_out;
      msb_reg_ce <= msbs_aligned;
      if msb_reg_ce = '1' then
        frame_msbs <= serdes_data_out;
        aligned_s  <= aligned;
      else
        frame_lsbs <= serdes_data_out;
      end if;
    end if;
  end process;

  process(ADC_DIV_CLK_EXT_I)
  begin
    if rising_edge(ADC_DIV_CLK_EXT_I) then
      ALIGNED_O    <= aligned_s;
    end if;
  end process;

  clock_domain_crossing : for i in CGN_NUM_SERIAL_BITS-1 downto 0 generate
    constant C_Y_OFFSET          : integer := i/4;
    constant rloc_1st_str : string := "X0" & "Y" & integer'image(C_Y_OFFSET);
    constant rloc_2nd_str : string := "X1" & "Y" & integer'image(C_Y_OFFSET);
    signal frame_int   : std_logic;
    signal frame_o_int : std_logic;
    attribute RLOC of frame_int:   signal is rloc_1st_str;
    attribute RLOC of frame_o_int: signal is rloc_2nd_str;
  begin
  
    process(ADC_HALFWORD_DIV_CLK_I)
    begin
      if rising_edge(ADC_HALFWORD_DIV_CLK_I) then
        if msb_reg_ce = '1' then
          frame_int <= frame_vec(i);
        end if;
      end if;
    end process;

    process(ADC_DIV_CLK_EXT_I)
    begin
      if rising_edge(ADC_DIV_CLK_EXT_I) then
        frame_o_int <= frame_int;
      end if;
    end process;
    
    FRAME_O(i) <= frame_o_int;
    
  end generate clock_domain_crossing;

  msbs_aligned <= AND_REDUCE(halfword_s);
  frame_vec    <= frame_msbs & frame_lsbs;
  aligned      <= '1' when frame_vec = TARGET_FRAME_WORD else '0';

  MSB_REG_CE_O <= msb_reg_ce;
  BITSLIP_O    <= bitslip_en;

end Behavioral;


