
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity adc_clock is
generic
(
  CGN_NUM_SERIAL_BITS    : integer := 12;
  CGN_ON_CHIP_TERM       : boolean := TRUE
);
port 
( 
  -- ADC Fast bit clock (ADCLK x 7)
  ADC_LCLK_P_I           : IN std_logic;
  ADC_LCLK_N_I           : IN std_logic;

  RESET_I                : IN  std_logic;
  PLL_LOCKED_O           : OUT std_logic;

  -- phase aligned clock
  ADC_BIT_CLK_O          : OUT std_logic;
  ADC_BIT_CLK_N_O        : OUT std_logic;
  ADC_HALFWORD_DIV_CLK_O : OUT std_logic;
  ADC_SERDES_STROBE_O    : OUT std_logic
);
end adc_clock;



architecture Behavioral of adc_clock is

  signal adc_lclk              : std_logic;
  signal adc_lclk_bufio        : std_logic;
  signal adc_bit_clk           : std_logic;
  signal pll_bit_clk           : std_logic;
  signal adc_halfword_div_clk  : std_logic;
  signal adc_halfword_div_gclk : std_logic;
  signal adc_serdes_strobe     : std_logic;
  signal pll_locked            : std_logic;
  signal clkfb                 : std_logic;

begin

  ----------------------------------------------------------------------------
  -- vvv PLL Implementation vvv
  ----------------------------------------------------------------------------

   adc_lclk_ibufgds: IBUFGDS
   generic map (
     DIFF_TERM    => CGN_ON_CHIP_TERM,
     IBUF_LOW_PWR => TRUE,
     IOSTANDARD   => "LVDS_25")
   port map (
     I          => ADC_LCLK_P_I,
     IB         => ADC_LCLK_N_I,
     O          => adc_lclk);

  -- also generated the inverted clock
  bufio2_inst : BUFIO2
    generic map (
      DIVIDE_BYPASS => TRUE,
      I_INVERT      => FALSE,
      USE_DOUBLER   => FALSE,
      DIVIDE        => 1)
    port map (
      DIVCLK        => adc_lclk_bufio,
      IOCLK         => open,
      SERDESSTROBE  => open,
      I             => adc_lclk);

  pll_base_inst : PLL_BASE
  generic map
   (BANDWIDTH            => "HIGH",
    CLK_FEEDBACK         => "CLKOUT0",
    COMPENSATION         => "SOURCE_SYNCHRONOUS",
    DIVCLK_DIVIDE        => 1,
    CLKFBOUT_MULT        => 2,
    CLKFBOUT_PHASE       => 0.000,
    CLKOUT0_DIVIDE       => 1,
    CLKOUT0_PHASE        => 0.000,
    CLKOUT0_DUTY_CYCLE   => 0.500,
    CLKOUT1_DIVIDE       => 1,
    CLKOUT1_PHASE        => 180.000,
    CLKOUT1_DUTY_CYCLE   => 0.500,
    CLKOUT2_DIVIDE       => CGN_NUM_SERIAL_BITS,
    CLKOUT2_PHASE        => 0.000,
    CLKOUT2_DUTY_CYCLE   => 0.500,
    CLKOUT3_DIVIDE       => CGN_NUM_SERIAL_BITS/2,
    CLKOUT3_PHASE        => 0.000,
    CLKOUT3_DUTY_CYCLE   => 0.500,
    CLKIN_PERIOD         => 2.083,
    REF_JITTER           => 0.010)
  port map
    -- Output clocks
   (CLKFBOUT            => open, --clkfbout,
    CLKOUT0             => pll_bit_clk,
    CLKOUT1             => open,
    CLKOUT2             => open,
    CLKOUT3             => adc_halfword_div_clk,
    CLKOUT4             => open,
    CLKOUT5             => open,
    -- Status and control signals
    LOCKED              => pll_locked,
    RST                 => RESET_I,
    -- Input clock control
    CLKFBIN             => clkfb,
    CLKIN               => adc_lclk_bufio);

  BUFPLL_inst : BUFPLL
  generic map (
    DIVIDE => CGN_NUM_SERIAL_BITS/2,         -- DIVCLK divider (1-8)
    ENABLE_SYNC => TRUE  -- Enable synchrnonization between PLL and GCLK (TRUE/FALSE)
  )
  port map (
    IOCLK        => adc_bit_clk,       -- 1-bit output: Output I/O clock
    LOCK         => PLL_LOCKED_O,      -- 1-bit output: Synchronized LOCK output
    SERDESSTROBE => adc_serdes_strobe, -- 1-bit output: Output SERDES strobe (connect to ISERDES2/OSERDES2)
    GCLK         => adc_halfword_div_gclk,   -- 1-bit input: BUFG clock input
    LOCKED       => pll_locked,        -- 1-bit input: LOCKED input from PLL
    PLLIN        => pll_bit_clk        -- 1-bit input: Clock input from PLL
  );

  BUFIO2FB_inst : BUFIO2FB
  generic map (
    DIVIDE_BYPASS => TRUE  -- Bypass divider (TRUE/FALSE)
  )
  port map (
    O => clkfb, -- 1-bit output: Output feedback clock (connect to feedback input of DCM/PLL)
    I => adc_bit_clk -- clkfbout  -- 1-bit input: Feedback clock input (connect to input port)
  );

  -- Buffer up the divided clock
  clkdiv_hw_buf_inst : BUFG
    port map (
      O => adc_halfword_div_gclk,
      I => adc_halfword_div_clk);


  ----------------------------------------------------------------------------
  -- ^^^ PLL Implementation ^^^
  ----------------------------------------------------------------------------

--------------------------------------------------------------------------------
  ADC_BIT_CLK_O          <= adc_bit_clk;
  ADC_BIT_CLK_N_O        <= '0';
  ADC_HALFWORD_DIV_CLK_O <= adc_halfword_div_gclk;
  ADC_SERDES_STROBE_O    <= adc_serdes_strobe;

---------------------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------------------

end Behavioral;


