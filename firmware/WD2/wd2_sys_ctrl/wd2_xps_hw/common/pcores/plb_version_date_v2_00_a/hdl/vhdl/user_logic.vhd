------------------------------------------------------------------------------
--                       Paul Scherrer Institute (PSI)
------------------------------------------------------------------------------
-- Unit    : user_logic.vhd
-- Author  : Goran Marinkovic, Section Diagnostic, modified tg32
-- Version : $Revision: 1.1 $
------------------------------------------------------------------------------
-- Copyrightę PSI, Section Diagnostic
------------------------------------------------------------------------------
-- Comment : This is the user logic for the E-XFEL firmware version.
------------------------------------------------------------------------------
-- Std. library (platform) ---------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    CGN_VERSION       : std_logic_vector := X"00000000";
    CGN_COMPAT_LEVEL  : integer := 0;
    CGN_DATE_YEAR     : integer := 0;
    CGN_DATE_MONTH    : integer := 0;
    CGN_DATE_DAY      : integer := 0;
    CGN_TIME_HOUR     : integer := 0;
    CGN_TIME_MINUTE   : integer := 0;
    CGN_TIME_SECOND   : integer := 0;
    C_SLV_DWIDTH      : integer := 32;
    C_NUM_REG         : integer := 3
  );
  port
  (
    ------------------------------------------------------------------------
    -- Bus protocol ports
    ------------------------------------------------------------------------
    Bus2IP_Clk        : in  std_logic;
    Bus2IP_Reset      : in  std_logic;
    Bus2IP_Data       : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE         : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE       : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE       : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data       : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck      : out std_logic;
    IP2Bus_WrAck      : out std_logic;
    IP2Bus_Error      : out std_logic
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture behavioral of user_logic is

  ---------------------------------------------------------------------------
  -- Types
  ---------------------------------------------------------------------------
  type     slv_reg_type is array (0 to C_NUM_REG-1) of std_logic_vector( 0 to C_SLV_DWIDTH - 1);
  ---------------------------------------------------------------------------
  -- Signals
  ---------------------------------------------------------------------------
  -- Register access
  constant REG_NONE              : std_logic_vector( 0 to C_NUM_REG - 1) := (others => '0');
  signal   slv_reg_rd            : slv_reg_type;
  signal   slv_reg_rd_ack        : std_logic := '0';
  signal   slv_reg_wr_ack        : std_logic := '0';
  signal   slv_ip2bus_data       : std_logic_vector( 0 to C_SLV_DWIDTH-1);

begin

  ---------------------------------------------------------------------------
  -- Status
  ---------------------------------------------------------------------------
  IP2Bus_RdAck   <= slv_reg_rd_ack;
  IP2Bus_WrAck   <= slv_reg_wr_ack;
  IP2Bus_Error   <= '0';

  ---------------------------------------------------------------------------
  -- IP to Bus data
  ---------------------------------------------------------------------------
  IP2Bus_Data    <= slv_ip2bus_data when (slv_reg_rd_ack = '1') else (others => '0');

  ---------------------------------------------------------------------------
  -- Register read
  ---------------------------------------------------------------------------
  slv_reg_rd_proc: process(Bus2IP_RdCE, slv_reg_rd) is
  begin
    slv_ip2bus_data      <= (others => '0');
    for register_index in 0 to C_NUM_REG - 1 loop
      if (Bus2IP_RdCE(register_index) = '1') then
        slv_ip2bus_data  <= slv_reg_rd(register_index);
      end if;
    end loop;
  end process slv_reg_rd_proc;

  slv_reg_rd_ack         <= '1' when (Bus2IP_RdCE /= REG_NONE) else '0';

  ---------------------------------------------------------------------------
  -- Register write
  ---------------------------------------------------------------------------
  -- no write registers, just ack

  slv_reg_wr_ack         <= '1' when (Bus2IP_WrCE /= REG_NONE) else '0';

  ---------------------------------------------------------------------------
  -- Version of the firmware assigned by user.
  ---------------------------------------------------------------------------
  slv_reg_rd( 0) <= CGN_VERSION;

  ---------------------------------------------------------------------------
  -- Compilation date and time. This values are set during elaboration by a
  -- tcl script run by EDK. Hence it updates every time the code is compiled.
  ---------------------------------------------------------------------------
  slv_reg_rd( 1) <= conv_std_logic_vector(CGN_DATE_YEAR,   16) &
                    conv_std_logic_vector(CGN_DATE_MONTH,   8) &
                    conv_std_logic_vector(CGN_DATE_DAY,     8);

  slv_reg_rd( 2) <= conv_std_logic_vector(CGN_COMPAT_LEVEL, 8) &
                    conv_std_logic_vector(CGN_TIME_HOUR,    8) &
                    conv_std_logic_vector(CGN_TIME_MINUTE,  8) &
                    conv_std_logic_vector(CGN_TIME_SECOND,  8);

end behavioral;
