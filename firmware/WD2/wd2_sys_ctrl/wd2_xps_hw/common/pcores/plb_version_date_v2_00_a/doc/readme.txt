
PLB Slave for firmware version and build date + time
----------------------------------------------------

User Input:
 CGN_VERSION       : 32 bit Version ID
 CGN_COMPAT_LEVEL  :  8 bit Compatibility Level
 
The date and time generics (CGN_DATE_YEAR, CGN_DATE_MONTH, CGN_DATE_DAY, CGN_TIME_HOUR, CGN_TIME_MINUTE, CGN_TIME_SECOND)
are set by a TCL script at elaboration phase of the build process to the current computer time.

Register usage:
===============

                           MSB                                                              LSB
Reg 0 : (Baseaddr+0x00) : | 32 Bit Version ID                                                  |
Reg 1 : (Baseaddr+0x04) : | 16 Bit Year                        |  8 Bit Month  |  8 Bit Day    |
Reg 2 : (Baseaddr+0x08) : |  8 Bit COMPAT_LEVEL | 8 Bit   Hour |  8 Bit Minute |  8 Bit Second |

