---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator RAM Arbiter
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.03.2015 11:19:31
--
--  Description : RAM arbiter  for the Wavedream 2Package.
--                Makes sure there is no access collision on the dual port RAM.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity buffer_flags is
generic
(
  CGN_NR_OF_BUFFERS : integer := 4
);
port
(
  -- Write Interface
  WRITE_DONE_I : in  std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
  FULL_O       : out std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
  -- Read Interface
  READ_DONE_I  : in  std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
  EMPTY_O      : out std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
  -- Reset
  RST_I        : in  std_logic;
  -- Clocks
  WR_CLK_I     : in  std_logic;
  RD_CLK_I     : in  std_logic
);
end buffer_flags;

architecture Behavioral of buffer_flags is

  signal full       : std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0) := (others=>'0');
  signal write_done : std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0) := (others=>'0');

begin

  -- Cross Clock Domain Signals
  written_sync : cdc_sync
    GENERIC MAP (
      -- Total_Latency = CGN_USE_INPUT_REG_A + CGN_USE_OUTPUT_REG_A + CGN_NUM_SYNC_REGS_B + CGN_NUM_OUTPUT_REGS_B
      -- Use timing ignore constraint in UCF:
      -- NET "*sig_cdc_ignore_timing*" TIG;
      CGN_DATA_WIDTH          => CGN_NR_OF_BUFFERS,
      CGN_USE_INPUT_REG_A     => 0,
      CGN_USE_OUTPUT_REG_A    => 0,
      CGN_USE_GRAY_CONVERSION => 0,
      CGN_NUM_SYNC_REGS_B     => 2,
      CGN_NUM_OUTPUT_REGS_B   => 0,
      CGN_GRAY_TO_BIN_STYLE   => 0 --  0: serial structure, 1: Kogge-Stone parallel prefix, 2: Sklansky parallel-prefix propagate-lookahead structure
    )
    PORT MAP (
      CLK_A_I => WR_CLK_I,
      CLK_B_I => RD_CLK_I,
      PORT_A_I => WRITE_DONE_I,
      PORT_B_O => write_done
    );

  full_sync : cdc_sync
    GENERIC MAP (
      -- Total_Latency = CGN_USE_INPUT_REG_A + CGN_USE_OUTPUT_REG_A + CGN_NUM_SYNC_REGS_B + CGN_NUM_OUTPUT_REGS_B
      -- Use timing ignore constraint in UCF:
      -- NET "*sig_cdc_ignore_timing*" TIG;
      CGN_DATA_WIDTH          => CGN_NR_OF_BUFFERS,
      CGN_USE_INPUT_REG_A     => 0,
      CGN_USE_OUTPUT_REG_A    => 0,
      CGN_USE_GRAY_CONVERSION => 0,
      CGN_NUM_SYNC_REGS_B     => 2,
      CGN_NUM_OUTPUT_REGS_B   => 0,
      CGN_GRAY_TO_BIN_STYLE   => 0 --  0: serial structure, 1: Kogge-Stone parallel prefix, 2: Sklansky parallel-prefix propagate-lookahead structure
    )
    PORT MAP (
      CLK_A_I => RD_CLK_I,
      CLK_B_I => WR_CLK_I,
      PORT_A_I => full,
      PORT_B_O => FULL_O
    );

  buffer_arbiter_array : for i in 0 to CGN_NR_OF_BUFFERS-1 generate
    flag_gen : process(RD_CLK_I)
    begin
      if rising_edge(RD_CLK_I) then
        if READ_DONE_I(i) = '1' or RST_I = '1' then
          full(i) <= '0';
        elsif write_done(i) = '1' then
          full(i) <= '1';
        end if;
      end if;
    end process flag_gen;
  end generate buffer_arbiter_array;

  EMPTY_O <= not full;

end Behavioral;
