---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Buffer Write Controller
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  08.04.2015 11:21:20
--
--  Description : Controls the write process to the data buffers of the
--                WaveDream2 (WD2) package generator.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity wd2_pkg_gen_wr_ctrl is
generic
(
  CGN_BUFFERS               : integer := 4; -- To be adapted to depth of one RAM
  CGN_MAX_SAMPLES_PER_EVENT : integer := 1024
);  
port
(
  BUSY_O           : out std_logic;

  WR_SAMPLE_NR_O   : out std_logic_vector(log2ceil(CGN_MAX_SAMPLES_PER_EVENT)-1 downto 0);
  WR_BUFER_NR_O    : out std_logic_vector(log2ceil(CGN_BUFFERS)-1 downto 0);
  BUF_WE_O         : out std_logic;
  BUF_WRITE_DONE_O : out std_logic_vector(CGN_BUFFERS-1 downto 0);
  BUF_FULL_I       : in  std_logic_vector(CGN_BUFFERS-1 downto 0);
  NO_FIFO_EMPTY_I  : in  std_logic;
  FIFO_RE_O        : out std_logic;

  EVENT_NR_I       : in  std_logic_vector(31 downto 0);
  EVENT_NR_O       : out std_logic_vector(31 downto 0);
  EVENT_LUT_WE_O   : out std_logic;
  ADC_0_CH_TX_EN_I : in  std_logic_vector(8 downto 0);
  ADC_1_CH_TX_EN_I : in  std_logic_vector(8 downto 0);
  READING_CH9_O    : out std_logic;

  -- General Signals
  ENABLE_I : IN std_logic;
  CLK_I    : IN std_logic;
  RST_I    : IN std_logic
);
end wd2_pkg_gen_wr_ctrl;

architecture Behavioral of wd2_pkg_gen_wr_ctrl is

  -- states of state machine
  type type_wr_state is (idle, write, skip_event);
  signal wr_state          : type_wr_state := idle;
  attribute fsm_encoding : string;
  attribute fsm_encoding of wr_state : signal is "one-hot";
  
  signal wr_buffer_nr      : std_logic_vector(log2ceil(CGN_BUFFERS)-1 downto 0) := (others=>'0');
  signal wr_sample_nr      : std_logic_vector(log2ceil(CGN_MAX_SAMPLES_PER_EVENT)-1 downto 0) := (others=>'0');

  signal adc_0_ch_tx_en    : std_logic_vector(8 downto 0) := (others=>'0');
  signal adc_1_ch_tx_en    : std_logic_vector(8 downto 0) := (others=>'0');
  signal reading_ch9       : std_logic := '0';

begin

  WR_BUFER_NR_O  <= wr_buffer_nr;
  WR_SAMPLE_NR_O <= wr_sample_nr;
  EVENT_NR_O     <= EVENT_NR_I when wr_state /= idle else (others=>'0');
  READING_CH9_O  <= reading_ch9;
  BUF_WE_O       <= NO_FIFO_EMPTY_I when wr_state = write else '0';
  FIFO_RE_O      <= NO_FIFO_EMPTY_I when wr_state /= idle else '0';

  fsm_write_buffer : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      -- fsm
      if RST_I = '1' then
        BUSY_O         <= '1';
        reading_ch9    <= '0';
        adc_0_ch_tx_en <= (others=>'0');
        adc_1_ch_tx_en <= (others=>'0');
        wr_buffer_nr   <= (others=>'0');
        wr_sample_nr   <= (others=>'0');
        wr_state       <= idle;
      else
        -- defaults -------------------
        BUSY_O           <= '1';
        EVENT_LUT_WE_O   <= '0';
        BUF_WRITE_DONE_O <= (others=>'0');
        -------------------------------
        case wr_state is  

          when idle =>
            reading_ch9  <= '0';
            wr_sample_nr <= (others=>'0');
            BUSY_O       <= BUF_FULL_I(CONV_INTEGER(wr_buffer_nr));
            if NO_FIFO_EMPTY_I = '1' then
              if BUF_FULL_I(CONV_INTEGER(wr_buffer_nr)) = '0' and ENABLE_I = '1' then
                wr_state <= write;
              else
                wr_state <= skip_event;
              end if;
            end if;

          when write =>
            if NO_FIFO_EMPTY_I = '1' then
              wr_sample_nr <= wr_sample_nr + 1;
              if wr_sample_nr = CONV_STD_LOGIC_VECTOR(512, wr_sample_nr'length) then
                -- delayed to allow for stop cell and trigger information
                -- to be received and written to LUT
                EVENT_LUT_WE_O <= '1';
                adc_0_ch_tx_en <= ADC_0_CH_TX_EN_I;
                adc_1_ch_tx_en <= ADC_1_CH_TX_EN_I;
              elsif wr_sample_nr = CONV_STD_LOGIC_VECTOR(CGN_MAX_SAMPLES_PER_EVENT-1, wr_sample_nr'length) then
                wr_sample_nr <= (others=>'0');
                if reading_ch9 = '1' or (adc_0_ch_tx_en(8) = '0' and adc_1_ch_tx_en(8) = '0') then
                -- if we just stored channel 9 or it will not be stored
                  reading_ch9 <= '0';
                  BUF_WRITE_DONE_O(CONV_INTEGER(wr_buffer_nr)) <= '1';
                  wr_buffer_nr <= wr_buffer_nr + 1;
                  wr_state <= idle;
                else
                  reading_ch9 <= '1';
                end if;
              end if;
            end if;

          when skip_event =>
            if NO_FIFO_EMPTY_I = '1' then
              wr_sample_nr <= wr_sample_nr + 1;
              if wr_sample_nr = CONV_STD_LOGIC_VECTOR(CGN_MAX_SAMPLES_PER_EVENT-1, wr_sample_nr'length) then
                wr_sample_nr <= (others=>'0');
                if reading_ch9 = '1' or (adc_0_ch_tx_en(8) = '0' and adc_1_ch_tx_en(8) = '0') then
                  reading_ch9 <= '0';
                  wr_state <= idle;
                else
                  reading_ch9 <= '1';
                end if;
              end if;
            end if;

          when others =>
            wr_state <= idle;
        end case;
      end if;
    end if;
  end process fsm_write_buffer;

end Behavioral;
