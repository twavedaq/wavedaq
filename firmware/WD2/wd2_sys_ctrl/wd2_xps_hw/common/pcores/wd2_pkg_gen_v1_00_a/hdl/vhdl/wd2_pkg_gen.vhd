---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.03.2015 09:14:57
--
--  Description : Package generator for the WaveDream2 (WD2) board. Generates the 
--                packages to be passed to the ll8_udp_gen for transmission over
--                ethernet or to the serdes interface. The unit generates the WD2
--                specific header and stores a certain set of drs data events to
--                allow retransmission.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;
use psi_3205_v1_00_a.fifo_gen_async;
use psi_3205_v1_00_a.fifo_gen_sync;
use psi_3205_v1_00_a.ram_sdp;

entity wd2_pkg_gen is
generic
(
  CGN_NR_OF_ADCS          : integer :=  2;
  CGN_CHNLS_PER_ADC       : integer :=  9;
  CGN_LANES_PER_ADC       : integer :=  8;
  CGN_ADC_CH_DWIDTH       : integer := 12;
  CGN_LL_DATA_WIDTH       : integer :=  8;
  CGN_LL_REM_WIDTH        : integer :=  2;
  CGN_FRAME_LENGTH_WIDTH  : integer := 14;
  CGN_IP_HEADER_SEL_WIDTH : integer :=  6
);  
port
(
  -- Local Link output interface
  LL_CLK_I             : in   std_logic;
  LL_TX_DATA_O         : out  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
  LL_TX_SOF_N_O        : out  std_logic;
  LL_TX_EOF_N_O        : out  std_logic;
  LL_TX_REM_O          : out  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
  LL_TX_SRC_RDY_N_O    : out  std_logic;
  LL_TX_DST_RDY_N_I    : in   std_logic;
  FRAME_LENGTH_O       : out  std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
  IP_HEADER_SEL_O      : out  std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);

  -- DRS Data Input
  SERDES_DATA_I        : in   std_logic_vector(CGN_NR_OF_ADCS*CGN_LANES_PER_ADC*CGN_ADC_CH_DWIDTH-1 downto 0);
  SERDES_WE_I          : in   std_logic_vector(CGN_NR_OF_ADCS-1 downto 0);
  DRS_EVENT_NR_I       : in   std_logic_vector(31 downto 0);
  FIFO_FULL_O          : out  std_logic_vector(CGN_NR_OF_ADCS-1 downto 0);
  BUSY_O               : out  std_logic;

  -- Side Data
  IP_HEADER_SEL_I      : in   std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
  ADC_0_CH_TX_EN_I     : in   std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
  ADC_1_CH_TX_EN_I     : in   std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
  READOUT_SOURCE_I     : in   std_logic;
  PROTOCOL_VERSION_I   : in   std_logic_vector(7 downto 0);
  BOARD_REVISION_I     : in   std_logic_vector(7 downto 0);
  BOARD_ID_I           : in   std_logic_vector(15 downto 0);
  CRATE_ID_I           : in   std_logic_vector(7 downto 0);
  SLOT_ID_I            : in   std_logic_vector(7 downto 0);
  SAMPLING_FREQ_I      : in   std_logic_vector(15 downto 0);
  PAYLOAD_LENGTH_I     : in   std_logic_vector(15 downto 0);
  TRIGGER_NR_I         : in   std_logic_vector(15 downto 0);
  DRS0_TRIGGER_CELL_I  : in   std_logic_vector(9 downto 0);
  DRS1_TRIGGER_CELL_I  : in   std_logic_vector(9 downto 0);
  TRIGGER_TYPE_I       : in   std_logic_vector(15 downto 0);
  TEMPERATURE_I        : in   std_logic_vector(15 downto 0);
  INTER_PKG_DELAY_I    : in   std_logic_vector(23 downto 0);

  -- General Signals
  ENABLE_I      : in  std_logic;
  ADC_DIV_CLK_I : in  std_logic_vector(CGN_NR_OF_ADCS-1 downto 0);
  RST_I         : in  std_logic
);
end wd2_pkg_gen;

architecture struct of wd2_pkg_gen is

  COMPONENT wd2_pkg_gen_wr_ctrl
  generic
  (
    CGN_BUFFERS               : integer := 4; -- To be adapted to depth of one RAM
    CGN_MAX_SAMPLES_PER_EVENT : integer := 1024
  );  
  port
  (
    BUSY_O           : out std_logic;
    WR_SAMPLE_NR_O   : out std_logic_vector(log2ceil(CGN_MAX_SAMPLES_PER_EVENT)-1 downto 0);
    WR_BUFER_NR_O    : out std_logic_vector(log2ceil(CGN_BUFFERS)-1 downto 0);
    BUF_WE_O         : out std_logic;
    BUF_WRITE_DONE_O : out std_logic_vector(CGN_BUFFERS-1 downto 0);
    BUF_FULL_I       : in  std_logic_vector(CGN_BUFFERS-1 downto 0);
    NO_FIFO_EMPTY_I  : in  std_logic;
    FIFO_RE_O        : out std_logic;
    EVENT_NR_I       : in  std_logic_vector(31 downto 0);
    EVENT_NR_O       : out std_logic_vector(31 downto 0);
    EVENT_LUT_WE_O   : out std_logic;
    ADC_0_CH_TX_EN_I : in  std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
    ADC_1_CH_TX_EN_I : in  std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
    READING_CH9_O    : out std_logic;
    ENABLE_I         : IN std_logic;
    CLK_I            : IN std_logic;
    RST_I            : IN std_logic
  );
  end COMPONENT;

  COMPONENT wd2_pkg_gen_rd_ctrl
  GENERIC
  (
    CGN_BUFFERS               : integer :=    4; -- To be adapted to depth of one RAM
    CGN_MAX_SAMPLES_PER_EVENT : integer := 1024;
    CGN_SEGMENTS              : integer :=    2;
    CGN_NR_OF_ADCS            : integer :=    2;
    CGN_CHNLS_PER_ADC         : integer :=    8;
    CGN_ADC_CH_DWIDTH         : integer :=   12
  );  
  PORT
  (
    BUF_RD_ADDR_O    : out std_logic_vector(log2ceil(CGN_BUFFERS*CGN_MAX_SAMPLES_PER_EVENT)-1 downto 0);
    BUF_READ_DONE_O  : out std_logic_vector(CGN_BUFFERS-1 downto 0);
    BUF_EMPTY_I      : in  std_logic_vector(CGN_BUFFERS-1 downto 0);
    RD_BUFFER_NR_O   : out std_logic_vector(log2ceil(CGN_BUFFERS)-1 downto 0);
    RD_CHIP_NR_O     : out std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0);
    RD_CHANNEL_NR_O  : out std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0);
    RD_SEGMENT_NR_O  : out std_logic_vector(log2ceil(CGN_SEGMENTS)-1 downto 0);
    SHUFFLER_EOP_N_O : out std_logic;
    SHUFFLER_EN_O    : out std_logic;
    SHUFFLER_WEN_O   : out std_logic;
    SHUFFLER_FULL_I  : in  std_logic;
    SHUFFLER_AFULL_I : in  std_logic;
    EVENT_NR_I       : in  std_logic_vector(31 downto 0);
    ADC_0_CH_TX_EN_I : in  std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
    ADC_1_CH_TX_EN_I : in  std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
    PKG_NR_O         : out std_logic_vector(15 downto 0);
    ENABLE_I         : IN std_logic;
    CLK_I            : IN std_logic;
    RST_I            : IN std_logic
  );
  END COMPONENT;

  COMPONENT wd2_pkg_gen_ll_ctrl
  generic
  (
    CGN_SEGMENTS            : integer :=  2;
    CGN_NR_OF_ADCS          : integer :=  2;
    CGN_CHNLS_PER_ADC       : integer :=  8;
    CGN_LL_DATA_WIDTH       : integer :=  8;
    CGN_LL_REM_WIDTH        : integer :=  2;
    CGN_FRAME_LENGTH_WIDTH  : integer := 14;
    CGN_IP_HEADER_SEL_WIDTH : integer :=  6
  );  
  port
  (
    LL_CLK_I             : in  std_logic;
    LL_TX_DATA_O         : out std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_TX_SOF_N_O        : out std_logic;
    LL_TX_EOF_N_O        : out std_logic;
    LL_TX_REM_O          : out std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_TX_SRC_RDY_N_O    : out std_logic;
    LL_TX_DST_RDY_N_I    : in  std_logic;
    FRAME_LENGTH_O       : out std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
    IP_HEADER_SEL_I      : in  std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
    IP_HEADER_SEL_O      : out std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
    SHUFFLER_DATA_I      : in  std_logic_vector(7 downto 0);
    SHUFFLER_RE_O        : out std_logic;
    SHUFFLER_EMPTY_I     : in  std_logic;
    SHUFFLER_EOP_N_I     : in  std_logic;
    PROTOCOL_VERSION_I   : in  std_logic_vector(7 downto 0);
    BOARD_REVISION_I     : in  std_logic_vector(7 downto 0);
    BOARD_ID_I           : in  std_logic_vector(15 downto 0);
    CRATE_ID_I           : in  std_logic_vector(7 downto 0);
    SLOT_ID_I            : in  std_logic_vector(7 downto 0);
    SAMPLING_FREQ_I      : in  std_logic_vector(15 downto 0);
    PAYLOAD_LENGTH_I     : in  std_logic_vector(15 downto 0);
    RD_CHIP_NR_I         : in  std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0);
    RD_CHANNEL_NR_I      : in  std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0);
    RD_SEGMENT_NR_I      : in  std_logic_vector(log2ceil(CGN_SEGMENTS)-1 downto 0);
    PKG_TYPE_I           : in  std_logic_vector(3 downto 0);
    EVENT_NR_I           : in  std_logic_vector(31 downto 0);
    TRIGGER_NR_I         : in  std_logic_vector(15 downto 0);
    DRS0_TRIGGER_CELL_I  : in  std_logic_vector(9 downto 0);
    DRS1_TRIGGER_CELL_I  : in  std_logic_vector(9 downto 0);
    TRIGGER_TYPE_I       : in  std_logic_vector(15 downto 0);
    INTER_PKG_DELAY_I    : in  std_logic_vector(23 downto 0);
    TEMPERATURE_I        : in  std_logic_vector(15 downto 0);
    UDP_PKG_NR_I         : in  std_logic_vector(15 downto 0);
    ENABLE_I             : IN  std_logic;
    RST_I                : IN  std_logic
  );
  end COMPONENT;

  COMPONENT buffer_flags is
  GENERIC
  (
    CGN_NR_OF_BUFFERS : integer := 4
  );
  PORT
  (
    WRITE_DONE_I : in  std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
    FULL_O       : out std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
    READ_DONE_I  : in  std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
    EMPTY_O      : out std_logic_vector(CGN_NR_OF_BUFFERS-1 downto 0);
    RST_I        : in  std_logic;
    WR_CLK_I     : in  std_logic;
    RD_CLK_I     : in  std_logic
  );
  END COMPONENT;

  COMPONENT wd2_pkg_gen_2x12to3x8
    PORT (
      DATA_I   : in  std_logic_vector(11 downto 0);
      WR_EN_I  : in  std_logic;
      FULL_O   : out std_logic;
      SOP_N_I  : in  std_logic;
      EOP_N_I  : in  std_logic;
      DATA_O   : out std_logic_vector( 7 downto 0);
      RD_EN_I  : in  std_logic;
      EMPTY_O  : out std_logic;
      SOP_N_O  : out std_logic;
      EOP_N_O  : out std_logic;
      ENABLE_I : in  std_logic;
      CLK_I    : in  std_logic;
      RST_I    : in  std_logic
    );
  END COMPONENT;

  -- UDP Package LUT RAM address:
  -- Entry:   | Buffer | Chip | ADC-Ch. | Ch. Seq. Nr |
  -- Index:   | 6 .. 5 |  4   | 3 ... 1 |      0      |

  -- Frame Number LUT RAM address:
  -- Entry:   | Buffer |
  -- Index:   | 1 .. 0 |

  -- Buffer RAM address:
  -- Entry:   | Buffer | Ch. Seq. Nr | Sample Nr |
  -- Index:   | 11..10 |      9      | 8  ...  0 |

  constant C_BUFFERS                : integer := 4; -- To be adapted to depth of one RAM
  constant C_SEGMENTS               : integer := 2;
  constant C_MAX_SAMPLES_PER_EVENT  : integer := 1024;
  constant C_LUT_ENTRIES_PER_BUFFER : integer := C_SEGMENTS*CGN_NR_OF_ADCS*CGN_CHNLS_PER_ADC;
  constant C_LUT_ADDRS              : integer := C_BUFFERS*C_LUT_ENTRIES_PER_BUFFER;
  constant C_BUF_ADDRS              : integer := C_BUFFERS*C_MAX_SAMPLES_PER_EVENT;
  constant C_DWIDTH_PER_ADC         : integer := CGN_LANES_PER_ADC*CGN_ADC_CH_DWIDTH;

  type event_nr_reg          is array (C_BUFFERS-1 downto 0) of std_logic_vector(31 downto 0);
  type adc_ch_en_reg         is array (C_BUFFERS-1 downto 0) of std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0);
  type sample_freq_reg       is array (C_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
  type payload_length_reg    is array (C_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
  type trigger_nr_reg        is array (C_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
  type drs0_trigger_cell_reg is array (C_BUFFERS-1 downto 0) of std_logic_vector( 9 downto 0);
  type drs1_trigger_cell_reg is array (C_BUFFERS-1 downto 0) of std_logic_vector( 9 downto 0);
  type trigger_type_reg      is array (C_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
  type header_nr_reg         is array (C_BUFFERS-1 downto 0) of std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
  type temperature_reg       is array (C_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
  signal event_nr_lut            : event_nr_reg          := (others=>(others=>'0'));
  signal event_nr_lut_q          : event_nr_reg          := (others=>(others=>'0'));
  signal adc_0_ch_en_lut         : adc_ch_en_reg         := (others=>(others=>'0'));
  signal adc_0_ch_en_lut_q       : adc_ch_en_reg         := (others=>(others=>'0'));
  signal adc_1_ch_en_lut         : adc_ch_en_reg         := (others=>(others=>'0'));
  signal adc_1_ch_en_lut_q       : adc_ch_en_reg         := (others=>(others=>'0'));
  signal sample_freq_lut         : sample_freq_reg       := (others=>(others=>'0'));
  signal sample_freq_lut_q       : sample_freq_reg       := (others=>(others=>'0'));
  signal payload_length_lut      : payload_length_reg    := (others=>(others=>'0'));
  signal payload_length_lut_q    : payload_length_reg    := (others=>(others=>'0'));
  signal trigger_nr_lut          : trigger_nr_reg        := (others=>(others=>'0'));
  signal trigger_nr_lut_q        : trigger_nr_reg        := (others=>(others=>'0'));
  signal drs0_trigger_cell_lut   : drs0_trigger_cell_reg := (others=>(others=>'0'));
  signal drs0_trigger_cell_lut_q : drs0_trigger_cell_reg := (others=>(others=>'0'));
  signal drs1_trigger_cell_lut   : drs1_trigger_cell_reg := (others=>(others=>'0'));
  signal drs1_trigger_cell_lut_q : drs1_trigger_cell_reg := (others=>(others=>'0'));
  signal trigger_type_lut        : trigger_type_reg      := (others=>(others=>'0'));
  signal trigger_type_lut_q      : trigger_type_reg      := (others=>(others=>'0'));
  signal header_nr_lut           : header_nr_reg         := (others=>(others=>'0'));
  signal header_nr_lut_q         : header_nr_reg         := (others=>(others=>'0'));
  signal temperature_lut         : temperature_reg       := (others=>(others=>'0'));
  signal temperature_lut_q       : temperature_reg       := (others=>(others=>'0'));

  signal adc_0_ch_tx_en      : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal adc_1_ch_tx_en      : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal reading_ch9         : std_logic := '0';

  signal fifo_empty          : std_logic_vector(CGN_NR_OF_ADCS-1 downto 0) := (others=>'0');
  signal fifo_full           : std_logic_vector(CGN_NR_OF_ADCS-1 downto 0) := (others=>'0');
  signal fifo_wea            : std_logic_vector(CGN_NR_OF_ADCS-1 downto 0) := (others=>'0');
  signal no_fifo_empty       : std_logic := '0';
  signal fifo_rd_en          : std_logic := '0';
  signal wr_we               : std_logic := '0';
  
  type fifo_out_vector  is array (CGN_NR_OF_ADCS-1 downto 0) of std_logic_vector(C_DWIDTH_PER_ADC-1 downto 0);
  signal fifo_data_out       : fifo_out_vector := (others=>(others=>'0'));
  type buf_in_vector  is array (CGN_NR_OF_ADCS-1 downto 0, CGN_CHNLS_PER_ADC-1 downto 0) of std_logic_vector(15 downto 0);
  signal buf_dina            : buf_in_vector  := (others=>(others=>(others=>'0')));
  type buf_out_vector is array (CGN_NR_OF_ADCS-1 downto 0, CGN_CHNLS_PER_ADC-1 downto 0) of std_logic_vector(15 downto 0);
  signal buf_doutb           : buf_out_vector := (others=>(others=>(others=>'0')));
  signal buf_addra           : std_logic_vector(log2ceil(C_BUF_ADDRS)-1 downto 0) := (others=>'0');
  signal buf_wea             : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0)     := (others=>'0');
  signal buf_addrb           : std_logic_vector(log2ceil(C_BUF_ADDRS)-1 downto 0) := (others=>'0');

  signal rd_segment_nr       : std_logic_vector(log2ceil(C_SEGMENTS)-1 downto 0) := (others=>'0');
  signal rd_chip_nr          : std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0) := (others=>'0');
  signal rd_channel_nr       : std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0) := (others=>'0');
  signal rd_chip_nr_s        : std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0) := (others=>'0');
  signal rd_channel_nr_s     : std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0) := (others=>'0');
  signal rd_buffer_nr        : std_logic_vector(log2ceil(C_BUFFERS)-1 downto 0) := (others=>'0');
  signal wr_buffer_nr        : std_logic_vector(log2ceil(C_BUFFERS)-1 downto 0) := (others=>'0');
  signal wr_sample_nr        : std_logic_vector(log2ceil(C_MAX_SAMPLES_PER_EVENT)-1 downto 0) := (others=>'0');

  signal wr_busy             : std_logic := '0';
  signal event_info_we       : std_logic := '0';
  signal event_nr_d          : std_logic_vector(31 downto 0) := (others=>'0');
  signal event_nr_q          : std_logic_vector(31 downto 0) := (others=>'0');
  signal adc_0_ch_en_q       : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal adc_1_ch_en_q       : std_logic_vector(CGN_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal sample_freq_q       : std_logic_vector(15 downto 0) := (others=>'0');
  signal payload_length_q    : std_logic_vector(15 downto 0) := (others=>'0');
  signal trigger_nr_q        : std_logic_vector(15 downto 0) := (others=>'0');
  signal drs0_trigger_cell_q : std_logic_vector( 9 downto 0) := (others=>'0');
  signal drs1_trigger_cell_q : std_logic_vector( 9 downto 0) := (others=>'0');
  signal trigger_type_q      : std_logic_vector(15 downto 0) := (others=>'0');
  signal temperature_q       : std_logic_vector(15 downto 0) := (others=>'0');
  signal header_nr_q         : std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0) := (others=>'0');

  signal inter_pkg_delay     : std_logic_vector(23 downto 0) := (others=>'0');

  signal udp_pkg_nr          : std_logic_vector(15 downto 0) := (others=>'0');

  signal write_done          : std_logic_vector(C_BUFFERS-1 downto 0) := (others=>'0');
  signal buffer_full         : std_logic_vector(C_BUFFERS-1 downto 0) := (others=>'0');
  signal read_done           : std_logic_vector(C_BUFFERS-1 downto 0) := (others=>'0');
  signal buffer_empty        : std_logic_vector(C_BUFFERS-1 downto 0) := (others=>'0');

  signal shuffler_data_in    : std_logic_vector(12 downto 0) := (others=>'0');
  signal shuffler_we         : std_logic := '0';
  signal shuffler_full       : std_logic := '0';
  signal buf_out_fifo_eop_n  : std_logic := '0';
  signal shuffler_data_out   : std_logic_vector(7 downto 0) := (others=>'0');
  signal shuffler_rd_en      : std_logic := '0';
  signal shuffler_empty      : std_logic := '0';
  signal shuffler_eop_n_out  : std_logic := '0';
  signal shuffler_en         : std_logic := '0';

  signal buffer_data_out     : std_logic_vector(11 downto 0) := (others=>'0');

  signal buf_out_fifo_din_s1 : std_logic_vector(12 downto 0) := (others=>'0');
  signal buf_out_fifo_din_s2 : std_logic_vector(12 downto 0) := (others=>'0');
  signal buf_out_fifo_we     : std_logic := '0';
  signal buf_out_fifo_we_s1  : std_logic := '0';
  signal buf_out_fifo_we_s2  : std_logic := '0';
  signal sync_fifo_we        : std_logic := '0';
  signal buf_out_fifo_full   : std_logic := '0';
  signal buf_out_fifo_afull  : std_logic := '0';
  signal buf_out_fifo_empty  : std_logic := '0';

  signal enable_on_daq       : std_logic := '0';
  signal enable_on_ll        : std_logic := '0';
  
  signal ll_rst              : std_logic := '0';

begin

  BUSY_O      <= wr_busy;
  FIFO_FULL_O <= fifo_full;

  serdes_to_ll_clk : cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 2,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => ADC_DIV_CLK_I(0),
    CLK_B_I => LL_CLK_I,
    PORT_A_I(0) => RST_I,
    PORT_A_I(1) => enable_on_daq,
    PORT_B_O(0) => ll_rst,
    PORT_B_O(1) => enable_on_ll
  );

  system_to_ll_clk : cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 24,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 1,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => LL_CLK_I, -- not used without input regs
    CLK_B_I => LL_CLK_I,
    PORT_A_I => INTER_PKG_DELAY_I,
    PORT_B_O => inter_pkg_delay
  );

  system_to_adc_clk : cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 1,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I    => ADC_DIV_CLK_I(0), -- not used without input regs
    CLK_B_I    => ADC_DIV_CLK_I(0),
    PORT_A_I(0) => ENABLE_I,
    PORT_B_O(0) => enable_on_daq
  );

  ll_to_serdes_tig : for i in C_BUFFERS-1 downto 0 generate
    event_info_serdes_to_ll_clk : cdc_sync
    GENERIC MAP (
      CGN_DATA_WIDTH          => 5*16+32+2*10+CGN_IP_HEADER_SEL_WIDTH,
      CGN_USE_INPUT_REG_A     => 0,
      CGN_USE_OUTPUT_REG_A    => 0,
      CGN_USE_GRAY_CONVERSION => 0,
      CGN_NUM_SYNC_REGS_B     => 2,
      CGN_NUM_OUTPUT_REGS_B   => 0,
      CGN_GRAY_TO_BIN_STYLE   => 0
    )
    PORT MAP (
      CLK_A_I => ADC_DIV_CLK_I(0),
      CLK_B_I => LL_CLK_I,
      PORT_A_I( 31 downto   0) => event_nr_lut(i),
      PORT_A_I( 47 downto  32) => sample_freq_lut(i),
      PORT_A_I( 63 downto  48) => payload_length_lut(i),
      PORT_A_I( 79 downto  64) => trigger_nr_lut(i),
      PORT_A_I( 89 downto  80) => drs0_trigger_cell_lut(i),
      PORT_A_I( 99 downto  90) => drs1_trigger_cell_lut(i),
      PORT_A_I(115 downto 100) => trigger_type_lut(i),
      PORT_A_I(131 downto 116) => temperature_lut(i),
      PORT_A_I(131+CGN_IP_HEADER_SEL_WIDTH downto 132) => header_nr_lut(i),
      PORT_B_O( 31 downto   0) => event_nr_lut_q(i),
      PORT_B_O( 47 downto  32) => sample_freq_lut_q(i),
      PORT_B_O( 63 downto  48) => payload_length_lut_q(i),
      PORT_B_O( 79 downto  64) => trigger_nr_lut_q(i),
      PORT_B_O( 89 downto  80) => drs0_trigger_cell_lut_q(i),
      PORT_B_O( 99 downto  90) => drs1_trigger_cell_lut_q(i),
      PORT_B_O(115 downto 100) => trigger_type_lut_q(i),
      PORT_B_O(131 downto 116) => temperature_lut_q(i),
      PORT_B_O(131+CGN_IP_HEADER_SEL_WIDTH downto 132) => header_nr_lut_q(i)
    );

    ch_en_info_serdes_to_ll_clk : cdc_sync
    GENERIC MAP (
      CGN_DATA_WIDTH          => 2*CGN_CHNLS_PER_ADC,
      CGN_USE_INPUT_REG_A     => 0,
      CGN_USE_OUTPUT_REG_A    => 0,
      CGN_USE_GRAY_CONVERSION => 0,
      CGN_NUM_SYNC_REGS_B     => 2,
      CGN_NUM_OUTPUT_REGS_B   => 0,
      CGN_GRAY_TO_BIN_STYLE   => 0
    )
    PORT MAP (
      CLK_A_I => ADC_DIV_CLK_I(0),
      CLK_B_I => LL_CLK_I,
      PORT_A_I(  CGN_CHNLS_PER_ADC-1 downto 0)                 => adc_0_ch_en_lut(i),
      PORT_A_I(2*CGN_CHNLS_PER_ADC-1 downto CGN_CHNLS_PER_ADC) => adc_1_ch_en_lut(i),
      PORT_B_O(  CGN_CHNLS_PER_ADC-1 downto 0)                 => adc_0_ch_en_lut_q(i),
      PORT_B_O(2*CGN_CHNLS_PER_ADC-1 downto CGN_CHNLS_PER_ADC) => adc_1_ch_en_lut_q(i)
    );
  end generate;

  -- There is no channel 9 in transparent mode
  adc_0_ch_tx_en <= ADC_0_CH_TX_EN_I when READOUT_SOURCE_I = '0' else ( "011111111" and ADC_0_CH_TX_EN_I );
  adc_1_ch_tx_en <= ADC_1_CH_TX_EN_I when READOUT_SOURCE_I = '0' else ( "011111111" and ADC_1_CH_TX_EN_I );

  adc_data_fifo : for adc in 0 to CGN_NR_OF_ADCS-1 generate
  begin

    -- might be replaced by synchronous fifo
    -- or even omitted when all is running on the same clock
    asynch_data_input_fifo_inst : entity psi_3205_v1_00_a.fifo_gen_async
      generic map
      (
        C_FAMILY            =>"spartan6", -- "spartan6" -- "virtex5",
        C_ISE_12_4          => false,  -- true
        C_ISE_14_7          => true, -- false
        C_DATA_WIDTH        => C_DWIDTH_PER_ADC,
        C_FIFO_DEPTH        => 2**4,
        C_MEMORY_TYPE       => 0,     -- 0 = distributed RAM, 1 = BRAM
        C_USE_EMBEDDED_REG  => 0,     -- Valid only for BRAM based FIFO, otherwise needs to be set to 0
        C_PRELOAD_REGS      => 1,     -- 1 = first word fall through
        C_PRELOAD_LATENCY   => 0,     -- 0 = first word fall through,  needs to be set 2 when C_USE_EMBEDDED_REG = 1
        C_HAS_WR_COUNT      => 0,
        C_WR_COUNT_WIDTH    => 4,
        C_HAS_ALMOST_FULL   => 1,
        C_HAS_PROG_FULL     => 0,     -- 0 or 1, 1 for constant prog full
        C_PROG_FULL_OFFSET  => 3,     -- Setting determines the difference between FULL and PROG_FULL conditions
        C_HAS_WR_ACK        => 0,
        C_WR_ACK_LOW        => 0,
        C_HAS_WR_ERR        => 0,
        C_WR_ERR_LOW        => 0,
        C_HAS_RD_COUNT      => 0,
        C_RD_COUNT_WIDTH    => 4,
        C_HAS_ALMOST_EMPTY  => 0,
        C_HAS_PROG_EMPTY    => 0,     -- 0 or 1, 1 for constant prog empty
        C_PROG_EMPTY_OFFSET => 2,     -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
        C_HAS_RD_ACK        => 0,
        C_RD_ACK_LOW        => 0,
        C_HAS_RD_ERR        => 0,
        C_RD_ERR_LOW        => 0
      )
      port map
      (
        RESET_ASYNC_I     => RST_I,
        WR_CLK_I          => ADC_DIV_CLK_I(adc),
        WR_EN_I           => fifo_wea(adc),
        WR_DATA_I         => SERDES_DATA_I((adc+1)*C_DWIDTH_PER_ADC-1 downto adc*C_DWIDTH_PER_ADC),
        WR_FULL_O         => fifo_full(adc),
        WR_ALMOST_FULL_O  => open,
        WR_PROG_FULL_O    => open,
        WR_COUNT_O        => open,
        WR_ACK_O          => open,
        WR_ERR_O          => open,
        RD_CLK_I          => ADC_DIV_CLK_I(0),
        RD_EN_I           => fifo_rd_en,
        RD_DATA_O         => fifo_data_out(adc),
        RD_EMPTY_O        => fifo_empty(adc),     -- only 1 entry left in fifo
        RD_ALMOST_EMPTY_O => open,
        RD_PROG_EMPTY_O   => open,
        RD_COUNT_O        => open,
        RD_ACK_O          => open,
        RD_ERR_O          => open
      );
  end generate adc_data_fifo;

  fifo_wea      <= SERDES_WE_I;
  no_fifo_empty <= AND_REDUCE(not fifo_empty);

  adc_buffer_array : for adc in 0 to CGN_NR_OF_ADCS-1 generate
    channel_buffer : for ch in 0 to CGN_CHNLS_PER_ADC-1 generate

      process(fifo_data_out, no_fifo_empty, wr_we, reading_ch9)
      begin
        if (ch = 8) then
          buf_dina(adc,  8) <= "0000" & fifo_data_out(adc)(8*CGN_ADC_CH_DWIDTH-1 downto 7*CGN_ADC_CH_DWIDTH);
          buf_wea(ch)       <= wr_we and reading_ch9;
        else
          buf_dina(adc, ch) <= "0000" & fifo_data_out(adc)((ch+1)*CGN_ADC_CH_DWIDTH-1 downto ch*CGN_ADC_CH_DWIDTH);
          buf_wea(ch)       <= wr_we and not reading_ch9;
        end if;
      end process;

      ram_buffer_x : entity psi_3205_v1_00_a.ram_sdp
      generic map
      (
        CGN_ADDR_WIDTH => log2ceil(C_BUF_ADDRS),
        CGN_DATA_WIDTH => 16,
        CGN_RAM_STYLE  => "block"  -- style of implemented ram type: {auto|block|distributed|pipe_distributed};
      )
      port map
      (
        PA_CLK_I    => ADC_DIV_CLK_I(0),
        PA_ADDR_I   => buf_addra,
        PA_DATA_I   => buf_dina(adc, ch),
        PA_WR_EN_I  => buf_wea(ch),
        PB_CLK_I    => LL_CLK_I,
        PB_ADDR_I   => buf_addrb,
        PB_RD_EN_I  => '1',
        PB_DATA_O   => buf_doutb(adc, ch)
      );

    end generate channel_buffer;
  end generate adc_buffer_array;

  event_buffer_flags : buffer_flags
  generic map
  (
    CGN_NR_OF_BUFFERS => C_BUFFERS
  )
  port map
  (
    WRITE_DONE_I => write_done,
    FULL_O       => buffer_full,
    READ_DONE_I  => read_done,
    EMPTY_O      => buffer_empty,
    RST_I        => RST_I,
    WR_CLK_I     => ADC_DIV_CLK_I(0),
    RD_CLK_I     => LL_CLK_I
  );

  wr_ctrl_inst : wd2_pkg_gen_wr_ctrl
  generic map (
    CGN_BUFFERS               => C_BUFFERS,
    CGN_MAX_SAMPLES_PER_EVENT => C_MAX_SAMPLES_PER_EVENT)
  port map (
    BUSY_O           => wr_busy,
    WR_SAMPLE_NR_O   => wr_sample_nr,
    WR_BUFER_NR_O    => wr_buffer_nr,
    BUF_WE_O         => wr_we,
    BUF_WRITE_DONE_O => write_done,
    BUF_FULL_I       => buffer_full,
    NO_FIFO_EMPTY_I  => no_fifo_empty,
    FIFO_RE_O        => fifo_rd_en,
    EVENT_NR_I       => DRS_EVENT_NR_I,
    EVENT_NR_O       => event_nr_d,
    EVENT_LUT_WE_O   => event_info_we,
    ADC_0_CH_TX_EN_I => adc_0_ch_tx_en,
    ADC_1_CH_TX_EN_I => adc_1_ch_tx_en,
    READING_CH9_O    => reading_ch9,
    ENABLE_I         => enable_on_daq,
    CLK_I            => ADC_DIV_CLK_I(0),
    RST_I            => RST_I);

  -- Arbitration done implicitly by buffer arbiter
  event_info_register : process(ADC_DIV_CLK_I(0))
  begin
    if rising_edge(ADC_DIV_CLK_I(0)) then
      if event_info_we = '1' then
        event_nr_lut(CONV_INTEGER(wr_buffer_nr))          <= event_nr_d;
        adc_0_ch_en_lut(CONV_INTEGER(wr_buffer_nr))       <= adc_0_ch_tx_en;
        adc_1_ch_en_lut(CONV_INTEGER(wr_buffer_nr))       <= adc_1_ch_tx_en;
        sample_freq_lut(CONV_INTEGER(wr_buffer_nr))       <= SAMPLING_FREQ_I;
        payload_length_lut(CONV_INTEGER(wr_buffer_nr))    <= PAYLOAD_LENGTH_I;
        trigger_nr_lut(CONV_INTEGER(wr_buffer_nr))        <= TRIGGER_NR_I;
        drs0_trigger_cell_lut(CONV_INTEGER(wr_buffer_nr)) <= DRS0_TRIGGER_CELL_I;
        drs1_trigger_cell_lut(CONV_INTEGER(wr_buffer_nr)) <= DRS1_TRIGGER_CELL_I;
        trigger_type_lut(CONV_INTEGER(wr_buffer_nr))      <= TRIGGER_TYPE_I;
        header_nr_lut(CONV_INTEGER(wr_buffer_nr))         <= IP_HEADER_SEL_I;
        temperature_lut(CONV_INTEGER(rd_buffer_nr))       <= TEMPERATURE_I;
      end if;
    end if;
  end process event_info_register;

  -- Arbitration done implicitly by buffer arbiter
  buf_addra           <= wr_buffer_nr & wr_sample_nr;
  event_nr_q          <= event_nr_lut_q(CONV_INTEGER(rd_buffer_nr));
  adc_0_ch_en_q       <= adc_0_ch_en_lut_q(CONV_INTEGER(rd_buffer_nr));
  adc_1_ch_en_q       <= adc_1_ch_en_lut_q(CONV_INTEGER(rd_buffer_nr));
  sample_freq_q       <= sample_freq_lut_q(CONV_INTEGER(rd_buffer_nr));
  payload_length_q    <= payload_length_lut_q(CONV_INTEGER(rd_buffer_nr));
  trigger_nr_q        <= trigger_nr_lut_q(CONV_INTEGER(rd_buffer_nr));
  drs0_trigger_cell_q <= drs0_trigger_cell_lut_q(CONV_INTEGER(rd_buffer_nr));
  drs1_trigger_cell_q <= drs1_trigger_cell_lut_q(CONV_INTEGER(rd_buffer_nr));
  trigger_type_q      <= trigger_type_lut_q(CONV_INTEGER(rd_buffer_nr));
  header_nr_q         <= header_nr_lut_q(CONV_INTEGER(rd_buffer_nr));
  temperature_q       <= temperature_lut_q(CONV_INTEGER(rd_buffer_nr));

  rd_ctrl_inst : wd2_pkg_gen_rd_ctrl
  generic map (
    CGN_BUFFERS               => C_BUFFERS,
    CGN_MAX_SAMPLES_PER_EVENT => C_MAX_SAMPLES_PER_EVENT,
    CGN_SEGMENTS              => C_SEGMENTS,
    CGN_NR_OF_ADCS            => CGN_NR_OF_ADCS,
    CGN_CHNLS_PER_ADC         => CGN_CHNLS_PER_ADC,
    CGN_ADC_CH_DWIDTH         => CGN_ADC_CH_DWIDTH)
  port map (
    BUF_RD_ADDR_O    => buf_addrb,
    BUF_READ_DONE_O  => read_done,
    BUF_EMPTY_I      => buffer_empty,
    RD_BUFFER_NR_O   => rd_buffer_nr,
    RD_CHIP_NR_O     => rd_chip_nr,
    RD_CHANNEL_NR_O  => rd_channel_nr,
    RD_SEGMENT_NR_O  => rd_segment_nr,
    SHUFFLER_EOP_N_O => buf_out_fifo_eop_n,
    SHUFFLER_EN_O    => shuffler_en,
    SHUFFLER_WEN_O   => buf_out_fifo_we,
    SHUFFLER_FULL_I  => buf_out_fifo_full,
    SHUFFLER_AFULL_I => buf_out_fifo_afull,
    EVENT_NR_I       => event_nr_q,
    ADC_0_CH_TX_EN_I => adc_0_ch_en_q,
    ADC_1_CH_TX_EN_I => adc_1_ch_en_q,
    PKG_NR_O         => udp_pkg_nr,
    ENABLE_I         => enable_on_ll,
    CLK_I            => LL_CLK_I,
    RST_I            => ll_rst);

  shuffler_we      <= not buf_out_fifo_empty and not shuffler_full;

  buffer_data_out     <= buf_doutb(CONV_INTEGER(rd_chip_nr_s), CONV_INTEGER(rd_channel_nr_s))(11 downto 0);

  process(LL_CLK_I) -- pipelining
  begin
    if rising_edge(LL_CLK_I) then
      if buf_out_fifo_full = '0' then
        rd_chip_nr_s        <= rd_chip_nr;
        rd_channel_nr_s     <= rd_channel_nr;
        buf_out_fifo_din_s1 <= buf_out_fifo_eop_n & buffer_data_out;
        buf_out_fifo_din_s2 <= buf_out_fifo_din_s1;
        buf_out_fifo_we_s1 <= buf_out_fifo_we;
        buf_out_fifo_we_s2 <= buf_out_fifo_we_s1;
      end if;
    end if;
  end process;

  sync_fifo_we <= buf_out_fifo_we_s2 and not buf_out_fifo_full;

  inst_sync_fifo_13x16_fwft : entity psi_3205_v1_00_a.fifo_gen_sync
  generic map
  (
    C_FAMILY            => "spartan6", -- "spartan6" -- "virtex5",
    C_ISE_12_4          => false,  -- true
    C_ISE_14_7          => true,   -- false
    C_FIFO_DEPTH        => 2**4,
    C_MEMORY_TYPE       => 0,     -- 0 = distributed RAM, 1 = BRAM
    C_HAS_DCOUNT        => 0,
    C_DCOUNT_WIDTH      => 4,
    C_PRELOAD_REGS      => 1,     -- 1 = first word fall through
    C_PRELOAD_LATENCY   => 0,     -- 0 = first word fall through
    C_USE_EMBEDDED_REG  => 0,
    C_WRITE_DATA_WIDTH  => 13,
    C_HAS_ALMOST_FULL   => 1,
    C_HAS_PROG_FULL     => 0,     -- 0 or 1, 1 for constant prog full
    C_PROG_FULL_OFFSET  => 3,     -- Setting determines the difference between FULL and PROG_FULL conditions
    C_HAS_WR_ACK        => 0,
    C_WR_ACK_LOW        => 0,
    C_HAS_WR_ERR        => 0,
    C_WR_ERR_LOW        => 0,
    C_READ_DATA_WIDTH   => 13,
    C_HAS_ALMOST_EMPTY  => 0,
    C_HAS_PROG_EMPTY    => 0,     -- 0 or 1, 1 for constant prog empty
    C_PROG_EMPTY_OFFSET => 2,     -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
    C_HAS_RD_ACK        => 0,
    C_RD_ACK_LOW        => 0,
    C_HAS_RD_ERR        => 0,
    C_RD_ERR_LOW        => 0
  )
  port map
  (
    CLK_I             => LL_CLK_I,
    RESET_SYNC_I      => ll_rst,
    DATA_COUNT_O      => open,
    WR_EN_I           => sync_fifo_we,
    WR_DATA_I         => buf_out_fifo_din_s2,
    WR_FULL_O         => buf_out_fifo_full,
    WR_ALMOST_FULL_O  => buf_out_fifo_afull,
    WR_PROG_FULL_O    => open,
    WR_ACK_O          => open,
    WR_ERR_O          => open,
    RD_EN_I           => shuffler_we,
    RD_DATA_O         => shuffler_data_in,
    RD_EMPTY_O        => buf_out_fifo_empty,
    RD_ALMOST_EMPTY_O => open,
    RD_PROG_EMPTY_O   => open,
    RD_ACK_O          => open,
    RD_ERR_O          => open
  );

  shuffling_inst : wd2_pkg_gen_2x12to3x8
  port map
  (
    DATA_I   => shuffler_data_in(11 downto 0),
    WR_EN_I  => shuffler_we,
    FULL_O   => shuffler_full,
    SOP_N_I  => '1',
    EOP_N_I  => shuffler_data_in(12),
    DATA_O   => shuffler_data_out,
    RD_EN_I  => shuffler_rd_en,
    EMPTY_O  => shuffler_empty,
    SOP_N_O  => open,
    EOP_N_O  => shuffler_eop_n_out,
    ENABLE_I => shuffler_en,
    CLK_I    => LL_CLK_I,
    RST_I    => ll_rst
  );

  --LL_TX_EOF_N_O <= shuffler_eop_n_out;

  ll_ctrl_inst : wd2_pkg_gen_ll_ctrl
  generic map (
    CGN_SEGMENTS            => C_SEGMENTS,
    CGN_NR_OF_ADCS          => CGN_NR_OF_ADCS,
    CGN_CHNLS_PER_ADC       => CGN_CHNLS_PER_ADC,
    CGN_LL_DATA_WIDTH       => CGN_LL_DATA_WIDTH,
    CGN_LL_REM_WIDTH        => CGN_LL_REM_WIDTH,
    CGN_FRAME_LENGTH_WIDTH  => CGN_FRAME_LENGTH_WIDTH,
    CGN_IP_HEADER_SEL_WIDTH => CGN_IP_HEADER_SEL_WIDTH) 
  port map (
    LL_CLK_I            => LL_CLK_I,
    LL_TX_DATA_O        => LL_TX_DATA_O,
    LL_TX_SOF_N_O       => LL_TX_SOF_N_O,
    LL_TX_EOF_N_O       => LL_TX_EOF_N_O,
    LL_TX_REM_O         => LL_TX_REM_O,
    LL_TX_SRC_RDY_N_O   => LL_TX_SRC_RDY_N_O,
    LL_TX_DST_RDY_N_I   => LL_TX_DST_RDY_N_I,
    FRAME_LENGTH_O      => FRAME_LENGTH_O,
    IP_HEADER_SEL_I     => header_nr_q,
    IP_HEADER_SEL_O     => IP_HEADER_SEL_O,
    SHUFFLER_DATA_I     => shuffler_data_out,
    SHUFFLER_RE_O       => shuffler_rd_en,
    SHUFFLER_EMPTY_I    => shuffler_empty,
    SHUFFLER_EOP_N_I    => shuffler_eop_n_out,
    PROTOCOL_VERSION_I  => PROTOCOL_VERSION_I,
    BOARD_REVISION_I    => BOARD_REVISION_I,
    BOARD_ID_I          => BOARD_ID_I,
    CRATE_ID_I          => CRATE_ID_I,
    SLOT_ID_I           => SLOT_ID_I,
    SAMPLING_FREQ_I     => sample_freq_q,
    PAYLOAD_LENGTH_I    => payload_length_q,
    RD_CHIP_NR_I        => rd_chip_nr,
    RD_CHANNEL_NR_I     => rd_channel_nr,
    RD_SEGMENT_NR_I     => rd_segment_nr,
    PKG_TYPE_I          => "0000",
    EVENT_NR_I          => event_nr_q,
    TRIGGER_NR_I        => trigger_nr_q,
    DRS0_TRIGGER_CELL_I => drs0_trigger_cell_q,
    DRS1_TRIGGER_CELL_I => drs1_trigger_cell_q,
    TRIGGER_TYPE_I      => trigger_type_q,
    INTER_PKG_DELAY_I   => inter_pkg_delay,
    TEMPERATURE_I       => temperature_q,
    UDP_PKG_NR_I        => udp_pkg_nr,
    ENABLE_I            => enable_on_ll,
    RST_I               => ll_rst);

end struct;
