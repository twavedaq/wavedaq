---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Local Link Interface Controller
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.03.2015 09:14:57
--
--  Description : Controls the local link interface of the WaveDream2 (WD2) 
--                package generator. The interface is attached to the UDP
--                packager.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity wd2_pkg_gen_ll_ctrl is
generic
(
  CGN_SEGMENTS            : integer :=  2;
  CGN_NR_OF_ADCS          : integer :=  2;
  CGN_CHNLS_PER_ADC       : integer :=  8;
  CGN_LL_DATA_WIDTH       : integer :=  8;
  CGN_LL_REM_WIDTH        : integer :=  2;
  CGN_FRAME_LENGTH_WIDTH  : integer := 14;
  CGN_IP_HEADER_SEL_WIDTH : integer :=  6
);  
port
(
  -- Local Link output interface
  LL_CLK_I             : in   std_logic;
  LL_TX_DATA_O         : out  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
  LL_TX_SOF_N_O        : out  std_logic;
  LL_TX_EOF_N_O        : out  std_logic;
  LL_TX_REM_O          : out  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
  LL_TX_SRC_RDY_N_O    : out  std_logic;
  LL_TX_DST_RDY_N_I    : in   std_logic;
  FRAME_LENGTH_O       : out  std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
  IP_HEADER_SEL_I      : in   std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
  IP_HEADER_SEL_O      : out  std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);

  -- Shuffler interface
  SHUFFLER_DATA_I      : in  std_logic_vector(7 downto 0);
  SHUFFLER_RE_O        : out std_logic;
  SHUFFLER_EMPTY_I     : in  std_logic;
  SHUFFLER_EOP_N_I     : in  std_logic;

  -- Side Data
  PROTOCOL_VERSION_I   : in  std_logic_vector(7 downto 0);
  BOARD_REVISION_I     : in  std_logic_vector(7 downto 0);
  BOARD_ID_I           : in  std_logic_vector(15 downto 0);
  CRATE_ID_I           : in  std_logic_vector(7 downto 0);
  SLOT_ID_I            : in  std_logic_vector(7 downto 0);
  SAMPLING_FREQ_I      : in  std_logic_vector(15 downto 0);
  PAYLOAD_LENGTH_I     : in  std_logic_vector(15 downto 0);
  RD_CHIP_NR_I         : in  std_logic_vector(log2ceil(CGN_NR_OF_ADCS)-1 downto 0);
  RD_CHANNEL_NR_I      : in  std_logic_vector(log2ceil(CGN_CHNLS_PER_ADC)-1 downto 0);
  RD_SEGMENT_NR_I      : in  std_logic_vector(log2ceil(CGN_SEGMENTS)-1 downto 0);
  PKG_TYPE_I           : in  std_logic_vector(3 downto 0);
  EVENT_NR_I           : in  std_logic_vector(31 downto 0);
  TRIGGER_NR_I         : in  std_logic_vector(15 downto 0);
  DRS0_TRIGGER_CELL_I  : in  std_logic_vector(9 downto 0);
  DRS1_TRIGGER_CELL_I  : in  std_logic_vector(9 downto 0);
  TRIGGER_TYPE_I       : in  std_logic_vector(15 downto 0);
  INTER_PKG_DELAY_I    : in  std_logic_vector(23 downto 0);
  TEMPERATURE_I        : in  std_logic_vector(15 downto 0);
  UDP_PKG_NR_I         : in  std_logic_vector(15 downto 0);

  -- General Signals
  ENABLE_I : IN std_logic;
  RST_I    : IN std_logic
);
end wd2_pkg_gen_ll_ctrl;

architecture Behavioral of wd2_pkg_gen_ll_ctrl is

  -- states of state machine
  type type_ll_state is (idle, transmit_header, transmit_data, wait_inter_pkg_delay); --, transmit_eof);
  signal ll_state           : type_ll_state := idle;
  attribute fsm_encoding : string;
  attribute fsm_encoding of ll_state : signal is "one-hot";

  type header_type is array (31 downto 0) of std_logic_vector(7 downto 0);
  signal header : header_type := (others=>(others=>'0'));
  signal header_byte_sel   : std_logic_vector(4 downto 0) := (others=>'0');

  signal inter_pkg_delay_count : std_logic_vector(23 downto 0) := (others=>'0');

  signal eof_s_n               : std_logic := '1';

begin

  --FRAME_LENGTH_O  <= (others=>'0');
  FRAME_LENGTH_O  <= "00" & X"320";
  IP_HEADER_SEL_O <= IP_HEADER_SEL_I;

  -- Header data mapping
  header(0)  <= PROTOCOL_VERSION_I;
  header(1)  <= BOARD_REVISION_I;
  header(2)  <= BOARD_ID_I(15 downto 8); -- Board ID (MSB)
  header(3)  <= BOARD_ID_I( 7 downto 0); -- Board ID (LSB)
  header(4)  <= CRATE_ID_I;
  header(5)  <= SLOT_ID_I;
  header(6)  <= "000" & RD_CHIP_NR_I & RD_CHANNEL_NR_I; -- Channel-Info: ADC | Channel-Info: Channel
  header(7)  <= "000" & RD_SEGMENT_NR_I & PKG_TYPE_I; -- Channel Segment Number (LSB)
  header(8)  <= EVENT_NR_I(31 downto 24); -- Data Sequence Number (MSB)
  header(9)  <= EVENT_NR_I(23 downto 16); -- Data Sequence Number (LSB)
  header(10) <= EVENT_NR_I(15 downto  8); -- Data Sequence Number (MSB)
  header(11) <= EVENT_NR_I( 7 downto  0); -- Data Sequence Number (LSB)
  header(12) <= SAMPLING_FREQ_I(15 downto 8); -- Sampling Frequency (MSB)
  header(13) <= SAMPLING_FREQ_I( 7 downto 0); -- Sampling Frequency (LSB)
  header(14) <= PAYLOAD_LENGTH_I(15 downto 8); -- Number of Samples (MSB)
  header(15) <= PAYLOAD_LENGTH_I( 7 downto 0); -- Number of Samples (LSB)
  header(16) <= TRIGGER_NR_I(15 downto 8); -- Hardware Sequence Number (Trigger Event) (MSB)
  header(17) <= TRIGGER_NR_I( 7 downto 0); -- Hardware Sequence Number (Trigger Event) (LSB)
  header(18) <= "000000" & DRS0_TRIGGER_CELL_I(9 downto 8); -- DRS Stop Cell (DRS Chip 0) (MSB)
  header(19) <=            DRS0_TRIGGER_CELL_I(7 downto 0); -- DRS Stop Cell (DRS Chip 0) (LSB)
  header(20) <= "000000" & DRS1_TRIGGER_CELL_I(9 downto 8); -- DRS Stop Cell (DRS Chip 1) (MSB)
  header(21) <=            DRS1_TRIGGER_CELL_I(7 downto 0); -- DRS Stop Cell (DRS Chip 1) (LSB)
  header(22) <= TRIGGER_TYPE_I(15 downto 8); -- Trigger Event Type (MSB)
  header(23) <= TRIGGER_TYPE_I( 7 downto 0); -- Trigger Event Type (LSB)
  header(24) <= TEMPERATURE_I(15 downto 8);
  header(25) <= TEMPERATURE_I( 7 downto 0);
  header(26) <= (others=>'0'); -- Reserved
  header(27) <= (others=>'0'); -- Reserved
  header(28) <= (others=>'0'); -- Reserved
  header(29) <= (others=>'0'); -- Reserved
  header(30) <= UDP_PKG_NR_I(15 downto 8); -- Package Sequence Number (MSB)
  header(31) <= UDP_PKG_NR_I( 7 downto 0); -- Package Sequence Number (LSB)

  LL_TX_REM_O   <= (others=>'0'); -- No reminder on 1Byte Bus.
  LL_TX_EOF_N_O <= SHUFFLER_EOP_N_I and eof_s_n;

  LL_TX_DATA_O <= header(CONV_INTEGER(header_byte_sel)) when ll_state = transmit_header else SHUFFLER_DATA_I;

  SHUFFLER_RE_O <= ( (not SHUFFLER_EMPTY_I) and (not LL_TX_DST_RDY_N_I) )
                   when ll_state = transmit_data
                   else '0';
  
  process(ll_state, SHUFFLER_EMPTY_I)
  begin
    if ll_state = transmit_data then
      LL_TX_SRC_RDY_N_O <= SHUFFLER_EMPTY_I;
    elsif ll_state = transmit_header then
      LL_TX_SRC_RDY_N_O <= '0';
    else
      LL_TX_SRC_RDY_N_O <= '1';
    end if;
  end process;

  fsm_ll_handler : process(LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      -- inter package delay timer
      if inter_pkg_delay_count > 0 then
        inter_pkg_delay_count <= inter_pkg_delay_count - 1;
      end if;

      if RST_I = '1' then
        header_byte_sel   <= (others=>'0');
        LL_TX_SOF_N_O     <= '1';
        --LL_TX_EOF_N_O     <= '1';
        eof_s_n           <= '1';
        ll_state <= idle;
      else
        -- defaults -----------------------
        --LL_TX_DATA_O      <= (others=>'0');
        --LL_TX_SOF_N_O     <= '1';
        --LL_TX_EOF_N_O     <= '1';
        -----------------------------------
        case ll_state is

          when idle =>
            LL_TX_SOF_N_O <= '1';
            header_byte_sel <= (others=>'0');
            if SHUFFLER_EMPTY_I = '0' and ENABLE_I = '1' then
              LL_TX_SOF_N_O     <= '0';
              eof_s_n           <= '1';
              --LL_TX_DATA_O      <= header(CONV_INTEGER(header_byte_sel));
              header_byte_sel   <= (others=>'0');--header_byte_sel + 1;
              ll_state <= transmit_header;
            end if;

          when transmit_header =>
            eof_s_n <= '1';
            --LL_TX_DATA_O <= header(CONV_INTEGER(header_byte_sel));
            if LL_TX_DST_RDY_N_I = '0' then
              LL_TX_SOF_N_O <= '1';
              header_byte_sel <= header_byte_sel + 1;
              if header_byte_sel = "11111" then
                ll_state <= transmit_data;
              end if;
            end if;

          when transmit_data =>
            LL_TX_SOF_N_O <= '1';
            if LL_TX_DST_RDY_N_I = '0' then
              --LL_TX_DATA_O  <= SHUFFLER_DATA_I;
              if SHUFFLER_EOP_N_I = '0' or eof_s_n = '0' then
                --LL_TX_EOF_N_O <= '0';
                --ll_state <= transmit_eof;
                eof_s_n <= '1';
                inter_pkg_delay_count <= INTER_PKG_DELAY_I;
                ll_state <= wait_inter_pkg_delay;
              end if;
            elsif SHUFFLER_EOP_N_I = '0' then
              eof_s_n <= '0';
            end if;

          when wait_inter_pkg_delay =>
            -- wait for inter package delay to reduce udp throuput
            LL_TX_SOF_N_O <= '1';
            eof_s_n <= '1';
            if inter_pkg_delay_count = 0 then
              ll_state <= idle;
            end if;

--          when transmit_eof =>
--            --LL_TX_EOF_N_O <= '0';
--            if LL_TX_DST_RDY_N_I = '0' then
--              --LL_TX_EOF_N_O <= '1';
--              ll_state <= idle;
--            end if;

          when others =>
            ll_state <= idle;
        end case;
      end if;
    end if;
  end process fsm_ll_handler;

end Behavioral;
