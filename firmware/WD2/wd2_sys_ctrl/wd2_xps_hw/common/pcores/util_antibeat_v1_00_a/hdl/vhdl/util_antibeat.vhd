---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Util Antibeat
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  27.06.2017 15:13:24
--
--  Description :  Antibeat to clean bouncing signal.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_antibeat is
  generic (
    CGN_CLK_PERIOD : real := 10.0;  -- ns
    CGN_T_STABLE   : real := 1000.0 -- ns
  );
  port (
    IN_I  : in  std_logic;
    OUT_O : out std_logic;
    CLK_I : in  std_logic
  );
end util_antibeat;

architecture behavioral of util_antibeat is

  constant C_COUNTS        : integer := INTEGER( (CGN_T_STABLE + CGN_CLK_PERIOD - 1.0) / CGN_CLK_PERIOD );
  constant C_COUNTER_WIDTH : integer := log2ceil(C_COUNTS+1);
  signal counter : std_logic_vector(C_COUNTER_WIDTH-1 downto 0) := (others=>'0');
  signal in_s1 : std_logic := '0';
  signal in_s2 : std_logic := '0';
  
begin

  -- sync ff
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      in_s1 <= IN_I;
      in_s2 <= in_s1;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      -- counter
      if in_s2 = '0' then
        counter <= CONV_STD_LOGIC_VECTOR(C_COUNTS, C_COUNTER_WIDTH);
      elsif counter > 0 then
        counter <= counter - 1;
      end if;
      -- output
      if counter = 0 then
        OUT_O <= '1';
      else
        OUT_O <= '0';
      end if;
    end if;
  end process;
  
end architecture behavioral;