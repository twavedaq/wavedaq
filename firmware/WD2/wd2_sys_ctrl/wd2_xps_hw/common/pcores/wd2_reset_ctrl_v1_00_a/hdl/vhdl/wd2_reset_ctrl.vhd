---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WD2 Reset Control
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  26.06.2017 12:42:35
--
--  Description :  WD2 reset signal generator.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity wd2_reset_ctrl is
  port (
    UC_MASTER_RST_I                  : in  std_logic;
    -- DRS FSM reset signals
    UC_REG_DRS_FSM_RST_I             : in  std_logic;
    UC_DRS_FSM_SOFT_RST_N_I          : in  std_logic;
    UC_REG_SCALER_RST_I              : in  std_logic;
    UC_REG_PACKAGER_RST_I            : in  std_logic;
    UC_REG_DCB_OSERDES_RST_I         : in  std_logic;
    UC_REG_TCB_OSERDES_RST_I         : in  std_logic;
    UC_REG_DATA_LINK_IF_RST_I        : in  std_logic;
    UC_REG_TRG_INFO_PERR_CNT_RST_I   : in  std_logic;
    UC_REG_EVENT_COUNTER_RST_I       : in  std_logic;
    -- PLL locks
    DAQ_PLL_LOCKED_I                 : in  std_logic;
    UC_DCM_LOCKED_I                  : in  std_logic;
    -- Reset outputs
    -- |--@ DAQ cloc k
    DRS_FSM_RST_O                    : out std_logic;
    SCALER_RST_O                     : out std_logic;
    PACKAGER_RST_O                   : out std_logic;
    SERDES_FRAME_CTRL_RST_O          : out std_logic;
    DCB_OSERDES_RST_O                : out std_logic;
    TCB_OSERDES_RST_O                : out std_logic;
    DATA_LINK_IF_RST_N_O             : out std_logic;
    TRG_INFO_PERR_CNT_RST_O          : out std_logic;
    EVENT_COUNTER_RST_O              : out std_logic;
    -- |--@ UC clock
    -- clocks
    DAQ_CLK_I                        : in  std_logic;
    UC_CLK_I                         : in  std_logic
  );
end wd2_reset_ctrl;

architecture behavioral of wd2_reset_ctrl is

  constant C_LOCK_DELAY    : integer := 10;
  constant C_MIN_RST_WIDTH : integer := 2;

  signal daq_lock_rst_sreg          : std_logic_vector(C_LOCK_DELAY-1 downto 0)    := (others=>'1');
  --signal uc_lock_rst_sreg           : std_logic_vector(C_LOCK_DELAY-1 downto 0)    := (others=>'1');
  signal drs_fsm_rst_sreg           : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal scaler_rst_sreg            : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal packager_rst_sreg          : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal dcb_oserdes_rst_sreg       : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal tcb_oserdes_rst_sreg       : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal data_link_if_rst_sreg      : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal trg_info_perr_cnt_rst_sreg : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');
  signal event_counter_rst_sreg     : std_logic_vector(C_MIN_RST_WIDTH-1 downto 0) := (others=>'1');

begin

  process(DAQ_CLK_I, DAQ_PLL_LOCKED_I)
  begin
    if DAQ_PLL_LOCKED_I = '0' then
      daq_lock_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      daq_lock_rst_sreg <= '0' & daq_lock_rst_sreg(C_LOCK_DELAY-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_DRS_FSM_RST_I, UC_DRS_FSM_SOFT_RST_N_I, UC_MASTER_RST_I)
  begin
    if UC_REG_DRS_FSM_RST_I    = '1' or
       UC_DRS_FSM_SOFT_RST_N_I = '0' or
      UC_MASTER_RST_I          = '1' then
      drs_fsm_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      drs_fsm_rst_sreg <= '0' & drs_fsm_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_SCALER_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_SCALER_RST_I = '1' or
       UC_MASTER_RST_I     = '1' then
      scaler_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      scaler_rst_sreg <= '0' & scaler_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_PACKAGER_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_PACKAGER_RST_I = '1' or
       UC_MASTER_RST_I       = '1' then
      packager_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      packager_rst_sreg <= '0' & packager_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_DCB_OSERDES_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_DCB_OSERDES_RST_I = '1' or
       UC_MASTER_RST_I          = '1' then
      dcb_oserdes_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      dcb_oserdes_rst_sreg <= '0' & dcb_oserdes_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_TCB_OSERDES_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_TCB_OSERDES_RST_I = '1' or
       UC_MASTER_RST_I          = '1' then
      tcb_oserdes_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      tcb_oserdes_rst_sreg <= '0' & tcb_oserdes_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_DATA_LINK_IF_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_DATA_LINK_IF_RST_I = '1' or
       UC_MASTER_RST_I           = '1' then
      data_link_if_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      data_link_if_rst_sreg <= '0' & data_link_if_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_TRG_INFO_PERR_CNT_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_TRG_INFO_PERR_CNT_RST_I = '1' or
       UC_MASTER_RST_I                = '1' then
        trg_info_perr_cnt_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      trg_info_perr_cnt_rst_sreg <= '0' & trg_info_perr_cnt_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I, UC_REG_EVENT_COUNTER_RST_I, UC_MASTER_RST_I)
  begin
    if UC_REG_EVENT_COUNTER_RST_I = '1' or
       UC_MASTER_RST_I            = '1' then
        event_counter_rst_sreg <= (others=>'1');
    elsif rising_edge(DAQ_CLK_I) then
      event_counter_rst_sreg <= '0' & event_counter_rst_sreg(C_MIN_RST_WIDTH-1 downto 1);
    end if;
  end process;

  process(DAQ_CLK_I)
  begin
    if rising_edge(DAQ_CLK_I) then
      DRS_FSM_RST_O           <=     daq_lock_rst_sreg(0) or drs_fsm_rst_sreg(0);
      SCALER_RST_O            <=     daq_lock_rst_sreg(0) or scaler_rst_sreg(0);
      PACKAGER_RST_O          <=     daq_lock_rst_sreg(0) or packager_rst_sreg(0);
      SERDES_FRAME_CTRL_RST_O <=     daq_lock_rst_sreg(0) or dcb_oserdes_rst_sreg(0);
      DCB_OSERDES_RST_O       <=     daq_lock_rst_sreg(0) or dcb_oserdes_rst_sreg(0);
      TCB_OSERDES_RST_O       <=     daq_lock_rst_sreg(0) or tcb_oserdes_rst_sreg(0);
      DATA_LINK_IF_RST_N_O    <= not(daq_lock_rst_sreg(0) or data_link_if_rst_sreg(0));
      TRG_INFO_PERR_CNT_RST_O <=     daq_lock_rst_sreg(0) or trg_info_perr_cnt_rst_sreg(0);
      EVENT_COUNTER_RST_O     <=     daq_lock_rst_sreg(0) or event_counter_rst_sreg(0);
    end if;
  end process;

  --process(UC_CLK_I, UC_DCM_LOCKED_I)
  --begin
  --  if UC_DCM_LOCKED_I = '0' then
  --    uc_lock_rst_sreg <= (others=>'1');
  --  elsif rising_edge(UC_CLK_I) then
  --    uc_lock_rst_sreg <= '0' & uc_lock_rst_sreg(C_LOCK_DELAY-1 downto 1);
  --  end if;
  --end process;

end architecture behavioral;
