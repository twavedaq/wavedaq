#######################################################################################
##  Paul Scherrer Institut
#######################################################################################
##
##  Unit: DRS4 Readout Control
##
##  Project: WaveDream2
##
##  PCB : -
##  Part: Xilinx Spartan6 XC6SLX100-3FGG484C
##
##  Tool Version: 14.7 (Version the code was testet with)
##
##  Author : Elmar Schmid (Author of generation script)
##  Created: 06.07.2015 11:32:33
##
##  Description: Entity to generate all control signals for the DRS4 chip and the
##                 ADC on the WD2 board. Configuration tasks over slow (SPI, Shiftregister)
##                 interfaces are not included here.
##
#######################################################################################
#######################################################################################

BEGIN wd2_drs4_read_ctrl

## Peripheral Options
OPTION IPTYPE = PERIPHERAL
OPTION IMP_NETLIST = TRUE
OPTION HDL = VHDL
OPTION IP_GROUP = USER


## Bus Interfaces

### Generics:
PARAMETER CGN_ADC_CLOCK_PERIOD_NS = 12, DT = INTEGER
PARAMETER CGN_SR_CLK_DIVIDER = 6, DT = INTEGER, RANGE = (2:32)
PARAMETER CGN_SR_CLK_DIV_COUNTER_SIZE = 3, DT = INTEGER
PARAMETER CGN_WRITE_DELAY_BITS = 6, DT = INTEGER
PARAMETER CGN_ODELAY_VALUE = 0, DT = INTEGER, RANGE = (0:255)
PARAMETER CGN_SR_CLOCK_PERIOD_NS  = 60, DT = INTEGER # min CGN_ADC_CLOCK_PERIOD_NS*2

### Ports:
PORT DRS_DATA_WEN_O         = "", DIR = O
PORT WRITE_DELAY_I          = "", DIR = I, VEC = [CGN_WRITE_DELAY_BITS-1:0]
PORT DRS_CH_TX_EN_I         = "", DIR = I, VEC = [17:0]
PORT EVENT_NUMBER_O         = "", DIR = O, VEC = [31:0]
PORT EVENT_COUNTER_RST_I    = "", DIR = I 
PORT DA_ACTIVE_I            = "", DIR = I
PORT READOUT_MODE_I         = "", DIR = I
PORT SINGLE_DAQ_I           = "", DIR = I
PORT NORMAL_DAQ_I           = "", DIR = I
PORT AUTO_DAQ_I             = "", DIR = I
PORT DRS_CONFIGURE_I        = "", DIR = I
PORT DRS_CLR_RSR_AT_RO_I    = "", DIR = I
PORT DRS_CONFIG_DONE_O      = "", DIR = O
PORT DRS_SOFT_TRIGGER_I     = "", DIR = I
PORT DRS_TRIGGER_I          = "", DIR = I
PORT DRS_DATA_VALID_O       = "", DIR = O
PORT READOUT_TRIGGER_O      = "", DIR = O
PORT DRS_CFR_DMODE_I        = "", DIR = I
PORT DRS_CFR_PLLEN_I        = "", DIR = I
PORT DRS_CFR_WSRLOOP_I      = "", DIR = I
PORT DRS_WSR_I              = "", DIR = I, VEC = [7:0]
PORT DRS_WCR_I              = "", DIR = I, VEC = [7:0]
PORT DRS_DENABLE_O          = "", DIR = O
PORT DRS_DWRITE_IO          = "", DIR = IO, THREE_STATE = TRUE, ENABLE=SINGLE, TRI_I = DRS_DWRITE_I,  TRI_O = DRS_DWRITE_O,  TRI_T = DRS_DWRITE_T
PORT DRS_DWRITE_I           = "", DIR = I
PORT DRS_DWRITE_O           = "", DIR = O
PORT DRS_DWRITE_T           = "", DIR = O
PORT DRS_RSRLOAD_O          = "", DIR = O
PORT DRS_SRCLK_0_O          = "", DIR = O
PORT DRS_SRCLK_1_O          = "", DIR = O
PORT DRS_ADDR_O             = "", DIR = O, VEC = [3:0]
PORT DRS_SRIN_O             = "", DIR = O
PORT DRS_SROUT_0_I          = "", DIR = I
PORT DRS_SROUT_1_I          = "", DIR = I
PORT BUSY_O                 = "", DIR = O
PORT SYS_BUSY_I             = "", DIR = I
PORT STOP_CELL_0_O          = "", DIR = O, VEC = [9:0]
PORT STOP_CELL_1_O          = "", DIR = O, VEC = [9:0]
PORT STOP_CELL_VALID_O      = "", DIR = O 
PORT STOP_WSR_0_O           = "", DIR = O, VEC = [7:0]
PORT STOP_WSR_1_O           = "", DIR = O, VEC = [7:0]
PORT LMK_DRS_REF_CLK_EN_I   = "", DIR = I
PORT LMK_LOCK_I             = "", DIR = I
PORT LMK_SYNC_N_I           = "", DIR = I
PORT RESET_I                = "", DIR = I, SIGIS = RST
PORT CLK_DRS_CTRL_I         = "", DIR = I, SIGIS = CLK

PORT CLK_DAQ_I              = "", DIR = I, SIGIS = CLK
PORT CLK_SYS_I              = "", DIR = I, SIGIS = CLK
END
