---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Timer
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  16.11.2017 16:11:22
--
--  Description :  Generic Timer.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library util_timer_v1_00_a;
use util_timer_v1_00_a.counter;

-- Add timing constraints to ignore cross clock domain paths
-- between microblaze and daq:
-- TIMESPEC TS_UC_TO_DAQ = FROM "TG_UC" TO "TG_DAQ" TIG;
-- TIMESPEC TS_DAQ_TO_UC = FROM "TG_DAQ" TO "TG_UC" TIG;


entity util_timer is
  port (
    TICKS_DAQ_O    : out std_logic_vector(63 downto 0);
    TICKS_UC_MSB_O : out std_logic_vector(31 downto 0);
    TICKS_UC_LSB_O : out std_logic_vector(31 downto 0);
    TICKS_SET_UC_I : in  std_logic_vector(63 downto 0);
    SET_UC_I       : in  std_logic;
    RE_LSB_UC_I    : in  std_logic;
    CLK_UC_I       : in  std_logic;
    CLK_DAQ_I      : in  std_logic
  );
end util_timer;

architecture behavioral of util_timer is
  
  signal time : std_logic_vector(63 downto 0) := (others=>'0');
  
begin
  
  TICKS_UC_LSB_O <= time(31 downto 0); 
  process(CLK_UC_I)
  begin
    if rising_edge(CLK_UC_I) then
      if RE_LSB_UC_I = '1' then
        TICKS_UC_MSB_O <= time(63 downto 32); 
      end if;
    end if;
  end process;

  time_counter : entity util_timer_v1_00_a.counter
  generic map (
    CGN_TOTAL_WIDTH => 64,
    CGN_LSB_WIDTH   => 2,
    CGN_DIR         => "UP"
  )
  port map (
    COUNT_I  => TICKS_SET_UC_I,
    COUNT_O  => time,
    LOAD_I   => SET_UC_I,
    ENABLE_I => '1',
    CLK_I    => CLK_DAQ_I
  );

  TICKS_DAQ_O <= time;
  
end architecture behavioral;