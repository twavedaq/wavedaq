--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_serdes_frame_ctrl.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  04.04.2016 08:26:17
--
--  Description :  
--    local link frame controller for OSERDES.
--    Note: protocol will only work if the available data rate at the input
--    of the OSERDES FIFO is greater or equal the data rate of the OSERDES.
--
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;
use psi_3205_v1_00_a.functions.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_serdes_frame_ctrl is
  generic
  (
    CGN_DATA_WIDTH        : integer := 8;
    CGN_LL_REM_WIDTH      : integer := 2;
    CGN_SIDE_DATA_WIDTH   : integer := 1;
    CGN_INTER_FRAME_GAP   : integer := 12;
    CGN_IDLE_PATTERN      : std_logic_vector( 7 downto 0) := X"5A";
    CGN_SOF_PATTERN       : std_logic_vector(15 downto 0) := X"F00D"
  );
  port
  (
    -- Ports
    CLK_I                : in  std_logic;
    RESET_I              : in  std_logic;

    -- SERDES FIFO IF
    SERDES_FIFO_DATA_O   : out std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    SERDES_FIFO_WE_O     : out std_logic;
    SERDES_FIFO_AFULL_I  : in  std_logic;
    --SERDES_FIFO_FULL_I   : in  std_logic;

    -- Local Link D
    LL_DATA_I            : in  std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_SOF_N_I           : in  std_logic;
    LL_EOF_N_I           : in  std_logic;
    LL_REM_I             : in  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0) := (others => '0');
    LL_SRC_RDY_N_I       : in  std_logic;
    LL_DST_RDY_N_O       : out std_logic;
    LL_SIDE_DATA_I       : in  std_logic_vector(CGN_SIDE_DATA_WIDTH-1 downto 0) := (others => '0')
  );
end ll_serdes_frame_ctrl;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of ll_serdes_frame_ctrl is

  constant C_WORD_COUNTER_WIDTH : integer := max(log2ceil(CGN_INTER_FRAME_GAP), 2);

  type states is (S_IDLE, S_START_FRAME, S_TX_DATA, S_TX_CHECKSUM, S_TX_IFG);
  signal state : states;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

  signal  word_count   : std_logic_vector(C_WORD_COUNTER_WIDTH-1 downto 0) := (others=>'0');

  signal  crc          : std_logic_vector(31 downto 0) := (others=>'0');
  signal  crc_byte     : std_logic_vector( 7 downto 0) := (others=>'0');
  signal  crc_calc     : std_logic := '0';
  signal  crc_reset    : std_logic := '0';

begin

  ---------------------------------------------------
  -- Calcalute Frame Check Sequence (CRC32)
  ---------------------------------------------------

  crc32_d8_inst : entity psi_3205_v1_00_a.crc32_d8
  port map
  (
    CLK_I        => CLK_I,
    RESET_I      => crc_reset,
    DATA_I       => LL_DATA_I,
    DATA_VALID_I => crc_calc,
    CRC_O        => crc,
    CRC_VALID_O  => open
  );
  -- multiplexer
  crc_byte <= crc((CONV_INTEGER(3-word_count(1 downto 0))+1)*8-1 downto CONV_INTEGER(3-word_count(1 downto 0))*8);

  SERDES_FIFO_WE_O <= not SERDES_FIFO_AFULL_I;

  fsm_output : process(state, LL_DATA_I, SERDES_FIFO_AFULL_I, LL_SRC_RDY_N_I, crc_byte)
  begin
    case state is
      when S_IDLE =>
        SERDES_FIFO_DATA_O <= CGN_IDLE_PATTERN;
        LL_DST_RDY_N_O     <= '1';
        crc_calc           <= '0';
        crc_reset          <= '0';
      when S_START_FRAME =>
        if word_count(0) = '1' then
          SERDES_FIFO_DATA_O <= CGN_SOF_PATTERN(15 downto 8);
        else
          SERDES_FIFO_DATA_O <= CGN_SOF_PATTERN( 7 downto 0);
        end if;
        LL_DST_RDY_N_O     <= '1';
        crc_calc           <= '0';
        crc_reset          <= '1';
      when S_TX_DATA => 
        SERDES_FIFO_DATA_O <= LL_DATA_I;
        LL_DST_RDY_N_O     <= SERDES_FIFO_AFULL_I;
        crc_calc           <= not SERDES_FIFO_AFULL_I and not LL_SRC_RDY_N_I;
        crc_reset          <= '0';
      when S_TX_CHECKSUM =>
        SERDES_FIFO_DATA_O <= crc_byte;
        LL_DST_RDY_N_O     <= '1';
        crc_calc           <= '0';
        crc_reset          <= '0';
      when S_TX_IFG =>
        SERDES_FIFO_DATA_O <= CGN_IDLE_PATTERN;
        LL_DST_RDY_N_O     <= '1';
        crc_calc           <= '0';
        crc_reset          <= '0';
    end case;
  end process fsm_output;
  
  ----------------------------------------
  -- State Machine
  ----------------------------------------

  fsm: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then

      -- word counter
      if word_count > 0 and SERDES_FIFO_AFULL_I = '0' then
        word_count <= word_count - 1;
      end if;

      if RESET_I = '1' then
        state <= S_IDLE;
        word_count <= (others=>'0');
      else
        case state is
          --------------------------------------------------------------------
          when S_IDLE =>
            if LL_SOF_N_I = '0' and LL_SRC_RDY_N_I = '0' then
              word_count <= CONV_STD_LOGIC_VECTOR(1, C_WORD_COUNTER_WIDTH);
              state <= S_START_FRAME;
            end if;
          --------------------------------------------------------------------
          when S_START_FRAME =>
            if word_count = 0 and SERDES_FIFO_AFULL_I = '0' then
              state <= S_TX_DATA;
            end if;
          --------------------------------------------------------------------
          when S_TX_DATA =>
            if LL_EOF_N_I = '0' and SERDES_FIFO_AFULL_I = '0' and LL_SRC_RDY_N_I = '0' then
              word_count <= CONV_STD_LOGIC_VECTOR(3, C_WORD_COUNTER_WIDTH);
              state <= S_TX_CHECKSUM;
            end if;
          --------------------------------------------------------------------
          when S_TX_CHECKSUM =>
            if word_count = 0 and SERDES_FIFO_AFULL_I = '0' then
              word_count <= CONV_STD_LOGIC_VECTOR(CGN_INTER_FRAME_GAP, C_WORD_COUNTER_WIDTH);
              state <= S_TX_IFG;
            end if;
          --------------------------------------------------------------------
          when S_TX_IFG =>
            if word_count = 0 and SERDES_FIFO_AFULL_I = '0' then
              state <= S_IDLE;
            end if;
          --------------------------------------------------------------------
        end case;
      end if;
    end if;
  end process;

-------------------------------------------------------------------------------
END behavior;

