---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WD2 SPI special commands decoder
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  03.08.2016 11:25:25
--
--  Description :  Interpreter for special binary WD2 commands transmitted via SPI over
--                 the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;

library UNISIM;
use UNISIM.Vcomponents.ALL;

entity spi_spec_cmd_decoder is
  generic
  (
    CGN_PULSE_LENGTH : integer := 1
  );
  port
  (
    CMD_I       : in  std_logic_vector(7 downto 0);
    RUN_CMD_I   : in  std_logic;
    CLEAR_O     : out std_logic;
    SYNC_O      : out std_logic;
    RECFG_O     : out std_logic;
    RST_I       : in  std_logic;
    CLK_I       : in  std_logic
  );
end spi_spec_cmd_decoder;

architecture behavioral_async of spi_spec_cmd_decoder is

  constant C_CMD_CLEAR : std_logic_vector(7 downto 0) := X"40";
  constant C_CMD_SYNC  : std_logic_vector(7 downto 0) := X"50";
  constant C_CMD_RECFG : std_logic_vector(7 downto 0) := X"F0";

  signal clear         : std_logic := '0';
  signal sync          : std_logic := '0';
  signal recfg         : std_logic := '0';

  signal reset_outputs : std_logic := '0';

  signal pw_addr       : std_logic_vector(3 downto 0) := (others=>'0');

begin

  pw_addr <= CONV_STD_LOGIC_VECTOR(CGN_PULSE_LENGTH-1, 4);

  pulse_width_sr_inst : SRL16E
  generic map (
    INIT => X"0000")
  port map (
    Q   => reset_outputs,  -- SRL data output
    A0  => pw_addr(0),     -- Select[0] input
    A1  => pw_addr(1),     -- Select[1] input
    A2  => pw_addr(2),     -- Select[2] input
    A3  => pw_addr(3),     -- Select[3] input
    CE  => '1',            -- Clock enable input
    CLK => CLK_I,          -- Clock input
    D   => RUN_CMD_I       -- SRL data input
  );

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if reset_outputs = '1' or RST_I = '1' then
        clear <= '0';
        sync  <= '0';
        recfg <= '0';
      elsif RUN_CMD_I = '1' then
        case CMD_I is
          when C_CMD_CLEAR => clear <= '1';
          when C_CMD_SYNC  => sync  <= '1';
          when C_CMD_RECFG => recfg <= '1';
          when others => null;
        end case;
      end if;
    end if;
  end process;

end behavioral_async;
