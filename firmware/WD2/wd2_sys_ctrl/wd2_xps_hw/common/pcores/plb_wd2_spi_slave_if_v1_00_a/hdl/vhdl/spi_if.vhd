---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic SPI Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.07.2014 14:58:31
--
--  Description :  Generic implementation of an SPI Slave Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity spi_if is
  generic (
    CGN_NUM_D_BITS     : integer := 8; -- allowable values: 1,2,3,...,32
    CGN_CPOL           : integer := 0; -- 0=positive _/"\_ , 1=negative "\_/"
    CGN_CPHA           : integer := 0; -- 0=first edge centered on data, 1=second edge centered on data
    CGN_LSB_FIRST      : integer := 0
  );
  port (
    -- SPI interface
    SOMI_O             : out std_logic;
    SIMO_I             : in  std_logic;
    SCK_I              : in  std_logic;
    SS_N_I             : in  std_logic;

    -- Status
    SLAVE_SEL_O        : out std_logic;
    RX_DATA_O          : out std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
    RX_DATA_VALID_O    : out std_logic;
    TX_DATA_I          : in  std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
    TX_DATA_ACK_O      : out std_logic;

    RST_ASYNC_I        : in  std_logic
  );
end spi_if;

architecture behavioral_async of spi_if is

  constant C_NUM_BYTES      : integer := (CGN_NUM_D_BITS+7)/8;

  signal edge_count         : std_logic_vector(5 downto 0) := (others=>'0');
  signal edge_count_zero    : std_logic := '0';
  signal edge_count_zero_s1 : std_logic := '0';
  signal edge_count_zero_s2 : std_logic := '0';
  signal edge_count_zero_s3 : std_logic := '0';

  signal input_shift_reg    : std_logic_vector(CGN_NUM_D_BITS-1 downto 0) := (others => '0');
  signal output_shift_reg   : std_logic_vector(CGN_NUM_D_BITS-1 downto 0) := (others => '0');

  signal somi               : std_logic := '0';
  signal sck                : std_logic := '0';

begin

  SLAVE_SEL_O <= SS_N_I;

  RX_DATA_O((C_NUM_BYTES*8)-1 downto CGN_NUM_D_BITS) <= (others=>'0');
  RX_DATA_O(CGN_NUM_D_BITS-1 downto 0)               <= input_shift_reg;

  SOMI_O <= output_shift_reg(0) when CGN_LSB_FIRST = 1 else output_shift_reg(CGN_NUM_D_BITS-1);

  handshake_valid : process(sck, SS_N_I)
  begin
    if SS_N_I = '1' then
      RX_DATA_VALID_O <= '0';
    elsif rising_edge(sck) then
      if edge_count = 1 then
        RX_DATA_VALID_O <= '1';
      else
        RX_DATA_VALID_O <= '0';
      end if;
    end if;
  end process handshake_valid;

  handshake_ack : process(sck, SS_N_I)
  begin
    if SS_N_I = '1' then
      TX_DATA_ACK_O   <= '0';
    elsif falling_edge(sck) then
      if edge_count = 0 then
        TX_DATA_ACK_O   <= '1';
      else
        TX_DATA_ACK_O   <= '0';
      end if;
    end if;
  end process handshake_ack;

  sck <= SCK_I when CGN_CPOL = CGN_CPHA else not(SCK_I);

  rx_logic : process(sck, RST_ASYNC_I, SS_N_I)
  begin
    if RST_ASYNC_I = '1' or SS_N_I = '1' then
      edge_count <= CONV_STD_LOGIC_VECTOR(CGN_NUM_D_BITS, 6);
    elsif rising_edge(sck) then
      -- shift input shift register
      if CGN_LSB_FIRST =1 then
        -- LSB first shifting
        input_shift_reg <= SIMO_I & input_shift_reg(CGN_NUM_D_BITS-1 downto 1);
      else
        -- MSB first shifting
        input_shift_reg <= input_shift_reg(CGN_NUM_D_BITS-2 downto 0) & SIMO_I;
      end if;
      -- count clock edge
      if edge_count > 0 then
        edge_count <= edge_count-1;
      else
        edge_count <= CONV_STD_LOGIC_VECTOR(CGN_NUM_D_BITS-1, 6);
      end if;
    end if;
  end process rx_logic;

  tx_logic : process(sck, RST_ASYNC_I, SS_N_I)
  begin
    if RST_ASYNC_I = '1' then
      output_shift_reg <= (others=>'0');
    elsif falling_edge(sck) then
      if edge_count = 0 then
        output_shift_reg <= TX_DATA_I(CGN_NUM_D_BITS-1 downto 0);
      else
        if CGN_CPHA = 1 and edge_count = CGN_NUM_D_BITS then
          output_shift_reg <= output_shift_reg;
        else
          -- shift output shift register
          if CGN_LSB_FIRST = 1 then
            -- LSB first shifting
            output_shift_reg <= '0' & output_shift_reg(CGN_NUM_D_BITS-1 downto 1);
          else
            -- MSB first shifting
            output_shift_reg <= output_shift_reg(CGN_NUM_D_BITS-2 downto 0) & '0';
          end if;
        end if;
      end if;
    end if;
  end process tx_logic;

end behavioral_async;
