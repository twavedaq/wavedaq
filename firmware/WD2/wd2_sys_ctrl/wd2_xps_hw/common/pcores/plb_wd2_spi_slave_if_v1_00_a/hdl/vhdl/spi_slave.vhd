---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic SPI Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.07.2014 14:58:31
--
--  Description :  Generic implementation of an SPI Slave Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity spi_slave is
  generic (
    CGN_NUM_D_BITS     : integer := 8; -- allowable values: 1,2,3,...,32
    CGN_CPOL           : integer := 0; -- 0=positive _/"\_ , 1=negative "\_/"
    CGN_CPHA           : integer := 0; -- 0=first edge centered on data, 1=second edge centered on data
    CGN_LSB_FIRST      : integer := 0;
    CGN_HAS_RX_FIFO    : integer := 1;
    CGN_HAS_TX_FIFO    : integer := 1
  );
  port (
    -- SPI interface
    SOMI_O             : out std_logic;
    SIMO_I             : in  std_logic;
    SCK_I              : in  std_logic;
    SS_N_I             : in  std_logic;

    -- Status
    SLAVE_SEL_O        : out std_logic;
    -- FIFO control and status
    RX_FIFO_RESET_I    : in  std_logic;
    RX_FIFO_AEMPTY_O   : out std_logic;
    RX_FIFO_EMPTY_O    : out std_logic;
    RX_FIFO_RE_I       : in  std_logic;
    RX_FIFO_DATA_O     : out std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
    TX_FIFO_RESET_I    : in  std_logic;
    TX_FIFO_AFULL_O    : out std_logic;
    TX_FIFO_FULL_O     : out std_logic;
    TX_FIFO_WE_I       : in  std_logic;
    TX_FIFO_DATA_I     : in  std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
    -- Register Interface
    RB_ADDR_O          : out std_logic_vector(31 downto 0);
    RB_BE_O            : out std_logic_vector( 3 downto 0);
    RB_REG_SEL_O       : out std_logic_vector( 1 downto 0);
    RB_DATA_O          : out std_logic_vector(31 downto 0);
    RB_DATA_I          : in  std_logic_vector(31 downto 0);
    RB_REQ_O           : out std_logic;
    RB_RD_ACK_I        : in  std_logic;
    RB_WR_ACK_I        : in  std_logic;
    RB_RNW_O           : out std_logic;
    -- special commands
    CLEAR_O            : out std_logic;
    SYNC_O             : out std_logic;
    RECFG_O            : out std_logic;

    RST_ASYNC_I        : in  std_logic;
    CLK_I              : in  std_logic
  );
end spi_slave;



architecture behavioral_async of spi_slave is

  constant C_NUM_BYTES       : integer := (CGN_NUM_D_BITS+7)/8;
  constant C_CMD_STX_ASCII   : std_logic_vector(7 downto 0) := X"02";
  constant C_CMD_LF_ASCII    : std_logic_vector(7 downto 0) := X"0A";
  constant C_CMD_CR_ASCII    : std_logic_vector(7 downto 0) := X"0D";
  constant C_CMD_READ_ID     : std_logic_vector(7 downto 0) := X"30";
  constant C_CMD_READ        : std_logic_vector(3 downto 0) := X"2";
  constant C_CMD_WRITE       : std_logic_vector(3 downto 0) := X"1";

  attribute fsm_encoding : string;
  attribute keep         : string;

  -- state of the binary cmd ctrl state machine
  type type_state is (s_rcv_cmd, s_rcv_addr, s_rcv_first_wr_data, s_rcv_nr_of_words, s_tx_bin_data, s_rx_ascii_data);
  signal state        : type_state;
  attribute fsm_encoding of state : signal is "one-hot";

  -- internal rx fifo signals
  signal rx_data_valid      : std_logic := '0';
  signal rx_data_valid_s2   : std_logic := '0';
  signal rx_data_valid_s3   : std_logic := '0';
  signal rx_data_valid_rise : std_logic := '0';
  signal rx_wr_en           : std_logic := '0';
  signal rx_data            : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');

  -- internal tx fifo signals
  signal bin_tx_en          : std_logic := '0';
  signal ascii_tx_en        : std_logic := '0';
  signal tx_data_ack        : std_logic := '0';
  signal tx_data_ack_s2     : std_logic := '0';
  signal tx_data_ack_s3     : std_logic := '0';
  signal tx_data_ack_rise   : std_logic := '0';
  signal tx_data            : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');
  signal tx_fifo_data       : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');
  signal tx_fifo_empty      : std_logic := '0';
  signal tx_rd_en           : std_logic := '0';
  signal rb_data            : std_logic_vector((C_NUM_BYTES*8)-1 downto 0) := (others => '0');

  signal cmd                : std_logic_vector( 7 downto 0) := (others=>'0');
  signal addr               : std_logic_vector(31 downto 0) := (others=>'0');
  signal nr_of_words        : std_logic_vector( 7 downto 0) := (others=>'0');
  signal addr_count         : std_logic_vector( 2 downto 0) := (others=>'0');
  signal tx_byte_sel        : std_logic_vector( 1 downto 0) := (others=>'0');
  signal process_data       : std_logic := '0';
  signal run_cmd            : std_logic := '0';

  signal slave_sel          : std_logic := '0';
  signal slave_sel_s1       : std_logic := '0';
  signal slave_sel_s2       : std_logic := '0';
  signal slave_sel_s3       : std_logic := '0';
  signal deselect           : std_logic := '0';

  signal uc_data_en         : std_logic := '0';

  component spi_if is
    generic (
      CGN_NUM_D_BITS     : integer := 8; -- allowable values: 1,2,3,...,32
      CGN_CPOL           : integer := 0; -- 0=positive _/"\_ , 1=negative "\_/"
      CGN_CPHA           : integer := 0; -- 0=first edge centered on data, 1=second edge centered on data
      CGN_LSB_FIRST      : integer := 0
    );
    port (
      -- SPI interface
      SOMI_O             : out std_logic;
      SIMO_I             : in  std_logic;
      SCK_I              : in  std_logic;
      SS_N_I             : in  std_logic;

      -- Status
      SLAVE_SEL_O        : out std_logic;
      RX_DATA_O          : out std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
      RX_DATA_VALID_O    : out std_logic;
      TX_DATA_I          : in  std_logic_vector((((CGN_NUM_D_BITS+7)/8)*8)-1 downto 0);
      TX_DATA_ACK_O      : out std_logic;

      RST_ASYNC_I            : in  std_logic
    );
  end component;

  component spi_fifo
    generic (
      CGN_NUM_BYTES : integer := 1; -- allowable values: 1,2,3,4
      CGN_HAS_FIFO  : integer := 1
    );
    port (
      WRITE_DATA_I   : in  std_logic_vector((C_NUM_BYTES*8)-1 downto 0);
      WRITE_EN_I     : in  std_logic;
      READ_DATA_O    : out std_logic_vector((C_NUM_BYTES*8)-1 downto 0);
      READ_EN_I      : in  std_logic;
      FULL_O         : out std_logic;
      ALMOST_FULL_O  : out std_logic;
      ALMOST_EMPTY_O : out std_logic;
      EMPTY_O        : out std_logic;
      RESET_I        : in  std_logic;
      CLK_I          : in  std_logic
    );
  end component;

  component spi_spec_cmd_decoder
    generic
    (
      CGN_PULSE_LENGTH : integer := 1
    );
    port
    (
      CMD_I       : in  std_logic_vector(7 downto 0);
      RUN_CMD_I   : in  std_logic;
      CLEAR_O     : out std_logic;
      SYNC_O      : out std_logic;
      RECFG_O     : out std_logic;
      RST_I       : in  std_logic;
      CLK_I       : in  std_logic
    );
  end component;

begin

  spi_if_inst : spi_if
    generic map
    (
      CGN_NUM_D_BITS => CGN_NUM_D_BITS,
      CGN_CPOL       => CGN_CPOL,
      CGN_CPHA       => CGN_CPHA,
      CGN_LSB_FIRST  => CGN_LSB_FIRST
    )
    port map
    (
      SOMI_O          => SOMI_O,
      SIMO_I          => SIMO_I,
      SCK_I           => SCK_I,
      SS_N_I          => SS_N_I,
      SLAVE_SEL_O     => slave_sel,
      RX_DATA_O       => rx_data,
      RX_DATA_VALID_O => rx_data_valid,
      TX_DATA_I       => tx_data,
      TX_DATA_ACK_O   => tx_data_ack,
      RST_ASYNC_I     => RST_ASYNC_I
    );
  SLAVE_SEL_O <= slave_sel;

  rx_fifo_or_reg : spi_fifo
  generic map(
    CGN_NUM_BYTES => C_NUM_BYTES,
    CGN_HAS_FIFO  => CGN_HAS_RX_FIFO
  )
  port map(
    WRITE_DATA_I   => rx_data,
    WRITE_EN_I     => rx_wr_en,
    READ_DATA_O    => RX_FIFO_DATA_O,
    READ_EN_I      => RX_FIFO_RE_I,
    FULL_O         => open,
    ALMOST_FULL_O  => open,
    ALMOST_EMPTY_O => RX_FIFO_AEMPTY_O,
    EMPTY_O        => RX_FIFO_EMPTY_O,
    RESET_I        => RX_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  rx_wr_en <= uc_data_en and rx_data_valid_rise;

  tx_fifo_or_reg : spi_fifo
  generic map(
    CGN_NUM_BYTES => C_NUM_BYTES,
    CGN_HAS_FIFO  => CGN_HAS_TX_FIFO
  )
  port map(
    WRITE_DATA_I   => TX_FIFO_DATA_I,
    WRITE_EN_I     => TX_FIFO_WE_I,
    READ_DATA_O    => tx_fifo_data,
    READ_EN_I      => tx_rd_en,
    FULL_O         => TX_FIFO_FULL_O,
    ALMOST_FULL_O  => TX_FIFO_AFULL_O,
    ALMOST_EMPTY_O => open,
    EMPTY_O        => tx_fifo_empty,
    RESET_I        => TX_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  tx_rd_en <= ascii_tx_en and tx_data_ack_rise and not tx_fifo_empty;

  cdc_sync_sys_to_drs_ctrl: cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  2,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I    => SCK_I,
    CLK_B_I    => CLK_I,
    PORT_A_I(0) => rx_data_valid,
    PORT_A_I(1) => tx_data_ack,
    PORT_B_O(0) => rx_data_valid_s2,
    PORT_B_O(1) => tx_data_ack_s2
  );

  edge_detectors : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      rx_data_valid_s3 <= rx_data_valid_s2;
      tx_data_ack_s3   <= tx_data_ack_s2;
      if rx_data_valid_s3 = '0' and rx_data_valid_s2 = '1' then
        rx_data_valid_rise <= '1';
      else
        rx_data_valid_rise <= '0';
      end if;
      if tx_data_ack_s3 = '0' and tx_data_ack_s2 = '1' then
        tx_data_ack_rise <= '1';
      else
        tx_data_ack_rise <= '0';
      end if;
    end if;
  end process edge_detectors;

  -- ASCII/Binary tx data MUX
  tx_data <= tx_fifo_data when (ascii_tx_en = '1' and tx_fifo_empty = '0')
        else rb_data      when (bin_tx_en = '1')
        else (others=>'0');

  -- Register bank data store
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RB_RD_ACK_I = '1' or RB_WR_ACK_I = '1' then
        -- use big endian notation as microblaze PLB
        case tx_byte_sel is
          when "00" =>
            rb_data <= RB_DATA_I(31 downto 24);
          when "01" =>
            rb_data <= RB_DATA_I(23 downto 16);
          when "10" =>
            rb_data <= RB_DATA_I(15 downto  8);
          when "11" =>
            rb_data <= RB_DATA_I( 7 downto  0);
          when others =>
            null;
        end case;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      slave_sel_s1 <= slave_sel;
      slave_sel_s2 <= slave_sel_s1;
      slave_sel_s3 <= slave_sel_s2;
    end if;
  end process;
  -- detect rising edge of the select signal
  deselect <= slave_sel_s2 and not slave_sel_s3;

  binary_cmd_ctrl_fsm_state : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if deselect = '1' then
        state <= s_rcv_cmd;
      else
        case state is
          when s_rcv_cmd =>
            if rx_data_valid_rise = '1' then
              if rx_data = C_CMD_STX_ASCII then
                state <= s_rx_ascii_data;
              elsif rx_data(7 downto 4) = C_CMD_READ or rx_data(7 downto 4) = C_CMD_WRITE then
                state <= s_rcv_addr;
              elsif rx_data = C_CMD_READ_ID then
                state <= s_tx_bin_data;
              end if;
            end if;
          when s_rcv_addr =>
            if rx_data_valid_rise = '1' and addr_count = "001" then
              if cmd(7 downto 4) = C_CMD_READ then
                state <= s_rcv_nr_of_words;
              elsif cmd(7 downto 4) = C_CMD_WRITE then
                state <= s_rcv_first_wr_data;
              else
                state <= s_tx_bin_data;
              end if;
            end if;
          when s_rcv_first_wr_data =>
            if rx_data_valid_rise = '1' then
              state <= s_tx_bin_data;
            end if;
          when s_rcv_nr_of_words =>
            if rx_data_valid_rise = '1' then
              state <= s_tx_bin_data;
            end if;
          when s_rx_ascii_data =>
            if rx_data_valid_rise = '1' and (rx_data = C_CMD_CR_ASCII or rx_data = C_CMD_LF_ASCII) then
              state <= s_rcv_cmd;
            else
              state <= s_rx_ascii_data;
            end if;
          when s_tx_bin_data =>
            state <= s_tx_bin_data;
          when others =>
            state <= s_rcv_cmd;
        end case;
      end if;
    end if;
  end process binary_cmd_ctrl_fsm_state;

  binary_cmd_ctrl_fsm_output : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_ASYNC_I = '1' then
        RB_REQ_O     <= '0';
        RB_RNW_O     <= '0';
        addr_count   <= (others=>'0');
        addr         <= (others=>'0');
        nr_of_words  <= (others=>'0');
        uc_data_en   <= '0';
        process_data <= '1';
        run_cmd      <= '0';
      else
        -- defaults -- --
        uc_data_en  <= '0';
        bin_tx_en   <= '0';
        ascii_tx_en <= '0';
        RB_REQ_O    <= '0';
        run_cmd     <= '0';
        -- -- -- -- -- --
        process_data <= rx_data_valid_rise;
        case state is
          when s_rcv_cmd =>
            RB_RNW_O    <= '0';
            ascii_tx_en <= '1';
            if rx_data_valid_rise = '1' then
              cmd        <= rx_data;
              run_cmd    <= '1';
              addr       <= (others=>'0');
              addr_count <= rx_data(2 downto 0);
            end if;
          when s_rcv_addr =>
            if rx_data_valid_rise = '1' then
              addr_count         <= addr_count - 1;
              addr( 7 downto  0) <= rx_data;
              addr(15 downto  8) <= addr( 7 downto  0);
              addr(23 downto 16) <= addr(15 downto  8);
              addr(31 downto 24) <= addr(23 downto 16);
              addr(31 downto 30) <= (others=>'0'); -- reserved address space
            end if;
          when s_rcv_first_wr_data =>
            if rx_data_valid_rise = '1' then
              RB_DATA_O <= rx_data & rx_data & rx_data & rx_data;
            end if;
          when s_rcv_nr_of_words =>
            if rx_data_valid_rise = '1' then
              addr        <= addr + 1;
              nr_of_words <= rx_data;
            end if;
            if tx_data_ack_rise = '1' then
            --if cmd(7 downto 4) = C_CMD_READ then
            -- condition obsolet: state is only used for reading
              RB_REQ_O <= '1';
              RB_RNW_O <= '1';
            -- end if;
            elsif RB_RD_ACK_I = '1' then
              RB_RNW_O <= '0';
            end if;
          when s_rx_ascii_data =>
            uc_data_en <= '1';
          when s_tx_bin_data =>
            bin_tx_en <= '1';
            if rx_data_valid_rise = '1' then
              addr      <= addr + 1;
              RB_DATA_O <= rx_data & rx_data & rx_data & rx_data;
            end if;
            if process_data = '1' and cmd(7 downto 4) = C_CMD_WRITE then
                RB_REQ_O <= '1';
            elsif tx_data_ack_rise = '1' and cmd(7 downto 4) = C_CMD_READ then
                RB_REQ_O <= '1';
                RB_RNW_O <= '1';
            elsif RB_RD_ACK_I = '1' then
              RB_RNW_O <= '0';
            end if;
          when others =>
            null;
        end case;
      end if;
    end if;
  end process binary_cmd_ctrl_fsm_output;

  tx_byte_sel  <= addr( 1 downto  0);
  RB_ADDR_O    <= X"0000" & addr(15 downto  0);
  -- access control-regs when addr(31 downto 12) /= X"00000" else status-regs:
  RB_REG_SEL_O <= "01" when addr(31 downto 12) /= X"00000" else "10";

  process(addr)
  begin
    -- use big endian notation as microblaze PLB
    case addr(1 downto 0) is
      when "00" => RB_BE_O <= "1000";
      when "01" => RB_BE_O <= "0100";
      when "10" => RB_BE_O <= "0010";
      when "11" => RB_BE_O <= "0001";
      when others => null;
    end case;
  end process;

  spec_cmd_dec_inst : spi_spec_cmd_decoder
    generic map
    (
      CGN_PULSE_LENGTH => 1
    )
    port map
    (
      CMD_I     => cmd,
      RUN_CMD_I => run_cmd,
      CLEAR_O   => CLEAR_O,
      SYNC_O    => SYNC_O,
      RECFG_O   => RECFG_O,
      RST_I     => RST_ASYNC_I,
      CLK_I     => CLK_I
    );

end behavioral_async;
