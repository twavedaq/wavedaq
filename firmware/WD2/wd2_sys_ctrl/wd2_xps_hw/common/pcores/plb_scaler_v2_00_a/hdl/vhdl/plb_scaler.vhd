------------------------------------------------------------------------------
-- plb_scaler.vhd - entity/architecture pair
------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library plb_scaler_v2_00_a;
use plb_scaler_v2_00_a.ipif_user_cfg.all;
use plb_scaler_v2_00_a.counter_buffer;
use plb_scaler_v2_00_a.ipif_plb;

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------

entity plb_scaler is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    CGN_NR_OF_COUNTER_VALUES       : integer := 19;
    CGN_COUNTER_VALUE_WIDTH        : integer := 64;
    CGN_DAQ_CLK_PERIOD_NS          : real    := 12.5;

    -- Bus protocol parameters -- DO NOT EDIT --
    C_BASEADDR                     : std_logic_vector     := X"FFFFFFFF";
    C_HIGHADDR                     : std_logic_vector     := X"00000000";
    C_SPLB_AWIDTH                  : integer              := 32;
    C_SPLB_DWIDTH                  : integer              := 128;
    C_SPLB_NUM_MASTERS             : integer              := 8;
    C_SPLB_MID_WIDTH               : integer              := 3;
    C_SPLB_NATIVE_DWIDTH           : integer              := 32;
    C_SPLB_P2P                     : integer              := 0;
    C_SPLB_SUPPORT_BURSTS          : integer              := 0;
    C_SPLB_SMALLEST_MASTER         : integer              := 32;
    C_SPLB_CLK_PERIOD_PS           : integer              := 10000;
    C_INCLUDE_DPHASE_TIMER         : integer              := 1;
    C_FAMILY                       : string               := "spartan6";
    C_MEM0_BASEADDR                : std_logic_vector     := X"FFFFFFFF";
    C_MEM0_HIGHADDR                : std_logic_vector     := X"00000000"
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    COUNTER_DATA_I                 : in  std_logic_vector(CGN_NR_OF_COUNTER_VALUES*CGN_COUNTER_VALUE_WIDTH-1 downto 0);
    DAQ_CLK_I                      : in  std_logic;
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- Bus protocol ports -- DO NOT EDIT --
    SPLB_Clk                       : in  std_logic;
    SPLB_Rst                       : in  std_logic;
    PLB_ABus                       : in  std_logic_vector(0 to 31);
    PLB_UABus                      : in  std_logic_vector(0 to 31);
    PLB_PAValid                    : in  std_logic;
    PLB_SAValid                    : in  std_logic;
    PLB_rdPrim                     : in  std_logic;
    PLB_wrPrim                     : in  std_logic;
    PLB_masterID                   : in  std_logic_vector(0 to C_SPLB_MID_WIDTH-1);
    PLB_abort                      : in  std_logic;
    PLB_busLock                    : in  std_logic;
    PLB_RNW                        : in  std_logic;
    PLB_BE                         : in  std_logic_vector(0 to C_SPLB_DWIDTH/8-1);
    PLB_MSize                      : in  std_logic_vector(0 to 1);
    PLB_size                       : in  std_logic_vector(0 to 3);
    PLB_type                       : in  std_logic_vector(0 to 2);
    PLB_lockErr                    : in  std_logic;
    PLB_wrDBus                     : in  std_logic_vector(0 to C_SPLB_DWIDTH-1);
    PLB_wrBurst                    : in  std_logic;
    PLB_rdBurst                    : in  std_logic;
    PLB_wrPendReq                  : in  std_logic;
    PLB_rdPendReq                  : in  std_logic;
    PLB_wrPendPri                  : in  std_logic_vector(0 to 1);
    PLB_rdPendPri                  : in  std_logic_vector(0 to 1);
    PLB_reqPri                     : in  std_logic_vector(0 to 1);
    PLB_TAttribute                 : in  std_logic_vector(0 to 15);
    Sl_addrAck                     : out std_logic;
    Sl_SSize                       : out std_logic_vector(0 to 1);
    Sl_wait                        : out std_logic;
    Sl_rearbitrate                 : out std_logic;
    Sl_wrDAck                      : out std_logic;
    Sl_wrComp                      : out std_logic;
    Sl_wrBTerm                     : out std_logic;
    Sl_rdDBus                      : out std_logic_vector(0 to C_SPLB_DWIDTH-1);
    Sl_rdWdAddr                    : out std_logic_vector(0 to 3);
    Sl_rdDAck                      : out std_logic;
    Sl_rdComp                      : out std_logic;
    Sl_rdBTerm                     : out std_logic;
    Sl_MBusy                       : out std_logic_vector(0 to C_SPLB_NUM_MASTERS-1);
    Sl_MWrErr                      : out std_logic_vector(0 to C_SPLB_NUM_MASTERS-1);
    Sl_MRdErr                      : out std_logic_vector(0 to C_SPLB_NUM_MASTERS-1);
    Sl_MIRQ                        : out std_logic_vector(0 to C_SPLB_NUM_MASTERS-1)
  );

  attribute SIGIS : string;
  attribute SIGIS of SPLB_Clk      : signal is "CLK";
  attribute SIGIS of SPLB_Rst      : signal is "RST";

end entity plb_scaler;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of plb_scaler is

  constant C_BUF_SEL_WIDTH : integer := 2;

  ---------------------------------------------------------------------------
  -- interface signal definitions
  ---------------------------------------------------------------------------
  
  signal user_to_ipif     : user_to_ipif_type;
  signal ipif_to_user     : ipif_to_user_type;

  ---------------------------------------------------------------------------
  -- register signal definitions
  ---------------------------------------------------------------------------

  -- local register signals for easier access
  -- ( ISE has problems with alias in some cases,
  --   therefore they are not used here )

  signal reg_ctrl            : std_logic_vector(C_REG_WIDTH-1 downto 0);
  signal reg_status          : std_logic_vector(C_REG_WIDTH-1 downto 0);
  
  signal reg_ctrl_buf_lock   : std_logic;
  signal reg_status_last_buf : std_logic_vector(C_BUF_SEL_WIDTH-1 downto 0);


  ---------------------------------------------------------------------------
  -- user logic signals
  ---------------------------------------------------------------------------

  signal mem_address      : std_logic_vector( C_MEM_DEPTH-1 downto 0);
  signal mem_data         : std_logic_vector(C_SLV_DWIDTH-1 downto 0);
  signal mem_wr_en        : std_logic;

begin

  ------------------------------------------------------
  -- register definition is done in ipif_user_cfg.vhdl
  ------------------------------------------------------

  -- control register
  reg_ctrl             <= ipif_to_user.reg.data(C_REG_CTRL);
                       
  reg_ctrl_buf_lock    <= reg_ctrl(0);
  
  -- status register
  
  reg_status(C_BUF_SEL_WIDTH-1 downto 0)           <= reg_status_last_buf;
  reg_status(C_REG_WIDTH-1 downto C_BUF_SEL_WIDTH) <= (others=>'0');
  
  user_to_ipif.reg.data(C_REG_STATUS)              <= reg_status;

  ------------------------------------------------------
  -- memory definition is done in ipif_user_cfg.vhdl
  ------------------------------------------------------

  user_to_ipif.mem.clk    <= DAQ_CLK_I;
  user_to_ipif.mem.addr   <= mem_address;
  user_to_ipif.mem.enable <= '1';
  user_to_ipif.mem.wr_en  <= mem_wr_en;
  user_to_ipif.mem.data   <= mem_data;

  ------------------------------------------------------------------------------
  -- user logic
  ------------------------------------------------------------------------------

  counter_buff_inst : entity plb_scaler_v2_00_a.counter_buffer
  generic map
  (
    CGN_MEM_DATA_WIDTH => C_SLV_DWIDTH,
    CGN_MEM_ADDR_WIDTH => C_MEM_DEPTH,
    CGN_NR_OF_VALUES   => CGN_NR_OF_COUNTER_VALUES,
    CGN_VALUE_WIDTH    => CGN_COUNTER_VALUE_WIDTH,
    CGN_BUF_SEL_WIDTH  => C_BUF_SEL_WIDTH,
    CGN_CLK_PERIOD_NS  => CGN_DAQ_CLK_PERIOD_NS 
  )
  port map
  (
    DATA_I          => COUNTER_DATA_I,
    BUFFER_LOCKED_I => reg_ctrl_buf_lock,
    MEM_DATA_O      => mem_data,
    MEM_ADDR_O      => mem_address,
    MEM_WR_EN_O     => mem_wr_en,
    LAST_BUFFER_O   => reg_status_last_buf,
    CLK_I           => DAQ_CLK_I
  );

  ------------------------------------------------------------------------------
  -- instantiate PLB IP Interface
  ------------------------------------------------------------------------------

  ipif_plb_inst : entity plb_scaler_v2_00_a.ipif_plb
    generic map
    (
      -- Bus protocol parameters, do not add to or delete
      C_BASEADDR                => C_BASEADDR,
      C_HIGHADDR                => C_HIGHADDR,
      C_SPLB_AWIDTH             => C_SPLB_AWIDTH,
      C_SPLB_DWIDTH             => C_SPLB_DWIDTH,
      C_SPLB_NUM_MASTERS        => C_SPLB_NUM_MASTERS,
      C_SPLB_MID_WIDTH          => C_SPLB_MID_WIDTH,
      C_SPLB_NATIVE_DWIDTH      => C_SPLB_NATIVE_DWIDTH,
      C_SPLB_P2P                => C_SPLB_P2P,
      C_SPLB_SUPPORT_BURSTS     => C_SPLB_SUPPORT_BURSTS,
      C_SPLB_SMALLEST_MASTER    => C_SPLB_SMALLEST_MASTER,
      C_SPLB_CLK_PERIOD_PS      => C_SPLB_CLK_PERIOD_PS,
      C_INCLUDE_DPHASE_TIMER    => C_INCLUDE_DPHASE_TIMER,
      C_FAMILY                  => C_FAMILY,
      C_MEM0_BASEADDR           => C_MEM0_BASEADDR,
      C_MEM0_HIGHADDR           => C_MEM0_HIGHADDR
    )
    port map
    (
      -- user ports for ip interface
      USER_TO_IPIF_I            => user_to_ipif,
      IPIF_TO_USER_O            => ipif_to_user,
      -- Bus protocol ports, do not add to or delete
      SPLB_Clk                  => SPLB_Clk,
      SPLB_Rst                  => SPLB_Rst,
      PLB_ABus                  => PLB_ABus,
      PLB_UABus                 => PLB_UABus,
      PLB_PAValid               => PLB_PAValid,
      PLB_SAValid               => PLB_SAValid,
      PLB_rdPrim                => PLB_rdPrim,
      PLB_wrPrim                => PLB_wrPrim,
      PLB_masterID              => PLB_masterID,
      PLB_abort                 => PLB_abort,
      PLB_busLock               => PLB_busLock,
      PLB_RNW                   => PLB_RNW,
      PLB_BE                    => PLB_BE,
      PLB_MSize                 => PLB_MSize,
      PLB_size                  => PLB_size,
      PLB_type                  => PLB_type,
      PLB_lockErr               => PLB_lockErr,
      PLB_wrDBus                => PLB_wrDBus,
      PLB_wrBurst               => PLB_wrBurst,
      PLB_rdBurst               => PLB_rdBurst,
      PLB_wrPendReq             => PLB_wrPendReq,
      PLB_rdPendReq             => PLB_rdPendReq,
      PLB_wrPendPri             => PLB_wrPendPri,
      PLB_rdPendPri             => PLB_rdPendPri,
      PLB_reqPri                => PLB_reqPri,
      PLB_TAttribute            => PLB_TAttribute,
      Sl_addrAck                => Sl_addrAck,
      Sl_SSize                  => Sl_SSize,
      Sl_wait                   => Sl_wait,
      Sl_rearbitrate            => Sl_rearbitrate,
      Sl_wrDAck                 => Sl_wrDAck,
      Sl_wrComp                 => Sl_wrComp,
      Sl_wrBTerm                => Sl_wrBTerm,
      Sl_rdDBus                 => Sl_rdDBus,
      Sl_rdWdAddr               => Sl_rdWdAddr,
      Sl_rdDAck                 => Sl_rdDAck,
      Sl_rdComp                 => Sl_rdComp,
      Sl_rdBTerm                => Sl_rdBTerm,
      Sl_MBusy                  => Sl_MBusy,
      Sl_MWrErr                 => Sl_MWrErr,
      Sl_MRdErr                 => Sl_MRdErr,
      Sl_MIRQ                   => Sl_MIRQ            
    );

end IMP;
