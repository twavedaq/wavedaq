--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : cdc_package.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  Clock Domain Crossing
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Package header
--------------------------------------------------------------------------------

package ipif_user_cfg is

  ---------------------------------------------------------------------------
  -- General Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_WIDTH         : integer := 32;
  constant  C_SLV_AWIDTH        : integer := 32;
  constant  C_SLV_DWIDTH        : integer := 32;

  ---------------------------------------------------------------------------
  -- Register Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_UNDEFINED     : std_logic_vector(C_REG_WIDTH-1 downto 0) := X"DEADBEEF";
  constant  C_DEFAULT_ZERO      : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_BIT_ALL        : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '1');
  constant  C_WR_BIT_NONE       : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_PULSE_NONE     : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  
  constant  C_WR_EXT_NO         : integer := 0;
  constant  C_WR_EXT_YES        : integer := 1;

  constant  C_REG_SELF          : integer := -1;
  constant  C_REGISTER_REG_READ : boolean := false;


     
  ---------------------------------------------------------------------------
  -- register description
  ---------------------------------------------------------------------------

  -- Define special register types, default is C_REG_RW, possible types:
  -- C_REG_RW  : Read / Write register
  -- C_REG_RO  : Read Only register
  -- C_REG_WR  : WRite register,               reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_SET : SET bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_CLR : CLR bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- set as record (<reg_type>, <reg_num>, <default>, <writable mask>),
  --   e.g. C_REG_CTRL_SET => (C_BIT_SET, C_REG_CTRL)
  --   <reg_num> only used for C_BIT_SET and C_BIT_CLR, otherwise set to C_REG_SELF 


  type  reg_type is (C_REG_RW, C_REG_RO, C_REG_WR, C_BIT_SET, C_BIT_CLR);

  type  reg_descr_record is
        record
          typ     : reg_type;
          reg     : integer;
          def     : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_msk  : std_logic_vector(C_REG_WIDTH-1 downto 0);
          pls_msk : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_ext  : integer;
        end record;

  type  reg_descr_type is array (natural range <>) of reg_descr_record;

  constant  C_REG_DESCR_DEFAULT : reg_descr_type(0 to 1) :=
  (        --  type         set/clr num       default           writable mask    write pulse only   external overwrite
    others => (C_REG_RW,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,   C_WR_EXT_NO)
  );


  ---------------------------------------------------------------------------
  -- User register definition
  ---------------------------------------------------------------------------

  -- register numbers  
  constant  C_REG_STATUS            : integer := 0;
  constant  C_REG_CTRL              : integer := 1;
  constant  C_REG_CTRL_SET          : integer := 2;
  constant  C_REG_CTRL_CLR          : integer := 3;
                                   
  constant  C_NUM_REG               : integer := 4;

-- not yet used
  constant  C_REG_CTRL_PULSE_MASK   : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');


  -- Register description 
  constant  C_REG_DESCR     : reg_descr_type(0 to C_NUM_REG-1) :=
  (                   --   type         set/clr num       default           writable mask    write pulse only        external write
    C_REG_STATUS       => (C_REG_RO,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_NONE,   C_WR_PULSE_NONE,        C_WR_EXT_NO ),
    C_REG_CTRL         => (C_REG_RW,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_REG_CTRL_PULSE_MASK,  C_WR_EXT_NO ),    
    C_REG_CTRL_SET     => (C_BIT_SET,   C_REG_CTRL,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,        C_WR_EXT_NO ),    
    C_REG_CTRL_CLR     => (C_BIT_CLR,   C_REG_CTRL,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,        C_WR_EXT_NO ),    
    others             => (C_REG_RW,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,        C_WR_EXT_NO )
  );

  ---------------------------------------------------------------------------
  -- register user interface signals
  ---------------------------------------------------------------------------

  type  reg_array is array (natural range <>) of std_logic_vector(C_REG_WIDTH-1 downto 0);

  type  user_to_reg_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_ext           :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  type  reg_to_user_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_pulse         :  std_logic_vector(0 to C_NUM_REG-1);
          rd_pulse         :  std_logic_vector(0 to C_NUM_REG-1);
        end record;


  -------------------------------------------------------------------------------------------
  -- User memory definition
  -------------------------------------------------------------------------------------------
    
  constant  C_MEM_DEPTH                  : integer              := 8;
  constant  C_NUM_MEM                    : integer              := 1;


  type  user_to_mem_type is
        record
          clk     : std_logic;
          enable  : std_logic;
          wr_en   : std_logic;
          addr    : std_logic_vector(C_MEM_DEPTH-1 downto 0);
          data    : std_logic_vector(C_SLV_DWIDTH-1 downto 0);
        end record;


  type  mem_to_user_type is
        record
          data    : std_logic_vector(C_SLV_DWIDTH-1 downto 0);
        end record;


  ---------------------------------------------------------------------------
  -- User interface signals to top level
  ---------------------------------------------------------------------------

  constant  C_NUM_CS : integer  := (1 + C_NUM_MEM);
   
  type  user_to_ipif_type is
        record
          reg    : user_to_reg_type;
          mem    : user_to_mem_type;
        end record;


  type  ipif_to_user_type is
        record
          reg    : reg_to_user_type;
          mem    : mem_to_user_type;
        end record;


--------------------------------------------------------------------------------
end;


--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body ipif_user_cfg is

end package body;
