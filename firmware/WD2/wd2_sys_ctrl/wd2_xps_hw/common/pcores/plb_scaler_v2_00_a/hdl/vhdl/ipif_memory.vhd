--------------------------------------------------------------------------------
--  Paul Scherrer Institut (PSI)
--------------------------------------------------------------------------------
--
--  Generic IPIF register file
--
--  Author  :  tg32
--  Created :  2018-02-02
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Library section
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library plb_scaler_v2_00_a;
use plb_scaler_v2_00_a.ipif_user_cfg.all;

library psi_3205_v1_00_a;
--use psi_3205_v1_00_a.all;
use psi_3205_v1_00_a.ram_sdp;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------


entity ipif_memory is
  port
  (
    ------------------------------------------------------------------------
    -- user ports
    ------------------------------------------------------------------------
    USER_TO_MEM_I      : in  user_to_mem_type;
    MEM_TO_USER_O      : out mem_to_user_type;

    ------------------------------------------------------------------------
    -- IPIF ports
    ------------------------------------------------------------------------
    Bus2IP_Clk         : in  std_logic;
    Bus2IP_Reset       : in  std_logic;
    Bus2IP_Data        : in  std_logic_vector(C_SLV_DWIDTH-1   downto 0);
    Bus2IP_BE          : in  std_logic_vector(C_SLV_DWIDTH/8-1 downto 0);
    Bus2IP_Addr        : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS          : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW         : in  std_logic;
    IP2Bus_Data        : out std_logic_vector(C_SLV_DWIDTH-1   downto 0);
    IP2Bus_RdAck       : out std_logic;
    IP2Bus_WrAck       : out std_logic;
    IP2Bus_Error       : out std_logic
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity ipif_memory;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ipif_memory is

  ------------------------------------------
  -- Signals for user logic memory space example
  ------------------------------------------
  type   DO_TYPE is array (0 to C_NUM_MEM-1) of std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_data_out                   : DO_TYPE;
  
  signal mem_address                    : std_logic_vector(C_MEM_DEPTH-1 downto 0);
  --signal mem_select                     : std_logic_vector(0 to 1);
  signal mem_select                     : std_logic_vector(0 to 0);
  signal mem_read_enable                : std_logic;
  signal mem_read_enable_dly1           : std_logic;
  signal mem_read_req                   : std_logic;
  signal mem_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal mem_read_ack_dly1              : std_logic;
  signal mem_read_ack                   : std_logic;
  signal mem_write_ack                  : std_logic;

  signal bram_wren_0 : std_logic;
  --signal bram_wren_1 : std_logic;
  
  
begin


  --mem_select      <= Bus2IP_CS(0 to 1);
  --mem_read_enable <= ( Bus2IP_CS(0) or Bus2IP_CS(1) ) and Bus2IP_RNW;
  --mem_read_ack    <= mem_read_ack_dly1;
  --mem_write_ack   <= ( Bus2IP_CS(0) or Bus2IP_CS(1) ) and not(Bus2IP_RNW);
  --mem_address     <= Bus2IP_Addr(C_SLV_AWIDTH-11 to C_SLV_AWIDTH-3);
  mem_select      <= Bus2IP_CS(0 to 0);
  mem_read_enable <= ( Bus2IP_CS(0) ) and Bus2IP_RNW;
  mem_read_ack    <= mem_read_ack_dly1;
  mem_write_ack   <= ( Bus2IP_CS(0) ) and not(Bus2IP_RNW);
  mem_address     <= Bus2IP_Addr(C_SLV_AWIDTH-10 to C_SLV_AWIDTH-3);

  -- implement single clock wide read request
  mem_read_req    <= mem_read_enable and not(mem_read_enable_dly1);
  BRAM_RD_REQ_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_enable_dly1 <= '0';
      else
        mem_read_enable_dly1 <= mem_read_enable;
      end if;
    end if;

  end process BRAM_RD_REQ_PROC;

  -- this process generates the read acknowledge 1 clock after read enable
  -- is presented to the BRAM block. The BRAM block has a 1 clock delay
  -- from read enable to data out.
  BRAM_RD_ACK_PROC : process( Bus2IP_Clk ) is
  begin

    if ( Bus2IP_Clk'event and Bus2IP_Clk = '1' ) then
      if ( Bus2IP_Reset = '1' ) then
        mem_read_ack_dly1 <= '0';
      else
        mem_read_ack_dly1 <= mem_read_req;
      end if;
    end if;

  end process BRAM_RD_ACK_PROC;


  -- implement Block RAM(s)
  bram_wren_0 <= not(Bus2IP_RNW) and Bus2IP_CS(0);
  --bram_wren_1 <= not(Bus2IP_RNW) and Bus2IP_CS(1);


-- Byte Enable ignored, just do 32bit access

  ram_sdp_inst :entity psi_3205_v1_00_a.ram_sdp
  generic map
  (
    CGN_ADDR_WIDTH => C_MEM_DEPTH,
    CGN_DATA_WIDTH => C_SLV_DWIDTH,
    CGN_RAM_STYLE  => "block"  -- {auto|block|distributed|pipe_distributed|block_power1|block_power2}
  )
  port map
  (
    PA_CLK_I   => USER_TO_MEM_I.clk,
    PA_ADDR_I  => USER_TO_MEM_I.addr,
    PA_DATA_I  => USER_TO_MEM_I.data,
    PA_WR_EN_I => USER_TO_MEM_I.wr_en,

    PB_CLK_I   => Bus2IP_Clk,
    PB_ADDR_I  => mem_address,
    PB_RD_EN_I => '1',
    PB_DATA_O  => mem_data_out(0)
  );

  -- implement Block RAM read mux
  MEM_IP2BUS_DATA_PROC : process( mem_data_out, mem_select ) is
  begin

    case mem_select is
      --when "10" => mem_ip2bus_data <= mem_data_out(0);
      --when "01" => mem_ip2bus_data <= mem_data_out(1);
      when "1" => mem_ip2bus_data <= mem_data_out(0);
      when others => mem_ip2bus_data <= (others => '0');
    end case;

  end process MEM_IP2BUS_DATA_PROC;

  ---------------

  IP2Bus_Data    <= mem_ip2bus_data;
  IP2Bus_RdAck   <= mem_read_ack; 
  IP2Bus_WrAck   <= mem_write_ack;
  IP2Bus_Error   <= '0';


end behavioral;
