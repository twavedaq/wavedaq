--------------------------------------------------------------------------------
--  Paul Scherrer Institut (PSI)
--------------------------------------------------------------------------------
--
--  Generic IPIF register file
--
--  Author  :  tg32
--  Created :  2018-02-02
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
-- Library section
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library plb_scaler_v2_00_a;
use plb_scaler_v2_00_a.ipif_user_cfg.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
--------------------------------------------------------------------------------

entity ipif_register is
  port
  (
    ------------------------------------------------------------------------
    -- Register ports
    ------------------------------------------------------------------------
    -- User ports added here
    USER_TO_REG_I      : in  user_to_reg_type;
    REG_TO_USER_O      : out reg_to_user_type;

    ------------------------------------------------------------------------
    -- IPIF ports
    ------------------------------------------------------------------------
    Bus2IP_Clk         : in  std_logic;
    Bus2IP_Reset       : in  std_logic;
    Bus2IP_Data        : in  std_logic_vector(C_SLV_DWIDTH-1   downto 0);
    Bus2IP_BE          : in  std_logic_vector(C_SLV_DWIDTH/8-1 downto 0);
    Bus2IP_RdCE        : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE        : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data        : out std_logic_vector(C_SLV_DWIDTH-1   downto 0);
    IP2Bus_RdAck       : out std_logic;
    IP2Bus_WrAck       : out std_logic;
    IP2Bus_Error       : out std_logic
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity ipif_register;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ipif_register is

  ---------------------------------------------------------------------------
  -- constant definitions
  ---------------------------------------------------------------------------
  constant  C_REG_SEL_NONE  : std_logic_vector(0 to C_NUM_REG-1) := (others => '0');

  ---------------------------------------------------------------------------
  -- signal definitions
  ---------------------------------------------------------------------------
  
  signal   slv_ip2bus_data  : std_logic_vector(C_SLV_DWIDTH-1 downto 0);
  signal   slv_rd_ack       : std_logic := '0';
  signal   slv_wr_ack       : std_logic := '0';
  
  signal   reg_rd           : reg_array(0 to C_NUM_REG-1) := (others => C_REG_UNDEFINED);
  signal   reg_wr           : reg_array(0 to C_NUM_REG-1) := (others => (others => '0'));
  
  signal   wr_pulse         : std_logic_vector(0 to C_NUM_REG-1) := (others => '0');
  signal   rd_pulse         : std_logic_vector(0 to C_NUM_REG-1) := (others => '0');

begin

  ---------------------------------------------------------------------------
  -- Register read
  ---------------------------------------------------------------------------
  
  reg_rd_proc: process(Bus2IP_RdCE, reg_rd) is
  begin
    slv_ip2bus_data             <= (others => '0');
    for reg_idx in 0 to C_NUM_REG - 1 loop
      if (Bus2IP_RdCE(reg_idx) = '1') then
        slv_ip2bus_data       <= reg_rd(reg_idx);
      end if;
    end loop;
  end process reg_rd_proc;


  gen_rd_pulse_reg:  if (C_REGISTER_REG_READ = true) generate
  begin 
    rd_pulse_proc: process(Bus2IP_Clk) is
    begin
      if rising_edge(Bus2IP_Clk) then
        rd_pulse <= Bus2IP_RdCE;         
      end if;
    end process;
  end generate;

  gen_rd_pulse_noreg: if (C_REGISTER_REG_READ = false) generate
  begin 
    rd_pulse <= Bus2IP_RdCE;
  end generate;

  slv_rd_ack <= '1' when (Bus2IP_RdCE /= C_REG_SEL_NONE) else '0';

  ---------------------------------------------------------------------------
  -- Register write
  ---------------------------------------------------------------------------
  
  reg_wr_proc: process(Bus2IP_Clk) is
  begin
    if rising_edge(Bus2IP_Clk) then
    
      reg_wr_gen: for reg_idx in 0 to C_NUM_REG - 1 loop
        wr_pulse(reg_idx) <= '0';
        -- clear register pulse bits
        reg_wr(reg_idx)      <= reg_wr(reg_idx) and (not C_REG_DESCR(reg_idx).pls_msk);

        if (C_REG_DESCR(reg_idx).typ /= C_REG_RO) then
          if (Bus2IP_Reset = '1') then
            if ((C_REG_DESCR(reg_idx).typ = C_REG_RW) or (C_REG_DESCR(reg_idx).typ = C_REG_WR)) then
              reg_wr(reg_idx) <= C_REG_DESCR(reg_idx).def;
            end if;

          elsif (Bus2IP_WrCE(reg_idx) = '1') then

              for byte_idx in (C_SLV_DWIDTH/8-1) downto 0 loop
                if (Bus2IP_BE(byte_idx) = '1') then
                  if ((C_REG_DESCR(reg_idx).typ = C_REG_RW) or (C_REG_DESCR(reg_idx).typ = C_REG_WR)) then
                    -- standard write
                    reg_wr(reg_idx)(byte_idx*8+7 downto byte_idx*8) <= Bus2IP_Data(byte_idx*8+7 downto byte_idx*8);
                    wr_pulse(reg_idx) <= '1'; 
                  elsif (C_REG_DESCR(reg_idx).typ = C_BIT_SET) then
                    -- set bits in another reg
                    reg_wr(C_REG_DESCR(reg_idx).reg)(byte_idx*8+7 downto byte_idx*8) <= reg_wr(C_REG_DESCR(reg_idx).reg)(byte_idx*8+7 downto byte_idx*8) or Bus2IP_Data(byte_idx*8+7 downto byte_idx*8);
                    wr_pulse(C_REG_DESCR(reg_idx).reg) <= '1'; 
                  elsif (C_REG_DESCR(reg_idx).typ = C_BIT_CLR) then
                    -- clear bits in another reg
                    reg_wr(C_REG_DESCR(reg_idx).reg)(byte_idx*8+7 downto byte_idx*8) <= reg_wr(C_REG_DESCR(reg_idx).reg)(byte_idx*8+7 downto byte_idx*8) and not Bus2IP_Data(byte_idx*8+7 downto byte_idx*8);
                    wr_pulse(C_REG_DESCR(reg_idx).reg) <= '1'; 
                  end if;
                end if;
              end loop;  -- byte_idx

          elsif ((C_REG_DESCR(reg_idx).wr_ext = C_WR_EXT_YES) and (USER_TO_REG_I.wr_ext(reg_idx) = '1')) then
            reg_wr(reg_idx) <= USER_TO_REG_I.data(reg_idx);
          end if;

          -- Set bits which are not active in "writable mask" to default value
          -- these 
          if ((C_REG_DESCR(reg_idx).typ = C_REG_RW) or (C_REG_DESCR(reg_idx).typ = C_REG_WR)) then
            reg_wr_mask_gen: for bit_idx in (C_SLV_DWIDTH-1) downto 0 loop
              if (C_REG_DESCR(reg_idx).wr_msk(bit_idx) = '0') then
                reg_wr(reg_idx)(bit_idx) <= C_REG_DESCR(reg_idx).def(bit_idx);
              end if;
            end loop; -- bit_idx
          end if;
          
        end if;  -- not C_REG_RO

      end loop;
    end if;
  end process reg_wr_proc;

  slv_wr_ack <= '1' when (Bus2IP_WrCE /= C_REG_SEL_NONE) else '0';

  ---------------------------------------------------------------------------
  -- Readback of C_REG_RW registers
  ---------------------------------------------------------------------------
  reg_rw_loop: for reg_idx in 0 to C_NUM_REG - 1 generate
  begin
  
    reg_rw_gen:if (C_REG_DESCR(reg_idx).typ = C_REG_RW) generate
    begin
      reg_rd(reg_idx) <= reg_wr(reg_idx);
    end generate; -- if C_REG_RW
    
    reg_ro_gen:if (C_REG_DESCR(reg_idx).typ /= C_REG_RW) generate
    begin
      reg_rd(reg_idx) <= USER_TO_REG_I.data(reg_idx);
    end generate; -- if C_REG_RW
        
    
  end generate; -- loop

  ---------------------------------------------------------------------------
  -- Outputs
  ---------------------------------------------------------------------------

  IP2Bus_RdAck   <= slv_rd_ack;
  IP2Bus_WrAck   <= slv_wr_ack;
  IP2Bus_Error   <= '0';
  IP2Bus_Data    <= slv_ip2bus_data when (slv_rd_ack = '1') else (others => '0');

  REG_TO_USER_O.data      <=  reg_wr;         
  REG_TO_USER_O.wr_pulse  <=  wr_pulse;    
  REG_TO_USER_O.rd_pulse  <=  rd_pulse;       

end behavioral;
