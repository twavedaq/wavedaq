---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Register Map
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created : 24.10.2019 07:55:18
--
--  Register Layout Version : 9
--
--  Description :  Mapping of the register content of WaveDream2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity util_register_map is
  generic (
    CGN_BITS_PER_REG : integer := 32;
    CGN_NUM_CTRL_REG : integer := 139;
    CGN_NUM_STAT_REG : integer := 89;
    CGN_HW_CTRL_REG  : std_logic_vector(0 to 138) := "1111111111111111111111111111111111111111111111111111111111111111111110000111111111111111111111111111111111111111111111111111111111111111111";
    CGN_HW_STAT_REG  : std_logic_vector(0 to 88) := "11111111111111111111111111111111111111111111111111111111111111111111111111111111111111111"
  );
  port (

    -- Control Register 0 [0x1000]: WDB_LOC
    WDB_LOC_BYTE_TRIG_O                       : out std_logic_vector(3 downto 0);
    WDB_LOC_REG_READ_O                        : out std_logic;
    WDB_LOC_REG_WRITE_O                       : out std_logic;
    CRATE_ID_O                                : out std_logic_vector(7 downto 0);     
    CRATE_ID_BIT_TRIG_O                       : out std_logic_vector(7 downto 0);
    SLOT_ID_O                                 : out std_logic_vector(7 downto 0);     
    SLOT_ID_BIT_TRIG_O                        : out std_logic_vector(7 downto 0);

    -- Control Register 1 [0x1004]: CTRL
    CTRL_BYTE_TRIG_O                          : out std_logic_vector(3 downto 0);
    CTRL_REG_READ_O                           : out std_logic;
    CTRL_REG_WRITE_O                          : out std_logic;
    VALID_DELAY_ADC_O                         : out std_logic_vector(5 downto 0);     
    VALID_DELAY_ADC_BIT_TRIG_O                : out std_logic_vector(5 downto 0);
    DAQ_DATA_PHASE_O                          : out std_logic_vector(7 downto 0);     
    DAQ_DATA_PHASE_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);
    FE_POWER_O                                : out std_logic;                        
    FE_POWER_BIT_TRIG_O                       : out std_logic;
    DRS_CLR_RSR_AFTER_RO_O                    : out std_logic;                        
    DRS_CLR_RSR_AFTER_RO_BIT_TRIG_O           : out std_logic;
    COMP_POWER_EN_O                           : out std_logic;                        
    COMP_POWER_EN_BIT_TRIG_O                  : out std_logic;
    DRS_READOUT_MODE_O                        : out std_logic;                        
    DRS_READOUT_MODE_BIT_TRIG_O               : out std_logic;
    DRS_WAVE_CONTINUOUS_O                     : out std_logic;                        
    DRS_WAVE_CONTINUOUS_BIT_TRIG_O            : out std_logic;
    DRS_CONFIGURE_O                           : out std_logic;                        
    DRS_CONFIGURE_BIT_TRIG_O                  : out std_logic;
    DAQ_SOFT_TRIGGER_O                        : out std_logic;                        
    DAQ_SOFT_TRIGGER_BIT_TRIG_O               : out std_logic;
    DAQ_AUTO_O                                : out std_logic;                        
    DAQ_AUTO_BIT_TRIG_O                       : out std_logic;
    DAQ_NORMAL_O                              : out std_logic;                        
    DAQ_NORMAL_BIT_TRIG_O                     : out std_logic;
    DAQ_SINGLE_O                              : out std_logic;                        
    DAQ_SINGLE_BIT_TRIG_O                     : out std_logic;

    -- Control Register 2 [0x1008]: CAL_CTRL
    CAL_CTRL_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    CAL_CTRL_REG_READ_O                       : out std_logic;
    CAL_CTRL_REG_WRITE_O                      : out std_logic;
    DRS_0_TIMING_REF_SEL_O                    : out std_logic;                        
    DRS_0_TIMING_REF_SEL_BIT_TRIG_O           : out std_logic;
    DRS_1_TIMING_REF_SEL_O                    : out std_logic;                        
    DRS_1_TIMING_REF_SEL_BIT_TRIG_O           : out std_logic;
    CALIB_BUFFER_EN_O                         : out std_logic;                        
    CALIB_BUFFER_EN_BIT_TRIG_O                : out std_logic;
    TIMING_CALIB_SIGNAL_EN_O                  : out std_logic;                        
    TIMING_CALIB_SIGNAL_EN_BIT_TRIG_O         : out std_logic;

    -- Control Register 3 [0x100C]: CLK_CTRL
    CLK_CTRL_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    CLK_CTRL_REG_READ_O                       : out std_logic;
    CLK_CTRL_REG_WRITE_O                      : out std_logic;
    TRIG_DAQ_CLK_CAL_CHK_O                    : out std_logic;                        
    TRIG_DAQ_CLK_CAL_CHK_BIT_TRIG_O           : out std_logic;
    DAQ_CLK_SRC_SEL_O                         : out std_logic;                        
    DAQ_CLK_SRC_SEL_BIT_TRIG_O                : out std_logic;
    EXT_CLK_IN_SEL_O                          : out std_logic;                        
    EXT_CLK_IN_SEL_BIT_TRIG_O                 : out std_logic;
    EXT_CLK_FREQ_O                            : out std_logic_vector(7 downto 0);     
    EXT_CLK_FREQ_BIT_TRIG_O                   : out std_logic_vector(7 downto 0);
    LOCAL_CLK_FREQ_O                          : out std_logic_vector(7 downto 0);     
    LOCAL_CLK_FREQ_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);

    -- Control Register 4 [0x1010]: DRS_CTRL
    DRS_CTRL_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DRS_CTRL_REG_READ_O                       : out std_logic;
    DRS_CTRL_REG_WRITE_O                      : out std_logic;
    DRS_WSRLOOP_O                             : out std_logic;                        
    DRS_WSRLOOP_BIT_TRIG_O                    : out std_logic;
    DRS_PLLEN_O                               : out std_logic;                        
    DRS_PLLEN_BIT_TRIG_O                      : out std_logic;
    DRS_DMODE_O                               : out std_logic;                        
    DRS_DMODE_BIT_TRIG_O                      : out std_logic;
    DRS_WSR_O                                 : out std_logic_vector(7 downto 0);     
    DRS_WSR_BIT_TRIG_O                        : out std_logic_vector(7 downto 0);
    DRS_WCR_O                                 : out std_logic_vector(7 downto 0);     
    DRS_WCR_BIT_TRIG_O                        : out std_logic_vector(7 downto 0);

    -- Control Register 5 [0x1014]: COM_CTRL
    COM_CTRL_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    COM_CTRL_REG_READ_O                       : out std_logic;
    COM_CTRL_REG_WRITE_O                      : out std_logic;
    DCB_SERDES_TRAIN_O                        : out std_logic;                        
    DCB_SERDES_TRAIN_BIT_TRIG_O               : out std_logic;
    TCB_SERDES_TRAIN_O                        : out std_logic;                        
    TCB_SERDES_TRAIN_BIT_TRIG_O               : out std_logic;
    TRG_TX_EN_O                               : out std_logic;                        
    TRG_TX_EN_BIT_TRIG_O                      : out std_logic;
    SCL_TX_EN_O                               : out std_logic;                        
    SCL_TX_EN_BIT_TRIG_O                      : out std_logic;
    SERDES_COM_EN_O                           : out std_logic;                        
    SERDES_COM_EN_BIT_TRIG_O                  : out std_logic;
    ETH_COM_EN_O                              : out std_logic;                        
    ETH_COM_EN_BIT_TRIG_O                     : out std_logic;
    INTER_PKG_DELAY_O                         : out std_logic_vector(23 downto 0);    
    INTER_PKG_DELAY_BIT_TRIG_O                : out std_logic_vector(23 downto 0);

    -- Control Register 6 [0x1018]: COM_PLD_SIZE
    COM_PLD_SIZE_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    COM_PLD_SIZE_REG_READ_O                   : out std_logic;
    COM_PLD_SIZE_REG_WRITE_O                  : out std_logic;
    FIRST_PKG_DLY_O                           : out std_logic_vector(13 downto 0);    
    FIRST_PKG_DLY_BIT_TRIG_O                  : out std_logic_vector(13 downto 0);
    COM_PLD_SIZE_O                            : out std_logic_vector(17 downto 0);    
    COM_PLD_SIZE_BIT_TRIG_O                   : out std_logic_vector(17 downto 0);

    -- Control Register 7 [0x101C]: DRS_CH_TX_EN
    DRS_CH_TX_EN_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    DRS_CH_TX_EN_REG_READ_O                   : out std_logic;
    DRS_CH_TX_EN_REG_WRITE_O                  : out std_logic;
    DRS_CH_TX_EN_O                            : out std_logic_vector(17 downto 0);    
    DRS_CH_TX_EN_BIT_TRIG_O                   : out std_logic_vector(17 downto 0);

    -- Control Register 8 [0x1020]: ADC_CH_TX_EN
    ADC_CH_TX_EN_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    ADC_CH_TX_EN_REG_READ_O                   : out std_logic;
    ADC_CH_TX_EN_REG_WRITE_O                  : out std_logic;
    ADC_CH_TX_EN_O                            : out std_logic_vector(15 downto 0);    
    ADC_CH_TX_EN_BIT_TRIG_O                   : out std_logic_vector(15 downto 0);

    -- Control Register 9 [0x1024]: TDC_CH_TX_EN
    TDC_CH_TX_EN_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    TDC_CH_TX_EN_REG_READ_O                   : out std_logic;
    TDC_CH_TX_EN_REG_WRITE_O                  : out std_logic;
    TDC_CH_TX_EN_O                            : out std_logic_vector(15 downto 0);    
    TDC_CH_TX_EN_BIT_TRIG_O                   : out std_logic_vector(15 downto 0);

    -- Control Register 10 [0x1028]: DRS_TX_SAMPLES
    DRS_TX_SAMPLES_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    DRS_TX_SAMPLES_REG_READ_O                 : out std_logic;
    DRS_TX_SAMPLES_REG_WRITE_O                : out std_logic;
    DRS_TX_SAMPLES_O                          : out std_logic_vector(10 downto 0);    
    DRS_TX_SAMPLES_BIT_TRIG_O                 : out std_logic_vector(10 downto 0);

    -- Control Register 11 [0x102C]: ADC_TX_SAMPLES
    ADC_TX_SAMPLES_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    ADC_TX_SAMPLES_REG_READ_O                 : out std_logic;
    ADC_TX_SAMPLES_REG_WRITE_O                : out std_logic;
    ADC_TX_SAMPLES_O                          : out std_logic_vector(11 downto 0);    
    ADC_TX_SAMPLES_BIT_TRIG_O                 : out std_logic_vector(11 downto 0);

    -- Control Register 12 [0x1030]: TDC_TX_SAMPLES
    TDC_TX_SAMPLES_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    TDC_TX_SAMPLES_REG_READ_O                 : out std_logic;
    TDC_TX_SAMPLES_REG_WRITE_O                : out std_logic;
    TDC_TX_SAMPLES_O                          : out std_logic_vector(12 downto 0);    
    TDC_TX_SAMPLES_BIT_TRIG_O                 : out std_logic_vector(12 downto 0);

    -- Control Register 13 [0x1034]: TRG_TX_SAMPLES
    TRG_TX_SAMPLES_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    TRG_TX_SAMPLES_REG_READ_O                 : out std_logic;
    TRG_TX_SAMPLES_REG_WRITE_O                : out std_logic;
    TRG_TX_SAMPLES_O                          : out std_logic_vector(9 downto 0);     
    TRG_TX_SAMPLES_BIT_TRIG_O                 : out std_logic_vector(9 downto 0);

    -- Control Register 14 [0x1038]: ADC_SAMPLE_DIV
    ADC_SAMPLE_DIV_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    ADC_SAMPLE_DIV_REG_READ_O                 : out std_logic;
    ADC_SAMPLE_DIV_REG_WRITE_O                : out std_logic;
    ADC_SAMPLE_DIV_O                          : out std_logic_vector(7 downto 0);     
    ADC_SAMPLE_DIV_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);

    -- Control Register 15 [0x103C]: ZERO_SUPR
    ZERO_SUPR_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    ZERO_SUPR_REG_READ_O                      : out std_logic;
    ZERO_SUPR_REG_WRITE_O                     : out std_logic;
    ZERO_SUPR_EN_O                            : out std_logic;                        
    ZERO_SUPR_EN_BIT_TRIG_O                   : out std_logic;
    ZERO_SUPR_WINDOW_O                        : out std_logic_vector(7 downto 0);     
    ZERO_SUPR_WINDOW_BIT_TRIG_O               : out std_logic_vector(7 downto 0);

    -- Control Register 16 [0x1040]: RST
    RST_BYTE_TRIG_O                           : out std_logic_vector(3 downto 0);
    RST_REG_READ_O                            : out std_logic;
    RST_REG_WRITE_O                           : out std_logic;
    DAQ_PLL_RST_O                             : out std_logic;                        
    DAQ_PLL_RST_BIT_TRIG_O                    : out std_logic;
    DCB_OSERDES_PLL_RST_O                     : out std_logic;                        
    DCB_OSERDES_PLL_RST_BIT_TRIG_O            : out std_logic;
    TCB_OSERDES_PLL_RST_O                     : out std_logic;                        
    TCB_OSERDES_PLL_RST_BIT_TRIG_O            : out std_logic;
    DCB_OSERDES_IF_RST_O                      : out std_logic;                        
    DCB_OSERDES_IF_RST_BIT_TRIG_O             : out std_logic;
    TCB_OSERDES_IF_RST_O                      : out std_logic;                        
    TCB_OSERDES_IF_RST_BIT_TRIG_O             : out std_logic;
    SCALER_RST_O                              : out std_logic;                        
    SCALER_RST_BIT_TRIG_O                     : out std_logic;
    TRB_PARITY_ERROR_COUNT_RST_O              : out std_logic;                        
    TRB_PARITY_ERROR_COUNT_RST_BIT_TRIG_O     : out std_logic;
    LMK_SYNC_LOCAL_O                          : out std_logic;                        
    LMK_SYNC_LOCAL_BIT_TRIG_O                 : out std_logic;
    ADC_RST_O                                 : out std_logic;                        
    ADC_RST_BIT_TRIG_O                        : out std_logic;
    ADC_IF_RST_O                              : out std_logic;                        
    ADC_IF_RST_BIT_TRIG_O                     : out std_logic;
    DATA_LINK_IF_RST_O                        : out std_logic;                        
    DATA_LINK_IF_RST_BIT_TRIG_O               : out std_logic;
    WD_PKGR_RST_O                             : out std_logic;                        
    WD_PKGR_RST_BIT_TRIG_O                    : out std_logic;
    EVENT_COUNTER_RST_O                       : out std_logic;                        
    EVENT_COUNTER_RST_BIT_TRIG_O              : out std_logic;
    DRS_CTRL_FSM_RST_O                        : out std_logic;                        
    DRS_CTRL_FSM_RST_BIT_TRIG_O               : out std_logic;
    RECONFIGURE_FPGA_O                        : out std_logic;                        
    RECONFIGURE_FPGA_BIT_TRIG_O               : out std_logic;

    -- Control Register 17 [0x1044]: APLY_CFG
    APLY_CFG_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    APLY_CFG_REG_READ_O                       : out std_logic;
    APLY_CFG_REG_WRITE_O                      : out std_logic;
    APPLY_SETTINGS_ADC_SAMPLE_DIV_O           : out std_logic;                        
    APPLY_SETTINGS_ADC_SAMPLE_DIV_BIT_TRIG_O  : out std_logic;
    APPLY_SETTINGS_MAX_PLD_SIZE_O             : out std_logic;                        
    APPLY_SETTINGS_MAX_PLD_SIZE_BIT_TRIG_O    : out std_logic;
    APPLY_SETTINGS_HV_O                       : out std_logic;                        
    APPLY_SETTINGS_HV_BIT_TRIG_O              : out std_logic;
    APPLY_SETTINGS_DRS_O                      : out std_logic;                        
    APPLY_SETTINGS_DRS_BIT_TRIG_O             : out std_logic;
    APPLY_SETTINGS_DAC_O                      : out std_logic;                        
    APPLY_SETTINGS_DAC_BIT_TRIG_O             : out std_logic;
    APPLY_SETTINGS_FRONTEND_O                 : out std_logic;                        
    APPLY_SETTINGS_FRONTEND_BIT_TRIG_O        : out std_logic;
    APPLY_SETTINGS_CTRL_O                     : out std_logic;                        
    APPLY_SETTINGS_CTRL_BIT_TRIG_O            : out std_logic;
    APPLY_SETTINGS_ADC_O                      : out std_logic;                        
    APPLY_SETTINGS_ADC_BIT_TRIG_O             : out std_logic;
    APPLY_SETTINGS_LMK_O                      : out std_logic;                        
    APPLY_SETTINGS_LMK_BIT_TRIG_O             : out std_logic;

    -- Control Register 18 [0x1048]: DAC0_A_B
    DAC0_A_B_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC0_A_B_REG_READ_O                       : out std_logic;
    DAC0_A_B_REG_WRITE_O                      : out std_logic;
    DAC0_CH_A_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_A_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC0_CH_B_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_B_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 19 [0x104C]: DAC0_C_D
    DAC0_C_D_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC0_C_D_REG_READ_O                       : out std_logic;
    DAC0_C_D_REG_WRITE_O                      : out std_logic;
    DAC0_CH_C_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_C_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC0_CH_D_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_D_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 20 [0x1050]: DAC0_E_F
    DAC0_E_F_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC0_E_F_REG_READ_O                       : out std_logic;
    DAC0_E_F_REG_WRITE_O                      : out std_logic;
    DAC0_CH_E_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_E_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC0_CH_F_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_F_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 21 [0x1054]: DAC0_G_H
    DAC0_G_H_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC0_G_H_REG_READ_O                       : out std_logic;
    DAC0_G_H_REG_WRITE_O                      : out std_logic;
    DAC0_CH_G_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_G_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC0_CH_H_O                               : out std_logic_vector(15 downto 0);    
    DAC0_CH_H_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 22 [0x1058]: DAC1_A_B
    DAC1_A_B_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC1_A_B_REG_READ_O                       : out std_logic;
    DAC1_A_B_REG_WRITE_O                      : out std_logic;
    DAC1_CH_A_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_A_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC1_CH_B_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_B_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 23 [0x105C]: DAC1_C_D
    DAC1_C_D_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC1_C_D_REG_READ_O                       : out std_logic;
    DAC1_C_D_REG_WRITE_O                      : out std_logic;
    DAC1_CH_C_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_C_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC1_CH_D_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_D_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 24 [0x1060]: DAC1_E_F
    DAC1_E_F_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC1_E_F_REG_READ_O                       : out std_logic;
    DAC1_E_F_REG_WRITE_O                      : out std_logic;
    DAC1_CH_E_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_E_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC1_CH_F_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_F_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 25 [0x1064]: DAC1_G_H
    DAC1_G_H_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC1_G_H_REG_READ_O                       : out std_logic;
    DAC1_G_H_REG_WRITE_O                      : out std_logic;
    DAC1_CH_G_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_G_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC1_CH_H_O                               : out std_logic_vector(15 downto 0);    
    DAC1_CH_H_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 26 [0x1068]: DAC2_A_B
    DAC2_A_B_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC2_A_B_REG_READ_O                       : out std_logic;
    DAC2_A_B_REG_WRITE_O                      : out std_logic;
    DAC2_CH_A_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_A_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC2_CH_B_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_B_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 27 [0x106C]: DAC2_C_D
    DAC2_C_D_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC2_C_D_REG_READ_O                       : out std_logic;
    DAC2_C_D_REG_WRITE_O                      : out std_logic;
    DAC2_CH_C_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_C_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC2_CH_D_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_D_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 28 [0x1070]: DAC2_E_F
    DAC2_E_F_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC2_E_F_REG_READ_O                       : out std_logic;
    DAC2_E_F_REG_WRITE_O                      : out std_logic;
    DAC2_CH_E_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_E_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC2_CH_F_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_F_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 29 [0x1074]: DAC2_G_H
    DAC2_G_H_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    DAC2_G_H_REG_READ_O                       : out std_logic;
    DAC2_G_H_REG_WRITE_O                      : out std_logic;
    DAC2_CH_G_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_G_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);
    DAC2_CH_H_O                               : out std_logic_vector(15 downto 0);    
    DAC2_CH_H_BIT_TRIG_O                      : out std_logic_vector(15 downto 0);

    -- Control Register 30 [0x1078]: FE_CFG_0_1
    FE_CFG_0_1_BYTE_TRIG_O                    : out std_logic_vector(3 downto 0);
    FE_CFG_0_1_REG_READ_O                     : out std_logic;
    FE_CFG_0_1_REG_WRITE_O                    : out std_logic;
    FE0_PZC_EN_O                              : out std_logic;                        
    FE0_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE0_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE0_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE0_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE0_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE0_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE0_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE0_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE0_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE0_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE0_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE0_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE0_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);
    FE1_PZC_EN_O                              : out std_logic;                        
    FE1_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE1_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE1_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE1_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE1_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE1_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE1_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE1_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE1_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE1_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE1_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE1_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE1_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);

    -- Control Register 31 [0x107C]: FE_CFG_2_3
    FE_CFG_2_3_BYTE_TRIG_O                    : out std_logic_vector(3 downto 0);
    FE_CFG_2_3_REG_READ_O                     : out std_logic;
    FE_CFG_2_3_REG_WRITE_O                    : out std_logic;
    FE2_PZC_EN_O                              : out std_logic;                        
    FE2_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE2_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE2_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE2_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE2_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE2_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE2_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE2_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE2_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE2_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE2_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE2_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE2_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);
    FE3_PZC_EN_O                              : out std_logic;                        
    FE3_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE3_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE3_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE3_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE3_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE3_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE3_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE3_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE3_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE3_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE3_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE3_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE3_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);

    -- Control Register 32 [0x1080]: FE_CFG_4_5
    FE_CFG_4_5_BYTE_TRIG_O                    : out std_logic_vector(3 downto 0);
    FE_CFG_4_5_REG_READ_O                     : out std_logic;
    FE_CFG_4_5_REG_WRITE_O                    : out std_logic;
    FE4_PZC_EN_O                              : out std_logic;                        
    FE4_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE4_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE4_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE4_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE4_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE4_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE4_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE4_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE4_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE4_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE4_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE4_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE4_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);
    FE5_PZC_EN_O                              : out std_logic;                        
    FE5_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE5_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE5_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE5_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE5_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE5_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE5_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE5_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE5_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE5_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE5_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE5_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE5_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);

    -- Control Register 33 [0x1084]: FE_CFG_6_7
    FE_CFG_6_7_BYTE_TRIG_O                    : out std_logic_vector(3 downto 0);
    FE_CFG_6_7_REG_READ_O                     : out std_logic;
    FE_CFG_6_7_REG_WRITE_O                    : out std_logic;
    FE6_PZC_EN_O                              : out std_logic;                        
    FE6_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE6_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE6_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE6_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE6_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE6_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE6_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE6_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE6_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE6_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE6_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE6_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE6_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);
    FE7_PZC_EN_O                              : out std_logic;                        
    FE7_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE7_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE7_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE7_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE7_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE7_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE7_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE7_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE7_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE7_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE7_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE7_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE7_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);

    -- Control Register 34 [0x1088]: FE_CFG_8_9
    FE_CFG_8_9_BYTE_TRIG_O                    : out std_logic_vector(3 downto 0);
    FE_CFG_8_9_REG_READ_O                     : out std_logic;
    FE_CFG_8_9_REG_WRITE_O                    : out std_logic;
    FE8_PZC_EN_O                              : out std_logic;                        
    FE8_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE8_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE8_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE8_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE8_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE8_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE8_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE8_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE8_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE8_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE8_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE8_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE8_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);
    FE9_PZC_EN_O                              : out std_logic;                        
    FE9_PZC_EN_BIT_TRIG_O                     : out std_logic;
    FE9_AMPLIFIER2_COMP_EN_O                  : out std_logic;                        
    FE9_AMPLIFIER2_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE9_AMPLIFIER2_EN_O                       : out std_logic;                        
    FE9_AMPLIFIER2_EN_BIT_TRIG_O              : out std_logic;
    FE9_AMPLIFIER1_COMP_EN_O                  : out std_logic;                        
    FE9_AMPLIFIER1_COMP_EN_BIT_TRIG_O         : out std_logic;
    FE9_AMPLIFIER1_EN_O                       : out std_logic;                        
    FE9_AMPLIFIER1_EN_BIT_TRIG_O              : out std_logic;
    FE9_ATTENUATION_O                         : out std_logic_vector(1 downto 0);     
    FE9_ATTENUATION_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    FE9_MUX_O                                 : out std_logic_vector(1 downto 0);     
    FE9_MUX_BIT_TRIG_O                        : out std_logic_vector(1 downto 0);

    -- Control Register 35 [0x108C]: FE_CFG_10_11
    FE_CFG_10_11_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    FE_CFG_10_11_REG_READ_O                   : out std_logic;
    FE_CFG_10_11_REG_WRITE_O                  : out std_logic;
    FE10_PZC_EN_O                             : out std_logic;                        
    FE10_PZC_EN_BIT_TRIG_O                    : out std_logic;
    FE10_AMPLIFIER2_COMP_EN_O                 : out std_logic;                        
    FE10_AMPLIFIER2_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE10_AMPLIFIER2_EN_O                      : out std_logic;                        
    FE10_AMPLIFIER2_EN_BIT_TRIG_O             : out std_logic;
    FE10_AMPLIFIER1_COMP_EN_O                 : out std_logic;                        
    FE10_AMPLIFIER1_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE10_AMPLIFIER1_EN_O                      : out std_logic;                        
    FE10_AMPLIFIER1_EN_BIT_TRIG_O             : out std_logic;
    FE10_ATTENUATION_O                        : out std_logic_vector(1 downto 0);     
    FE10_ATTENUATION_BIT_TRIG_O               : out std_logic_vector(1 downto 0);
    FE10_MUX_O                                : out std_logic_vector(1 downto 0);     
    FE10_MUX_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE11_PZC_EN_O                             : out std_logic;                        
    FE11_PZC_EN_BIT_TRIG_O                    : out std_logic;
    FE11_AMPLIFIER2_COMP_EN_O                 : out std_logic;                        
    FE11_AMPLIFIER2_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE11_AMPLIFIER2_EN_O                      : out std_logic;                        
    FE11_AMPLIFIER2_EN_BIT_TRIG_O             : out std_logic;
    FE11_AMPLIFIER1_COMP_EN_O                 : out std_logic;                        
    FE11_AMPLIFIER1_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE11_AMPLIFIER1_EN_O                      : out std_logic;                        
    FE11_AMPLIFIER1_EN_BIT_TRIG_O             : out std_logic;
    FE11_ATTENUATION_O                        : out std_logic_vector(1 downto 0);     
    FE11_ATTENUATION_BIT_TRIG_O               : out std_logic_vector(1 downto 0);
    FE11_MUX_O                                : out std_logic_vector(1 downto 0);     
    FE11_MUX_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);

    -- Control Register 36 [0x1090]: FE_CFG_12_13
    FE_CFG_12_13_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    FE_CFG_12_13_REG_READ_O                   : out std_logic;
    FE_CFG_12_13_REG_WRITE_O                  : out std_logic;
    FE12_PZC_EN_O                             : out std_logic;                        
    FE12_PZC_EN_BIT_TRIG_O                    : out std_logic;
    FE12_AMPLIFIER2_COMP_EN_O                 : out std_logic;                        
    FE12_AMPLIFIER2_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE12_AMPLIFIER2_EN_O                      : out std_logic;                        
    FE12_AMPLIFIER2_EN_BIT_TRIG_O             : out std_logic;
    FE12_AMPLIFIER1_COMP_EN_O                 : out std_logic;                        
    FE12_AMPLIFIER1_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE12_AMPLIFIER1_EN_O                      : out std_logic;                        
    FE12_AMPLIFIER1_EN_BIT_TRIG_O             : out std_logic;
    FE12_ATTENUATION_O                        : out std_logic_vector(1 downto 0);     
    FE12_ATTENUATION_BIT_TRIG_O               : out std_logic_vector(1 downto 0);
    FE12_MUX_O                                : out std_logic_vector(1 downto 0);     
    FE12_MUX_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE13_PZC_EN_O                             : out std_logic;                        
    FE13_PZC_EN_BIT_TRIG_O                    : out std_logic;
    FE13_AMPLIFIER2_COMP_EN_O                 : out std_logic;                        
    FE13_AMPLIFIER2_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE13_AMPLIFIER2_EN_O                      : out std_logic;                        
    FE13_AMPLIFIER2_EN_BIT_TRIG_O             : out std_logic;
    FE13_AMPLIFIER1_COMP_EN_O                 : out std_logic;                        
    FE13_AMPLIFIER1_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE13_AMPLIFIER1_EN_O                      : out std_logic;                        
    FE13_AMPLIFIER1_EN_BIT_TRIG_O             : out std_logic;
    FE13_ATTENUATION_O                        : out std_logic_vector(1 downto 0);     
    FE13_ATTENUATION_BIT_TRIG_O               : out std_logic_vector(1 downto 0);
    FE13_MUX_O                                : out std_logic_vector(1 downto 0);     
    FE13_MUX_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);

    -- Control Register 37 [0x1094]: FE_CFG_14_15
    FE_CFG_14_15_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    FE_CFG_14_15_REG_READ_O                   : out std_logic;
    FE_CFG_14_15_REG_WRITE_O                  : out std_logic;
    FE14_PZC_EN_O                             : out std_logic;                        
    FE14_PZC_EN_BIT_TRIG_O                    : out std_logic;
    FE14_AMPLIFIER2_COMP_EN_O                 : out std_logic;                        
    FE14_AMPLIFIER2_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE14_AMPLIFIER2_EN_O                      : out std_logic;                        
    FE14_AMPLIFIER2_EN_BIT_TRIG_O             : out std_logic;
    FE14_AMPLIFIER1_COMP_EN_O                 : out std_logic;                        
    FE14_AMPLIFIER1_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE14_AMPLIFIER1_EN_O                      : out std_logic;                        
    FE14_AMPLIFIER1_EN_BIT_TRIG_O             : out std_logic;
    FE14_ATTENUATION_O                        : out std_logic_vector(1 downto 0);     
    FE14_ATTENUATION_BIT_TRIG_O               : out std_logic_vector(1 downto 0);
    FE14_MUX_O                                : out std_logic_vector(1 downto 0);     
    FE14_MUX_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE15_PZC_EN_O                             : out std_logic;                        
    FE15_PZC_EN_BIT_TRIG_O                    : out std_logic;
    FE15_AMPLIFIER2_COMP_EN_O                 : out std_logic;                        
    FE15_AMPLIFIER2_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE15_AMPLIFIER2_EN_O                      : out std_logic;                        
    FE15_AMPLIFIER2_EN_BIT_TRIG_O             : out std_logic;
    FE15_AMPLIFIER1_COMP_EN_O                 : out std_logic;                        
    FE15_AMPLIFIER1_COMP_EN_BIT_TRIG_O        : out std_logic;
    FE15_AMPLIFIER1_EN_O                      : out std_logic;                        
    FE15_AMPLIFIER1_EN_BIT_TRIG_O             : out std_logic;
    FE15_ATTENUATION_O                        : out std_logic_vector(1 downto 0);     
    FE15_ATTENUATION_BIT_TRIG_O               : out std_logic_vector(1 downto 0);
    FE15_MUX_O                                : out std_logic_vector(1 downto 0);     
    FE15_MUX_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);

    -- Control Register 38 [0x1098]: HV_U_TARGET_0
    HV_U_TARGET_0_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_0_REG_READ_O                  : out std_logic;
    HV_U_TARGET_0_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_0_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_0_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 39 [0x109C]: HV_U_TARGET_1
    HV_U_TARGET_1_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_1_REG_READ_O                  : out std_logic;
    HV_U_TARGET_1_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_1_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_1_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 40 [0x10A0]: HV_U_TARGET_2
    HV_U_TARGET_2_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_2_REG_READ_O                  : out std_logic;
    HV_U_TARGET_2_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_2_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_2_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 41 [0x10A4]: HV_U_TARGET_3
    HV_U_TARGET_3_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_3_REG_READ_O                  : out std_logic;
    HV_U_TARGET_3_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_3_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_3_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 42 [0x10A8]: HV_U_TARGET_4
    HV_U_TARGET_4_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_4_REG_READ_O                  : out std_logic;
    HV_U_TARGET_4_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_4_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_4_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 43 [0x10AC]: HV_U_TARGET_5
    HV_U_TARGET_5_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_5_REG_READ_O                  : out std_logic;
    HV_U_TARGET_5_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_5_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_5_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 44 [0x10B0]: HV_U_TARGET_6
    HV_U_TARGET_6_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_6_REG_READ_O                  : out std_logic;
    HV_U_TARGET_6_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_6_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_6_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 45 [0x10B4]: HV_U_TARGET_7
    HV_U_TARGET_7_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_7_REG_READ_O                  : out std_logic;
    HV_U_TARGET_7_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_7_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_7_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 46 [0x10B8]: HV_U_TARGET_8
    HV_U_TARGET_8_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_8_REG_READ_O                  : out std_logic;
    HV_U_TARGET_8_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_8_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_8_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 47 [0x10BC]: HV_U_TARGET_9
    HV_U_TARGET_9_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    HV_U_TARGET_9_REG_READ_O                  : out std_logic;
    HV_U_TARGET_9_REG_WRITE_O                 : out std_logic;
    HV_U_TARGET_9_O                           : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_9_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Control Register 48 [0x10C0]: HV_U_TARGET_10
    HV_U_TARGET_10_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_TARGET_10_REG_READ_O                 : out std_logic;
    HV_U_TARGET_10_REG_WRITE_O                : out std_logic;
    HV_U_TARGET_10_O                          : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_10_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 49 [0x10C4]: HV_U_TARGET_11
    HV_U_TARGET_11_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_TARGET_11_REG_READ_O                 : out std_logic;
    HV_U_TARGET_11_REG_WRITE_O                : out std_logic;
    HV_U_TARGET_11_O                          : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_11_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 50 [0x10C8]: HV_U_TARGET_12
    HV_U_TARGET_12_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_TARGET_12_REG_READ_O                 : out std_logic;
    HV_U_TARGET_12_REG_WRITE_O                : out std_logic;
    HV_U_TARGET_12_O                          : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_12_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 51 [0x10CC]: HV_U_TARGET_13
    HV_U_TARGET_13_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_TARGET_13_REG_READ_O                 : out std_logic;
    HV_U_TARGET_13_REG_WRITE_O                : out std_logic;
    HV_U_TARGET_13_O                          : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_13_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 52 [0x10D0]: HV_U_TARGET_14
    HV_U_TARGET_14_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_TARGET_14_REG_READ_O                 : out std_logic;
    HV_U_TARGET_14_REG_WRITE_O                : out std_logic;
    HV_U_TARGET_14_O                          : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_14_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 53 [0x10D4]: HV_U_TARGET_15
    HV_U_TARGET_15_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_TARGET_15_REG_READ_O                 : out std_logic;
    HV_U_TARGET_15_REG_WRITE_O                : out std_logic;
    HV_U_TARGET_15_O                          : out std_logic_vector(31 downto 0);    
    HV_U_TARGET_15_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 54 [0x10D8]: HV_R_SHUNT
    HV_R_SHUNT_BYTE_TRIG_O                    : out std_logic_vector(3 downto 0);
    HV_R_SHUNT_REG_READ_O                     : out std_logic;
    HV_R_SHUNT_REG_WRITE_O                    : out std_logic;
    HV_R_SHUNT_O                              : out std_logic_vector(31 downto 0);    
    HV_R_SHUNT_BIT_TRIG_O                     : out std_logic_vector(31 downto 0);

    -- Control Register 55 [0x10DC]: LMK_0
    LMK_0_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_0_REG_READ_O                          : out std_logic;
    LMK_0_REG_WRITE_O                         : out std_logic;
    LMK0_RESET_O                              : out std_logic;                        
    LMK0_RESET_BIT_TRIG_O                     : out std_logic;
    LMK0_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK0_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK0_CLKOUT_EN_O                          : out std_logic;                        
    LMK0_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK0_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK0_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK0_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK0_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 56 [0x10E0]: LMK_1
    LMK_1_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_1_REG_READ_O                          : out std_logic;
    LMK_1_REG_WRITE_O                         : out std_logic;
    LMK1_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK1_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK1_CLKOUT_EN_O                          : out std_logic;                        
    LMK1_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK1_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK1_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK1_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK1_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 57 [0x10E4]: LMK_2
    LMK_2_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_2_REG_READ_O                          : out std_logic;
    LMK_2_REG_WRITE_O                         : out std_logic;
    LMK2_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK2_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK2_CLKOUT_EN_O                          : out std_logic;                        
    LMK2_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK2_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK2_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK2_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK2_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 58 [0x10E8]: LMK_3
    LMK_3_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_3_REG_READ_O                          : out std_logic;
    LMK_3_REG_WRITE_O                         : out std_logic;
    LMK3_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK3_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK3_CLKOUT_EN_O                          : out std_logic;                        
    LMK3_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK3_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK3_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK3_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK3_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 59 [0x10EC]: LMK_4
    LMK_4_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_4_REG_READ_O                          : out std_logic;
    LMK_4_REG_WRITE_O                         : out std_logic;
    LMK4_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK4_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK4_CLKOUT_EN_O                          : out std_logic;                        
    LMK4_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK4_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK4_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK4_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK4_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 60 [0x10F0]: LMK_5
    LMK_5_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_5_REG_READ_O                          : out std_logic;
    LMK_5_REG_WRITE_O                         : out std_logic;
    LMK5_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK5_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK5_CLKOUT_EN_O                          : out std_logic;                        
    LMK5_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK5_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK5_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK5_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK5_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 61 [0x10F4]: LMK_6
    LMK_6_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_6_REG_READ_O                          : out std_logic;
    LMK_6_REG_WRITE_O                         : out std_logic;
    LMK6_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK6_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK6_CLKOUT_EN_O                          : out std_logic;                        
    LMK6_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK6_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK6_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK6_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK6_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 62 [0x10F8]: LMK_7
    LMK_7_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_7_REG_READ_O                          : out std_logic;
    LMK_7_REG_WRITE_O                         : out std_logic;
    LMK7_CLKOUT_MUX_O                         : out std_logic_vector(1 downto 0);     
    LMK7_CLKOUT_MUX_BIT_TRIG_O                : out std_logic_vector(1 downto 0);
    LMK7_CLKOUT_EN_O                          : out std_logic;                        
    LMK7_CLKOUT_EN_BIT_TRIG_O                 : out std_logic;
    LMK7_CLKOUT_DIV_O                         : out std_logic_vector(7 downto 0);     
    LMK7_CLKOUT_DIV_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    LMK7_CLKOUT_DLY_O                         : out std_logic_vector(3 downto 0);     
    LMK7_CLKOUT_DLY_BIT_TRIG_O                : out std_logic_vector(3 downto 0);

    -- Control Register 63 [0x10FC]: LMK_8
    LMK_8_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_8_REG_READ_O                          : out std_logic;
    LMK_8_REG_WRITE_O                         : out std_logic;
    LMK8_PHASE_NOISE_OPT_O                    : out std_logic_vector(27 downto 0);    
    LMK8_PHASE_NOISE_OPT_BIT_TRIG_O           : out std_logic_vector(27 downto 0);

    -- Control Register 64 [0x1100]: LMK_9
    LMK_9_BYTE_TRIG_O                         : out std_logic_vector(3 downto 0);
    LMK_9_REG_READ_O                          : out std_logic;
    LMK_9_REG_WRITE_O                         : out std_logic;
    LMK9_VBOOST_O                             : out std_logic;                        
    LMK9_VBOOST_BIT_TRIG_O                    : out std_logic;

    -- Control Register 65 [0x1104]: LMK_11
    LMK_11_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    LMK_11_REG_READ_O                         : out std_logic;
    LMK_11_REG_WRITE_O                        : out std_logic;
    LMK11_DIV4_O                              : out std_logic;                        
    LMK11_DIV4_BIT_TRIG_O                     : out std_logic;

    -- Control Register 66 [0x1108]: LMK_13
    LMK_13_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    LMK_13_REG_READ_O                         : out std_logic;
    LMK_13_REG_WRITE_O                        : out std_logic;
    LMK13_OSCIN_FREQ_O                        : out std_logic_vector(7 downto 0);     
    LMK13_OSCIN_FREQ_BIT_TRIG_O               : out std_logic_vector(7 downto 0);
    LMK13_VCO_R4_LF_O                         : out std_logic_vector(2 downto 0);     
    LMK13_VCO_R4_LF_BIT_TRIG_O                : out std_logic_vector(2 downto 0);
    LMK13_VCO_R3_LF_O                         : out std_logic_vector(2 downto 0);     
    LMK13_VCO_R3_LF_BIT_TRIG_O                : out std_logic_vector(2 downto 0);
    LMK13_VCO_C3_C4_LF_O                      : out std_logic_vector(3 downto 0);     
    LMK13_VCO_C3_C4_LF_BIT_TRIG_O             : out std_logic_vector(3 downto 0);

    -- Control Register 67 [0x110C]: LMK_14
    LMK_14_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    LMK_14_REG_READ_O                         : out std_logic;
    LMK_14_REG_WRITE_O                        : out std_logic;
    LMK14_EN_FOUT_O                           : out std_logic;                        
    LMK14_EN_FOUT_BIT_TRIG_O                  : out std_logic;
    LMK14_EN_CLKOUT_GLOBAL_O                  : out std_logic;                        
    LMK14_EN_CLKOUT_GLOBAL_BIT_TRIG_O         : out std_logic;
    LMK14_POWERDOWN_O                         : out std_logic;                        
    LMK14_POWERDOWN_BIT_TRIG_O                : out std_logic;
    LMK14_PLL_MUX_O                           : out std_logic_vector(3 downto 0);     
    LMK14_PLL_MUX_BIT_TRIG_O                  : out std_logic_vector(3 downto 0);
    LMK14_PLL_R_O                             : out std_logic_vector(11 downto 0);    
    LMK14_PLL_R_BIT_TRIG_O                    : out std_logic_vector(11 downto 0);

    -- Control Register 68 [0x1110]: LMK_15
    LMK_15_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    LMK_15_REG_READ_O                         : out std_logic;
    LMK_15_REG_WRITE_O                        : out std_logic;
    LMK15_PLL_CP_GAIN_O                       : out std_logic_vector(1 downto 0);     
    LMK15_PLL_CP_GAIN_BIT_TRIG_O              : out std_logic_vector(1 downto 0);
    LMK15_VCO_DIV_O                           : out std_logic_vector(3 downto 0);     
    LMK15_VCO_DIV_BIT_TRIG_O                  : out std_logic_vector(3 downto 0);
    LMK15_PLL_N_O                             : out std_logic_vector(17 downto 0);    
    LMK15_PLL_N_BIT_TRIG_O                    : out std_logic_vector(17 downto 0);

    -- Control Register 69 [0x1114]: ADC_0_CFG_1458
    -- '--> Pure software register => no ports available

    -- Control Register 70 [0x1118]: ADC_0_CFG_2367
    -- '--> Pure software register => no ports available

    -- Control Register 71 [0x111C]: ADC_1_CFG_1458
    -- '--> Pure software register => no ports available

    -- Control Register 72 [0x1120]: ADC_1_CFG_2367
    -- '--> Pure software register => no ports available

    -- Control Register 73 [0x1124]: TRG_CFG
    TRG_CFG_BYTE_TRIG_O                       : out std_logic_vector(3 downto 0);
    TRG_CFG_REG_READ_O                        : out std_logic;
    TRG_CFG_REG_WRITE_O                       : out std_logic;
    LEAD_TRAIL_EDGE_SEL_O                     : out std_logic;                        
    LEAD_TRAIL_EDGE_SEL_BIT_TRIG_O            : out std_logic;
    EXT_TRIGGER_OUT_ENABLE_O                  : out std_logic;                        
    EXT_TRIGGER_OUT_ENABLE_BIT_TRIG_O         : out std_logic;
    EXT_ASYNC_TRIGGER_EN_O                    : out std_logic;                        
    EXT_ASYNC_TRIGGER_EN_BIT_TRIG_O           : out std_logic;
    PATTERN_TRIGGER_EN_O                      : out std_logic;                        
    PATTERN_TRIGGER_EN_BIT_TRIG_O             : out std_logic;
    TRIGGER_OUT_PULSE_LENGTH_O                : out std_logic_vector(2 downto 0);     
    TRIGGER_OUT_PULSE_LENGTH_BIT_TRIG_O       : out std_logic_vector(2 downto 0);
    TRIGGER_DELAY_O                           : out std_logic_vector(7 downto 0);     
    TRIGGER_DELAY_BIT_TRIG_O                  : out std_logic_vector(7 downto 0);

    -- Control Register 74 [0x1128]: TRG_SRC_POL
    TRG_SRC_POL_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    TRG_SRC_POL_REG_READ_O                    : out std_logic;
    TRG_SRC_POL_REG_WRITE_O                   : out std_logic;
    TRG_SRC_POLARITY_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_POLARITY_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 75 [0x112C]: TRG_AUTO_PERIOD
    TRG_AUTO_PERIOD_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_AUTO_PERIOD_REG_READ_O                : out std_logic;
    TRG_AUTO_PERIOD_REG_WRITE_O               : out std_logic;
    AUTO_TRIGGER_PERIOD_O                     : out std_logic_vector(31 downto 0);    
    AUTO_TRIGGER_PERIOD_BIT_TRIG_O            : out std_logic_vector(31 downto 0);

    -- Control Register 76 [0x1130]: TRG_PTRN_EN
    TRG_PTRN_EN_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    TRG_PTRN_EN_REG_READ_O                    : out std_logic;
    TRG_PTRN_EN_REG_WRITE_O                   : out std_logic;
    TRG_PTRN_EN_O                             : out std_logic_vector(18 downto 0);    
    TRG_PTRN_EN_BIT_TRIG_O                    : out std_logic_vector(18 downto 0);

    -- Control Register 77 [0x1134]: TRG_SRC_EN_PTRN0
    TRG_SRC_EN_PTRN0_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN0_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN0_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN0_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN0_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 78 [0x1138]: TRG_STATE_PTRN0
    TRG_STATE_PTRN0_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN0_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN0_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN0_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN0_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 79 [0x113C]: TRG_SRC_EN_PTRN1
    TRG_SRC_EN_PTRN1_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN1_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN1_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN1_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN1_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 80 [0x1140]: TRG_STATE_PTRN1
    TRG_STATE_PTRN1_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN1_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN1_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN1_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN1_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 81 [0x1144]: TRG_SRC_EN_PTRN2
    TRG_SRC_EN_PTRN2_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN2_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN2_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN2_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN2_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 82 [0x1148]: TRG_STATE_PTRN2
    TRG_STATE_PTRN2_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN2_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN2_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN2_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN2_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 83 [0x114C]: TRG_SRC_EN_PTRN3
    TRG_SRC_EN_PTRN3_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN3_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN3_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN3_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN3_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 84 [0x1150]: TRG_STATE_PTRN3
    TRG_STATE_PTRN3_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN3_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN3_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN3_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN3_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 85 [0x1154]: TRG_SRC_EN_PTRN4
    TRG_SRC_EN_PTRN4_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN4_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN4_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN4_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN4_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 86 [0x1158]: TRG_STATE_PTRN4
    TRG_STATE_PTRN4_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN4_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN4_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN4_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN4_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 87 [0x115C]: TRG_SRC_EN_PTRN5
    TRG_SRC_EN_PTRN5_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN5_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN5_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN5_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN5_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 88 [0x1160]: TRG_STATE_PTRN5
    TRG_STATE_PTRN5_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN5_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN5_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN5_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN5_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 89 [0x1164]: TRG_SRC_EN_PTRN6
    TRG_SRC_EN_PTRN6_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN6_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN6_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN6_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN6_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 90 [0x1168]: TRG_STATE_PTRN6
    TRG_STATE_PTRN6_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN6_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN6_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN6_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN6_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 91 [0x116C]: TRG_SRC_EN_PTRN7
    TRG_SRC_EN_PTRN7_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN7_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN7_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN7_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN7_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 92 [0x1170]: TRG_STATE_PTRN7
    TRG_STATE_PTRN7_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN7_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN7_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN7_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN7_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 93 [0x1174]: TRG_SRC_EN_PTRN8
    TRG_SRC_EN_PTRN8_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN8_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN8_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN8_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN8_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 94 [0x1178]: TRG_STATE_PTRN8
    TRG_STATE_PTRN8_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN8_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN8_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN8_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN8_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 95 [0x117C]: TRG_SRC_EN_PTRN9
    TRG_SRC_EN_PTRN9_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN9_REG_READ_O               : out std_logic;
    TRG_SRC_EN_PTRN9_REG_WRITE_O              : out std_logic;
    TRG_SRC_EN_PTRN9_O                        : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN9_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 96 [0x1180]: TRG_STATE_PTRN9
    TRG_STATE_PTRN9_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN9_REG_READ_O                : out std_logic;
    TRG_STATE_PTRN9_REG_WRITE_O               : out std_logic;
    TRG_STATE_PTRN9_O                         : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN9_BIT_TRIG_O                : out std_logic_vector(18 downto 0);

    -- Control Register 97 [0x1184]: TRG_SRC_EN_PTRN10
    TRG_SRC_EN_PTRN10_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN10_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN10_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN10_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN10_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 98 [0x1188]: TRG_STATE_PTRN10
    TRG_STATE_PTRN10_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN10_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN10_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN10_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN10_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 99 [0x118C]: TRG_SRC_EN_PTRN11
    TRG_SRC_EN_PTRN11_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN11_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN11_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN11_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN11_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 100 [0x1190]: TRG_STATE_PTRN11
    TRG_STATE_PTRN11_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN11_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN11_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN11_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN11_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 101 [0x1194]: TRG_SRC_EN_PTRN12
    TRG_SRC_EN_PTRN12_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN12_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN12_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN12_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN12_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 102 [0x1198]: TRG_STATE_PTRN12
    TRG_STATE_PTRN12_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN12_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN12_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN12_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN12_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 103 [0x119C]: TRG_SRC_EN_PTRN13
    TRG_SRC_EN_PTRN13_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN13_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN13_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN13_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN13_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 104 [0x11A0]: TRG_STATE_PTRN13
    TRG_STATE_PTRN13_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN13_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN13_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN13_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN13_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 105 [0x11A4]: TRG_SRC_EN_PTRN14
    TRG_SRC_EN_PTRN14_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN14_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN14_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN14_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN14_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 106 [0x11A8]: TRG_STATE_PTRN14
    TRG_STATE_PTRN14_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN14_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN14_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN14_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN14_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 107 [0x11AC]: TRG_SRC_EN_PTRN15
    TRG_SRC_EN_PTRN15_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN15_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN15_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN15_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN15_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 108 [0x11B0]: TRG_STATE_PTRN15
    TRG_STATE_PTRN15_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN15_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN15_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN15_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN15_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 109 [0x11B4]: TRG_SRC_EN_PTRN16
    TRG_SRC_EN_PTRN16_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN16_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN16_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN16_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN16_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 110 [0x11B8]: TRG_STATE_PTRN16
    TRG_STATE_PTRN16_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN16_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN16_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN16_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN16_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 111 [0x11BC]: TRG_SRC_EN_PTRN17
    TRG_SRC_EN_PTRN17_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN17_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN17_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN17_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN17_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 112 [0x11C0]: TRG_STATE_PTRN17
    TRG_STATE_PTRN17_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN17_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN17_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN17_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN17_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 113 [0x11C4]: TRG_SRC_EN_PTRN18
    TRG_SRC_EN_PTRN18_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    TRG_SRC_EN_PTRN18_REG_READ_O              : out std_logic;
    TRG_SRC_EN_PTRN18_REG_WRITE_O             : out std_logic;
    TRG_SRC_EN_PTRN18_O                       : out std_logic_vector(18 downto 0);    
    TRG_SRC_EN_PTRN18_BIT_TRIG_O              : out std_logic_vector(18 downto 0);

    -- Control Register 114 [0x11C8]: TRG_STATE_PTRN18
    TRG_STATE_PTRN18_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    TRG_STATE_PTRN18_REG_READ_O               : out std_logic;
    TRG_STATE_PTRN18_REG_WRITE_O              : out std_logic;
    TRG_STATE_PTRN18_O                        : out std_logic_vector(18 downto 0);    
    TRG_STATE_PTRN18_BIT_TRIG_O               : out std_logic_vector(18 downto 0);

    -- Control Register 115 [0x11CC]: ADV_TRG_CTRL
    ADV_TRG_CTRL_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    ADV_TRG_CTRL_REG_READ_O                   : out std_logic;
    ADV_TRG_CTRL_REG_WRITE_O                  : out std_logic;
    ADV_TRG_CTRL_O                            : out std_logic_vector(31 downto 0);    
    ADV_TRG_CTRL_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Control Register 116 [0x11D0]: ADV_TRG_CH_CAL0
    ADV_TRG_CH_CAL0_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    ADV_TRG_CH_CAL0_REG_READ_O                : out std_logic;
    ADV_TRG_CH_CAL0_REG_WRITE_O               : out std_logic;
    ADV_TRG_CH_CAL0_O                         : out std_logic_vector(31 downto 0);    
    ADV_TRG_CH_CAL0_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Control Register 117 [0x11D4]: ADV_TRG_CH_CAL1
    ADV_TRG_CH_CAL1_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    ADV_TRG_CH_CAL1_REG_READ_O                : out std_logic;
    ADV_TRG_CH_CAL1_REG_WRITE_O               : out std_logic;
    ADV_TRG_CH_CAL1_O                         : out std_logic_vector(31 downto 0);    
    ADV_TRG_CH_CAL1_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Control Register 118 [0x11D8]: ADV_TRG_CH_CAL2
    ADV_TRG_CH_CAL2_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    ADV_TRG_CH_CAL2_REG_READ_O                : out std_logic;
    ADV_TRG_CH_CAL2_REG_WRITE_O               : out std_logic;
    ADV_TRG_CH_CAL2_O                         : out std_logic_vector(31 downto 0);    
    ADV_TRG_CH_CAL2_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Control Register 119 [0x11DC]: ADV_TRG_CH_CAL3
    ADV_TRG_CH_CAL3_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    ADV_TRG_CH_CAL3_REG_READ_O                : out std_logic;
    ADV_TRG_CH_CAL3_REG_WRITE_O               : out std_logic;
    ADV_TRG_CH_CAL3_O                         : out std_logic_vector(31 downto 0);    
    ADV_TRG_CH_CAL3_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Control Register 120 [0x11E0]: ADV_TRG_PED_CFG
    ADV_TRG_PED_CFG_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    ADV_TRG_PED_CFG_REG_READ_O                : out std_logic;
    ADV_TRG_PED_CFG_REG_WRITE_O               : out std_logic;
    ADV_TRG_PED_CFG_O                         : out std_logic_vector(31 downto 0);    
    ADV_TRG_PED_CFG_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Control Register 121 [0x11E4]: ADV_TRG_THR0
    ADV_TRG_THR0_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    ADV_TRG_THR0_REG_READ_O                   : out std_logic;
    ADV_TRG_THR0_REG_WRITE_O                  : out std_logic;
    ADV_TRG_THR0_O                            : out std_logic_vector(31 downto 0);    
    ADV_TRG_THR0_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Control Register 122 [0x11E8]: ADV_TRG_THR1
    ADV_TRG_THR1_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    ADV_TRG_THR1_REG_READ_O                   : out std_logic;
    ADV_TRG_THR1_REG_WRITE_O                  : out std_logic;
    ADV_TRG_THR1_O                            : out std_logic_vector(31 downto 0);    
    ADV_TRG_THR1_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Control Register 123 [0x11EC]: ADV_TRG_THR2
    ADV_TRG_THR2_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    ADV_TRG_THR2_REG_READ_O                   : out std_logic;
    ADV_TRG_THR2_REG_WRITE_O                  : out std_logic;
    ADV_TRG_THR2_O                            : out std_logic_vector(31 downto 0);    
    ADV_TRG_THR2_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Control Register 124 [0x11F0]: ADV_TRG_TX_CHK_WORD0
    ADV_TRG_TX_CHK_WORD0_BYTE_TRIG_O          : out std_logic_vector(3 downto 0);
    ADV_TRG_TX_CHK_WORD0_REG_READ_O           : out std_logic;
    ADV_TRG_TX_CHK_WORD0_REG_WRITE_O          : out std_logic;
    ADV_TRG_TX_CHK_WORD0_O                    : out std_logic_vector(31 downto 0);    
    ADV_TRG_TX_CHK_WORD0_BIT_TRIG_O           : out std_logic_vector(31 downto 0);

    -- Control Register 125 [0x11F4]: ADV_TRG_TX_CHK_WORD1
    ADV_TRG_TX_CHK_WORD1_BYTE_TRIG_O          : out std_logic_vector(3 downto 0);
    ADV_TRG_TX_CHK_WORD1_REG_READ_O           : out std_logic;
    ADV_TRG_TX_CHK_WORD1_REG_WRITE_O          : out std_logic;
    ADV_TRG_TX_CHK_WORD1_O                    : out std_logic_vector(31 downto 0);    
    ADV_TRG_TX_CHK_WORD1_BIT_TRIG_O           : out std_logic_vector(31 downto 0);

    -- Control Register 126 [0x11F8]: ADV_TRG_TDC_CH_MASK
    ADV_TRG_TDC_CH_MASK_BYTE_TRIG_O           : out std_logic_vector(3 downto 0);
    ADV_TRG_TDC_CH_MASK_REG_READ_O            : out std_logic;
    ADV_TRG_TDC_CH_MASK_REG_WRITE_O           : out std_logic;
    ADV_TRG_TDC_CH_MASK_O                     : out std_logic_vector(31 downto 0);    
    ADV_TRG_TDC_CH_MASK_BIT_TRIG_O            : out std_logic_vector(31 downto 0);

    -- Control Register 127 [0x11FC]: ADV_TRG_CFG12
    ADV_TRG_CFG12_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG12_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG12_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_12_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_12_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 128 [0x1200]: ADV_TRG_CFG13
    ADV_TRG_CFG13_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG13_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG13_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_13_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_13_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 129 [0x1204]: ADV_TRG_CFG14
    ADV_TRG_CFG14_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG14_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG14_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_14_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_14_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 130 [0x1208]: ADV_TRG_CFG15
    ADV_TRG_CFG15_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG15_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG15_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_15_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_15_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 131 [0x120C]: ADV_TRG_CFG16
    ADV_TRG_CFG16_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG16_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG16_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_16_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_16_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 132 [0x1210]: ADV_TRG_CFG17
    ADV_TRG_CFG17_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG17_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG17_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_17_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_17_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 133 [0x1214]: ADV_TRG_CFG18
    ADV_TRG_CFG18_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG18_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG18_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_18_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_18_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 134 [0x1218]: ADV_TRG_CFG19
    ADV_TRG_CFG19_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_CFG19_REG_READ_O                  : out std_logic;
    ADV_TRG_CFG19_REG_WRITE_O                 : out std_logic;
    ADV_TRG_CFG_19_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_CFG_19_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Control Register 135 [0x121C]: SET_TIME_LSB
    SET_TIME_LSB_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    SET_TIME_LSB_REG_READ_O                   : out std_logic;
    SET_TIME_LSB_REG_WRITE_O                  : out std_logic;
    SET_TIME_LSB_O                            : out std_logic_vector(31 downto 0);    
    SET_TIME_LSB_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Control Register 136 [0x1220]: SET_TIME_MSB
    SET_TIME_MSB_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    SET_TIME_MSB_REG_READ_O                   : out std_logic;
    SET_TIME_MSB_REG_WRITE_O                  : out std_logic;
    SET_TIME_MSB_O                            : out std_logic_vector(31 downto 0);    
    SET_TIME_MSB_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Control Register 137 [0x1224]: DBG_SIG_SEL
    DBG_SIG_SEL_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    DBG_SIG_SEL_REG_READ_O                    : out std_logic;
    DBG_SIG_SEL_REG_WRITE_O                   : out std_logic;
    MCX_TX_SIG_SEL_O                          : out std_logic_vector(3 downto 0);     
    MCX_TX_SIG_SEL_BIT_TRIG_O                 : out std_logic_vector(3 downto 0);
    MCX_RX_SIG_SEL_O                          : out std_logic_vector(3 downto 0);     
    MCX_RX_SIG_SEL_BIT_TRIG_O                 : out std_logic_vector(3 downto 0);

    -- Control Register 138 [0x1228]: CRC32_REG_BANK
    CRC32_REG_BANK_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    CRC32_REG_BANK_REG_READ_O                 : out std_logic;
    CRC32_REG_BANK_REG_WRITE_O                : out std_logic;
    CRC32_REG_BANK_O                          : out std_logic_vector(31 downto 0);    
    CRC32_REG_BANK_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 0 [0x0000]: HW_VER
    HW_VER_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    HW_VER_REG_READ_O                         : out std_logic;
    HW_VER_REG_WRITE_O                        : out std_logic;
    BOARD_MAGIC_I                             : in  std_logic_vector(7 downto 0);     
    BOARD_MAGIC_O                             : out std_logic_vector(7 downto 0);     
    BOARD_MAGIC_BIT_TRIG_O                    : out std_logic_vector(7 downto 0);
    VENDOR_ID_I                               : in  std_logic_vector(7 downto 0);     
    VENDOR_ID_O                               : out std_logic_vector(7 downto 0);     
    VENDOR_ID_BIT_TRIG_O                      : out std_logic_vector(7 downto 0);
    BOARD_TYPE_I                              : in  std_logic_vector(7 downto 0);     
    BOARD_TYPE_O                              : out std_logic_vector(7 downto 0);     
    BOARD_TYPE_BIT_TRIG_O                     : out std_logic_vector(7 downto 0);
    BOARD_REVISION_I                          : in  std_logic_vector(5 downto 0);     
    BOARD_REVISION_O                          : out std_logic_vector(5 downto 0);     
    BOARD_REVISION_BIT_TRIG_O                 : out std_logic_vector(5 downto 0);
    BOARD_VARIANT_I                           : in  std_logic_vector(1 downto 0);     
    BOARD_VARIANT_O                           : out std_logic_vector(1 downto 0);     
    BOARD_VARIANT_BIT_TRIG_O                  : out std_logic_vector(1 downto 0);

    -- Status Register 1 [0x0004]: REG_LAYOUT_VER
    REG_LAYOUT_VER_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    REG_LAYOUT_VER_REG_READ_O                 : out std_logic;
    REG_LAYOUT_VER_REG_WRITE_O                : out std_logic;
    REG_LAYOUT_COMP_LEVEL_I                   : in  std_logic_vector(15 downto 0);    
    REG_LAYOUT_COMP_LEVEL_O                   : out std_logic_vector(15 downto 0);    
    REG_LAYOUT_COMP_LEVEL_BIT_TRIG_O          : out std_logic_vector(15 downto 0);
    REG_LAYOUT_VERSION_I                      : in  std_logic_vector(15 downto 0);    
    REG_LAYOUT_VERSION_O                      : out std_logic_vector(15 downto 0);    
    REG_LAYOUT_VERSION_BIT_TRIG_O             : out std_logic_vector(15 downto 0);

    -- Status Register 2 [0x0008]: FW_BUILD_DATE
    FW_BUILD_DATE_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    FW_BUILD_DATE_REG_READ_O                  : out std_logic;
    FW_BUILD_DATE_REG_WRITE_O                 : out std_logic;
    FW_BUILD_YEAR_I                           : in  std_logic_vector(15 downto 0);    
    FW_BUILD_YEAR_O                           : out std_logic_vector(15 downto 0);    
    FW_BUILD_YEAR_BIT_TRIG_O                  : out std_logic_vector(15 downto 0);
    FW_BUILD_MONTH_I                          : in  std_logic_vector(7 downto 0);     
    FW_BUILD_MONTH_O                          : out std_logic_vector(7 downto 0);     
    FW_BUILD_MONTH_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);
    FW_BUILD_DAY_I                            : in  std_logic_vector(7 downto 0);     
    FW_BUILD_DAY_O                            : out std_logic_vector(7 downto 0);     
    FW_BUILD_DAY_BIT_TRIG_O                   : out std_logic_vector(7 downto 0);

    -- Status Register 3 [0x000C]: FW_BUILD_TIME
    FW_BUILD_TIME_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    FW_BUILD_TIME_REG_READ_O                  : out std_logic;
    FW_BUILD_TIME_REG_WRITE_O                 : out std_logic;
    FW_COMPAT_LEVEL_I                         : in  std_logic_vector(7 downto 0);     
    FW_COMPAT_LEVEL_O                         : out std_logic_vector(7 downto 0);     
    FW_COMPAT_LEVEL_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    FW_BUILD_HOUR_I                           : in  std_logic_vector(7 downto 0);     
    FW_BUILD_HOUR_O                           : out std_logic_vector(7 downto 0);     
    FW_BUILD_HOUR_BIT_TRIG_O                  : out std_logic_vector(7 downto 0);
    FW_BUILD_MINUTE_I                         : in  std_logic_vector(7 downto 0);     
    FW_BUILD_MINUTE_O                         : out std_logic_vector(7 downto 0);     
    FW_BUILD_MINUTE_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    FW_BUILD_SECOND_I                         : in  std_logic_vector(7 downto 0);     
    FW_BUILD_SECOND_O                         : out std_logic_vector(7 downto 0);     
    FW_BUILD_SECOND_BIT_TRIG_O                : out std_logic_vector(7 downto 0);

    -- Status Register 4 [0x0010]: SW_BUILD_DATE
    SW_BUILD_DATE_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    SW_BUILD_DATE_REG_READ_O                  : out std_logic;
    SW_BUILD_DATE_REG_WRITE_O                 : out std_logic;
    SW_BUILD_YEAR_I                           : in  std_logic_vector(15 downto 0);    
    SW_BUILD_YEAR_O                           : out std_logic_vector(15 downto 0);    
    SW_BUILD_YEAR_BIT_TRIG_O                  : out std_logic_vector(15 downto 0);
    SW_BUILD_MONTH_I                          : in  std_logic_vector(7 downto 0);     
    SW_BUILD_MONTH_O                          : out std_logic_vector(7 downto 0);     
    SW_BUILD_MONTH_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);
    SW_BUILD_DAY_I                            : in  std_logic_vector(7 downto 0);     
    SW_BUILD_DAY_O                            : out std_logic_vector(7 downto 0);     
    SW_BUILD_DAY_BIT_TRIG_O                   : out std_logic_vector(7 downto 0);

    -- Status Register 5 [0x0014]: SW_BUILD_TIME
    SW_BUILD_TIME_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    SW_BUILD_TIME_REG_READ_O                  : out std_logic;
    SW_BUILD_TIME_REG_WRITE_O                 : out std_logic;
    SW_BUILD_HOUR_I                           : in  std_logic_vector(7 downto 0);     
    SW_BUILD_HOUR_O                           : out std_logic_vector(7 downto 0);     
    SW_BUILD_HOUR_BIT_TRIG_O                  : out std_logic_vector(7 downto 0);
    SW_BUILD_MINUTE_I                         : in  std_logic_vector(7 downto 0);     
    SW_BUILD_MINUTE_O                         : out std_logic_vector(7 downto 0);     
    SW_BUILD_MINUTE_BIT_TRIG_O                : out std_logic_vector(7 downto 0);
    SW_BUILD_SECOND_I                         : in  std_logic_vector(7 downto 0);     
    SW_BUILD_SECOND_O                         : out std_logic_vector(7 downto 0);     
    SW_BUILD_SECOND_BIT_TRIG_O                : out std_logic_vector(7 downto 0);

    -- Status Register 6 [0x0018]: FW_GIT_HASH_TAG
    FW_GIT_HASH_TAG_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    FW_GIT_HASH_TAG_REG_READ_O                : out std_logic;
    FW_GIT_HASH_TAG_REG_WRITE_O               : out std_logic;
    FW_GIT_HASH_TAG_I                         : in  std_logic_vector(31 downto 0);    
    FW_GIT_HASH_TAG_O                         : out std_logic_vector(31 downto 0);    
    FW_GIT_HASH_TAG_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Status Register 7 [0x001C]: SW_GIT_HASH_TAG
    SW_GIT_HASH_TAG_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    SW_GIT_HASH_TAG_REG_READ_O                : out std_logic;
    SW_GIT_HASH_TAG_REG_WRITE_O               : out std_logic;
    SW_GIT_HASH_TAG_I                         : in  std_logic_vector(31 downto 0);    
    SW_GIT_HASH_TAG_O                         : out std_logic_vector(31 downto 0);    
    SW_GIT_HASH_TAG_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Status Register 8 [0x0020]: PROT_VER
    PROT_VER_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    PROT_VER_REG_READ_O                       : out std_logic;
    PROT_VER_REG_WRITE_O                      : out std_logic;
    PROTOCOL_VERSION_I                        : in  std_logic_vector(7 downto 0);     
    PROTOCOL_VERSION_O                        : out std_logic_vector(7 downto 0);     
    PROTOCOL_VERSION_BIT_TRIG_O               : out std_logic_vector(7 downto 0);

    -- Status Register 9 [0x0024]: SN
    SN_BYTE_TRIG_O                            : out std_logic_vector(3 downto 0);
    SN_REG_READ_O                             : out std_logic;
    SN_REG_WRITE_O                            : out std_logic;
    SERIAL_NUMBER_I                           : in  std_logic_vector(15 downto 0);    
    SERIAL_NUMBER_O                           : out std_logic_vector(15 downto 0);    
    SERIAL_NUMBER_BIT_TRIG_O                  : out std_logic_vector(15 downto 0);

    -- Status Register 10 [0x0028]: STATUS
    STATUS_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    STATUS_REG_READ_O                         : out std_logic;
    STATUS_REG_WRITE_O                        : out std_logic;
    TEMPERATURE_I                             : in  std_logic_vector(15 downto 0);    
    TEMPERATURE_O                             : out std_logic_vector(15 downto 0);    
    TEMPERATURE_BIT_TRIG_O                    : out std_logic_vector(15 downto 0);
    OVERTEMP_I                                : in  std_logic;                        
    OVERTEMP_O                                : out std_logic;                        
    OVERTEMP_BIT_TRIG_O                       : out std_logic;
    DAQ_CLK_DEF_PHASE_OK_I                    : in  std_logic;                        
    DAQ_CLK_DEF_PHASE_OK_O                    : out std_logic;                        
    DAQ_CLK_DEF_PHASE_OK_BIT_TRIG_O           : out std_logic;
    DAQ_CLK_DEF_PHASE_CHKD_I                  : in  std_logic;                        
    DAQ_CLK_DEF_PHASE_CHKD_O                  : out std_logic;                        
    DAQ_CLK_DEF_PHASE_CHKD_BIT_TRIG_O         : out std_logic;
    DRS_CONFIG_DONE_I                         : in  std_logic;                        
    DRS_CONFIG_DONE_O                         : out std_logic;                        
    DRS_CONFIG_DONE_BIT_TRIG_O                : out std_logic;
    BOARD_SEL_I                               : in  std_logic;                        
    BOARD_SEL_O                               : out std_logic;                        
    BOARD_SEL_BIT_TRIG_O                      : out std_logic;
    SERIAL_BUSY_I                             : in  std_logic;                        
    SERIAL_BUSY_O                             : out std_logic;                        
    SERIAL_BUSY_BIT_TRIG_O                    : out std_logic;
    PACKAGER_BUSY_I                           : in  std_logic;                        
    PACKAGER_BUSY_O                           : out std_logic;                        
    PACKAGER_BUSY_BIT_TRIG_O                  : out std_logic;
    DRS_CTRL_BUSY_I                           : in  std_logic;                        
    DRS_CTRL_BUSY_O                           : out std_logic;                        
    DRS_CTRL_BUSY_BIT_TRIG_O                  : out std_logic;
    SYS_BUSY_I                                : in  std_logic;                        
    SYS_BUSY_O                                : out std_logic;                        
    SYS_BUSY_BIT_TRIG_O                       : out std_logic;
    HV_BOARD_PLUGGED_I                        : in  std_logic;                        
    HV_BOARD_PLUGGED_O                        : out std_logic;                        
    HV_BOARD_PLUGGED_BIT_TRIG_O               : out std_logic;
    BACKPLANE_PLUGGED_I                       : in  std_logic;                        
    BACKPLANE_PLUGGED_O                       : out std_logic;                        
    BACKPLANE_PLUGGED_BIT_TRIG_O              : out std_logic;

    -- Status Register 11 [0x002C]: PLL_LOCK
    PLL_LOCK_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    PLL_LOCK_REG_READ_O                       : out std_logic;
    PLL_LOCK_REG_WRITE_O                      : out std_logic;
    SYS_DCM_LOCK_I                            : in  std_logic;                        
    SYS_DCM_LOCK_O                            : out std_logic;                        
    SYS_DCM_LOCK_BIT_TRIG_O                   : out std_logic;
    DAQ_PLL_LOCK_I                            : in  std_logic;                        
    DAQ_PLL_LOCK_O                            : out std_logic;                        
    DAQ_PLL_LOCK_BIT_TRIG_O                   : out std_logic;
    OSERDES_PLL_LOCK_DCB_I                    : in  std_logic;                        
    OSERDES_PLL_LOCK_DCB_O                    : out std_logic;                        
    OSERDES_PLL_LOCK_DCB_BIT_TRIG_O           : out std_logic;
    OSERDES_PLL_LOCK_TCB_I                    : in  std_logic;                        
    OSERDES_PLL_LOCK_TCB_O                    : out std_logic;                        
    OSERDES_PLL_LOCK_TCB_BIT_TRIG_O           : out std_logic;
    ISERDES_PLL_LOCK_0_I                      : in  std_logic;                        
    ISERDES_PLL_LOCK_0_O                      : out std_logic;                        
    ISERDES_PLL_LOCK_0_BIT_TRIG_O             : out std_logic;
    ISERDES_PLL_LOCK_1_I                      : in  std_logic;                        
    ISERDES_PLL_LOCK_1_O                      : out std_logic;                        
    ISERDES_PLL_LOCK_1_BIT_TRIG_O             : out std_logic;
    DRS_PLL_LOCK_0_I                          : in  std_logic;                        
    DRS_PLL_LOCK_0_O                          : out std_logic;                        
    DRS_PLL_LOCK_0_BIT_TRIG_O                 : out std_logic;
    DRS_PLL_LOCK_1_I                          : in  std_logic;                        
    DRS_PLL_LOCK_1_O                          : out std_logic;                        
    DRS_PLL_LOCK_1_BIT_TRIG_O                 : out std_logic;
    LMK_PLL_LOCK_I                            : in  std_logic;                        
    LMK_PLL_LOCK_O                            : out std_logic;                        
    LMK_PLL_LOCK_BIT_TRIG_O                   : out std_logic;

    -- Status Register 12 [0x0030]: DRS_STOP_CELL
    DRS_STOP_CELL_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    DRS_STOP_CELL_REG_READ_O                  : out std_logic;
    DRS_STOP_CELL_REG_WRITE_O                 : out std_logic;
    DRS_STOP_CELL_0_I                         : in  std_logic_vector(9 downto 0);     
    DRS_STOP_CELL_0_O                         : out std_logic_vector(9 downto 0);     
    DRS_STOP_CELL_0_BIT_TRIG_O                : out std_logic_vector(9 downto 0);
    DRS_STOP_CELL_1_I                         : in  std_logic_vector(9 downto 0);     
    DRS_STOP_CELL_1_O                         : out std_logic_vector(9 downto 0);     
    DRS_STOP_CELL_1_BIT_TRIG_O                : out std_logic_vector(9 downto 0);

    -- Status Register 13 [0x0034]: DRS_STOP_WSR
    DRS_STOP_WSR_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    DRS_STOP_WSR_REG_READ_O                   : out std_logic;
    DRS_STOP_WSR_REG_WRITE_O                  : out std_logic;
    DRS_STOP_WSR_0_I                          : in  std_logic_vector(7 downto 0);     
    DRS_STOP_WSR_0_O                          : out std_logic_vector(7 downto 0);     
    DRS_STOP_WSR_0_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);
    DRS_STOP_WSR_1_I                          : in  std_logic_vector(7 downto 0);     
    DRS_STOP_WSR_1_O                          : out std_logic_vector(7 downto 0);     
    DRS_STOP_WSR_1_BIT_TRIG_O                 : out std_logic_vector(7 downto 0);

    -- Status Register 14 [0x0038]: DRS_SAMPLE_FREQ
    DRS_SAMPLE_FREQ_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    DRS_SAMPLE_FREQ_REG_READ_O                : out std_logic;
    DRS_SAMPLE_FREQ_REG_WRITE_O               : out std_logic;
    DRS_SAMPLE_FREQ_I                         : in  std_logic_vector(23 downto 0);    
    DRS_SAMPLE_FREQ_O                         : out std_logic_vector(23 downto 0);    
    DRS_SAMPLE_FREQ_BIT_TRIG_O                : out std_logic_vector(23 downto 0);

    -- Status Register 15 [0x003C]: ADC_SAMPLE_FREQ
    ADC_SAMPLE_FREQ_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    ADC_SAMPLE_FREQ_REG_READ_O                : out std_logic;
    ADC_SAMPLE_FREQ_REG_WRITE_O               : out std_logic;
    ADC_SAMPLE_FREQ_I                         : in  std_logic_vector(23 downto 0);    
    ADC_SAMPLE_FREQ_O                         : out std_logic_vector(23 downto 0);    
    ADC_SAMPLE_FREQ_BIT_TRIG_O                : out std_logic_vector(23 downto 0);

    -- Status Register 16 [0x0040]: TDC_SAMPLE_FREQ
    TDC_SAMPLE_FREQ_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    TDC_SAMPLE_FREQ_REG_READ_O                : out std_logic;
    TDC_SAMPLE_FREQ_REG_WRITE_O               : out std_logic;
    TDC_SAMPLE_FREQ_I                         : in  std_logic_vector(23 downto 0);    
    TDC_SAMPLE_FREQ_O                         : out std_logic_vector(23 downto 0);    
    TDC_SAMPLE_FREQ_BIT_TRIG_O                : out std_logic_vector(23 downto 0);

    -- Status Register 17 [0x0044]: HV_VER
    HV_VER_BYTE_TRIG_O                        : out std_logic_vector(3 downto 0);
    HV_VER_REG_READ_O                         : out std_logic;
    HV_VER_REG_WRITE_O                        : out std_logic;
    HV_VER_I                                  : in  std_logic_vector(31 downto 0);    
    HV_VER_O                                  : out std_logic_vector(31 downto 0);    
    HV_VER_BIT_TRIG_O                         : out std_logic_vector(31 downto 0);

    -- Status Register 18 [0x0048]: HV_I_MEAS_0
    HV_I_MEAS_0_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_0_REG_READ_O                    : out std_logic;
    HV_I_MEAS_0_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_0_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_0_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_0_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 19 [0x004C]: HV_I_MEAS_1
    HV_I_MEAS_1_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_1_REG_READ_O                    : out std_logic;
    HV_I_MEAS_1_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_1_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_1_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_1_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 20 [0x0050]: HV_I_MEAS_2
    HV_I_MEAS_2_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_2_REG_READ_O                    : out std_logic;
    HV_I_MEAS_2_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_2_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_2_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_2_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 21 [0x0054]: HV_I_MEAS_3
    HV_I_MEAS_3_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_3_REG_READ_O                    : out std_logic;
    HV_I_MEAS_3_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_3_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_3_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_3_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 22 [0x0058]: HV_I_MEAS_4
    HV_I_MEAS_4_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_4_REG_READ_O                    : out std_logic;
    HV_I_MEAS_4_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_4_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_4_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_4_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 23 [0x005C]: HV_I_MEAS_5
    HV_I_MEAS_5_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_5_REG_READ_O                    : out std_logic;
    HV_I_MEAS_5_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_5_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_5_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_5_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 24 [0x0060]: HV_I_MEAS_6
    HV_I_MEAS_6_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_6_REG_READ_O                    : out std_logic;
    HV_I_MEAS_6_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_6_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_6_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_6_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 25 [0x0064]: HV_I_MEAS_7
    HV_I_MEAS_7_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_7_REG_READ_O                    : out std_logic;
    HV_I_MEAS_7_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_7_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_7_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_7_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 26 [0x0068]: HV_I_MEAS_8
    HV_I_MEAS_8_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_8_REG_READ_O                    : out std_logic;
    HV_I_MEAS_8_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_8_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_8_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_8_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 27 [0x006C]: HV_I_MEAS_9
    HV_I_MEAS_9_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_I_MEAS_9_REG_READ_O                    : out std_logic;
    HV_I_MEAS_9_REG_WRITE_O                   : out std_logic;
    HV_I_MEAS_9_I                             : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_9_O                             : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_9_BIT_TRIG_O                    : out std_logic_vector(31 downto 0);

    -- Status Register 28 [0x0070]: HV_I_MEAS_10
    HV_I_MEAS_10_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    HV_I_MEAS_10_REG_READ_O                   : out std_logic;
    HV_I_MEAS_10_REG_WRITE_O                  : out std_logic;
    HV_I_MEAS_10_I                            : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_10_O                            : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_10_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 29 [0x0074]: HV_I_MEAS_11
    HV_I_MEAS_11_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    HV_I_MEAS_11_REG_READ_O                   : out std_logic;
    HV_I_MEAS_11_REG_WRITE_O                  : out std_logic;
    HV_I_MEAS_11_I                            : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_11_O                            : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_11_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 30 [0x0078]: HV_I_MEAS_12
    HV_I_MEAS_12_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    HV_I_MEAS_12_REG_READ_O                   : out std_logic;
    HV_I_MEAS_12_REG_WRITE_O                  : out std_logic;
    HV_I_MEAS_12_I                            : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_12_O                            : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_12_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 31 [0x007C]: HV_I_MEAS_13
    HV_I_MEAS_13_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    HV_I_MEAS_13_REG_READ_O                   : out std_logic;
    HV_I_MEAS_13_REG_WRITE_O                  : out std_logic;
    HV_I_MEAS_13_I                            : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_13_O                            : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_13_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 32 [0x0080]: HV_I_MEAS_14
    HV_I_MEAS_14_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    HV_I_MEAS_14_REG_READ_O                   : out std_logic;
    HV_I_MEAS_14_REG_WRITE_O                  : out std_logic;
    HV_I_MEAS_14_I                            : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_14_O                            : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_14_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 33 [0x0084]: HV_I_MEAS_15
    HV_I_MEAS_15_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    HV_I_MEAS_15_REG_READ_O                   : out std_logic;
    HV_I_MEAS_15_REG_WRITE_O                  : out std_logic;
    HV_I_MEAS_15_I                            : in  std_logic_vector(31 downto 0);    
    HV_I_MEAS_15_O                            : out std_logic_vector(31 downto 0);    
    HV_I_MEAS_15_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 34 [0x0088]: HV_U_BASE_MEAS
    HV_U_BASE_MEAS_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    HV_U_BASE_MEAS_REG_READ_O                 : out std_logic;
    HV_U_BASE_MEAS_REG_WRITE_O                : out std_logic;
    HV_U_BASE_MEAS_I                          : in  std_logic_vector(31 downto 0);    
    HV_U_BASE_MEAS_O                          : out std_logic_vector(31 downto 0);    
    HV_U_BASE_MEAS_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 35 [0x008C]: HV_TEMP_0
    HV_TEMP_0_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    HV_TEMP_0_REG_READ_O                      : out std_logic;
    HV_TEMP_0_REG_WRITE_O                     : out std_logic;
    HV_TEMP_0_I                               : in  std_logic_vector(31 downto 0);    
    HV_TEMP_0_O                               : out std_logic_vector(31 downto 0);    
    HV_TEMP_0_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 36 [0x0090]: HV_TEMP_1
    HV_TEMP_1_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    HV_TEMP_1_REG_READ_O                      : out std_logic;
    HV_TEMP_1_REG_WRITE_O                     : out std_logic;
    HV_TEMP_1_I                               : in  std_logic_vector(31 downto 0);    
    HV_TEMP_1_O                               : out std_logic_vector(31 downto 0);    
    HV_TEMP_1_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 37 [0x0094]: HV_TEMP_2
    HV_TEMP_2_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    HV_TEMP_2_REG_READ_O                      : out std_logic;
    HV_TEMP_2_REG_WRITE_O                     : out std_logic;
    HV_TEMP_2_I                               : in  std_logic_vector(31 downto 0);    
    HV_TEMP_2_O                               : out std_logic_vector(31 downto 0);    
    HV_TEMP_2_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 38 [0x0098]: HV_TEMP_3
    HV_TEMP_3_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    HV_TEMP_3_REG_READ_O                      : out std_logic;
    HV_TEMP_3_REG_WRITE_O                     : out std_logic;
    HV_TEMP_3_I                               : in  std_logic_vector(31 downto 0);    
    HV_TEMP_3_O                               : out std_logic_vector(31 downto 0);    
    HV_TEMP_3_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 39 [0x009C]: SCALER_0
    SCALER_0_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_0_REG_READ_O                       : out std_logic;
    SCALER_0_REG_WRITE_O                      : out std_logic;
    SCALER_0_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_0_O                                : out std_logic_vector(31 downto 0);    
    SCALER_0_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 40 [0x00A0]: SCALER_1
    SCALER_1_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_1_REG_READ_O                       : out std_logic;
    SCALER_1_REG_WRITE_O                      : out std_logic;
    SCALER_1_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_1_O                                : out std_logic_vector(31 downto 0);    
    SCALER_1_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 41 [0x00A4]: SCALER_2
    SCALER_2_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_2_REG_READ_O                       : out std_logic;
    SCALER_2_REG_WRITE_O                      : out std_logic;
    SCALER_2_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_2_O                                : out std_logic_vector(31 downto 0);    
    SCALER_2_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 42 [0x00A8]: SCALER_3
    SCALER_3_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_3_REG_READ_O                       : out std_logic;
    SCALER_3_REG_WRITE_O                      : out std_logic;
    SCALER_3_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_3_O                                : out std_logic_vector(31 downto 0);    
    SCALER_3_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 43 [0x00AC]: SCALER_4
    SCALER_4_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_4_REG_READ_O                       : out std_logic;
    SCALER_4_REG_WRITE_O                      : out std_logic;
    SCALER_4_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_4_O                                : out std_logic_vector(31 downto 0);    
    SCALER_4_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 44 [0x00B0]: SCALER_5
    SCALER_5_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_5_REG_READ_O                       : out std_logic;
    SCALER_5_REG_WRITE_O                      : out std_logic;
    SCALER_5_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_5_O                                : out std_logic_vector(31 downto 0);    
    SCALER_5_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 45 [0x00B4]: SCALER_6
    SCALER_6_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_6_REG_READ_O                       : out std_logic;
    SCALER_6_REG_WRITE_O                      : out std_logic;
    SCALER_6_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_6_O                                : out std_logic_vector(31 downto 0);    
    SCALER_6_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 46 [0x00B8]: SCALER_7
    SCALER_7_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_7_REG_READ_O                       : out std_logic;
    SCALER_7_REG_WRITE_O                      : out std_logic;
    SCALER_7_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_7_O                                : out std_logic_vector(31 downto 0);    
    SCALER_7_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 47 [0x00BC]: SCALER_8
    SCALER_8_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_8_REG_READ_O                       : out std_logic;
    SCALER_8_REG_WRITE_O                      : out std_logic;
    SCALER_8_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_8_O                                : out std_logic_vector(31 downto 0);    
    SCALER_8_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 48 [0x00C0]: SCALER_9
    SCALER_9_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    SCALER_9_REG_READ_O                       : out std_logic;
    SCALER_9_REG_WRITE_O                      : out std_logic;
    SCALER_9_I                                : in  std_logic_vector(31 downto 0);    
    SCALER_9_O                                : out std_logic_vector(31 downto 0);    
    SCALER_9_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 49 [0x00C4]: SCALER_10
    SCALER_10_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    SCALER_10_REG_READ_O                      : out std_logic;
    SCALER_10_REG_WRITE_O                     : out std_logic;
    SCALER_10_I                               : in  std_logic_vector(31 downto 0);    
    SCALER_10_O                               : out std_logic_vector(31 downto 0);    
    SCALER_10_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 50 [0x00C8]: SCALER_11
    SCALER_11_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    SCALER_11_REG_READ_O                      : out std_logic;
    SCALER_11_REG_WRITE_O                     : out std_logic;
    SCALER_11_I                               : in  std_logic_vector(31 downto 0);    
    SCALER_11_O                               : out std_logic_vector(31 downto 0);    
    SCALER_11_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 51 [0x00CC]: SCALER_12
    SCALER_12_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    SCALER_12_REG_READ_O                      : out std_logic;
    SCALER_12_REG_WRITE_O                     : out std_logic;
    SCALER_12_I                               : in  std_logic_vector(31 downto 0);    
    SCALER_12_O                               : out std_logic_vector(31 downto 0);    
    SCALER_12_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 52 [0x00D0]: SCALER_13
    SCALER_13_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    SCALER_13_REG_READ_O                      : out std_logic;
    SCALER_13_REG_WRITE_O                     : out std_logic;
    SCALER_13_I                               : in  std_logic_vector(31 downto 0);    
    SCALER_13_O                               : out std_logic_vector(31 downto 0);    
    SCALER_13_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 53 [0x00D4]: SCALER_14
    SCALER_14_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    SCALER_14_REG_READ_O                      : out std_logic;
    SCALER_14_REG_WRITE_O                     : out std_logic;
    SCALER_14_I                               : in  std_logic_vector(31 downto 0);    
    SCALER_14_O                               : out std_logic_vector(31 downto 0);    
    SCALER_14_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 54 [0x00D8]: SCALER_15
    SCALER_15_BYTE_TRIG_O                     : out std_logic_vector(3 downto 0);
    SCALER_15_REG_READ_O                      : out std_logic;
    SCALER_15_REG_WRITE_O                     : out std_logic;
    SCALER_15_I                               : in  std_logic_vector(31 downto 0);    
    SCALER_15_O                               : out std_logic_vector(31 downto 0);    
    SCALER_15_BIT_TRIG_O                      : out std_logic_vector(31 downto 0);

    -- Status Register 55 [0x00DC]: SCALER_PTRN_TRG
    SCALER_PTRN_TRG_BYTE_TRIG_O               : out std_logic_vector(3 downto 0);
    SCALER_PTRN_TRG_REG_READ_O                : out std_logic;
    SCALER_PTRN_TRG_REG_WRITE_O               : out std_logic;
    SCALER_PTRN_TRG_I                         : in  std_logic_vector(31 downto 0);    
    SCALER_PTRN_TRG_O                         : out std_logic_vector(31 downto 0);    
    SCALER_PTRN_TRG_BIT_TRIG_O                : out std_logic_vector(31 downto 0);

    -- Status Register 56 [0x00E0]: SCALER_EXT_TRG
    SCALER_EXT_TRG_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    SCALER_EXT_TRG_REG_READ_O                 : out std_logic;
    SCALER_EXT_TRG_REG_WRITE_O                : out std_logic;
    SCALER_EXT_TRG_I                          : in  std_logic_vector(31 downto 0);    
    SCALER_EXT_TRG_O                          : out std_logic_vector(31 downto 0);    
    SCALER_EXT_TRG_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 57 [0x00E4]: SCALER_EXT_CLK
    SCALER_EXT_CLK_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    SCALER_EXT_CLK_REG_READ_O                 : out std_logic;
    SCALER_EXT_CLK_REG_WRITE_O                : out std_logic;
    SCALER_EXT_CLK_I                          : in  std_logic_vector(31 downto 0);    
    SCALER_EXT_CLK_O                          : out std_logic_vector(31 downto 0);    
    SCALER_EXT_CLK_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 58 [0x00E8]: SCALER_TIME_STAMP_LSB
    SCALER_TIME_STAMP_LSB_BYTE_TRIG_O         : out std_logic_vector(3 downto 0);
    SCALER_TIME_STAMP_LSB_REG_READ_O          : out std_logic;
    SCALER_TIME_STAMP_LSB_REG_WRITE_O         : out std_logic;
    SCALER_TIME_STAMP_LSB_I                   : in  std_logic_vector(31 downto 0);    
    SCALER_TIME_STAMP_LSB_O                   : out std_logic_vector(31 downto 0);    
    SCALER_TIME_STAMP_LSB_BIT_TRIG_O          : out std_logic_vector(31 downto 0);

    -- Status Register 59 [0x00EC]: SCALER_TIME_STAMP_MSB
    SCALER_TIME_STAMP_MSB_BYTE_TRIG_O         : out std_logic_vector(3 downto 0);
    SCALER_TIME_STAMP_MSB_REG_READ_O          : out std_logic;
    SCALER_TIME_STAMP_MSB_REG_WRITE_O         : out std_logic;
    SCALER_TIME_STAMP_MSB_I                   : in  std_logic_vector(31 downto 0);    
    SCALER_TIME_STAMP_MSB_O                   : out std_logic_vector(31 downto 0);    
    SCALER_TIME_STAMP_MSB_BIT_TRIG_O          : out std_logic_vector(31 downto 0);

    -- Status Register 60 [0x00F0]: TIME_LSB
    TIME_LSB_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    TIME_LSB_REG_READ_O                       : out std_logic;
    TIME_LSB_REG_WRITE_O                      : out std_logic;
    TIME_LSB_I                                : in  std_logic_vector(31 downto 0);    
    TIME_LSB_O                                : out std_logic_vector(31 downto 0);    
    TIME_LSB_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 61 [0x00F4]: TIME_MSB
    TIME_MSB_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    TIME_MSB_REG_READ_O                       : out std_logic;
    TIME_MSB_REG_WRITE_O                      : out std_logic;
    TIME_MSB_I                                : in  std_logic_vector(31 downto 0);    
    TIME_MSB_O                                : out std_logic_vector(31 downto 0);    
    TIME_MSB_BIT_TRIG_O                       : out std_logic_vector(31 downto 0);

    -- Status Register 62 [0x00F8]: COMP_CH_STAT
    COMP_CH_STAT_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    COMP_CH_STAT_REG_READ_O                   : out std_logic;
    COMP_CH_STAT_REG_WRITE_O                  : out std_logic;
    COMP_CH_STAT_I                            : in  std_logic_vector(15 downto 0);    
    COMP_CH_STAT_O                            : out std_logic_vector(15 downto 0);    
    COMP_CH_STAT_BIT_TRIG_O                   : out std_logic_vector(15 downto 0);

    -- Status Register 63 [0x00FC]: EVENT_TX_RATE
    EVENT_TX_RATE_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    EVENT_TX_RATE_REG_READ_O                  : out std_logic;
    EVENT_TX_RATE_REG_WRITE_O                 : out std_logic;
    EVENT_TX_RATE_I                           : in  std_logic_vector(31 downto 0);    
    EVENT_TX_RATE_O                           : out std_logic_vector(31 downto 0);    
    EVENT_TX_RATE_BIT_TRIG_O                  : out std_logic_vector(31 downto 0);

    -- Status Register 64 [0x0100]: EVENT_NR
    EVENT_NR_BYTE_TRIG_O                      : out std_logic_vector(3 downto 0);
    EVENT_NR_REG_READ_O                       : out std_logic;
    EVENT_NR_REG_WRITE_O                      : out std_logic;
    EVENT_NUMBER_I                            : in  std_logic_vector(31 downto 0);    
    EVENT_NUMBER_O                            : out std_logic_vector(31 downto 0);    
    EVENT_NUMBER_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 65 [0x0104]: TRB_INFO_STAT
    TRB_INFO_STAT_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    TRB_INFO_STAT_REG_READ_O                  : out std_logic;
    TRB_INFO_STAT_REG_WRITE_O                 : out std_logic;
    TRB_FLAG_NEW_I                            : in  std_logic;                        
    TRB_FLAG_NEW_O                            : out std_logic;                        
    TRB_FLAG_NEW_BIT_TRIG_O                   : out std_logic;
    TRB_FLAG_PARITY_ERROR_I                   : in  std_logic;                        
    TRB_FLAG_PARITY_ERROR_O                   : out std_logic;                        
    TRB_FLAG_PARITY_ERROR_BIT_TRIG_O          : out std_logic;
    TRB_PARITY_ERROR_COUNT_I                  : in  std_logic_vector(15 downto 0);    
    TRB_PARITY_ERROR_COUNT_O                  : out std_logic_vector(15 downto 0);    
    TRB_PARITY_ERROR_COUNT_BIT_TRIG_O         : out std_logic_vector(15 downto 0);

    -- Status Register 66 [0x0108]: TRB_INFO_LSB
    TRB_INFO_LSB_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    TRB_INFO_LSB_REG_READ_O                   : out std_logic;
    TRB_INFO_LSB_REG_WRITE_O                  : out std_logic;
    TRB_INFO_LSB_I                            : in  std_logic_vector(31 downto 0);    
    TRB_INFO_LSB_O                            : out std_logic_vector(31 downto 0);    
    TRB_INFO_LSB_BIT_TRIG_O                   : out std_logic_vector(31 downto 0);

    -- Status Register 67 [0x010C]: TRB_INFO_MSB
    TRB_INFO_MSB_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    TRB_INFO_MSB_REG_READ_O                   : out std_logic;
    TRB_INFO_MSB_REG_WRITE_O                  : out std_logic;
    TRB_INFO_MSB_I                            : in  std_logic_vector(15 downto 0);    
    TRB_INFO_MSB_O                            : out std_logic_vector(15 downto 0);    
    TRB_INFO_MSB_BIT_TRIG_O                   : out std_logic_vector(15 downto 0);

    -- Status Register 68 [0x0110]: ADV_TRG_TRIG_CELL
    ADV_TRG_TRIG_CELL_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    ADV_TRG_TRIG_CELL_REG_READ_O              : out std_logic;
    ADV_TRG_TRIG_CELL_REG_WRITE_O             : out std_logic;
    ADV_TRG_TRIG_CELL_I                       : in  std_logic_vector(31 downto 0);    
    ADV_TRG_TRIG_CELL_O                       : out std_logic_vector(31 downto 0);    
    ADV_TRG_TRIG_CELL_BIT_TRIG_O              : out std_logic_vector(31 downto 0);

    -- Status Register 69 [0x0114]: ADV_TRG_STAT1
    ADV_TRG_STAT1_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_STAT1_REG_READ_O                  : out std_logic;
    ADV_TRG_STAT1_REG_WRITE_O                 : out std_logic;
    ADV_TRG_STAT_1_I                          : in  std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_1_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_1_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 70 [0x0118]: ADV_TRG_STAT2
    ADV_TRG_STAT2_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_STAT2_REG_READ_O                  : out std_logic;
    ADV_TRG_STAT2_REG_WRITE_O                 : out std_logic;
    ADV_TRG_STAT_2_I                          : in  std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_2_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_2_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 71 [0x011C]: ADV_TRG_STAT3
    ADV_TRG_STAT3_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_STAT3_REG_READ_O                  : out std_logic;
    ADV_TRG_STAT3_REG_WRITE_O                 : out std_logic;
    ADV_TRG_STAT_3_I                          : in  std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_3_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_3_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 72 [0x0120]: ADV_TRG_STAT4
    ADV_TRG_STAT4_BYTE_TRIG_O                 : out std_logic_vector(3 downto 0);
    ADV_TRG_STAT4_REG_READ_O                  : out std_logic;
    ADV_TRG_STAT4_REG_WRITE_O                 : out std_logic;
    ADV_TRG_STAT_4_I                          : in  std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_4_O                          : out std_logic_vector(31 downto 0);    
    ADV_TRG_STAT_4_BIT_TRIG_O                 : out std_logic_vector(31 downto 0);

    -- Status Register 73 [0x0124]: MAX_DRS_ADC_PKT_SAMPLES
    MAX_DRS_ADC_PKT_SAMPLES_BYTE_TRIG_O       : out std_logic_vector(3 downto 0);
    MAX_DRS_ADC_PKT_SAMPLES_REG_READ_O        : out std_logic;
    MAX_DRS_ADC_PKT_SAMPLES_REG_WRITE_O       : out std_logic;
    MAX_DRS_ADC_PKT_SAMPLES_I                 : in  std_logic_vector(15 downto 0);    
    MAX_DRS_ADC_PKT_SAMPLES_O                 : out std_logic_vector(15 downto 0);    
    MAX_DRS_ADC_PKT_SAMPLES_BIT_TRIG_O        : out std_logic_vector(15 downto 0);

    -- Status Register 74 [0x0128]: MAX_TDC_PKT_SAMPLES
    MAX_TDC_PKT_SAMPLES_BYTE_TRIG_O           : out std_logic_vector(3 downto 0);
    MAX_TDC_PKT_SAMPLES_REG_READ_O            : out std_logic;
    MAX_TDC_PKT_SAMPLES_REG_WRITE_O           : out std_logic;
    MAX_TDC_PKT_SAMPLES_I                     : in  std_logic_vector(17 downto 0);    
    MAX_TDC_PKT_SAMPLES_O                     : out std_logic_vector(17 downto 0);    
    MAX_TDC_PKT_SAMPLES_BIT_TRIG_O            : out std_logic_vector(17 downto 0);

    -- Status Register 75 [0x012C]: MAX_TRG_PKT_SAMPLES
    MAX_TRG_PKT_SAMPLES_BYTE_TRIG_O           : out std_logic_vector(3 downto 0);
    MAX_TRG_PKT_SAMPLES_REG_READ_O            : out std_logic;
    MAX_TRG_PKT_SAMPLES_REG_WRITE_O           : out std_logic;
    MAX_TRG_PKT_SAMPLES_I                     : in  std_logic_vector(15 downto 0);    
    MAX_TRG_PKT_SAMPLES_O                     : out std_logic_vector(15 downto 0);    
    MAX_TRG_PKT_SAMPLES_BIT_TRIG_O            : out std_logic_vector(15 downto 0);

    -- Status Register 76 [0x0130]: MAX_SCL_PKT_SAMPLES
    MAX_SCL_PKT_SAMPLES_BYTE_TRIG_O           : out std_logic_vector(3 downto 0);
    MAX_SCL_PKT_SAMPLES_REG_READ_O            : out std_logic;
    MAX_SCL_PKT_SAMPLES_REG_WRITE_O           : out std_logic;
    MAX_SCL_PKT_SAMPLES_I                     : in  std_logic_vector(15 downto 0);    
    MAX_SCL_PKT_SAMPLES_O                     : out std_logic_vector(15 downto 0);    
    MAX_SCL_PKT_SAMPLES_BIT_TRIG_O            : out std_logic_vector(15 downto 0);

    -- Status Register 77 [0x0134]: CLK_CTRL_MOD_FLAG
    CLK_CTRL_MOD_FLAG_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    CLK_CTRL_MOD_FLAG_REG_READ_O              : out std_logic;
    CLK_CTRL_MOD_FLAG_REG_WRITE_O             : out std_logic;
    TRIGGER_DAQ_CLK_CAL_MOD_I                 : in  std_logic;                        
    TRIGGER_DAQ_CLK_CAL_MOD_O                 : out std_logic;                        
    TRIGGER_DAQ_CLK_CAL_MOD_BIT_TRIG_O        : out std_logic;
    ADC_RST_MOD_I                             : in  std_logic;                        
    ADC_RST_MOD_O                             : out std_logic;                        
    ADC_RST_MOD_BIT_TRIG_O                    : out std_logic;
    CLK_SEL_AND_DRS_CLK_DIV_MOD_I             : in  std_logic;                        
    CLK_SEL_AND_DRS_CLK_DIV_MOD_O             : out std_logic;                        
    CLK_SEL_AND_DRS_CLK_DIV_MOD_BIT_TRIG_O    : out std_logic;
    EXT_CLK_FREQ_MOD_I                        : in  std_logic;                        
    EXT_CLK_FREQ_MOD_O                        : out std_logic;                        
    EXT_CLK_FREQ_MOD_BIT_TRIG_O               : out std_logic;
    LOCAL_CLK_FREQ_MOD_I                      : in  std_logic;                        
    LOCAL_CLK_FREQ_MOD_O                      : out std_logic;                        
    LOCAL_CLK_FREQ_MOD_BIT_TRIG_O             : out std_logic;

    -- Status Register 78 [0x0138]: DRS_MOD_FLAG
    DRS_MOD_FLAG_BYTE_TRIG_O                  : out std_logic_vector(3 downto 0);
    DRS_MOD_FLAG_REG_READ_O                   : out std_logic;
    DRS_MOD_FLAG_REG_WRITE_O                  : out std_logic;
    DRS_CTRL_MOD_I                            : in  std_logic;                        
    DRS_CTRL_MOD_O                            : out std_logic;                        
    DRS_CTRL_MOD_BIT_TRIG_O                   : out std_logic;
    DRS_WSR_MOD_I                             : in  std_logic;                        
    DRS_WSR_MOD_O                             : out std_logic;                        
    DRS_WSR_MOD_BIT_TRIG_O                    : out std_logic;
    DRS_WCR_MOD_I                             : in  std_logic;                        
    DRS_WCR_MOD_O                             : out std_logic;                        
    DRS_WCR_MOD_BIT_TRIG_O                    : out std_logic;

    -- Status Register 79 [0x013C]: COM_PLD_SIZE_MOD_FLAG
    COM_PLD_SIZE_MOD_FLAG_BYTE_TRIG_O         : out std_logic_vector(3 downto 0);
    COM_PLD_SIZE_MOD_FLAG_REG_READ_O          : out std_logic;
    COM_PLD_SIZE_MOD_FLAG_REG_WRITE_O         : out std_logic;
    COM_PLD_SIZE_MOD_I                        : in  std_logic_vector(3 downto 0);     
    COM_PLD_SIZE_MOD_O                        : out std_logic_vector(3 downto 0);     
    COM_PLD_SIZE_MOD_BIT_TRIG_O               : out std_logic_vector(3 downto 0);

    -- Status Register 80 [0x0140]: ADC_SAMPLE_DIV_MOD_FLAG
    ADC_SAMPLE_DIV_MOD_FLAG_BYTE_TRIG_O       : out std_logic_vector(3 downto 0);
    ADC_SAMPLE_DIV_MOD_FLAG_REG_READ_O        : out std_logic;
    ADC_SAMPLE_DIV_MOD_FLAG_REG_WRITE_O       : out std_logic;
    ADC_SAMPLE_DIV_MOD_I                      : in  std_logic_vector(3 downto 0);     
    ADC_SAMPLE_DIV_MOD_O                      : out std_logic_vector(3 downto 0);     
    ADC_SAMPLE_DIV_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);

    -- Status Register 81 [0x0144]: DAC_0_1_MOD_FLAG
    DAC_0_1_MOD_FLAG_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    DAC_0_1_MOD_FLAG_REG_READ_O               : out std_logic;
    DAC_0_1_MOD_FLAG_REG_WRITE_O              : out std_logic;
    DAC0_A_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_A_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_A_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_B_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_B_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_B_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_C_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_C_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_C_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_D_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_D_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_D_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_E_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_E_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_E_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_F_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_F_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_F_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_G_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_G_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_G_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC0_H_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC0_H_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC0_H_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_A_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_A_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_A_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_B_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_B_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_B_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_C_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_C_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_C_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_D_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_D_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_D_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_E_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_E_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_E_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_F_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_F_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_F_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_G_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_G_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_G_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC1_H_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC1_H_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC1_H_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);

    -- Status Register 82 [0x0148]: DAC_2_MOD_FLAG
    DAC_2_MOD_FLAG_BYTE_TRIG_O                : out std_logic_vector(3 downto 0);
    DAC_2_MOD_FLAG_REG_READ_O                 : out std_logic;
    DAC_2_MOD_FLAG_REG_WRITE_O                : out std_logic;
    DAC2_A_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_A_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_A_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_B_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_B_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_B_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_C_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_C_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_C_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_D_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_D_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_D_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_E_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_E_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_E_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_F_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_F_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_F_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_G_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_G_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_G_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);
    DAC2_H_MOD_I                              : in  std_logic_vector(1 downto 0);     
    DAC2_H_MOD_O                              : out std_logic_vector(1 downto 0);     
    DAC2_H_MOD_BIT_TRIG_O                     : out std_logic_vector(1 downto 0);

    -- Status Register 83 [0x014C]: FE_0_15_MOD_FLAG
    FE_0_15_MOD_FLAG_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    FE_0_15_MOD_FLAG_REG_READ_O               : out std_logic;
    FE_0_15_MOD_FLAG_REG_WRITE_O              : out std_logic;
    FE_0_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_0_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_0_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_1_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_1_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_1_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_2_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_2_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_2_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_3_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_3_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_3_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_4_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_4_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_4_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_5_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_5_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_5_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_6_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_6_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_6_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_7_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_7_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_7_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_8_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_8_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_8_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_9_MOD_I                                : in  std_logic_vector(1 downto 0);     
    FE_9_MOD_O                                : out std_logic_vector(1 downto 0);     
    FE_9_MOD_BIT_TRIG_O                       : out std_logic_vector(1 downto 0);
    FE_10_MOD_I                               : in  std_logic_vector(1 downto 0);     
    FE_10_MOD_O                               : out std_logic_vector(1 downto 0);     
    FE_10_MOD_BIT_TRIG_O                      : out std_logic_vector(1 downto 0);
    FE_11_MOD_I                               : in  std_logic_vector(1 downto 0);     
    FE_11_MOD_O                               : out std_logic_vector(1 downto 0);     
    FE_11_MOD_BIT_TRIG_O                      : out std_logic_vector(1 downto 0);
    FE_12_MOD_I                               : in  std_logic_vector(1 downto 0);     
    FE_12_MOD_O                               : out std_logic_vector(1 downto 0);     
    FE_12_MOD_BIT_TRIG_O                      : out std_logic_vector(1 downto 0);
    FE_13_MOD_I                               : in  std_logic_vector(1 downto 0);     
    FE_13_MOD_O                               : out std_logic_vector(1 downto 0);     
    FE_13_MOD_BIT_TRIG_O                      : out std_logic_vector(1 downto 0);
    FE_14_MOD_I                               : in  std_logic_vector(1 downto 0);     
    FE_14_MOD_O                               : out std_logic_vector(1 downto 0);     
    FE_14_MOD_BIT_TRIG_O                      : out std_logic_vector(1 downto 0);
    FE_15_MOD_I                               : in  std_logic_vector(1 downto 0);     
    FE_15_MOD_O                               : out std_logic_vector(1 downto 0);     
    FE_15_MOD_BIT_TRIG_O                      : out std_logic_vector(1 downto 0);

    -- Status Register 84 [0x0150]: HV_U_TARGET_0_7_MOD_FLAG
    HV_U_TARGET_0_7_MOD_FLAG_BYTE_TRIG_O      : out std_logic_vector(3 downto 0);
    HV_U_TARGET_0_7_MOD_FLAG_REG_READ_O       : out std_logic;
    HV_U_TARGET_0_7_MOD_FLAG_REG_WRITE_O      : out std_logic;
    HV_U_TARGET_0_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_0_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_0_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_1_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_1_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_1_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_2_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_2_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_2_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_3_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_3_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_3_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_4_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_4_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_4_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_5_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_5_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_5_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_6_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_6_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_6_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_7_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_7_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_7_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);

    -- Status Register 85 [0x0154]: HV_U_TARGET_8_15_MOD_FLAG
    HV_U_TARGET_8_15_MOD_FLAG_BYTE_TRIG_O     : out std_logic_vector(3 downto 0);
    HV_U_TARGET_8_15_MOD_FLAG_REG_READ_O      : out std_logic;
    HV_U_TARGET_8_15_MOD_FLAG_REG_WRITE_O     : out std_logic;
    HV_U_TARGET_8_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_8_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_8_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_9_MOD_I                       : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_9_MOD_O                       : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_9_MOD_BIT_TRIG_O              : out std_logic_vector(3 downto 0);
    HV_U_TARGET_10_MOD_I                      : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_10_MOD_O                      : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_10_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);
    HV_U_TARGET_11_MOD_I                      : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_11_MOD_O                      : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_11_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);
    HV_U_TARGET_12_MOD_I                      : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_12_MOD_O                      : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_12_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);
    HV_U_TARGET_13_MOD_I                      : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_13_MOD_O                      : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_13_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);
    HV_U_TARGET_14_MOD_I                      : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_14_MOD_O                      : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_14_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);
    HV_U_TARGET_15_MOD_I                      : in  std_logic_vector(3 downto 0);     
    HV_U_TARGET_15_MOD_O                      : out std_logic_vector(3 downto 0);     
    HV_U_TARGET_15_MOD_BIT_TRIG_O             : out std_logic_vector(3 downto 0);

    -- Status Register 86 [0x0158]: HV_MOD_FLAG
    HV_MOD_FLAG_BYTE_TRIG_O                   : out std_logic_vector(3 downto 0);
    HV_MOD_FLAG_REG_READ_O                    : out std_logic;
    HV_MOD_FLAG_REG_WRITE_O                   : out std_logic;
    HV_R_SHUNT_MOD_I                          : in  std_logic_vector(3 downto 0);     
    HV_R_SHUNT_MOD_O                          : out std_logic_vector(3 downto 0);     
    HV_R_SHUNT_MOD_BIT_TRIG_O                 : out std_logic_vector(3 downto 0);

    -- Status Register 87 [0x015C]: LMK_0_7_MOD_FLAG
    LMK_0_7_MOD_FLAG_BYTE_TRIG_O              : out std_logic_vector(3 downto 0);
    LMK_0_7_MOD_FLAG_REG_READ_O               : out std_logic;
    LMK_0_7_MOD_FLAG_REG_WRITE_O              : out std_logic;
    LMK_0_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_0_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_0_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_1_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_1_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_1_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_2_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_2_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_2_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_3_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_3_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_3_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_4_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_4_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_4_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_5_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_5_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_5_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_6_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_6_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_6_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_7_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_7_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_7_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);

    -- Status Register 88 [0x0160]: LMK_8_15_MOD_FLAG
    LMK_8_15_MOD_FLAG_BYTE_TRIG_O             : out std_logic_vector(3 downto 0);
    LMK_8_15_MOD_FLAG_REG_READ_O              : out std_logic;
    LMK_8_15_MOD_FLAG_REG_WRITE_O             : out std_logic;
    LMK_8_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_8_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_8_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_9_MOD_I                               : in  std_logic_vector(3 downto 0);     
    LMK_9_MOD_O                               : out std_logic_vector(3 downto 0);     
    LMK_9_MOD_BIT_TRIG_O                      : out std_logic_vector(3 downto 0);
    LMK_11_MOD_I                              : in  std_logic_vector(3 downto 0);     
    LMK_11_MOD_O                              : out std_logic_vector(3 downto 0);     
    LMK_11_MOD_BIT_TRIG_O                     : out std_logic_vector(3 downto 0);
    LMK_13_MOD_I                              : in  std_logic_vector(3 downto 0);     
    LMK_13_MOD_O                              : out std_logic_vector(3 downto 0);     
    LMK_13_MOD_BIT_TRIG_O                     : out std_logic_vector(3 downto 0);
    LMK_14_MOD_I                              : in  std_logic_vector(3 downto 0);     
    LMK_14_MOD_O                              : out std_logic_vector(3 downto 0);     
    LMK_14_MOD_BIT_TRIG_O                     : out std_logic_vector(3 downto 0);
    LMK_15_MOD_I                              : in  std_logic_vector(3 downto 0);     
    LMK_15_MOD_O                              : out std_logic_vector(3 downto 0);     
    LMK_15_MOD_BIT_TRIG_O                     : out std_logic_vector(3 downto 0);

  -- Register Bank Status Contents (Ext to uBlaze)
    STATUS_REG_O                              : out std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0);

  -- Register Bank Status Contents (uBlaze to Ext)
    STATUS_REG_I                              : in  std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0);

  -- Register Bank Read Bits
    STAT_REG_READ_I                           : in  std_logic_vector(CGN_NUM_STAT_REG-1 downto 0);

  -- Register Bank Write Bits
    STAT_REG_WRITE_I                          : in  std_logic_vector(CGN_NUM_STAT_REG-1 downto 0);

  -- Register Bank Bit Triggers
    STAT_BIT_TRIG_I                           : in  std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0);

  -- Register Bank Byte Triggers
    STAT_BYTE_TRIG_I                          : in  std_logic_vector((CGN_NUM_STAT_REG*CGN_BITS_PER_REG/8)-1 downto 0);

  -- Register Bank Control Contents
    CONTROL_REG_I                             : in  std_logic_vector(CGN_NUM_CTRL_REG*CGN_BITS_PER_REG-1 downto 0);

  -- Register Bank Read Bits
    CTRL_REG_READ_I                           : in  std_logic_vector(CGN_NUM_CTRL_REG-1 downto 0);

  -- Register Bank Write Bits
    CTRL_REG_WRITE_I                          : in  std_logic_vector(CGN_NUM_CTRL_REG-1 downto 0);

  -- Register Bank Bit Triggers
    CTRL_BIT_TRIG_I                           : in  std_logic_vector(CGN_NUM_CTRL_REG*CGN_BITS_PER_REG-1 downto 0);

  -- Register Bank Byte Triggers
    CTRL_BYTE_TRIG_I                          : in  std_logic_vector((CGN_NUM_CTRL_REG*CGN_BITS_PER_REG/8)-1 downto 0)
  );
end util_register_map;

architecture structural of util_register_map is

  constant C_CTRL_REG0_OFFSET : integer := 0*CGN_BITS_PER_REG;
  constant C_CTRL_REG1_OFFSET : integer := 1*CGN_BITS_PER_REG;
  constant C_CTRL_REG2_OFFSET : integer := 2*CGN_BITS_PER_REG;
  constant C_CTRL_REG3_OFFSET : integer := 3*CGN_BITS_PER_REG;
  constant C_CTRL_REG4_OFFSET : integer := 4*CGN_BITS_PER_REG;
  constant C_CTRL_REG5_OFFSET : integer := 5*CGN_BITS_PER_REG;
  constant C_CTRL_REG6_OFFSET : integer := 6*CGN_BITS_PER_REG;
  constant C_CTRL_REG7_OFFSET : integer := 7*CGN_BITS_PER_REG;
  constant C_CTRL_REG8_OFFSET : integer := 8*CGN_BITS_PER_REG;
  constant C_CTRL_REG9_OFFSET : integer := 9*CGN_BITS_PER_REG;
  constant C_CTRL_REG10_OFFSET : integer := 10*CGN_BITS_PER_REG;
  constant C_CTRL_REG11_OFFSET : integer := 11*CGN_BITS_PER_REG;
  constant C_CTRL_REG12_OFFSET : integer := 12*CGN_BITS_PER_REG;
  constant C_CTRL_REG13_OFFSET : integer := 13*CGN_BITS_PER_REG;
  constant C_CTRL_REG14_OFFSET : integer := 14*CGN_BITS_PER_REG;
  constant C_CTRL_REG15_OFFSET : integer := 15*CGN_BITS_PER_REG;
  constant C_CTRL_REG16_OFFSET : integer := 16*CGN_BITS_PER_REG;
  constant C_CTRL_REG17_OFFSET : integer := 17*CGN_BITS_PER_REG;
  constant C_CTRL_REG18_OFFSET : integer := 18*CGN_BITS_PER_REG;
  constant C_CTRL_REG19_OFFSET : integer := 19*CGN_BITS_PER_REG;
  constant C_CTRL_REG20_OFFSET : integer := 20*CGN_BITS_PER_REG;
  constant C_CTRL_REG21_OFFSET : integer := 21*CGN_BITS_PER_REG;
  constant C_CTRL_REG22_OFFSET : integer := 22*CGN_BITS_PER_REG;
  constant C_CTRL_REG23_OFFSET : integer := 23*CGN_BITS_PER_REG;
  constant C_CTRL_REG24_OFFSET : integer := 24*CGN_BITS_PER_REG;
  constant C_CTRL_REG25_OFFSET : integer := 25*CGN_BITS_PER_REG;
  constant C_CTRL_REG26_OFFSET : integer := 26*CGN_BITS_PER_REG;
  constant C_CTRL_REG27_OFFSET : integer := 27*CGN_BITS_PER_REG;
  constant C_CTRL_REG28_OFFSET : integer := 28*CGN_BITS_PER_REG;
  constant C_CTRL_REG29_OFFSET : integer := 29*CGN_BITS_PER_REG;
  constant C_CTRL_REG30_OFFSET : integer := 30*CGN_BITS_PER_REG;
  constant C_CTRL_REG31_OFFSET : integer := 31*CGN_BITS_PER_REG;
  constant C_CTRL_REG32_OFFSET : integer := 32*CGN_BITS_PER_REG;
  constant C_CTRL_REG33_OFFSET : integer := 33*CGN_BITS_PER_REG;
  constant C_CTRL_REG34_OFFSET : integer := 34*CGN_BITS_PER_REG;
  constant C_CTRL_REG35_OFFSET : integer := 35*CGN_BITS_PER_REG;
  constant C_CTRL_REG36_OFFSET : integer := 36*CGN_BITS_PER_REG;
  constant C_CTRL_REG37_OFFSET : integer := 37*CGN_BITS_PER_REG;
  constant C_CTRL_REG38_OFFSET : integer := 38*CGN_BITS_PER_REG;
  constant C_CTRL_REG39_OFFSET : integer := 39*CGN_BITS_PER_REG;
  constant C_CTRL_REG40_OFFSET : integer := 40*CGN_BITS_PER_REG;
  constant C_CTRL_REG41_OFFSET : integer := 41*CGN_BITS_PER_REG;
  constant C_CTRL_REG42_OFFSET : integer := 42*CGN_BITS_PER_REG;
  constant C_CTRL_REG43_OFFSET : integer := 43*CGN_BITS_PER_REG;
  constant C_CTRL_REG44_OFFSET : integer := 44*CGN_BITS_PER_REG;
  constant C_CTRL_REG45_OFFSET : integer := 45*CGN_BITS_PER_REG;
  constant C_CTRL_REG46_OFFSET : integer := 46*CGN_BITS_PER_REG;
  constant C_CTRL_REG47_OFFSET : integer := 47*CGN_BITS_PER_REG;
  constant C_CTRL_REG48_OFFSET : integer := 48*CGN_BITS_PER_REG;
  constant C_CTRL_REG49_OFFSET : integer := 49*CGN_BITS_PER_REG;
  constant C_CTRL_REG50_OFFSET : integer := 50*CGN_BITS_PER_REG;
  constant C_CTRL_REG51_OFFSET : integer := 51*CGN_BITS_PER_REG;
  constant C_CTRL_REG52_OFFSET : integer := 52*CGN_BITS_PER_REG;
  constant C_CTRL_REG53_OFFSET : integer := 53*CGN_BITS_PER_REG;
  constant C_CTRL_REG54_OFFSET : integer := 54*CGN_BITS_PER_REG;
  constant C_CTRL_REG55_OFFSET : integer := 55*CGN_BITS_PER_REG;
  constant C_CTRL_REG56_OFFSET : integer := 56*CGN_BITS_PER_REG;
  constant C_CTRL_REG57_OFFSET : integer := 57*CGN_BITS_PER_REG;
  constant C_CTRL_REG58_OFFSET : integer := 58*CGN_BITS_PER_REG;
  constant C_CTRL_REG59_OFFSET : integer := 59*CGN_BITS_PER_REG;
  constant C_CTRL_REG60_OFFSET : integer := 60*CGN_BITS_PER_REG;
  constant C_CTRL_REG61_OFFSET : integer := 61*CGN_BITS_PER_REG;
  constant C_CTRL_REG62_OFFSET : integer := 62*CGN_BITS_PER_REG;
  constant C_CTRL_REG63_OFFSET : integer := 63*CGN_BITS_PER_REG;
  constant C_CTRL_REG64_OFFSET : integer := 64*CGN_BITS_PER_REG;
  constant C_CTRL_REG65_OFFSET : integer := 65*CGN_BITS_PER_REG;
  constant C_CTRL_REG66_OFFSET : integer := 66*CGN_BITS_PER_REG;
  constant C_CTRL_REG67_OFFSET : integer := 67*CGN_BITS_PER_REG;
  constant C_CTRL_REG68_OFFSET : integer := 68*CGN_BITS_PER_REG;
  constant C_CTRL_REG69_OFFSET : integer := 69*CGN_BITS_PER_REG;
  constant C_CTRL_REG70_OFFSET : integer := 70*CGN_BITS_PER_REG;
  constant C_CTRL_REG71_OFFSET : integer := 71*CGN_BITS_PER_REG;
  constant C_CTRL_REG72_OFFSET : integer := 72*CGN_BITS_PER_REG;
  constant C_CTRL_REG73_OFFSET : integer := 73*CGN_BITS_PER_REG;
  constant C_CTRL_REG74_OFFSET : integer := 74*CGN_BITS_PER_REG;
  constant C_CTRL_REG75_OFFSET : integer := 75*CGN_BITS_PER_REG;
  constant C_CTRL_REG76_OFFSET : integer := 76*CGN_BITS_PER_REG;
  constant C_CTRL_REG77_OFFSET : integer := 77*CGN_BITS_PER_REG;
  constant C_CTRL_REG78_OFFSET : integer := 78*CGN_BITS_PER_REG;
  constant C_CTRL_REG79_OFFSET : integer := 79*CGN_BITS_PER_REG;
  constant C_CTRL_REG80_OFFSET : integer := 80*CGN_BITS_PER_REG;
  constant C_CTRL_REG81_OFFSET : integer := 81*CGN_BITS_PER_REG;
  constant C_CTRL_REG82_OFFSET : integer := 82*CGN_BITS_PER_REG;
  constant C_CTRL_REG83_OFFSET : integer := 83*CGN_BITS_PER_REG;
  constant C_CTRL_REG84_OFFSET : integer := 84*CGN_BITS_PER_REG;
  constant C_CTRL_REG85_OFFSET : integer := 85*CGN_BITS_PER_REG;
  constant C_CTRL_REG86_OFFSET : integer := 86*CGN_BITS_PER_REG;
  constant C_CTRL_REG87_OFFSET : integer := 87*CGN_BITS_PER_REG;
  constant C_CTRL_REG88_OFFSET : integer := 88*CGN_BITS_PER_REG;
  constant C_CTRL_REG89_OFFSET : integer := 89*CGN_BITS_PER_REG;
  constant C_CTRL_REG90_OFFSET : integer := 90*CGN_BITS_PER_REG;
  constant C_CTRL_REG91_OFFSET : integer := 91*CGN_BITS_PER_REG;
  constant C_CTRL_REG92_OFFSET : integer := 92*CGN_BITS_PER_REG;
  constant C_CTRL_REG93_OFFSET : integer := 93*CGN_BITS_PER_REG;
  constant C_CTRL_REG94_OFFSET : integer := 94*CGN_BITS_PER_REG;
  constant C_CTRL_REG95_OFFSET : integer := 95*CGN_BITS_PER_REG;
  constant C_CTRL_REG96_OFFSET : integer := 96*CGN_BITS_PER_REG;
  constant C_CTRL_REG97_OFFSET : integer := 97*CGN_BITS_PER_REG;
  constant C_CTRL_REG98_OFFSET : integer := 98*CGN_BITS_PER_REG;
  constant C_CTRL_REG99_OFFSET : integer := 99*CGN_BITS_PER_REG;
  constant C_CTRL_REG100_OFFSET : integer := 100*CGN_BITS_PER_REG;
  constant C_CTRL_REG101_OFFSET : integer := 101*CGN_BITS_PER_REG;
  constant C_CTRL_REG102_OFFSET : integer := 102*CGN_BITS_PER_REG;
  constant C_CTRL_REG103_OFFSET : integer := 103*CGN_BITS_PER_REG;
  constant C_CTRL_REG104_OFFSET : integer := 104*CGN_BITS_PER_REG;
  constant C_CTRL_REG105_OFFSET : integer := 105*CGN_BITS_PER_REG;
  constant C_CTRL_REG106_OFFSET : integer := 106*CGN_BITS_PER_REG;
  constant C_CTRL_REG107_OFFSET : integer := 107*CGN_BITS_PER_REG;
  constant C_CTRL_REG108_OFFSET : integer := 108*CGN_BITS_PER_REG;
  constant C_CTRL_REG109_OFFSET : integer := 109*CGN_BITS_PER_REG;
  constant C_CTRL_REG110_OFFSET : integer := 110*CGN_BITS_PER_REG;
  constant C_CTRL_REG111_OFFSET : integer := 111*CGN_BITS_PER_REG;
  constant C_CTRL_REG112_OFFSET : integer := 112*CGN_BITS_PER_REG;
  constant C_CTRL_REG113_OFFSET : integer := 113*CGN_BITS_PER_REG;
  constant C_CTRL_REG114_OFFSET : integer := 114*CGN_BITS_PER_REG;
  constant C_CTRL_REG115_OFFSET : integer := 115*CGN_BITS_PER_REG;
  constant C_CTRL_REG116_OFFSET : integer := 116*CGN_BITS_PER_REG;
  constant C_CTRL_REG117_OFFSET : integer := 117*CGN_BITS_PER_REG;
  constant C_CTRL_REG118_OFFSET : integer := 118*CGN_BITS_PER_REG;
  constant C_CTRL_REG119_OFFSET : integer := 119*CGN_BITS_PER_REG;
  constant C_CTRL_REG120_OFFSET : integer := 120*CGN_BITS_PER_REG;
  constant C_CTRL_REG121_OFFSET : integer := 121*CGN_BITS_PER_REG;
  constant C_CTRL_REG122_OFFSET : integer := 122*CGN_BITS_PER_REG;
  constant C_CTRL_REG123_OFFSET : integer := 123*CGN_BITS_PER_REG;
  constant C_CTRL_REG124_OFFSET : integer := 124*CGN_BITS_PER_REG;
  constant C_CTRL_REG125_OFFSET : integer := 125*CGN_BITS_PER_REG;
  constant C_CTRL_REG126_OFFSET : integer := 126*CGN_BITS_PER_REG;
  constant C_CTRL_REG127_OFFSET : integer := 127*CGN_BITS_PER_REG;
  constant C_CTRL_REG128_OFFSET : integer := 128*CGN_BITS_PER_REG;
  constant C_CTRL_REG129_OFFSET : integer := 129*CGN_BITS_PER_REG;
  constant C_CTRL_REG130_OFFSET : integer := 130*CGN_BITS_PER_REG;
  constant C_CTRL_REG131_OFFSET : integer := 131*CGN_BITS_PER_REG;
  constant C_CTRL_REG132_OFFSET : integer := 132*CGN_BITS_PER_REG;
  constant C_CTRL_REG133_OFFSET : integer := 133*CGN_BITS_PER_REG;
  constant C_CTRL_REG134_OFFSET : integer := 134*CGN_BITS_PER_REG;
  constant C_CTRL_REG135_OFFSET : integer := 135*CGN_BITS_PER_REG;
  constant C_CTRL_REG136_OFFSET : integer := 136*CGN_BITS_PER_REG;
  constant C_CTRL_REG137_OFFSET : integer := 137*CGN_BITS_PER_REG;
  constant C_CTRL_REG138_OFFSET : integer := 138*CGN_BITS_PER_REG;

  constant C_STAT_REG0_OFFSET : integer := 0*CGN_BITS_PER_REG;
  constant C_STAT_REG1_OFFSET : integer := 1*CGN_BITS_PER_REG;
  constant C_STAT_REG2_OFFSET : integer := 2*CGN_BITS_PER_REG;
  constant C_STAT_REG3_OFFSET : integer := 3*CGN_BITS_PER_REG;
  constant C_STAT_REG4_OFFSET : integer := 4*CGN_BITS_PER_REG;
  constant C_STAT_REG5_OFFSET : integer := 5*CGN_BITS_PER_REG;
  constant C_STAT_REG6_OFFSET : integer := 6*CGN_BITS_PER_REG;
  constant C_STAT_REG7_OFFSET : integer := 7*CGN_BITS_PER_REG;
  constant C_STAT_REG8_OFFSET : integer := 8*CGN_BITS_PER_REG;
  constant C_STAT_REG9_OFFSET : integer := 9*CGN_BITS_PER_REG;
  constant C_STAT_REG10_OFFSET : integer := 10*CGN_BITS_PER_REG;
  constant C_STAT_REG11_OFFSET : integer := 11*CGN_BITS_PER_REG;
  constant C_STAT_REG12_OFFSET : integer := 12*CGN_BITS_PER_REG;
  constant C_STAT_REG13_OFFSET : integer := 13*CGN_BITS_PER_REG;
  constant C_STAT_REG14_OFFSET : integer := 14*CGN_BITS_PER_REG;
  constant C_STAT_REG15_OFFSET : integer := 15*CGN_BITS_PER_REG;
  constant C_STAT_REG16_OFFSET : integer := 16*CGN_BITS_PER_REG;
  constant C_STAT_REG17_OFFSET : integer := 17*CGN_BITS_PER_REG;
  constant C_STAT_REG18_OFFSET : integer := 18*CGN_BITS_PER_REG;
  constant C_STAT_REG19_OFFSET : integer := 19*CGN_BITS_PER_REG;
  constant C_STAT_REG20_OFFSET : integer := 20*CGN_BITS_PER_REG;
  constant C_STAT_REG21_OFFSET : integer := 21*CGN_BITS_PER_REG;
  constant C_STAT_REG22_OFFSET : integer := 22*CGN_BITS_PER_REG;
  constant C_STAT_REG23_OFFSET : integer := 23*CGN_BITS_PER_REG;
  constant C_STAT_REG24_OFFSET : integer := 24*CGN_BITS_PER_REG;
  constant C_STAT_REG25_OFFSET : integer := 25*CGN_BITS_PER_REG;
  constant C_STAT_REG26_OFFSET : integer := 26*CGN_BITS_PER_REG;
  constant C_STAT_REG27_OFFSET : integer := 27*CGN_BITS_PER_REG;
  constant C_STAT_REG28_OFFSET : integer := 28*CGN_BITS_PER_REG;
  constant C_STAT_REG29_OFFSET : integer := 29*CGN_BITS_PER_REG;
  constant C_STAT_REG30_OFFSET : integer := 30*CGN_BITS_PER_REG;
  constant C_STAT_REG31_OFFSET : integer := 31*CGN_BITS_PER_REG;
  constant C_STAT_REG32_OFFSET : integer := 32*CGN_BITS_PER_REG;
  constant C_STAT_REG33_OFFSET : integer := 33*CGN_BITS_PER_REG;
  constant C_STAT_REG34_OFFSET : integer := 34*CGN_BITS_PER_REG;
  constant C_STAT_REG35_OFFSET : integer := 35*CGN_BITS_PER_REG;
  constant C_STAT_REG36_OFFSET : integer := 36*CGN_BITS_PER_REG;
  constant C_STAT_REG37_OFFSET : integer := 37*CGN_BITS_PER_REG;
  constant C_STAT_REG38_OFFSET : integer := 38*CGN_BITS_PER_REG;
  constant C_STAT_REG39_OFFSET : integer := 39*CGN_BITS_PER_REG;
  constant C_STAT_REG40_OFFSET : integer := 40*CGN_BITS_PER_REG;
  constant C_STAT_REG41_OFFSET : integer := 41*CGN_BITS_PER_REG;
  constant C_STAT_REG42_OFFSET : integer := 42*CGN_BITS_PER_REG;
  constant C_STAT_REG43_OFFSET : integer := 43*CGN_BITS_PER_REG;
  constant C_STAT_REG44_OFFSET : integer := 44*CGN_BITS_PER_REG;
  constant C_STAT_REG45_OFFSET : integer := 45*CGN_BITS_PER_REG;
  constant C_STAT_REG46_OFFSET : integer := 46*CGN_BITS_PER_REG;
  constant C_STAT_REG47_OFFSET : integer := 47*CGN_BITS_PER_REG;
  constant C_STAT_REG48_OFFSET : integer := 48*CGN_BITS_PER_REG;
  constant C_STAT_REG49_OFFSET : integer := 49*CGN_BITS_PER_REG;
  constant C_STAT_REG50_OFFSET : integer := 50*CGN_BITS_PER_REG;
  constant C_STAT_REG51_OFFSET : integer := 51*CGN_BITS_PER_REG;
  constant C_STAT_REG52_OFFSET : integer := 52*CGN_BITS_PER_REG;
  constant C_STAT_REG53_OFFSET : integer := 53*CGN_BITS_PER_REG;
  constant C_STAT_REG54_OFFSET : integer := 54*CGN_BITS_PER_REG;
  constant C_STAT_REG55_OFFSET : integer := 55*CGN_BITS_PER_REG;
  constant C_STAT_REG56_OFFSET : integer := 56*CGN_BITS_PER_REG;
  constant C_STAT_REG57_OFFSET : integer := 57*CGN_BITS_PER_REG;
  constant C_STAT_REG58_OFFSET : integer := 58*CGN_BITS_PER_REG;
  constant C_STAT_REG59_OFFSET : integer := 59*CGN_BITS_PER_REG;
  constant C_STAT_REG60_OFFSET : integer := 60*CGN_BITS_PER_REG;
  constant C_STAT_REG61_OFFSET : integer := 61*CGN_BITS_PER_REG;
  constant C_STAT_REG62_OFFSET : integer := 62*CGN_BITS_PER_REG;
  constant C_STAT_REG63_OFFSET : integer := 63*CGN_BITS_PER_REG;
  constant C_STAT_REG64_OFFSET : integer := 64*CGN_BITS_PER_REG;
  constant C_STAT_REG65_OFFSET : integer := 65*CGN_BITS_PER_REG;
  constant C_STAT_REG66_OFFSET : integer := 66*CGN_BITS_PER_REG;
  constant C_STAT_REG67_OFFSET : integer := 67*CGN_BITS_PER_REG;
  constant C_STAT_REG68_OFFSET : integer := 68*CGN_BITS_PER_REG;
  constant C_STAT_REG69_OFFSET : integer := 69*CGN_BITS_PER_REG;
  constant C_STAT_REG70_OFFSET : integer := 70*CGN_BITS_PER_REG;
  constant C_STAT_REG71_OFFSET : integer := 71*CGN_BITS_PER_REG;
  constant C_STAT_REG72_OFFSET : integer := 72*CGN_BITS_PER_REG;
  constant C_STAT_REG73_OFFSET : integer := 73*CGN_BITS_PER_REG;
  constant C_STAT_REG74_OFFSET : integer := 74*CGN_BITS_PER_REG;
  constant C_STAT_REG75_OFFSET : integer := 75*CGN_BITS_PER_REG;
  constant C_STAT_REG76_OFFSET : integer := 76*CGN_BITS_PER_REG;
  constant C_STAT_REG77_OFFSET : integer := 77*CGN_BITS_PER_REG;
  constant C_STAT_REG78_OFFSET : integer := 78*CGN_BITS_PER_REG;
  constant C_STAT_REG79_OFFSET : integer := 79*CGN_BITS_PER_REG;
  constant C_STAT_REG80_OFFSET : integer := 80*CGN_BITS_PER_REG;
  constant C_STAT_REG81_OFFSET : integer := 81*CGN_BITS_PER_REG;
  constant C_STAT_REG82_OFFSET : integer := 82*CGN_BITS_PER_REG;
  constant C_STAT_REG83_OFFSET : integer := 83*CGN_BITS_PER_REG;
  constant C_STAT_REG84_OFFSET : integer := 84*CGN_BITS_PER_REG;
  constant C_STAT_REG85_OFFSET : integer := 85*CGN_BITS_PER_REG;
  constant C_STAT_REG86_OFFSET : integer := 86*CGN_BITS_PER_REG;
  constant C_STAT_REG87_OFFSET : integer := 87*CGN_BITS_PER_REG;
  constant C_STAT_REG88_OFFSET : integer := 88*CGN_BITS_PER_REG;

  -- Register Bank Status Contents (Defaults)
  signal status_reg : std_logic_vector(CGN_NUM_STAT_REG*CGN_BITS_PER_REG-1 downto 0) := X"00000000" & -- 0x0160 LMK_8_15_MOD_FLAG
                                                                                        X"00000000" & -- 0x015C LMK_0_7_MOD_FLAG
                                                                                        X"00000000" & -- 0x0158 HV_MOD_FLAG
                                                                                        X"00000000" & -- 0x0154 HV_U_TARGET_8_15_MOD_FLAG
                                                                                        X"00000000" & -- 0x0150 HV_U_TARGET_0_7_MOD_FLAG
                                                                                        X"00000000" & -- 0x014C FE_0_15_MOD_FLAG
                                                                                        X"00000000" & -- 0x0148 DAC_2_MOD_FLAG
                                                                                        X"00000000" & -- 0x0144 DAC_0_1_MOD_FLAG
                                                                                        X"00000000" & -- 0x0140 ADC_SAMPLE_DIV_MOD_FLAG
                                                                                        X"00000000" & -- 0x013C COM_PLD_SIZE_MOD_FLAG
                                                                                        X"00000000" & -- 0x0138 DRS_MOD_FLAG
                                                                                        X"00000000" & -- 0x0134 CLK_CTRL_MOD_FLAG
                                                                                        X"00000000" & -- 0x0130 MAX_SCL_PKT_SAMPLES
                                                                                        X"00000000" & -- 0x012C MAX_TRG_PKT_SAMPLES
                                                                                        X"00000000" & -- 0x0128 MAX_TDC_PKT_SAMPLES
                                                                                        X"00000000" & -- 0x0124 MAX_DRS_ADC_PKT_SAMPLES
                                                                                        X"00000000" & -- 0x0120 ADV_TRG_STAT4
                                                                                        X"00000000" & -- 0x011C ADV_TRG_STAT3
                                                                                        X"00000000" & -- 0x0118 ADV_TRG_STAT2
                                                                                        X"00000000" & -- 0x0114 ADV_TRG_STAT1
                                                                                        X"00000000" & -- 0x0110 ADV_TRG_TRIG_CELL
                                                                                        X"00000000" & -- 0x010C TRB_INFO_MSB
                                                                                        X"00000000" & -- 0x0108 TRB_INFO_LSB
                                                                                        X"00000000" & -- 0x0104 TRB_INFO_STAT
                                                                                        X"00000000" & -- 0x0100 EVENT_NR
                                                                                        X"00000000" & -- 0x00FC EVENT_TX_RATE
                                                                                        X"00000000" & -- 0x00F8 COMP_CH_STAT
                                                                                        X"00000000" & -- 0x00F4 TIME_MSB
                                                                                        X"00000000" & -- 0x00F0 TIME_LSB
                                                                                        X"00000000" & -- 0x00EC SCALER_TIME_STAMP_MSB
                                                                                        X"00000000" & -- 0x00E8 SCALER_TIME_STAMP_LSB
                                                                                        X"00000000" & -- 0x00E4 SCALER_EXT_CLK
                                                                                        X"00000000" & -- 0x00E0 SCALER_EXT_TRG
                                                                                        X"00000000" & -- 0x00DC SCALER_PTRN_TRG
                                                                                        X"00000000" & -- 0x00D8 SCALER_15
                                                                                        X"00000000" & -- 0x00D4 SCALER_14
                                                                                        X"00000000" & -- 0x00D0 SCALER_13
                                                                                        X"00000000" & -- 0x00CC SCALER_12
                                                                                        X"00000000" & -- 0x00C8 SCALER_11
                                                                                        X"00000000" & -- 0x00C4 SCALER_10
                                                                                        X"00000000" & -- 0x00C0 SCALER_9
                                                                                        X"00000000" & -- 0x00BC SCALER_8
                                                                                        X"00000000" & -- 0x00B8 SCALER_7
                                                                                        X"00000000" & -- 0x00B4 SCALER_6
                                                                                        X"00000000" & -- 0x00B0 SCALER_5
                                                                                        X"00000000" & -- 0x00AC SCALER_4
                                                                                        X"00000000" & -- 0x00A8 SCALER_3
                                                                                        X"00000000" & -- 0x00A4 SCALER_2
                                                                                        X"00000000" & -- 0x00A0 SCALER_1
                                                                                        X"00000000" & -- 0x009C SCALER_0
                                                                                        X"00000000" & -- 0x0098 HV_TEMP_3
                                                                                        X"00000000" & -- 0x0094 HV_TEMP_2
                                                                                        X"00000000" & -- 0x0090 HV_TEMP_1
                                                                                        X"00000000" & -- 0x008C HV_TEMP_0
                                                                                        X"00000000" & -- 0x0088 HV_U_BASE_MEAS
                                                                                        X"00000000" & -- 0x0084 HV_I_MEAS_15
                                                                                        X"00000000" & -- 0x0080 HV_I_MEAS_14
                                                                                        X"00000000" & -- 0x007C HV_I_MEAS_13
                                                                                        X"00000000" & -- 0x0078 HV_I_MEAS_12
                                                                                        X"00000000" & -- 0x0074 HV_I_MEAS_11
                                                                                        X"00000000" & -- 0x0070 HV_I_MEAS_10
                                                                                        X"00000000" & -- 0x006C HV_I_MEAS_9
                                                                                        X"00000000" & -- 0x0068 HV_I_MEAS_8
                                                                                        X"00000000" & -- 0x0064 HV_I_MEAS_7
                                                                                        X"00000000" & -- 0x0060 HV_I_MEAS_6
                                                                                        X"00000000" & -- 0x005C HV_I_MEAS_5
                                                                                        X"00000000" & -- 0x0058 HV_I_MEAS_4
                                                                                        X"00000000" & -- 0x0054 HV_I_MEAS_3
                                                                                        X"00000000" & -- 0x0050 HV_I_MEAS_2
                                                                                        X"00000000" & -- 0x004C HV_I_MEAS_1
                                                                                        X"00000000" & -- 0x0048 HV_I_MEAS_0
                                                                                        X"00000000" & -- 0x0044 HV_VER
                                                                                        X"0009C400" & -- 0x0040 TDC_SAMPLE_FREQ
                                                                                        X"00013880" & -- 0x003C ADC_SAMPLE_FREQ
                                                                                        X"00000000" & -- 0x0038 DRS_SAMPLE_FREQ
                                                                                        X"00000000" & -- 0x0034 DRS_STOP_WSR
                                                                                        X"00000000" & -- 0x0030 DRS_STOP_CELL
                                                                                        X"00000000" & -- 0x002C PLL_LOCK
                                                                                        X"00000000" & -- 0x0028 STATUS
                                                                                        X"00000000" & -- 0x0024 SN
                                                                                        X"00000006" & -- 0x0020 PROT_VER
                                                                                        X"00000000" & -- 0x001C SW_GIT_HASH_TAG
                                                                                        X"00000000" & -- 0x0018 FW_GIT_HASH_TAG
                                                                                        X"00000000" & -- 0x0014 SW_BUILD_TIME
                                                                                        X"00000000" & -- 0x0010 SW_BUILD_DATE
                                                                                        X"05000000" & -- 0x000C FW_BUILD_TIME
                                                                                        X"00000000" & -- 0x0008 FW_BUILD_DATE
                                                                                        X"00090009" & -- 0x0004 REG_LAYOUT_VER
                                                                                        X"AC01021B";  -- 0x0000 HW_VER


begin

  STATUS_REG_O <= status_reg;

  -- Control Register 0 [0x1000]: WDB_LOC
  WDB_LOC_BYTE_TRIG_O                       <= CTRL_BYTE_TRIG_I((C_CTRL_REG0_OFFSET/8)+3 downto C_CTRL_REG0_OFFSET/8);
  WDB_LOC_REG_READ_O                        <= CTRL_REG_READ_I(C_CTRL_REG0_OFFSET/CGN_BITS_PER_REG);
  WDB_LOC_REG_WRITE_O                       <= CTRL_REG_WRITE_I(C_CTRL_REG0_OFFSET/CGN_BITS_PER_REG);
  CRATE_ID_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG0_OFFSET+23 downto C_CTRL_REG0_OFFSET+16);
  CRATE_ID_O                                <= CONTROL_REG_I(C_CTRL_REG0_OFFSET+23 downto C_CTRL_REG0_OFFSET+16);
  SLOT_ID_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG0_OFFSET+7 downto C_CTRL_REG0_OFFSET+0);
  SLOT_ID_O                                 <= CONTROL_REG_I(C_CTRL_REG0_OFFSET+7 downto C_CTRL_REG0_OFFSET+0);

  -- Control Register 1 [0x1004]: CTRL
  CTRL_BYTE_TRIG_O                          <= CTRL_BYTE_TRIG_I((C_CTRL_REG1_OFFSET/8)+3 downto C_CTRL_REG1_OFFSET/8);
  CTRL_REG_READ_O                           <= CTRL_REG_READ_I(C_CTRL_REG1_OFFSET/CGN_BITS_PER_REG);
  CTRL_REG_WRITE_O                          <= CTRL_REG_WRITE_I(C_CTRL_REG1_OFFSET/CGN_BITS_PER_REG);
  VALID_DELAY_ADC_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+29 downto C_CTRL_REG1_OFFSET+24);
  VALID_DELAY_ADC_O                         <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+29 downto C_CTRL_REG1_OFFSET+24);
  DAQ_DATA_PHASE_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+23 downto C_CTRL_REG1_OFFSET+16);
  DAQ_DATA_PHASE_O                          <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+23 downto C_CTRL_REG1_OFFSET+16);
  FE_POWER_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+11);
  FE_POWER_O                                <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+11);
  DRS_CLR_RSR_AFTER_RO_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+10);
  DRS_CLR_RSR_AFTER_RO_O                    <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+10);
  COMP_POWER_EN_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+9);
  COMP_POWER_EN_O                           <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+9);
  DRS_READOUT_MODE_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+8);
  DRS_READOUT_MODE_O                        <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+8);
  DRS_WAVE_CONTINUOUS_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+7);
  DRS_WAVE_CONTINUOUS_O                     <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+7);
  DRS_CONFIGURE_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+6);
  DRS_CONFIGURE_O                           <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+6);
  DAQ_SOFT_TRIGGER_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+5);
  DAQ_SOFT_TRIGGER_O                        <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+5);
  DAQ_AUTO_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+2);
  DAQ_AUTO_O                                <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+2);
  DAQ_NORMAL_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+1);
  DAQ_NORMAL_O                              <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+1);
  DAQ_SINGLE_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG1_OFFSET+0);
  DAQ_SINGLE_O                              <= CONTROL_REG_I(C_CTRL_REG1_OFFSET+0);

  -- Control Register 2 [0x1008]: CAL_CTRL
  CAL_CTRL_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG2_OFFSET/8)+3 downto C_CTRL_REG2_OFFSET/8);
  CAL_CTRL_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG2_OFFSET/CGN_BITS_PER_REG);
  CAL_CTRL_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG2_OFFSET/CGN_BITS_PER_REG);
  DRS_0_TIMING_REF_SEL_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG2_OFFSET+3);
  DRS_0_TIMING_REF_SEL_O                    <= CONTROL_REG_I(C_CTRL_REG2_OFFSET+3);
  DRS_1_TIMING_REF_SEL_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG2_OFFSET+2);
  DRS_1_TIMING_REF_SEL_O                    <= CONTROL_REG_I(C_CTRL_REG2_OFFSET+2);
  CALIB_BUFFER_EN_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG2_OFFSET+1);
  CALIB_BUFFER_EN_O                         <= CONTROL_REG_I(C_CTRL_REG2_OFFSET+1);
  TIMING_CALIB_SIGNAL_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG2_OFFSET+0);
  TIMING_CALIB_SIGNAL_EN_O                  <= CONTROL_REG_I(C_CTRL_REG2_OFFSET+0);

  -- Control Register 3 [0x100C]: CLK_CTRL
  CLK_CTRL_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG3_OFFSET/8)+3 downto C_CTRL_REG3_OFFSET/8);
  CLK_CTRL_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG3_OFFSET/CGN_BITS_PER_REG);
  CLK_CTRL_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG3_OFFSET/CGN_BITS_PER_REG);
  TRIG_DAQ_CLK_CAL_CHK_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG3_OFFSET+24);
  TRIG_DAQ_CLK_CAL_CHK_O                    <= CONTROL_REG_I(C_CTRL_REG3_OFFSET+24);
  DAQ_CLK_SRC_SEL_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG3_OFFSET+17);
  DAQ_CLK_SRC_SEL_O                         <= CONTROL_REG_I(C_CTRL_REG3_OFFSET+17);
  EXT_CLK_IN_SEL_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG3_OFFSET+16);
  EXT_CLK_IN_SEL_O                          <= CONTROL_REG_I(C_CTRL_REG3_OFFSET+16);
  EXT_CLK_FREQ_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG3_OFFSET+15 downto C_CTRL_REG3_OFFSET+8);
  EXT_CLK_FREQ_O                            <= CONTROL_REG_I(C_CTRL_REG3_OFFSET+15 downto C_CTRL_REG3_OFFSET+8);
  LOCAL_CLK_FREQ_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG3_OFFSET+7 downto C_CTRL_REG3_OFFSET+0);
  LOCAL_CLK_FREQ_O                          <= CONTROL_REG_I(C_CTRL_REG3_OFFSET+7 downto C_CTRL_REG3_OFFSET+0);

  -- Control Register 4 [0x1010]: DRS_CTRL
  DRS_CTRL_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG4_OFFSET/8)+3 downto C_CTRL_REG4_OFFSET/8);
  DRS_CTRL_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG4_OFFSET/CGN_BITS_PER_REG);
  DRS_CTRL_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG4_OFFSET/CGN_BITS_PER_REG);
  DRS_WSRLOOP_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG4_OFFSET+18);
  DRS_WSRLOOP_O                             <= CONTROL_REG_I(C_CTRL_REG4_OFFSET+18);
  DRS_PLLEN_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG4_OFFSET+17);
  DRS_PLLEN_O                               <= CONTROL_REG_I(C_CTRL_REG4_OFFSET+17);
  DRS_DMODE_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG4_OFFSET+16);
  DRS_DMODE_O                               <= CONTROL_REG_I(C_CTRL_REG4_OFFSET+16);
  DRS_WSR_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG4_OFFSET+15 downto C_CTRL_REG4_OFFSET+8);
  DRS_WSR_O                                 <= CONTROL_REG_I(C_CTRL_REG4_OFFSET+15 downto C_CTRL_REG4_OFFSET+8);
  DRS_WCR_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG4_OFFSET+7 downto C_CTRL_REG4_OFFSET+0);
  DRS_WCR_O                                 <= CONTROL_REG_I(C_CTRL_REG4_OFFSET+7 downto C_CTRL_REG4_OFFSET+0);

  -- Control Register 5 [0x1014]: COM_CTRL
  COM_CTRL_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG5_OFFSET/8)+3 downto C_CTRL_REG5_OFFSET/8);
  COM_CTRL_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG5_OFFSET/CGN_BITS_PER_REG);
  COM_CTRL_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG5_OFFSET/CGN_BITS_PER_REG);
  DCB_SERDES_TRAIN_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+31);
  DCB_SERDES_TRAIN_O                        <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+31);
  TCB_SERDES_TRAIN_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+30);
  TCB_SERDES_TRAIN_O                        <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+30);
  TRG_TX_EN_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+27);
  TRG_TX_EN_O                               <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+27);
  SCL_TX_EN_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+26);
  SCL_TX_EN_O                               <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+26);
  SERDES_COM_EN_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+25);
  SERDES_COM_EN_O                           <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+25);
  ETH_COM_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+24);
  ETH_COM_EN_O                              <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+24);
  INTER_PKG_DELAY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG5_OFFSET+23 downto C_CTRL_REG5_OFFSET+0);
  INTER_PKG_DELAY_O                         <= CONTROL_REG_I(C_CTRL_REG5_OFFSET+23 downto C_CTRL_REG5_OFFSET+0);

  -- Control Register 6 [0x1018]: COM_PLD_SIZE
  COM_PLD_SIZE_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG6_OFFSET/8)+3 downto C_CTRL_REG6_OFFSET/8);
  COM_PLD_SIZE_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG6_OFFSET/CGN_BITS_PER_REG);
  COM_PLD_SIZE_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG6_OFFSET/CGN_BITS_PER_REG);
  FIRST_PKG_DLY_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG6_OFFSET+31 downto C_CTRL_REG6_OFFSET+18);
  FIRST_PKG_DLY_O                           <= CONTROL_REG_I(C_CTRL_REG6_OFFSET+31 downto C_CTRL_REG6_OFFSET+18);
  COM_PLD_SIZE_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG6_OFFSET+17 downto C_CTRL_REG6_OFFSET+0);
  COM_PLD_SIZE_O                            <= CONTROL_REG_I(C_CTRL_REG6_OFFSET+17 downto C_CTRL_REG6_OFFSET+0);

  -- Control Register 7 [0x101C]: DRS_CH_TX_EN
  DRS_CH_TX_EN_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG7_OFFSET/8)+3 downto C_CTRL_REG7_OFFSET/8);
  DRS_CH_TX_EN_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG7_OFFSET/CGN_BITS_PER_REG);
  DRS_CH_TX_EN_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG7_OFFSET/CGN_BITS_PER_REG);
  DRS_CH_TX_EN_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG7_OFFSET+17 downto C_CTRL_REG7_OFFSET+0);
  DRS_CH_TX_EN_O                            <= CONTROL_REG_I(C_CTRL_REG7_OFFSET+17 downto C_CTRL_REG7_OFFSET+0);

  -- Control Register 8 [0x1020]: ADC_CH_TX_EN
  ADC_CH_TX_EN_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG8_OFFSET/8)+3 downto C_CTRL_REG8_OFFSET/8);
  ADC_CH_TX_EN_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG8_OFFSET/CGN_BITS_PER_REG);
  ADC_CH_TX_EN_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG8_OFFSET/CGN_BITS_PER_REG);
  ADC_CH_TX_EN_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG8_OFFSET+15 downto C_CTRL_REG8_OFFSET+0);
  ADC_CH_TX_EN_O                            <= CONTROL_REG_I(C_CTRL_REG8_OFFSET+15 downto C_CTRL_REG8_OFFSET+0);

  -- Control Register 9 [0x1024]: TDC_CH_TX_EN
  TDC_CH_TX_EN_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG9_OFFSET/8)+3 downto C_CTRL_REG9_OFFSET/8);
  TDC_CH_TX_EN_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG9_OFFSET/CGN_BITS_PER_REG);
  TDC_CH_TX_EN_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG9_OFFSET/CGN_BITS_PER_REG);
  TDC_CH_TX_EN_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG9_OFFSET+15 downto C_CTRL_REG9_OFFSET+0);
  TDC_CH_TX_EN_O                            <= CONTROL_REG_I(C_CTRL_REG9_OFFSET+15 downto C_CTRL_REG9_OFFSET+0);

  -- Control Register 10 [0x1028]: DRS_TX_SAMPLES
  DRS_TX_SAMPLES_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG10_OFFSET/8)+3 downto C_CTRL_REG10_OFFSET/8);
  DRS_TX_SAMPLES_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG10_OFFSET/CGN_BITS_PER_REG);
  DRS_TX_SAMPLES_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG10_OFFSET/CGN_BITS_PER_REG);
  DRS_TX_SAMPLES_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG10_OFFSET+10 downto C_CTRL_REG10_OFFSET+0);
  DRS_TX_SAMPLES_O                          <= CONTROL_REG_I(C_CTRL_REG10_OFFSET+10 downto C_CTRL_REG10_OFFSET+0);

  -- Control Register 11 [0x102C]: ADC_TX_SAMPLES
  ADC_TX_SAMPLES_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG11_OFFSET/8)+3 downto C_CTRL_REG11_OFFSET/8);
  ADC_TX_SAMPLES_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG11_OFFSET/CGN_BITS_PER_REG);
  ADC_TX_SAMPLES_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG11_OFFSET/CGN_BITS_PER_REG);
  ADC_TX_SAMPLES_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG11_OFFSET+11 downto C_CTRL_REG11_OFFSET+0);
  ADC_TX_SAMPLES_O                          <= CONTROL_REG_I(C_CTRL_REG11_OFFSET+11 downto C_CTRL_REG11_OFFSET+0);

  -- Control Register 12 [0x1030]: TDC_TX_SAMPLES
  TDC_TX_SAMPLES_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG12_OFFSET/8)+3 downto C_CTRL_REG12_OFFSET/8);
  TDC_TX_SAMPLES_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG12_OFFSET/CGN_BITS_PER_REG);
  TDC_TX_SAMPLES_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG12_OFFSET/CGN_BITS_PER_REG);
  TDC_TX_SAMPLES_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG12_OFFSET+12 downto C_CTRL_REG12_OFFSET+0);
  TDC_TX_SAMPLES_O                          <= CONTROL_REG_I(C_CTRL_REG12_OFFSET+12 downto C_CTRL_REG12_OFFSET+0);

  -- Control Register 13 [0x1034]: TRG_TX_SAMPLES
  TRG_TX_SAMPLES_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG13_OFFSET/8)+3 downto C_CTRL_REG13_OFFSET/8);
  TRG_TX_SAMPLES_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG13_OFFSET/CGN_BITS_PER_REG);
  TRG_TX_SAMPLES_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG13_OFFSET/CGN_BITS_PER_REG);
  TRG_TX_SAMPLES_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG13_OFFSET+9 downto C_CTRL_REG13_OFFSET+0);
  TRG_TX_SAMPLES_O                          <= CONTROL_REG_I(C_CTRL_REG13_OFFSET+9 downto C_CTRL_REG13_OFFSET+0);

  -- Control Register 14 [0x1038]: ADC_SAMPLE_DIV
  ADC_SAMPLE_DIV_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG14_OFFSET/8)+3 downto C_CTRL_REG14_OFFSET/8);
  ADC_SAMPLE_DIV_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG14_OFFSET/CGN_BITS_PER_REG);
  ADC_SAMPLE_DIV_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG14_OFFSET/CGN_BITS_PER_REG);
  ADC_SAMPLE_DIV_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG14_OFFSET+7 downto C_CTRL_REG14_OFFSET+0);
  ADC_SAMPLE_DIV_O                          <= CONTROL_REG_I(C_CTRL_REG14_OFFSET+7 downto C_CTRL_REG14_OFFSET+0);

  -- Control Register 15 [0x103C]: ZERO_SUPR
  ZERO_SUPR_BYTE_TRIG_O                     <= CTRL_BYTE_TRIG_I((C_CTRL_REG15_OFFSET/8)+3 downto C_CTRL_REG15_OFFSET/8);
  ZERO_SUPR_REG_READ_O                      <= CTRL_REG_READ_I(C_CTRL_REG15_OFFSET/CGN_BITS_PER_REG);
  ZERO_SUPR_REG_WRITE_O                     <= CTRL_REG_WRITE_I(C_CTRL_REG15_OFFSET/CGN_BITS_PER_REG);
  ZERO_SUPR_EN_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG15_OFFSET+8);
  ZERO_SUPR_EN_O                            <= CONTROL_REG_I(C_CTRL_REG15_OFFSET+8);
  ZERO_SUPR_WINDOW_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG15_OFFSET+7 downto C_CTRL_REG15_OFFSET+0);
  ZERO_SUPR_WINDOW_O                        <= CONTROL_REG_I(C_CTRL_REG15_OFFSET+7 downto C_CTRL_REG15_OFFSET+0);

  -- Control Register 16 [0x1040]: RST
  RST_BYTE_TRIG_O                           <= CTRL_BYTE_TRIG_I((C_CTRL_REG16_OFFSET/8)+3 downto C_CTRL_REG16_OFFSET/8);
  RST_REG_READ_O                            <= CTRL_REG_READ_I(C_CTRL_REG16_OFFSET/CGN_BITS_PER_REG);
  RST_REG_WRITE_O                           <= CTRL_REG_WRITE_I(C_CTRL_REG16_OFFSET/CGN_BITS_PER_REG);
  DAQ_PLL_RST_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+14);
  DAQ_PLL_RST_O                             <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+14);
  DCB_OSERDES_PLL_RST_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+13);
  DCB_OSERDES_PLL_RST_O                     <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+13);
  TCB_OSERDES_PLL_RST_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+12);
  TCB_OSERDES_PLL_RST_O                     <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+12);
  DCB_OSERDES_IF_RST_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+11);
  DCB_OSERDES_IF_RST_O                      <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+11);
  TCB_OSERDES_IF_RST_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+10);
  TCB_OSERDES_IF_RST_O                      <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+10);
  SCALER_RST_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+9);
  SCALER_RST_O                              <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+9);
  TRB_PARITY_ERROR_COUNT_RST_BIT_TRIG_O     <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+8);
  TRB_PARITY_ERROR_COUNT_RST_O              <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+8);
  LMK_SYNC_LOCAL_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+7);
  LMK_SYNC_LOCAL_O                          <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+7);
  ADC_RST_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+6);
  ADC_RST_O                                 <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+6);
  ADC_IF_RST_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+5);
  ADC_IF_RST_O                              <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+5);
  DATA_LINK_IF_RST_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+4);
  DATA_LINK_IF_RST_O                        <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+4);
  WD_PKGR_RST_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+3);
  WD_PKGR_RST_O                             <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+3);
  EVENT_COUNTER_RST_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+2);
  EVENT_COUNTER_RST_O                       <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+2);
  DRS_CTRL_FSM_RST_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+1);
  DRS_CTRL_FSM_RST_O                        <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+1);
  RECONFIGURE_FPGA_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG16_OFFSET+0);
  RECONFIGURE_FPGA_O                        <= CONTROL_REG_I(C_CTRL_REG16_OFFSET+0);

  -- Control Register 17 [0x1044]: APLY_CFG
  APLY_CFG_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG17_OFFSET/8)+3 downto C_CTRL_REG17_OFFSET/8);
  APLY_CFG_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG17_OFFSET/CGN_BITS_PER_REG);
  APLY_CFG_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG17_OFFSET/CGN_BITS_PER_REG);
  APPLY_SETTINGS_ADC_SAMPLE_DIV_BIT_TRIG_O  <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+8);
  APPLY_SETTINGS_ADC_SAMPLE_DIV_O           <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+8);
  APPLY_SETTINGS_MAX_PLD_SIZE_BIT_TRIG_O    <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+7);
  APPLY_SETTINGS_MAX_PLD_SIZE_O             <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+7);
  APPLY_SETTINGS_HV_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+6);
  APPLY_SETTINGS_HV_O                       <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+6);
  APPLY_SETTINGS_DRS_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+5);
  APPLY_SETTINGS_DRS_O                      <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+5);
  APPLY_SETTINGS_DAC_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+4);
  APPLY_SETTINGS_DAC_O                      <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+4);
  APPLY_SETTINGS_FRONTEND_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+3);
  APPLY_SETTINGS_FRONTEND_O                 <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+3);
  APPLY_SETTINGS_CTRL_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+2);
  APPLY_SETTINGS_CTRL_O                     <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+2);
  APPLY_SETTINGS_ADC_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+1);
  APPLY_SETTINGS_ADC_O                      <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+1);
  APPLY_SETTINGS_LMK_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG17_OFFSET+0);
  APPLY_SETTINGS_LMK_O                      <= CONTROL_REG_I(C_CTRL_REG17_OFFSET+0);

  -- Control Register 18 [0x1048]: DAC0_A_B
  DAC0_A_B_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG18_OFFSET/8)+3 downto C_CTRL_REG18_OFFSET/8);
  DAC0_A_B_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG18_OFFSET/CGN_BITS_PER_REG);
  DAC0_A_B_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG18_OFFSET/CGN_BITS_PER_REG);
  DAC0_CH_A_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG18_OFFSET+31 downto C_CTRL_REG18_OFFSET+16);
  DAC0_CH_A_O                               <= CONTROL_REG_I(C_CTRL_REG18_OFFSET+31 downto C_CTRL_REG18_OFFSET+16);
  DAC0_CH_B_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG18_OFFSET+15 downto C_CTRL_REG18_OFFSET+0);
  DAC0_CH_B_O                               <= CONTROL_REG_I(C_CTRL_REG18_OFFSET+15 downto C_CTRL_REG18_OFFSET+0);

  -- Control Register 19 [0x104C]: DAC0_C_D
  DAC0_C_D_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG19_OFFSET/8)+3 downto C_CTRL_REG19_OFFSET/8);
  DAC0_C_D_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG19_OFFSET/CGN_BITS_PER_REG);
  DAC0_C_D_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG19_OFFSET/CGN_BITS_PER_REG);
  DAC0_CH_C_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG19_OFFSET+31 downto C_CTRL_REG19_OFFSET+16);
  DAC0_CH_C_O                               <= CONTROL_REG_I(C_CTRL_REG19_OFFSET+31 downto C_CTRL_REG19_OFFSET+16);
  DAC0_CH_D_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG19_OFFSET+15 downto C_CTRL_REG19_OFFSET+0);
  DAC0_CH_D_O                               <= CONTROL_REG_I(C_CTRL_REG19_OFFSET+15 downto C_CTRL_REG19_OFFSET+0);

  -- Control Register 20 [0x1050]: DAC0_E_F
  DAC0_E_F_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG20_OFFSET/8)+3 downto C_CTRL_REG20_OFFSET/8);
  DAC0_E_F_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG20_OFFSET/CGN_BITS_PER_REG);
  DAC0_E_F_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG20_OFFSET/CGN_BITS_PER_REG);
  DAC0_CH_E_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG20_OFFSET+31 downto C_CTRL_REG20_OFFSET+16);
  DAC0_CH_E_O                               <= CONTROL_REG_I(C_CTRL_REG20_OFFSET+31 downto C_CTRL_REG20_OFFSET+16);
  DAC0_CH_F_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG20_OFFSET+15 downto C_CTRL_REG20_OFFSET+0);
  DAC0_CH_F_O                               <= CONTROL_REG_I(C_CTRL_REG20_OFFSET+15 downto C_CTRL_REG20_OFFSET+0);

  -- Control Register 21 [0x1054]: DAC0_G_H
  DAC0_G_H_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG21_OFFSET/8)+3 downto C_CTRL_REG21_OFFSET/8);
  DAC0_G_H_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG21_OFFSET/CGN_BITS_PER_REG);
  DAC0_G_H_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG21_OFFSET/CGN_BITS_PER_REG);
  DAC0_CH_G_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG21_OFFSET+31 downto C_CTRL_REG21_OFFSET+16);
  DAC0_CH_G_O                               <= CONTROL_REG_I(C_CTRL_REG21_OFFSET+31 downto C_CTRL_REG21_OFFSET+16);
  DAC0_CH_H_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG21_OFFSET+15 downto C_CTRL_REG21_OFFSET+0);
  DAC0_CH_H_O                               <= CONTROL_REG_I(C_CTRL_REG21_OFFSET+15 downto C_CTRL_REG21_OFFSET+0);

  -- Control Register 22 [0x1058]: DAC1_A_B
  DAC1_A_B_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG22_OFFSET/8)+3 downto C_CTRL_REG22_OFFSET/8);
  DAC1_A_B_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG22_OFFSET/CGN_BITS_PER_REG);
  DAC1_A_B_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG22_OFFSET/CGN_BITS_PER_REG);
  DAC1_CH_A_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG22_OFFSET+31 downto C_CTRL_REG22_OFFSET+16);
  DAC1_CH_A_O                               <= CONTROL_REG_I(C_CTRL_REG22_OFFSET+31 downto C_CTRL_REG22_OFFSET+16);
  DAC1_CH_B_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG22_OFFSET+15 downto C_CTRL_REG22_OFFSET+0);
  DAC1_CH_B_O                               <= CONTROL_REG_I(C_CTRL_REG22_OFFSET+15 downto C_CTRL_REG22_OFFSET+0);

  -- Control Register 23 [0x105C]: DAC1_C_D
  DAC1_C_D_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG23_OFFSET/8)+3 downto C_CTRL_REG23_OFFSET/8);
  DAC1_C_D_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG23_OFFSET/CGN_BITS_PER_REG);
  DAC1_C_D_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG23_OFFSET/CGN_BITS_PER_REG);
  DAC1_CH_C_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG23_OFFSET+31 downto C_CTRL_REG23_OFFSET+16);
  DAC1_CH_C_O                               <= CONTROL_REG_I(C_CTRL_REG23_OFFSET+31 downto C_CTRL_REG23_OFFSET+16);
  DAC1_CH_D_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG23_OFFSET+15 downto C_CTRL_REG23_OFFSET+0);
  DAC1_CH_D_O                               <= CONTROL_REG_I(C_CTRL_REG23_OFFSET+15 downto C_CTRL_REG23_OFFSET+0);

  -- Control Register 24 [0x1060]: DAC1_E_F
  DAC1_E_F_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG24_OFFSET/8)+3 downto C_CTRL_REG24_OFFSET/8);
  DAC1_E_F_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG24_OFFSET/CGN_BITS_PER_REG);
  DAC1_E_F_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG24_OFFSET/CGN_BITS_PER_REG);
  DAC1_CH_E_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG24_OFFSET+31 downto C_CTRL_REG24_OFFSET+16);
  DAC1_CH_E_O                               <= CONTROL_REG_I(C_CTRL_REG24_OFFSET+31 downto C_CTRL_REG24_OFFSET+16);
  DAC1_CH_F_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG24_OFFSET+15 downto C_CTRL_REG24_OFFSET+0);
  DAC1_CH_F_O                               <= CONTROL_REG_I(C_CTRL_REG24_OFFSET+15 downto C_CTRL_REG24_OFFSET+0);

  -- Control Register 25 [0x1064]: DAC1_G_H
  DAC1_G_H_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG25_OFFSET/8)+3 downto C_CTRL_REG25_OFFSET/8);
  DAC1_G_H_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG25_OFFSET/CGN_BITS_PER_REG);
  DAC1_G_H_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG25_OFFSET/CGN_BITS_PER_REG);
  DAC1_CH_G_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG25_OFFSET+31 downto C_CTRL_REG25_OFFSET+16);
  DAC1_CH_G_O                               <= CONTROL_REG_I(C_CTRL_REG25_OFFSET+31 downto C_CTRL_REG25_OFFSET+16);
  DAC1_CH_H_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG25_OFFSET+15 downto C_CTRL_REG25_OFFSET+0);
  DAC1_CH_H_O                               <= CONTROL_REG_I(C_CTRL_REG25_OFFSET+15 downto C_CTRL_REG25_OFFSET+0);

  -- Control Register 26 [0x1068]: DAC2_A_B
  DAC2_A_B_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG26_OFFSET/8)+3 downto C_CTRL_REG26_OFFSET/8);
  DAC2_A_B_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG26_OFFSET/CGN_BITS_PER_REG);
  DAC2_A_B_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG26_OFFSET/CGN_BITS_PER_REG);
  DAC2_CH_A_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG26_OFFSET+31 downto C_CTRL_REG26_OFFSET+16);
  DAC2_CH_A_O                               <= CONTROL_REG_I(C_CTRL_REG26_OFFSET+31 downto C_CTRL_REG26_OFFSET+16);
  DAC2_CH_B_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG26_OFFSET+15 downto C_CTRL_REG26_OFFSET+0);
  DAC2_CH_B_O                               <= CONTROL_REG_I(C_CTRL_REG26_OFFSET+15 downto C_CTRL_REG26_OFFSET+0);

  -- Control Register 27 [0x106C]: DAC2_C_D
  DAC2_C_D_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG27_OFFSET/8)+3 downto C_CTRL_REG27_OFFSET/8);
  DAC2_C_D_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG27_OFFSET/CGN_BITS_PER_REG);
  DAC2_C_D_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG27_OFFSET/CGN_BITS_PER_REG);
  DAC2_CH_C_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG27_OFFSET+31 downto C_CTRL_REG27_OFFSET+16);
  DAC2_CH_C_O                               <= CONTROL_REG_I(C_CTRL_REG27_OFFSET+31 downto C_CTRL_REG27_OFFSET+16);
  DAC2_CH_D_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG27_OFFSET+15 downto C_CTRL_REG27_OFFSET+0);
  DAC2_CH_D_O                               <= CONTROL_REG_I(C_CTRL_REG27_OFFSET+15 downto C_CTRL_REG27_OFFSET+0);

  -- Control Register 28 [0x1070]: DAC2_E_F
  DAC2_E_F_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG28_OFFSET/8)+3 downto C_CTRL_REG28_OFFSET/8);
  DAC2_E_F_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG28_OFFSET/CGN_BITS_PER_REG);
  DAC2_E_F_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG28_OFFSET/CGN_BITS_PER_REG);
  DAC2_CH_E_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG28_OFFSET+31 downto C_CTRL_REG28_OFFSET+16);
  DAC2_CH_E_O                               <= CONTROL_REG_I(C_CTRL_REG28_OFFSET+31 downto C_CTRL_REG28_OFFSET+16);
  DAC2_CH_F_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG28_OFFSET+15 downto C_CTRL_REG28_OFFSET+0);
  DAC2_CH_F_O                               <= CONTROL_REG_I(C_CTRL_REG28_OFFSET+15 downto C_CTRL_REG28_OFFSET+0);

  -- Control Register 29 [0x1074]: DAC2_G_H
  DAC2_G_H_BYTE_TRIG_O                      <= CTRL_BYTE_TRIG_I((C_CTRL_REG29_OFFSET/8)+3 downto C_CTRL_REG29_OFFSET/8);
  DAC2_G_H_REG_READ_O                       <= CTRL_REG_READ_I(C_CTRL_REG29_OFFSET/CGN_BITS_PER_REG);
  DAC2_G_H_REG_WRITE_O                      <= CTRL_REG_WRITE_I(C_CTRL_REG29_OFFSET/CGN_BITS_PER_REG);
  DAC2_CH_G_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG29_OFFSET+31 downto C_CTRL_REG29_OFFSET+16);
  DAC2_CH_G_O                               <= CONTROL_REG_I(C_CTRL_REG29_OFFSET+31 downto C_CTRL_REG29_OFFSET+16);
  DAC2_CH_H_BIT_TRIG_O                      <= CTRL_BIT_TRIG_I(C_CTRL_REG29_OFFSET+15 downto C_CTRL_REG29_OFFSET+0);
  DAC2_CH_H_O                               <= CONTROL_REG_I(C_CTRL_REG29_OFFSET+15 downto C_CTRL_REG29_OFFSET+0);

  -- Control Register 30 [0x1078]: FE_CFG_0_1
  FE_CFG_0_1_BYTE_TRIG_O                    <= CTRL_BYTE_TRIG_I((C_CTRL_REG30_OFFSET/8)+3 downto C_CTRL_REG30_OFFSET/8);
  FE_CFG_0_1_REG_READ_O                     <= CTRL_REG_READ_I(C_CTRL_REG30_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_0_1_REG_WRITE_O                    <= CTRL_REG_WRITE_I(C_CTRL_REG30_OFFSET/CGN_BITS_PER_REG);
  FE0_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+24);
  FE0_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+24);
  FE0_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+23);
  FE0_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+23);
  FE0_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+22);
  FE0_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+22);
  FE0_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+21);
  FE0_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+21);
  FE0_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+20);
  FE0_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+20);
  FE0_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+19 downto C_CTRL_REG30_OFFSET+18);
  FE0_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+19 downto C_CTRL_REG30_OFFSET+18);
  FE0_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+17 downto C_CTRL_REG30_OFFSET+16);
  FE0_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+17 downto C_CTRL_REG30_OFFSET+16);
  FE1_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+8);
  FE1_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+8);
  FE1_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+7);
  FE1_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+7);
  FE1_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+6);
  FE1_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+6);
  FE1_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+5);
  FE1_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+5);
  FE1_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+4);
  FE1_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+4);
  FE1_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+3 downto C_CTRL_REG30_OFFSET+2);
  FE1_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+3 downto C_CTRL_REG30_OFFSET+2);
  FE1_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG30_OFFSET+1 downto C_CTRL_REG30_OFFSET+0);
  FE1_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG30_OFFSET+1 downto C_CTRL_REG30_OFFSET+0);

  -- Control Register 31 [0x107C]: FE_CFG_2_3
  FE_CFG_2_3_BYTE_TRIG_O                    <= CTRL_BYTE_TRIG_I((C_CTRL_REG31_OFFSET/8)+3 downto C_CTRL_REG31_OFFSET/8);
  FE_CFG_2_3_REG_READ_O                     <= CTRL_REG_READ_I(C_CTRL_REG31_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_2_3_REG_WRITE_O                    <= CTRL_REG_WRITE_I(C_CTRL_REG31_OFFSET/CGN_BITS_PER_REG);
  FE2_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+24);
  FE2_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+24);
  FE2_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+23);
  FE2_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+23);
  FE2_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+22);
  FE2_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+22);
  FE2_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+21);
  FE2_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+21);
  FE2_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+20);
  FE2_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+20);
  FE2_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+19 downto C_CTRL_REG31_OFFSET+18);
  FE2_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+19 downto C_CTRL_REG31_OFFSET+18);
  FE2_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+17 downto C_CTRL_REG31_OFFSET+16);
  FE2_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+17 downto C_CTRL_REG31_OFFSET+16);
  FE3_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+8);
  FE3_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+8);
  FE3_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+7);
  FE3_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+7);
  FE3_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+6);
  FE3_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+6);
  FE3_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+5);
  FE3_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+5);
  FE3_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+4);
  FE3_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+4);
  FE3_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+3 downto C_CTRL_REG31_OFFSET+2);
  FE3_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+3 downto C_CTRL_REG31_OFFSET+2);
  FE3_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG31_OFFSET+1 downto C_CTRL_REG31_OFFSET+0);
  FE3_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG31_OFFSET+1 downto C_CTRL_REG31_OFFSET+0);

  -- Control Register 32 [0x1080]: FE_CFG_4_5
  FE_CFG_4_5_BYTE_TRIG_O                    <= CTRL_BYTE_TRIG_I((C_CTRL_REG32_OFFSET/8)+3 downto C_CTRL_REG32_OFFSET/8);
  FE_CFG_4_5_REG_READ_O                     <= CTRL_REG_READ_I(C_CTRL_REG32_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_4_5_REG_WRITE_O                    <= CTRL_REG_WRITE_I(C_CTRL_REG32_OFFSET/CGN_BITS_PER_REG);
  FE4_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+24);
  FE4_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+24);
  FE4_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+23);
  FE4_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+23);
  FE4_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+22);
  FE4_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+22);
  FE4_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+21);
  FE4_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+21);
  FE4_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+20);
  FE4_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+20);
  FE4_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+19 downto C_CTRL_REG32_OFFSET+18);
  FE4_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+19 downto C_CTRL_REG32_OFFSET+18);
  FE4_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+17 downto C_CTRL_REG32_OFFSET+16);
  FE4_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+17 downto C_CTRL_REG32_OFFSET+16);
  FE5_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+8);
  FE5_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+8);
  FE5_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+7);
  FE5_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+7);
  FE5_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+6);
  FE5_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+6);
  FE5_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+5);
  FE5_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+5);
  FE5_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+4);
  FE5_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+4);
  FE5_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+3 downto C_CTRL_REG32_OFFSET+2);
  FE5_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+3 downto C_CTRL_REG32_OFFSET+2);
  FE5_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG32_OFFSET+1 downto C_CTRL_REG32_OFFSET+0);
  FE5_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG32_OFFSET+1 downto C_CTRL_REG32_OFFSET+0);

  -- Control Register 33 [0x1084]: FE_CFG_6_7
  FE_CFG_6_7_BYTE_TRIG_O                    <= CTRL_BYTE_TRIG_I((C_CTRL_REG33_OFFSET/8)+3 downto C_CTRL_REG33_OFFSET/8);
  FE_CFG_6_7_REG_READ_O                     <= CTRL_REG_READ_I(C_CTRL_REG33_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_6_7_REG_WRITE_O                    <= CTRL_REG_WRITE_I(C_CTRL_REG33_OFFSET/CGN_BITS_PER_REG);
  FE6_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+24);
  FE6_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+24);
  FE6_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+23);
  FE6_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+23);
  FE6_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+22);
  FE6_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+22);
  FE6_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+21);
  FE6_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+21);
  FE6_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+20);
  FE6_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+20);
  FE6_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+19 downto C_CTRL_REG33_OFFSET+18);
  FE6_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+19 downto C_CTRL_REG33_OFFSET+18);
  FE6_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+17 downto C_CTRL_REG33_OFFSET+16);
  FE6_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+17 downto C_CTRL_REG33_OFFSET+16);
  FE7_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+8);
  FE7_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+8);
  FE7_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+7);
  FE7_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+7);
  FE7_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+6);
  FE7_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+6);
  FE7_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+5);
  FE7_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+5);
  FE7_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+4);
  FE7_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+4);
  FE7_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+3 downto C_CTRL_REG33_OFFSET+2);
  FE7_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+3 downto C_CTRL_REG33_OFFSET+2);
  FE7_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG33_OFFSET+1 downto C_CTRL_REG33_OFFSET+0);
  FE7_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG33_OFFSET+1 downto C_CTRL_REG33_OFFSET+0);

  -- Control Register 34 [0x1088]: FE_CFG_8_9
  FE_CFG_8_9_BYTE_TRIG_O                    <= CTRL_BYTE_TRIG_I((C_CTRL_REG34_OFFSET/8)+3 downto C_CTRL_REG34_OFFSET/8);
  FE_CFG_8_9_REG_READ_O                     <= CTRL_REG_READ_I(C_CTRL_REG34_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_8_9_REG_WRITE_O                    <= CTRL_REG_WRITE_I(C_CTRL_REG34_OFFSET/CGN_BITS_PER_REG);
  FE8_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+24);
  FE8_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+24);
  FE8_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+23);
  FE8_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+23);
  FE8_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+22);
  FE8_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+22);
  FE8_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+21);
  FE8_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+21);
  FE8_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+20);
  FE8_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+20);
  FE8_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+19 downto C_CTRL_REG34_OFFSET+18);
  FE8_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+19 downto C_CTRL_REG34_OFFSET+18);
  FE8_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+17 downto C_CTRL_REG34_OFFSET+16);
  FE8_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+17 downto C_CTRL_REG34_OFFSET+16);
  FE9_PZC_EN_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+8);
  FE9_PZC_EN_O                              <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+8);
  FE9_AMPLIFIER2_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+7);
  FE9_AMPLIFIER2_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+7);
  FE9_AMPLIFIER2_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+6);
  FE9_AMPLIFIER2_EN_O                       <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+6);
  FE9_AMPLIFIER1_COMP_EN_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+5);
  FE9_AMPLIFIER1_COMP_EN_O                  <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+5);
  FE9_AMPLIFIER1_EN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+4);
  FE9_AMPLIFIER1_EN_O                       <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+4);
  FE9_ATTENUATION_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+3 downto C_CTRL_REG34_OFFSET+2);
  FE9_ATTENUATION_O                         <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+3 downto C_CTRL_REG34_OFFSET+2);
  FE9_MUX_BIT_TRIG_O                        <= CTRL_BIT_TRIG_I(C_CTRL_REG34_OFFSET+1 downto C_CTRL_REG34_OFFSET+0);
  FE9_MUX_O                                 <= CONTROL_REG_I(C_CTRL_REG34_OFFSET+1 downto C_CTRL_REG34_OFFSET+0);

  -- Control Register 35 [0x108C]: FE_CFG_10_11
  FE_CFG_10_11_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG35_OFFSET/8)+3 downto C_CTRL_REG35_OFFSET/8);
  FE_CFG_10_11_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG35_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_10_11_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG35_OFFSET/CGN_BITS_PER_REG);
  FE10_PZC_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+24);
  FE10_PZC_EN_O                             <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+24);
  FE10_AMPLIFIER2_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+23);
  FE10_AMPLIFIER2_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+23);
  FE10_AMPLIFIER2_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+22);
  FE10_AMPLIFIER2_EN_O                      <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+22);
  FE10_AMPLIFIER1_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+21);
  FE10_AMPLIFIER1_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+21);
  FE10_AMPLIFIER1_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+20);
  FE10_AMPLIFIER1_EN_O                      <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+20);
  FE10_ATTENUATION_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+19 downto C_CTRL_REG35_OFFSET+18);
  FE10_ATTENUATION_O                        <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+19 downto C_CTRL_REG35_OFFSET+18);
  FE10_MUX_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+17 downto C_CTRL_REG35_OFFSET+16);
  FE10_MUX_O                                <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+17 downto C_CTRL_REG35_OFFSET+16);
  FE11_PZC_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+8);
  FE11_PZC_EN_O                             <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+8);
  FE11_AMPLIFIER2_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+7);
  FE11_AMPLIFIER2_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+7);
  FE11_AMPLIFIER2_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+6);
  FE11_AMPLIFIER2_EN_O                      <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+6);
  FE11_AMPLIFIER1_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+5);
  FE11_AMPLIFIER1_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+5);
  FE11_AMPLIFIER1_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+4);
  FE11_AMPLIFIER1_EN_O                      <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+4);
  FE11_ATTENUATION_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+3 downto C_CTRL_REG35_OFFSET+2);
  FE11_ATTENUATION_O                        <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+3 downto C_CTRL_REG35_OFFSET+2);
  FE11_MUX_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG35_OFFSET+1 downto C_CTRL_REG35_OFFSET+0);
  FE11_MUX_O                                <= CONTROL_REG_I(C_CTRL_REG35_OFFSET+1 downto C_CTRL_REG35_OFFSET+0);

  -- Control Register 36 [0x1090]: FE_CFG_12_13
  FE_CFG_12_13_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG36_OFFSET/8)+3 downto C_CTRL_REG36_OFFSET/8);
  FE_CFG_12_13_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG36_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_12_13_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG36_OFFSET/CGN_BITS_PER_REG);
  FE12_PZC_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+24);
  FE12_PZC_EN_O                             <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+24);
  FE12_AMPLIFIER2_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+23);
  FE12_AMPLIFIER2_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+23);
  FE12_AMPLIFIER2_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+22);
  FE12_AMPLIFIER2_EN_O                      <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+22);
  FE12_AMPLIFIER1_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+21);
  FE12_AMPLIFIER1_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+21);
  FE12_AMPLIFIER1_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+20);
  FE12_AMPLIFIER1_EN_O                      <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+20);
  FE12_ATTENUATION_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+19 downto C_CTRL_REG36_OFFSET+18);
  FE12_ATTENUATION_O                        <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+19 downto C_CTRL_REG36_OFFSET+18);
  FE12_MUX_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+17 downto C_CTRL_REG36_OFFSET+16);
  FE12_MUX_O                                <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+17 downto C_CTRL_REG36_OFFSET+16);
  FE13_PZC_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+8);
  FE13_PZC_EN_O                             <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+8);
  FE13_AMPLIFIER2_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+7);
  FE13_AMPLIFIER2_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+7);
  FE13_AMPLIFIER2_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+6);
  FE13_AMPLIFIER2_EN_O                      <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+6);
  FE13_AMPLIFIER1_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+5);
  FE13_AMPLIFIER1_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+5);
  FE13_AMPLIFIER1_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+4);
  FE13_AMPLIFIER1_EN_O                      <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+4);
  FE13_ATTENUATION_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+3 downto C_CTRL_REG36_OFFSET+2);
  FE13_ATTENUATION_O                        <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+3 downto C_CTRL_REG36_OFFSET+2);
  FE13_MUX_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG36_OFFSET+1 downto C_CTRL_REG36_OFFSET+0);
  FE13_MUX_O                                <= CONTROL_REG_I(C_CTRL_REG36_OFFSET+1 downto C_CTRL_REG36_OFFSET+0);

  -- Control Register 37 [0x1094]: FE_CFG_14_15
  FE_CFG_14_15_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG37_OFFSET/8)+3 downto C_CTRL_REG37_OFFSET/8);
  FE_CFG_14_15_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG37_OFFSET/CGN_BITS_PER_REG);
  FE_CFG_14_15_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG37_OFFSET/CGN_BITS_PER_REG);
  FE14_PZC_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+24);
  FE14_PZC_EN_O                             <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+24);
  FE14_AMPLIFIER2_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+23);
  FE14_AMPLIFIER2_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+23);
  FE14_AMPLIFIER2_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+22);
  FE14_AMPLIFIER2_EN_O                      <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+22);
  FE14_AMPLIFIER1_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+21);
  FE14_AMPLIFIER1_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+21);
  FE14_AMPLIFIER1_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+20);
  FE14_AMPLIFIER1_EN_O                      <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+20);
  FE14_ATTENUATION_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+19 downto C_CTRL_REG37_OFFSET+18);
  FE14_ATTENUATION_O                        <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+19 downto C_CTRL_REG37_OFFSET+18);
  FE14_MUX_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+17 downto C_CTRL_REG37_OFFSET+16);
  FE14_MUX_O                                <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+17 downto C_CTRL_REG37_OFFSET+16);
  FE15_PZC_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+8);
  FE15_PZC_EN_O                             <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+8);
  FE15_AMPLIFIER2_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+7);
  FE15_AMPLIFIER2_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+7);
  FE15_AMPLIFIER2_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+6);
  FE15_AMPLIFIER2_EN_O                      <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+6);
  FE15_AMPLIFIER1_COMP_EN_BIT_TRIG_O        <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+5);
  FE15_AMPLIFIER1_COMP_EN_O                 <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+5);
  FE15_AMPLIFIER1_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+4);
  FE15_AMPLIFIER1_EN_O                      <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+4);
  FE15_ATTENUATION_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+3 downto C_CTRL_REG37_OFFSET+2);
  FE15_ATTENUATION_O                        <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+3 downto C_CTRL_REG37_OFFSET+2);
  FE15_MUX_BIT_TRIG_O                       <= CTRL_BIT_TRIG_I(C_CTRL_REG37_OFFSET+1 downto C_CTRL_REG37_OFFSET+0);
  FE15_MUX_O                                <= CONTROL_REG_I(C_CTRL_REG37_OFFSET+1 downto C_CTRL_REG37_OFFSET+0);

  -- Control Register 38 [0x1098]: HV_U_TARGET_0
  HV_U_TARGET_0_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG38_OFFSET/8)+3 downto C_CTRL_REG38_OFFSET/8);
  HV_U_TARGET_0_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG38_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_0_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG38_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_0_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG38_OFFSET+31 downto C_CTRL_REG38_OFFSET+0);
  HV_U_TARGET_0_O                           <= CONTROL_REG_I(C_CTRL_REG38_OFFSET+31 downto C_CTRL_REG38_OFFSET+0);

  -- Control Register 39 [0x109C]: HV_U_TARGET_1
  HV_U_TARGET_1_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG39_OFFSET/8)+3 downto C_CTRL_REG39_OFFSET/8);
  HV_U_TARGET_1_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG39_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_1_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG39_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_1_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG39_OFFSET+31 downto C_CTRL_REG39_OFFSET+0);
  HV_U_TARGET_1_O                           <= CONTROL_REG_I(C_CTRL_REG39_OFFSET+31 downto C_CTRL_REG39_OFFSET+0);

  -- Control Register 40 [0x10A0]: HV_U_TARGET_2
  HV_U_TARGET_2_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG40_OFFSET/8)+3 downto C_CTRL_REG40_OFFSET/8);
  HV_U_TARGET_2_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG40_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_2_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG40_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_2_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG40_OFFSET+31 downto C_CTRL_REG40_OFFSET+0);
  HV_U_TARGET_2_O                           <= CONTROL_REG_I(C_CTRL_REG40_OFFSET+31 downto C_CTRL_REG40_OFFSET+0);

  -- Control Register 41 [0x10A4]: HV_U_TARGET_3
  HV_U_TARGET_3_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG41_OFFSET/8)+3 downto C_CTRL_REG41_OFFSET/8);
  HV_U_TARGET_3_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG41_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_3_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG41_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_3_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG41_OFFSET+31 downto C_CTRL_REG41_OFFSET+0);
  HV_U_TARGET_3_O                           <= CONTROL_REG_I(C_CTRL_REG41_OFFSET+31 downto C_CTRL_REG41_OFFSET+0);

  -- Control Register 42 [0x10A8]: HV_U_TARGET_4
  HV_U_TARGET_4_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG42_OFFSET/8)+3 downto C_CTRL_REG42_OFFSET/8);
  HV_U_TARGET_4_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG42_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_4_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG42_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_4_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG42_OFFSET+31 downto C_CTRL_REG42_OFFSET+0);
  HV_U_TARGET_4_O                           <= CONTROL_REG_I(C_CTRL_REG42_OFFSET+31 downto C_CTRL_REG42_OFFSET+0);

  -- Control Register 43 [0x10AC]: HV_U_TARGET_5
  HV_U_TARGET_5_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG43_OFFSET/8)+3 downto C_CTRL_REG43_OFFSET/8);
  HV_U_TARGET_5_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG43_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_5_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG43_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_5_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG43_OFFSET+31 downto C_CTRL_REG43_OFFSET+0);
  HV_U_TARGET_5_O                           <= CONTROL_REG_I(C_CTRL_REG43_OFFSET+31 downto C_CTRL_REG43_OFFSET+0);

  -- Control Register 44 [0x10B0]: HV_U_TARGET_6
  HV_U_TARGET_6_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG44_OFFSET/8)+3 downto C_CTRL_REG44_OFFSET/8);
  HV_U_TARGET_6_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG44_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_6_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG44_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_6_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG44_OFFSET+31 downto C_CTRL_REG44_OFFSET+0);
  HV_U_TARGET_6_O                           <= CONTROL_REG_I(C_CTRL_REG44_OFFSET+31 downto C_CTRL_REG44_OFFSET+0);

  -- Control Register 45 [0x10B4]: HV_U_TARGET_7
  HV_U_TARGET_7_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG45_OFFSET/8)+3 downto C_CTRL_REG45_OFFSET/8);
  HV_U_TARGET_7_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG45_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_7_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG45_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_7_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG45_OFFSET+31 downto C_CTRL_REG45_OFFSET+0);
  HV_U_TARGET_7_O                           <= CONTROL_REG_I(C_CTRL_REG45_OFFSET+31 downto C_CTRL_REG45_OFFSET+0);

  -- Control Register 46 [0x10B8]: HV_U_TARGET_8
  HV_U_TARGET_8_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG46_OFFSET/8)+3 downto C_CTRL_REG46_OFFSET/8);
  HV_U_TARGET_8_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG46_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_8_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG46_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_8_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG46_OFFSET+31 downto C_CTRL_REG46_OFFSET+0);
  HV_U_TARGET_8_O                           <= CONTROL_REG_I(C_CTRL_REG46_OFFSET+31 downto C_CTRL_REG46_OFFSET+0);

  -- Control Register 47 [0x10BC]: HV_U_TARGET_9
  HV_U_TARGET_9_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG47_OFFSET/8)+3 downto C_CTRL_REG47_OFFSET/8);
  HV_U_TARGET_9_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG47_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_9_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG47_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_9_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG47_OFFSET+31 downto C_CTRL_REG47_OFFSET+0);
  HV_U_TARGET_9_O                           <= CONTROL_REG_I(C_CTRL_REG47_OFFSET+31 downto C_CTRL_REG47_OFFSET+0);

  -- Control Register 48 [0x10C0]: HV_U_TARGET_10
  HV_U_TARGET_10_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG48_OFFSET/8)+3 downto C_CTRL_REG48_OFFSET/8);
  HV_U_TARGET_10_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG48_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_10_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG48_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_10_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG48_OFFSET+31 downto C_CTRL_REG48_OFFSET+0);
  HV_U_TARGET_10_O                          <= CONTROL_REG_I(C_CTRL_REG48_OFFSET+31 downto C_CTRL_REG48_OFFSET+0);

  -- Control Register 49 [0x10C4]: HV_U_TARGET_11
  HV_U_TARGET_11_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG49_OFFSET/8)+3 downto C_CTRL_REG49_OFFSET/8);
  HV_U_TARGET_11_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG49_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_11_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG49_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_11_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG49_OFFSET+31 downto C_CTRL_REG49_OFFSET+0);
  HV_U_TARGET_11_O                          <= CONTROL_REG_I(C_CTRL_REG49_OFFSET+31 downto C_CTRL_REG49_OFFSET+0);

  -- Control Register 50 [0x10C8]: HV_U_TARGET_12
  HV_U_TARGET_12_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG50_OFFSET/8)+3 downto C_CTRL_REG50_OFFSET/8);
  HV_U_TARGET_12_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG50_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_12_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG50_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_12_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG50_OFFSET+31 downto C_CTRL_REG50_OFFSET+0);
  HV_U_TARGET_12_O                          <= CONTROL_REG_I(C_CTRL_REG50_OFFSET+31 downto C_CTRL_REG50_OFFSET+0);

  -- Control Register 51 [0x10CC]: HV_U_TARGET_13
  HV_U_TARGET_13_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG51_OFFSET/8)+3 downto C_CTRL_REG51_OFFSET/8);
  HV_U_TARGET_13_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG51_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_13_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG51_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_13_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG51_OFFSET+31 downto C_CTRL_REG51_OFFSET+0);
  HV_U_TARGET_13_O                          <= CONTROL_REG_I(C_CTRL_REG51_OFFSET+31 downto C_CTRL_REG51_OFFSET+0);

  -- Control Register 52 [0x10D0]: HV_U_TARGET_14
  HV_U_TARGET_14_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG52_OFFSET/8)+3 downto C_CTRL_REG52_OFFSET/8);
  HV_U_TARGET_14_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG52_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_14_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG52_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_14_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG52_OFFSET+31 downto C_CTRL_REG52_OFFSET+0);
  HV_U_TARGET_14_O                          <= CONTROL_REG_I(C_CTRL_REG52_OFFSET+31 downto C_CTRL_REG52_OFFSET+0);

  -- Control Register 53 [0x10D4]: HV_U_TARGET_15
  HV_U_TARGET_15_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG53_OFFSET/8)+3 downto C_CTRL_REG53_OFFSET/8);
  HV_U_TARGET_15_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG53_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_15_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG53_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_15_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG53_OFFSET+31 downto C_CTRL_REG53_OFFSET+0);
  HV_U_TARGET_15_O                          <= CONTROL_REG_I(C_CTRL_REG53_OFFSET+31 downto C_CTRL_REG53_OFFSET+0);

  -- Control Register 54 [0x10D8]: HV_R_SHUNT
  HV_R_SHUNT_BYTE_TRIG_O                    <= CTRL_BYTE_TRIG_I((C_CTRL_REG54_OFFSET/8)+3 downto C_CTRL_REG54_OFFSET/8);
  HV_R_SHUNT_REG_READ_O                     <= CTRL_REG_READ_I(C_CTRL_REG54_OFFSET/CGN_BITS_PER_REG);
  HV_R_SHUNT_REG_WRITE_O                    <= CTRL_REG_WRITE_I(C_CTRL_REG54_OFFSET/CGN_BITS_PER_REG);
  HV_R_SHUNT_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG54_OFFSET+31 downto C_CTRL_REG54_OFFSET+0);
  HV_R_SHUNT_O                              <= CONTROL_REG_I(C_CTRL_REG54_OFFSET+31 downto C_CTRL_REG54_OFFSET+0);

  -- Control Register 55 [0x10DC]: LMK_0
  LMK_0_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG55_OFFSET/8)+3 downto C_CTRL_REG55_OFFSET/8);
  LMK_0_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG55_OFFSET/CGN_BITS_PER_REG);
  LMK_0_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG55_OFFSET/CGN_BITS_PER_REG);
  LMK0_RESET_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG55_OFFSET+31);
  LMK0_RESET_O                              <= CONTROL_REG_I(C_CTRL_REG55_OFFSET+31);
  LMK0_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG55_OFFSET+18 downto C_CTRL_REG55_OFFSET+17);
  LMK0_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG55_OFFSET+18 downto C_CTRL_REG55_OFFSET+17);
  LMK0_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG55_OFFSET+16);
  LMK0_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG55_OFFSET+16);
  LMK0_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG55_OFFSET+15 downto C_CTRL_REG55_OFFSET+8);
  LMK0_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG55_OFFSET+15 downto C_CTRL_REG55_OFFSET+8);
  LMK0_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG55_OFFSET+7 downto C_CTRL_REG55_OFFSET+4);
  LMK0_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG55_OFFSET+7 downto C_CTRL_REG55_OFFSET+4);

  -- Control Register 56 [0x10E0]: LMK_1
  LMK_1_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG56_OFFSET/8)+3 downto C_CTRL_REG56_OFFSET/8);
  LMK_1_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG56_OFFSET/CGN_BITS_PER_REG);
  LMK_1_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG56_OFFSET/CGN_BITS_PER_REG);
  LMK1_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG56_OFFSET+18 downto C_CTRL_REG56_OFFSET+17);
  LMK1_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG56_OFFSET+18 downto C_CTRL_REG56_OFFSET+17);
  LMK1_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG56_OFFSET+16);
  LMK1_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG56_OFFSET+16);
  LMK1_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG56_OFFSET+15 downto C_CTRL_REG56_OFFSET+8);
  LMK1_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG56_OFFSET+15 downto C_CTRL_REG56_OFFSET+8);
  LMK1_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG56_OFFSET+7 downto C_CTRL_REG56_OFFSET+4);
  LMK1_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG56_OFFSET+7 downto C_CTRL_REG56_OFFSET+4);

  -- Control Register 57 [0x10E4]: LMK_2
  LMK_2_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG57_OFFSET/8)+3 downto C_CTRL_REG57_OFFSET/8);
  LMK_2_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG57_OFFSET/CGN_BITS_PER_REG);
  LMK_2_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG57_OFFSET/CGN_BITS_PER_REG);
  LMK2_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG57_OFFSET+18 downto C_CTRL_REG57_OFFSET+17);
  LMK2_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG57_OFFSET+18 downto C_CTRL_REG57_OFFSET+17);
  LMK2_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG57_OFFSET+16);
  LMK2_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG57_OFFSET+16);
  LMK2_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG57_OFFSET+15 downto C_CTRL_REG57_OFFSET+8);
  LMK2_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG57_OFFSET+15 downto C_CTRL_REG57_OFFSET+8);
  LMK2_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG57_OFFSET+7 downto C_CTRL_REG57_OFFSET+4);
  LMK2_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG57_OFFSET+7 downto C_CTRL_REG57_OFFSET+4);

  -- Control Register 58 [0x10E8]: LMK_3
  LMK_3_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG58_OFFSET/8)+3 downto C_CTRL_REG58_OFFSET/8);
  LMK_3_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG58_OFFSET/CGN_BITS_PER_REG);
  LMK_3_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG58_OFFSET/CGN_BITS_PER_REG);
  LMK3_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG58_OFFSET+18 downto C_CTRL_REG58_OFFSET+17);
  LMK3_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG58_OFFSET+18 downto C_CTRL_REG58_OFFSET+17);
  LMK3_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG58_OFFSET+16);
  LMK3_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG58_OFFSET+16);
  LMK3_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG58_OFFSET+15 downto C_CTRL_REG58_OFFSET+8);
  LMK3_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG58_OFFSET+15 downto C_CTRL_REG58_OFFSET+8);
  LMK3_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG58_OFFSET+7 downto C_CTRL_REG58_OFFSET+4);
  LMK3_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG58_OFFSET+7 downto C_CTRL_REG58_OFFSET+4);

  -- Control Register 59 [0x10EC]: LMK_4
  LMK_4_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG59_OFFSET/8)+3 downto C_CTRL_REG59_OFFSET/8);
  LMK_4_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG59_OFFSET/CGN_BITS_PER_REG);
  LMK_4_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG59_OFFSET/CGN_BITS_PER_REG);
  LMK4_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG59_OFFSET+18 downto C_CTRL_REG59_OFFSET+17);
  LMK4_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG59_OFFSET+18 downto C_CTRL_REG59_OFFSET+17);
  LMK4_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG59_OFFSET+16);
  LMK4_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG59_OFFSET+16);
  LMK4_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG59_OFFSET+15 downto C_CTRL_REG59_OFFSET+8);
  LMK4_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG59_OFFSET+15 downto C_CTRL_REG59_OFFSET+8);
  LMK4_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG59_OFFSET+7 downto C_CTRL_REG59_OFFSET+4);
  LMK4_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG59_OFFSET+7 downto C_CTRL_REG59_OFFSET+4);

  -- Control Register 60 [0x10F0]: LMK_5
  LMK_5_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG60_OFFSET/8)+3 downto C_CTRL_REG60_OFFSET/8);
  LMK_5_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG60_OFFSET/CGN_BITS_PER_REG);
  LMK_5_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG60_OFFSET/CGN_BITS_PER_REG);
  LMK5_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG60_OFFSET+18 downto C_CTRL_REG60_OFFSET+17);
  LMK5_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG60_OFFSET+18 downto C_CTRL_REG60_OFFSET+17);
  LMK5_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG60_OFFSET+16);
  LMK5_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG60_OFFSET+16);
  LMK5_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG60_OFFSET+15 downto C_CTRL_REG60_OFFSET+8);
  LMK5_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG60_OFFSET+15 downto C_CTRL_REG60_OFFSET+8);
  LMK5_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG60_OFFSET+7 downto C_CTRL_REG60_OFFSET+4);
  LMK5_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG60_OFFSET+7 downto C_CTRL_REG60_OFFSET+4);

  -- Control Register 61 [0x10F4]: LMK_6
  LMK_6_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG61_OFFSET/8)+3 downto C_CTRL_REG61_OFFSET/8);
  LMK_6_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG61_OFFSET/CGN_BITS_PER_REG);
  LMK_6_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG61_OFFSET/CGN_BITS_PER_REG);
  LMK6_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG61_OFFSET+18 downto C_CTRL_REG61_OFFSET+17);
  LMK6_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG61_OFFSET+18 downto C_CTRL_REG61_OFFSET+17);
  LMK6_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG61_OFFSET+16);
  LMK6_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG61_OFFSET+16);
  LMK6_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG61_OFFSET+15 downto C_CTRL_REG61_OFFSET+8);
  LMK6_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG61_OFFSET+15 downto C_CTRL_REG61_OFFSET+8);
  LMK6_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG61_OFFSET+7 downto C_CTRL_REG61_OFFSET+4);
  LMK6_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG61_OFFSET+7 downto C_CTRL_REG61_OFFSET+4);

  -- Control Register 62 [0x10F8]: LMK_7
  LMK_7_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG62_OFFSET/8)+3 downto C_CTRL_REG62_OFFSET/8);
  LMK_7_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG62_OFFSET/CGN_BITS_PER_REG);
  LMK_7_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG62_OFFSET/CGN_BITS_PER_REG);
  LMK7_CLKOUT_MUX_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG62_OFFSET+18 downto C_CTRL_REG62_OFFSET+17);
  LMK7_CLKOUT_MUX_O                         <= CONTROL_REG_I(C_CTRL_REG62_OFFSET+18 downto C_CTRL_REG62_OFFSET+17);
  LMK7_CLKOUT_EN_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG62_OFFSET+16);
  LMK7_CLKOUT_EN_O                          <= CONTROL_REG_I(C_CTRL_REG62_OFFSET+16);
  LMK7_CLKOUT_DIV_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG62_OFFSET+15 downto C_CTRL_REG62_OFFSET+8);
  LMK7_CLKOUT_DIV_O                         <= CONTROL_REG_I(C_CTRL_REG62_OFFSET+15 downto C_CTRL_REG62_OFFSET+8);
  LMK7_CLKOUT_DLY_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG62_OFFSET+7 downto C_CTRL_REG62_OFFSET+4);
  LMK7_CLKOUT_DLY_O                         <= CONTROL_REG_I(C_CTRL_REG62_OFFSET+7 downto C_CTRL_REG62_OFFSET+4);

  -- Control Register 63 [0x10FC]: LMK_8
  LMK_8_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG63_OFFSET/8)+3 downto C_CTRL_REG63_OFFSET/8);
  LMK_8_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG63_OFFSET/CGN_BITS_PER_REG);
  LMK_8_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG63_OFFSET/CGN_BITS_PER_REG);
  LMK8_PHASE_NOISE_OPT_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG63_OFFSET+31 downto C_CTRL_REG63_OFFSET+4);
  LMK8_PHASE_NOISE_OPT_O                    <= CONTROL_REG_I(C_CTRL_REG63_OFFSET+31 downto C_CTRL_REG63_OFFSET+4);

  -- Control Register 64 [0x1100]: LMK_9
  LMK_9_BYTE_TRIG_O                         <= CTRL_BYTE_TRIG_I((C_CTRL_REG64_OFFSET/8)+3 downto C_CTRL_REG64_OFFSET/8);
  LMK_9_REG_READ_O                          <= CTRL_REG_READ_I(C_CTRL_REG64_OFFSET/CGN_BITS_PER_REG);
  LMK_9_REG_WRITE_O                         <= CTRL_REG_WRITE_I(C_CTRL_REG64_OFFSET/CGN_BITS_PER_REG);
  LMK9_VBOOST_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG64_OFFSET+16);
  LMK9_VBOOST_O                             <= CONTROL_REG_I(C_CTRL_REG64_OFFSET+16);

  -- Control Register 65 [0x1104]: LMK_11
  LMK_11_BYTE_TRIG_O                        <= CTRL_BYTE_TRIG_I((C_CTRL_REG65_OFFSET/8)+3 downto C_CTRL_REG65_OFFSET/8);
  LMK_11_REG_READ_O                         <= CTRL_REG_READ_I(C_CTRL_REG65_OFFSET/CGN_BITS_PER_REG);
  LMK_11_REG_WRITE_O                        <= CTRL_REG_WRITE_I(C_CTRL_REG65_OFFSET/CGN_BITS_PER_REG);
  LMK11_DIV4_BIT_TRIG_O                     <= CTRL_BIT_TRIG_I(C_CTRL_REG65_OFFSET+15);
  LMK11_DIV4_O                              <= CONTROL_REG_I(C_CTRL_REG65_OFFSET+15);

  -- Control Register 66 [0x1108]: LMK_13
  LMK_13_BYTE_TRIG_O                        <= CTRL_BYTE_TRIG_I((C_CTRL_REG66_OFFSET/8)+3 downto C_CTRL_REG66_OFFSET/8);
  LMK_13_REG_READ_O                         <= CTRL_REG_READ_I(C_CTRL_REG66_OFFSET/CGN_BITS_PER_REG);
  LMK_13_REG_WRITE_O                        <= CTRL_REG_WRITE_I(C_CTRL_REG66_OFFSET/CGN_BITS_PER_REG);
  LMK13_OSCIN_FREQ_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG66_OFFSET+21 downto C_CTRL_REG66_OFFSET+14);
  LMK13_OSCIN_FREQ_O                        <= CONTROL_REG_I(C_CTRL_REG66_OFFSET+21 downto C_CTRL_REG66_OFFSET+14);
  LMK13_VCO_R4_LF_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG66_OFFSET+13 downto C_CTRL_REG66_OFFSET+11);
  LMK13_VCO_R4_LF_O                         <= CONTROL_REG_I(C_CTRL_REG66_OFFSET+13 downto C_CTRL_REG66_OFFSET+11);
  LMK13_VCO_R3_LF_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG66_OFFSET+10 downto C_CTRL_REG66_OFFSET+8);
  LMK13_VCO_R3_LF_O                         <= CONTROL_REG_I(C_CTRL_REG66_OFFSET+10 downto C_CTRL_REG66_OFFSET+8);
  LMK13_VCO_C3_C4_LF_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG66_OFFSET+7 downto C_CTRL_REG66_OFFSET+4);
  LMK13_VCO_C3_C4_LF_O                      <= CONTROL_REG_I(C_CTRL_REG66_OFFSET+7 downto C_CTRL_REG66_OFFSET+4);

  -- Control Register 67 [0x110C]: LMK_14
  LMK_14_BYTE_TRIG_O                        <= CTRL_BYTE_TRIG_I((C_CTRL_REG67_OFFSET/8)+3 downto C_CTRL_REG67_OFFSET/8);
  LMK_14_REG_READ_O                         <= CTRL_REG_READ_I(C_CTRL_REG67_OFFSET/CGN_BITS_PER_REG);
  LMK_14_REG_WRITE_O                        <= CTRL_REG_WRITE_I(C_CTRL_REG67_OFFSET/CGN_BITS_PER_REG);
  LMK14_EN_FOUT_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG67_OFFSET+28);
  LMK14_EN_FOUT_O                           <= CONTROL_REG_I(C_CTRL_REG67_OFFSET+28);
  LMK14_EN_CLKOUT_GLOBAL_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG67_OFFSET+27);
  LMK14_EN_CLKOUT_GLOBAL_O                  <= CONTROL_REG_I(C_CTRL_REG67_OFFSET+27);
  LMK14_POWERDOWN_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG67_OFFSET+26);
  LMK14_POWERDOWN_O                         <= CONTROL_REG_I(C_CTRL_REG67_OFFSET+26);
  LMK14_PLL_MUX_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG67_OFFSET+23 downto C_CTRL_REG67_OFFSET+20);
  LMK14_PLL_MUX_O                           <= CONTROL_REG_I(C_CTRL_REG67_OFFSET+23 downto C_CTRL_REG67_OFFSET+20);
  LMK14_PLL_R_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG67_OFFSET+19 downto C_CTRL_REG67_OFFSET+8);
  LMK14_PLL_R_O                             <= CONTROL_REG_I(C_CTRL_REG67_OFFSET+19 downto C_CTRL_REG67_OFFSET+8);

  -- Control Register 68 [0x1110]: LMK_15
  LMK_15_BYTE_TRIG_O                        <= CTRL_BYTE_TRIG_I((C_CTRL_REG68_OFFSET/8)+3 downto C_CTRL_REG68_OFFSET/8);
  LMK_15_REG_READ_O                         <= CTRL_REG_READ_I(C_CTRL_REG68_OFFSET/CGN_BITS_PER_REG);
  LMK_15_REG_WRITE_O                        <= CTRL_REG_WRITE_I(C_CTRL_REG68_OFFSET/CGN_BITS_PER_REG);
  LMK15_PLL_CP_GAIN_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG68_OFFSET+31 downto C_CTRL_REG68_OFFSET+30);
  LMK15_PLL_CP_GAIN_O                       <= CONTROL_REG_I(C_CTRL_REG68_OFFSET+31 downto C_CTRL_REG68_OFFSET+30);
  LMK15_VCO_DIV_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG68_OFFSET+29 downto C_CTRL_REG68_OFFSET+26);
  LMK15_VCO_DIV_O                           <= CONTROL_REG_I(C_CTRL_REG68_OFFSET+29 downto C_CTRL_REG68_OFFSET+26);
  LMK15_PLL_N_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG68_OFFSET+25 downto C_CTRL_REG68_OFFSET+8);
  LMK15_PLL_N_O                             <= CONTROL_REG_I(C_CTRL_REG68_OFFSET+25 downto C_CTRL_REG68_OFFSET+8);

  -- Control Register 69 [0x1114]: ADC_0_CFG_1458
  -- '--> Pure software register => no ports assigned

  -- Control Register 70 [0x1118]: ADC_0_CFG_2367
  -- '--> Pure software register => no ports assigned

  -- Control Register 71 [0x111C]: ADC_1_CFG_1458
  -- '--> Pure software register => no ports assigned

  -- Control Register 72 [0x1120]: ADC_1_CFG_2367
  -- '--> Pure software register => no ports assigned

  -- Control Register 73 [0x1124]: TRG_CFG
  TRG_CFG_BYTE_TRIG_O                       <= CTRL_BYTE_TRIG_I((C_CTRL_REG73_OFFSET/8)+3 downto C_CTRL_REG73_OFFSET/8);
  TRG_CFG_REG_READ_O                        <= CTRL_REG_READ_I(C_CTRL_REG73_OFFSET/CGN_BITS_PER_REG);
  TRG_CFG_REG_WRITE_O                       <= CTRL_REG_WRITE_I(C_CTRL_REG73_OFFSET/CGN_BITS_PER_REG);
  LEAD_TRAIL_EDGE_SEL_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG73_OFFSET+16);
  LEAD_TRAIL_EDGE_SEL_O                     <= CONTROL_REG_I(C_CTRL_REG73_OFFSET+16);
  EXT_TRIGGER_OUT_ENABLE_BIT_TRIG_O         <= CTRL_BIT_TRIG_I(C_CTRL_REG73_OFFSET+15);
  EXT_TRIGGER_OUT_ENABLE_O                  <= CONTROL_REG_I(C_CTRL_REG73_OFFSET+15);
  EXT_ASYNC_TRIGGER_EN_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG73_OFFSET+13);
  EXT_ASYNC_TRIGGER_EN_O                    <= CONTROL_REG_I(C_CTRL_REG73_OFFSET+13);
  PATTERN_TRIGGER_EN_BIT_TRIG_O             <= CTRL_BIT_TRIG_I(C_CTRL_REG73_OFFSET+12);
  PATTERN_TRIGGER_EN_O                      <= CONTROL_REG_I(C_CTRL_REG73_OFFSET+12);
  TRIGGER_OUT_PULSE_LENGTH_BIT_TRIG_O       <= CTRL_BIT_TRIG_I(C_CTRL_REG73_OFFSET+10 downto C_CTRL_REG73_OFFSET+8);
  TRIGGER_OUT_PULSE_LENGTH_O                <= CONTROL_REG_I(C_CTRL_REG73_OFFSET+10 downto C_CTRL_REG73_OFFSET+8);
  TRIGGER_DELAY_BIT_TRIG_O                  <= CTRL_BIT_TRIG_I(C_CTRL_REG73_OFFSET+7 downto C_CTRL_REG73_OFFSET+0);
  TRIGGER_DELAY_O                           <= CONTROL_REG_I(C_CTRL_REG73_OFFSET+7 downto C_CTRL_REG73_OFFSET+0);

  -- Control Register 74 [0x1128]: TRG_SRC_POL
  TRG_SRC_POL_BYTE_TRIG_O                   <= CTRL_BYTE_TRIG_I((C_CTRL_REG74_OFFSET/8)+3 downto C_CTRL_REG74_OFFSET/8);
  TRG_SRC_POL_REG_READ_O                    <= CTRL_REG_READ_I(C_CTRL_REG74_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_POL_REG_WRITE_O                   <= CTRL_REG_WRITE_I(C_CTRL_REG74_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_POLARITY_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG74_OFFSET+18 downto C_CTRL_REG74_OFFSET+0);
  TRG_SRC_POLARITY_O                        <= CONTROL_REG_I(C_CTRL_REG74_OFFSET+18 downto C_CTRL_REG74_OFFSET+0);

  -- Control Register 75 [0x112C]: TRG_AUTO_PERIOD
  TRG_AUTO_PERIOD_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG75_OFFSET/8)+3 downto C_CTRL_REG75_OFFSET/8);
  TRG_AUTO_PERIOD_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG75_OFFSET/CGN_BITS_PER_REG);
  TRG_AUTO_PERIOD_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG75_OFFSET/CGN_BITS_PER_REG);
  AUTO_TRIGGER_PERIOD_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG75_OFFSET+31 downto C_CTRL_REG75_OFFSET+0);
  AUTO_TRIGGER_PERIOD_O                     <= CONTROL_REG_I(C_CTRL_REG75_OFFSET+31 downto C_CTRL_REG75_OFFSET+0);

  -- Control Register 76 [0x1130]: TRG_PTRN_EN
  TRG_PTRN_EN_BYTE_TRIG_O                   <= CTRL_BYTE_TRIG_I((C_CTRL_REG76_OFFSET/8)+3 downto C_CTRL_REG76_OFFSET/8);
  TRG_PTRN_EN_REG_READ_O                    <= CTRL_REG_READ_I(C_CTRL_REG76_OFFSET/CGN_BITS_PER_REG);
  TRG_PTRN_EN_REG_WRITE_O                   <= CTRL_REG_WRITE_I(C_CTRL_REG76_OFFSET/CGN_BITS_PER_REG);
  TRG_PTRN_EN_BIT_TRIG_O                    <= CTRL_BIT_TRIG_I(C_CTRL_REG76_OFFSET+18 downto C_CTRL_REG76_OFFSET+0);
  TRG_PTRN_EN_O                             <= CONTROL_REG_I(C_CTRL_REG76_OFFSET+18 downto C_CTRL_REG76_OFFSET+0);

  -- Control Register 77 [0x1134]: TRG_SRC_EN_PTRN0
  TRG_SRC_EN_PTRN0_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG77_OFFSET/8)+3 downto C_CTRL_REG77_OFFSET/8);
  TRG_SRC_EN_PTRN0_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG77_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN0_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG77_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN0_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG77_OFFSET+18 downto C_CTRL_REG77_OFFSET+0);
  TRG_SRC_EN_PTRN0_O                        <= CONTROL_REG_I(C_CTRL_REG77_OFFSET+18 downto C_CTRL_REG77_OFFSET+0);

  -- Control Register 78 [0x1138]: TRG_STATE_PTRN0
  TRG_STATE_PTRN0_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG78_OFFSET/8)+3 downto C_CTRL_REG78_OFFSET/8);
  TRG_STATE_PTRN0_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG78_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN0_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG78_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN0_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG78_OFFSET+18 downto C_CTRL_REG78_OFFSET+0);
  TRG_STATE_PTRN0_O                         <= CONTROL_REG_I(C_CTRL_REG78_OFFSET+18 downto C_CTRL_REG78_OFFSET+0);

  -- Control Register 79 [0x113C]: TRG_SRC_EN_PTRN1
  TRG_SRC_EN_PTRN1_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG79_OFFSET/8)+3 downto C_CTRL_REG79_OFFSET/8);
  TRG_SRC_EN_PTRN1_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG79_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN1_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG79_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN1_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG79_OFFSET+18 downto C_CTRL_REG79_OFFSET+0);
  TRG_SRC_EN_PTRN1_O                        <= CONTROL_REG_I(C_CTRL_REG79_OFFSET+18 downto C_CTRL_REG79_OFFSET+0);

  -- Control Register 80 [0x1140]: TRG_STATE_PTRN1
  TRG_STATE_PTRN1_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG80_OFFSET/8)+3 downto C_CTRL_REG80_OFFSET/8);
  TRG_STATE_PTRN1_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG80_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN1_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG80_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN1_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG80_OFFSET+18 downto C_CTRL_REG80_OFFSET+0);
  TRG_STATE_PTRN1_O                         <= CONTROL_REG_I(C_CTRL_REG80_OFFSET+18 downto C_CTRL_REG80_OFFSET+0);

  -- Control Register 81 [0x1144]: TRG_SRC_EN_PTRN2
  TRG_SRC_EN_PTRN2_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG81_OFFSET/8)+3 downto C_CTRL_REG81_OFFSET/8);
  TRG_SRC_EN_PTRN2_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG81_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN2_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG81_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN2_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG81_OFFSET+18 downto C_CTRL_REG81_OFFSET+0);
  TRG_SRC_EN_PTRN2_O                        <= CONTROL_REG_I(C_CTRL_REG81_OFFSET+18 downto C_CTRL_REG81_OFFSET+0);

  -- Control Register 82 [0x1148]: TRG_STATE_PTRN2
  TRG_STATE_PTRN2_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG82_OFFSET/8)+3 downto C_CTRL_REG82_OFFSET/8);
  TRG_STATE_PTRN2_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG82_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN2_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG82_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN2_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG82_OFFSET+18 downto C_CTRL_REG82_OFFSET+0);
  TRG_STATE_PTRN2_O                         <= CONTROL_REG_I(C_CTRL_REG82_OFFSET+18 downto C_CTRL_REG82_OFFSET+0);

  -- Control Register 83 [0x114C]: TRG_SRC_EN_PTRN3
  TRG_SRC_EN_PTRN3_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG83_OFFSET/8)+3 downto C_CTRL_REG83_OFFSET/8);
  TRG_SRC_EN_PTRN3_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG83_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN3_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG83_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN3_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG83_OFFSET+18 downto C_CTRL_REG83_OFFSET+0);
  TRG_SRC_EN_PTRN3_O                        <= CONTROL_REG_I(C_CTRL_REG83_OFFSET+18 downto C_CTRL_REG83_OFFSET+0);

  -- Control Register 84 [0x1150]: TRG_STATE_PTRN3
  TRG_STATE_PTRN3_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG84_OFFSET/8)+3 downto C_CTRL_REG84_OFFSET/8);
  TRG_STATE_PTRN3_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG84_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN3_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG84_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN3_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG84_OFFSET+18 downto C_CTRL_REG84_OFFSET+0);
  TRG_STATE_PTRN3_O                         <= CONTROL_REG_I(C_CTRL_REG84_OFFSET+18 downto C_CTRL_REG84_OFFSET+0);

  -- Control Register 85 [0x1154]: TRG_SRC_EN_PTRN4
  TRG_SRC_EN_PTRN4_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG85_OFFSET/8)+3 downto C_CTRL_REG85_OFFSET/8);
  TRG_SRC_EN_PTRN4_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG85_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN4_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG85_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN4_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG85_OFFSET+18 downto C_CTRL_REG85_OFFSET+0);
  TRG_SRC_EN_PTRN4_O                        <= CONTROL_REG_I(C_CTRL_REG85_OFFSET+18 downto C_CTRL_REG85_OFFSET+0);

  -- Control Register 86 [0x1158]: TRG_STATE_PTRN4
  TRG_STATE_PTRN4_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG86_OFFSET/8)+3 downto C_CTRL_REG86_OFFSET/8);
  TRG_STATE_PTRN4_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG86_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN4_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG86_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN4_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG86_OFFSET+18 downto C_CTRL_REG86_OFFSET+0);
  TRG_STATE_PTRN4_O                         <= CONTROL_REG_I(C_CTRL_REG86_OFFSET+18 downto C_CTRL_REG86_OFFSET+0);

  -- Control Register 87 [0x115C]: TRG_SRC_EN_PTRN5
  TRG_SRC_EN_PTRN5_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG87_OFFSET/8)+3 downto C_CTRL_REG87_OFFSET/8);
  TRG_SRC_EN_PTRN5_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG87_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN5_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG87_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN5_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG87_OFFSET+18 downto C_CTRL_REG87_OFFSET+0);
  TRG_SRC_EN_PTRN5_O                        <= CONTROL_REG_I(C_CTRL_REG87_OFFSET+18 downto C_CTRL_REG87_OFFSET+0);

  -- Control Register 88 [0x1160]: TRG_STATE_PTRN5
  TRG_STATE_PTRN5_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG88_OFFSET/8)+3 downto C_CTRL_REG88_OFFSET/8);
  TRG_STATE_PTRN5_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG88_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN5_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG88_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN5_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG88_OFFSET+18 downto C_CTRL_REG88_OFFSET+0);
  TRG_STATE_PTRN5_O                         <= CONTROL_REG_I(C_CTRL_REG88_OFFSET+18 downto C_CTRL_REG88_OFFSET+0);

  -- Control Register 89 [0x1164]: TRG_SRC_EN_PTRN6
  TRG_SRC_EN_PTRN6_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG89_OFFSET/8)+3 downto C_CTRL_REG89_OFFSET/8);
  TRG_SRC_EN_PTRN6_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG89_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN6_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG89_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN6_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG89_OFFSET+18 downto C_CTRL_REG89_OFFSET+0);
  TRG_SRC_EN_PTRN6_O                        <= CONTROL_REG_I(C_CTRL_REG89_OFFSET+18 downto C_CTRL_REG89_OFFSET+0);

  -- Control Register 90 [0x1168]: TRG_STATE_PTRN6
  TRG_STATE_PTRN6_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG90_OFFSET/8)+3 downto C_CTRL_REG90_OFFSET/8);
  TRG_STATE_PTRN6_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG90_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN6_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG90_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN6_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG90_OFFSET+18 downto C_CTRL_REG90_OFFSET+0);
  TRG_STATE_PTRN6_O                         <= CONTROL_REG_I(C_CTRL_REG90_OFFSET+18 downto C_CTRL_REG90_OFFSET+0);

  -- Control Register 91 [0x116C]: TRG_SRC_EN_PTRN7
  TRG_SRC_EN_PTRN7_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG91_OFFSET/8)+3 downto C_CTRL_REG91_OFFSET/8);
  TRG_SRC_EN_PTRN7_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG91_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN7_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG91_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN7_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG91_OFFSET+18 downto C_CTRL_REG91_OFFSET+0);
  TRG_SRC_EN_PTRN7_O                        <= CONTROL_REG_I(C_CTRL_REG91_OFFSET+18 downto C_CTRL_REG91_OFFSET+0);

  -- Control Register 92 [0x1170]: TRG_STATE_PTRN7
  TRG_STATE_PTRN7_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG92_OFFSET/8)+3 downto C_CTRL_REG92_OFFSET/8);
  TRG_STATE_PTRN7_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG92_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN7_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG92_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN7_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG92_OFFSET+18 downto C_CTRL_REG92_OFFSET+0);
  TRG_STATE_PTRN7_O                         <= CONTROL_REG_I(C_CTRL_REG92_OFFSET+18 downto C_CTRL_REG92_OFFSET+0);

  -- Control Register 93 [0x1174]: TRG_SRC_EN_PTRN8
  TRG_SRC_EN_PTRN8_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG93_OFFSET/8)+3 downto C_CTRL_REG93_OFFSET/8);
  TRG_SRC_EN_PTRN8_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG93_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN8_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG93_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN8_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG93_OFFSET+18 downto C_CTRL_REG93_OFFSET+0);
  TRG_SRC_EN_PTRN8_O                        <= CONTROL_REG_I(C_CTRL_REG93_OFFSET+18 downto C_CTRL_REG93_OFFSET+0);

  -- Control Register 94 [0x1178]: TRG_STATE_PTRN8
  TRG_STATE_PTRN8_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG94_OFFSET/8)+3 downto C_CTRL_REG94_OFFSET/8);
  TRG_STATE_PTRN8_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG94_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN8_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG94_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN8_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG94_OFFSET+18 downto C_CTRL_REG94_OFFSET+0);
  TRG_STATE_PTRN8_O                         <= CONTROL_REG_I(C_CTRL_REG94_OFFSET+18 downto C_CTRL_REG94_OFFSET+0);

  -- Control Register 95 [0x117C]: TRG_SRC_EN_PTRN9
  TRG_SRC_EN_PTRN9_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG95_OFFSET/8)+3 downto C_CTRL_REG95_OFFSET/8);
  TRG_SRC_EN_PTRN9_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG95_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN9_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG95_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN9_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG95_OFFSET+18 downto C_CTRL_REG95_OFFSET+0);
  TRG_SRC_EN_PTRN9_O                        <= CONTROL_REG_I(C_CTRL_REG95_OFFSET+18 downto C_CTRL_REG95_OFFSET+0);

  -- Control Register 96 [0x1180]: TRG_STATE_PTRN9
  TRG_STATE_PTRN9_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG96_OFFSET/8)+3 downto C_CTRL_REG96_OFFSET/8);
  TRG_STATE_PTRN9_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG96_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN9_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG96_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN9_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG96_OFFSET+18 downto C_CTRL_REG96_OFFSET+0);
  TRG_STATE_PTRN9_O                         <= CONTROL_REG_I(C_CTRL_REG96_OFFSET+18 downto C_CTRL_REG96_OFFSET+0);

  -- Control Register 97 [0x1184]: TRG_SRC_EN_PTRN10
  TRG_SRC_EN_PTRN10_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG97_OFFSET/8)+3 downto C_CTRL_REG97_OFFSET/8);
  TRG_SRC_EN_PTRN10_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG97_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN10_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG97_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN10_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG97_OFFSET+18 downto C_CTRL_REG97_OFFSET+0);
  TRG_SRC_EN_PTRN10_O                       <= CONTROL_REG_I(C_CTRL_REG97_OFFSET+18 downto C_CTRL_REG97_OFFSET+0);

  -- Control Register 98 [0x1188]: TRG_STATE_PTRN10
  TRG_STATE_PTRN10_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG98_OFFSET/8)+3 downto C_CTRL_REG98_OFFSET/8);
  TRG_STATE_PTRN10_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG98_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN10_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG98_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN10_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG98_OFFSET+18 downto C_CTRL_REG98_OFFSET+0);
  TRG_STATE_PTRN10_O                        <= CONTROL_REG_I(C_CTRL_REG98_OFFSET+18 downto C_CTRL_REG98_OFFSET+0);

  -- Control Register 99 [0x118C]: TRG_SRC_EN_PTRN11
  TRG_SRC_EN_PTRN11_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG99_OFFSET/8)+3 downto C_CTRL_REG99_OFFSET/8);
  TRG_SRC_EN_PTRN11_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG99_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN11_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG99_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN11_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG99_OFFSET+18 downto C_CTRL_REG99_OFFSET+0);
  TRG_SRC_EN_PTRN11_O                       <= CONTROL_REG_I(C_CTRL_REG99_OFFSET+18 downto C_CTRL_REG99_OFFSET+0);

  -- Control Register 100 [0x1190]: TRG_STATE_PTRN11
  TRG_STATE_PTRN11_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG100_OFFSET/8)+3 downto C_CTRL_REG100_OFFSET/8);
  TRG_STATE_PTRN11_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG100_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN11_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG100_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN11_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG100_OFFSET+18 downto C_CTRL_REG100_OFFSET+0);
  TRG_STATE_PTRN11_O                        <= CONTROL_REG_I(C_CTRL_REG100_OFFSET+18 downto C_CTRL_REG100_OFFSET+0);

  -- Control Register 101 [0x1194]: TRG_SRC_EN_PTRN12
  TRG_SRC_EN_PTRN12_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG101_OFFSET/8)+3 downto C_CTRL_REG101_OFFSET/8);
  TRG_SRC_EN_PTRN12_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG101_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN12_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG101_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN12_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG101_OFFSET+18 downto C_CTRL_REG101_OFFSET+0);
  TRG_SRC_EN_PTRN12_O                       <= CONTROL_REG_I(C_CTRL_REG101_OFFSET+18 downto C_CTRL_REG101_OFFSET+0);

  -- Control Register 102 [0x1198]: TRG_STATE_PTRN12
  TRG_STATE_PTRN12_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG102_OFFSET/8)+3 downto C_CTRL_REG102_OFFSET/8);
  TRG_STATE_PTRN12_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG102_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN12_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG102_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN12_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG102_OFFSET+18 downto C_CTRL_REG102_OFFSET+0);
  TRG_STATE_PTRN12_O                        <= CONTROL_REG_I(C_CTRL_REG102_OFFSET+18 downto C_CTRL_REG102_OFFSET+0);

  -- Control Register 103 [0x119C]: TRG_SRC_EN_PTRN13
  TRG_SRC_EN_PTRN13_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG103_OFFSET/8)+3 downto C_CTRL_REG103_OFFSET/8);
  TRG_SRC_EN_PTRN13_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG103_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN13_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG103_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN13_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG103_OFFSET+18 downto C_CTRL_REG103_OFFSET+0);
  TRG_SRC_EN_PTRN13_O                       <= CONTROL_REG_I(C_CTRL_REG103_OFFSET+18 downto C_CTRL_REG103_OFFSET+0);

  -- Control Register 104 [0x11A0]: TRG_STATE_PTRN13
  TRG_STATE_PTRN13_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG104_OFFSET/8)+3 downto C_CTRL_REG104_OFFSET/8);
  TRG_STATE_PTRN13_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG104_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN13_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG104_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN13_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG104_OFFSET+18 downto C_CTRL_REG104_OFFSET+0);
  TRG_STATE_PTRN13_O                        <= CONTROL_REG_I(C_CTRL_REG104_OFFSET+18 downto C_CTRL_REG104_OFFSET+0);

  -- Control Register 105 [0x11A4]: TRG_SRC_EN_PTRN14
  TRG_SRC_EN_PTRN14_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG105_OFFSET/8)+3 downto C_CTRL_REG105_OFFSET/8);
  TRG_SRC_EN_PTRN14_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG105_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN14_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG105_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN14_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG105_OFFSET+18 downto C_CTRL_REG105_OFFSET+0);
  TRG_SRC_EN_PTRN14_O                       <= CONTROL_REG_I(C_CTRL_REG105_OFFSET+18 downto C_CTRL_REG105_OFFSET+0);

  -- Control Register 106 [0x11A8]: TRG_STATE_PTRN14
  TRG_STATE_PTRN14_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG106_OFFSET/8)+3 downto C_CTRL_REG106_OFFSET/8);
  TRG_STATE_PTRN14_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG106_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN14_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG106_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN14_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG106_OFFSET+18 downto C_CTRL_REG106_OFFSET+0);
  TRG_STATE_PTRN14_O                        <= CONTROL_REG_I(C_CTRL_REG106_OFFSET+18 downto C_CTRL_REG106_OFFSET+0);

  -- Control Register 107 [0x11AC]: TRG_SRC_EN_PTRN15
  TRG_SRC_EN_PTRN15_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG107_OFFSET/8)+3 downto C_CTRL_REG107_OFFSET/8);
  TRG_SRC_EN_PTRN15_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG107_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN15_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG107_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN15_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG107_OFFSET+18 downto C_CTRL_REG107_OFFSET+0);
  TRG_SRC_EN_PTRN15_O                       <= CONTROL_REG_I(C_CTRL_REG107_OFFSET+18 downto C_CTRL_REG107_OFFSET+0);

  -- Control Register 108 [0x11B0]: TRG_STATE_PTRN15
  TRG_STATE_PTRN15_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG108_OFFSET/8)+3 downto C_CTRL_REG108_OFFSET/8);
  TRG_STATE_PTRN15_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG108_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN15_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG108_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN15_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG108_OFFSET+18 downto C_CTRL_REG108_OFFSET+0);
  TRG_STATE_PTRN15_O                        <= CONTROL_REG_I(C_CTRL_REG108_OFFSET+18 downto C_CTRL_REG108_OFFSET+0);

  -- Control Register 109 [0x11B4]: TRG_SRC_EN_PTRN16
  TRG_SRC_EN_PTRN16_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG109_OFFSET/8)+3 downto C_CTRL_REG109_OFFSET/8);
  TRG_SRC_EN_PTRN16_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG109_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN16_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG109_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN16_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG109_OFFSET+18 downto C_CTRL_REG109_OFFSET+0);
  TRG_SRC_EN_PTRN16_O                       <= CONTROL_REG_I(C_CTRL_REG109_OFFSET+18 downto C_CTRL_REG109_OFFSET+0);

  -- Control Register 110 [0x11B8]: TRG_STATE_PTRN16
  TRG_STATE_PTRN16_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG110_OFFSET/8)+3 downto C_CTRL_REG110_OFFSET/8);
  TRG_STATE_PTRN16_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG110_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN16_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG110_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN16_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG110_OFFSET+18 downto C_CTRL_REG110_OFFSET+0);
  TRG_STATE_PTRN16_O                        <= CONTROL_REG_I(C_CTRL_REG110_OFFSET+18 downto C_CTRL_REG110_OFFSET+0);

  -- Control Register 111 [0x11BC]: TRG_SRC_EN_PTRN17
  TRG_SRC_EN_PTRN17_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG111_OFFSET/8)+3 downto C_CTRL_REG111_OFFSET/8);
  TRG_SRC_EN_PTRN17_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG111_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN17_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG111_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN17_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG111_OFFSET+18 downto C_CTRL_REG111_OFFSET+0);
  TRG_SRC_EN_PTRN17_O                       <= CONTROL_REG_I(C_CTRL_REG111_OFFSET+18 downto C_CTRL_REG111_OFFSET+0);

  -- Control Register 112 [0x11C0]: TRG_STATE_PTRN17
  TRG_STATE_PTRN17_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG112_OFFSET/8)+3 downto C_CTRL_REG112_OFFSET/8);
  TRG_STATE_PTRN17_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG112_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN17_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG112_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN17_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG112_OFFSET+18 downto C_CTRL_REG112_OFFSET+0);
  TRG_STATE_PTRN17_O                        <= CONTROL_REG_I(C_CTRL_REG112_OFFSET+18 downto C_CTRL_REG112_OFFSET+0);

  -- Control Register 113 [0x11C4]: TRG_SRC_EN_PTRN18
  TRG_SRC_EN_PTRN18_BYTE_TRIG_O             <= CTRL_BYTE_TRIG_I((C_CTRL_REG113_OFFSET/8)+3 downto C_CTRL_REG113_OFFSET/8);
  TRG_SRC_EN_PTRN18_REG_READ_O              <= CTRL_REG_READ_I(C_CTRL_REG113_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN18_REG_WRITE_O             <= CTRL_REG_WRITE_I(C_CTRL_REG113_OFFSET/CGN_BITS_PER_REG);
  TRG_SRC_EN_PTRN18_BIT_TRIG_O              <= CTRL_BIT_TRIG_I(C_CTRL_REG113_OFFSET+18 downto C_CTRL_REG113_OFFSET+0);
  TRG_SRC_EN_PTRN18_O                       <= CONTROL_REG_I(C_CTRL_REG113_OFFSET+18 downto C_CTRL_REG113_OFFSET+0);

  -- Control Register 114 [0x11C8]: TRG_STATE_PTRN18
  TRG_STATE_PTRN18_BYTE_TRIG_O              <= CTRL_BYTE_TRIG_I((C_CTRL_REG114_OFFSET/8)+3 downto C_CTRL_REG114_OFFSET/8);
  TRG_STATE_PTRN18_REG_READ_O               <= CTRL_REG_READ_I(C_CTRL_REG114_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN18_REG_WRITE_O              <= CTRL_REG_WRITE_I(C_CTRL_REG114_OFFSET/CGN_BITS_PER_REG);
  TRG_STATE_PTRN18_BIT_TRIG_O               <= CTRL_BIT_TRIG_I(C_CTRL_REG114_OFFSET+18 downto C_CTRL_REG114_OFFSET+0);
  TRG_STATE_PTRN18_O                        <= CONTROL_REG_I(C_CTRL_REG114_OFFSET+18 downto C_CTRL_REG114_OFFSET+0);

  -- Control Register 115 [0x11CC]: ADV_TRG_CTRL
  ADV_TRG_CTRL_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG115_OFFSET/8)+3 downto C_CTRL_REG115_OFFSET/8);
  ADV_TRG_CTRL_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG115_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CTRL_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG115_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CTRL_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG115_OFFSET+31 downto C_CTRL_REG115_OFFSET+0);
  ADV_TRG_CTRL_O                            <= CONTROL_REG_I(C_CTRL_REG115_OFFSET+31 downto C_CTRL_REG115_OFFSET+0);

  -- Control Register 116 [0x11D0]: ADV_TRG_CH_CAL0
  ADV_TRG_CH_CAL0_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG116_OFFSET/8)+3 downto C_CTRL_REG116_OFFSET/8);
  ADV_TRG_CH_CAL0_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG116_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL0_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG116_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL0_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG116_OFFSET+31 downto C_CTRL_REG116_OFFSET+0);
  ADV_TRG_CH_CAL0_O                         <= CONTROL_REG_I(C_CTRL_REG116_OFFSET+31 downto C_CTRL_REG116_OFFSET+0);

  -- Control Register 117 [0x11D4]: ADV_TRG_CH_CAL1
  ADV_TRG_CH_CAL1_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG117_OFFSET/8)+3 downto C_CTRL_REG117_OFFSET/8);
  ADV_TRG_CH_CAL1_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG117_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL1_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG117_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL1_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG117_OFFSET+31 downto C_CTRL_REG117_OFFSET+0);
  ADV_TRG_CH_CAL1_O                         <= CONTROL_REG_I(C_CTRL_REG117_OFFSET+31 downto C_CTRL_REG117_OFFSET+0);

  -- Control Register 118 [0x11D8]: ADV_TRG_CH_CAL2
  ADV_TRG_CH_CAL2_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG118_OFFSET/8)+3 downto C_CTRL_REG118_OFFSET/8);
  ADV_TRG_CH_CAL2_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG118_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL2_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG118_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL2_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG118_OFFSET+31 downto C_CTRL_REG118_OFFSET+0);
  ADV_TRG_CH_CAL2_O                         <= CONTROL_REG_I(C_CTRL_REG118_OFFSET+31 downto C_CTRL_REG118_OFFSET+0);

  -- Control Register 119 [0x11DC]: ADV_TRG_CH_CAL3
  ADV_TRG_CH_CAL3_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG119_OFFSET/8)+3 downto C_CTRL_REG119_OFFSET/8);
  ADV_TRG_CH_CAL3_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG119_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL3_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG119_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CH_CAL3_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG119_OFFSET+31 downto C_CTRL_REG119_OFFSET+0);
  ADV_TRG_CH_CAL3_O                         <= CONTROL_REG_I(C_CTRL_REG119_OFFSET+31 downto C_CTRL_REG119_OFFSET+0);

  -- Control Register 120 [0x11E0]: ADV_TRG_PED_CFG
  ADV_TRG_PED_CFG_BYTE_TRIG_O               <= CTRL_BYTE_TRIG_I((C_CTRL_REG120_OFFSET/8)+3 downto C_CTRL_REG120_OFFSET/8);
  ADV_TRG_PED_CFG_REG_READ_O                <= CTRL_REG_READ_I(C_CTRL_REG120_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_PED_CFG_REG_WRITE_O               <= CTRL_REG_WRITE_I(C_CTRL_REG120_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_PED_CFG_BIT_TRIG_O                <= CTRL_BIT_TRIG_I(C_CTRL_REG120_OFFSET+31 downto C_CTRL_REG120_OFFSET+0);
  ADV_TRG_PED_CFG_O                         <= CONTROL_REG_I(C_CTRL_REG120_OFFSET+31 downto C_CTRL_REG120_OFFSET+0);

  -- Control Register 121 [0x11E4]: ADV_TRG_THR0
  ADV_TRG_THR0_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG121_OFFSET/8)+3 downto C_CTRL_REG121_OFFSET/8);
  ADV_TRG_THR0_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG121_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_THR0_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG121_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_THR0_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG121_OFFSET+31 downto C_CTRL_REG121_OFFSET+0);
  ADV_TRG_THR0_O                            <= CONTROL_REG_I(C_CTRL_REG121_OFFSET+31 downto C_CTRL_REG121_OFFSET+0);

  -- Control Register 122 [0x11E8]: ADV_TRG_THR1
  ADV_TRG_THR1_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG122_OFFSET/8)+3 downto C_CTRL_REG122_OFFSET/8);
  ADV_TRG_THR1_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG122_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_THR1_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG122_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_THR1_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG122_OFFSET+31 downto C_CTRL_REG122_OFFSET+0);
  ADV_TRG_THR1_O                            <= CONTROL_REG_I(C_CTRL_REG122_OFFSET+31 downto C_CTRL_REG122_OFFSET+0);

  -- Control Register 123 [0x11EC]: ADV_TRG_THR2
  ADV_TRG_THR2_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG123_OFFSET/8)+3 downto C_CTRL_REG123_OFFSET/8);
  ADV_TRG_THR2_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG123_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_THR2_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG123_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_THR2_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG123_OFFSET+31 downto C_CTRL_REG123_OFFSET+0);
  ADV_TRG_THR2_O                            <= CONTROL_REG_I(C_CTRL_REG123_OFFSET+31 downto C_CTRL_REG123_OFFSET+0);

  -- Control Register 124 [0x11F0]: ADV_TRG_TX_CHK_WORD0
  ADV_TRG_TX_CHK_WORD0_BYTE_TRIG_O          <= CTRL_BYTE_TRIG_I((C_CTRL_REG124_OFFSET/8)+3 downto C_CTRL_REG124_OFFSET/8);
  ADV_TRG_TX_CHK_WORD0_REG_READ_O           <= CTRL_REG_READ_I(C_CTRL_REG124_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TX_CHK_WORD0_REG_WRITE_O          <= CTRL_REG_WRITE_I(C_CTRL_REG124_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TX_CHK_WORD0_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG124_OFFSET+31 downto C_CTRL_REG124_OFFSET+0);
  ADV_TRG_TX_CHK_WORD0_O                    <= CONTROL_REG_I(C_CTRL_REG124_OFFSET+31 downto C_CTRL_REG124_OFFSET+0);

  -- Control Register 125 [0x11F4]: ADV_TRG_TX_CHK_WORD1
  ADV_TRG_TX_CHK_WORD1_BYTE_TRIG_O          <= CTRL_BYTE_TRIG_I((C_CTRL_REG125_OFFSET/8)+3 downto C_CTRL_REG125_OFFSET/8);
  ADV_TRG_TX_CHK_WORD1_REG_READ_O           <= CTRL_REG_READ_I(C_CTRL_REG125_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TX_CHK_WORD1_REG_WRITE_O          <= CTRL_REG_WRITE_I(C_CTRL_REG125_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TX_CHK_WORD1_BIT_TRIG_O           <= CTRL_BIT_TRIG_I(C_CTRL_REG125_OFFSET+31 downto C_CTRL_REG125_OFFSET+0);
  ADV_TRG_TX_CHK_WORD1_O                    <= CONTROL_REG_I(C_CTRL_REG125_OFFSET+31 downto C_CTRL_REG125_OFFSET+0);

  -- Control Register 126 [0x11F8]: ADV_TRG_TDC_CH_MASK
  ADV_TRG_TDC_CH_MASK_BYTE_TRIG_O           <= CTRL_BYTE_TRIG_I((C_CTRL_REG126_OFFSET/8)+3 downto C_CTRL_REG126_OFFSET/8);
  ADV_TRG_TDC_CH_MASK_REG_READ_O            <= CTRL_REG_READ_I(C_CTRL_REG126_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TDC_CH_MASK_REG_WRITE_O           <= CTRL_REG_WRITE_I(C_CTRL_REG126_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TDC_CH_MASK_BIT_TRIG_O            <= CTRL_BIT_TRIG_I(C_CTRL_REG126_OFFSET+31 downto C_CTRL_REG126_OFFSET+0);
  ADV_TRG_TDC_CH_MASK_O                     <= CONTROL_REG_I(C_CTRL_REG126_OFFSET+31 downto C_CTRL_REG126_OFFSET+0);

  -- Control Register 127 [0x11FC]: ADV_TRG_CFG12
  ADV_TRG_CFG12_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG127_OFFSET/8)+3 downto C_CTRL_REG127_OFFSET/8);
  ADV_TRG_CFG12_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG127_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG12_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG127_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_12_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG127_OFFSET+31 downto C_CTRL_REG127_OFFSET+0);
  ADV_TRG_CFG_12_O                          <= CONTROL_REG_I(C_CTRL_REG127_OFFSET+31 downto C_CTRL_REG127_OFFSET+0);

  -- Control Register 128 [0x1200]: ADV_TRG_CFG13
  ADV_TRG_CFG13_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG128_OFFSET/8)+3 downto C_CTRL_REG128_OFFSET/8);
  ADV_TRG_CFG13_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG128_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG13_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG128_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_13_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG128_OFFSET+31 downto C_CTRL_REG128_OFFSET+0);
  ADV_TRG_CFG_13_O                          <= CONTROL_REG_I(C_CTRL_REG128_OFFSET+31 downto C_CTRL_REG128_OFFSET+0);

  -- Control Register 129 [0x1204]: ADV_TRG_CFG14
  ADV_TRG_CFG14_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG129_OFFSET/8)+3 downto C_CTRL_REG129_OFFSET/8);
  ADV_TRG_CFG14_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG129_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG14_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG129_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_14_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG129_OFFSET+31 downto C_CTRL_REG129_OFFSET+0);
  ADV_TRG_CFG_14_O                          <= CONTROL_REG_I(C_CTRL_REG129_OFFSET+31 downto C_CTRL_REG129_OFFSET+0);

  -- Control Register 130 [0x1208]: ADV_TRG_CFG15
  ADV_TRG_CFG15_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG130_OFFSET/8)+3 downto C_CTRL_REG130_OFFSET/8);
  ADV_TRG_CFG15_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG130_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG15_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG130_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_15_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG130_OFFSET+31 downto C_CTRL_REG130_OFFSET+0);
  ADV_TRG_CFG_15_O                          <= CONTROL_REG_I(C_CTRL_REG130_OFFSET+31 downto C_CTRL_REG130_OFFSET+0);

  -- Control Register 131 [0x120C]: ADV_TRG_CFG16
  ADV_TRG_CFG16_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG131_OFFSET/8)+3 downto C_CTRL_REG131_OFFSET/8);
  ADV_TRG_CFG16_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG131_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG16_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG131_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_16_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG131_OFFSET+31 downto C_CTRL_REG131_OFFSET+0);
  ADV_TRG_CFG_16_O                          <= CONTROL_REG_I(C_CTRL_REG131_OFFSET+31 downto C_CTRL_REG131_OFFSET+0);

  -- Control Register 132 [0x1210]: ADV_TRG_CFG17
  ADV_TRG_CFG17_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG132_OFFSET/8)+3 downto C_CTRL_REG132_OFFSET/8);
  ADV_TRG_CFG17_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG132_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG17_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG132_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_17_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG132_OFFSET+31 downto C_CTRL_REG132_OFFSET+0);
  ADV_TRG_CFG_17_O                          <= CONTROL_REG_I(C_CTRL_REG132_OFFSET+31 downto C_CTRL_REG132_OFFSET+0);

  -- Control Register 133 [0x1214]: ADV_TRG_CFG18
  ADV_TRG_CFG18_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG133_OFFSET/8)+3 downto C_CTRL_REG133_OFFSET/8);
  ADV_TRG_CFG18_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG133_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG18_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG133_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_18_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG133_OFFSET+31 downto C_CTRL_REG133_OFFSET+0);
  ADV_TRG_CFG_18_O                          <= CONTROL_REG_I(C_CTRL_REG133_OFFSET+31 downto C_CTRL_REG133_OFFSET+0);

  -- Control Register 134 [0x1218]: ADV_TRG_CFG19
  ADV_TRG_CFG19_BYTE_TRIG_O                 <= CTRL_BYTE_TRIG_I((C_CTRL_REG134_OFFSET/8)+3 downto C_CTRL_REG134_OFFSET/8);
  ADV_TRG_CFG19_REG_READ_O                  <= CTRL_REG_READ_I(C_CTRL_REG134_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG19_REG_WRITE_O                 <= CTRL_REG_WRITE_I(C_CTRL_REG134_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_CFG_19_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG134_OFFSET+31 downto C_CTRL_REG134_OFFSET+0);
  ADV_TRG_CFG_19_O                          <= CONTROL_REG_I(C_CTRL_REG134_OFFSET+31 downto C_CTRL_REG134_OFFSET+0);

  -- Control Register 135 [0x121C]: SET_TIME_LSB
  SET_TIME_LSB_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG135_OFFSET/8)+3 downto C_CTRL_REG135_OFFSET/8);
  SET_TIME_LSB_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG135_OFFSET/CGN_BITS_PER_REG);
  SET_TIME_LSB_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG135_OFFSET/CGN_BITS_PER_REG);
  SET_TIME_LSB_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG135_OFFSET+31 downto C_CTRL_REG135_OFFSET+0);
  SET_TIME_LSB_O                            <= CONTROL_REG_I(C_CTRL_REG135_OFFSET+31 downto C_CTRL_REG135_OFFSET+0);

  -- Control Register 136 [0x1220]: SET_TIME_MSB
  SET_TIME_MSB_BYTE_TRIG_O                  <= CTRL_BYTE_TRIG_I((C_CTRL_REG136_OFFSET/8)+3 downto C_CTRL_REG136_OFFSET/8);
  SET_TIME_MSB_REG_READ_O                   <= CTRL_REG_READ_I(C_CTRL_REG136_OFFSET/CGN_BITS_PER_REG);
  SET_TIME_MSB_REG_WRITE_O                  <= CTRL_REG_WRITE_I(C_CTRL_REG136_OFFSET/CGN_BITS_PER_REG);
  SET_TIME_MSB_BIT_TRIG_O                   <= CTRL_BIT_TRIG_I(C_CTRL_REG136_OFFSET+31 downto C_CTRL_REG136_OFFSET+0);
  SET_TIME_MSB_O                            <= CONTROL_REG_I(C_CTRL_REG136_OFFSET+31 downto C_CTRL_REG136_OFFSET+0);

  -- Control Register 137 [0x1224]: DBG_SIG_SEL
  DBG_SIG_SEL_BYTE_TRIG_O                   <= CTRL_BYTE_TRIG_I((C_CTRL_REG137_OFFSET/8)+3 downto C_CTRL_REG137_OFFSET/8);
  DBG_SIG_SEL_REG_READ_O                    <= CTRL_REG_READ_I(C_CTRL_REG137_OFFSET/CGN_BITS_PER_REG);
  DBG_SIG_SEL_REG_WRITE_O                   <= CTRL_REG_WRITE_I(C_CTRL_REG137_OFFSET/CGN_BITS_PER_REG);
  MCX_TX_SIG_SEL_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG137_OFFSET+19 downto C_CTRL_REG137_OFFSET+16);
  MCX_TX_SIG_SEL_O                          <= CONTROL_REG_I(C_CTRL_REG137_OFFSET+19 downto C_CTRL_REG137_OFFSET+16);
  MCX_RX_SIG_SEL_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG137_OFFSET+3 downto C_CTRL_REG137_OFFSET+0);
  MCX_RX_SIG_SEL_O                          <= CONTROL_REG_I(C_CTRL_REG137_OFFSET+3 downto C_CTRL_REG137_OFFSET+0);

  -- Control Register 138 [0x1228]: CRC32_REG_BANK
  CRC32_REG_BANK_BYTE_TRIG_O                <= CTRL_BYTE_TRIG_I((C_CTRL_REG138_OFFSET/8)+3 downto C_CTRL_REG138_OFFSET/8);
  CRC32_REG_BANK_REG_READ_O                 <= CTRL_REG_READ_I(C_CTRL_REG138_OFFSET/CGN_BITS_PER_REG);
  CRC32_REG_BANK_REG_WRITE_O                <= CTRL_REG_WRITE_I(C_CTRL_REG138_OFFSET/CGN_BITS_PER_REG);
  CRC32_REG_BANK_BIT_TRIG_O                 <= CTRL_BIT_TRIG_I(C_CTRL_REG138_OFFSET+31 downto C_CTRL_REG138_OFFSET+0);
  CRC32_REG_BANK_O                          <= CONTROL_REG_I(C_CTRL_REG138_OFFSET+31 downto C_CTRL_REG138_OFFSET+0);

  -- Status Register 0 [0x0000]: HW_VER
  HW_VER_BYTE_TRIG_O                                                    <= STAT_BYTE_TRIG_I((C_STAT_REG0_OFFSET/8)+3 downto C_STAT_REG0_OFFSET/8);
  HW_VER_REG_READ_O                                                     <= STAT_REG_READ_I(C_STAT_REG0_OFFSET/CGN_BITS_PER_REG);
  HW_VER_REG_WRITE_O                                                    <= STAT_REG_WRITE_I(C_STAT_REG0_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG0_OFFSET+31 downto C_STAT_REG0_OFFSET+24)        <= x"AC";
  BOARD_MAGIC_O                                                         <= x"AC";
  status_reg(C_STAT_REG0_OFFSET+23 downto C_STAT_REG0_OFFSET+16)        <= x"01";
  VENDOR_ID_O                                                           <= x"01";
  status_reg(C_STAT_REG0_OFFSET+15 downto C_STAT_REG0_OFFSET+8)         <= x"02";
  BOARD_TYPE_O                                                          <= x"02";
  status_reg(C_STAT_REG0_OFFSET+7 downto C_STAT_REG0_OFFSET+2)          <= "000110";
  BOARD_REVISION_O                                                      <= "000110";
  status_reg(C_STAT_REG0_OFFSET+1 downto C_STAT_REG0_OFFSET+0)          <= "11";
  BOARD_VARIANT_O                                                       <= "11";

  -- Status Register 1 [0x0004]: REG_LAYOUT_VER
  REG_LAYOUT_VER_BYTE_TRIG_O                                            <= STAT_BYTE_TRIG_I((C_STAT_REG1_OFFSET/8)+3 downto C_STAT_REG1_OFFSET/8);
  REG_LAYOUT_VER_REG_READ_O                                             <= STAT_REG_READ_I(C_STAT_REG1_OFFSET/CGN_BITS_PER_REG);
  REG_LAYOUT_VER_REG_WRITE_O                                            <= STAT_REG_WRITE_I(C_STAT_REG1_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG1_OFFSET+31 downto C_STAT_REG1_OFFSET+16)        <= x"0009";
  REG_LAYOUT_COMP_LEVEL_O                                               <= x"0009";
  status_reg(C_STAT_REG1_OFFSET+15 downto C_STAT_REG1_OFFSET+0)         <= x"0009";
  REG_LAYOUT_VERSION_O                                                  <= x"0009";

  -- Status Register 2 [0x0008]: FW_BUILD_DATE
  FW_BUILD_DATE_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG2_OFFSET/8)+3 downto C_STAT_REG2_OFFSET/8);
  FW_BUILD_DATE_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG2_OFFSET/CGN_BITS_PER_REG);
  FW_BUILD_DATE_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG2_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG2_OFFSET+31 downto C_STAT_REG2_OFFSET+16)        <= FW_BUILD_YEAR_I;
  FW_BUILD_YEAR_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG2_OFFSET+31 downto C_STAT_REG2_OFFSET+16);
  FW_BUILD_YEAR_O                                                       <= STATUS_REG_I(C_STAT_REG2_OFFSET+31 downto C_STAT_REG2_OFFSET+16);
  status_reg(C_STAT_REG2_OFFSET+15 downto C_STAT_REG2_OFFSET+8)         <= FW_BUILD_MONTH_I;
  FW_BUILD_MONTH_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG2_OFFSET+15 downto C_STAT_REG2_OFFSET+8);
  FW_BUILD_MONTH_O                                                      <= STATUS_REG_I(C_STAT_REG2_OFFSET+15 downto C_STAT_REG2_OFFSET+8);
  status_reg(C_STAT_REG2_OFFSET+7 downto C_STAT_REG2_OFFSET+0)          <= FW_BUILD_DAY_I;
  FW_BUILD_DAY_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG2_OFFSET+7 downto C_STAT_REG2_OFFSET+0);
  FW_BUILD_DAY_O                                                        <= STATUS_REG_I(C_STAT_REG2_OFFSET+7 downto C_STAT_REG2_OFFSET+0);

  -- Status Register 3 [0x000C]: FW_BUILD_TIME
  FW_BUILD_TIME_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG3_OFFSET/8)+3 downto C_STAT_REG3_OFFSET/8);
  FW_BUILD_TIME_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG3_OFFSET/CGN_BITS_PER_REG);
  FW_BUILD_TIME_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG3_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG3_OFFSET+31 downto C_STAT_REG3_OFFSET+24)        <= x"05";
  FW_COMPAT_LEVEL_O                                                     <= x"05";
  status_reg(C_STAT_REG3_OFFSET+23 downto C_STAT_REG3_OFFSET+16)        <= FW_BUILD_HOUR_I;
  FW_BUILD_HOUR_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG3_OFFSET+23 downto C_STAT_REG3_OFFSET+16);
  FW_BUILD_HOUR_O                                                       <= STATUS_REG_I(C_STAT_REG3_OFFSET+23 downto C_STAT_REG3_OFFSET+16);
  status_reg(C_STAT_REG3_OFFSET+15 downto C_STAT_REG3_OFFSET+8)         <= FW_BUILD_MINUTE_I;
  FW_BUILD_MINUTE_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG3_OFFSET+15 downto C_STAT_REG3_OFFSET+8);
  FW_BUILD_MINUTE_O                                                     <= STATUS_REG_I(C_STAT_REG3_OFFSET+15 downto C_STAT_REG3_OFFSET+8);
  status_reg(C_STAT_REG3_OFFSET+7 downto C_STAT_REG3_OFFSET+0)          <= FW_BUILD_SECOND_I;
  FW_BUILD_SECOND_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG3_OFFSET+7 downto C_STAT_REG3_OFFSET+0);
  FW_BUILD_SECOND_O                                                     <= STATUS_REG_I(C_STAT_REG3_OFFSET+7 downto C_STAT_REG3_OFFSET+0);

  -- Status Register 4 [0x0010]: SW_BUILD_DATE
  SW_BUILD_DATE_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG4_OFFSET/8)+3 downto C_STAT_REG4_OFFSET/8);
  SW_BUILD_DATE_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG4_OFFSET/CGN_BITS_PER_REG);
  SW_BUILD_DATE_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG4_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG4_OFFSET+31 downto C_STAT_REG4_OFFSET+16)        <= SW_BUILD_YEAR_I;
  SW_BUILD_YEAR_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG4_OFFSET+31 downto C_STAT_REG4_OFFSET+16);
  SW_BUILD_YEAR_O                                                       <= STATUS_REG_I(C_STAT_REG4_OFFSET+31 downto C_STAT_REG4_OFFSET+16);
  status_reg(C_STAT_REG4_OFFSET+15 downto C_STAT_REG4_OFFSET+8)         <= SW_BUILD_MONTH_I;
  SW_BUILD_MONTH_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG4_OFFSET+15 downto C_STAT_REG4_OFFSET+8);
  SW_BUILD_MONTH_O                                                      <= STATUS_REG_I(C_STAT_REG4_OFFSET+15 downto C_STAT_REG4_OFFSET+8);
  status_reg(C_STAT_REG4_OFFSET+7 downto C_STAT_REG4_OFFSET+0)          <= SW_BUILD_DAY_I;
  SW_BUILD_DAY_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG4_OFFSET+7 downto C_STAT_REG4_OFFSET+0);
  SW_BUILD_DAY_O                                                        <= STATUS_REG_I(C_STAT_REG4_OFFSET+7 downto C_STAT_REG4_OFFSET+0);

  -- Status Register 5 [0x0014]: SW_BUILD_TIME
  SW_BUILD_TIME_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG5_OFFSET/8)+3 downto C_STAT_REG5_OFFSET/8);
  SW_BUILD_TIME_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG5_OFFSET/CGN_BITS_PER_REG);
  SW_BUILD_TIME_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG5_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG5_OFFSET+31 downto C_STAT_REG5_OFFSET+24)        <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG5_OFFSET+23 downto C_STAT_REG5_OFFSET+16)        <= SW_BUILD_HOUR_I;
  SW_BUILD_HOUR_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG5_OFFSET+23 downto C_STAT_REG5_OFFSET+16);
  SW_BUILD_HOUR_O                                                       <= STATUS_REG_I(C_STAT_REG5_OFFSET+23 downto C_STAT_REG5_OFFSET+16);
  status_reg(C_STAT_REG5_OFFSET+15 downto C_STAT_REG5_OFFSET+8)         <= SW_BUILD_MINUTE_I;
  SW_BUILD_MINUTE_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG5_OFFSET+15 downto C_STAT_REG5_OFFSET+8);
  SW_BUILD_MINUTE_O                                                     <= STATUS_REG_I(C_STAT_REG5_OFFSET+15 downto C_STAT_REG5_OFFSET+8);
  status_reg(C_STAT_REG5_OFFSET+7 downto C_STAT_REG5_OFFSET+0)          <= SW_BUILD_SECOND_I;
  SW_BUILD_SECOND_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG5_OFFSET+7 downto C_STAT_REG5_OFFSET+0);
  SW_BUILD_SECOND_O                                                     <= STATUS_REG_I(C_STAT_REG5_OFFSET+7 downto C_STAT_REG5_OFFSET+0);

  -- Status Register 6 [0x0018]: FW_GIT_HASH_TAG
  FW_GIT_HASH_TAG_BYTE_TRIG_O                                           <= STAT_BYTE_TRIG_I((C_STAT_REG6_OFFSET/8)+3 downto C_STAT_REG6_OFFSET/8);
  FW_GIT_HASH_TAG_REG_READ_O                                            <= STAT_REG_READ_I(C_STAT_REG6_OFFSET/CGN_BITS_PER_REG);
  FW_GIT_HASH_TAG_REG_WRITE_O                                           <= STAT_REG_WRITE_I(C_STAT_REG6_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG6_OFFSET+31 downto C_STAT_REG6_OFFSET+0)         <= FW_GIT_HASH_TAG_I;
  FW_GIT_HASH_TAG_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG6_OFFSET+31 downto C_STAT_REG6_OFFSET+0);
  FW_GIT_HASH_TAG_O                                                     <= STATUS_REG_I(C_STAT_REG6_OFFSET+31 downto C_STAT_REG6_OFFSET+0);

  -- Status Register 7 [0x001C]: SW_GIT_HASH_TAG
  SW_GIT_HASH_TAG_BYTE_TRIG_O                                           <= STAT_BYTE_TRIG_I((C_STAT_REG7_OFFSET/8)+3 downto C_STAT_REG7_OFFSET/8);
  SW_GIT_HASH_TAG_REG_READ_O                                            <= STAT_REG_READ_I(C_STAT_REG7_OFFSET/CGN_BITS_PER_REG);
  SW_GIT_HASH_TAG_REG_WRITE_O                                           <= STAT_REG_WRITE_I(C_STAT_REG7_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG7_OFFSET+31 downto C_STAT_REG7_OFFSET+0)         <= SW_GIT_HASH_TAG_I;
  SW_GIT_HASH_TAG_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG7_OFFSET+31 downto C_STAT_REG7_OFFSET+0);
  SW_GIT_HASH_TAG_O                                                     <= STATUS_REG_I(C_STAT_REG7_OFFSET+31 downto C_STAT_REG7_OFFSET+0);

  -- Status Register 8 [0x0020]: PROT_VER
  PROT_VER_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG8_OFFSET/8)+3 downto C_STAT_REG8_OFFSET/8);
  PROT_VER_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG8_OFFSET/CGN_BITS_PER_REG);
  PROT_VER_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG8_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG8_OFFSET+31 downto C_STAT_REG8_OFFSET+8)         <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG8_OFFSET+7 downto C_STAT_REG8_OFFSET+0)          <= PROTOCOL_VERSION_I;
  PROTOCOL_VERSION_BIT_TRIG_O                                           <= STAT_BIT_TRIG_I(C_STAT_REG8_OFFSET+7 downto C_STAT_REG8_OFFSET+0);
  PROTOCOL_VERSION_O                                                    <= STATUS_REG_I(C_STAT_REG8_OFFSET+7 downto C_STAT_REG8_OFFSET+0);

  -- Status Register 9 [0x0024]: SN
  SN_BYTE_TRIG_O                                                        <= STAT_BYTE_TRIG_I((C_STAT_REG9_OFFSET/8)+3 downto C_STAT_REG9_OFFSET/8);
  SN_REG_READ_O                                                         <= STAT_REG_READ_I(C_STAT_REG9_OFFSET/CGN_BITS_PER_REG);
  SN_REG_WRITE_O                                                        <= STAT_REG_WRITE_I(C_STAT_REG9_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG9_OFFSET+31 downto C_STAT_REG9_OFFSET+16)        <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG9_OFFSET+15 downto C_STAT_REG9_OFFSET+0)         <= SERIAL_NUMBER_I;
  SERIAL_NUMBER_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG9_OFFSET+15 downto C_STAT_REG9_OFFSET+0);
  SERIAL_NUMBER_O                                                       <= STATUS_REG_I(C_STAT_REG9_OFFSET+15 downto C_STAT_REG9_OFFSET+0);

  -- Status Register 10 [0x0028]: STATUS
  STATUS_BYTE_TRIG_O                                                    <= STAT_BYTE_TRIG_I((C_STAT_REG10_OFFSET/8)+3 downto C_STAT_REG10_OFFSET/8);
  STATUS_REG_READ_O                                                     <= STAT_REG_READ_I(C_STAT_REG10_OFFSET/CGN_BITS_PER_REG);
  STATUS_REG_WRITE_O                                                    <= STAT_REG_WRITE_I(C_STAT_REG10_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG10_OFFSET+31 downto C_STAT_REG10_OFFSET+16)      <= TEMPERATURE_I;
  TEMPERATURE_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+31 downto C_STAT_REG10_OFFSET+16);
  TEMPERATURE_O                                                         <= STATUS_REG_I(C_STAT_REG10_OFFSET+31 downto C_STAT_REG10_OFFSET+16);
  status_reg(C_STAT_REG10_OFFSET+15)                                    <= OVERTEMP_I;
  OVERTEMP_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+15);
  OVERTEMP_O                                                            <= STATUS_REG_I(C_STAT_REG10_OFFSET+15);
  status_reg(C_STAT_REG10_OFFSET+14 downto C_STAT_REG10_OFFSET+13)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG10_OFFSET+12)                                    <= DAQ_CLK_DEF_PHASE_OK_I;
  DAQ_CLK_DEF_PHASE_OK_BIT_TRIG_O                                       <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+12);
  DAQ_CLK_DEF_PHASE_OK_O                                                <= STATUS_REG_I(C_STAT_REG10_OFFSET+12);
  status_reg(C_STAT_REG10_OFFSET+11)                                    <= DAQ_CLK_DEF_PHASE_CHKD_I;
  DAQ_CLK_DEF_PHASE_CHKD_BIT_TRIG_O                                     <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+11);
  DAQ_CLK_DEF_PHASE_CHKD_O                                              <= STATUS_REG_I(C_STAT_REG10_OFFSET+11);
  status_reg(C_STAT_REG10_OFFSET+10)                                    <= '0'; -- reserved
  status_reg(C_STAT_REG10_OFFSET+9)                                     <= DRS_CONFIG_DONE_I;
  DRS_CONFIG_DONE_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+9);
  DRS_CONFIG_DONE_O                                                     <= STATUS_REG_I(C_STAT_REG10_OFFSET+9);
  status_reg(C_STAT_REG10_OFFSET+8)                                     <= BOARD_SEL_I;
  BOARD_SEL_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+8);
  BOARD_SEL_O                                                           <= STATUS_REG_I(C_STAT_REG10_OFFSET+8);
  status_reg(C_STAT_REG10_OFFSET+7)                                     <= SERIAL_BUSY_I;
  SERIAL_BUSY_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+7);
  SERIAL_BUSY_O                                                         <= STATUS_REG_I(C_STAT_REG10_OFFSET+7);
  status_reg(C_STAT_REG10_OFFSET+6)                                     <= PACKAGER_BUSY_I;
  PACKAGER_BUSY_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+6);
  PACKAGER_BUSY_O                                                       <= STATUS_REG_I(C_STAT_REG10_OFFSET+6);
  status_reg(C_STAT_REG10_OFFSET+5)                                     <= DRS_CTRL_BUSY_I;
  DRS_CTRL_BUSY_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+5);
  DRS_CTRL_BUSY_O                                                       <= STATUS_REG_I(C_STAT_REG10_OFFSET+5);
  status_reg(C_STAT_REG10_OFFSET+4)                                     <= SYS_BUSY_I;
  SYS_BUSY_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+4);
  SYS_BUSY_O                                                            <= STATUS_REG_I(C_STAT_REG10_OFFSET+4);
  status_reg(C_STAT_REG10_OFFSET+3 downto C_STAT_REG10_OFFSET+2)        <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG10_OFFSET+1)                                     <= HV_BOARD_PLUGGED_I;
  HV_BOARD_PLUGGED_BIT_TRIG_O                                           <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+1);
  HV_BOARD_PLUGGED_O                                                    <= STATUS_REG_I(C_STAT_REG10_OFFSET+1);
  status_reg(C_STAT_REG10_OFFSET+0)                                     <= BACKPLANE_PLUGGED_I;
  BACKPLANE_PLUGGED_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG10_OFFSET+0);
  BACKPLANE_PLUGGED_O                                                   <= STATUS_REG_I(C_STAT_REG10_OFFSET+0);

  -- Status Register 11 [0x002C]: PLL_LOCK
  PLL_LOCK_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG11_OFFSET/8)+3 downto C_STAT_REG11_OFFSET/8);
  PLL_LOCK_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG11_OFFSET/CGN_BITS_PER_REG);
  PLL_LOCK_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG11_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG11_OFFSET+31 downto C_STAT_REG11_OFFSET+9)       <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG11_OFFSET+8)                                     <= SYS_DCM_LOCK_I;
  SYS_DCM_LOCK_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+8);
  SYS_DCM_LOCK_O                                                        <= STATUS_REG_I(C_STAT_REG11_OFFSET+8);
  status_reg(C_STAT_REG11_OFFSET+7)                                     <= DAQ_PLL_LOCK_I;
  DAQ_PLL_LOCK_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+7);
  DAQ_PLL_LOCK_O                                                        <= STATUS_REG_I(C_STAT_REG11_OFFSET+7);
  status_reg(C_STAT_REG11_OFFSET+6)                                     <= OSERDES_PLL_LOCK_DCB_I;
  OSERDES_PLL_LOCK_DCB_BIT_TRIG_O                                       <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+6);
  OSERDES_PLL_LOCK_DCB_O                                                <= STATUS_REG_I(C_STAT_REG11_OFFSET+6);
  status_reg(C_STAT_REG11_OFFSET+5)                                     <= OSERDES_PLL_LOCK_TCB_I;
  OSERDES_PLL_LOCK_TCB_BIT_TRIG_O                                       <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+5);
  OSERDES_PLL_LOCK_TCB_O                                                <= STATUS_REG_I(C_STAT_REG11_OFFSET+5);
  status_reg(C_STAT_REG11_OFFSET+4)                                     <= ISERDES_PLL_LOCK_0_I;
  ISERDES_PLL_LOCK_0_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+4);
  ISERDES_PLL_LOCK_0_O                                                  <= STATUS_REG_I(C_STAT_REG11_OFFSET+4);
  status_reg(C_STAT_REG11_OFFSET+3)                                     <= ISERDES_PLL_LOCK_1_I;
  ISERDES_PLL_LOCK_1_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+3);
  ISERDES_PLL_LOCK_1_O                                                  <= STATUS_REG_I(C_STAT_REG11_OFFSET+3);
  status_reg(C_STAT_REG11_OFFSET+2)                                     <= DRS_PLL_LOCK_0_I;
  DRS_PLL_LOCK_0_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+2);
  DRS_PLL_LOCK_0_O                                                      <= STATUS_REG_I(C_STAT_REG11_OFFSET+2);
  status_reg(C_STAT_REG11_OFFSET+1)                                     <= DRS_PLL_LOCK_1_I;
  DRS_PLL_LOCK_1_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+1);
  DRS_PLL_LOCK_1_O                                                      <= STATUS_REG_I(C_STAT_REG11_OFFSET+1);
  status_reg(C_STAT_REG11_OFFSET+0)                                     <= LMK_PLL_LOCK_I;
  LMK_PLL_LOCK_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG11_OFFSET+0);
  LMK_PLL_LOCK_O                                                        <= STATUS_REG_I(C_STAT_REG11_OFFSET+0);

  -- Status Register 12 [0x0030]: DRS_STOP_CELL
  DRS_STOP_CELL_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG12_OFFSET/8)+3 downto C_STAT_REG12_OFFSET/8);
  DRS_STOP_CELL_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG12_OFFSET/CGN_BITS_PER_REG);
  DRS_STOP_CELL_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG12_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG12_OFFSET+31 downto C_STAT_REG12_OFFSET+26)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG12_OFFSET+25 downto C_STAT_REG12_OFFSET+16)      <= DRS_STOP_CELL_0_I;
  DRS_STOP_CELL_0_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG12_OFFSET+25 downto C_STAT_REG12_OFFSET+16);
  DRS_STOP_CELL_0_O                                                     <= STATUS_REG_I(C_STAT_REG12_OFFSET+25 downto C_STAT_REG12_OFFSET+16);
  status_reg(C_STAT_REG12_OFFSET+15 downto C_STAT_REG12_OFFSET+10)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG12_OFFSET+9 downto C_STAT_REG12_OFFSET+0)        <= DRS_STOP_CELL_1_I;
  DRS_STOP_CELL_1_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG12_OFFSET+9 downto C_STAT_REG12_OFFSET+0);
  DRS_STOP_CELL_1_O                                                     <= STATUS_REG_I(C_STAT_REG12_OFFSET+9 downto C_STAT_REG12_OFFSET+0);

  -- Status Register 13 [0x0034]: DRS_STOP_WSR
  DRS_STOP_WSR_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG13_OFFSET/8)+3 downto C_STAT_REG13_OFFSET/8);
  DRS_STOP_WSR_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG13_OFFSET/CGN_BITS_PER_REG);
  DRS_STOP_WSR_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG13_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG13_OFFSET+31 downto C_STAT_REG13_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG13_OFFSET+15 downto C_STAT_REG13_OFFSET+8)       <= DRS_STOP_WSR_0_I;
  DRS_STOP_WSR_0_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG13_OFFSET+15 downto C_STAT_REG13_OFFSET+8);
  DRS_STOP_WSR_0_O                                                      <= STATUS_REG_I(C_STAT_REG13_OFFSET+15 downto C_STAT_REG13_OFFSET+8);
  status_reg(C_STAT_REG13_OFFSET+7 downto C_STAT_REG13_OFFSET+0)        <= DRS_STOP_WSR_1_I;
  DRS_STOP_WSR_1_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG13_OFFSET+7 downto C_STAT_REG13_OFFSET+0);
  DRS_STOP_WSR_1_O                                                      <= STATUS_REG_I(C_STAT_REG13_OFFSET+7 downto C_STAT_REG13_OFFSET+0);

  -- Status Register 14 [0x0038]: DRS_SAMPLE_FREQ
  DRS_SAMPLE_FREQ_BYTE_TRIG_O                                           <= STAT_BYTE_TRIG_I((C_STAT_REG14_OFFSET/8)+3 downto C_STAT_REG14_OFFSET/8);
  DRS_SAMPLE_FREQ_REG_READ_O                                            <= STAT_REG_READ_I(C_STAT_REG14_OFFSET/CGN_BITS_PER_REG);
  DRS_SAMPLE_FREQ_REG_WRITE_O                                           <= STAT_REG_WRITE_I(C_STAT_REG14_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG14_OFFSET+31 downto C_STAT_REG14_OFFSET+24)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG14_OFFSET+23 downto C_STAT_REG14_OFFSET+0)       <= DRS_SAMPLE_FREQ_I;
  DRS_SAMPLE_FREQ_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG14_OFFSET+23 downto C_STAT_REG14_OFFSET+0);
  DRS_SAMPLE_FREQ_O                                                     <= STATUS_REG_I(C_STAT_REG14_OFFSET+23 downto C_STAT_REG14_OFFSET+0);

  -- Status Register 15 [0x003C]: ADC_SAMPLE_FREQ
  ADC_SAMPLE_FREQ_BYTE_TRIG_O                                           <= STAT_BYTE_TRIG_I((C_STAT_REG15_OFFSET/8)+3 downto C_STAT_REG15_OFFSET/8);
  ADC_SAMPLE_FREQ_REG_READ_O                                            <= STAT_REG_READ_I(C_STAT_REG15_OFFSET/CGN_BITS_PER_REG);
  ADC_SAMPLE_FREQ_REG_WRITE_O                                           <= STAT_REG_WRITE_I(C_STAT_REG15_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG15_OFFSET+31 downto C_STAT_REG15_OFFSET+24)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG15_OFFSET+23 downto C_STAT_REG15_OFFSET+0)       <= ADC_SAMPLE_FREQ_I;
  ADC_SAMPLE_FREQ_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG15_OFFSET+23 downto C_STAT_REG15_OFFSET+0);
  ADC_SAMPLE_FREQ_O                                                     <= STATUS_REG_I(C_STAT_REG15_OFFSET+23 downto C_STAT_REG15_OFFSET+0);

  -- Status Register 16 [0x0040]: TDC_SAMPLE_FREQ
  TDC_SAMPLE_FREQ_BYTE_TRIG_O                                           <= STAT_BYTE_TRIG_I((C_STAT_REG16_OFFSET/8)+3 downto C_STAT_REG16_OFFSET/8);
  TDC_SAMPLE_FREQ_REG_READ_O                                            <= STAT_REG_READ_I(C_STAT_REG16_OFFSET/CGN_BITS_PER_REG);
  TDC_SAMPLE_FREQ_REG_WRITE_O                                           <= STAT_REG_WRITE_I(C_STAT_REG16_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG16_OFFSET+31 downto C_STAT_REG16_OFFSET+24)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG16_OFFSET+23 downto C_STAT_REG16_OFFSET+0)       <= TDC_SAMPLE_FREQ_I;
  TDC_SAMPLE_FREQ_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG16_OFFSET+23 downto C_STAT_REG16_OFFSET+0);
  TDC_SAMPLE_FREQ_O                                                     <= STATUS_REG_I(C_STAT_REG16_OFFSET+23 downto C_STAT_REG16_OFFSET+0);

  -- Status Register 17 [0x0044]: HV_VER
  HV_VER_BYTE_TRIG_O                                                    <= STAT_BYTE_TRIG_I((C_STAT_REG17_OFFSET/8)+3 downto C_STAT_REG17_OFFSET/8);
  HV_VER_REG_READ_O                                                     <= STAT_REG_READ_I(C_STAT_REG17_OFFSET/CGN_BITS_PER_REG);
  HV_VER_REG_WRITE_O                                                    <= STAT_REG_WRITE_I(C_STAT_REG17_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG17_OFFSET+31 downto C_STAT_REG17_OFFSET+0)       <= HV_VER_I;
  HV_VER_BIT_TRIG_O                                                     <= STAT_BIT_TRIG_I(C_STAT_REG17_OFFSET+31 downto C_STAT_REG17_OFFSET+0);
  HV_VER_O                                                              <= STATUS_REG_I(C_STAT_REG17_OFFSET+31 downto C_STAT_REG17_OFFSET+0);

  -- Status Register 18 [0x0048]: HV_I_MEAS_0
  HV_I_MEAS_0_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG18_OFFSET/8)+3 downto C_STAT_REG18_OFFSET/8);
  HV_I_MEAS_0_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG18_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_0_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG18_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG18_OFFSET+31 downto C_STAT_REG18_OFFSET+0)       <= HV_I_MEAS_0_I;
  HV_I_MEAS_0_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG18_OFFSET+31 downto C_STAT_REG18_OFFSET+0);
  HV_I_MEAS_0_O                                                         <= STATUS_REG_I(C_STAT_REG18_OFFSET+31 downto C_STAT_REG18_OFFSET+0);

  -- Status Register 19 [0x004C]: HV_I_MEAS_1
  HV_I_MEAS_1_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG19_OFFSET/8)+3 downto C_STAT_REG19_OFFSET/8);
  HV_I_MEAS_1_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG19_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_1_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG19_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG19_OFFSET+31 downto C_STAT_REG19_OFFSET+0)       <= HV_I_MEAS_1_I;
  HV_I_MEAS_1_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG19_OFFSET+31 downto C_STAT_REG19_OFFSET+0);
  HV_I_MEAS_1_O                                                         <= STATUS_REG_I(C_STAT_REG19_OFFSET+31 downto C_STAT_REG19_OFFSET+0);

  -- Status Register 20 [0x0050]: HV_I_MEAS_2
  HV_I_MEAS_2_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG20_OFFSET/8)+3 downto C_STAT_REG20_OFFSET/8);
  HV_I_MEAS_2_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG20_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_2_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG20_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG20_OFFSET+31 downto C_STAT_REG20_OFFSET+0)       <= HV_I_MEAS_2_I;
  HV_I_MEAS_2_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG20_OFFSET+31 downto C_STAT_REG20_OFFSET+0);
  HV_I_MEAS_2_O                                                         <= STATUS_REG_I(C_STAT_REG20_OFFSET+31 downto C_STAT_REG20_OFFSET+0);

  -- Status Register 21 [0x0054]: HV_I_MEAS_3
  HV_I_MEAS_3_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG21_OFFSET/8)+3 downto C_STAT_REG21_OFFSET/8);
  HV_I_MEAS_3_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG21_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_3_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG21_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG21_OFFSET+31 downto C_STAT_REG21_OFFSET+0)       <= HV_I_MEAS_3_I;
  HV_I_MEAS_3_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG21_OFFSET+31 downto C_STAT_REG21_OFFSET+0);
  HV_I_MEAS_3_O                                                         <= STATUS_REG_I(C_STAT_REG21_OFFSET+31 downto C_STAT_REG21_OFFSET+0);

  -- Status Register 22 [0x0058]: HV_I_MEAS_4
  HV_I_MEAS_4_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG22_OFFSET/8)+3 downto C_STAT_REG22_OFFSET/8);
  HV_I_MEAS_4_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG22_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_4_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG22_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG22_OFFSET+31 downto C_STAT_REG22_OFFSET+0)       <= HV_I_MEAS_4_I;
  HV_I_MEAS_4_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG22_OFFSET+31 downto C_STAT_REG22_OFFSET+0);
  HV_I_MEAS_4_O                                                         <= STATUS_REG_I(C_STAT_REG22_OFFSET+31 downto C_STAT_REG22_OFFSET+0);

  -- Status Register 23 [0x005C]: HV_I_MEAS_5
  HV_I_MEAS_5_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG23_OFFSET/8)+3 downto C_STAT_REG23_OFFSET/8);
  HV_I_MEAS_5_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG23_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_5_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG23_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG23_OFFSET+31 downto C_STAT_REG23_OFFSET+0)       <= HV_I_MEAS_5_I;
  HV_I_MEAS_5_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG23_OFFSET+31 downto C_STAT_REG23_OFFSET+0);
  HV_I_MEAS_5_O                                                         <= STATUS_REG_I(C_STAT_REG23_OFFSET+31 downto C_STAT_REG23_OFFSET+0);

  -- Status Register 24 [0x0060]: HV_I_MEAS_6
  HV_I_MEAS_6_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG24_OFFSET/8)+3 downto C_STAT_REG24_OFFSET/8);
  HV_I_MEAS_6_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG24_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_6_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG24_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG24_OFFSET+31 downto C_STAT_REG24_OFFSET+0)       <= HV_I_MEAS_6_I;
  HV_I_MEAS_6_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG24_OFFSET+31 downto C_STAT_REG24_OFFSET+0);
  HV_I_MEAS_6_O                                                         <= STATUS_REG_I(C_STAT_REG24_OFFSET+31 downto C_STAT_REG24_OFFSET+0);

  -- Status Register 25 [0x0064]: HV_I_MEAS_7
  HV_I_MEAS_7_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG25_OFFSET/8)+3 downto C_STAT_REG25_OFFSET/8);
  HV_I_MEAS_7_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG25_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_7_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG25_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG25_OFFSET+31 downto C_STAT_REG25_OFFSET+0)       <= HV_I_MEAS_7_I;
  HV_I_MEAS_7_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG25_OFFSET+31 downto C_STAT_REG25_OFFSET+0);
  HV_I_MEAS_7_O                                                         <= STATUS_REG_I(C_STAT_REG25_OFFSET+31 downto C_STAT_REG25_OFFSET+0);

  -- Status Register 26 [0x0068]: HV_I_MEAS_8
  HV_I_MEAS_8_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG26_OFFSET/8)+3 downto C_STAT_REG26_OFFSET/8);
  HV_I_MEAS_8_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG26_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_8_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG26_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG26_OFFSET+31 downto C_STAT_REG26_OFFSET+0)       <= HV_I_MEAS_8_I;
  HV_I_MEAS_8_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG26_OFFSET+31 downto C_STAT_REG26_OFFSET+0);
  HV_I_MEAS_8_O                                                         <= STATUS_REG_I(C_STAT_REG26_OFFSET+31 downto C_STAT_REG26_OFFSET+0);

  -- Status Register 27 [0x006C]: HV_I_MEAS_9
  HV_I_MEAS_9_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG27_OFFSET/8)+3 downto C_STAT_REG27_OFFSET/8);
  HV_I_MEAS_9_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG27_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_9_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG27_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG27_OFFSET+31 downto C_STAT_REG27_OFFSET+0)       <= HV_I_MEAS_9_I;
  HV_I_MEAS_9_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG27_OFFSET+31 downto C_STAT_REG27_OFFSET+0);
  HV_I_MEAS_9_O                                                         <= STATUS_REG_I(C_STAT_REG27_OFFSET+31 downto C_STAT_REG27_OFFSET+0);

  -- Status Register 28 [0x0070]: HV_I_MEAS_10
  HV_I_MEAS_10_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG28_OFFSET/8)+3 downto C_STAT_REG28_OFFSET/8);
  HV_I_MEAS_10_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG28_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_10_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG28_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG28_OFFSET+31 downto C_STAT_REG28_OFFSET+0)       <= HV_I_MEAS_10_I;
  HV_I_MEAS_10_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG28_OFFSET+31 downto C_STAT_REG28_OFFSET+0);
  HV_I_MEAS_10_O                                                        <= STATUS_REG_I(C_STAT_REG28_OFFSET+31 downto C_STAT_REG28_OFFSET+0);

  -- Status Register 29 [0x0074]: HV_I_MEAS_11
  HV_I_MEAS_11_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG29_OFFSET/8)+3 downto C_STAT_REG29_OFFSET/8);
  HV_I_MEAS_11_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG29_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_11_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG29_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG29_OFFSET+31 downto C_STAT_REG29_OFFSET+0)       <= HV_I_MEAS_11_I;
  HV_I_MEAS_11_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG29_OFFSET+31 downto C_STAT_REG29_OFFSET+0);
  HV_I_MEAS_11_O                                                        <= STATUS_REG_I(C_STAT_REG29_OFFSET+31 downto C_STAT_REG29_OFFSET+0);

  -- Status Register 30 [0x0078]: HV_I_MEAS_12
  HV_I_MEAS_12_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG30_OFFSET/8)+3 downto C_STAT_REG30_OFFSET/8);
  HV_I_MEAS_12_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG30_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_12_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG30_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG30_OFFSET+31 downto C_STAT_REG30_OFFSET+0)       <= HV_I_MEAS_12_I;
  HV_I_MEAS_12_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG30_OFFSET+31 downto C_STAT_REG30_OFFSET+0);
  HV_I_MEAS_12_O                                                        <= STATUS_REG_I(C_STAT_REG30_OFFSET+31 downto C_STAT_REG30_OFFSET+0);

  -- Status Register 31 [0x007C]: HV_I_MEAS_13
  HV_I_MEAS_13_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG31_OFFSET/8)+3 downto C_STAT_REG31_OFFSET/8);
  HV_I_MEAS_13_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG31_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_13_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG31_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG31_OFFSET+31 downto C_STAT_REG31_OFFSET+0)       <= HV_I_MEAS_13_I;
  HV_I_MEAS_13_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG31_OFFSET+31 downto C_STAT_REG31_OFFSET+0);
  HV_I_MEAS_13_O                                                        <= STATUS_REG_I(C_STAT_REG31_OFFSET+31 downto C_STAT_REG31_OFFSET+0);

  -- Status Register 32 [0x0080]: HV_I_MEAS_14
  HV_I_MEAS_14_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG32_OFFSET/8)+3 downto C_STAT_REG32_OFFSET/8);
  HV_I_MEAS_14_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG32_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_14_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG32_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG32_OFFSET+31 downto C_STAT_REG32_OFFSET+0)       <= HV_I_MEAS_14_I;
  HV_I_MEAS_14_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG32_OFFSET+31 downto C_STAT_REG32_OFFSET+0);
  HV_I_MEAS_14_O                                                        <= STATUS_REG_I(C_STAT_REG32_OFFSET+31 downto C_STAT_REG32_OFFSET+0);

  -- Status Register 33 [0x0084]: HV_I_MEAS_15
  HV_I_MEAS_15_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG33_OFFSET/8)+3 downto C_STAT_REG33_OFFSET/8);
  HV_I_MEAS_15_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG33_OFFSET/CGN_BITS_PER_REG);
  HV_I_MEAS_15_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG33_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG33_OFFSET+31 downto C_STAT_REG33_OFFSET+0)       <= HV_I_MEAS_15_I;
  HV_I_MEAS_15_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG33_OFFSET+31 downto C_STAT_REG33_OFFSET+0);
  HV_I_MEAS_15_O                                                        <= STATUS_REG_I(C_STAT_REG33_OFFSET+31 downto C_STAT_REG33_OFFSET+0);

  -- Status Register 34 [0x0088]: HV_U_BASE_MEAS
  HV_U_BASE_MEAS_BYTE_TRIG_O                                            <= STAT_BYTE_TRIG_I((C_STAT_REG34_OFFSET/8)+3 downto C_STAT_REG34_OFFSET/8);
  HV_U_BASE_MEAS_REG_READ_O                                             <= STAT_REG_READ_I(C_STAT_REG34_OFFSET/CGN_BITS_PER_REG);
  HV_U_BASE_MEAS_REG_WRITE_O                                            <= STAT_REG_WRITE_I(C_STAT_REG34_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG34_OFFSET+31 downto C_STAT_REG34_OFFSET+0)       <= HV_U_BASE_MEAS_I;
  HV_U_BASE_MEAS_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG34_OFFSET+31 downto C_STAT_REG34_OFFSET+0);
  HV_U_BASE_MEAS_O                                                      <= STATUS_REG_I(C_STAT_REG34_OFFSET+31 downto C_STAT_REG34_OFFSET+0);

  -- Status Register 35 [0x008C]: HV_TEMP_0
  HV_TEMP_0_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG35_OFFSET/8)+3 downto C_STAT_REG35_OFFSET/8);
  HV_TEMP_0_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG35_OFFSET/CGN_BITS_PER_REG);
  HV_TEMP_0_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG35_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG35_OFFSET+31 downto C_STAT_REG35_OFFSET+0)       <= HV_TEMP_0_I;
  HV_TEMP_0_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG35_OFFSET+31 downto C_STAT_REG35_OFFSET+0);
  HV_TEMP_0_O                                                           <= STATUS_REG_I(C_STAT_REG35_OFFSET+31 downto C_STAT_REG35_OFFSET+0);

  -- Status Register 36 [0x0090]: HV_TEMP_1
  HV_TEMP_1_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG36_OFFSET/8)+3 downto C_STAT_REG36_OFFSET/8);
  HV_TEMP_1_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG36_OFFSET/CGN_BITS_PER_REG);
  HV_TEMP_1_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG36_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG36_OFFSET+31 downto C_STAT_REG36_OFFSET+0)       <= HV_TEMP_1_I;
  HV_TEMP_1_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG36_OFFSET+31 downto C_STAT_REG36_OFFSET+0);
  HV_TEMP_1_O                                                           <= STATUS_REG_I(C_STAT_REG36_OFFSET+31 downto C_STAT_REG36_OFFSET+0);

  -- Status Register 37 [0x0094]: HV_TEMP_2
  HV_TEMP_2_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG37_OFFSET/8)+3 downto C_STAT_REG37_OFFSET/8);
  HV_TEMP_2_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG37_OFFSET/CGN_BITS_PER_REG);
  HV_TEMP_2_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG37_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG37_OFFSET+31 downto C_STAT_REG37_OFFSET+0)       <= HV_TEMP_2_I;
  HV_TEMP_2_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG37_OFFSET+31 downto C_STAT_REG37_OFFSET+0);
  HV_TEMP_2_O                                                           <= STATUS_REG_I(C_STAT_REG37_OFFSET+31 downto C_STAT_REG37_OFFSET+0);

  -- Status Register 38 [0x0098]: HV_TEMP_3
  HV_TEMP_3_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG38_OFFSET/8)+3 downto C_STAT_REG38_OFFSET/8);
  HV_TEMP_3_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG38_OFFSET/CGN_BITS_PER_REG);
  HV_TEMP_3_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG38_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG38_OFFSET+31 downto C_STAT_REG38_OFFSET+0)       <= HV_TEMP_3_I;
  HV_TEMP_3_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG38_OFFSET+31 downto C_STAT_REG38_OFFSET+0);
  HV_TEMP_3_O                                                           <= STATUS_REG_I(C_STAT_REG38_OFFSET+31 downto C_STAT_REG38_OFFSET+0);

  -- Status Register 39 [0x009C]: SCALER_0
  SCALER_0_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG39_OFFSET/8)+3 downto C_STAT_REG39_OFFSET/8);
  SCALER_0_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG39_OFFSET/CGN_BITS_PER_REG);
  SCALER_0_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG39_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG39_OFFSET+31 downto C_STAT_REG39_OFFSET+0)       <= SCALER_0_I;
  SCALER_0_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG39_OFFSET+31 downto C_STAT_REG39_OFFSET+0);
  SCALER_0_O                                                            <= STATUS_REG_I(C_STAT_REG39_OFFSET+31 downto C_STAT_REG39_OFFSET+0);

  -- Status Register 40 [0x00A0]: SCALER_1
  SCALER_1_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG40_OFFSET/8)+3 downto C_STAT_REG40_OFFSET/8);
  SCALER_1_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG40_OFFSET/CGN_BITS_PER_REG);
  SCALER_1_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG40_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG40_OFFSET+31 downto C_STAT_REG40_OFFSET+0)       <= SCALER_1_I;
  SCALER_1_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG40_OFFSET+31 downto C_STAT_REG40_OFFSET+0);
  SCALER_1_O                                                            <= STATUS_REG_I(C_STAT_REG40_OFFSET+31 downto C_STAT_REG40_OFFSET+0);

  -- Status Register 41 [0x00A4]: SCALER_2
  SCALER_2_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG41_OFFSET/8)+3 downto C_STAT_REG41_OFFSET/8);
  SCALER_2_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG41_OFFSET/CGN_BITS_PER_REG);
  SCALER_2_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG41_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG41_OFFSET+31 downto C_STAT_REG41_OFFSET+0)       <= SCALER_2_I;
  SCALER_2_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG41_OFFSET+31 downto C_STAT_REG41_OFFSET+0);
  SCALER_2_O                                                            <= STATUS_REG_I(C_STAT_REG41_OFFSET+31 downto C_STAT_REG41_OFFSET+0);

  -- Status Register 42 [0x00A8]: SCALER_3
  SCALER_3_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG42_OFFSET/8)+3 downto C_STAT_REG42_OFFSET/8);
  SCALER_3_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG42_OFFSET/CGN_BITS_PER_REG);
  SCALER_3_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG42_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG42_OFFSET+31 downto C_STAT_REG42_OFFSET+0)       <= SCALER_3_I;
  SCALER_3_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG42_OFFSET+31 downto C_STAT_REG42_OFFSET+0);
  SCALER_3_O                                                            <= STATUS_REG_I(C_STAT_REG42_OFFSET+31 downto C_STAT_REG42_OFFSET+0);

  -- Status Register 43 [0x00AC]: SCALER_4
  SCALER_4_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG43_OFFSET/8)+3 downto C_STAT_REG43_OFFSET/8);
  SCALER_4_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG43_OFFSET/CGN_BITS_PER_REG);
  SCALER_4_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG43_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG43_OFFSET+31 downto C_STAT_REG43_OFFSET+0)       <= SCALER_4_I;
  SCALER_4_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG43_OFFSET+31 downto C_STAT_REG43_OFFSET+0);
  SCALER_4_O                                                            <= STATUS_REG_I(C_STAT_REG43_OFFSET+31 downto C_STAT_REG43_OFFSET+0);

  -- Status Register 44 [0x00B0]: SCALER_5
  SCALER_5_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG44_OFFSET/8)+3 downto C_STAT_REG44_OFFSET/8);
  SCALER_5_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG44_OFFSET/CGN_BITS_PER_REG);
  SCALER_5_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG44_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG44_OFFSET+31 downto C_STAT_REG44_OFFSET+0)       <= SCALER_5_I;
  SCALER_5_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG44_OFFSET+31 downto C_STAT_REG44_OFFSET+0);
  SCALER_5_O                                                            <= STATUS_REG_I(C_STAT_REG44_OFFSET+31 downto C_STAT_REG44_OFFSET+0);

  -- Status Register 45 [0x00B4]: SCALER_6
  SCALER_6_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG45_OFFSET/8)+3 downto C_STAT_REG45_OFFSET/8);
  SCALER_6_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG45_OFFSET/CGN_BITS_PER_REG);
  SCALER_6_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG45_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG45_OFFSET+31 downto C_STAT_REG45_OFFSET+0)       <= SCALER_6_I;
  SCALER_6_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG45_OFFSET+31 downto C_STAT_REG45_OFFSET+0);
  SCALER_6_O                                                            <= STATUS_REG_I(C_STAT_REG45_OFFSET+31 downto C_STAT_REG45_OFFSET+0);

  -- Status Register 46 [0x00B8]: SCALER_7
  SCALER_7_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG46_OFFSET/8)+3 downto C_STAT_REG46_OFFSET/8);
  SCALER_7_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG46_OFFSET/CGN_BITS_PER_REG);
  SCALER_7_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG46_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG46_OFFSET+31 downto C_STAT_REG46_OFFSET+0)       <= SCALER_7_I;
  SCALER_7_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG46_OFFSET+31 downto C_STAT_REG46_OFFSET+0);
  SCALER_7_O                                                            <= STATUS_REG_I(C_STAT_REG46_OFFSET+31 downto C_STAT_REG46_OFFSET+0);

  -- Status Register 47 [0x00BC]: SCALER_8
  SCALER_8_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG47_OFFSET/8)+3 downto C_STAT_REG47_OFFSET/8);
  SCALER_8_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG47_OFFSET/CGN_BITS_PER_REG);
  SCALER_8_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG47_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG47_OFFSET+31 downto C_STAT_REG47_OFFSET+0)       <= SCALER_8_I;
  SCALER_8_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG47_OFFSET+31 downto C_STAT_REG47_OFFSET+0);
  SCALER_8_O                                                            <= STATUS_REG_I(C_STAT_REG47_OFFSET+31 downto C_STAT_REG47_OFFSET+0);

  -- Status Register 48 [0x00C0]: SCALER_9
  SCALER_9_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG48_OFFSET/8)+3 downto C_STAT_REG48_OFFSET/8);
  SCALER_9_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG48_OFFSET/CGN_BITS_PER_REG);
  SCALER_9_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG48_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG48_OFFSET+31 downto C_STAT_REG48_OFFSET+0)       <= SCALER_9_I;
  SCALER_9_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG48_OFFSET+31 downto C_STAT_REG48_OFFSET+0);
  SCALER_9_O                                                            <= STATUS_REG_I(C_STAT_REG48_OFFSET+31 downto C_STAT_REG48_OFFSET+0);

  -- Status Register 49 [0x00C4]: SCALER_10
  SCALER_10_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG49_OFFSET/8)+3 downto C_STAT_REG49_OFFSET/8);
  SCALER_10_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG49_OFFSET/CGN_BITS_PER_REG);
  SCALER_10_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG49_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG49_OFFSET+31 downto C_STAT_REG49_OFFSET+0)       <= SCALER_10_I;
  SCALER_10_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG49_OFFSET+31 downto C_STAT_REG49_OFFSET+0);
  SCALER_10_O                                                           <= STATUS_REG_I(C_STAT_REG49_OFFSET+31 downto C_STAT_REG49_OFFSET+0);

  -- Status Register 50 [0x00C8]: SCALER_11
  SCALER_11_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG50_OFFSET/8)+3 downto C_STAT_REG50_OFFSET/8);
  SCALER_11_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG50_OFFSET/CGN_BITS_PER_REG);
  SCALER_11_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG50_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG50_OFFSET+31 downto C_STAT_REG50_OFFSET+0)       <= SCALER_11_I;
  SCALER_11_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG50_OFFSET+31 downto C_STAT_REG50_OFFSET+0);
  SCALER_11_O                                                           <= STATUS_REG_I(C_STAT_REG50_OFFSET+31 downto C_STAT_REG50_OFFSET+0);

  -- Status Register 51 [0x00CC]: SCALER_12
  SCALER_12_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG51_OFFSET/8)+3 downto C_STAT_REG51_OFFSET/8);
  SCALER_12_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG51_OFFSET/CGN_BITS_PER_REG);
  SCALER_12_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG51_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG51_OFFSET+31 downto C_STAT_REG51_OFFSET+0)       <= SCALER_12_I;
  SCALER_12_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG51_OFFSET+31 downto C_STAT_REG51_OFFSET+0);
  SCALER_12_O                                                           <= STATUS_REG_I(C_STAT_REG51_OFFSET+31 downto C_STAT_REG51_OFFSET+0);

  -- Status Register 52 [0x00D0]: SCALER_13
  SCALER_13_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG52_OFFSET/8)+3 downto C_STAT_REG52_OFFSET/8);
  SCALER_13_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG52_OFFSET/CGN_BITS_PER_REG);
  SCALER_13_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG52_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG52_OFFSET+31 downto C_STAT_REG52_OFFSET+0)       <= SCALER_13_I;
  SCALER_13_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG52_OFFSET+31 downto C_STAT_REG52_OFFSET+0);
  SCALER_13_O                                                           <= STATUS_REG_I(C_STAT_REG52_OFFSET+31 downto C_STAT_REG52_OFFSET+0);

  -- Status Register 53 [0x00D4]: SCALER_14
  SCALER_14_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG53_OFFSET/8)+3 downto C_STAT_REG53_OFFSET/8);
  SCALER_14_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG53_OFFSET/CGN_BITS_PER_REG);
  SCALER_14_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG53_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG53_OFFSET+31 downto C_STAT_REG53_OFFSET+0)       <= SCALER_14_I;
  SCALER_14_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG53_OFFSET+31 downto C_STAT_REG53_OFFSET+0);
  SCALER_14_O                                                           <= STATUS_REG_I(C_STAT_REG53_OFFSET+31 downto C_STAT_REG53_OFFSET+0);

  -- Status Register 54 [0x00D8]: SCALER_15
  SCALER_15_BYTE_TRIG_O                                                 <= STAT_BYTE_TRIG_I((C_STAT_REG54_OFFSET/8)+3 downto C_STAT_REG54_OFFSET/8);
  SCALER_15_REG_READ_O                                                  <= STAT_REG_READ_I(C_STAT_REG54_OFFSET/CGN_BITS_PER_REG);
  SCALER_15_REG_WRITE_O                                                 <= STAT_REG_WRITE_I(C_STAT_REG54_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG54_OFFSET+31 downto C_STAT_REG54_OFFSET+0)       <= SCALER_15_I;
  SCALER_15_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG54_OFFSET+31 downto C_STAT_REG54_OFFSET+0);
  SCALER_15_O                                                           <= STATUS_REG_I(C_STAT_REG54_OFFSET+31 downto C_STAT_REG54_OFFSET+0);

  -- Status Register 55 [0x00DC]: SCALER_PTRN_TRG
  SCALER_PTRN_TRG_BYTE_TRIG_O                                           <= STAT_BYTE_TRIG_I((C_STAT_REG55_OFFSET/8)+3 downto C_STAT_REG55_OFFSET/8);
  SCALER_PTRN_TRG_REG_READ_O                                            <= STAT_REG_READ_I(C_STAT_REG55_OFFSET/CGN_BITS_PER_REG);
  SCALER_PTRN_TRG_REG_WRITE_O                                           <= STAT_REG_WRITE_I(C_STAT_REG55_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG55_OFFSET+31 downto C_STAT_REG55_OFFSET+0)       <= SCALER_PTRN_TRG_I;
  SCALER_PTRN_TRG_BIT_TRIG_O                                            <= STAT_BIT_TRIG_I(C_STAT_REG55_OFFSET+31 downto C_STAT_REG55_OFFSET+0);
  SCALER_PTRN_TRG_O                                                     <= STATUS_REG_I(C_STAT_REG55_OFFSET+31 downto C_STAT_REG55_OFFSET+0);

  -- Status Register 56 [0x00E0]: SCALER_EXT_TRG
  SCALER_EXT_TRG_BYTE_TRIG_O                                            <= STAT_BYTE_TRIG_I((C_STAT_REG56_OFFSET/8)+3 downto C_STAT_REG56_OFFSET/8);
  SCALER_EXT_TRG_REG_READ_O                                             <= STAT_REG_READ_I(C_STAT_REG56_OFFSET/CGN_BITS_PER_REG);
  SCALER_EXT_TRG_REG_WRITE_O                                            <= STAT_REG_WRITE_I(C_STAT_REG56_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG56_OFFSET+31 downto C_STAT_REG56_OFFSET+0)       <= SCALER_EXT_TRG_I;
  SCALER_EXT_TRG_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG56_OFFSET+31 downto C_STAT_REG56_OFFSET+0);
  SCALER_EXT_TRG_O                                                      <= STATUS_REG_I(C_STAT_REG56_OFFSET+31 downto C_STAT_REG56_OFFSET+0);

  -- Status Register 57 [0x00E4]: SCALER_EXT_CLK
  SCALER_EXT_CLK_BYTE_TRIG_O                                            <= STAT_BYTE_TRIG_I((C_STAT_REG57_OFFSET/8)+3 downto C_STAT_REG57_OFFSET/8);
  SCALER_EXT_CLK_REG_READ_O                                             <= STAT_REG_READ_I(C_STAT_REG57_OFFSET/CGN_BITS_PER_REG);
  SCALER_EXT_CLK_REG_WRITE_O                                            <= STAT_REG_WRITE_I(C_STAT_REG57_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG57_OFFSET+31 downto C_STAT_REG57_OFFSET+0)       <= SCALER_EXT_CLK_I;
  SCALER_EXT_CLK_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG57_OFFSET+31 downto C_STAT_REG57_OFFSET+0);
  SCALER_EXT_CLK_O                                                      <= STATUS_REG_I(C_STAT_REG57_OFFSET+31 downto C_STAT_REG57_OFFSET+0);

  -- Status Register 58 [0x00E8]: SCALER_TIME_STAMP_LSB
  SCALER_TIME_STAMP_LSB_BYTE_TRIG_O                                     <= STAT_BYTE_TRIG_I((C_STAT_REG58_OFFSET/8)+3 downto C_STAT_REG58_OFFSET/8);
  SCALER_TIME_STAMP_LSB_REG_READ_O                                      <= STAT_REG_READ_I(C_STAT_REG58_OFFSET/CGN_BITS_PER_REG);
  SCALER_TIME_STAMP_LSB_REG_WRITE_O                                     <= STAT_REG_WRITE_I(C_STAT_REG58_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG58_OFFSET+31 downto C_STAT_REG58_OFFSET+0)       <= SCALER_TIME_STAMP_LSB_I;
  SCALER_TIME_STAMP_LSB_BIT_TRIG_O                                      <= STAT_BIT_TRIG_I(C_STAT_REG58_OFFSET+31 downto C_STAT_REG58_OFFSET+0);
  SCALER_TIME_STAMP_LSB_O                                               <= STATUS_REG_I(C_STAT_REG58_OFFSET+31 downto C_STAT_REG58_OFFSET+0);

  -- Status Register 59 [0x00EC]: SCALER_TIME_STAMP_MSB
  SCALER_TIME_STAMP_MSB_BYTE_TRIG_O                                     <= STAT_BYTE_TRIG_I((C_STAT_REG59_OFFSET/8)+3 downto C_STAT_REG59_OFFSET/8);
  SCALER_TIME_STAMP_MSB_REG_READ_O                                      <= STAT_REG_READ_I(C_STAT_REG59_OFFSET/CGN_BITS_PER_REG);
  SCALER_TIME_STAMP_MSB_REG_WRITE_O                                     <= STAT_REG_WRITE_I(C_STAT_REG59_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG59_OFFSET+31 downto C_STAT_REG59_OFFSET+0)       <= SCALER_TIME_STAMP_MSB_I;
  SCALER_TIME_STAMP_MSB_BIT_TRIG_O                                      <= STAT_BIT_TRIG_I(C_STAT_REG59_OFFSET+31 downto C_STAT_REG59_OFFSET+0);
  SCALER_TIME_STAMP_MSB_O                                               <= STATUS_REG_I(C_STAT_REG59_OFFSET+31 downto C_STAT_REG59_OFFSET+0);

  -- Status Register 60 [0x00F0]: TIME_LSB
  TIME_LSB_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG60_OFFSET/8)+3 downto C_STAT_REG60_OFFSET/8);
  TIME_LSB_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG60_OFFSET/CGN_BITS_PER_REG);
  TIME_LSB_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG60_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG60_OFFSET+31 downto C_STAT_REG60_OFFSET+0)       <= TIME_LSB_I;
  TIME_LSB_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG60_OFFSET+31 downto C_STAT_REG60_OFFSET+0);
  TIME_LSB_O                                                            <= STATUS_REG_I(C_STAT_REG60_OFFSET+31 downto C_STAT_REG60_OFFSET+0);

  -- Status Register 61 [0x00F4]: TIME_MSB
  TIME_MSB_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG61_OFFSET/8)+3 downto C_STAT_REG61_OFFSET/8);
  TIME_MSB_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG61_OFFSET/CGN_BITS_PER_REG);
  TIME_MSB_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG61_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG61_OFFSET+31 downto C_STAT_REG61_OFFSET+0)       <= TIME_MSB_I;
  TIME_MSB_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG61_OFFSET+31 downto C_STAT_REG61_OFFSET+0);
  TIME_MSB_O                                                            <= STATUS_REG_I(C_STAT_REG61_OFFSET+31 downto C_STAT_REG61_OFFSET+0);

  -- Status Register 62 [0x00F8]: COMP_CH_STAT
  COMP_CH_STAT_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG62_OFFSET/8)+3 downto C_STAT_REG62_OFFSET/8);
  COMP_CH_STAT_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG62_OFFSET/CGN_BITS_PER_REG);
  COMP_CH_STAT_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG62_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG62_OFFSET+31 downto C_STAT_REG62_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG62_OFFSET+15 downto C_STAT_REG62_OFFSET+0)       <= COMP_CH_STAT_I;
  COMP_CH_STAT_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG62_OFFSET+15 downto C_STAT_REG62_OFFSET+0);
  COMP_CH_STAT_O                                                        <= STATUS_REG_I(C_STAT_REG62_OFFSET+15 downto C_STAT_REG62_OFFSET+0);

  -- Status Register 63 [0x00FC]: EVENT_TX_RATE
  EVENT_TX_RATE_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG63_OFFSET/8)+3 downto C_STAT_REG63_OFFSET/8);
  EVENT_TX_RATE_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG63_OFFSET/CGN_BITS_PER_REG);
  EVENT_TX_RATE_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG63_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG63_OFFSET+31 downto C_STAT_REG63_OFFSET+0)       <= EVENT_TX_RATE_I;
  EVENT_TX_RATE_BIT_TRIG_O                                              <= STAT_BIT_TRIG_I(C_STAT_REG63_OFFSET+31 downto C_STAT_REG63_OFFSET+0);
  EVENT_TX_RATE_O                                                       <= STATUS_REG_I(C_STAT_REG63_OFFSET+31 downto C_STAT_REG63_OFFSET+0);

  -- Status Register 64 [0x0100]: EVENT_NR
  EVENT_NR_BYTE_TRIG_O                                                  <= STAT_BYTE_TRIG_I((C_STAT_REG64_OFFSET/8)+3 downto C_STAT_REG64_OFFSET/8);
  EVENT_NR_REG_READ_O                                                   <= STAT_REG_READ_I(C_STAT_REG64_OFFSET/CGN_BITS_PER_REG);
  EVENT_NR_REG_WRITE_O                                                  <= STAT_REG_WRITE_I(C_STAT_REG64_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG64_OFFSET+31 downto C_STAT_REG64_OFFSET+0)       <= EVENT_NUMBER_I;
  EVENT_NUMBER_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG64_OFFSET+31 downto C_STAT_REG64_OFFSET+0);
  EVENT_NUMBER_O                                                        <= STATUS_REG_I(C_STAT_REG64_OFFSET+31 downto C_STAT_REG64_OFFSET+0);

  -- Status Register 65 [0x0104]: TRB_INFO_STAT
  TRB_INFO_STAT_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG65_OFFSET/8)+3 downto C_STAT_REG65_OFFSET/8);
  TRB_INFO_STAT_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG65_OFFSET/CGN_BITS_PER_REG);
  TRB_INFO_STAT_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG65_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG65_OFFSET+31)                                    <= TRB_FLAG_NEW_I;
  TRB_FLAG_NEW_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG65_OFFSET+31);
  TRB_FLAG_NEW_O                                                        <= STATUS_REG_I(C_STAT_REG65_OFFSET+31);
  status_reg(C_STAT_REG65_OFFSET+30)                                    <= TRB_FLAG_PARITY_ERROR_I;
  TRB_FLAG_PARITY_ERROR_BIT_TRIG_O                                      <= STAT_BIT_TRIG_I(C_STAT_REG65_OFFSET+30);
  TRB_FLAG_PARITY_ERROR_O                                               <= STATUS_REG_I(C_STAT_REG65_OFFSET+30);
  status_reg(C_STAT_REG65_OFFSET+29 downto C_STAT_REG65_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG65_OFFSET+15 downto C_STAT_REG65_OFFSET+0)       <= TRB_PARITY_ERROR_COUNT_I;
  TRB_PARITY_ERROR_COUNT_BIT_TRIG_O                                     <= STAT_BIT_TRIG_I(C_STAT_REG65_OFFSET+15 downto C_STAT_REG65_OFFSET+0);
  TRB_PARITY_ERROR_COUNT_O                                              <= STATUS_REG_I(C_STAT_REG65_OFFSET+15 downto C_STAT_REG65_OFFSET+0);

  -- Status Register 66 [0x0108]: TRB_INFO_LSB
  TRB_INFO_LSB_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG66_OFFSET/8)+3 downto C_STAT_REG66_OFFSET/8);
  TRB_INFO_LSB_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG66_OFFSET/CGN_BITS_PER_REG);
  TRB_INFO_LSB_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG66_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG66_OFFSET+31 downto C_STAT_REG66_OFFSET+0)       <= TRB_INFO_LSB_I;
  TRB_INFO_LSB_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG66_OFFSET+31 downto C_STAT_REG66_OFFSET+0);
  TRB_INFO_LSB_O                                                        <= STATUS_REG_I(C_STAT_REG66_OFFSET+31 downto C_STAT_REG66_OFFSET+0);

  -- Status Register 67 [0x010C]: TRB_INFO_MSB
  TRB_INFO_MSB_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG67_OFFSET/8)+3 downto C_STAT_REG67_OFFSET/8);
  TRB_INFO_MSB_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG67_OFFSET/CGN_BITS_PER_REG);
  TRB_INFO_MSB_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG67_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG67_OFFSET+31 downto C_STAT_REG67_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG67_OFFSET+15 downto C_STAT_REG67_OFFSET+0)       <= TRB_INFO_MSB_I;
  TRB_INFO_MSB_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG67_OFFSET+15 downto C_STAT_REG67_OFFSET+0);
  TRB_INFO_MSB_O                                                        <= STATUS_REG_I(C_STAT_REG67_OFFSET+15 downto C_STAT_REG67_OFFSET+0);

  -- Status Register 68 [0x0110]: ADV_TRG_TRIG_CELL
  ADV_TRG_TRIG_CELL_BYTE_TRIG_O                                         <= STAT_BYTE_TRIG_I((C_STAT_REG68_OFFSET/8)+3 downto C_STAT_REG68_OFFSET/8);
  ADV_TRG_TRIG_CELL_REG_READ_O                                          <= STAT_REG_READ_I(C_STAT_REG68_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_TRIG_CELL_REG_WRITE_O                                         <= STAT_REG_WRITE_I(C_STAT_REG68_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG68_OFFSET+31 downto C_STAT_REG68_OFFSET+0)       <= ADV_TRG_TRIG_CELL_I;
  ADV_TRG_TRIG_CELL_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG68_OFFSET+31 downto C_STAT_REG68_OFFSET+0);
  ADV_TRG_TRIG_CELL_O                                                   <= STATUS_REG_I(C_STAT_REG68_OFFSET+31 downto C_STAT_REG68_OFFSET+0);

  -- Status Register 69 [0x0114]: ADV_TRG_STAT1
  ADV_TRG_STAT1_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG69_OFFSET/8)+3 downto C_STAT_REG69_OFFSET/8);
  ADV_TRG_STAT1_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG69_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_STAT1_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG69_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG69_OFFSET+31 downto C_STAT_REG69_OFFSET+0)       <= ADV_TRG_STAT_1_I;
  ADV_TRG_STAT_1_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG69_OFFSET+31 downto C_STAT_REG69_OFFSET+0);
  ADV_TRG_STAT_1_O                                                      <= STATUS_REG_I(C_STAT_REG69_OFFSET+31 downto C_STAT_REG69_OFFSET+0);

  -- Status Register 70 [0x0118]: ADV_TRG_STAT2
  ADV_TRG_STAT2_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG70_OFFSET/8)+3 downto C_STAT_REG70_OFFSET/8);
  ADV_TRG_STAT2_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG70_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_STAT2_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG70_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG70_OFFSET+31 downto C_STAT_REG70_OFFSET+0)       <= ADV_TRG_STAT_2_I;
  ADV_TRG_STAT_2_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG70_OFFSET+31 downto C_STAT_REG70_OFFSET+0);
  ADV_TRG_STAT_2_O                                                      <= STATUS_REG_I(C_STAT_REG70_OFFSET+31 downto C_STAT_REG70_OFFSET+0);

  -- Status Register 71 [0x011C]: ADV_TRG_STAT3
  ADV_TRG_STAT3_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG71_OFFSET/8)+3 downto C_STAT_REG71_OFFSET/8);
  ADV_TRG_STAT3_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG71_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_STAT3_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG71_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG71_OFFSET+31 downto C_STAT_REG71_OFFSET+0)       <= ADV_TRG_STAT_3_I;
  ADV_TRG_STAT_3_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG71_OFFSET+31 downto C_STAT_REG71_OFFSET+0);
  ADV_TRG_STAT_3_O                                                      <= STATUS_REG_I(C_STAT_REG71_OFFSET+31 downto C_STAT_REG71_OFFSET+0);

  -- Status Register 72 [0x0120]: ADV_TRG_STAT4
  ADV_TRG_STAT4_BYTE_TRIG_O                                             <= STAT_BYTE_TRIG_I((C_STAT_REG72_OFFSET/8)+3 downto C_STAT_REG72_OFFSET/8);
  ADV_TRG_STAT4_REG_READ_O                                              <= STAT_REG_READ_I(C_STAT_REG72_OFFSET/CGN_BITS_PER_REG);
  ADV_TRG_STAT4_REG_WRITE_O                                             <= STAT_REG_WRITE_I(C_STAT_REG72_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG72_OFFSET+31 downto C_STAT_REG72_OFFSET+0)       <= ADV_TRG_STAT_4_I;
  ADV_TRG_STAT_4_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG72_OFFSET+31 downto C_STAT_REG72_OFFSET+0);
  ADV_TRG_STAT_4_O                                                      <= STATUS_REG_I(C_STAT_REG72_OFFSET+31 downto C_STAT_REG72_OFFSET+0);

  -- Status Register 73 [0x0124]: MAX_DRS_ADC_PKT_SAMPLES
  MAX_DRS_ADC_PKT_SAMPLES_BYTE_TRIG_O                                   <= STAT_BYTE_TRIG_I((C_STAT_REG73_OFFSET/8)+3 downto C_STAT_REG73_OFFSET/8);
  MAX_DRS_ADC_PKT_SAMPLES_REG_READ_O                                    <= STAT_REG_READ_I(C_STAT_REG73_OFFSET/CGN_BITS_PER_REG);
  MAX_DRS_ADC_PKT_SAMPLES_REG_WRITE_O                                   <= STAT_REG_WRITE_I(C_STAT_REG73_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG73_OFFSET+31 downto C_STAT_REG73_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG73_OFFSET+15 downto C_STAT_REG73_OFFSET+0)       <= MAX_DRS_ADC_PKT_SAMPLES_I;
  MAX_DRS_ADC_PKT_SAMPLES_BIT_TRIG_O                                    <= STAT_BIT_TRIG_I(C_STAT_REG73_OFFSET+15 downto C_STAT_REG73_OFFSET+0);
  MAX_DRS_ADC_PKT_SAMPLES_O                                             <= STATUS_REG_I(C_STAT_REG73_OFFSET+15 downto C_STAT_REG73_OFFSET+0);

  -- Status Register 74 [0x0128]: MAX_TDC_PKT_SAMPLES
  MAX_TDC_PKT_SAMPLES_BYTE_TRIG_O                                       <= STAT_BYTE_TRIG_I((C_STAT_REG74_OFFSET/8)+3 downto C_STAT_REG74_OFFSET/8);
  MAX_TDC_PKT_SAMPLES_REG_READ_O                                        <= STAT_REG_READ_I(C_STAT_REG74_OFFSET/CGN_BITS_PER_REG);
  MAX_TDC_PKT_SAMPLES_REG_WRITE_O                                       <= STAT_REG_WRITE_I(C_STAT_REG74_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG74_OFFSET+31 downto C_STAT_REG74_OFFSET+18)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG74_OFFSET+17 downto C_STAT_REG74_OFFSET+0)       <= MAX_TDC_PKT_SAMPLES_I;
  MAX_TDC_PKT_SAMPLES_BIT_TRIG_O                                        <= STAT_BIT_TRIG_I(C_STAT_REG74_OFFSET+17 downto C_STAT_REG74_OFFSET+0);
  MAX_TDC_PKT_SAMPLES_O                                                 <= STATUS_REG_I(C_STAT_REG74_OFFSET+17 downto C_STAT_REG74_OFFSET+0);

  -- Status Register 75 [0x012C]: MAX_TRG_PKT_SAMPLES
  MAX_TRG_PKT_SAMPLES_BYTE_TRIG_O                                       <= STAT_BYTE_TRIG_I((C_STAT_REG75_OFFSET/8)+3 downto C_STAT_REG75_OFFSET/8);
  MAX_TRG_PKT_SAMPLES_REG_READ_O                                        <= STAT_REG_READ_I(C_STAT_REG75_OFFSET/CGN_BITS_PER_REG);
  MAX_TRG_PKT_SAMPLES_REG_WRITE_O                                       <= STAT_REG_WRITE_I(C_STAT_REG75_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG75_OFFSET+31 downto C_STAT_REG75_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG75_OFFSET+15 downto C_STAT_REG75_OFFSET+0)       <= MAX_TRG_PKT_SAMPLES_I;
  MAX_TRG_PKT_SAMPLES_BIT_TRIG_O                                        <= STAT_BIT_TRIG_I(C_STAT_REG75_OFFSET+15 downto C_STAT_REG75_OFFSET+0);
  MAX_TRG_PKT_SAMPLES_O                                                 <= STATUS_REG_I(C_STAT_REG75_OFFSET+15 downto C_STAT_REG75_OFFSET+0);

  -- Status Register 76 [0x0130]: MAX_SCL_PKT_SAMPLES
  MAX_SCL_PKT_SAMPLES_BYTE_TRIG_O                                       <= STAT_BYTE_TRIG_I((C_STAT_REG76_OFFSET/8)+3 downto C_STAT_REG76_OFFSET/8);
  MAX_SCL_PKT_SAMPLES_REG_READ_O                                        <= STAT_REG_READ_I(C_STAT_REG76_OFFSET/CGN_BITS_PER_REG);
  MAX_SCL_PKT_SAMPLES_REG_WRITE_O                                       <= STAT_REG_WRITE_I(C_STAT_REG76_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG76_OFFSET+31 downto C_STAT_REG76_OFFSET+16)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG76_OFFSET+15 downto C_STAT_REG76_OFFSET+0)       <= MAX_SCL_PKT_SAMPLES_I;
  MAX_SCL_PKT_SAMPLES_BIT_TRIG_O                                        <= STAT_BIT_TRIG_I(C_STAT_REG76_OFFSET+15 downto C_STAT_REG76_OFFSET+0);
  MAX_SCL_PKT_SAMPLES_O                                                 <= STATUS_REG_I(C_STAT_REG76_OFFSET+15 downto C_STAT_REG76_OFFSET+0);

  -- Status Register 77 [0x0134]: CLK_CTRL_MOD_FLAG
  CLK_CTRL_MOD_FLAG_BYTE_TRIG_O                                         <= STAT_BYTE_TRIG_I((C_STAT_REG77_OFFSET/8)+3 downto C_STAT_REG77_OFFSET/8);
  CLK_CTRL_MOD_FLAG_REG_READ_O                                          <= STAT_REG_READ_I(C_STAT_REG77_OFFSET/CGN_BITS_PER_REG);
  CLK_CTRL_MOD_FLAG_REG_WRITE_O                                         <= STAT_REG_WRITE_I(C_STAT_REG77_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG77_OFFSET+31 downto C_STAT_REG77_OFFSET+5)       <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG77_OFFSET+4)                                     <= TRIGGER_DAQ_CLK_CAL_MOD_I;
  TRIGGER_DAQ_CLK_CAL_MOD_BIT_TRIG_O                                    <= STAT_BIT_TRIG_I(C_STAT_REG77_OFFSET+4);
  TRIGGER_DAQ_CLK_CAL_MOD_O                                             <= STATUS_REG_I(C_STAT_REG77_OFFSET+4);
  status_reg(C_STAT_REG77_OFFSET+3)                                     <= ADC_RST_MOD_I;
  ADC_RST_MOD_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG77_OFFSET+3);
  ADC_RST_MOD_O                                                         <= STATUS_REG_I(C_STAT_REG77_OFFSET+3);
  status_reg(C_STAT_REG77_OFFSET+2)                                     <= CLK_SEL_AND_DRS_CLK_DIV_MOD_I;
  CLK_SEL_AND_DRS_CLK_DIV_MOD_BIT_TRIG_O                                <= STAT_BIT_TRIG_I(C_STAT_REG77_OFFSET+2);
  CLK_SEL_AND_DRS_CLK_DIV_MOD_O                                         <= STATUS_REG_I(C_STAT_REG77_OFFSET+2);
  status_reg(C_STAT_REG77_OFFSET+1)                                     <= EXT_CLK_FREQ_MOD_I;
  EXT_CLK_FREQ_MOD_BIT_TRIG_O                                           <= STAT_BIT_TRIG_I(C_STAT_REG77_OFFSET+1);
  EXT_CLK_FREQ_MOD_O                                                    <= STATUS_REG_I(C_STAT_REG77_OFFSET+1);
  status_reg(C_STAT_REG77_OFFSET+0)                                     <= LOCAL_CLK_FREQ_MOD_I;
  LOCAL_CLK_FREQ_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG77_OFFSET+0);
  LOCAL_CLK_FREQ_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG77_OFFSET+0);

  -- Status Register 78 [0x0138]: DRS_MOD_FLAG
  DRS_MOD_FLAG_BYTE_TRIG_O                                              <= STAT_BYTE_TRIG_I((C_STAT_REG78_OFFSET/8)+3 downto C_STAT_REG78_OFFSET/8);
  DRS_MOD_FLAG_REG_READ_O                                               <= STAT_REG_READ_I(C_STAT_REG78_OFFSET/CGN_BITS_PER_REG);
  DRS_MOD_FLAG_REG_WRITE_O                                              <= STAT_REG_WRITE_I(C_STAT_REG78_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG78_OFFSET+31 downto C_STAT_REG78_OFFSET+3)       <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG78_OFFSET+2)                                     <= DRS_CTRL_MOD_I;
  DRS_CTRL_MOD_BIT_TRIG_O                                               <= STAT_BIT_TRIG_I(C_STAT_REG78_OFFSET+2);
  DRS_CTRL_MOD_O                                                        <= STATUS_REG_I(C_STAT_REG78_OFFSET+2);
  status_reg(C_STAT_REG78_OFFSET+1)                                     <= DRS_WSR_MOD_I;
  DRS_WSR_MOD_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG78_OFFSET+1);
  DRS_WSR_MOD_O                                                         <= STATUS_REG_I(C_STAT_REG78_OFFSET+1);
  status_reg(C_STAT_REG78_OFFSET+0)                                     <= DRS_WCR_MOD_I;
  DRS_WCR_MOD_BIT_TRIG_O                                                <= STAT_BIT_TRIG_I(C_STAT_REG78_OFFSET+0);
  DRS_WCR_MOD_O                                                         <= STATUS_REG_I(C_STAT_REG78_OFFSET+0);

  -- Status Register 79 [0x013C]: COM_PLD_SIZE_MOD_FLAG
  COM_PLD_SIZE_MOD_FLAG_BYTE_TRIG_O                                     <= STAT_BYTE_TRIG_I((C_STAT_REG79_OFFSET/8)+3 downto C_STAT_REG79_OFFSET/8);
  COM_PLD_SIZE_MOD_FLAG_REG_READ_O                                      <= STAT_REG_READ_I(C_STAT_REG79_OFFSET/CGN_BITS_PER_REG);
  COM_PLD_SIZE_MOD_FLAG_REG_WRITE_O                                     <= STAT_REG_WRITE_I(C_STAT_REG79_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG79_OFFSET+31 downto C_STAT_REG79_OFFSET+4)       <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG79_OFFSET+3 downto C_STAT_REG79_OFFSET+0)        <= COM_PLD_SIZE_MOD_I;
  COM_PLD_SIZE_MOD_BIT_TRIG_O                                           <= STAT_BIT_TRIG_I(C_STAT_REG79_OFFSET+3 downto C_STAT_REG79_OFFSET+0);
  COM_PLD_SIZE_MOD_O                                                    <= STATUS_REG_I(C_STAT_REG79_OFFSET+3 downto C_STAT_REG79_OFFSET+0);

  -- Status Register 80 [0x0140]: ADC_SAMPLE_DIV_MOD_FLAG
  ADC_SAMPLE_DIV_MOD_FLAG_BYTE_TRIG_O                                   <= STAT_BYTE_TRIG_I((C_STAT_REG80_OFFSET/8)+3 downto C_STAT_REG80_OFFSET/8);
  ADC_SAMPLE_DIV_MOD_FLAG_REG_READ_O                                    <= STAT_REG_READ_I(C_STAT_REG80_OFFSET/CGN_BITS_PER_REG);
  ADC_SAMPLE_DIV_MOD_FLAG_REG_WRITE_O                                   <= STAT_REG_WRITE_I(C_STAT_REG80_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG80_OFFSET+31 downto C_STAT_REG80_OFFSET+4)       <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG80_OFFSET+3 downto C_STAT_REG80_OFFSET+0)        <= ADC_SAMPLE_DIV_MOD_I;
  ADC_SAMPLE_DIV_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG80_OFFSET+3 downto C_STAT_REG80_OFFSET+0);
  ADC_SAMPLE_DIV_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG80_OFFSET+3 downto C_STAT_REG80_OFFSET+0);

  -- Status Register 81 [0x0144]: DAC_0_1_MOD_FLAG
  DAC_0_1_MOD_FLAG_BYTE_TRIG_O                                          <= STAT_BYTE_TRIG_I((C_STAT_REG81_OFFSET/8)+3 downto C_STAT_REG81_OFFSET/8);
  DAC_0_1_MOD_FLAG_REG_READ_O                                           <= STAT_REG_READ_I(C_STAT_REG81_OFFSET/CGN_BITS_PER_REG);
  DAC_0_1_MOD_FLAG_REG_WRITE_O                                          <= STAT_REG_WRITE_I(C_STAT_REG81_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG81_OFFSET+31 downto C_STAT_REG81_OFFSET+30)      <= DAC0_A_MOD_I;
  DAC0_A_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+31 downto C_STAT_REG81_OFFSET+30);
  DAC0_A_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+31 downto C_STAT_REG81_OFFSET+30);
  status_reg(C_STAT_REG81_OFFSET+29 downto C_STAT_REG81_OFFSET+28)      <= DAC0_B_MOD_I;
  DAC0_B_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+29 downto C_STAT_REG81_OFFSET+28);
  DAC0_B_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+29 downto C_STAT_REG81_OFFSET+28);
  status_reg(C_STAT_REG81_OFFSET+27 downto C_STAT_REG81_OFFSET+26)      <= DAC0_C_MOD_I;
  DAC0_C_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+27 downto C_STAT_REG81_OFFSET+26);
  DAC0_C_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+27 downto C_STAT_REG81_OFFSET+26);
  status_reg(C_STAT_REG81_OFFSET+25 downto C_STAT_REG81_OFFSET+24)      <= DAC0_D_MOD_I;
  DAC0_D_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+25 downto C_STAT_REG81_OFFSET+24);
  DAC0_D_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+25 downto C_STAT_REG81_OFFSET+24);
  status_reg(C_STAT_REG81_OFFSET+23 downto C_STAT_REG81_OFFSET+22)      <= DAC0_E_MOD_I;
  DAC0_E_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+23 downto C_STAT_REG81_OFFSET+22);
  DAC0_E_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+23 downto C_STAT_REG81_OFFSET+22);
  status_reg(C_STAT_REG81_OFFSET+21 downto C_STAT_REG81_OFFSET+20)      <= DAC0_F_MOD_I;
  DAC0_F_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+21 downto C_STAT_REG81_OFFSET+20);
  DAC0_F_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+21 downto C_STAT_REG81_OFFSET+20);
  status_reg(C_STAT_REG81_OFFSET+19 downto C_STAT_REG81_OFFSET+18)      <= DAC0_G_MOD_I;
  DAC0_G_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+19 downto C_STAT_REG81_OFFSET+18);
  DAC0_G_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+19 downto C_STAT_REG81_OFFSET+18);
  status_reg(C_STAT_REG81_OFFSET+17 downto C_STAT_REG81_OFFSET+16)      <= DAC0_H_MOD_I;
  DAC0_H_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+17 downto C_STAT_REG81_OFFSET+16);
  DAC0_H_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+17 downto C_STAT_REG81_OFFSET+16);
  status_reg(C_STAT_REG81_OFFSET+15 downto C_STAT_REG81_OFFSET+14)      <= DAC1_A_MOD_I;
  DAC1_A_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+15 downto C_STAT_REG81_OFFSET+14);
  DAC1_A_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+15 downto C_STAT_REG81_OFFSET+14);
  status_reg(C_STAT_REG81_OFFSET+13 downto C_STAT_REG81_OFFSET+12)      <= DAC1_B_MOD_I;
  DAC1_B_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+13 downto C_STAT_REG81_OFFSET+12);
  DAC1_B_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+13 downto C_STAT_REG81_OFFSET+12);
  status_reg(C_STAT_REG81_OFFSET+11 downto C_STAT_REG81_OFFSET+10)      <= DAC1_C_MOD_I;
  DAC1_C_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+11 downto C_STAT_REG81_OFFSET+10);
  DAC1_C_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+11 downto C_STAT_REG81_OFFSET+10);
  status_reg(C_STAT_REG81_OFFSET+9 downto C_STAT_REG81_OFFSET+8)        <= DAC1_D_MOD_I;
  DAC1_D_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+9 downto C_STAT_REG81_OFFSET+8);
  DAC1_D_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+9 downto C_STAT_REG81_OFFSET+8);
  status_reg(C_STAT_REG81_OFFSET+7 downto C_STAT_REG81_OFFSET+6)        <= DAC1_E_MOD_I;
  DAC1_E_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+7 downto C_STAT_REG81_OFFSET+6);
  DAC1_E_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+7 downto C_STAT_REG81_OFFSET+6);
  status_reg(C_STAT_REG81_OFFSET+5 downto C_STAT_REG81_OFFSET+4)        <= DAC1_F_MOD_I;
  DAC1_F_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+5 downto C_STAT_REG81_OFFSET+4);
  DAC1_F_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+5 downto C_STAT_REG81_OFFSET+4);
  status_reg(C_STAT_REG81_OFFSET+3 downto C_STAT_REG81_OFFSET+2)        <= DAC1_G_MOD_I;
  DAC1_G_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+3 downto C_STAT_REG81_OFFSET+2);
  DAC1_G_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+3 downto C_STAT_REG81_OFFSET+2);
  status_reg(C_STAT_REG81_OFFSET+1 downto C_STAT_REG81_OFFSET+0)        <= DAC1_H_MOD_I;
  DAC1_H_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG81_OFFSET+1 downto C_STAT_REG81_OFFSET+0);
  DAC1_H_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG81_OFFSET+1 downto C_STAT_REG81_OFFSET+0);

  -- Status Register 82 [0x0148]: DAC_2_MOD_FLAG
  DAC_2_MOD_FLAG_BYTE_TRIG_O                                            <= STAT_BYTE_TRIG_I((C_STAT_REG82_OFFSET/8)+3 downto C_STAT_REG82_OFFSET/8);
  DAC_2_MOD_FLAG_REG_READ_O                                             <= STAT_REG_READ_I(C_STAT_REG82_OFFSET/CGN_BITS_PER_REG);
  DAC_2_MOD_FLAG_REG_WRITE_O                                            <= STAT_REG_WRITE_I(C_STAT_REG82_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG82_OFFSET+31 downto C_STAT_REG82_OFFSET+30)      <= DAC2_A_MOD_I;
  DAC2_A_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+31 downto C_STAT_REG82_OFFSET+30);
  DAC2_A_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+31 downto C_STAT_REG82_OFFSET+30);
  status_reg(C_STAT_REG82_OFFSET+29 downto C_STAT_REG82_OFFSET+28)      <= DAC2_B_MOD_I;
  DAC2_B_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+29 downto C_STAT_REG82_OFFSET+28);
  DAC2_B_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+29 downto C_STAT_REG82_OFFSET+28);
  status_reg(C_STAT_REG82_OFFSET+27 downto C_STAT_REG82_OFFSET+26)      <= DAC2_C_MOD_I;
  DAC2_C_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+27 downto C_STAT_REG82_OFFSET+26);
  DAC2_C_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+27 downto C_STAT_REG82_OFFSET+26);
  status_reg(C_STAT_REG82_OFFSET+25 downto C_STAT_REG82_OFFSET+24)      <= DAC2_D_MOD_I;
  DAC2_D_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+25 downto C_STAT_REG82_OFFSET+24);
  DAC2_D_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+25 downto C_STAT_REG82_OFFSET+24);
  status_reg(C_STAT_REG82_OFFSET+23 downto C_STAT_REG82_OFFSET+22)      <= DAC2_E_MOD_I;
  DAC2_E_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+23 downto C_STAT_REG82_OFFSET+22);
  DAC2_E_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+23 downto C_STAT_REG82_OFFSET+22);
  status_reg(C_STAT_REG82_OFFSET+21 downto C_STAT_REG82_OFFSET+20)      <= DAC2_F_MOD_I;
  DAC2_F_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+21 downto C_STAT_REG82_OFFSET+20);
  DAC2_F_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+21 downto C_STAT_REG82_OFFSET+20);
  status_reg(C_STAT_REG82_OFFSET+19 downto C_STAT_REG82_OFFSET+18)      <= DAC2_G_MOD_I;
  DAC2_G_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+19 downto C_STAT_REG82_OFFSET+18);
  DAC2_G_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+19 downto C_STAT_REG82_OFFSET+18);
  status_reg(C_STAT_REG82_OFFSET+17 downto C_STAT_REG82_OFFSET+16)      <= DAC2_H_MOD_I;
  DAC2_H_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG82_OFFSET+17 downto C_STAT_REG82_OFFSET+16);
  DAC2_H_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG82_OFFSET+17 downto C_STAT_REG82_OFFSET+16);
  status_reg(C_STAT_REG82_OFFSET+15 downto C_STAT_REG82_OFFSET+0)       <= (others=>'0'); -- reserved

  -- Status Register 83 [0x014C]: FE_0_15_MOD_FLAG
  FE_0_15_MOD_FLAG_BYTE_TRIG_O                                          <= STAT_BYTE_TRIG_I((C_STAT_REG83_OFFSET/8)+3 downto C_STAT_REG83_OFFSET/8);
  FE_0_15_MOD_FLAG_REG_READ_O                                           <= STAT_REG_READ_I(C_STAT_REG83_OFFSET/CGN_BITS_PER_REG);
  FE_0_15_MOD_FLAG_REG_WRITE_O                                          <= STAT_REG_WRITE_I(C_STAT_REG83_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG83_OFFSET+31 downto C_STAT_REG83_OFFSET+30)      <= FE_0_MOD_I;
  FE_0_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+31 downto C_STAT_REG83_OFFSET+30);
  FE_0_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+31 downto C_STAT_REG83_OFFSET+30);
  status_reg(C_STAT_REG83_OFFSET+29 downto C_STAT_REG83_OFFSET+28)      <= FE_1_MOD_I;
  FE_1_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+29 downto C_STAT_REG83_OFFSET+28);
  FE_1_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+29 downto C_STAT_REG83_OFFSET+28);
  status_reg(C_STAT_REG83_OFFSET+27 downto C_STAT_REG83_OFFSET+26)      <= FE_2_MOD_I;
  FE_2_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+27 downto C_STAT_REG83_OFFSET+26);
  FE_2_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+27 downto C_STAT_REG83_OFFSET+26);
  status_reg(C_STAT_REG83_OFFSET+25 downto C_STAT_REG83_OFFSET+24)      <= FE_3_MOD_I;
  FE_3_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+25 downto C_STAT_REG83_OFFSET+24);
  FE_3_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+25 downto C_STAT_REG83_OFFSET+24);
  status_reg(C_STAT_REG83_OFFSET+23 downto C_STAT_REG83_OFFSET+22)      <= FE_4_MOD_I;
  FE_4_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+23 downto C_STAT_REG83_OFFSET+22);
  FE_4_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+23 downto C_STAT_REG83_OFFSET+22);
  status_reg(C_STAT_REG83_OFFSET+21 downto C_STAT_REG83_OFFSET+20)      <= FE_5_MOD_I;
  FE_5_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+21 downto C_STAT_REG83_OFFSET+20);
  FE_5_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+21 downto C_STAT_REG83_OFFSET+20);
  status_reg(C_STAT_REG83_OFFSET+19 downto C_STAT_REG83_OFFSET+18)      <= FE_6_MOD_I;
  FE_6_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+19 downto C_STAT_REG83_OFFSET+18);
  FE_6_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+19 downto C_STAT_REG83_OFFSET+18);
  status_reg(C_STAT_REG83_OFFSET+17 downto C_STAT_REG83_OFFSET+16)      <= FE_7_MOD_I;
  FE_7_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+17 downto C_STAT_REG83_OFFSET+16);
  FE_7_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+17 downto C_STAT_REG83_OFFSET+16);
  status_reg(C_STAT_REG83_OFFSET+15 downto C_STAT_REG83_OFFSET+14)      <= FE_8_MOD_I;
  FE_8_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+15 downto C_STAT_REG83_OFFSET+14);
  FE_8_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+15 downto C_STAT_REG83_OFFSET+14);
  status_reg(C_STAT_REG83_OFFSET+13 downto C_STAT_REG83_OFFSET+12)      <= FE_9_MOD_I;
  FE_9_MOD_BIT_TRIG_O                                                   <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+13 downto C_STAT_REG83_OFFSET+12);
  FE_9_MOD_O                                                            <= STATUS_REG_I(C_STAT_REG83_OFFSET+13 downto C_STAT_REG83_OFFSET+12);
  status_reg(C_STAT_REG83_OFFSET+11 downto C_STAT_REG83_OFFSET+10)      <= FE_10_MOD_I;
  FE_10_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+11 downto C_STAT_REG83_OFFSET+10);
  FE_10_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG83_OFFSET+11 downto C_STAT_REG83_OFFSET+10);
  status_reg(C_STAT_REG83_OFFSET+9 downto C_STAT_REG83_OFFSET+8)        <= FE_11_MOD_I;
  FE_11_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+9 downto C_STAT_REG83_OFFSET+8);
  FE_11_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG83_OFFSET+9 downto C_STAT_REG83_OFFSET+8);
  status_reg(C_STAT_REG83_OFFSET+7 downto C_STAT_REG83_OFFSET+6)        <= FE_12_MOD_I;
  FE_12_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+7 downto C_STAT_REG83_OFFSET+6);
  FE_12_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG83_OFFSET+7 downto C_STAT_REG83_OFFSET+6);
  status_reg(C_STAT_REG83_OFFSET+5 downto C_STAT_REG83_OFFSET+4)        <= FE_13_MOD_I;
  FE_13_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+5 downto C_STAT_REG83_OFFSET+4);
  FE_13_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG83_OFFSET+5 downto C_STAT_REG83_OFFSET+4);
  status_reg(C_STAT_REG83_OFFSET+3 downto C_STAT_REG83_OFFSET+2)        <= FE_14_MOD_I;
  FE_14_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+3 downto C_STAT_REG83_OFFSET+2);
  FE_14_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG83_OFFSET+3 downto C_STAT_REG83_OFFSET+2);
  status_reg(C_STAT_REG83_OFFSET+1 downto C_STAT_REG83_OFFSET+0)        <= FE_15_MOD_I;
  FE_15_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG83_OFFSET+1 downto C_STAT_REG83_OFFSET+0);
  FE_15_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG83_OFFSET+1 downto C_STAT_REG83_OFFSET+0);

  -- Status Register 84 [0x0150]: HV_U_TARGET_0_7_MOD_FLAG
  HV_U_TARGET_0_7_MOD_FLAG_BYTE_TRIG_O                                  <= STAT_BYTE_TRIG_I((C_STAT_REG84_OFFSET/8)+3 downto C_STAT_REG84_OFFSET/8);
  HV_U_TARGET_0_7_MOD_FLAG_REG_READ_O                                   <= STAT_REG_READ_I(C_STAT_REG84_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_0_7_MOD_FLAG_REG_WRITE_O                                  <= STAT_REG_WRITE_I(C_STAT_REG84_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG84_OFFSET+31 downto C_STAT_REG84_OFFSET+28)      <= HV_U_TARGET_0_MOD_I;
  HV_U_TARGET_0_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+31 downto C_STAT_REG84_OFFSET+28);
  HV_U_TARGET_0_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+31 downto C_STAT_REG84_OFFSET+28);
  status_reg(C_STAT_REG84_OFFSET+27 downto C_STAT_REG84_OFFSET+24)      <= HV_U_TARGET_1_MOD_I;
  HV_U_TARGET_1_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+27 downto C_STAT_REG84_OFFSET+24);
  HV_U_TARGET_1_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+27 downto C_STAT_REG84_OFFSET+24);
  status_reg(C_STAT_REG84_OFFSET+23 downto C_STAT_REG84_OFFSET+20)      <= HV_U_TARGET_2_MOD_I;
  HV_U_TARGET_2_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+23 downto C_STAT_REG84_OFFSET+20);
  HV_U_TARGET_2_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+23 downto C_STAT_REG84_OFFSET+20);
  status_reg(C_STAT_REG84_OFFSET+19 downto C_STAT_REG84_OFFSET+16)      <= HV_U_TARGET_3_MOD_I;
  HV_U_TARGET_3_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+19 downto C_STAT_REG84_OFFSET+16);
  HV_U_TARGET_3_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+19 downto C_STAT_REG84_OFFSET+16);
  status_reg(C_STAT_REG84_OFFSET+15 downto C_STAT_REG84_OFFSET+12)      <= HV_U_TARGET_4_MOD_I;
  HV_U_TARGET_4_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+15 downto C_STAT_REG84_OFFSET+12);
  HV_U_TARGET_4_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+15 downto C_STAT_REG84_OFFSET+12);
  status_reg(C_STAT_REG84_OFFSET+11 downto C_STAT_REG84_OFFSET+8)       <= HV_U_TARGET_5_MOD_I;
  HV_U_TARGET_5_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+11 downto C_STAT_REG84_OFFSET+8);
  HV_U_TARGET_5_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+11 downto C_STAT_REG84_OFFSET+8);
  status_reg(C_STAT_REG84_OFFSET+7 downto C_STAT_REG84_OFFSET+4)        <= HV_U_TARGET_6_MOD_I;
  HV_U_TARGET_6_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+7 downto C_STAT_REG84_OFFSET+4);
  HV_U_TARGET_6_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+7 downto C_STAT_REG84_OFFSET+4);
  status_reg(C_STAT_REG84_OFFSET+3 downto C_STAT_REG84_OFFSET+0)        <= HV_U_TARGET_7_MOD_I;
  HV_U_TARGET_7_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG84_OFFSET+3 downto C_STAT_REG84_OFFSET+0);
  HV_U_TARGET_7_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG84_OFFSET+3 downto C_STAT_REG84_OFFSET+0);

  -- Status Register 85 [0x0154]: HV_U_TARGET_8_15_MOD_FLAG
  HV_U_TARGET_8_15_MOD_FLAG_BYTE_TRIG_O                                 <= STAT_BYTE_TRIG_I((C_STAT_REG85_OFFSET/8)+3 downto C_STAT_REG85_OFFSET/8);
  HV_U_TARGET_8_15_MOD_FLAG_REG_READ_O                                  <= STAT_REG_READ_I(C_STAT_REG85_OFFSET/CGN_BITS_PER_REG);
  HV_U_TARGET_8_15_MOD_FLAG_REG_WRITE_O                                 <= STAT_REG_WRITE_I(C_STAT_REG85_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG85_OFFSET+31 downto C_STAT_REG85_OFFSET+28)      <= HV_U_TARGET_8_MOD_I;
  HV_U_TARGET_8_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+31 downto C_STAT_REG85_OFFSET+28);
  HV_U_TARGET_8_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG85_OFFSET+31 downto C_STAT_REG85_OFFSET+28);
  status_reg(C_STAT_REG85_OFFSET+27 downto C_STAT_REG85_OFFSET+24)      <= HV_U_TARGET_9_MOD_I;
  HV_U_TARGET_9_MOD_BIT_TRIG_O                                          <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+27 downto C_STAT_REG85_OFFSET+24);
  HV_U_TARGET_9_MOD_O                                                   <= STATUS_REG_I(C_STAT_REG85_OFFSET+27 downto C_STAT_REG85_OFFSET+24);
  status_reg(C_STAT_REG85_OFFSET+23 downto C_STAT_REG85_OFFSET+20)      <= HV_U_TARGET_10_MOD_I;
  HV_U_TARGET_10_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+23 downto C_STAT_REG85_OFFSET+20);
  HV_U_TARGET_10_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG85_OFFSET+23 downto C_STAT_REG85_OFFSET+20);
  status_reg(C_STAT_REG85_OFFSET+19 downto C_STAT_REG85_OFFSET+16)      <= HV_U_TARGET_11_MOD_I;
  HV_U_TARGET_11_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+19 downto C_STAT_REG85_OFFSET+16);
  HV_U_TARGET_11_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG85_OFFSET+19 downto C_STAT_REG85_OFFSET+16);
  status_reg(C_STAT_REG85_OFFSET+15 downto C_STAT_REG85_OFFSET+12)      <= HV_U_TARGET_12_MOD_I;
  HV_U_TARGET_12_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+15 downto C_STAT_REG85_OFFSET+12);
  HV_U_TARGET_12_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG85_OFFSET+15 downto C_STAT_REG85_OFFSET+12);
  status_reg(C_STAT_REG85_OFFSET+11 downto C_STAT_REG85_OFFSET+8)       <= HV_U_TARGET_13_MOD_I;
  HV_U_TARGET_13_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+11 downto C_STAT_REG85_OFFSET+8);
  HV_U_TARGET_13_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG85_OFFSET+11 downto C_STAT_REG85_OFFSET+8);
  status_reg(C_STAT_REG85_OFFSET+7 downto C_STAT_REG85_OFFSET+4)        <= HV_U_TARGET_14_MOD_I;
  HV_U_TARGET_14_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+7 downto C_STAT_REG85_OFFSET+4);
  HV_U_TARGET_14_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG85_OFFSET+7 downto C_STAT_REG85_OFFSET+4);
  status_reg(C_STAT_REG85_OFFSET+3 downto C_STAT_REG85_OFFSET+0)        <= HV_U_TARGET_15_MOD_I;
  HV_U_TARGET_15_MOD_BIT_TRIG_O                                         <= STAT_BIT_TRIG_I(C_STAT_REG85_OFFSET+3 downto C_STAT_REG85_OFFSET+0);
  HV_U_TARGET_15_MOD_O                                                  <= STATUS_REG_I(C_STAT_REG85_OFFSET+3 downto C_STAT_REG85_OFFSET+0);

  -- Status Register 86 [0x0158]: HV_MOD_FLAG
  HV_MOD_FLAG_BYTE_TRIG_O                                               <= STAT_BYTE_TRIG_I((C_STAT_REG86_OFFSET/8)+3 downto C_STAT_REG86_OFFSET/8);
  HV_MOD_FLAG_REG_READ_O                                                <= STAT_REG_READ_I(C_STAT_REG86_OFFSET/CGN_BITS_PER_REG);
  HV_MOD_FLAG_REG_WRITE_O                                               <= STAT_REG_WRITE_I(C_STAT_REG86_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG86_OFFSET+31 downto C_STAT_REG86_OFFSET+4)       <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG86_OFFSET+3 downto C_STAT_REG86_OFFSET+0)        <= HV_R_SHUNT_MOD_I;
  HV_R_SHUNT_MOD_BIT_TRIG_O                                             <= STAT_BIT_TRIG_I(C_STAT_REG86_OFFSET+3 downto C_STAT_REG86_OFFSET+0);
  HV_R_SHUNT_MOD_O                                                      <= STATUS_REG_I(C_STAT_REG86_OFFSET+3 downto C_STAT_REG86_OFFSET+0);

  -- Status Register 87 [0x015C]: LMK_0_7_MOD_FLAG
  LMK_0_7_MOD_FLAG_BYTE_TRIG_O                                          <= STAT_BYTE_TRIG_I((C_STAT_REG87_OFFSET/8)+3 downto C_STAT_REG87_OFFSET/8);
  LMK_0_7_MOD_FLAG_REG_READ_O                                           <= STAT_REG_READ_I(C_STAT_REG87_OFFSET/CGN_BITS_PER_REG);
  LMK_0_7_MOD_FLAG_REG_WRITE_O                                          <= STAT_REG_WRITE_I(C_STAT_REG87_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG87_OFFSET+31 downto C_STAT_REG87_OFFSET+28)      <= LMK_0_MOD_I;
  LMK_0_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+31 downto C_STAT_REG87_OFFSET+28);
  LMK_0_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+31 downto C_STAT_REG87_OFFSET+28);
  status_reg(C_STAT_REG87_OFFSET+27 downto C_STAT_REG87_OFFSET+24)      <= LMK_1_MOD_I;
  LMK_1_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+27 downto C_STAT_REG87_OFFSET+24);
  LMK_1_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+27 downto C_STAT_REG87_OFFSET+24);
  status_reg(C_STAT_REG87_OFFSET+23 downto C_STAT_REG87_OFFSET+20)      <= LMK_2_MOD_I;
  LMK_2_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+23 downto C_STAT_REG87_OFFSET+20);
  LMK_2_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+23 downto C_STAT_REG87_OFFSET+20);
  status_reg(C_STAT_REG87_OFFSET+19 downto C_STAT_REG87_OFFSET+16)      <= LMK_3_MOD_I;
  LMK_3_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+19 downto C_STAT_REG87_OFFSET+16);
  LMK_3_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+19 downto C_STAT_REG87_OFFSET+16);
  status_reg(C_STAT_REG87_OFFSET+15 downto C_STAT_REG87_OFFSET+12)      <= LMK_4_MOD_I;
  LMK_4_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+15 downto C_STAT_REG87_OFFSET+12);
  LMK_4_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+15 downto C_STAT_REG87_OFFSET+12);
  status_reg(C_STAT_REG87_OFFSET+11 downto C_STAT_REG87_OFFSET+8)       <= LMK_5_MOD_I;
  LMK_5_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+11 downto C_STAT_REG87_OFFSET+8);
  LMK_5_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+11 downto C_STAT_REG87_OFFSET+8);
  status_reg(C_STAT_REG87_OFFSET+7 downto C_STAT_REG87_OFFSET+4)        <= LMK_6_MOD_I;
  LMK_6_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+7 downto C_STAT_REG87_OFFSET+4);
  LMK_6_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+7 downto C_STAT_REG87_OFFSET+4);
  status_reg(C_STAT_REG87_OFFSET+3 downto C_STAT_REG87_OFFSET+0)        <= LMK_7_MOD_I;
  LMK_7_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG87_OFFSET+3 downto C_STAT_REG87_OFFSET+0);
  LMK_7_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG87_OFFSET+3 downto C_STAT_REG87_OFFSET+0);

  -- Status Register 88 [0x0160]: LMK_8_15_MOD_FLAG
  LMK_8_15_MOD_FLAG_BYTE_TRIG_O                                         <= STAT_BYTE_TRIG_I((C_STAT_REG88_OFFSET/8)+3 downto C_STAT_REG88_OFFSET/8);
  LMK_8_15_MOD_FLAG_REG_READ_O                                          <= STAT_REG_READ_I(C_STAT_REG88_OFFSET/CGN_BITS_PER_REG);
  LMK_8_15_MOD_FLAG_REG_WRITE_O                                         <= STAT_REG_WRITE_I(C_STAT_REG88_OFFSET/CGN_BITS_PER_REG);
  status_reg(C_STAT_REG88_OFFSET+31 downto C_STAT_REG88_OFFSET+24)      <= (others=>'0'); -- reserved
  status_reg(C_STAT_REG88_OFFSET+23 downto C_STAT_REG88_OFFSET+20)      <= LMK_8_MOD_I;
  LMK_8_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG88_OFFSET+23 downto C_STAT_REG88_OFFSET+20);
  LMK_8_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG88_OFFSET+23 downto C_STAT_REG88_OFFSET+20);
  status_reg(C_STAT_REG88_OFFSET+19 downto C_STAT_REG88_OFFSET+16)      <= LMK_9_MOD_I;
  LMK_9_MOD_BIT_TRIG_O                                                  <= STAT_BIT_TRIG_I(C_STAT_REG88_OFFSET+19 downto C_STAT_REG88_OFFSET+16);
  LMK_9_MOD_O                                                           <= STATUS_REG_I(C_STAT_REG88_OFFSET+19 downto C_STAT_REG88_OFFSET+16);
  status_reg(C_STAT_REG88_OFFSET+15 downto C_STAT_REG88_OFFSET+12)      <= LMK_11_MOD_I;
  LMK_11_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG88_OFFSET+15 downto C_STAT_REG88_OFFSET+12);
  LMK_11_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG88_OFFSET+15 downto C_STAT_REG88_OFFSET+12);
  status_reg(C_STAT_REG88_OFFSET+11 downto C_STAT_REG88_OFFSET+8)       <= LMK_13_MOD_I;
  LMK_13_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG88_OFFSET+11 downto C_STAT_REG88_OFFSET+8);
  LMK_13_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG88_OFFSET+11 downto C_STAT_REG88_OFFSET+8);
  status_reg(C_STAT_REG88_OFFSET+7 downto C_STAT_REG88_OFFSET+4)        <= LMK_14_MOD_I;
  LMK_14_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG88_OFFSET+7 downto C_STAT_REG88_OFFSET+4);
  LMK_14_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG88_OFFSET+7 downto C_STAT_REG88_OFFSET+4);
  status_reg(C_STAT_REG88_OFFSET+3 downto C_STAT_REG88_OFFSET+0)        <= LMK_15_MOD_I;
  LMK_15_MOD_BIT_TRIG_O                                                 <= STAT_BIT_TRIG_I(C_STAT_REG88_OFFSET+3 downto C_STAT_REG88_OFFSET+0);
  LMK_15_MOD_O                                                          <= STATUS_REG_I(C_STAT_REG88_OFFSET+3 downto C_STAT_REG88_OFFSET+0);

end architecture structural;