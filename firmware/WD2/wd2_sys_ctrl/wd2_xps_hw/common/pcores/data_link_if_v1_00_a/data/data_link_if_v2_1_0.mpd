#######################################################################################
##  Paul Scherrer Institut
#######################################################################################
##
##  Unit :  Data Link Interface
##
##  Project :  WaveDream2
##
##  PCB  :  -
##  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
##
##  Tool Version :  14.7 (Version the code was testet with)
##
##  Author  :  Elmar Schmid (Author of generation script)
##  Created :  24.10.2017 14:31
##
##  Description :  .mpd file for interface between WD2 packager and Ethernet
##                 or SERDES links.
##
#######################################################################################
#######################################################################################

BEGIN data_link_if

## Peripheral Options
OPTION IPTYPE = PERIPHERAL
OPTION IMP_NETLIST = TRUE
OPTION HDL = VHDL
OPTION IP_GROUP = USER

## Bus Interfaces
BUS_INTERFACE BUS = LL32_I,       BUS_STD = SINGLE_LLINK, BUS_TYPE = TARGET
BUS_INTERFACE BUS = LL8_ETH_O,    BUS_STD = SINGLE_LLINK, BUS_TYPE = INITIATOR
BUS_INTERFACE BUS = LL8_SERDES_O, BUS_STD = SINGLE_LLINK, BUS_TYPE = INITIATOR

### Generics:
PARAMETER CGN_ETH_FRAME_LEN_WIDTH = 12, DT = INTEGER

### Ports:
PORT LL32_I_CLK_I             = LL_CLK,      DIR = I, BUS = LL32_I, INITIALVAL = GND, SIGIS = CLK
PORT LL32_I_RST_N_I           = LL_RST_N,    DIR = I, BUS = LL32_I, INITIALVAL = VCC
PORT LL32_I_DATA_I            = LL_DATA,     DIR = I, BUS = LL32_I, INITIALVAL = GND, VEC = [31:0]
PORT LL32_I_SRC_RDY_N_I       = LL_SRCRDY_N, DIR = I, BUS = LL32_I, INITIALVAL = VCC
PORT LL32_I_DST_RDY_N_O       = LL_DSTRDY_N, DIR = O, BUS = LL32_I 
PORT LL32_I_SOF_N_I           = LL_SOF_N,    DIR = I, BUS = LL32_I, INITIALVAL = VCC
PORT LL32_I_EOF_N_I           = LL_EOF_N,    DIR = I, BUS = LL32_I, INITIALVAL = VCC
PORT ETH_DST_VALID_I          = "",          DIR = I
PORT ETH_COM_EN_I             = "",          DIR = I
PORT LL8_ETH_O_CLK_I          = LL_CLK,      DIR = I, BUS = LL8_ETH_O, INITIALVAL = GND, SIGIS = CLK
PORT LL8_ETH_O_DATA_O         = LL_DATA,     DIR = O, BUS = LL8_ETH_O, VEC = [7:0]
PORT LL8_ETH_O_SRC_RDY_N_O    = LL_SRCRDY_N, DIR = O, BUS = LL8_ETH_O
PORT LL8_ETH_O_DST_RDY_N_I    = LL_DSTRDY_N, DIR = I, BUS = LL8_ETH_O, INITIALVAL = VCC
PORT LL8_ETH_O_SOF_N_O        = LL_SOF_N,    DIR = O, BUS = LL8_ETH_O
PORT LL8_ETH_O_EOF_N_O        = LL_EOF_N,    DIR = O, BUS = LL8_ETH_O
PORT SERDES_COM_EN_I          = "",          DIR = I
PORT LL8_SERDES_O_CLK_I       = LL_CLK,      DIR = I, BUS = LL8_SERDES_O, INITIALVAL = GND, SIGIS = CLK
PORT LL8_SERDES_O_DATA_O      = LL_DATA,     DIR = O, BUS = LL8_SERDES_O, VEC = [7:0]
PORT LL8_SERDES_O_SRC_RDY_N_O = LL_SRCRDY_N, DIR = O, BUS = LL8_SERDES_O
PORT LL8_SERDES_O_DST_RDY_N_I = LL_DSTRDY_N, DIR = I, BUS = LL8_SERDES_O, INITIALVAL = VCC
PORT LL8_SERDES_O_SOF_N_O     = LL_SOF_N,    DIR = O, BUS = LL8_SERDES_O
PORT LL8_SERDES_O_EOF_N_O     = LL_EOF_N,    DIR = O, BUS = LL8_SERDES_O

END
