---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Data Link Interface
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  24.10.2017 14:33
--
--  Description :  Interface between WD2 packager and Ethernet or SERDES links.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;
Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;

library data_link_if_v1_00_a;
use data_link_if_v1_00_a.data_link_ctrl;

entity data_link_if is
  generic (
    CGN_ETH_FRAME_LEN_WIDTH : integer := 12
  );
  port (
    -- Interface to packager
    LL32_I_CLK_I             : in  std_logic;
    LL32_I_RST_N_I           : in  std_logic;
    LL32_I_DATA_I            : in  std_logic_vector(31 downto 0);
    LL32_I_SRC_RDY_N_I       : in  std_logic;
    LL32_I_DST_RDY_N_O       : out std_logic;
    LL32_I_SOF_N_I           : in  std_logic;
    LL32_I_EOF_N_I           : in  std_logic;
    ETH_DST_VALID_I          : in  std_logic;
    ETH_COM_EN_I             : in  std_logic;
    SERDES_COM_EN_I          : in  std_logic;
    -- Ethernet Local Link Interface
    LL8_ETH_O_CLK_I          : in  std_logic;
    LL8_ETH_O_DATA_O         : out std_logic_vector(7 downto 0);
    LL8_ETH_O_SRC_RDY_N_O    : out std_logic;
    LL8_ETH_O_DST_RDY_N_I    : in  std_logic;
    LL8_ETH_O_SOF_N_O        : out std_logic;
    LL8_ETH_O_EOF_N_O        : out std_logic;
    -- SERDES Local Link Interface
    LL8_SERDES_O_CLK_I       : in  std_logic;
    LL8_SERDES_O_DATA_O      : out std_logic_vector(7 downto 0);
    LL8_SERDES_O_SRC_RDY_N_O : out std_logic;
    LL8_SERDES_O_DST_RDY_N_I : in  std_logic;
    LL8_SERDES_O_SOF_N_O     : out std_logic;
    LL8_SERDES_O_EOF_N_O     : out std_logic
  );
end data_link_if;

architecture behavioral of data_link_if is

  signal pkg_dst_rdy_n             : std_logic := '1';
  signal cdc_fifo_wr_rdy           : std_logic := '0';
  signal eth_cdc_fifo_wen          : std_logic := '0';
  signal eth_cdc_fifo_ren          : std_logic := '0';
  signal eth_cdc_fifo_full         : std_logic := '0';
  signal eth_cdc_fifo_empty        : std_logic := '0';
  signal eth_cdc_fifo_din          : std_logic_vector(31 downto 0) := (others=>'0');
  signal eth_cdc_fifo_dout         : std_logic_vector(31 downto 0) := (others=>'0');
  signal serdes_cdc_fifo_wen       : std_logic := '0';
  signal serdes_cdc_fifo_ren       : std_logic := '0';
  signal serdes_cdc_fifo_full      : std_logic := '0';
  signal serdes_cdc_fifo_empty     : std_logic := '0';
  signal serdes_cdc_fifo_din       : std_logic_vector(31 downto 0) := (others=>'0');
  signal serdes_cdc_fifo_dout      : std_logic_vector(31 downto 0) := (others=>'0');
  
  signal eth_pkt_fifo_din          : std_logic_vector(7 downto 0) := (others=>'0');
  signal eth_pkt_fifo_sof_n        : std_logic := '1';
  signal eth_pkt_fifo_eof_n        : std_logic := '1';
  signal eth_pkt_fifo_src_rdy_n    : std_logic := '1';
  signal eth_pkt_fifo_dst_rdy_n    : std_logic := '1';
  signal eth_pkt_fifo_pkt_ok       : std_logic := '0';
  signal eth_pkt_fifo_pkt_err      : std_logic := '0';

  signal serdes_pkt_fifo_din       : std_logic_vector(7 downto 0) := (others=>'0');
  signal serdes_pkt_fifo_sof_n     : std_logic := '1';
  signal serdes_pkt_fifo_eof_n     : std_logic := '1';
  signal serdes_pkt_fifo_src_rdy_n : std_logic := '1';
  signal serdes_pkt_fifo_dst_rdy_n : std_logic := '1';
  signal serdes_pkt_fifo_pkt_ok    : std_logic := '0';
  signal serdes_pkt_fifo_pkt_err   : std_logic := '0';

  signal rst                       : std_logic := '0';
  signal eth_rst                   : std_logic := '0';
  signal serdes_rst                : std_logic := '0';
  signal eth_rst_n                 : std_logic := '0';
  signal serdes_rst_n              : std_logic := '0';
  signal eth_com_en                : std_logic := '0';
  signal serdes_com_en             : std_logic := '0';
  signal frame_active              : std_logic := '0';

begin

  rst <= not LL32_I_RST_N_I;
  eth_rst_n    <= not eth_rst;
  serdes_rst_n <= not serdes_rst;

  sync_rst_to_eth_clk : entity psi_3205_v1_00_a.cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 1,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => LL32_I_CLK_I,
    CLK_B_I => LL8_ETH_O_CLK_I,
    -- -----------------------------------------------
    PORT_A_I(0) => rst,
    -- -----------------------------------------------
    PORT_B_O(0) => eth_rst
  );

  sync_rst_to_serdes_clk : entity psi_3205_v1_00_a.cdc_sync
  GENERIC MAP (
    CGN_DATA_WIDTH          => 1,
    CGN_USE_INPUT_REG_A     => 0,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0,
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP (
    CLK_A_I => LL32_I_CLK_I,
    CLK_B_I => LL8_SERDES_O_CLK_I,
    -- -----------------------------------------------
    PORT_A_I(0) => rst,
    -- -----------------------------------------------
    PORT_B_O(0) => serdes_rst
  );

  process(LL32_I_CLK_I)
  begin
    if rising_edge(LL32_I_CLK_I) then
      if ( (rst='1') or ((cdc_fifo_wr_rdy = '1') and (LL32_I_EOF_N_I = '0')) ) then
        frame_active <= '0';
      elsif ((cdc_fifo_wr_rdy = '1') and (LL32_I_SOF_N_I = '0')) then
        frame_active <= '1';
      end if;
    end if;
  end process;

  process(LL32_I_CLK_I)
  begin
    if rising_edge(LL32_I_CLK_I) then
      if rst = '1' then
        eth_com_en    <= '0';
        serdes_com_en <= '0';
      elsif frame_active = '0' and not((cdc_fifo_wr_rdy = '1') and (LL32_I_SOF_N_I = '0')) then
        eth_com_en    <= ETH_COM_EN_I and ETH_DST_VALID_I;
        serdes_com_en <= SERDES_COM_EN_I;
      end if;
    end if;
  end process;

  process(eth_com_en, serdes_com_en, eth_cdc_fifo_full, serdes_cdc_fifo_full)
  begin
    if eth_com_en = '1' and serdes_com_en = '1' then
      pkg_dst_rdy_n <= eth_cdc_fifo_full or serdes_cdc_fifo_full;
    elsif eth_com_en = '1' then
      pkg_dst_rdy_n <= eth_cdc_fifo_full;
    elsif serdes_com_en = '1' then
      pkg_dst_rdy_n <= serdes_cdc_fifo_full;
    else
      pkg_dst_rdy_n <= '1';
    end if;
  end process;
  LL32_I_DST_RDY_N_O   <= pkg_dst_rdy_n;

  cdc_fifo_wr_rdy      <= not(LL32_I_SRC_RDY_N_I) and not(pkg_dst_rdy_n);
  -- handle reset behavior of flags according to FIFO behavior

  --------------------------------------------------------------
  -- Ethernet Link Interface                                  --
  --------------------------------------------------------------

  eth_cdc_fifo_wen <= cdc_fifo_wr_rdy when eth_com_en = '1' else '0';
  eth_cdc_fifo_din <= LL32_I_SOF_N_I & LL32_I_EOF_N_I & LL32_I_DATA_I(29 downto 0);

  ETH_CDC_FIFO : entity psi_3205_v1_00_a.fifo_gen_async
  generic map
  (
    C_FAMILY            => "spartan6", -- "spartan6" -- "virtex5";
    C_ISE_12_4          => false,  -- true
    C_ISE_14_7          => true, -- false
    C_DATA_WIDTH        => 32,
    C_FIFO_DEPTH        => 2**4,
    C_MEMORY_TYPE       => 0,     -- 0 = distributed RAM, 1 = BRAM
    C_USE_EMBEDDED_REG  => 0,     -- Valid only for BRAM based FIFO, otherwise needs to be set to 0
    C_PRELOAD_REGS      => 1,     -- 1 = first word fall through
    C_PRELOAD_LATENCY   => 0,     -- 0 = first word fall through,  needs to be set 2 when C_USE_EMBEDDED_REG = 1
    -- write port
    C_HAS_WR_COUNT      => 0,
    C_WR_COUNT_WIDTH    => 4,
    C_HAS_ALMOST_FULL   => 0,
    C_HAS_PROG_FULL     => 0,     -- 0 or 1; 1 for constant prog full
    C_PROG_FULL_OFFSET  => 3,     -- Setting determines the difference between FULL and PROG_FULL conditions
    C_HAS_WR_ACK        => 0,
    C_WR_ACK_LOW        => 0,
    C_HAS_WR_ERR        => 0,
    C_WR_ERR_LOW        => 0,
    -- read port
    C_HAS_RD_COUNT      => 0,
    C_RD_COUNT_WIDTH    => 4,
    C_HAS_ALMOST_EMPTY  => 0,
    C_HAS_PROG_EMPTY    => 0,     -- 0 or 1; 1 for constant prog empty
    C_PROG_EMPTY_OFFSET => 2,     -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
    C_HAS_RD_ACK        => 0,
    C_RD_ACK_LOW        => 0,
    C_HAS_RD_ERR        => 0,
    C_RD_ERR_LOW        => 0
  )
  port map
  (
    -- Common
    RESET_ASYNC_I     => rst,
    -- Write Port
    WR_CLK_I          => LL32_I_CLK_I,
    WR_EN_I           => eth_cdc_fifo_wen,
    WR_DATA_I         => eth_cdc_fifo_din,
    WR_FULL_O         => eth_cdc_fifo_full,
    WR_ALMOST_FULL_O  => open,
    WR_PROG_FULL_O    => open,
    WR_COUNT_O        => open,
    WR_ACK_O          => open,
    WR_ERR_O          => open,
    -- Read Port
    RD_CLK_I          => LL8_ETH_O_CLK_I,
    RD_EN_I           => eth_cdc_fifo_ren,
    RD_DATA_O         => eth_cdc_fifo_dout,
    RD_EMPTY_O        => eth_cdc_fifo_empty,
    RD_ALMOST_EMPTY_O => open,
    RD_PROG_EMPTY_O   => open,
    RD_COUNT_O        => open,
    RD_ACK_O          => open,
    RD_ERR_O          => open
  );

  ETH_DATA_LINK_CTRL : entity data_link_if_v1_00_a.data_link_ctrl
  generic map(
    CGN_ETH_FRAME_LEN_WIDTH => CGN_ETH_FRAME_LEN_WIDTH,
    CGN_PKT_FIFO_DEPTH      => 2**CGN_ETH_FRAME_LEN_WIDTH -- 4k fifo
  )
  port map(
    -- CDC FIFO interface
    CDC_FIFO_DATA_I  => eth_cdc_fifo_dout,
    CDC_FIFO_EMPTY_I => eth_cdc_fifo_empty,
    CDC_FIFO_REN_O   => eth_cdc_fifo_ren,
    -- LL8 Packet FIFO interface
    LL8_CLK_I        => LL8_ETH_O_CLK_I,
    LL8_RST_N_I      => eth_rst_n,
    LL8_DATA_O       => eth_pkt_fifo_din,
    LL8_SRC_RDY_N_O  => eth_pkt_fifo_src_rdy_n,
    LL8_DST_RDY_N_I  => eth_pkt_fifo_dst_rdy_n,
    LL8_SOF_N_O      => eth_pkt_fifo_sof_n,
    LL8_EOF_N_O      => eth_pkt_fifo_eof_n,
    LL8_PKT_OK_O     => eth_pkt_fifo_pkt_ok,
    LL8_PKT_ERR_O    => eth_pkt_fifo_pkt_err
  );
  
  ETH_LL_PACKET_FIFO : entity psi_3205_v1_00_a.ll8_packet_fifo
  generic map
  (
    CGN_ADDR_WIDTH     => 14 -- = 4095 bytes
    -- jumbo frames suported but with a maximum of
    -- 2048 samples @ 12 bit (ADC data) = 3072 bytes in one packet
  )
  port map
  (
    CLK_I              => LL8_ETH_O_CLK_I,
    RESET_I            => eth_rst,
    -- ll us interface
    LL8_I_DATA_I       => eth_pkt_fifo_din,
    LL8_I_SOF_N_I      => eth_pkt_fifo_sof_n,
    LL8_I_EOF_N_I      => eth_pkt_fifo_eof_n,
    LL8_I_SRC_RDY_N_I  => eth_pkt_fifo_src_rdy_n,
    LL8_I_DST_RDY_N_O  => eth_pkt_fifo_dst_rdy_n,
    LL8_I_PKT_OK_I     => eth_pkt_fifo_pkt_ok,
    LL8_I_PKT_ERR_I    => eth_pkt_fifo_pkt_err,
    -- ll ds interface
    LL8_O_DATA_O       => LL8_ETH_O_DATA_O,
    LL8_O_SOF_N_O      => LL8_ETH_O_SOF_N_O,
    LL8_O_EOF_N_O      => LL8_ETH_O_EOF_N_O,
    LL8_O_SRC_RDY_N_O  => LL8_ETH_O_SRC_RDY_N_O,
    LL8_O_DST_RDY_N_I  => LL8_ETH_O_DST_RDY_N_I
  );
  --------------------------------------------------------------
  --------------------------------------------------------------
  
  --------------------------------------------------------------
  -- SERDES Link Interface                                    --
  --------------------------------------------------------------

  serdes_cdc_fifo_wen <= cdc_fifo_wr_rdy when serdes_com_en = '1' else '0';
  serdes_cdc_fifo_din <= LL32_I_SOF_N_I & LL32_I_EOF_N_I & LL32_I_DATA_I(29 downto 0);

  SERDES_CDC_FIFO : entity psi_3205_v1_00_a.fifo_gen_async
  generic map
  (
    C_FAMILY            => "spartan6", -- "spartan6" -- "virtex5";
    C_ISE_12_4          => false,  -- true
    C_ISE_14_7          => true, -- false
    C_DATA_WIDTH        => 32,
    C_FIFO_DEPTH        => 2**4,
    C_MEMORY_TYPE       => 0,     -- 0 = distributed RAM, 1 = BRAM
    C_USE_EMBEDDED_REG  => 0,     -- Valid only for BRAM based FIFO, otherwise needs to be set to 0
    C_PRELOAD_REGS      => 1,     -- 1 = first word fall through
    C_PRELOAD_LATENCY   => 0,     -- 0 = first word fall through,  needs to be set 2 when C_USE_EMBEDDED_REG = 1
    -- write port
    C_HAS_WR_COUNT      => 0,
    C_WR_COUNT_WIDTH    => 4,
    C_HAS_ALMOST_FULL   => 0,
    C_HAS_PROG_FULL     => 0,     -- 0 or 1; 1 for constant prog full
    C_PROG_FULL_OFFSET  => 3,     -- Setting determines the difference between FULL and PROG_FULL conditions
    C_HAS_WR_ACK        => 0,
    C_WR_ACK_LOW        => 0,
    C_HAS_WR_ERR        => 0,
    C_WR_ERR_LOW        => 0,
    -- read port
    C_HAS_RD_COUNT      => 0,
    C_RD_COUNT_WIDTH    => 4,
    C_HAS_ALMOST_EMPTY  => 0,
    C_HAS_PROG_EMPTY    => 0,     -- 0 or 1; 1 for constant prog empty
    C_PROG_EMPTY_OFFSET => 2,     -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
    C_HAS_RD_ACK        => 0,
    C_RD_ACK_LOW        => 0,
    C_HAS_RD_ERR        => 0,
    C_RD_ERR_LOW        => 0
  )
  port map
  (
    -- Common
    RESET_ASYNC_I     => rst,
    -- Write Port
    WR_CLK_I          => LL32_I_CLK_I,
    WR_EN_I           => serdes_cdc_fifo_wen,
    WR_DATA_I         => serdes_cdc_fifo_din,
    WR_FULL_O         => serdes_cdc_fifo_full,
    WR_ALMOST_FULL_O  => open,
    WR_PROG_FULL_O    => open,
    WR_COUNT_O        => open,
    WR_ACK_O          => open,
    WR_ERR_O          => open,
    -- Read Port
    RD_CLK_I          => LL8_SERDES_O_CLK_I,
    RD_EN_I           => serdes_cdc_fifo_ren,
    RD_DATA_O         => serdes_cdc_fifo_dout,
    RD_EMPTY_O        => serdes_cdc_fifo_empty,
    RD_ALMOST_EMPTY_O => open,
    RD_PROG_EMPTY_O   => open,
    RD_COUNT_O        => open,
    RD_ACK_O          => open,
    RD_ERR_O          => open
  );

  SERDES_DATA_LINK_CTRL : entity data_link_if_v1_00_a.data_link_ctrl
  generic map(
    CGN_PKT_FIFO_DEPTH      => 2**CGN_ETH_FRAME_LEN_WIDTH -- 4k fifo
  )
  port map(
    -- CDC FIFO interface
    CDC_FIFO_DATA_I  => serdes_cdc_fifo_dout,
    CDC_FIFO_EMPTY_I => serdes_cdc_fifo_empty,
    CDC_FIFO_REN_O   => serdes_cdc_fifo_ren,
    -- LL8 Packet FIFO interface
    LL8_CLK_I        => LL8_SERDES_O_CLK_I,
    LL8_RST_N_I      => serdes_rst_n,
    LL8_DATA_O       => serdes_pkt_fifo_din,
    LL8_SRC_RDY_N_O  => serdes_pkt_fifo_src_rdy_n,
    LL8_DST_RDY_N_I  => serdes_pkt_fifo_dst_rdy_n,
    LL8_SOF_N_O      => serdes_pkt_fifo_sof_n,
    LL8_EOF_N_O      => serdes_pkt_fifo_eof_n,
    LL8_PKT_OK_O     => serdes_pkt_fifo_pkt_ok,
    LL8_PKT_ERR_O    => serdes_pkt_fifo_pkt_err
  );
  
  SERDES_LL_PACKET_FIFO : entity psi_3205_v1_00_a.ll8_packet_fifo
  generic map
  (
    CGN_ADDR_WIDTH     => 14 -- = 4095 bytes
    -- jumbo frames suported but with a maximum of
    -- 2048 samples @ 12 bit (ADC data) = 3072 bytes in one packet
  )
  port map
  (
    CLK_I              => LL8_SERDES_O_CLK_I,
    RESET_I            => serdes_rst,
    -- ll us interface
    LL8_I_DATA_I       => serdes_pkt_fifo_din,
    LL8_I_SOF_N_I      => serdes_pkt_fifo_sof_n,
    LL8_I_EOF_N_I      => serdes_pkt_fifo_eof_n,
    LL8_I_SRC_RDY_N_I  => serdes_pkt_fifo_src_rdy_n,
    LL8_I_DST_RDY_N_O  => serdes_pkt_fifo_dst_rdy_n,
    LL8_I_PKT_OK_I     => serdes_pkt_fifo_pkt_ok,
    LL8_I_PKT_ERR_I    => serdes_pkt_fifo_pkt_err,
    -- ll ds interface
    LL8_O_DATA_O       => LL8_SERDES_O_DATA_O,
    LL8_O_SOF_N_O      => LL8_SERDES_O_SOF_N_O,
    LL8_O_EOF_N_O      => LL8_SERDES_O_EOF_N_O,
    LL8_O_SRC_RDY_N_O  => LL8_SERDES_O_SRC_RDY_N_O,
    LL8_O_DST_RDY_N_I  => LL8_SERDES_O_DST_RDY_N_I
  );
  --------------------------------------------------------------
  --------------------------------------------------------------
  
end architecture behavioral;
