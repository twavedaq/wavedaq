---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Data Link Interface
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  24.10.2017 14:33
--
--  Description :  Interface between WD2 packager and Ethernet or SERDES links.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.ALL;
use ieee.std_logic_unsigned.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity data_link_ctrl is
  generic (
    CGN_ETH_FRAME_LEN_WIDTH : integer := 12;
    CGN_PKT_FIFO_DEPTH      : integer := 2**12
  );
  port (
    -- CDC FIFO interface
    CDC_FIFO_DATA_I  : in  std_logic_vector(31 downto 0);
    CDC_FIFO_EMPTY_I : in  std_logic;
    CDC_FIFO_REN_O   : out std_logic;
    -- LL8 Packet FIFO interface
    LL8_CLK_I        : in  std_logic;
    LL8_RST_N_I      : in  std_logic;
    LL8_DATA_O       : out std_logic_vector( 7 downto 0);
    LL8_SOF_N_O      : out std_logic;
    LL8_EOF_N_O      : out std_logic;
    LL8_SRC_RDY_N_O  : out std_logic;
    LL8_DST_RDY_N_I  : in  std_logic;
    LL8_PKT_OK_O     : out std_logic;
    LL8_PKT_ERR_O    : out std_logic
  );
end data_link_ctrl;

architecture behavioral of data_link_ctrl is
  
  type sreg_type is array (2 downto 0) of std_logic_vector(7 downto 0);
  signal sreg : sreg_type := (others=>(others=>'0'));
  
  signal byte_count   : std_logic_vector(log2ceil(CGN_PKT_FIFO_DEPTH)-1 downto 0) := (others=>'0');
  signal data_len     : std_logic_vector(1 downto 0) := (others=>'0');

  signal fill_level_n : std_logic_vector(2 downto 0) := (others=>'1');
  signal eof_n_sreg   : std_logic_vector(2 downto 0) := (others=>'1');
  signal ll8_eof_n    : std_logic := '1';
  signal cdc_sof_n    : std_logic := '1';
  signal sof_n        : std_logic := '1';
  signal src_rdy_n    : std_logic := '1';

  signal sreg_load    : std_logic := '0';
  signal sreg_shift   : std_logic := '0';
  signal sreg_empty   : std_logic := '0';
  
begin

  data_len  <= CDC_FIFO_DATA_I(25 downto 24);
  cdc_sof_n <= CDC_FIFO_DATA_I(31);

  sreg_empty <= fill_level_n(1);
  src_rdy_n  <= fill_level_n(2);
  ll8_eof_n  <= eof_n_sreg(2);
  sreg_load  <= not(CDC_FIFO_EMPTY_I) and sreg_empty and not(LL8_DST_RDY_N_I);
  sreg_shift <= not(sreg_empty) and not(LL8_DST_RDY_N_I);
  
  CDC_FIFO_REN_O  <= sreg_load;

  LL8_DATA_O      <= sreg(2);
  LL8_SOF_N_O     <= sof_n;
  LL8_EOF_N_O     <= ll8_eof_n;
  LL8_SRC_RDY_N_O <= src_rdy_n;
  LL8_PKT_OK_O    <= '1' when (byte_count = 0 and ll8_eof_n = '0') else '0';
  LL8_PKT_ERR_O   <= '1' when (byte_count = 0 and ll8_eof_n = '1') else '0';
  
  process(LL8_CLK_I)
  begin
    if rising_edge(LL8_CLK_I) then
      if LL8_RST_N_I = '0' then
        eof_n_sreg   <= (others=>'1');
        fill_level_n <= (others=>'1');
      else
        sreg(0) <= CDC_FIFO_DATA_I(7 downto 0);
        if sreg_load = '1' then
          sof_n <= cdc_sof_n;
          if data_len = "10" then
            eof_n_sreg   <= "11" & CDC_FIFO_DATA_I(30);
            fill_level_n <= "000";
            sreg(2) <= CDC_FIFO_DATA_I(23 downto 16);
            sreg(1) <= CDC_FIFO_DATA_I(15 downto  8);
          else
            eof_n_sreg   <= '1' & CDC_FIFO_DATA_I(30) & '1';
            fill_level_n <= "001";
            sreg(2) <= CDC_FIFO_DATA_I(15 downto 8);
            sreg(1) <= CDC_FIFO_DATA_I( 7 downto 0);
          end if;
        elsif sreg_shift = '1' then
          sof_n            <= '1';
          sreg(0)          <= (others=>'0');
          sreg(2 downto 1) <= sreg(1 downto 0);
          eof_n_sreg       <= eof_n_sreg(1 downto 0) & '1';
          fill_level_n     <= fill_level_n(1 downto 0) & '1';
        end if;
      end if;
    end if;
  end process;

  process(LL8_CLK_I)
  begin
    if rising_edge(LL8_CLK_I) then
      if LL8_RST_N_I = '0' then
        byte_count <= (others=>'0');
      else
        if cdc_sof_n = '0' and sreg_load = '1' then
          byte_count <= CDC_FIFO_DATA_I(byte_count'length-1 downto 0) + 1;
        elsif src_rdy_n = '0' and LL8_DST_RDY_N_I = '0' then
          byte_count <= byte_count - 1;
        end if;
      end if;
    end if;
  end process;

end architecture behavioral;