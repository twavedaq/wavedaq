---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Differential IO Buffer with IN_EN_I
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.04.2018 12:00:51
--
--  Description :  Differential IO buffer with IN_EN_I to be insertet in xps designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_iobuf_ds_en is
  generic (
    CGN_IN_EN_POLARITY : string    := "ACTIVE_HIGH"; -- ACTIVE_HIGH, ACTIVE_LOW
    CGN_BUFFER_TYPE    : string    := "IO"; -- IO, I, O
    CGN_SIZE           : integer   := 8
  );
  port (
    -- O Buffer
    OBUF_DS_P_I  : out    std_logic_vector(CGN_SIZE-1 downto 0);
    OBUF_DS_N_I  : out    std_logic_vector(CGN_SIZE-1 downto 0);
    OBUF_INT_O   : in     std_logic_vector(CGN_SIZE-1 downto 0);
    -- I Buffer
    IBUF_DS_P_I   : in    std_logic_vector(CGN_SIZE-1 downto 0);
    IBUF_DS_N_I   : in    std_logic_vector(CGN_SIZE-1 downto 0);
    IBUF_INT_O    : out   std_logic_vector(CGN_SIZE-1 downto 0);
    -- IO Buffer
    IOBUF_DS_P_IO : inout std_logic_vector(CGN_SIZE-1 downto 0);
    IOBUF_DS_N_IO : inout std_logic_vector(CGN_SIZE-1 downto 0);
    IOBUF_INT_O   : out   std_logic_vector(CGN_SIZE-1 downto 0);
    IOBUF_INT_I   : in    std_logic_vector(CGN_SIZE-1 downto 0);
    IOBUF_INT_T   : in    std_logic_vector(CGN_SIZE-1 downto 0);
    -- Input Enable
    IN_EN_I       : in  std_logic_vector(CGN_SIZE-1 downto 0)
  );
end util_iobuf_ds_en;

architecture behavioral of util_iobuf_ds_en is

  signal diff_ibuf_out  : std_logic_vector(CGN_SIZE-1 downto 0);
  signal diff_iobuf_out : std_logic_vector(CGN_SIZE-1 downto 0);

begin

  OBUFDS_INST : if CGN_BUFFER_TYPE = "O" generate
    OBUFDS_ARRAY : for i in CGN_SIZE-1 downto 0 generate
    
      OBUFDS_SINGLE_INST : OBUFDS
      generic map (
        IOSTANDARD => "DEFAULT"
      )
      port map (
        O  => OBUF_DS_P_I(i),
        OB => OBUF_DS_N_I(i),
        I  => OBUF_INT_O(i)
      );
      
    end generate;
  end generate;
  
  IBUFDS_INST : if CGN_BUFFER_TYPE = "I" generate
    IBUFDS_ARRAY : for i in CGN_SIZE-1 downto 0 generate
    
      IBUFDS_SINGLE_INST : IBUFDS
      generic map (
        CAPACITANCE => "LOW",
        DIFF_TERM => FALSE,
        IBUF_DELAY_VALUE => "0",
        IFD_DELAY_VALUE => "AUTO",
        IOSTANDARD => "DEFAULT"
      )
      port map (
        O  => diff_ibuf_out(i),
        I  => IBUF_DS_P_I(i),
        IB => IBUF_DS_N_I(i)
      );
      
      EN_ACT_HIGH_I : if CGN_IN_EN_POLARITY = "ACTIVE_HIGH" generate
        IBUF_INT_O(i) <= diff_ibuf_out(i) when IN_EN_I(i) = '1' else '0';
      end generate;
      EN_ACT_LOW_I : if CGN_IN_EN_POLARITY = "ACTIVE_LOW" generate
        IBUF_INT_O(i) <= diff_ibuf_out(i) when IN_EN_I(i) = '0' else '0';
      end generate;
    end generate;
  end generate;

  IOBUFDS_INST : if CGN_BUFFER_TYPE = "IO" generate
    IOBUFDS_ARRAY : for i in CGN_SIZE-1 downto 0 generate
    
      IOBUFDS_SINGLE_INST : IOBUFDS
      generic map (
        IBUF_DELAY_VALUE => "0",
        IFD_DELAY_VALUE  => "AUTO",
        IOSTANDARD       => "DEFAULT"
      )
      port map (
        O   => diff_iobuf_out(i),
        IO  => IOBUF_DS_P_IO(i),
        IOB => IOBUF_DS_N_IO(i),
        I   => IOBUF_INT_I(i),
        T   => IOBUF_INT_T(i)
      );
      
      EN_ACT_HIGH_I : if CGN_IN_EN_POLARITY = "ACTIVE_HIGH" generate
        IOBUF_INT_O(i) <= diff_iobuf_out(i) when IN_EN_I(i) = '1' else '0';
      end generate;
      EN_ACT_LOW_I : if CGN_IN_EN_POLARITY = "ACTIVE_LOW" generate
        IOBUF_INT_O(i) <= diff_iobuf_out(i) when IN_EN_I(i) = '0' else '0';
      end generate;
    end generate;
  end generate;

end architecture behavioral;