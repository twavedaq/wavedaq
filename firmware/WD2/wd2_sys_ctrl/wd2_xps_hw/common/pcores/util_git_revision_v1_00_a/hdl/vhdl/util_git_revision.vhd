---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  GIT Revision Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  03.11.2016 13:56:53
--
--  Description :  import the latest git hashtag into the firware.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity util_git_revision is
  generic (
    CGN_GIT_HASHTAG   : integer := 0
  );
  port (
    GIT_HASHTAG_O  : out std_logic_vector(31 downto 0)
  );
end util_git_revision;

architecture behavioral of util_git_revision is

begin

  GIT_HASHTAG_O   <= conv_std_logic_vector(CGN_GIT_HASHTAG, 32);

end architecture behavioral;