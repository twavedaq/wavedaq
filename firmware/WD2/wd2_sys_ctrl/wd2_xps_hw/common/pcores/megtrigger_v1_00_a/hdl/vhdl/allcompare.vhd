----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Marco Francesconi
-- 
-- Create Date:    16:16:50 21/05/2016 
-- Design Name: 
-- Module Name:    allsum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this moule search the waveform maximum
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

Library UNISIM;
use UNISIM.vcomponents.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

entity allcompare is
   Port (
      din : in  ADCCalibratedData;
      clk : in  STD_LOGIC;
      maxout : out  ADCMaxValueData;
      idout  : out  STD_LOGIC_VECTOR (3 downto 0)
   );
end allcompare;

architecture Behavioral of allcompare is
TYPE WFMDataOne IS ARRAY (7 downto 0) OF ADCMaxValueData;
TYPE WFMDataTwo IS ARRAY (3 downto 0) OF ADCMaxValueData;
TYPE WFMDataThree IS ARRAY (1 downto 0) OF ADCMaxValueData;

SIGNAL WfOne : WFMDataOne;
SIGNAL WfTwo : WFMDataTwo;
SIGNAL WfThree : WFMDataThree;

TYPE MaxDataOne IS ARRAY (7 downto 0) OF STD_LOGIC_VECTOR(0 downto 0);
TYPE MaxDataTwo IS ARRAY (3 downto 0) OF STD_LOGIC_VECTOR(1 downto 0);
TYPE MaxDataThree IS ARRAY (1 downto 0) OF STD_LOGIC_VECTOR(2 downto 0);

SIGNAL MaxOne: MaxDataOne;
SIGNAL MaxTwo: MaxDataTwo;
SIGNAL MaxThree: MaxDataThree;

begin

   g_first_cmp : FOR i IN 0 TO 7 GENERATE
      first_cmp : entity megtrigger_v1_00_a.singlecompare 
      generic map(
         MAXSIZE => 1,
         CLKPOLARITY => '1'
      )
      port map(
         dinA => din(2*i),
         dinB => din(2*i+1),
         clk => clk,
         maxA => "0",
         maxB => "1",
         dout => WfOne(i),
         max => MaxOne(i) 
      );
   END GENERATE g_first_cmp;

   g_second_cmp : FOR i IN 0 TO 3 GENERATE
      SIGNAL maxA : STD_LOGIC_VECTOR(MaxOne(2*i)'Length downto 0);
      SIGNAL maxB : STD_LOGIC_VECTOR(MaxOne(2*i+1)'Length downto 0);
   BEGIN
      maxA <= "0" & MaxOne(2*i);
      maxB <= "1" & MaxOne(2*i+1);

      second_cmp : entity megtrigger_v1_00_a.singlecompare 
      generic map(
         MAXSIZE => 2,
         CLKPOLARITY => '1'
      )
      port map(
         dinA => WfOne(2*i),
         dinB => WfOne(2*i+1),
         clk => clk,
         maxA => maxA,
         maxB => maxB,
         dout => WfTwo(i),
         max => MaxTwo(i) 
      );
   END GENERATE g_second_cmp;

   g_third_cmp : FOR i IN 0 TO 1 GENERATE
      SIGNAL maxA : STD_LOGIC_VECTOR(MaxTwo(2*i)'Length downto 0);
      SIGNAL maxB : STD_LOGIC_VECTOR(MaxTwo(2*i+1)'Length downto 0);
   BEGIN
      maxA <= "0" & MaxTwo(2*i);
      maxB <= "1" & MaxTwo(2*i+1);

      third_cmp : entity megtrigger_v1_00_a.singlecompare 
      generic map(
         MAXSIZE => 3,
         CLKPOLARITY => '1'
      )
      port map(
         dinA => WfTwo(2*i),
         dinB => WfTwo(2*i+1),
         clk => clk,
         maxA => maxA,
         maxB => maxB,
         dout => WfThree(i),
         max => MaxThree(i) 
      );
   END GENERATE g_third_cmp;
   
   b_last_cmp: BLOCK
      SIGNAL maxA : STD_LOGIC_VECTOR(MaxThree(0)'Length downto 0);
      SIGNAL maxB : STD_LOGIC_VECTOR(MaxThree(1)'Length downto 0);
   BEGIN
      maxA <= "0" & MaxThree(0);
      maxB <= "1" & MaxThree(1);

      fourth_cmp : entity megtrigger_v1_00_a.singlecompare 
      generic map(
         MAXSIZE => 4,
         CLKPOLARITY => '1'
      )
      port map(
         dinA => WfThree(0),
         dinB => WfThree(1),
         clk => clk,
         maxA => maxA,
         maxB => maxB,
         dout => maxout,
         max => idout 
      );
   END BLOCK b_last_cmp;
end Behavioral;

