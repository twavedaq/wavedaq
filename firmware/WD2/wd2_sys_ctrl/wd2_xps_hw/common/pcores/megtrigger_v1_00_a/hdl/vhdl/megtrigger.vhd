----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    16:54:27 02/18/2015 
-- Design Name: 
-- Module Name:    wdint_vhd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: this is the top module of the WDB firmware (MEG II trigger side)
--              it has as input the parallelised FADC data and the 80-100 MHz clock
--              the outputs are the results of trigger computation as tirgger bit and/or 
--              parallel data to serder depending on the application
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity megtrigger is
  Generic (
    C_ADC_INPUT_DIM : natural := 12;
    C_TDC_INPUT_DIM : natural := 8;
    C_CAL_DIM : natural := 8;
    C_OFFSET_DIM : natural := 8
  );
  Port (
    DATA_CLK_I        : in STD_LOGIC;
    SYSTEM_CLK_I      : in STD_LOGIC;
    UC_CLK_I          : in STD_LOGIC;
    -- ADC data
    ADCDATA_I        : in  STD_LOGIC_VECTOR (ADC_INPUT_DIM*16-1 downto 0);
    -- TDC data
    TDCDATA_I         : in  STD_LOGIC_VECTOR (TDC_INPUT_DIM*16-1 downto 0);
    -- raw comparator output
    --COMP_TRG_I        : in  STD_LOGIC_VECTOR (15 downto 0);
    -- shaper output shaped from local trigger
    SHAPER_TRG_I      : in  STD_LOGIC_VECTOR (15 downto 0);
    -- calibration factors
    CALDATA_I         : in  STD_LOGIC_VECTOR (CAL_DIM*16-1 downto 0); 
    -- time offset to be subtracted
    OFFSETDATA_I      : in  STD_LOGIC_VECTOR (OFFSET_DIM*16-1 downto 0); 
    -- pedestal subtraction configuration
    PEDCONF_I         : in  STD_LOGIC_VECTOR  (31 downto 0);
    -- trigger thresholds: use to be assigned...
    THR0_I            : in  STD_LOGIC_VECTOR  (31 downto 0);
    THR1_I            : in  STD_LOGIC_VECTOR  (31 downto 0);
    THR2_I            : in  STD_LOGIC_VECTOR  (31 downto 0);
    -- debug serdes word
    DBGSERDES_I      : in  STD_LOGIC_VECTOR  (63 downto 0);
    -- tdc channel mask
    TDCCHMASK_I       : in STD_LOGIC_VECTOR   (15 downto 0);
    -- control bits
    CTRLBUS_I         : in STD_LOGIC_VECTOR   (31 downto 0);
    -- connection to local pattern trigger
    LOCAL_TRIG_O      : out STD_LOGIC;
    LOCAL_VETO_TRIG_O : out STD_LOGIC;
    -- trigger bus
    SYNC_I            : in STD_LOGIC;
    DRS_BUSY_I        : in STD_LOGIC;
   --out bits
    TCBData_O         : out STD_LOGIC_VECTOR (63 downto 0)
  );
end megtrigger;

architecture Behavioral of megtrigger is 

-- signals from CDC
  SIGNAL caldata : STD_LOGIC_VECTOR (CAL_DIM*16-1 downto 0);
  SIGNAL offdata : STD_LOGIC_VECTOR (OFFSET_DIM*16-1 downto 0);
  SIGNAL tdcchmask : STD_LOGIC_VECTOR (15 downto 0);
  SIGNAL pedconf : STD_LOGIC_VECTOR (31 downto 0);
  SIGNAL thr0 : STD_LOGIC_VECTOR (31 downto 0);
  SIGNAL thr1 : STD_LOGIC_VECTOR (31 downto 0);
  SIGNAL thr2 : STD_LOGIC_VECTOR (31 downto 0);
  SIGNAL controlbus : STD_LOGIC_VECTOR (31 downto 0);
  SIGNAL dbgserdes : STD_LOGIC_VECTOR (63 downto 0);

-- control bus signals
  SIGNAL BUSYMASK : STD_LOGIC;
--  SIGNAL FADCMODE : STD_LOGIC; --unused
--  SIGNAL TESTTXMODE : STD_LOGIC; --unused
--  SIGNAL SWSYNC : STD_LOGIC;
  SIGNAL ALGSEL : STD_LOGIC_VECTOR (1 downto 0);
  SIGNAL DEBUG_CTRL : STD_LOGIC;
  SIGNAL FORCERUN : STD_LOGIC;
  SIGNAL CLEANPED : STD_LOGIC;
  SIGNAL TDC_POLARITY : STD_LOGIC;
--  SIGNAL MASKSYNC : STD_LOGIC;

-- control signals
  SIGNAL FORCEPED : STD_LOGIC;
  SIGNAL RUNMODE : STD_LOGIC;
  SIGNAL SYNC : STD_LOGIC;

-- pedestal subtraction configuration
  SIGNAL PedTHR : STD_LOGIC_VECTOR(ADC_INPUT_DIM-1 downto 0);
  SIGNAL PedDlyEnable : STD_LOGIC_VECTOR(1 downto 0);
  SIGNAL PedDlySelect : STD_LOGIC_VECTOR(3 downto 0);
  SIGNAL PedAdderSelect : STD_LOGIC_VECTOR(2 downto 0);

-- support arrays for data and calibration words
  SIGNAL adc : ADCInputData;
  SIGNAL tdc : TDCInputData;
  SIGNAL cal : CalibData;
  SIGNAL offset : OffsetData;
  SIGNAL comp_reg : STD_LOGIC_VECTOR (15 downto 0);

-- waveform logic outputs
  SIGNAL SUM : ADCSummedData;
  SIGNAL TRG0 : STD_LOGIC;
  SIGNAL TRG1 : STD_LOGIC;
  SIGNAL MAX : STD_LOGIC_VECTOR (3 downto 0);
  SIGNAL MAXWFM : ADCMaxValueData;

-- tdc logic outputs
  SIGNAL TDCHIT : STD_LOGIC_VECTOR (15 downto 0);
  SIGNAL TDCTIME : TimeData;
  SIGNAL TDCPULSE : STD_LOGIC;
  SIGNAL AVGNTDC : AvgNTdcData;
  SIGNAL AVGTIME : AvgTimeData; 

--pattern generation logic
  SIGNAL SERDES_PATTERN_ON_SYSCLK : STD_LOGIC_VECTOR (3 downto 0);
  SIGNAL SERDES_PATTERN : STD_LOGIC_VECTOR (3 downto 0);
  SIGNAL PatternData : STD_LOGIC_VECTOR (63 downto 0);
  SIGNAL DebugData : STD_LOGIC_VECTOR (63 downto 0);

--output signal
  SIGNAL ProcData : STD_LOGIC_VECTOR (63 downto 0);
begin

-- fix to zero the local triggers
  LOCAL_TRIG_O      <= '0';
  LOCAL_VETO_TRIG_O <= '0';

-- clock domain crossings for each reg
  caldata_cdc : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH          => CAL_DIM*16,
    CGN_USE_INPUT_REG_A     => 1,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0, 
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP
  (
    CLK_A_I => UC_CLK_I,
    CLK_B_I => DATA_CLK_I,
    PORT_A_I => CALDATA_I,
    PORT_B_O => caldata
  );

  offsetdata_cdc : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH          => OFFSET_DIM*16,
    CGN_USE_INPUT_REG_A     => 1,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0, 
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP
  (
    CLK_A_I => UC_CLK_I,
    CLK_B_I => DATA_CLK_I,
    PORT_A_I => OFFSETDATA_I,
    PORT_B_O => offdata
  );
  
  tdcchmask_cdc : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH          => 16,
    CGN_USE_INPUT_REG_A     => 1,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0, 
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP
  (
    CLK_A_I => UC_CLK_I,
    CLK_B_I => DATA_CLK_I,
    PORT_A_I(15 downto 0) => TDCCHMASK_I,
    PORT_B_O(15 downto 0) => tdcchmask
  );
  
  regs_cdc : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH          => 7*32,
    CGN_USE_INPUT_REG_A     => 1,
    CGN_USE_OUTPUT_REG_A    => 0,
    CGN_USE_GRAY_CONVERSION => 0, 
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 0,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP
  (
    CLK_A_I => UC_CLK_I,
    CLK_B_I => DATA_CLK_I,
    PORT_A_I(32*0+31 downto 32*0) => PEDCONF_I,
    PORT_A_I(32*1+31 downto 32*1) => THR0_I,
    PORT_A_I(32*2+31 downto 32*2) => THR1_I,
    PORT_A_I(32*3+31 downto 32*3) => THR2_I,
    PORT_A_I(32*4+63 downto 32*4) => DBGSERDES_I, --64 bits
    PORT_A_I(32*6+31 downto 32*6) => CTRLBUS_I,
    PORT_B_O(32*0+31 downto 32*0) => pedconf,
    PORT_B_O(32*1+31 downto 32*1) => thr0,
    PORT_B_O(32*2+31 downto 32*2) => thr1,
    PORT_B_O(32*3+31 downto 32*3) => thr2,
    PORT_B_O(32*4+63 downto 32*4) => dbgserdes, --64 bits
    PORT_B_O(32*6+31 downto 32*6) => controlbus
  );

  -- temporary assignment of control bus
  -- DRS_BUSY_I is 0 while DRS logic is ready to accept triggers
  BUSYMASK     <= controlbus(0);
--  FADCMODE     <= controlbus(1); -- unused
--  TESTTXMODE   <= controlbus(2); -- unused
--  SWSYNC      <= controlbus(3); --unused
  ALGSEL       <= controlbus(5 downto 4);
  FORCERUN     <= controlbus(7);
  CLEANPED     <= controlbus(8);
  TDC_POLARITY <= controlbus(9);
  DEBUG_CTRL   <= controlbus(10);
--  MASKSYNC     <= controlbus(11); --unused
  
  control : entity megtrigger_v1_00_a.control port map(
    DRS_BUSY_I => DRS_BUSY_I,
    CLK => DATA_CLK_I,
    BUSYMASK => BUSYMASK,
    FORCERUN => FORCERUN,
    CLEANPED => CLEANPED,
    FORCEPED => FORCEPED,
    RUNMODE => RUNMODE
  );

  -- decode pedestal configuration
  PedTHR <= pedconf(ADC_INPUT_DIM-1 downto 0);
  PedDlyEnable <= pedconf(21 downto 20);
  PedDlySelect <= pedconf(19 downto 16);
  PedAdderSelect <= pedconf(26 downto 24);

  -- register SYNC on DATA_CLK
  process (SYSTEM_CLK_I) begin
    if(rising_edge(SYSTEM_CLK_I)) then
      SYNC <= SYNC_I;
    end if;
  end process;
  
   -- decode input data in array of vectors
   g_data_in : FOR i IN 0 TO 15 GENERATE
      process (DATA_CLK_I) begin
         if(rising_edge(DATA_CLK_I)) then
            tdc(i) <= TDCDATA_I(TDC_INPUT_DIM*(i+1)-1 downto TDC_INPUT_DIM*i);
            adc(i) <= ADCDATA_I(ADC_INPUT_DIM*(i+1)-1 downto ADC_INPUT_DIM*i);
         end if;
      end process;
   END GENERATE g_data_in;

   --decode calibration data in array of vectors
   g_calib : FOR i IN 0 TO 15 GENERATE
     cal(i) <= caldata(CAL_DIM*(i+1)-1 downto CAL_DIM*i);
     offset(i) <= offdata(OFFSET_DIM*(i+1)-1 downto OFFSET_DIM*i);
   END GENERATE g_calib;

  -- here the waveform algorithm block
  mainwfmblock : entity megtrigger_v1_00_a.mainwfm port map(
    CLK => DATA_CLK_I,
    DATAIN  => adc,
    CAL => cal,
    PedTHR => PedTHR,
    PedDlyEnable => PedDlyEnable,
    PedDlySelect => PedDlySelect,
    PedAdderSelect => PedAdderSelect,
    SumTHR0 => thr0,
    SumTHR1 => thr1,
    SumTHR2 => thr2,
    RUNMODE => RUNMODE,
    FORCEPED => FORCEPED,
    SUM => SUM,
    MAX => MAX,
    MAXWFM => MAXWFM,
    TRG0 => TRG0,
    TRG1 => TRG1,
    TRG2 => open
  );

  --tdc algorithm block
  maintdcblock : entity megtrigger_v1_00_a.maintdc port map(
    CLK       => DATA_CLK_I,
    DATAIN    => tdc,
    OFFSET    => offset,
    POLARITY  => TDC_POLARITY,
    TDCCHMASK => tdcchmask,
    DATAOUT   => TDCTIME,
    HIT       => TDCHIT,
    PULSE     => TDCPULSE,
    AVGNTDC   => AVGNTDC,
    AVGTIME   => AVGTIME
  );
  
-- register the shaped comparator data before sending to serders port
  process (DATA_CLK_I) begin
    if(rising_edge(DATA_CLK_I)) then
      comp_reg  <= SHAPER_TRG_I;
    end if;
  end process;

-- variable pattern generation
  process (SYSTEM_CLK_I) begin
    if(rising_edge(SYSTEM_CLK_I)) then
      if(SYNC = '1') then
        SERDES_PATTERN_ON_SYSCLK <= "0000";
      else
        SERDES_PATTERN_ON_SYSCLK <= SERDES_PATTERN_ON_SYSCLK+1;
      end if;
    end if;
  end process;

  pattern_cdc : cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH          => 4,
    CGN_USE_INPUT_REG_A     => 1,
    CGN_USE_OUTPUT_REG_A    => 1,
    CGN_USE_GRAY_CONVERSION => 1, 
    CGN_NUM_SYNC_REGS_B     => 2,
    CGN_NUM_OUTPUT_REGS_B   => 1,
    CGN_GRAY_TO_BIN_STYLE   => 0
  )
  PORT MAP
  (
    CLK_A_I => SYSTEM_CLK_I,
    CLK_B_I => DATA_CLK_I,
    PORT_A_I => SERDES_PATTERN_ON_SYSCLK,
    PORT_B_O => SERDES_PATTERN
  );
  
  PatternData <= X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN & X"A" & SERDES_PATTERN; 

-- multiplexer to select fixed or variable pattern
  process (DATA_CLK_I) begin
    if(rising_edge(DATA_CLK_I)) then
      if(DEBUG_CTRL = '1')then
         DebugData <= PatternData;
      else
         DebugData <= dbgserdes;
      end if;
    end if;
  end process;

  -- mux for ProcData
   -- if ALGSEL
   --  0 => LXe (time & QSUM)
   --  1 => TC (time & hit for each pair of channels)
   --  2 => AUX (discr status, max & QSUM)
   -- otherwise => serdes calibration pattern
   process (DATA_CLK_I) begin
     if(rising_edge(DATA_CLK_I)) then
       case ALGSEL is
         when "00" =>
           ProcData <= TDCPULSE & "00" & AVGNTDC & AVGTIME & X"00000" & "000" & SUM;
--               ProcData(0) <= SUM(15 downto 0);
--               ProcData(1) <= MAX & SUM(21) & SUM(21) & SUM(21) & SUM(21) & SUM(21) & SUM(21) & SUM(21 downto 16);
--               ProcData(2) <= MAXWFM(13 downto 0) & "00";
--               ProcData(3) <= AVGHIT & AVGTIME(10 downto 0) & MAXWFM(17 downto 14);
         when "01" =>
           ProcData <= TDCTIME(15) & TDCTIME(14) & TDCTIME(13) & TDCTIME(12) & TDCTIME(11) & TDCTIME(10) & TDCTIME(9) & TDCTIME(8) & TDCTIME(7) & TDCTIME(6) & TDCTIME(5) & TDCTIME(4) & TDCTIME(3) & TDCTIME(2) & TDCTIME(1) & TDCTIME(0) & TDCHIT;
--               ProcData(0) <= HIT_VEC;
--               ProcData(1) <= TIME_VEC(5)(0) & TIME_VEC(4) & TIME_VEC(3) & TIME_VEC(2) & TIME_VEC(1) & TIME_VEC(0);
--               ProcData(2) <= TIME_VEC(10)(1 downto 0) & TIME_VEC(9) & TIME_VEC(8) & TIME_VEC(7) & TIME_VEC(6) & TIME_VEC(5)(2 downto 1);
--               ProcData(3) <= TIME_VEC(15) & TIME_VEC(14) & TIME_VEC(13) & TIME_VEC(12) & TIME_VEC(11) & TIME_VEC(10)(2);
         when "10" =>
           ProcData <= comp_reg & MAX &  MAXWFM(MAXWFM'length-3 downto 0) & SUM; -- removed 2 bit out of MAXWFM
--               ProcData(0) <= SUM(15 downto 0);
--               ProcData(1) <= MAXWFM(9 downto 0) & SUM(21 downto 16);
--               ProcData(2) <= '0' & TRG2 & TRG1 & TRG0 & MAX & MAXWFM(17 downto 10);
--               ProcData(3) <= comp_reg;
            when others =>
               ProcData <= DebugData;
         end case;
      end if;
   end process;
 
   TCBData_O <= ProcData;


end Behavioral;
