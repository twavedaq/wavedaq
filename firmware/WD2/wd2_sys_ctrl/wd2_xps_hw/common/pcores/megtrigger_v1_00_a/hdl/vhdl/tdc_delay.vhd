library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_misc.or_reduce;


entity TDC_DELAY is
  generic(
    CGN_DLY        : natural := 1;
    CGN_TDCSIZE    : natural := 4
  );
  port(
    CLK_I         : in std_logic;
    HIT_I         : in std_logic;
    TDCTIME_I     : in std_logic_vector(CGN_TDCSIZE-1 downto 0);
    HIT_O         : out std_logic;
    TDCTIME_O     : out std_logic_vector(CGN_TDCSIZE-1 downto 0)
   );
end;

architecture BEHAVIORAL of TDC_DELAY is
begin
        
  dly_sr: if(CGN_DLY /=0) generate
    signal hit_dly: std_logic_vector(CGN_DLY-1 downto 0);
    type tdctime_sr_type is array (CGN_DLY-1 downto 0) of std_logic_vector(CGN_TDCSIZE-1 downto 0);
    signal tdctime_dly: tdctime_sr_type;
  begin
    process(CLK_I)
    begin
      if(rising_edge(CLK_I)) then
        hit_dly <= hit_dly(CGN_DLY-2 downto 0) & HIT_I;
        tdctime_dly <= tdctime_dly(CGN_DLY-2 downto 0) & TDCTIME_I;
      end if;
    end process;
    HIT_O <= hit_dly(CGN_DLY-1);  
    TDCTIME_O <= tdctime_dly(CGN_DLY-1);  
  end generate;

  no_dly_sr: if (CGN_DLY =0) generate
    HIT_O <= HIT_I;
    TDCTIME_O <= TDCTIME_I;
  end generate;


end BEHAVIORAL;
