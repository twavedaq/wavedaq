--
--	Package File Template
--
--	Purpose: This package defines supplemental types, subtypes, 
--		 constants, and functions 
--
--   To use any of the example code shown below, uncomment the lines and modify as necessary
--

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;
use IEEE.math_real."ceil";
use IEEE.math_real."log2";

package common is
-- adc word size, if bigger than 16 revise the PedConf register
   CONSTANT ADC_INPUT_DIM : integer := 12;
-- tdc word size
   CONSTANT TDC_INPUT_DIM : integer := 8;
-- channel calibration size
   CONSTANT CAL_DIM : integer := 8;
-- channel calibration size
   CONSTANT OFFSET_DIM : integer := 4;
-- number of clock cycles to stretch TDC pulses
   CONSTANT TDC_STRETCHING : natural := 2;

-- encoded time size
   CONSTANT TIME_DIM : integer := integer(ceil(log2(real(TDC_INPUT_DIM))));

-- input types
   TYPE ADCInputData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (ADC_INPUT_DIM-1 downto 0);   
   TYPE TDCInputData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (TDC_INPUT_DIM-1 downto 0);   
   TYPE CalibData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (CAL_DIM-1 downto 0);   
   TYPE OffsetData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (OFFSET_DIM-1 downto 0);   

-- useful types for waveform logic
   TYPE ADCCalibratedData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (ADC_INPUT_DIM+CAL_DIM downto 0); --size is ADC_INPUT_DIM + 1(pedestal) + CAL_DIM
   TYPE ADCFirstSumData IS ARRAY (7 downto 0) OF STD_LOGIC_VECTOR (ADC_INPUT_DIM+CAL_DIM+1 downto 0); --size is ADC_INPUT_DIM + 1(pedestal) + CAL_DIM + 1 (SUM)
   SUBTYPE ADCSummedData IS STD_LOGIC_VECTOR(ADC_INPUT_DIM+CAL_DIM+4 downto 0);
   SUBTYPE ADCMaxValueData IS STD_LOGIC_VECTOR(ADC_INPUT_DIM+CAL_DIM downto 0);

-- useful types for time logic
   TYPE TimeData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR(TIME_DIM-1 downto 0);
   TYPE TimeStretchedData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR(TIME_DIM+TDC_STRETCHING-2 downto 0);
   SUBTYPE AvgTimeData IS STD_LOGIC_VECTOR(TIME_DIM+TDC_STRETCHING+4-2 downto 0);
   SUBTYPE AvgNTdcData IS STD_LOGIC_VECTOR(4 downto 0);

-- TYPE ADCData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (15 downto 0);
-- TYPE PedData IS ARRAY (15 downto 0) OF SIGNED (11 downto 0);
-- TYPE TMPCalibData IS ARRAY (7 downto 0) OF STD_LOGIC_VECTOR (47 downto 0);
-- TYPE SERDESData IS ARRAY (3 downto 0) OF STD_LOGIC_VECTOR (15 downto 0);
-- TYPE TDCData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (7 downto 0);
-- TYPE TimeData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR (2 downto 0);
-- TYPE FirstTimeOUT IS ARRAY (7 downto 0) OF STD_LOGIC_VECTOR (4 downto 0);
-- TYPE SecondTimeOUT IS ARRAY (3 downto 0) OF STD_LOGIC_VECTOR (6 downto 0);
-- TYPE ThirdTimeOUT IS ARRAY (1 downto 0) OF STD_LOGIC_VECTOR (8 downto 0);
-- TYPE FirstHitOUT IS ARRAY (7 downto 0) OF STD_LOGIC;
-- TYPE SecondHitOUT IS ARRAY (3 downto 0) OF STD_LOGIC;
-- TYPE ThirdHitOUT IS ARRAY (1 downto 0) OF STD_LOGIC;
-- TYPE FirstStageOUT IS ARRAY (7 downto 0 ) OF  STD_LOGIC_VECTOR(47 downto 0);
-- TYPE SecondStageOUT IS ARRAY (3 downto 0 ) OF  STD_LOGIC_VECTOR(47 downto 0);
-- TYPE ThirdStageOUT IS ARRAY (1 downto 0 ) OF  STD_LOGIC_VECTOR(47 downto 0);
-- TYPE WFMData IS ARRAY (15 downto 0) OF STD_LOGIC_VECTOR(35 downto 0);


end common;

package body common is
 
end common;
