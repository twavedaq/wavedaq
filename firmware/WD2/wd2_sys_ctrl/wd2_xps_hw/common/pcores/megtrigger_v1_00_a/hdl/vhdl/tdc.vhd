library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.std_logic_misc.all;

entity TDC is
  generic(CGN_NBITS_O      : natural);
          --CGN_FALLING_EDGE : bit);
  port(CLK           : in std_logic;
       TDCDATA_I     : in std_logic_vector((2**CGN_NBITS_O)-1 downto 0);
       NEGPOLARITY_I : in std_logic;
       TDCTIME_O     : out std_logic_vector(CGN_NBITS_O-1 downto 0);
       STATE_O       : out std_logic; 
       HIT_O         : out std_logic);
       --WIDTH_O       : out std_logic_vector(8 downto 0));
end;

architecture BEHAVIORAL of TDC is

signal tdcdata_old : std_logic;
  signal TDCDATA   : std_logic_vector((2**CGN_NBITS_O)-1 downto 0);

begin

  TDCDATA <= TDCDATA_I when (NEGPOLARITY_I = '0' )
             else not(TDCDATA_I);

  process(CLK)

  --variable tmp_width : std_logic_vector(8 downto 0) := "000000000";
  variable first_hit : std_logic;
  --variable end_bit   : std_logic;

  begin

    if (CLK'event and CLK='1') then --clock cycle
      STATE_O <= or_reduce(TDCDATA);
      tdcdata_old <= TDCDATA(TDCDATA'low);
                first_hit := '0';
      if ((TDCDATA(TDCDATA'high) = '1') and (tdcdata_old = '1')) then --1s from previous TDCDATA and 1s in present TDCDATA
        HIT_O <= '0';
      end if;
      for i in TDCDATA'high downto TDCDATA'low loop --loop on 8 bits
        if(i = TDCDATA'high) then --bit 0
          if(TDCDATA(TDCDATA'high) = '0') then --case bit 0 is 0
            HIT_O <= '0'; --no hit
            TDCTIME_O <= "000"; --default value if no hit
            --tmp_width := "000000000"; --reset width
            first_hit := '0';
            --if (tdcdata_old = '1') then end_bit := '1'; --falling edge coincident with rising edge of the clock
            --end if;
          elsif (TDCDATA(TDCDATA'high) = '1') then -- case bit 0 is 1
            if (tdcdata_old = '1') then --if previous 8 bits word ends with 1 the signal is the same
              HIT_O <= '0'; --not another hit
              --tmp_width := std_logic_vector(unsigned(tmp_width)+("000000001")); --width increases
              first_hit := '0';
            else --if previous 8 bits ends with a 0
              HIT_O <= '1'; --it is a new hit
              --TDCTIME_O <= "000"; --the time of the hit is 0 if the hit is at the beginning of the word
              TDCTIME_O <= "111"; --**********this is for the other ordering
              --tmp_width := "000000001"; --the width increases from 0 to 1
              first_hit := '0';
            end if; --end "if previous 8 bits ends with a 0" and "if previous 8 bits word ends with 1"
          end if; --end "case bit 0 is 0" and "case bit 0 is 1"
        else --bits after bit 0
          --if (TDCDATA(i) = '1' and TDCDATA(i-1) = '1' and end_bit = '0') then tmp_width := std_logic_vector(unsigned(tmp_width)+("000000001")); --increase width if the current bit is 1
          --end if; --end "increase width if the current bit is 1"
          if (TDCDATA(i) = '1' and TDCDATA(i+1) = '0' and first_hit = '0') then --first hit in the middle of the word
            HIT_O <= '1'; --it is a hit
            --TDCTIME_O <= std_logic_vector(to_unsigned((7-i),3)); --store the time as the position of the high bit in the word
            TDCTIME_O <= std_logic_vector(to_unsigned((i),3)); --**********this is so that the "0" is at the last sampling
            --tmp_width := "000000001"; --signal width = 0
            first_hit := '1';
            --end_bit := '0';
          --elsif (TDCDATA(i) = '0' and TDCDATA(i+1) = '1') then end_bit := '1';--falling edge
          end if; --end "first hit in the middle of the word" and "falling edge"
        end if; --end "bit 0" and "bits after bit 0"
      end loop; --end "loop on 8 bits"
      --WIDTH_O <= tmp_width; --assign width
    end if; --end "clock cycle"
  end process;

end BEHAVIORAL;
