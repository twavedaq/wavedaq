----------------------------------------------------------------------------------
-- Company: 
-- Engineer:
-- 
-- Create Date:    16:16:50 21/05/2016 
-- Design Name: 
-- Module Name:    siglecompare - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this moule does a comparison
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

Library UNISIM;
use UNISIM.vcomponents.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

entity singlecompare is
   Generic(
      MAXSIZE : integer;
      CLKPOLARITY : STD_LOGIC := '1'
   );
   Port (
      dinA : in  ADCMaxValueData;
      dinB : in  ADCMaxValueData;
      clk : in  STD_LOGIC;
      maxA : in  STD_LOGIC_VECTOR (MAXSIZE-1 downto 0);
      maxB : in  STD_LOGIC_VECTOR (MAXSIZE-1 downto 0);
      dout : out ADCMaxValueData;
      max : out  STD_LOGIC_VECTOR (MAXSIZE-1 downto 0)
   );
end singlecompare;

architecture Behavioral of singlecompare is
   SIGNAL CMP : STD_LOGIC;
begin
   process(dinA, dinB) begin
      if(signed(dinA) >= signed(dinB)) then
         CMP <= '1';
      else
         CMP <= '0';
      end if;
   end process;

   process(clk, cmp) begin
      if(clk'event and clk = CLKPOLARITY) then
         if(CMP = '1') then
            dout <= dinA;
            max <= maxA;
         else
            dout <= dinB;
            max <= maxB;
         end if;
      end if;
   end process;
end Behavioral;

