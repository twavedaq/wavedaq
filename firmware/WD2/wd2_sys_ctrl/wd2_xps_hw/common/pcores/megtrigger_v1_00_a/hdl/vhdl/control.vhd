----------------------------------------------------------------------------------
-- Company: 
-- Engineer:
-- 
-- Create Date:    16:16:50 21/05/2016 
-- Design Name: 
-- Module Name:    control - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this moule does a comparison
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

Library UNISIM;
use UNISIM.vcomponents.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

entity control is
   Generic(
      FORCEPED_TIME : integer := 200
   );
   Port (
      DRS_BUSY_I : in  STD_LOGIC;
      CLK : in  STD_LOGIC;
      BUSYMASK : in  STD_LOGIC;
      FORCERUN : in  STD_LOGIC;
      CLEANPED : in  STD_LOGIC;
      FORCEPED : out STD_LOGIC;
      RUNMODE : out STD_LOGIC
   );
end control;
	
architecture Behavioral of control is
  SIGNAL MASKED_BUSY : STD_LOGIC;
  SIGNAL OLD_BUSY : STD_LOGIC;
  signal OLD_CLEANPED : STD_LOGIC;
  signal OLD_FORCEPED : STD_LOGIC;

  SIGNAL GEN_RUNMODE : STD_LOGIC;
  SIGNAL GEN_FORCEPED : STD_LOGIC;
  
begin
  -- mask BUSY and apply SWSTOP
  MASKED_BUSY <= (DRS_BUSY_I and BUSYMASK);

  -- delay BUSY, CLEANPED and FORCEPED to get edges
  process(CLK)
  begin
    if(clk'event and clk = '1') then
      OLD_BUSY <= MASKED_BUSY;
		OLD_CLEANPED <= CLEANPED;
		OLD_FORCEPED <= GEN_FORCEPED;
	 end if;
  end process;
  
  -- generate a FORCEPED_TIME pulse on negative edge of BUSY (sw trigger on CLEANPED)
  process(CLK, MASKED_BUSY, OLD_BUSY, GEN_FORCEPED)
    VARIABLE   counter         : INTEGER RANGE 0 TO FORCEPED_TIME;
  begin
    if(clk'event and clk = '1') then
      if((MASKED_BUSY = '0' and OLD_BUSY = '1') or (OLD_CLEANPED = '0' and CLEANPED = '1')) then
		  -- back to run => clean pedestal
		  GEN_FORCEPED <= '1';
		  counter := FORCEPED_TIME;
		  
      else
        if(GEN_FORCEPED = '1') then
          if (counter = 0) then
			   GEN_FORCEPED <= '0';
		    else
            counter := counter-1;
		    end if;
		  end if;
      end if;
    end if;
  end process;
  
  process(CLK, GEN_FORCEPED, OLD_FORCEPED)
  begin
    if(clk'event and clk = '1') then
	   if (MASKED_BUSY = '1')then
		  -- clear RUNMODE if board is busy
		  GEN_RUNMODE <= '0';
	   elsif (OLD_FORCEPED = '1' and GEN_FORCEPED = '0') then
		  -- set RUNMODE if pedestal build is finished
		  GEN_RUNMODE <= '1';
		end if;
	 end if;
  end process;
  
  --or output signal with control bits
  RUNMODE <= GEN_RUNMODE or FORCERUN;
  FORCEPED <= GEN_FORCEPED;
  
end Behavioral;

