
----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Luca Galli, Marco Francesconi, Manuel Meucci
-- 
-- Create Date:    16:54:27 11/22/2017 
-- Design Name: 
-- Module Name:    main_vhd - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: here all the tdc-based trigger algorithms
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_MISC.ALL;
--use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
--use ieee.std_logic_unsigned.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
library UNISIM;
use UNISIM.VComponents.all;

entity maintdc is
  Port (CLK   : in STD_LOGIC;
    -- TDC Data
    DATAIN    : in TDCInputData;
    -- config 
    OFFSET    : in OffsetData;
    POLARITY  : in STD_LOGIC;
    TDCCHMASK : in  STD_LOGIC_VECTOR(15 downto 0);
    -- channel hit time
    DATAOUT   : out TimeData;
    HIT       : out STD_LOGIC_VECTOR(15 downto 0);
    --OR of all inputs (with masking)
    PULSE     : out STD_LOGIC;    
    --averaged hit time
    AVGTIME   : out AvgTimeData;
    AVGNTDC   : out AvgNTdcData
  );
end maintdc;

architecture Behavioral of maintdc is
-- delayed data from serdes (offset corrected)
  signal DLYDATAIN       : TDCInputData;

-- single channel time
  signal DATAOUT_TMP     : TimeData;
  signal HIT_TMP         : STD_LOGIC_VECTOR(15 downto 0);
  signal TDCSTATE_TMP    : STD_LOGIC_VECTOR(15 downto 0);
  signal TDCSTATE_MASKED : STD_LOGIC_VECTOR(15 downto 0);
  signal HIT_MASKED      : STD_LOGIC_VECTOR(15 downto 0);

-- stretched signals
  signal TDCTIME_STRETCHED : TimeStretchedData;
  signal HIT_STRETCHED     : STD_LOGIC_VECTOR(15 downto 0);

-- delayed signals
  signal TDCTIME_DLY : TimeStretchedData;
  signal HIT_DLY     : STD_LOGIC_VECTOR(15 downto 0);

-- should merge signal
  signal MERGE : STD_LOGIC;

begin
-- instantiation of 16 tdc WITH STRETCHER AND DELAY
  g_tdc : FOR i IN 0 TO 15 GENERATE
   i_offset_corr: entity megtrigger_v1_00_a.tdc_offset
    GENERIC MAP(
      CGN_TDCSIZE => TDC_INPUT_DIM,
      CGN_DLYSIZE => OFFSET_DIM - TIME_DIM 
    )
    PORT MAP(
      CLK_I => CLK,
      DLY_I => OFFSET(i),
      TDCDATA_I => DATAIN(i),
      TDCDATA_O => DLYDATAIN(i)
    );

    i_tdc : entity megtrigger_v1_00_a.tdc
    GENERIC MAP (TIME_DIM) PORT MAP (
      CLK            => CLK,
      TDCDATA_I      => DLYDATAIN(i),
      NEGPOLARITY_I  => POLARITY,
      TDCTIME_O      => DATAOUT_TMP(i),
      STATE_O        => TDCSTATE_TMP(i),
      HIT_O          => HIT_TMP(i)
    );

    DATAOUT(i) <= DATAOUT_TMP(i);
    TDCSTATE_MASKED(i) <= TDCSTATE_TMP(i) when (TDCCHMASK(i) = '0') else '0';
    HIT_MASKED(i) <= HIT_TMP(i) when (TDCCHMASK(i) = '0') else '0';
    HIT(i) <= HIT_MASKED(i);

    i_tdc_stretch : entity megtrigger_v1_00_a.tdc_stretch
    GENERIC MAP (
      CGN_WIDTH_SIZE => TDC_STRETCHING
    )
    PORT MAP(
      CLK => CLK,
      TDCTIME_I => DATAOUT_TMP(i),
      HIT_I => HIT_MASKED(i),
      TDCTIME_O => TDCTIME_STRETCHED(i),
      HIT_O => HIT_STRETCHED(i)
    );

    i_tdc_dly: entity megtrigger_v1_00_a.tdc_delay
    GENERIC MAP(
      CGN_DLY => 1,
      CGN_TDCSIZE => TDCTIME_STRETCHED(i)'length
    )
    PORT MAP(
      CLK_I => CLK,
      HIT_I => HIT_STRETCHED(i),
      TDCTIME_I => TDCTIME_STRETCHED(i),
      HIT_O => HIT_DLY(i),
      TDCTIME_O => TDCTIME_DLY(i)
    );

  END GENERATE g_tdc;

  tdc_merge_trigger: entity megtrigger_v1_00_a.tdc_trigger
  GENERIC MAP (
    CGN_DLY => 0
  )
  PORT MAP(
    CLK_I => CLK,
    HIT_I => HIT_STRETCHED,
    STATE_I => TDCSTATE_MASKED,
    PULSE_O => PULSE,
    MERGE_O => MERGE
  );

  tdc_sum: entity megtrigger_v1_00_a.tdc_sum
  GENERIC MAP(
    CGN_TDCSIZE => TDCTIME_DLY(0)'length
  )
  PORT MAP(
    CLK_I => CLK,
    HIT_I => HIT_DLY,
    TDCTIME_I => TDCTIME_DLY,
    MERGE_I => MERGE,
    TDCTIME_O => AVGTIME,
    NTDC_O => AVGNTDC
  );

end Behavioral;
