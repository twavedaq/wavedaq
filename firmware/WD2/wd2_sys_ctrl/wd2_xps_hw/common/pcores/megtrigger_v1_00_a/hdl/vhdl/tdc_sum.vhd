library ieee;
use ieee.std_logic_1164.all;
use IEEE.numeric_std.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

entity TDC_SUM is
  generic(
    CGN_TDCSIZE    : natural := 4
  );
  port(
    CLK_I         : in std_logic;
    HIT_I         : in std_logic_vector(15 downto 0);
    TDCTIME_I     : in TimeStretchedData;
    MERGE_I       : in std_logic;
    TDCTIME_O     : out AvgTimeData;
    NTDC_O        : out AvgNTdcData
   );
end;

architecture BEHAVIORAL of TDC_SUM is
  signal tdctime_masked : TimeStretchedData;
  signal tdctime_sum1 : std_logic_vector(8*(CGN_TDCSIZE+1)-1 downto 0);
  signal nhit_sum1 : std_logic_vector(8*2-1 downto 0);
  signal tdctime_sum2 : std_logic_vector(4*(CGN_TDCSIZE+2)-1 downto 0);
  signal nhit_sum2 : std_logic_vector(4*3-1 downto 0);
  signal tdctime_sum3 : std_logic_vector(2*(CGN_TDCSIZE+3)-1 downto 0);
  signal nhit_sum3 : std_logic_vector(2*4-1 downto 0);
  signal tdctime_sum4 : std_logic_vector((CGN_TDCSIZE+4)-1 downto 0);
  signal nhit_sum4 : std_logic_vector(5-1 downto 0);
  signal merge :std_logic_vector(2 downto 0);
begin

  masking: for I in 0 to 15 generate
    tdctime_masked(I) <= TDCTIME_I(I) when (HIT_I(I) = '1')
                         else (others => '0');
  end generate;
  

  adder_ree: process(CLK_I)
  begin
    if(rising_edge(CLK_I)) then
	   merge <= merge(1 downto 0) & MERGE_I;
      for I in 0 to 7 loop
        tdctime_sum1((CGN_TDCSIZE+1)*(I+1)-1 downto (CGN_TDCSIZE+1)*I) <= std_logic_vector(unsigned('0' & tdctime_masked(2*I)) + unsigned('0' & tdctime_masked(2*I+1)));
        nhit_sum1(2*(I+1)-1 downto 2*I) <= std_logic_vector(unsigned('0' & HIT_I(2*I+1 downto 2*I+1))+unsigned('0' & HIT_I(2*I downto 2*I)));
		end loop;
      for I in 0 to 3 loop
        tdctime_sum2((CGN_TDCSIZE+2)*(I+1)-1 downto (CGN_TDCSIZE+2)*I) <= std_logic_vector(unsigned('0' & tdctime_sum1((CGN_TDCSIZE+1)*(2*I+1)-1 downto (CGN_TDCSIZE+1)*2*I)) + unsigned('0' & tdctime_sum1((CGN_TDCSIZE+1)*(2*I+2)-1 downto (CGN_TDCSIZE+1)*(2*I+1))));
		  nhit_sum2(3*(I+1)-1 downto 3*I) <= std_logic_vector(unsigned('0' & nhit_sum1(2*(2*I+1)-1 downto 2*(2*I)))+unsigned('0' & nhit_sum1(2*(2*I+2)-1 downto 2*(2*I+1))));
		end loop;
      for I in 0 to 1 loop
        tdctime_sum3((CGN_TDCSIZE+3)*(I+1)-1 downto (CGN_TDCSIZE+3)*I) <= std_logic_vector(unsigned('0' & tdctime_sum2((CGN_TDCSIZE+2)*(2*I+1)-1 downto (CGN_TDCSIZE+2)*2*I)) + unsigned('0' & tdctime_sum2((CGN_TDCSIZE+2)*(2*I+2)-1 downto (CGN_TDCSIZE+2)*(2*I+1))));
		  nhit_sum3(4*(I+1)-1 downto 4*I) <= std_logic_vector(unsigned('0' & nhit_sum2(3*(2*I+1)-1 downto 3*(2*I)))+unsigned('0' & nhit_sum2(3*(2*I+2)-1 downto 3*(2*I+1))));
		end loop;
		if(merge(2)='1') then
        tdctime_sum4((CGN_TDCSIZE+4)-1 downto 0) <= std_logic_vector(unsigned('0' & tdctime_sum3((CGN_TDCSIZE+3)-1 downto 0)) + unsigned('0' & tdctime_sum3((CGN_TDCSIZE+3)*2-1 downto (CGN_TDCSIZE+3))));
        nhit_sum4 <= std_logic_vector(unsigned('0' & nhit_sum3(3 downto 0))+unsigned('0' & nhit_sum3(7 downto 4)));
		else
		  nhit_sum4 <= (others =>'0');
		end if;
    end if;
  end process;

  TDCTIME_O <= tdctime_sum4;
  NTDC_O <= nhit_sum4;

end BEHAVIORAL;
