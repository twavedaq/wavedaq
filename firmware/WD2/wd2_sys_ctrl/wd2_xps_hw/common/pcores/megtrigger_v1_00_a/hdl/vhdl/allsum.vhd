----------------------------------------------------------------------------------
-- Company: 
-- Engineer: Marco Francesconi
-- 
-- Create Date:    16:16:50 21/05/2016 
-- Design Name: 
-- Module Name:    allsum - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: this module sum up the waveforms
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values

Library UNISIM;
use UNISIM.vcomponents.all;

library megtrigger_v1_00_a;
use megtrigger_v1_00_a.common.ALL;

entity allsum is
  Port (
    din : in  ADCFirstSumData;
    clk : in  STD_LOGIC;
    --sync : in  STD_LOGIC;
    dout : out  ADCSummedData
  );
end allsum;

architecture Behavioral of allsum is

-- types and signal for adder tree
  TYPE ADCSecondSumData IS ARRAY (3 downto 0) OF STD_LOGIC_VECTOR (ADC_INPUT_DIM+CAL_DIM+2 downto 0); --size is ADC_INPUT_DIM + 1(pedestal) + CAL_DIM + 2 (SUM)
  TYPE ADCThirdSumData IS ARRAY (1 downto 0) OF STD_LOGIC_VECTOR (ADC_INPUT_DIM+CAL_DIM+3 downto 0); --size is ADC_INPUT_DIM + 1(pedestal) + CAL_DIM + 3 (SUM)
  SIGNAL SecOut : ADCSecondSumData;
  SIGNAL ThiOut : ADCThirdSumData;

begin

--logic tree using both clock edges
--second stage
  g_secondstage_adc : FOR i IN 0 TO 3 GENERATE
    PROCESS (clk) begin
      if(clk'event and clk = '0') then
        SecOUT(i) <= STD_LOGIC_VECTOR(Resize(SIGNED(din(2*i)), SecOUT(i)'length) + Resize(SIGNED(din(2*i+1)), SecOUT(i)'length));
      end if;
    END PROCESS;
  END GENERATE g_secondstage_adc;

--third stage
  g_thirdstage_adc : FOR i IN 0 TO 1 GENERATE
    PROCESS (clk) begin
      if(clk'event and clk = '0') then
        ThiOUT(i) <= STD_LOGIC_VECTOR(Resize(SIGNED(SecOUT(2*i)), ThiOUT(i)'length) + Resize(SIGNED(SecOUT(2*i+1)), ThiOUT(i)'length));
      end if;
    END PROCESS;
  END GENERATE g_thirdstage_adc;

--last stage
  PROCESS (clk) begin
    if(clk'event and clk = '0') then
      dout <= STD_LOGIC_VECTOR(Resize(SIGNED(ThiOUT(0)), dout'length) + Resize(SIGNED(ThiOUT(1)), dout'length));
    end if;
  END PROCESS;

end Behavioral;

