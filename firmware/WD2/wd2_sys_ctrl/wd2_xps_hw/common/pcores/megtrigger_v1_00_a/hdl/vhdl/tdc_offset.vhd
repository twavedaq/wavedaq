library ieee;
use ieee.std_logic_1164.all;
use IEEE.math_real.all;
use IEEE.numeric_std.ALL;

entity TDC_OFFSET is
  generic(
    CGN_TDCSIZE    : natural := 8;
    CGN_DLYSIZE    : natural := 1
  );
  port(
    CLK_I         : in std_logic;
    DLY_I         : in std_logic_vector(CGN_DLYSIZE+integer(ceil(log2(real(CGN_TDCSIZE))))-1 downto 0);
    TDCDATA_I     : in std_logic_vector(CGN_TDCSIZE-1 downto 0);
    TDCDATA_O     : out std_logic_vector(CGN_TDCSIZE-1 downto 0)
   );
end;

architecture BEHAVIORAL of TDC_OFFSET is
   signal TDCDATA_REG : std_logic_vector(CGN_TDCSIZE-2 downto 0);
   signal TDCDATA_TOMUX : std_logic_vector(CGN_TDCSIZE+CGN_TDCSIZE-2 downto 0);
   signal TDCDATA_MUX : std_logic_vector(CGN_TDCSIZE-1 downto 0);
   signal FRAC_DLY_SEL : std_logic_vector(integer(ceil(log2(real(CGN_TDCSIZE))))-1 downto 0);
   signal REG_DLY_SEL : std_logic_vector(CGN_DLYSIZE-1 downto 0);
begin
   --prepares data for mux
   process(CLK_I)
   begin
     if(rising_edge(CLK_I)) then
       TDCDATA_REG <= TDCDATA_I(CGN_TDCSIZE-2 downto 0);
     end if;
   end process;
   TDCDATA_TOMUX <= TDCDATA_REG & TDCDATA_I;

   -- decode delay
   FRAC_DLY_SEL <= DLY_I(integer(ceil(log2(real(CGN_TDCSIZE))))-1 downto 0);
   REG_DLY_SEL <= DLY_I(CGN_DLYSIZE+integer(ceil(log2(real(CGN_TDCSIZE))))-1 downto integer(ceil(log2(real(CGN_TDCSIZE)))));

   -- mux
   --process(CLK_I)
   --begin
   --  if(rising_edge(CLK_I)) then
   --    TDCDATA_MUX <= TDCDATA_TOMUX(to_integer(unsigned(FRAC_DLY_SEL))+CGN_TDCSIZE-1 downto to_integer(unsigned(FRAC_DLY_SEL)));
   --  end if;
   --end process;
   g_mux: for I in 0 to CGN_TDCSIZE-1 generate
      process(CLK_I)
      begin
         if(rising_edge(CLK_I)) then
            if(to_integer(unsigned(FRAC_DLY_SEL))<CGN_TDCSIZE) then
               TDCDATA_MUX(I) <= TDCDATA_TOMUX(I+to_integer(unsigned(FRAC_DLY_SEL)));
            else
               TDCDATA_MUX(I) <= '0';
            end if;
         end if;
      end process;
   end generate;

   -- clocked delay
   no_dly: if(CGN_DLYSIZE = 0) generate
       TDCDATA_O <= TDCDATA_MUX;
   end generate;
   sr_dly: if(CGN_DLYSIZE /= 0) generate
      type sr_type is array (2**CGN_DLYSIZE-1 downto 0) of std_logic_vector(CGN_TDCSIZE-1 downto 0);
      signal tdcdata_dly: sr_type;
   begin
      process(CLK_I)
      begin
        if(rising_edge(CLK_I)) then
          tdcdata_dly <= tdcdata_dly(2**CGN_DLYSIZE-2 downto 0) & TDCDATA_MUX;
        end if;
      end process;
      TDCDATA_O <= tdcdata_dly(to_integer(unsigned(REG_DLY_SEL)));
   end generate;

end BEHAVIORAL;
