-- TestBench Template 
use std.env.stop;
use STD.textio.all;

library ieee;
   use ieee.std_logic_1164.ALL;
   use IEEE.std_logic_textio.all;

library megtrigger_v1_00_a;
   use megtrigger_v1_00_a.ALL;
   use megtrigger_v1_00_a.common.all;

ENTITY megtrigger_tb IS
END megtrigger_tb;

ARCHITECTURE behavior OF megtrigger_tb IS 

-- Component Declaration
component megtrigger
   Port (
      DATA_CLK_I        : in  STD_LOGIC;
      UC_CLK_I          : in  STD_LOGIC;
      SYSTEM_CLK_I      : in  STD_LOGIC;
      ADCDATA_I         : in  STD_LOGIC_VECTOR(ADC_INPUT_DIM*16-1 downto 0) ;
      TDCDATA_I         : in STD_LOGIC_VECTOR(TDC_INPUT_DIM*16-1 downto 0) ;
      SHAPER_TRG_I      : in STD_LOGIC_VECTOR(15 downto 0) ;
      CALDATA_I         : in STD_LOGIC_VECTOR(CAL_DIM*16-1 downto 0) ;
      OFFSETDATA_I      : in STD_LOGIC_VECTOR(OFFSET_DIM*16-1 downto 0) ;
      PEDCONF_I         : in STD_LOGIC_VECTOR(31 downto 0) ;
      THR0_I            : in STD_LOGIC_VECTOR(31 downto 0) ;
      THR1_I            : in STD_LOGIC_VECTOR(31 downto 0) ;
      THR2_I            : in STD_LOGIC_VECTOR(31 downto 0) ;
      DBGSERDES_I       : in STD_LOGIC_VECTOR(63 downto 0) ;
      TDCCHMASK_I       : in STD_LOGIC_VECTOR(15 downto 0) ;
      CTRLBUS_I         : in STD_LOGIC_VECTOR(31 downto 0) ;
      LOCAL_TRIG_O      : out STD_LOGIC;
      LOCAL_VETO_TRIG_O : out STD_LOGIC;
      SYNC_I            : in STD_LOGIC;
      DRS_BUSY_I        : in STD_LOGIC;
      TCBData_O         : out STD_LOGIC_VECTOR(63 downto 0)
   );
end component;

   CONSTANT clk_period : time := 12500 ps;

   SIGNAL clk : std_logic;
   SIGNAL uc_clk : std_logic;
   SIGNAL system_clk : std_logic;

   --input signals
   SIGNAL adc : ADCInputData;
   SIGNAL ADCDATA : STD_LOGIC_VECTOR (ADC_INPUT_DIM*16-1 downto 0);
   SIGNAL tdc : TDCInputData;
   SIGNAL TDCDATA : STD_LOGIC_VECTOR (TDC_INPUT_DIM*16-1 downto 0);
   SIGNAL COMP : STD_LOGIC_VECTOR (15 downto 0);

   --control bus
   SIGNAL CONTROLBUS : std_logic_vector(31 downto 0);

   SIGNAL BUSYMASK : std_logic;
   SIGNAL ALGSEL : std_logic_vector(1 downto 0);
   SIGNAL FORCERUN : std_logic;
   SIGNAL CLEANPED : std_logic;
   SIGNAL TDC_POLARITY : std_logic;
   SIGNAL DEBUG_CTRL : std_logic;

   --pedestal configuration
   SIGNAL PEDCONF : std_logic_vector(31 downto 0);

   SIGNAL PED_THR : STD_LOGIC_VECTOR(ADC_INPUT_DIM-1 downto 0);
   SIGNAL PED_DLY_ENABLE : STD_LOGIC_VECTOR(1 downto 0);
   SIGNAL PED_DLY_SELECT : STD_LOGIC_VECTOR(3 downto 0);
   SIGNAL PED_ADDER_SELECT : STD_LOGIC_VECTOR(2 downto 0);

   -- trgbus
   SIGNAL SYNC : std_logic;
   SIGNAL DRS_BUSY : std_logic;

   --calib
   SIGNAL calib : CalibData;
   SIGNAL CALDATA : STD_LOGIC_VECTOR (CAL_DIM*16-1 downto 0);

   --offset
   SIGNAL offset : OffsetData;
   SIGNAL OFFDATA : STD_LOGIC_VECTOR (OFFSET_DIM*16-1 downto 0);

   -- others
   SIGNAL TDCCHMASK : std_logic_vector(15 downto 0);
   SIGNAL DEBUGWORD : std_logic_vector(63 downto 0);
   SIGNAL TCBDATA : std_logic_vector(63 downto 0);
BEGIN

-- Component Instantiation
   uut: megtrigger 
      PORT MAP(
         DATA_CLK_I => clk, 
         UC_CLK_I => uc_clk,
         SYSTEM_CLK_I => system_clk,
         ADCDATA_I => ADCDATA,
         TDCDATA_I => TDCDATA,
         SHAPER_TRG_I => COMP,
         CALDATA_I => CALDATA,
         OFFSETDATA_I => OFFDATA,
         PEDCONF_I => PEDCONF,
         THR0_I => X"00000000",
         THR1_I => X"00000000",
         THR2_I => X"00000000",
         DBGSERDES_I => DEBUGWORD,
         TDCCHMASK_I => TDCCHMASK,
         CTRLBUS_I => CONTROLBUS,
         LOCAL_TRIG_O => open,
         LOCAL_VETO_TRIG_O => open,
         SYNC_I => SYNC,
         DRS_BUSY_I => DRS_BUSY,
         TCBData_O => TCBDATA
      );

   --clock generating process
   clock : PROCESS
   BEGIN
         clk <= '1';
         wait for clk_period/4;
         uc_clk <= '1';
         system_clk <= '1';
         wait for clk_period/4;
         clk <= '0';
         wait for clk_period/4;
         uc_clk <= '0';
         system_clk <= '0';
         wait for clk_period/4;
   END PROCESS clock;

   -- encode bus
   CONTROLBUS <= ( 0 => BUSYMASK,
                   4 => ALGSEL(0),
                   5 => ALGSEL(1),
                   7 => FORCERUN,
                   8 => CLEANPED,
                   9 => TDC_POLARITY,
                  10 => DEBUG_CTRL,
              others => '0');

   PEDCONF(ADC_INPUT_DIM-1 downto 0) <= PED_THR;
   PEDCONF(15 downto ADC_INPUT_DIM) <= (others => '0');
   PEDCONF(19 downto 16) <= PED_DLY_SELECT;
   PEDCONF(21 downto 20) <= PED_DLY_ENABLE;
   PEDCONF(23 downto 22) <= (others => '0');
   PEDCONF(26 downto 24) <= PED_ADDER_SELECT;
   PEDCONF(31 downto 27) <= (others => '0');

   g_calib : FOR i IN 0 TO 15 GENERATE
     ADCDATA(ADC_INPUT_DIM*(i+1)-1 downto ADC_INPUT_DIM*i) <= adc(i);
     TDCDATA(TDC_INPUT_DIM*(i+1)-1 downto TDC_INPUT_DIM*i) <= tdc(i);
     CALDATA(CAL_DIM*(i+1)-1 downto CAL_DIM*i) <= calib(i);
     OFFDATA(OFFSET_DIM*(i+1)-1 downto OFFSET_DIM*i) <= offset(i);
   END GENERATE g_calib;

   --  Test Bench Statements

   -- Fixed data
   BUSYMASK <= '0'; 
   --ALGSEL <= "01";
   FORCERUN <= '0';
   CLEANPED <= '0';
   --TDC_POLARITY <= '0';
   DEBUG_CTRL <= '1';
   --PED_THR <= (others => '0');
   --PED_DLY_SELECT <= "0000";
   --PED_DLY_ENABLE <= "00";
   --PED_ADDER_SELECT <= "111";
   DEBUGWORD <= X"DEADBEEFDEADBEEF";
   --SYNC <= '0';
   DRS_BUSY <= '0';
   --TDCCHMASK <= X"0000";

   COMP <= X"0000"; 

   --calib(0) <= (0 => '1', others=>'0');
   --calib(1) <= (0 => '1', others=>'0');
   --calib(2) <= (0 => '1', others=>'0');
   --calib(3) <= (0 => '1', others=>'0');
   --calib(4) <= (0 => '1', others=>'0');
   --calib(5) <= (0 => '1', others=>'0');
   --calib(6) <= (0 => '1', others=>'0');
   --calib(7) <= (0 => '1', others=>'0');
   --calib(8) <= (0 => '1', others=>'0');
   --calib(9) <= (0 => '1', others=>'0');
   --calib(10) <= (0 => '1', others=>'0');
   --calib(11) <= (0 => '1', others=>'0');
   --calib(12) <= (0 => '1', others=>'0');
   --calib(13) <= (0 => '1', others=>'0');
   --calib(14) <= (0 => '1', others=>'0');
   --calib(15) <= (0 => '1', others=>'0');
   ----calib(0) <= (others=>'0');
   --calib(1) <= (others=>'0');
   --calib(2) <= (others=>'0');
   --calib(3) <= (others=>'0');
   --calib(4) <= (others=>'0');
   --calib(5) <= (others=>'0');
   --calib(6) <= (others=>'0');
   --calib(7) <= (others=>'0');
   --calib(8) <= (others=>'0');
   --calib(9) <= (others=>'0');
   --calib(10) <= (others=>'0');
   --calib(11) <= (others=>'0');
   --calib(12) <= (others=>'0');
   --calib(13) <= (others=>'0');
   --calib(14) <= (others=>'0');
   --calib(15) <= (others=>'0');

   --adc(0) <= (others=>'0');
   --adc(1) <= (others=>'0');
   --adc(2) <= (others=>'0');
   --adc(3) <= (others=>'0');
   --adc(4) <= (others=>'0');
   --adc(5) <= (others=>'0');
   --adc(6) <= (others=>'0');
   --adc(7) <= (others=>'0');
   --adc(8) <= (others=>'0');
   --adc(9) <= (others=>'0');
   --adc(10) <= (others=>'0');
   --adc(11) <= (others=>'0');
   --adc(12) <= (others=>'0');
   --adc(13) <= (others=>'0');
   --adc(14) <= (others=>'0');
   --adc(15) <= (others=>'0');

   --tdc(0) <= (others=>'0');
   --tdc(1) <= (others=>'0');
   --tdc(2) <= (others=>'0');
   --tdc(3) <= (others=>'0');
   --tdc(4) <= (others=>'0');
   --tdc(5) <= (others=>'0');
   --tdc(6) <= (others=>'0');
   --tdc(7) <= (others=>'0');
   --tdc(8) <= (others=>'0');
   --tdc(9) <= (others=>'0');
   --tdc(10) <= (others=>'0');
   --tdc(11) <= (others=>'0');
   --tdc(12) <= (others=>'0');
   --tdc(13) <= (others=>'0');
   --tdc(14) <= (others=>'0');
   --tdc(15) <= (others=>'0');

   -- variable stimuli
   tb : PROCESS
      file input_file : TEXT open READ_MODE is "file_io.in";
      file output_file : TEXT open WRITE_MODE is "file_io.out";
      variable my_line : LINE;
      variable input_line :LINE;
      variable output_line : LINE;
      variable my_adc : std_logic_vector(ADC_INPUT_DIM-1 downto 0);
      variable my_tdc : std_logic_vector(TDC_INPUT_DIM-1 downto 0);
      variable my_cal : std_logic_vector(CAL_DIM-1 downto 0);
      variable my_offset : std_logic_vector(OFFSET_DIM-1 downto 0);
      variable my_algsel : std_logic_vector(1 downto 0);
      variable my_bit : std_logic;
      variable my_16bit : std_logic_vector(15 downto 0);
   BEGIN

      --conf values
      readline(input_file, input_line);
      read(input_line, my_algsel);
      read(input_line, my_bit);
      read(input_line, my_16bit);
      ALGSEL <= my_algsel;
      TDC_POLARITY <= my_bit;
      TDCCHMASK <= my_16bit;
      write(my_line, string'("*************ALGSEL: "));
      write(my_line, my_algsel);
      writeline(output, my_line);
      SYNC <= '0';

      --weights
      readline(input_file, input_line);
      for iCh in 0 to 15 loop
        read(input_line, my_cal);
        calib(iCh) <= my_cal;
        write(my_line, string'("*************Ch"));
        write(my_line, iCh);
        write(my_line, string'(": "));
        write(my_line, my_cal);
        writeline(output, my_line);
      end loop;

      --offsets
      readline(input_file, input_line);
      for iCh in 0 to 15 loop
        read(input_line, my_offset);
        offset(iCh) <= my_offset;
        write(my_line, string'("*************Ch"));
        write(my_line, iCh);
        write(my_line, string'(": "));
        write(my_line, my_offset);
        writeline(output, my_line);
      end loop;

      wait for 125 ns; -- wait until global set/reset completes

      SYNC <= '1';
      wait for 4*clk_period;
      SYNC <= '0';
      wait for clk_period;

      loop
        exit when endfile(input_file);
        readline(input_file, input_line);
        for iCh in 0 to 15 loop
           read(input_line, my_adc);
           adc(iCh) <= my_adc;
           read(input_line, my_tdc);
           tdc(iCh) <= my_tdc;
        end loop;
        write(output_line, tcbdata);
        writeline(output_file, output_line);
        wait for clk_period;
      end loop;

      wait for 10*clk_period;

      report "*** Test SUCCESSFULL ****"  severity failure ;

   END PROCESS tb;

--  End Test Bench 
END;
