---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  LMK Sync Logic
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.12.2015 12:15:01
--
--  Description :  Logic to synchronnize LMKs on all WaveDream boards.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_lmk_sync_logic is
  generic (
    CGN_NO_PIPELINE_FFS        : integer := 1;
    CGN_EXTRA_PLL_RST_CYCLES   : integer := 4;
    CGN_EXTRA_ADCIF_RST_CYCLES : integer := 6
  );
  port (
    BACKPLANE_PLUGGED_N_I  : in  std_logic;
    LMK_SYNC_BACKPLANE_P_I : in  std_logic;
    LMK_SYNC_BACKPLANE_N_I : in  std_logic;
    LMK_SYNC_LOCAL_I       : in  std_logic;
    LMK_SYNC_N_O           : out std_logic;
    LMK_SYNC_N_I           : in  std_logic;
    LMK_SYNC_N_T           : out std_logic;
    LOGIC_SYNC_O           : out std_logic;
    BACKPLANE_SYNC_O       : out std_logic;
    LMK_SYNC_MONITOR_N_O   : out std_logic;
    PLL_RST_O              : out std_logic;
    ADCIF_RST_O            : out std_logic;
    ADC_RST_O              : out std_logic;
    SRST_I                 : in  std_logic;
    CLK_SYS_I              : in  std_logic;
    CLK_DIV_DATA_I         : in  std_logic;
    CLK_I                  : in  std_logic
  );
end util_lmk_sync_logic;

architecture behavioral of util_lmk_sync_logic is

  constant C_ADCIF_RST_LEN : integer := CGN_NO_PIPELINE_FFS+CGN_EXTRA_ADCIF_RST_CYCLES;

  type type_pipeline_ff is array (CGN_NO_PIPELINE_FFS downto 0) of std_logic;

  signal lmk_sync_backplane   : std_logic := '0';
  signal pipeline_ff_n        : type_pipeline_ff := (others=>'0');
  signal lmk_sync_backplane_s : std_logic := '1';
  signal output_iob_ff_n     : std_logic := '1';
  
  signal sync_local           : std_logic := '0';
  signal sync_local_s0        : std_logic := '0';
  signal sync_local_edge      : std_logic := '0';
  signal sync_local_edge_s0   : std_logic := '0';
  signal sync_local_edge_s1   : std_logic := '0';

  signal pll_rst_chain        : std_logic_vector(CGN_EXTRA_PLL_RST_CYCLES downto 0) := (others=>'0');
  signal adcif_rst_chain      : std_logic_vector(C_ADCIF_RST_LEN downto 0) := (others=>'0');

  signal adc_rst_s0           : std_logic := '0';
  signal adc_rst_s1           : std_logic := '0';
  signal adc_rst              : std_logic := '0';
  
  attribute keep : string;
  attribute keep of output_iob_ff_n: signal is "TRUE";

begin

  sync_ibufds_inst : IBUFDS
  generic map (
    DIFF_TERM => FALSE,
    IBUF_LOW_PWR => TRUE,
    IOSTANDARD => "LVDS_25")
  port map (
    O  => lmk_sync_backplane,
    I  => LMK_SYNC_BACKPLANE_P_I,
    IB => LMK_SYNC_BACKPLANE_N_I
  );
  
  BACKPLANE_SYNC_O <= lmk_sync_backplane;
  
  IDDR2_inst : IDDR2
  generic map(
    DDR_ALIGNMENT => "NONE",
    INIT_Q0 => '0',
    INIT_Q1 => '0',
    SRTYPE => "ASYNC")
  port map (
    Q0 => lmk_sync_backplane_s,
    Q1 => open,
    C0 => CLK_I,
    C1 => not CLK_I,
    CE => '1',
    D => lmk_sync_backplane,
    R => BACKPLANE_PLUGGED_N_I,
    S => '0'
  );
  
  cdc_sync_sys_to_trigger_delay: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I    => CLK_SYS_I,
    CLK_B_I    => CLK_I,
    PORT_A_I(0) => LMK_SYNC_LOCAL_I,
    PORT_B_O(0) => sync_local
  );
  
  cdc_sync_glob_to_sys: cdc_sync  
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  1,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I    => CLK_I,
    CLK_B_I    => CLK_SYS_I,
    PORT_A_I(0) => adc_rst,
    PORT_B_O(0) => ADC_RST_O
  );

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      sync_local_s0      <= sync_local;
      sync_local_edge_s0 <= sync_local_edge;
      sync_local_edge_s1 <= sync_local_edge_s0;
      if sync_local_edge_s1 = '1' then
        sync_local_edge <= '0';
      elsif sync_local = '1' and sync_local_s0 = '0' then
        sync_local_edge <= '1';
      end if;
    end if;
  end process;
  
  pipeline_ff_n(0) <= not (lmk_sync_backplane_s or sync_local_edge);

  synch_ffs : for ff_number in 1 to CGN_NO_PIPELINE_FFS generate
    single_sync_ff : process(CLK_I)
    begin
      if rising_edge(CLK_I) then
        if SRST_I = '1' then
          pipeline_ff_n(ff_number) <= '0';
        else
          pipeline_ff_n(ff_number) <= pipeline_ff_n(ff_number-1);
        end if;
      end if;
    end process single_sync_ff;
  end generate synch_ffs;

  output_ff : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if SRST_I = '1' then
        output_iob_ff_n <= '1';
      else
        output_iob_ff_n <= pipeline_ff_n(CGN_NO_PIPELINE_FFS);
      end if;
    end if;
  end process output_ff;
  
  LMK_SYNC_N_O         <= output_iob_ff_n;
  LMK_SYNC_MONITOR_N_O <= LMK_SYNC_N_I;
  LMK_SYNC_N_T         <= '0';
  LOGIC_SYNC_O         <= not  pipeline_ff_n(CGN_NO_PIPELINE_FFS);
  
  -- delay releas of pll reset
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if pipeline_ff_n(CGN_NO_PIPELINE_FFS) = '0' then
        pll_rst_chain <= (others=>'1');
      else
        pll_rst_chain <= pll_rst_chain(CGN_EXTRA_PLL_RST_CYCLES-1 downto 0) & '0';
      end if;
    end if;
  end process;

  PLL_RST_O <= pll_rst_chain(CGN_EXTRA_PLL_RST_CYCLES);
  
  -- stretch ADC IF reset
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if pipeline_ff_n(0) = '0' then
        adcif_rst_chain <= (others=>'1');
      elsif rising_edge(CLK_I) then
        adcif_rst_chain <= adcif_rst_chain(C_ADCIF_RST_LEN-1 downto 0) & '0';
      end if;
    end if;
  end process;

  ADCIF_RST_O <= adcif_rst_chain(C_ADCIF_RST_LEN);

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if SRST_I = '1' then
        adc_rst_s0 <= '0';
        adc_rst_s1 <= '0';
      else
        adc_rst_s0 <= pipeline_ff_n(CGN_NO_PIPELINE_FFS);
        adc_rst_s1 <= adc_rst_s0;
      end if;
    end if;
  end process single_sync_ff;

  adc_rst <= not(adc_rst_s1) and pipeline_ff_n(CGN_NO_PIPELINE_FFS);
  
end architecture behavioral;
