---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Build Date and Time Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  03.11.2016 12:14:34
--
--  Description :  Provide build date and time to firmware.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity util_build_date_time is
  generic (
    CGN_DATE_YEAR     : integer := 0;
    CGN_DATE_MONTH    : integer := 1;
    CGN_DATE_DAY      : integer := 1;
    CGN_TIME_HOUR     : integer := 0;
    CGN_TIME_MINUTE   : integer := 0;
    CGN_TIME_SECOND   : integer := 0;
    CGN_BCD_FORMAT    : integer := 1
  );
  port (
    BUILD_YEAR_O   : out std_logic_vector(15 downto 0);
    BUILD_MONTH_O  : out std_logic_vector( 7 downto 0);
    BUILD_DAY_O    : out std_logic_vector( 7 downto 0);
    BUILD_HOUR_O   : out std_logic_vector( 7 downto 0);
    BUILD_MINUTE_O : out std_logic_vector( 7 downto 0);
    BUILD_SECOND_O : out std_logic_vector( 7 downto 0)
  );
end util_build_date_time;

architecture behavioral of util_build_date_time is

begin

  BCD_FORMAT : if CGN_BCD_FORMAT > 0 generate
    BUILD_YEAR_O(15 downto 12)   <= conv_std_logic_vector( CGN_DATE_YEAR/1000,          4);
    BUILD_YEAR_O(11 downto  8)   <= conv_std_logic_vector((CGN_DATE_YEAR mod 1000)/100, 4);
    BUILD_YEAR_O( 7 downto  4)   <= conv_std_logic_vector((CGN_DATE_YEAR mod 100) /10 , 4);
    BUILD_YEAR_O( 3 downto  0)   <= conv_std_logic_vector((CGN_DATE_YEAR mod 10)      , 4);
    BUILD_MONTH_O(7 downto  4)   <= conv_std_logic_vector( CGN_DATE_MONTH/10,           4);
    BUILD_MONTH_O(3 downto  0)   <= conv_std_logic_vector( CGN_DATE_MONTH mod 10,       4);
    BUILD_DAY_O(7 downto 4)      <= conv_std_logic_vector( CGN_DATE_DAY/10,             4);
    BUILD_DAY_O(3 downto 0)      <= conv_std_logic_vector( CGN_DATE_DAY mod 10,         4);
    BUILD_HOUR_O(7 downto 4)     <= conv_std_logic_vector( CGN_TIME_HOUR/10,            4);
    BUILD_HOUR_O(3 downto 0)     <= conv_std_logic_vector( CGN_TIME_HOUR mod 10,        4);
    BUILD_MINUTE_O(7 downto 4)   <= conv_std_logic_vector( CGN_TIME_MINUTE/10,          4);
    BUILD_MINUTE_O(3 downto 0)   <= conv_std_logic_vector( CGN_TIME_MINUTE mod 10,      4);
    BUILD_SECOND_O(7 downto 4)   <= conv_std_logic_vector( CGN_TIME_SECOND/10,          4);
    BUILD_SECOND_O(3 downto 0)   <= conv_std_logic_vector( CGN_TIME_SECOND mod 10,      4);
  end generate BCD_FORMAT;

  DECIMAL_FORMAT : if CGN_BCD_FORMAT = 0 generate
    BUILD_YEAR_O   <= conv_std_logic_vector(CGN_DATE_YEAR,   16);
    BUILD_MONTH_O  <= conv_std_logic_vector(CGN_DATE_MONTH,   8);
    BUILD_DAY_O    <= conv_std_logic_vector(CGN_DATE_DAY,     8);
    BUILD_HOUR_O   <= conv_std_logic_vector(CGN_TIME_HOUR,    8);
    BUILD_MINUTE_O <= conv_std_logic_vector(CGN_TIME_MINUTE,  8);
    BUILD_SECOND_O <= conv_std_logic_vector(CGN_TIME_SECOND,  8);
  end generate DECIMAL_FORMAT;

end architecture behavioral;