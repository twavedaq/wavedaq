---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Synchronization Register
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  16.09.2014 10:24:30
--
--  Description :  Generic ODDR output to be insertet in xps designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_oddr_reg is
  generic (
    CGN_DDR_ALIGNMENT  : string    := "NONE"; -- Sets output alignment to "NONE", "C0", "C1" 
    CGN_INIT           : bit := '0';    -- Sets initial state of the Q output to '0' or '1'
    CGN_SRTYPE         : string    := "SYNC"; -- Specifies "SYNC" or "ASYNC" set/reset
    CGN_DIFF_OUT       : boolean   := FALSE;
    CGN_REGISTER_WIDTH : integer   := 32
  );
  port (
    Q_O    : out std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0); -- 1-bit output data
    Q_N_O  : out std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0); -- 1-bit output data of diff pair
    D0_I   : in  std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0); -- 1-bit data input (associated with C0)
    D1_I   : in  std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0); -- 1-bit data input (associated with C1)
    R_I    : in  std_logic; -- 1-bit reset input 
    S_I    : in  std_logic; -- 1-bit set input   
    CE_I   : in  std_logic; -- 1-bit clock enable input
    C0_I   : in  std_logic  -- 1-bit clock input
  );
end util_oddr_reg;

architecture behavioral of util_oddr_reg is

  signal q    : std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0);
  signal c0_n : std_logic := '0';

begin

  c0_n <= not C0_I;
  
  oddr2_inst : for i in CGN_REGISTER_WIDTH-1 downto 0 generate

    ODDR2_inst : ODDR2
    generic map(
      DDR_ALIGNMENT => CGN_DDR_ALIGNMENT,
      INIT          => CGN_INIT,
      SRTYPE        => CGN_SRTYPE)
    port map (
      Q  => q(i),
      C0 => C0_I,
      C1 => c0_n,
      CE => CE_I,
      D0 => D0_I(i),
      D1 => D1_I(i),
      R  => R_I,
      S  => S_I
    );

    diff_out_buffer : if CGN_DIFF_OUT = TRUE generate
      OBUFDS_inst : OBUFDS
      generic map (
        IOSTANDARD => "DEFAULT")
      port map (
        O  => Q_O(i),   -- Diff_p output (connect directly to top-level port)
        OB => Q_N_O(i), -- Diff_n output (connect directly to top-level port)
        I  => q(i)      -- Buffer input 
      );
    end generate diff_out_buffer;
  
    se_out_buffer : if CGN_DIFF_OUT = FALSE generate
      Q_O(i) <= q(i);
    end generate se_out_buffer;

  end generate oddr2_inst;

end architecture behavioral;