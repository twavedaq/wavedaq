--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : mac_cmp.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    compare mac address to MAC_ADDR_I or broadcast or promiscuous.
--    MAC_VALID_O stays set until beginning of new frame.
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity mac_cmp is
  port
  (
    CLK_I            : in  std_logic;
    RESET_I          : in  std_logic;
    DATA_I           : in  std_logic_vector(7 downto 0);
    DATA_VALID_I     : in  std_logic;
    MAC_PROMISC_EN_I : in  std_logic;
    MAC_BRDCST_EN_I  : in  std_logic;
    MAC_ADDR_I       : in  std_logic_vector(47 downto 0);
    MAC_VALID_O      : out std_logic
  );
end mac_cmp;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behaviour of mac_cmp is

  signal data_r       : std_logic_vector( 7 downto 0);
  signal mac_addr_cmp : std_logic;
  signal mac_bcst_cmp : std_logic;


  type state_type is (S_IDLE, S_START, S_MAC_0, S_MAC_1, S_MAC_2, S_MAC_3, S_MAC_4, S_MAC_5, S_MAC_CMP);
  signal state : state_type;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

begin

  mac_proc: process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if (RESET_I = '1') then
        MAC_VALID_O <= '0';
        state       <= S_IDLE;
      else

        -- register data
        data_r <= DATA_I;

        case state is
          when S_IDLE =>       -- wait for inter frame gap
                               if (DATA_VALID_I = '0') then
                                 state <= S_START;
                               end if;

          when S_START =>      -- wait for start of new frame
                               if (DATA_VALID_I = '1') then
                                 MAC_VALID_O   <= '0';
                                 mac_addr_cmp  <= '1';
                                 mac_bcst_cmp  <= '1';
                                 state <= S_MAC_0;
                               end if;

          when S_MAC_0 =>      if (data_r /= MAC_ADDR_I(47 downto 40)) then
                                 mac_addr_cmp  <= '0';
                               end if;

                               if (data_r /= x"ff") then
                                 mac_bcst_cmp  <= '0';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_MAC_1;
                               else
                                 state <= S_START;
                               end if;

          when S_MAC_1 =>      if (data_r /= MAC_ADDR_I(39 downto 32)) then
                                 mac_addr_cmp  <= '0';
                               end if;

                               if (data_r /= x"ff") then
                                 mac_bcst_cmp  <= '0';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_MAC_2;
                               else
                                 state <= S_START;
                               end if;

          when S_MAC_2 =>      if (data_r /= MAC_ADDR_I(31 downto 24)) then
                                 mac_addr_cmp  <= '0';
                               end if;

                               if (data_r /= x"ff") then
                                 mac_bcst_cmp  <= '0';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_MAC_3;
                               else
                                 state <= S_START;
                               end if;

          when S_MAC_3 =>      if (data_r /= MAC_ADDR_I(23 downto 16)) then
                                 mac_addr_cmp  <= '0';
                               end if;

                               if (data_r /= x"ff") then
                                 mac_bcst_cmp  <= '0';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_MAC_4;
                               else
                                 state <= S_START;
                               end if;

          when S_MAC_4 =>      if (data_r /= MAC_ADDR_I(15 downto 8) ) then
                                 mac_addr_cmp  <= '0';
                               end if;

                               if (data_r /= x"ff") then
                                 mac_bcst_cmp  <= '0';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_MAC_5;
                               else
                                 state <= S_START;
                               end if;

          when S_MAC_5 =>      if (data_r /= MAC_ADDR_I(7 downto 0) ) then
                                 mac_addr_cmp  <= '0';
                               end if;

                               if (data_r /= x"ff") then
                                 mac_bcst_cmp  <= '0';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_MAC_CMP;
                               else
                                 state <= S_START;
                               end if;

          when S_MAC_CMP =>    if ( (MAC_PROMISC_EN_I = '1') or ((MAC_BRDCST_EN_I = '1') and (mac_bcst_cmp = '1')) or (mac_addr_cmp = '1') ) then
                                 MAC_VALID_O <= '1';
                               end if;

                               if (DATA_VALID_I = '1') then
                                 state <= S_IDLE;
                               else
                                 state <= S_START;
                               end if;

          when others =>       state <= S_IDLE;
        end case;

      end if;
    end if;
  end process;

end architecture behaviour;
