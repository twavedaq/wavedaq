--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : user_logic.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library unisim;
use unisim.vcomponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 8
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------

    -- LocalLink 0 signals ----------------------------------------------------
--    LL_CLK_I                   : in  std_logic;

    LL8_TX_I_DATA_I            : in  std_logic_vector(7 downto 0);
    LL8_TX_I_SOF_N_I           : in  std_logic;
    LL8_TX_I_EOF_N_I           : in  std_logic;
    LL8_TX_I_SRC_RDY_N_I       : in  std_logic;
    LL8_TX_I_DST_RDY_N_O       : out std_logic;

    LL8_RX_O_DATA_O            : out std_logic_vector(7 downto 0);
    LL8_RX_O_SOF_N_O           : out std_logic;
    LL8_RX_O_EOF_N_O           : out std_logic;
    LL8_RX_O_SRC_RDY_N_O       : out std_logic;
    LL8_RX_O_DST_RDY_N_I       : in  std_logic;


    -- GMII 0 signals ---------------------------------------------------------
    GMII_TXD_O              : out std_logic_vector(7 downto 0);
    GMII_TX_EN_O            : out std_logic;
    GMII_TX_ER_O            : out std_logic;
    GMII_TX_CLK_O           : out std_logic;
    GMII_RXD_I              : in  std_logic_vector(7 downto 0);
    GMII_RX_DV_I            : in  std_logic;
    GMII_RX_ER_I            : in  std_logic;
    GMII_RX_CLK_I           : in  std_logic;

   -- MDIO + ctrl signals ---------------------------------------------------------
    MDC_O                   : out std_logic;
    MDIO_I                  : in  std_logic;
    MDIO_O                  : out std_logic;
    MDIO_T                  : out std_logic;

    GMAC_INTERRUPT_O        : out std_logic;
    PHY_RESET_N_O           : out std_logic;

    CLK_GMII_RX_O           : out std_logic;
    -- Reference clock for RGMII IODELAYs Need to supply a 200MHz clock
    CLK_IODELAY_REF_200_I   : in  std_logic;

    -- MII  signals ----------------------------------------------------------
    MII_TX_CLK_I            : in  std_logic;

    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  component gmii_phy_if is
  --  generic
  --  (
  --    C_NUM_REG                      : integer              := 8
  --  );
    port
    (
      -- GMII PHY signals ------------------------------------------------------
      GMII_PHY_TXD_O              : out std_logic_vector(7 downto 0);
      GMII_PHY_TX_EN_O            : out std_logic;
      GMII_PHY_TX_ER_O            : out std_logic;
      GMII_PHY_TX_CLK_O           : out std_logic;

      GMII_PHY_RXD_I              : in  std_logic_vector(7 downto 0);
      GMII_PHY_RX_DV_I            : in  std_logic;
      GMII_PHY_RX_ER_I            : in  std_logic;
      GMII_PHY_RX_CLK_I           : in  std_logic;

      -- GMII user signals -----------------------------------------------------
      GMII_USER_TXD_I              : in  std_logic_vector(7 downto 0);
      GMII_USER_TX_EN_I            : in  std_logic;
      GMII_USER_TX_ER_I            : in  std_logic;
      GMII_USER_TX_CLK_I           : in  std_logic;

      GMII_USER_RXD_O              : out std_logic_vector(7 downto 0);
      GMII_USER_RX_DV_O            : out std_logic;
      GMII_USER_RX_ER_O            : out std_logic;
      GMII_USER_RX_CLK_O           : out std_logic
    );
  end component;


  component gmii_to_ll8 is
  port
  (
    -- gmii interface
    GMII_RX_CLK_I        : in  std_logic;
    GMII_RXD_I           : in  std_logic_vector(7 downto 0);
    GMII_RX_DV_I         : in  std_logic;
    GMII_RX_ER_I         : in  std_logic;

    -- ll interface
    LL8_DATA_O           : out std_logic_vector(7 downto 0);
    LL8_SOF_N_O          : out std_logic;
    LL8_EOF_N_O          : out std_logic;
    LL8_SRC_RDY_N_O      : out std_logic;
    LL8_DST_RDY_N_I      : in  std_logic;

    -- MAC filter
    MAC_PROMISC_EN_I     : in  std_logic;
    MAC_BRDCST_EN_I      : in  std_logic;
    MAC_ADDR_I           : in  std_logic_vector(47 downto 0);

    -- ctrl signals
    RESET_I              : in  std_logic
  );
  end component;


  component ll8_to_gmii is
  port
  (
     CLK_I               : in std_logic;
     RESET_I             : in std_logic;
     IFG_COUNT_I         : in std_logic_vector(7 downto 0);

  -- ll interface
     LL_DATA_I           : in std_logic_vector(7 DOWNTO 0);
     LL_SOF_N_I          : in std_logic;
     LL_EOF_N_I          : in std_logic;
     LL_SRC_RDY_N_I      : in std_logic;
     LL_DST_RDY_N_O      : out std_logic;

  -- xgmii to xaui interface

     GMII_TXD_O          : out std_logic_vector(7 downto 0);
     GMII_TX_EN_O        : out std_logic;
     GMII_TX_ER_O        : out std_logic
  );
  end component;


  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg3                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg4                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg5                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg6                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg7                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel              : std_logic_vector(0 to 7);
  signal slv_reg_read_sel               : std_logic_vector(0 to 7);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;


  signal ctrl_reg_r                     : std_logic_vector(31 downto 0);
  signal ctrl_reg_w                     : std_logic_vector(31 downto 0);

  constant C_CTRL_REG_W_DEFAULT         : std_logic_vector(31 downto 0) := "00000000000000000000000000000000";

  ------------------------------------------
  -- Signals for ethernet phy
  ------------------------------------------

  signal clk_gmii_rx  : std_logic;

  signal gmii_txd     : std_logic_vector(7 downto 0);
  signal gmii_tx_en   : std_logic;
  signal gmii_tx_er   : std_logic;

  signal gmii_rxd     : std_logic_vector(7 downto 0);
  signal gmii_rx_dv   : std_logic;
  signal gmii_rx_er   : std_logic;


  signal reset        : std_logic;

  signal ifg_count    : std_logic_vector(7 downto 0);


  signal mac_promisc_en : std_logic;
  signal mac_brdcst_en  : std_logic;
  signal mac_addr       : std_logic_vector(47 downto 0);


begin


  --USER logic implementation added here

  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  --
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  --
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  --
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 7);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 7);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4) or Bus2IP_WrCE(5) or Bus2IP_WrCE(6) or Bus2IP_WrCE(7);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3) or Bus2IP_RdCE(4) or Bus2IP_RdCE(5) or Bus2IP_RdCE(6) or Bus2IP_RdCE(7);

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= C_CTRL_REG_W_DEFAULT;
--        slv_reg1 <= (others => '0');
--        slv_reg2 <= (others => '0');
        slv_reg3 <= (others => '0');
        slv_reg4 <= (others => '0');
        slv_reg5 <= (others => '0');
        slv_reg6 <= (others => '0');
        slv_reg7 <= (others => '0');
      else
        case slv_reg_write_sel is
          when "10000000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "01000000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= slv_reg0(byte_index*8 to byte_index*8+7) or Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00100000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= slv_reg0(byte_index*8 to byte_index*8+7) and not Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00010000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg3(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00001000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg4(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00000100" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg5(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00000010" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg6(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00000001" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg7(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1, slv_reg2, slv_reg3, slv_reg4, slv_reg5, slv_reg6, slv_reg7 ) is
  begin

    case slv_reg_read_sel is
      when "10000000" => slv_ip2bus_data <= slv_reg0;
      when "01000000" => slv_ip2bus_data <= slv_reg1;
      when "00100000" => slv_ip2bus_data <= slv_reg2;
      when "00010000" => slv_ip2bus_data <= slv_reg3;
      when "00001000" => slv_ip2bus_data <= slv_reg4;
      when "00000100" => slv_ip2bus_data <= slv_reg5;
      when "00000010" => slv_ip2bus_data <= slv_reg6;
      when "00000001" => slv_ip2bus_data <= slv_reg7;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';


  ------------------------------------------------------------------------------
  -- Register Usage
  ------------------------------------------------------------------------------

 ----------------------------------------------
 -- reg 0: CTRL REG
 -- reg 1  write: set   bits in CTRL REG
 -- reg 1 read: Status
 -- reg 2 clear bits in CTRL REG

  ctrl_reg_w <= slv_reg0;
  slv_reg1   <= ctrl_reg_r;
  slv_reg2   <=(others => '0');



  ctrl_reg_r(0)           <= MDIO_I;
  ctrl_reg_r(31 downto 1) <= (others => '0');

  MDIO_O                  <= ctrl_reg_w(4);
  MDIO_T                  <= ctrl_reg_w(5);
  MDC_O                   <= ctrl_reg_w(6);

  GMAC_INTERRUPT_O        <= '0';
  PHY_RESET_N_O           <= ctrl_reg_w(0);

 ----------------------------------------------
 -- reg 3
  ifg_count <= slv_reg3(24 to 31);


 ----------------------------------------------
 -- reg 4

 --  reserved

 ----------------------------------------------
 -- reg 5

--  reserved



 ----------------------------------------------
 -- reg 6,7: MAC Filter
  mac_addr(47 downto 16) <= slv_reg6;
  mac_addr(15 downto  0) <= slv_reg7(16 to 31);
  mac_brdcst_en          <= slv_reg7(0); -- MSB
  mac_promisc_en         <= slv_reg7(1);

  reset                  <= Bus2IP_Reset;

  ---------------------------------------------------
  -- PHY Interface
  ---------------------------------------------------

  gmii_phy_if_int: gmii_phy_if
  port map
  (
    -- PHY Interface
    GMII_PHY_TX_CLK_O  => GMII_TX_CLK_O,
    GMII_PHY_TXD_O     => GMII_TXD_O,
    GMII_PHY_TX_EN_O   => GMII_TX_EN_O,
    GMII_PHY_TX_ER_O   => GMII_TX_ER_O,

    GMII_PHY_RX_CLK_I  => GMII_RX_CLK_I,
    GMII_PHY_RXD_I     => GMII_RXD_I,
    GMII_PHY_RX_DV_I   => GMII_RX_DV_I,
    GMII_PHY_RX_ER_I   => GMII_RX_ER_I,

    -- User Interface
    GMII_USER_TXD_I    => gmii_txd,
    GMII_USER_TX_EN_I  => gmii_tx_en,
    GMII_USER_TX_ER_I  => gmii_tx_er,
    GMII_USER_TX_CLK_I => clk_gmii_rx,

    GMII_USER_RXD_O    => gmii_rxd,
    GMII_USER_RX_DV_O  => gmii_rx_dv,
    GMII_USER_RX_ER_O  => gmii_rx_er,
    GMII_USER_RX_CLK_O => clk_gmii_rx
  );

  CLK_GMII_RX_O  <= clk_gmii_rx;

  ---------------------------------------------------
  -- RX
  ---------------------------------------------------

  gmii_to_ll8_inst : gmii_to_ll8
  port map
  (
    -- gmii interface
    GMII_RX_CLK_I    => clk_gmii_rx,
    GMII_RXD_I       => gmii_rxd,
    GMII_RX_DV_I     => gmii_rx_dv,
    GMII_RX_ER_I     => gmii_rx_er,

    -- ll interface
    LL8_DATA_O       => LL8_RX_O_DATA_O,
    LL8_SOF_N_O      => LL8_RX_O_SOF_N_O,
    LL8_EOF_N_O      => LL8_RX_O_EOF_N_O,
    LL8_SRC_RDY_N_O  => LL8_RX_O_SRC_RDY_N_O,
    LL8_DST_RDY_N_I  => LL8_RX_O_DST_RDY_N_I,

    -- MAC filter
    MAC_PROMISC_EN_I => mac_promisc_en,
    MAC_BRDCST_EN_I  => mac_brdcst_en,
    MAC_ADDR_I       => mac_addr,

    -- ctrl signals
    RESET_I          => reset
  );


  ---------------------------------------------------
  -- TX
  ---------------------------------------------------

  ll8_to_gmii_inst: ll8_to_gmii
  PORT MAP
  (
    -- ctrl
    CLK_I          => clk_gmii_rx,
    RESET_I        => reset,
    IFG_COUNT_I    => ifg_count,

    -- ll interface
    LL_DATA_I      => LL8_TX_I_DATA_I,
    LL_SOF_N_I     => LL8_TX_I_SOF_N_I,
    LL_EOF_N_I     => LL8_TX_I_EOF_N_I,
    LL_SRC_RDY_N_I => LL8_TX_I_SRC_RDY_N_I,
    LL_DST_RDY_N_O => LL8_TX_I_DST_RDY_N_O,

    -- gmii interface
    GMII_TXD_O     => gmii_txd,
    GMII_TX_EN_O   => gmii_tx_en,
    GMII_TX_ER_O   => gmii_tx_er
  );

end IMP;
