---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic SPI Controler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.07.2014 14:58:31
--
--  Description :  Generic implementation of an SPI Master Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity spi_master is
  generic (
    CGN_NUM_SS_BITS     : integer := 1;
    CGN_SCK_RATIO       : integer := 4; -- allowable values: 2,4,8,10,12...
    CGN_MULTI_MASTER    : integer := 0;
    CGN_HAS_NOB_FIFO    : integer := 1;
    CGN_HAS_RX_FIFO     : integer := 1;
    CGN_HAS_TX_FIFO     : integer := 1
  );
  port (
    -- SPI interface
    MOSI_O              : out std_logic;
    MISO_I              : in  std_logic;
    SCK_O               : out std_logic;
    SS_N_O              : out std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);

    -- Configuration
    CPOL_I              : in  std_logic; -- 0=positive _/"\_ , 1=negative "\_/"
    CPHA_I              : in  std_logic; -- 0=first edge centered on data, 1=second edge centered on data
    LSB_FIRST_I         : in  std_logic;
    LOOP_I              : in  std_logic;
    MANUAL_SLAVE_SEL_I  : in  std_logic;
    SLAVE_SEL_N_I       : in  std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);

    -- Status
    BUSY_O              : out std_logic;
    RX_FIFO_OVERFLOW_O  : out std_logic;
    TX_FIFO_UNDERFLOW_O : out std_logic;
    -- FIFO control and status
    RX_FIFO_RESET_I     : in  std_logic;
    RX_FIFO_AEMPTY_O    : out std_logic;
    RX_FIFO_EMPTY_O     : out std_logic;
    RX_FIFO_RE_I        : in  std_logic;
    RX_FIFO_DATA_O      : out std_logic_vector(7 downto 0);
    TX_FIFO_RESET_I     : in  std_logic;
    TX_FIFO_AFULL_O     : out std_logic;
    TX_FIFO_FULL_O      : out std_logic;
    TX_FIFO_WE_I        : in  std_logic;
    TX_FIFO_DATA_I      : in  std_logic_vector(7 downto 0);
    NOB_FIFO_RESET_I    : in  std_logic;
    NOB_FIFO_AFULL_O    : out std_logic;
    NOB_FIFO_FULL_O     : out std_logic;
    NOB_FIFO_WE_I       : in  std_logic;
    NOB_FIFO_DATA_I     : in  std_logic_vector(15 downto 0);

    RESET_I             : in  std_logic;
    CLK_I               : in  std_logic
  );
end spi_master;

architecture behavioral of spi_master is

  constant C_DIV_COUNTER_SIZE : integer := 5;
  constant C_COUNTER_LOAD     : std_logic_vector(C_DIV_COUNTER_SIZE-1 downto 0) := CONV_STD_LOGIC_VECTOR( (CGN_SCK_RATIO/2)-1 , C_DIV_COUNTER_SIZE);

  -- states of state machine
  type type_state is (idle, load, sync, select_slave, start, wait_tx_edge_noshift, wait_tx_edge, wait_rx_edge, stop);
  signal state            : type_state := idle;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

  signal sck_positive     : std_logic := '0';
  signal sck_rising_edge  : std_logic := '0';
  signal sck_falling_edge : std_logic := '0';
  signal clk_div_count    : std_logic_vector(C_DIV_COUNTER_SIZE-1 downto 0) := (others => '0');
  signal edge_count       : std_logic_vector(19 downto 0);
  
  signal mosi             : std_logic := '0';
  signal miso             : std_logic := '0';
  signal slave_select     : std_logic := '1';

  signal input_shift_reg  : std_logic_vector(7 downto 0) := (others => '0');
  signal output_shift_reg : std_logic_vector(7 downto 0) := (others => '0');

  -- internal rx fifo signals
  signal nob_fifo_aempty : std_logic := '0';
  signal nob_fifo_empty  : std_logic := '0';
  signal nob_fifo_re     : std_logic := '0';
  signal nob_fifo_rdata  : std_logic_vector(15 downto 0) := (others => '0');

  -- internal rx fifo signals
  signal rx_fifo_full   : std_logic := '0';
  signal rx_fifo_afull  : std_logic := '0';
  signal rx_fifo_we     : std_logic := '0';

  -- internal tx fifo signals
  signal tx_fifo_aempty : std_logic := '0';
  signal tx_fifo_empty  : std_logic := '0';
  signal tx_fifo_re     : std_logic := '0';
  signal tx_fifo_rdata  : std_logic_vector(7 downto 0) := (others => '0');

  component spi_fifo
    generic (
      CGN_DATA_WIDTH : integer := 1;
      CGN_HAS_FIFO   : integer := 1
    );
    port (
      WRITE_DATA_I   : in  std_logic_vector((CGN_DATA_WIDTH*8)-1 downto 0);
      WRITE_EN_I     : in  std_logic;
      READ_DATA_O    : out std_logic_vector((CGN_DATA_WIDTH*8)-1 downto 0);
      READ_EN_I      : in  std_logic;
      FULL_O         : out std_logic;
      ALMOST_FULL_O  : out std_logic;
      ALMOST_EMPTY_O : out std_logic;
      EMPTY_O        : out std_logic;
      RESET_I        : in  std_logic;
      CLK_I          : in  std_logic
    );
  end component;

begin

  nob_fifo_or_reg : spi_fifo
  generic map(
    CGN_DATA_WIDTH => 2,
    CGN_HAS_FIFO   => CGN_HAS_NOB_FIFO
  )
  port map(
    WRITE_DATA_I   => NOB_FIFO_DATA_I,
    WRITE_EN_I     => NOB_FIFO_WE_I,
    READ_DATA_O    => nob_fifo_rdata,
    READ_EN_I      => nob_fifo_re,
    FULL_O         => NOB_FIFO_FULL_O,
    ALMOST_FULL_O  => NOB_FIFO_AFULL_O,
    ALMOST_EMPTY_O => nob_fifo_aempty,
    EMPTY_O        => nob_fifo_empty,
    RESET_I        => NOB_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  rx_fifo_or_reg : spi_fifo
    generic map(
      CGN_DATA_WIDTH => 1,
      CGN_HAS_FIFO   => CGN_HAS_RX_FIFO
    )
    port map(
      WRITE_DATA_I   => input_shift_reg,
      WRITE_EN_I     => rx_fifo_we,
      READ_DATA_O    => RX_FIFO_DATA_O,
      READ_EN_I      => RX_FIFO_RE_I,
      FULL_O         => rx_fifo_full,
      ALMOST_FULL_O  => rx_fifo_afull,
      ALMOST_EMPTY_O => RX_FIFO_AEMPTY_O,
      EMPTY_O        => RX_FIFO_EMPTY_O,
      RESET_I        => RX_FIFO_RESET_I,
      CLK_I          => CLK_I
    );

  tx_fifo_or_reg : spi_fifo
  generic map(
    CGN_HAS_FIFO  => CGN_HAS_TX_FIFO
  )
  port map(
    WRITE_DATA_I   => TX_FIFO_DATA_I,
    WRITE_EN_I     => TX_FIFO_WE_I,
    READ_DATA_O    => tx_fifo_rdata,
    READ_EN_I      => tx_fifo_re,
    FULL_O         => TX_FIFO_FULL_O,
    ALMOST_FULL_O  => TX_FIFO_AFULL_O,
    ALMOST_EMPTY_O => tx_fifo_aempty,
    EMPTY_O        => tx_fifo_empty,
    RESET_I        => TX_FIFO_RESET_I,
    CLK_I          => CLK_I
  );

  rx_fifo_overflow_reg : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RX_FIFO_RESET_I = '1' then
        RX_FIFO_OVERFLOW_O <= '0';
      elsif rx_fifo_full = '1' and rx_fifo_we = '1' then
        RX_FIFO_OVERFLOW_O <= '1';
      end if;
    end if;
  end process rx_fifo_overflow_reg;

  tx_fifo_underflow_reg : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if TX_FIFO_RESET_I = '1' then
        TX_FIFO_UNDERFLOW_O <= '0';
      elsif tx_fifo_empty = '1' and tx_fifo_re = '1' then
        TX_FIFO_UNDERFLOW_O <= '1';
      end if;
    end if;
  end process tx_fifo_underflow_reg;

  mosi <= output_shift_reg(0) when LSB_FIRST_I = '1' else output_shift_reg(7);
  -- Loopback mode
  miso <= mosi when LOOP_I = '1' else MISO_I;

  slave_select_mask : process(MANUAL_SLAVE_SEL_I, SLAVE_SEL_N_I, slave_select)
    variable i : integer;
  begin
    if MANUAL_SLAVE_SEL_I = '1' then
      SS_N_O <= SLAVE_SEL_N_I;
    else
      for i in CGN_NUM_SS_BITS-1 downto 0 loop
        SS_N_O(i) <= SLAVE_SEL_N_I(i) or slave_select;
      end loop;
    end if;
  end process slave_select_mask;

  control_fsm : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RESET_I = '1' then
        clk_div_count <= (others => '0');
        edge_count <= (others => '0');
        tx_fifo_re    <= '0';
        sck_positive  <= '0';
        BUSY_O        <= '0';
        if CGN_MULTI_MASTER = 1 then
          MOSI_O <= 'Z';
        else
          MOSI_O <= mosi;
        end if;
        state      <= idle;
      else

        -- clock edge counter
        if edge_count > 0 and clk_div_count = 0 then
          edge_count <= edge_count-1;
        end if;

        -- clock divide counter
        if clk_div_count = 0 then
          clk_div_count <= C_COUNTER_LOAD;
        else
          clk_div_count <= clk_div_count - 1;
        end if ;

        if CGN_MULTI_MASTER = 1 then
          MOSI_O <= 'Z';
        else
          MOSI_O <= mosi;
        end if;

        tx_fifo_re   <= '0';
        nob_fifo_re  <= '0';
        rx_fifo_we   <= '0';
        if CPOL_I = '0' then
          SCK_O <= sck_positive;
        else
          SCK_O <= not sck_positive;
        end if;

        case state is
        when idle =>
        -- deselect slave wait for action
          slave_select <= '1';
          edge_count <= (others => '0');
          if tx_fifo_empty = '0' and nob_fifo_empty = '0' then
            tx_fifo_re  <= '1';
            nob_fifo_re <= '1';
            state <= load;
            BUSY_O        <= '1';
          else
            BUSY_O        <= '0';
          end if;
        when load =>
        -- select slave and load data into tx register
          output_shift_reg <= tx_fifo_rdata;
          edge_count       <= nob_fifo_rdata & "000" & '0';
          state <= sync;
        when sync =>
          if clk_div_count = 0 then
            slave_select <= '0';
            MOSI_O <= mosi;
            if CPHA_I = '1' then
              state <= wait_tx_edge_noshift;
            else
              state <= wait_rx_edge;
            end if;
          end if;
        when wait_tx_edge_noshift =>
        -- wait for transmit clock edge and shift output sreg with edge
          MOSI_O <= mosi;
          if clk_div_count = 0 then
            sck_positive <= not sck_positive;
            state <= wait_rx_edge;
          end if;
        when wait_tx_edge =>
        -- wait for transmit clock edge and shift output sreg with edge
          MOSI_O <= mosi;
          if clk_div_count = 0 then
            sck_positive <= not sck_positive;
            if edge_count > 0 then
              if (edge_count(3 downto 0) = X"0" and CPHA_I = '0') or
                 (edge_count(3 downto 0) = X"F" and CPHA_I = '1') then
                output_shift_reg <= tx_fifo_rdata;
                tx_fifo_re <= '1';
                rx_fifo_we <= '1'; -- no full-check => eat or die
              else -- load next byte (and request after next from FIFO)
                if LSB_FIRST_I = '1' then
                  output_shift_reg <= '0' & output_shift_reg(7 downto 1);
                else
                  output_shift_reg <= output_shift_reg(6 downto 0) & '0';
                end if;
              end if;
              state <= wait_rx_edge;
            else
              state <= stop;
            end if;
          end if;
        when wait_rx_edge =>
        -- wait for receive clock edge and shift input sreg with edge
          MOSI_O <= mosi;
          if clk_div_count = 0 then
            sck_positive <= not sck_positive;
            if LSB_FIRST_I = '1' then
              input_shift_reg <= miso & input_shift_reg(7 downto 1);
            else
              input_shift_reg <= input_shift_reg(6 downto 0) & miso;
            end if;
            if edge_count > 0 then
              state <= wait_tx_edge;
            else
              state <= stop;
            end if;
          end if;
        when stop =>
        -- store received data and return to idle state
          --slave_select <= '1';
          if clk_div_count = 0 then
            rx_fifo_we <= '1'; -- no full-check => eat or die
            state <= idle;
          end if;
        when others =>
          state <= idle;
        end case;
      end if;
    end if;
  end process control_fsm;

end behavioral;
