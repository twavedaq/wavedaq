---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  SPI one_reg_fifo/FIFO
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  15.07.2014 07:52:16
--
--  Description :  FIFO or one_reg_fifo for the SPI Interface.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity spi_fifo is
  generic (
    CGN_DATA_WIDTH    : integer := 1;
    CGN_HAS_FIFO      : integer := 1
  );
  port (
    WRITE_DATA_I   : in  std_logic_vector((CGN_DATA_WIDTH*8)-1 downto 0);
    WRITE_EN_I     : in  std_logic;
    READ_DATA_O    : out std_logic_vector((CGN_DATA_WIDTH*8)-1 downto 0);
    READ_EN_I      : in  std_logic;
    FULL_O         : out std_logic;
    ALMOST_FULL_O  : out std_logic;
    ALMOST_EMPTY_O : out std_logic;
    EMPTY_O        : out std_logic;

    RESET_I        : in  std_logic;
    CLK_I          : in  std_logic
  );
end spi_fifo;

architecture behavioral of spi_fifo is

  signal one_reg_fifo : std_logic_vector((CGN_DATA_WIDTH*8)-1 downto 0) := (others => '0');
  signal full         : std_logic := '0';
  signal empty        : std_logic := '0';

  component fifo_synch_8x512
    port (
      clk          : in  std_logic;
      srst         : in  std_logic;
      din          : in  std_logic_vector(7 downto 0);
      wr_en        : in  std_logic;
      rd_en        : in  std_logic;
      dout         : out std_logic_vector(7 downto 0);
      full         : out std_logic;
      almost_full  : out std_logic;
      empty        : out std_logic;
      almost_empty : out std_logic
    );
  end component;

  component fifo_synch_16x512
    port (
      clk          : in  std_logic;
      srst         : in  std_logic;
      din          : in  std_logic_vector(15 downto 0);
      wr_en        : in  std_logic;
      rd_en        : in  std_logic;
      dout         : out std_logic_vector(15 downto 0);
      full         : out std_logic;
      almost_full  : out std_logic;
      empty        : out std_logic;
      almost_empty : out std_logic
    );
  end component;

begin

  fifo_1byte : if CGN_HAS_FIFO = 1 and CGN_DATA_WIDTH = 1 generate
    fifo_8x512_inst : fifo_synch_8x512
    port map (
      clk          => CLK_I,
      srst         => RESET_I,
      din          => WRITE_DATA_I,
      wr_en        => WRITE_EN_I,
      rd_en        => READ_EN_I,
      dout         => READ_DATA_O,
      full         => FULL_O,
      almost_full  => ALMOST_FULL_O,
      empty        => EMPTY_O,
      almost_empty => ALMOST_EMPTY_O
    );
  end generate fifo_1byte;

  fifo_2byte : if CGN_HAS_FIFO = 1 and CGN_DATA_WIDTH = 2 generate
    fifo_16x512_inst : fifo_synch_16x512
    port map (
      clk          => CLK_I,
      srst         => RESET_I,
      din          => WRITE_DATA_I,
      wr_en        => WRITE_EN_I,
      rd_en        => READ_EN_I,
      dout         => READ_DATA_O,
      full         => FULL_O,
      almost_full  => ALMOST_FULL_O,
      empty        => EMPTY_O,
      almost_empty => ALMOST_EMPTY_O
    );
  end generate fifo_2byte;

  no_fifo : if CGN_HAS_FIFO = 0 generate
    simple_register : process(CLK_I)
    begin
      if rising_edge(CLK_I) then
        if RESET_I = '1' then
          one_reg_fifo <= (others=>'0');
          full         <= '0';
          empty        <= '1';
        elsif READ_EN_I = '1' then
          full         <= '0';
          empty        <= '1';
        elsif WRITE_EN_I = '1' and full = '0' then
          one_reg_fifo   <= WRITE_DATA_I;
          full         <= '1';
          empty        <= '0';
        end if;
      end if;
    end process simple_register;

    READ_DATA_O    <= one_reg_fifo;
    FULL_O         <= full;
    ALMOST_FULL_O  <= full;
    ALMOST_EMPTY_O <= empty;
    EMPTY_O        <= empty;
  end generate no_fifo;

end behavioral;
