---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Generic Register Bank
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  08.08.2014 12:21:26
--
--  Description :  Generic implementation of a register bank including bit triggers
--                 upon writing a 1 to the corresponding bit as well as byte triggers
--                 and modified flags upon writing to corresponding bytes.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
--use ieee.std_logic_unsigned.all;
-- synopsys translate_off
library UNISIM;
use UNISIM.Vcomponents.ALL;
-- synopsys translate_on

entity generic_register_bank is
  generic (
    CGN_NO_CTRL_REG       : integer := 8;
    CGN_NO_STAT_REG       : integer := 8;
    CGN_HW_CTRL_REG       : std_logic_vector := x"FF";
    CGN_HW_STAT_REG       : std_logic_vector := x"FF";
    CGN_REG_ADDR_WIDTH    : integer := 8;
    CGN_REG_DATA_WIDTH    : integer := 32  -- 64,32,16,8
  );
  port (
    UC_ADDR_I        : in  std_logic_vector( CGN_REG_ADDR_WIDTH-1 downto 0 );
    UC_BE_I          : in  std_logic_vector( (CGN_REG_DATA_WIDTH/8)-1 downto 0 );
    UC_RNW_I         : in  std_logic;
    UC_REG_EN_I      : in  std_logic_vector( 1 downto 0 ); -- "01" enables the control regsiter, "10" enables the status register
    UC_DATA_I        : in  std_logic_vector( CGN_REG_DATA_WIDTH-1 downto 0 );
    UC_DATA_O        : out std_logic_vector( CGN_REG_DATA_WIDTH-1 downto 0 );
    STAT_REG_D_I     : in  std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
    STAT_REG_D_O     : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
    STAT_REG_READ_O  : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
    STAT_REG_WRITE_O : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
    STAT_BIT_TRIG_O  : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
    STAT_BYTE_TRIG_O : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH/8)-1 downto 0 );
    CTRL_REG_Q_O     : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
    CTRL_REG_READ_O  : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
    CTRL_REG_WRITE_O : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
    CTRL_BIT_TRIG_O  : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
    CTRL_BYTE_TRIG_O : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH/8)-1 downto 0 );
    RESET_I          : in  std_logic;
    CLK_I            : in  std_logic
  );
end generic_register_bank;



architecture behavioral of generic_register_bank is

  -- Types
  subtype type_reg is std_logic_vector(CGN_REG_DATA_WIDTH-1 downto 0);
  type type_control_reg_arr is array (CGN_NO_CTRL_REG-1 downto 0) of type_reg;
  type type_status_reg_arr  is array (CGN_NO_STAT_REG-1 downto 0) of type_reg;

  type type_stat_byte_trig_arr is array (CGN_NO_STAT_REG-1 downto 0) of std_logic_vector((CGN_REG_DATA_WIDTH/8)-1 downto 0);
  type type_ctrl_byte_trig_arr is array (CGN_NO_CTRL_REG-1 downto 0) of std_logic_vector((CGN_REG_DATA_WIDTH/8)-1 downto 0);
  type type_stat_rd_wr_arr     is array (CGN_NO_STAT_REG-1 downto 0) of std_logic;
  type type_ctrl_rd_wr_arr     is array (CGN_NO_CTRL_REG-1 downto 0) of std_logic;
  --subtype type_control_reg_no is integer range 0 to (CGN_NO_CTRL_REG-1);
  --subtype type_status_reg_no  is integer range 0 to (CGN_NO_STAT_REG-1);

  -- Constants
  constant C_SEL_STAT_REG : std_logic_vector( 1 downto 0) := "10";
  constant C_SEL_CTRL_REG : std_logic_vector( 1 downto 0) := "01";
  constant C_DUMMY_WORD   : std_logic_vector(31 downto 0) := x"DEADBEEF";

  -- Signals
  signal ctrl_reg_array       : type_control_reg_arr := (others=>(others=>'0'));
  signal ctrl_reg_read_array  : type_ctrl_rd_wr_arr;
  signal ctrl_reg_write_array : type_ctrl_rd_wr_arr;
  signal ctrl_bit_trig_array  : type_control_reg_arr;
  signal ctrl_byte_trig_array : type_ctrl_byte_trig_arr;

  signal stat_reg_array       : type_status_reg_arr;
  signal stat_reg_out_array   : type_status_reg_arr;
  signal stat_reg_read_array  : type_stat_rd_wr_arr;
  signal stat_reg_write_array : type_stat_rd_wr_arr;
  signal stat_bit_trig_array  : type_status_reg_arr;
  signal stat_byte_trig_array : type_stat_byte_trig_arr;

begin

  assign_status_content : process(STAT_REG_D_I, stat_reg_read_array, stat_reg_write_array)
  begin
    for reg_address in CGN_NO_STAT_REG-1 downto 0 loop
      if CGN_HW_STAT_REG(reg_address) = '1' then
        stat_reg_array(reg_address) <= STAT_REG_D_I((reg_address+1)*CGN_REG_DATA_WIDTH-1 downto reg_address*CGN_REG_DATA_WIDTH);
      end if;
      STAT_BIT_TRIG_O((reg_address+1)*CGN_REG_DATA_WIDTH-1 downto reg_address*CGN_REG_DATA_WIDTH)        <= stat_bit_trig_array(reg_address);
      STAT_BYTE_TRIG_O(((reg_address+1)*CGN_REG_DATA_WIDTH/8)-1 downto reg_address*CGN_REG_DATA_WIDTH/8) <= stat_byte_trig_array(reg_address);
      STAT_REG_READ_O(reg_address)  <= stat_reg_read_array(reg_address);
      STAT_REG_WRITE_O(reg_address) <= stat_reg_write_array(reg_address);
      if CGN_HW_STAT_REG(reg_address) = '1' then
        STAT_REG_D_O((reg_address+1)*CGN_REG_DATA_WIDTH-1 downto reg_address*CGN_REG_DATA_WIDTH) <= stat_reg_out_array(reg_address);
      end if;
    end loop;
  end process assign_status_content;

  assign_ctrl_content : process(ctrl_reg_read_array, ctrl_reg_array, ctrl_bit_trig_array, ctrl_byte_trig_array)
  begin
    for reg_address in CGN_NO_CTRL_REG-1 downto 0 loop
      if CGN_HW_CTRL_REG(reg_address) = '1' then
        CTRL_REG_Q_O((reg_address+1)*CGN_REG_DATA_WIDTH-1 downto reg_address*CGN_REG_DATA_WIDTH)         <= ctrl_reg_array(reg_address);
      end if;
      CTRL_BIT_TRIG_O((reg_address+1)*CGN_REG_DATA_WIDTH-1 downto reg_address*CGN_REG_DATA_WIDTH)        <= ctrl_bit_trig_array(reg_address);
      CTRL_BYTE_TRIG_O(((reg_address+1)*CGN_REG_DATA_WIDTH/8)-1 downto reg_address*CGN_REG_DATA_WIDTH/8) <= ctrl_byte_trig_array(reg_address);
      CTRL_REG_READ_O(reg_address)  <= ctrl_reg_read_array(reg_address);
      CTRL_REG_WRITE_O(reg_address) <= ctrl_reg_write_array(reg_address);
    end loop;
  end process assign_ctrl_content;

  write_register_array : process (CLK_I)
    variable address : integer := 0;
  begin
    address := CONV_INTEGER(UNSIGNED(UC_ADDR_I));
    if rising_edge(CLK_I) then
      -- vvv default assignments start here vvv
      ctrl_bit_trig_array  <= (others=>(others=>'0'));
      ctrl_byte_trig_array <= (others=>(others=>'0'));
      ctrl_reg_write_array <= (others=>'0');
      stat_bit_trig_array  <= (others=>(others=>'0'));
      stat_byte_trig_array <= (others=>(others=>'0'));
      stat_reg_write_array <= (others=>'0');
      -- ^^^ default assignments end here   ^^^
      if RESET_I = '1' then
        for reg_address in CGN_NO_CTRL_REG-1 downto 0 loop
          if CGN_HW_CTRL_REG(address) = '1' then
            ctrl_reg_array   <= (others=>(others=>'0'));
          end if;
        end loop;
        ctrl_bit_trig_array  <= (others=>(others=>'0'));
        ctrl_byte_trig_array <= (others=>(others=>'0'));
        ctrl_reg_write_array <= (others=>'0');
        for reg_address in CGN_NO_STAT_REG-1 downto 0 loop
          if CGN_HW_STAT_REG(address) = '1' then
            stat_reg_out_array <= (others=>(others=>'0'));
          end if;
        end loop;
        stat_bit_trig_array  <= (others=>(others=>'0'));
        stat_byte_trig_array <= (others=>(others=>'0'));
        stat_reg_write_array <= (others=>'0');
      elsif UC_RNW_I = '0' then
        if UC_REG_EN_I = C_SEL_CTRL_REG and CGN_HW_CTRL_REG(address) = '1' then
          ctrl_reg_write_array(address) <= '1';
          for byte_index in (CGN_REG_DATA_WIDTH/8)-1 downto 0 loop
            if UC_BE_I(byte_index) = '1' then
              ctrl_reg_array(address)(byte_index*8+7 downto byte_index*8)      <= UC_DATA_I(byte_index*8+7 downto byte_index*8);
              ctrl_bit_trig_array(address)(byte_index*8+7 downto byte_index*8) <= UC_DATA_I(byte_index*8+7 downto byte_index*8);
              ctrl_byte_trig_array(address)(byte_index) <= '1';
            end if;
          end loop;
        elsif UC_REG_EN_I = C_SEL_STAT_REG and CGN_HW_STAT_REG(address) = '1' then
          stat_reg_write_array(address) <= '1';
          for byte_index in (CGN_REG_DATA_WIDTH/8)-1 downto 0 loop
            if UC_BE_I(byte_index) = '1' then
              stat_reg_out_array(address)(byte_index*8+7 downto byte_index*8)  <= UC_DATA_I(byte_index*8+7 downto byte_index*8);
              stat_bit_trig_array(address)(byte_index*8+7 downto byte_index*8) <= UC_DATA_I(byte_index*8+7 downto byte_index*8);
              stat_byte_trig_array(address)(byte_index) <= '1';
            end if;
          end loop;
        end if;
      end if;
    end if;
  end process write_register_array;

  read_register_array : process (UC_REG_EN_I, UC_ADDR_I, ctrl_reg_array, stat_reg_array)
    variable address : integer := 0;
  begin
    address := CONV_INTEGER(UNSIGNED(UC_ADDR_I));
    ctrl_reg_read_array <= (others=>'0');
    stat_reg_read_array <= (others=>'0');
    case UC_REG_EN_I is
      when C_SEL_CTRL_REG =>
        if CGN_HW_CTRL_REG(address) = '1' then
          ctrl_reg_read_array(address) <= '1';
          UC_DATA_O <= ctrl_reg_array(address);
        else
          UC_DATA_O <= C_DUMMY_WORD;
        end if;
      when C_SEL_STAT_REG =>
        if CGN_HW_STAT_REG(address) = '1' then
          stat_reg_read_array(address) <= '1';
          UC_DATA_O <= stat_reg_array(address);
        else
          UC_DATA_O <= C_DUMMY_WORD;
        end if;
      when others =>
        UC_DATA_O <= (others=>'0');
    end case;
  end process read_register_array;

end behavioral;
