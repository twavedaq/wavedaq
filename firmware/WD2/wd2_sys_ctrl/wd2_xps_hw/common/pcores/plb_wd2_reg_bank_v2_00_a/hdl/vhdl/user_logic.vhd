------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Thu Aug 14 12:00:34 2014 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_AWIDTH                 -- Slave interface address bus width
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_MEM                    -- Number of memory spaces
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Addr                  -- Bus to IP address bus
--   Bus2IP_CS                    -- Bus to IP chip select for user logic memory selection
--   Bus2IP_RNW                   -- Bus to IP read/not write
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    CGN_NO_CTRL_REG                : integer              := 8;
    CGN_NO_STAT_REG                : integer              := 8;
    CGN_HW_CTRL_REG                : std_logic_vector     := x"FF";
    CGN_HW_STAT_REG                : std_logic_vector     := x"FF";
    CGN_REG_ADDR_WIDTH             : integer              := 8;
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_AWIDTH                   : integer              := 32;
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_MEM                      : integer              := 2
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    STAT_REG_D_I                   : in  std_logic_vector( (CGN_NO_STAT_REG*C_SLV_DWIDTH)-1 downto 0 );
    STAT_REG_D_O                   : out std_logic_vector( (CGN_NO_STAT_REG*C_SLV_DWIDTH)-1 downto 0 );
    STAT_REG_READ_O                : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
    STAT_REG_WRITE_O               : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
    STAT_BIT_TRIG_O                : out std_logic_vector( (CGN_NO_STAT_REG*C_SLV_DWIDTH)-1 downto 0 );
    STAT_BYTE_TRIG_O               : out std_logic_vector( (CGN_NO_STAT_REG*C_SLV_DWIDTH/8)-1 downto 0 );
    CTRL_REG_Q_O                   : out std_logic_vector( (CGN_NO_CTRL_REG*C_SLV_DWIDTH)-1 downto 0 );
    CTRL_REG_READ_O                : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
    CTRL_REG_WRITE_O               : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
    CTRL_BIT_TRIG_O                : out std_logic_vector( (CGN_NO_CTRL_REG*C_SLV_DWIDTH)-1 downto 0 );
    CTRL_BYTE_TRIG_O               : out std_logic_vector( (CGN_NO_CTRL_REG*C_SLV_DWIDTH/8)-1 downto 0 );
    SPI_ADDR_I                     : in  std_logic_vector(C_SLV_AWIDTH-1 downto 0);
    SPI_RNW_I                      : in  std_logic;
    SPI_DATA_I                     : in  std_logic_vector(C_SLV_DWIDTH-1 downto 0);
    SPI_BE_I                       : in  std_logic_vector(C_SLV_DWIDTH/8-1 downto 0);
    SPI_REG_SEL_I                  : in  std_logic_vector(1 downto 0);
    SPI_REQ_I                      : in  std_logic;
    SPI_DATA_O                     : out std_logic_vector(C_SLV_DWIDTH-1 downto 0);
    SPI_RD_ACK_O                   : out std_logic;
    SPI_WR_ACK_O                   : out std_logic;
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS                      : in  std_logic_vector(0 to C_NUM_MEM-1);
    Bus2IP_RNW                     : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  component generic_register_bank is
    generic (
      CGN_NO_CTRL_REG    : integer := 8;
      CGN_NO_STAT_REG    : integer := 8;
      CGN_HW_CTRL_REG    : std_logic_vector := x"FF";
      CGN_HW_STAT_REG    : std_logic_vector := x"FF";
      CGN_REG_ADDR_WIDTH : integer := 8;
      CGN_REG_DATA_WIDTH : integer := 32  -- 64,32,16,8
    );
    port (
      UC_ADDR_I        : in  std_logic_vector( CGN_REG_ADDR_WIDTH-1 downto 0 );
      UC_BE_I          : in  std_logic_vector( (CGN_REG_DATA_WIDTH/8)-1 downto 0 );
      UC_RNW_I         : in  std_logic;
      UC_REG_EN_I      : in  std_logic_vector( 1 downto 0 ); -- "01" enables the control regsiter, "10" enables the status register
      UC_DATA_I        : in  std_logic_vector( CGN_REG_DATA_WIDTH-1 downto 0 );
      UC_DATA_O        : out std_logic_vector( CGN_REG_DATA_WIDTH-1 downto 0 );
      STAT_REG_D_I     : in  std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      STAT_REG_D_O     : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      STAT_REG_READ_O  : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
      STAT_REG_WRITE_O : out std_logic_vector( CGN_NO_STAT_REG-1 downto 0 );
      STAT_BIT_TRIG_O  : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      STAT_BYTE_TRIG_O : out std_logic_vector( (CGN_NO_STAT_REG*CGN_REG_DATA_WIDTH/8)-1 downto 0 );
      CTRL_REG_Q_O     : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      CTRL_REG_READ_O  : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
      CTRL_REG_WRITE_O : out std_logic_vector( CGN_NO_CTRL_REG-1 downto 0 );
      CTRL_BIT_TRIG_O  : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH)-1 downto 0 );
      CTRL_BYTE_TRIG_O : out std_logic_vector( (CGN_NO_CTRL_REG*CGN_REG_DATA_WIDTH/8)-1 downto 0 );
      RESET_I          : in  std_logic;
      CLK_I            : in  std_logic
    );
  end component;

  ------------------------------------------
  -- Signals for user logic memory space example
  ------------------------------------------
  attribute fsm_encoding : string;

  type type_state is (idle_s, grant_spi_s, grant_uc_s);
  signal state        : type_state;
  attribute fsm_encoding of state : signal is "one-hot";

  signal reg_bank_data_out : std_logic_vector(C_SLV_DWIDTH-1 downto 0) := (others=>'0');
  signal reg_bank_addr     : std_logic_vector(CGN_REG_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal reg_bank_be       : std_logic_vector(3 downto 0)  := (others=>'0');
  signal reg_bank_rnw      : std_logic := '0';
  signal reg_bank_en       : std_logic_vector(1 downto 0) := (others=>'0');
  signal reg_bank_data_in  : std_logic_vector(C_SLV_DWIDTH-1 downto 0) := (others=>'0');
  signal spi_ack           : std_logic := '0';
  signal spi_reg_in_s      : std_logic := '0';
  signal spi_req           : std_logic := '0';
  signal uc_ack            : std_logic := '0';
  signal uc_req_in_s       : std_logic := '0';
  signal uc_req            : std_logic := '0';
  signal uc_write_ack      : std_logic := '0';
  signal uc_read_ack       : std_logic := '0';
  signal spi_write_ack     : std_logic := '0';
  signal spi_read_ack      : std_logic := '0';

begin

  --USER logic implementation added here

  ------------------------------------------
  -- Example code to access user logic memory region
  -- 
  -- Note:
  -- The example code presented here is to show you one way of using
  -- the user logic memory space features. The Bus2IP_Addr, Bus2IP_CS,
  -- and Bus2IP_RNW IPIC signals are dedicated to these user logic
  -- memory spaces. Each user logic memory space has its own address
  -- range and is allocated one bit on the Bus2IP_CS signal to indicated
  -- selection of that memory space. Typically these user logic memory
  -- spaces are used to implement memory controller type cores, but it
  -- can also be used in cores that need to access additional address space
  -- (non C_BASEADDR based), s.t. bridges. This code snippet infers
  -- 2 256x32-bit (byte accessible) single-port Block RAM by XST.
  ------------------------------------------


  wd2_register_bank : generic_register_bank
    generic map(
      CGN_NO_CTRL_REG    => CGN_NO_CTRL_REG,
      CGN_NO_STAT_REG    => CGN_NO_STAT_REG,
      CGN_HW_CTRL_REG    => CGN_HW_CTRL_REG,
      CGN_HW_STAT_REG    => CGN_HW_STAT_REG,
      CGN_REG_ADDR_WIDTH => CGN_REG_ADDR_WIDTH,
      CGN_REG_DATA_WIDTH => C_SLV_DWIDTH
    )
    port map(
      UC_ADDR_I        => reg_bank_addr,     -- Bus2IP_Addr(C_SLV_AWIDTH-10 to C_SLV_AWIDTH-3),
      UC_BE_I          => reg_bank_be,       -- Bus2IP_BE,
      UC_RNW_I         => reg_bank_rnw,      -- Bus2IP_RNW,
      UC_REG_EN_I      => reg_bank_en,       -- Bus2IP_CS,
      UC_DATA_I        => reg_bank_data_in,  -- Bus2IP_Data,
      UC_DATA_O        => reg_bank_data_out, -- mem_ip2bus_data,
      STAT_REG_D_I     => STAT_REG_D_I,
      STAT_REG_D_O     => STAT_REG_D_O,
      STAT_REG_READ_O  => STAT_REG_READ_O,
      STAT_REG_WRITE_O => STAT_REG_WRITE_O,
      STAT_BIT_TRIG_O  => STAT_BIT_TRIG_O,
      STAT_BYTE_TRIG_O => STAT_BYTE_TRIG_O,
      CTRL_REG_Q_O     => CTRL_REG_Q_O,
      CTRL_REG_READ_O  => CTRL_REG_READ_O,
      CTRL_REG_WRITE_O => CTRL_REG_WRITE_O,
      CTRL_BIT_TRIG_O  => CTRL_BIT_TRIG_O,
      CTRL_BYTE_TRIG_O => CTRL_BYTE_TRIG_O,
      RESET_I          => Bus2IP_Reset,
      CLK_I            => Bus2IP_Clk
    );

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= reg_bank_data_out when uc_read_ack = '1' else (others => '0');
  SPI_DATA_O   <= reg_bank_data_out;

  uc_write_ack  <= uc_ack  and not(Bus2IP_RNW);
  uc_read_ack   <= uc_ack  and Bus2IP_RNW;
  spi_write_ack <= spi_ack and not(SPI_RNW_I);
  spi_read_ack  <= spi_ack and SPI_RNW_I;

  reg_bank_addr    <= SPI_ADDR_I(17 downto 2) when spi_ack = '1' else Bus2IP_Addr(C_SLV_AWIDTH-18 to C_SLV_AWIDTH-3);
  reg_bank_be      <= SPI_BE_I                when spi_ack = '1' else Bus2IP_BE;
  reg_bank_rnw     <= SPI_RNW_I               when spi_ack = '1' else Bus2IP_RNW;
  reg_bank_en      <= SPI_REG_SEL_I           when spi_ack = '1' else Bus2IP_CS;
  reg_bank_data_in <= SPI_DATA_I              when spi_ack = '1' else Bus2IP_Data;

  arbiter_fsm : process(Bus2IP_Clk)
  begin
    if rising_edge(Bus2IP_Clk) then
      if Bus2IP_Reset = '1' then
        state <= idle_s;
      else
        case state is
          when idle_s =>
            if spi_req = '1' then
              state <= grant_spi_s;
            elsif uc_req = '1' then
              state <= grant_uc_s;
            end if;
          when grant_spi_s =>
            state <= idle_s;
          when grant_uc_s =>
            state <= idle_s;
          when others =>
            state <= idle_s;
        end case;
      end if;
    end if;
  end process;

  arbiter_fsm_output : process(state)
  begin
    case state is
      when idle_s =>
        spi_ack <= '0';
        uc_ack  <= '0';
      when grant_spi_s =>
        spi_ack <= '1';
        uc_ack  <= '0';
      when grant_uc_s =>
        spi_ack <= '0';
        uc_ack  <= '1';
      when others =>
        spi_ack <= '0';
        uc_ack  <= '0';
    end case;
  end process;

  store_req_sr_reg : process(Bus2IP_Clk)
  begin
    if rising_edge(Bus2IP_Clk) then
      if Bus2IP_Reset = '1' then
        spi_req <= '0';
        uc_req  <= '0';
        spi_reg_in_s <= '0';
        uc_req_in_s  <= '0';
      else

        -- store for edge detection
        spi_reg_in_s <= SPI_REQ_I;
        uc_req_in_s  <= Bus2IP_CS(0) or Bus2IP_CS(1);

        if spi_ack = '1' then
          spi_req <= '0';
        elsif SPI_REQ_I = '1' and spi_reg_in_s = '0' then -- rising edge
          spi_req <= '1';
        end if;
        
        if uc_ack = '1' then
          uc_req <= '0';
        elsif (Bus2IP_CS(0) = '1' or Bus2IP_CS(1) = '1') and uc_req_in_s = '0' then -- rising edge
          uc_req <= '1';
        end if;

      end if;
    end if;
  end process;

  IP2Bus_WrAck <= uc_write_ack;
  IP2Bus_RdAck <= uc_read_ack;
  IP2Bus_Error <= '0';
  SPI_WR_ACK_O <= spi_write_ack;
  SPI_RD_ACK_O <= spi_read_ack;

end IMP;
