---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 BRAM instance 8kx12 4kx24
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  07.11.2017 12:23:34
--
--  Description : Block RAM instance for event buffer in WaveDream2.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity bram_8kx12_4kx24 is
port
(
  -- Write
  WR_A_DATA_I : in  std_logic_vector(11 downto 0);
  WR_A_ADDR_I : in  std_logic_vector(12 downto 0);
  WR_A_EN_I   : in  std_logic;

  -- Read
  RD_B_DATA_O : out std_logic_vector(23 downto 0);
  RD_B_ADDR_I : in  std_logic_vector(11 downto 0);
  
  CLK_I       : in  std_logic
);
end bram_8kx12_4kx24;

architecture behavioral of bram_8kx12_4kx24 is

  constant C_DWIDTH_IN  : integer := 2;
  constant C_DWIDTH_OUT : integer := 4;

  signal rd_b_data      : std_logic_vector(23 downto 0) := (others => '0');

begin

  -- shuffling
  -- WR_A_DATA_I 0: A[11:0]
  --             1: B[11:0]
  --                  23:22      21:20     19:18    17:16    15:14    13:12    11:10     9:8      7:6      5:4      3:2      1:0
  -- rd_b_data   0: B[11:10] & A[11:10] & B[9:8] & A[9:8] & B[7:6] & A[7:6] & B[5:4] & A[5:4] & B[3:2] & A[3:2] & B[1:0] & A[1:0]
  -- RD_B_DATA_O 0: A[7:0] & B[3:0] & A[11:8] & B[11:4]
  RD_B_DATA_O <= rd_b_data(13 downto 12) & rd_b_data( 9 downto  8) & rd_b_data( 5 downto  4) & rd_b_data( 1 downto  0) & 
                 rd_b_data( 7 downto  6) & rd_b_data( 3 downto  2) &
                 rd_b_data(21 downto 20) & rd_b_data(17 downto 16) & 
                 rd_b_data(23 downto 22) & rd_b_data(19 downto 18) & rd_b_data(15 downto 14) & rd_b_data(11 downto 10);

  BRAM_inst : for i in 5 downto 0 generate
    -- buffers
    BRAM_8kx2_4kx4 : entity psi_3205_v1_00_a.ram_dp
    generic map
    (
      CGN_DATA_WIDTH_A  => C_DWIDTH_IN,
      CGN_ADDR_WIDTH_A  => 13,
      CGN_READ_PORT_A   => false,
      CGN_WRITE_PORT_A  => true,
      CGN_READ_FIRST_A  => false,
      CGN_USE_OUTREG_A  => false,
      CGN_DATA_WIDTH_B  => C_DWIDTH_OUT,
      CGN_ADDR_WIDTH_B  => 12,
      CGN_READ_PORT_B   => true,
      CGN_WRITE_PORT_B  => false,
      CGN_READ_FIRST_B  => true,
      CGN_USE_OUTREG_B  => false,
      CGN_RAM_STYLE     => "block"
    )
    port map
    (
      PA_CLK_I   => CLK_I,
      PA_EN_I    => '1',
      PA_ADDR_I  => WR_A_ADDR_I,
      PA_DATA_I  => WR_A_DATA_I((i+1)*C_DWIDTH_IN-1 downto i*C_DWIDTH_IN),
      PA_WR_EN_I => WR_A_EN_I,
      PA_DATA_O  => open,
      PB_CLK_I   => CLK_I,
      PB_EN_I    => '1',
      PB_ADDR_I  => RD_B_ADDR_I,
      PB_DATA_I  => (others=>'0'),
      PB_WR_EN_I => '0',
      PB_DATA_O  => rd_b_data((i+1)*C_DWIDTH_OUT-1 downto i*C_DWIDTH_OUT)
    );
  end generate;
  
end behavioral;
