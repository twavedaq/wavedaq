---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.08.2017 15:29:37
--
--  Description : Event data buffer for package generator of the WaveDream2 (WD2) board.
--                Stores the data of one event to be transmitted via ethernet or oserdes.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;

entity wd2_pkg_gen_event_buffer is
port
(
  -- Buffer Ctrl
  DATA_O                      : out std_logic_vector(23 downto 0);
  DATA_SIZE_O                 : out std_logic_vector( 1 downto 0);
  DATA_VALID_O                : out std_logic;
  WR_DRS_BUFFER_READY_O       : out std_logic;
  RD_DRS_BUFFER_READY_O       : out std_logic;
  WR_RING_BUFFER_READY_O      : out std_logic;
  RD_RING_BUFFER_READY_O      : out std_logic;
  CLR_BUFFER_I                : in  std_logic; -- controlled by write fsm
  WR_BUFFER_SENT_O            : out std_logic;
  RD_BUFFER_SENT_O            : out std_logic;
  SET_BUFFER_SENT_I           : in  std_logic; -- controlled by read fsm
  CLR_BUFFER_SENT_I           : in  std_logic; -- controlled by write fsm
  STORE_TX_ENABLE_I           : in  std_logic;
  STORE_ZERO_SUPPRESSION_I    : in  std_logic;
  WR_BUF_SEL_I                : in  std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0);
  RD_BUF_SEL_I                : in  std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0);
  RD_DATA_TYPE_SEL_I          : in  std_logic_vector(7 downto 0);
  RD_CH_SEL_I                 : in  std_logic_vector(4 downto 0);
  RD_EN_I                     : in  std_logic;
  RD_INIT_I                   : in  std_logic;
  -- ADC (& DRS) input
  DRS_ADC_DATA_I              : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC*CPK_DRS_ADC_CH_WR_DWIDTH-1 downto 0);
  -- DRS
  DRS_BUF_WEN_I               : in  std_logic;
  -- ADC
  ADC_BUF_WEN_I               : in  std_logic;
  ADC_SAMPLING_DIV_I          : in  std_logic_vector(7 downto 0);
  -- TDC
  TDC_DATA_I                  : in  std_logic_vector(CPK_NR_OF_TDC_CHNLS*CPK_TDC_WR_DWIDTH-1 downto 0);
  TDC_BUF_WEN_I               : in  std_logic;
  -- Trigger
  TRG_DATA_I                  : in  std_logic_vector(CPK_TRG_WR_DWIDTH-1 downto 0);
  TRG_BUF_WEN_I               : in  std_logic;
  -- Scaler
  SCL_DATA_I                  : in  std_logic_vector(CPK_NR_OF_SCALERS*CPK_SCL_WR_DWIDTH-1 downto 0);
  SCL_BUF_WEN_I               : in  std_logic;
  -- TX control
  DRS_CH_TX_EN_I              : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0);
  ADC_CH_TX_EN_I              : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC-1 downto 0);
  TDC_CH_TX_EN_I              : in  std_logic_vector(CPK_NR_OF_TDC_CHNLS-1 downto 0);
  TRG_TX_EN_I                 : in  std_logic;
  SCL_TX_EN_I                 : in  std_logic;
  ZERO_SUPPRESSION_EN_I       : in  std_logic;
  ZERO_SUPPRESSION_INHIBIT_I  : in  std_logic;
  ZERO_SUPPRESSION_MASK_I     : in  std_logic_vector(15 downto 0);
  ZERO_SUPPRESSION_MASK_O     : out std_logic_vector(15 downto 0);
  TYPE_TX_EN_O                : out std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0);
  ITEM_TX_EN_O                : out std_logic;
  DRS_TX_SAMPLES_I            : in  std_logic_vector(10 downto 0);
  ADC_TX_SAMPLES_I            : in  std_logic_vector(11 downto 0);
  TDC_TX_SAMPLES_I            : in  std_logic_vector(12 downto 0);
  TRG_TX_SAMPLES_I            : in  std_logic_vector( 9 downto 0);
  DATASET_TX_SAMPLES_O        : out std_logic_vector(15 downto 0);
  DRS_MAX_SAMPLES_PER_PKG_I   : in  std_logic_vector(15 downto 0);
  ADC_MAX_SAMPLES_PER_PKG_I   : in  std_logic_vector(15 downto 0);
  TDC_MAX_SAMPLES_PER_PKG_I   : in  std_logic_vector(17 downto 0);
  TRG_MAX_SAMPLES_PER_PKG_I   : in  std_logic_vector(15 downto 0);
  SCL_MAX_SAMPLES_PER_PKG_I   : in  std_logic_vector(15 downto 0);
  MAX_SAMPLES_PER_PKG_O       : out std_logic_vector(17 downto 0);
  LAST_TYPE_O                 : out std_logic;
  LAST_CH_O                   : out std_logic;
  -- Clock and reset
  CLK_I                       : in  std_logic;
  RST_I                       : in  std_logic
);
end wd2_pkg_gen_event_buffer;

architecture struct of wd2_pkg_gen_event_buffer is

  constant C_BUF_SEL_WIDTH  : integer := log2ceil(CPK_NR_OF_BUFFERS);

  constant C_DRS_WR_AWIDTH  : integer := log2ceil(CPK_DRS_SAMPLES_PER_EVENT);
  constant C_DRS_RD_AWIDTH  : integer := C_DRS_WR_AWIDTH - 1; -- twice the data width of write port
  constant C_ADC_WR_AWIDTH  : integer := log2ceil(CPK_ADC_SAMPLES_PER_EVENT);
  constant C_ADC_RD_AWIDTH  : integer := C_ADC_WR_AWIDTH - 1; -- twice the data width of write port
  constant C_TDC_WR_AWIDTH  : integer := log2ceil(CPK_TDC_SAMPLES_PER_EVENT/8); -- parallel ISERDES output (byte)
  constant C_TDC_RD_AWIDTH  : integer := C_TDC_WR_AWIDTH - 1; -- twice the data width of write port
  constant C_TRG_WR_AWIDTH  : integer := log2ceil(CPK_TRG_SAMPLES_PER_EVENT);
  constant C_TRG_RD_AWIDTH  : integer := C_TRG_WR_AWIDTH + 2; -- a quarter of the data width at the write port

  signal drs_buf_dout       : std_logic_vector(CPK_DRS_ADC_RD_DWIDTH-1 downto 0) := (others=>'0');
  signal adc_buf_dout       : std_logic_vector(CPK_DRS_ADC_RD_DWIDTH-1 downto 0) := (others=>'0');
  signal tdc_buf_dout       : std_logic_vector(CPK_TDC_RD_DWIDTH-1 downto 0)     := (others=>'0');
  signal trg_buf_dout       : std_logic_vector(CPK_TRG_RD_DWIDTH-1 downto 0)     := (others=>'0');
  signal scl_buf_dout       : std_logic_vector(CPK_SCL_RD_DWIDTH-1 downto 0)     := (others=>'0');

  signal drs_data_valid     : std_logic := '0';
  signal adc_data_valid     : std_logic := '0';
  signal tdc_data_valid     : std_logic := '0';
  signal trg_data_valid     : std_logic := '0';
  signal scl_data_valid     : std_logic := '0';

  signal drs_buf_ren        : std_logic := '0';
  signal adc_buf_ren        : std_logic := '0';
  signal tdc_buf_ren        : std_logic := '0';
  signal trg_buf_ren        : std_logic := '0';
  signal scl_buf_ren        : std_logic := '0';
  
  signal adc_sample_div     : std_logic_vector(7 downto 0) := (others=>'0');
  signal adc_sample_count   : std_logic_vector(7 downto 0) := (others=>'0');
  signal adc_buf_wen        : std_logic := '0';
  
  signal last_type          : std_logic_vector(7 downto 0) := (others=>'0');
  
  signal drs_extra_ch_en    : std_logic := '0';

  -- number of samples to transmit
  signal drs_tx_samples     : std_logic_vector(15 downto 0) := (others=>'0');
  signal adc_tx_samples     : std_logic_vector(15 downto 0) := (others=>'0');
  signal tdc_tx_samples     : std_logic_vector(15 downto 0) := (others=>'0');
  signal trg_tx_samples     : std_logic_vector(15 downto 0) := (others=>'0');

  -- number of samples to per package
  signal drs_max_samples    : std_logic_vector(17 downto 0) := (others=>'0');
  signal adc_max_samples    : std_logic_vector(17 downto 0) := (others=>'0');
  signal tdc_max_samples    : std_logic_vector(17 downto 0) := (others=>'0');
  signal trg_max_samples    : std_logic_vector(17 downto 0) := (others=>'0');
  signal scl_max_samples    : std_logic_vector(17 downto 0) := (others=>'0');

  -- channels to transmit
  signal drs_ch_tx_en       : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0) := (others=>'0');
  signal adc_ch_tx_en       : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal tdc_ch_tx_en       : std_logic_vector(CPK_NR_OF_TDC_CHNLS-1 downto 0)              := (others=>'0');

  -- last channels
  signal drs_last_ch        : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0) := (others=>'0');
  signal adc_last_ch        : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal tdc_last_ch        : std_logic_vector(CPK_NR_OF_TDC_CHNLS-1 downto 0)              := (others=>'0');

  -- type transmit enables
  signal drs_tx_en          : std_logic := '0'; 
  signal adc_tx_en          : std_logic := '0'; 
  signal tdc_tx_en          : std_logic := '0'; 
  signal trg_tx_en          : std_logic := '0';
  signal scl_tx_en          : std_logic := '0';
  signal dmy_tx_en          : std_logic := '0';

  -- zero suppression
  type zero_suppr_mask_type  is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(CPK_NR_OF_TDC_CHNLS-1 downto 0);
  signal zero_suppr_mask    : zero_suppr_mask_type := (others=>(others=>'0'));
  signal drs_ch_tx_en_zs    : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0) := (others=>'0');
  signal adc_ch_tx_en_zs    : std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC-1 downto 0) := (others=>'0');
  signal tdc_ch_tx_en_zs    : std_logic_vector(CPK_NR_OF_TDC_CHNLS-1 downto 0)              := (others=>'0');

  -- buffer flags of types
  signal drs_buf_0to7_ready : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0');
  signal drs_buf_8_ready    : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0');
  signal adc_ring_buf_ready : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0');
  signal tdc_ring_buf_ready : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0');
  signal trg_ring_buf_ready : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0');

  -- buffer flags of whole buffer
  signal ring_buffer_ready  : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0');
  signal buffer_sent        : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'1');
  
  signal scl_data           : std_logic_vector(CPK_NR_OF_SCALERS*CPK_SCL_WR_DWIDTH-1 downto 0) := (others=>'0');

  signal store_z_suppr_sr   : std_logic_vector(3 downto 0) := (others=>'0');
  signal store_z_suppr      : std_logic := '0';

begin

  -- Data Output MUX
  process(CLK_I)
    -- Note: buffer selection is done using the MSBs of read address
  begin
    if rising_edge(CLK_I) then
      if RD_INIT_I = '1' or RST_I = '1' then
        DATA_SIZE_O  <= "00";
        DATA_O       <= (others=>'0');
        DATA_VALID_O <= '0';
      elsif RD_EN_I = '1' then
        case RD_DATA_TYPE_SEL_I is
        when CPK_PKG_TYPE_DRS => 
          DATA_SIZE_O  <= "10";
          -- 2x12bit to 3x8bit data shuffling
          DATA_O       <= drs_buf_dout;
          DATA_VALID_O <= drs_data_valid;
        when CPK_PKG_TYPE_ADC =>
          DATA_SIZE_O  <= "10";
          -- 2x12bit to 3x8bit data shuffling
          DATA_O       <= adc_buf_dout;
          DATA_VALID_O <= adc_data_valid;
        when CPK_PKG_TYPE_TDC =>
          DATA_SIZE_O  <= "01";
          DATA_O       <= X"00" & tdc_buf_dout;
          DATA_VALID_O <= tdc_data_valid;
        when CPK_PKG_TYPE_TRG =>
          DATA_SIZE_O  <= "01";
          DATA_O       <= X"00" & trg_buf_dout;
          DATA_VALID_O <= trg_data_valid;
        when CPK_PKG_TYPE_SCL =>
          DATA_SIZE_O  <= "01";
          DATA_O       <= X"00" & scl_buf_dout;
          DATA_VALID_O <= scl_data_valid;
        when CPK_PKG_TYPE_DMY =>
          DATA_SIZE_O  <= "01";
          DATA_O       <= X"DDC0DE";
          DATA_VALID_O <= '1';
        when others => 
          DATA_SIZE_O  <= "00";
          DATA_O       <= X"D0" & RD_DATA_TYPE_SEL_I & "000" & RD_CH_SEL_I;
          DATA_VALID_O <= '0';
        end case;
      end if;
    end if;
  end process;

  -- Control Output MUX
  process(RD_BUF_SEL_I, RD_DATA_TYPE_SEL_I, RD_CH_SEL_I,
          drs_ch_tx_en, drs_ch_tx_en_zs, drs_tx_samples, drs_max_samples, drs_last_ch,
          adc_ch_tx_en, adc_ch_tx_en_zs, adc_tx_samples, adc_max_samples, adc_last_ch,
          tdc_ch_tx_en, tdc_ch_tx_en_zs, tdc_tx_samples, tdc_max_samples, tdc_last_ch,
          trg_tx_en, trg_tx_samples, trg_max_samples,
          scl_tx_en, scl_max_samples)
    -- Note: buffer selection is done using the MSBs of read address
  begin
    case RD_DATA_TYPE_SEL_I is
      when CPK_PKG_TYPE_DRS => 
        TYPE_TX_EN_O          <= drs_ch_tx_en;
        ITEM_TX_EN_O          <= drs_ch_tx_en_zs(CONV_INTEGER(RD_CH_SEL_I));
        DATASET_TX_SAMPLES_O  <= drs_tx_samples;
        MAX_SAMPLES_PER_PKG_O <= drs_max_samples;
        LAST_CH_O             <= drs_last_ch(CONV_INTEGER(RD_CH_SEL_I));
      when CPK_PKG_TYPE_ADC =>
        TYPE_TX_EN_O          <= "00" & adc_ch_tx_en;
        ITEM_TX_EN_O          <= adc_ch_tx_en_zs(CONV_INTEGER(RD_CH_SEL_I));
        DATASET_TX_SAMPLES_O  <= adc_tx_samples;
        MAX_SAMPLES_PER_PKG_O <= adc_max_samples;
        LAST_CH_O             <= adc_last_ch(CONV_INTEGER(RD_CH_SEL_I));
      when CPK_PKG_TYPE_TDC =>
        TYPE_TX_EN_O          <= "00" & tdc_ch_tx_en;
        ITEM_TX_EN_O          <= tdc_ch_tx_en_zs(CONV_INTEGER(RD_CH_SEL_I));
        DATASET_TX_SAMPLES_O  <= tdc_tx_samples;
        MAX_SAMPLES_PER_PKG_O <= tdc_max_samples;
        LAST_CH_O             <= tdc_last_ch(CONV_INTEGER(RD_CH_SEL_I));
      when CPK_PKG_TYPE_TRG =>
        TYPE_TX_EN_O          <= (others=>'1');
        ITEM_TX_EN_O          <= trg_tx_en;
        DATASET_TX_SAMPLES_O  <= trg_tx_samples;
        MAX_SAMPLES_PER_PKG_O <= trg_max_samples;
        LAST_CH_O             <= '1';
      when CPK_PKG_TYPE_SCL =>
        TYPE_TX_EN_O          <= (others=>'1');
        ITEM_TX_EN_O          <= scl_tx_en;
        DATASET_TX_SAMPLES_O  <= CONV_STD_LOGIC_VECTOR(CPK_NR_OF_SCALERS, DATASET_TX_SAMPLES_O'length);
        MAX_SAMPLES_PER_PKG_O <= scl_max_samples;
        LAST_CH_O             <= '1';
      when CPK_PKG_TYPE_DMY =>
        TYPE_TX_EN_O          <= (others=>'0');
        ITEM_TX_EN_O          <= dmy_tx_en;
        DATASET_TX_SAMPLES_O  <= (others=>'0');
        MAX_SAMPLES_PER_PKG_O <= (others=>'0');
        LAST_CH_O             <= '1';
      when others =>
        TYPE_TX_EN_O          <= (others=>'0');
        ITEM_TX_EN_O          <= '0';
        DATASET_TX_SAMPLES_O  <= (others=>'0');
        MAX_SAMPLES_PER_PKG_O <= (others=>'0');
        LAST_CH_O             <= '0';
    end case;
  end process;

  WR_DRS_BUFFER_READY_O  <= drs_buf_8_ready(CONV_INTEGER(WR_BUF_SEL_I))
                            when drs_extra_ch_en = '1' else
                            drs_buf_0to7_ready(CONV_INTEGER(WR_BUF_SEL_I));
  RD_DRS_BUFFER_READY_O  <= drs_buf_8_ready(CONV_INTEGER(RD_BUF_SEL_I))
                            when drs_extra_ch_en = '1' else
                            drs_buf_0to7_ready(CONV_INTEGER(RD_BUF_SEL_I));
  WR_RING_BUFFER_READY_O <= ring_buffer_ready(CONV_INTEGER(WR_BUF_SEL_I));
  RD_RING_BUFFER_READY_O <= ring_buffer_ready(CONV_INTEGER(RD_BUF_SEL_I));
  WR_BUFFER_SENT_O       <= buffer_sent(CONV_INTEGER(WR_BUF_SEL_I));
  RD_BUFFER_SENT_O       <= buffer_sent(CONV_INTEGER(RD_BUF_SEL_I));

  drs_extra_ch_en <= OR_REDUCE(drs_ch_tx_en(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC));   

  adc_sample_div <= X"00" when ADC_SAMPLING_DIV_I = 0 else ADC_SAMPLING_DIV_I - 1;
  
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' or adc_sample_count = 0 then
        adc_sample_count <= adc_sample_div;
      else
        adc_sample_count <= adc_sample_count - 1;
      end if;
    end if;
  end process;

  adc_buf_wen <= ADC_BUF_WEN_I when adc_sample_count = 0 else '0';

  drs_buf_ren <= RD_EN_I when RD_DATA_TYPE_SEL_I = CPK_PKG_TYPE_DRS else '0';
  adc_buf_ren <= RD_EN_I when RD_DATA_TYPE_SEL_I = CPK_PKG_TYPE_ADC else '0';
  tdc_buf_ren <= RD_EN_I when RD_DATA_TYPE_SEL_I = CPK_PKG_TYPE_TDC else '0';
  trg_buf_ren <= RD_EN_I when RD_DATA_TYPE_SEL_I = CPK_PKG_TYPE_TRG else '0';
  scl_buf_ren <= RD_EN_I when RD_DATA_TYPE_SEL_I = CPK_PKG_TYPE_SCL else '0';
  
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if ZERO_SUPPRESSION_EN_I = '1' and ZERO_SUPPRESSION_INHIBIT_I = '0' then
        drs_ch_tx_en_zs <= drs_ch_tx_en and ("11" & zero_suppr_mask(CONV_INTEGER(RD_BUF_SEL_I)));
        adc_ch_tx_en_zs <= adc_ch_tx_en and zero_suppr_mask(CONV_INTEGER(RD_BUF_SEL_I));
        tdc_ch_tx_en_zs <= tdc_ch_tx_en and zero_suppr_mask(CONV_INTEGER(RD_BUF_SEL_I));
      else
        drs_ch_tx_en_zs <= drs_ch_tx_en;
        adc_ch_tx_en_zs <= adc_ch_tx_en;
        tdc_ch_tx_en_zs <= tdc_ch_tx_en;
      end if;
    end if;
  end process;

  ZERO_SUPPRESSION_MASK_O <= zero_suppr_mask(CONV_INTEGER(RD_BUF_SEL_I));
  
  -- DRS decode last_ch
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      drs_last_ch <= (others=>'0');
      for i in drs_ch_tx_en_zs'length-1 downto 0 loop
        if drs_ch_tx_en_zs(i) = '1' then
          drs_last_ch(i) <= '1';
          exit;
        elsif i = 0 then
          drs_last_ch(i) <= '1';
        end if;
      end loop;
    end if;
  end process;

  -- ADC decode last_ch
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      adc_last_ch <= (others=>'0');
      for i in adc_ch_tx_en_zs'length-1 downto 0 loop
        if adc_ch_tx_en_zs(i) = '1' then
          adc_last_ch(i) <= '1';
          exit;
        elsif i = 0 then
          adc_last_ch(i) <= '1';
        end if;
      end loop;
    end if;
  end process;

  -- TDC decode last_ch
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      tdc_last_ch <= (others=>'0');
      for i in tdc_ch_tx_en_zs'length-1 downto 0 loop
        if tdc_ch_tx_en_zs(i) = '1' then
          tdc_last_ch(i) <= '1';
          exit;
        elsif i = 0 then
          tdc_last_ch(i) <= '1';
        end if;
      end loop;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        drs_tx_en       <= '0';
        adc_tx_en       <= '0';
        tdc_tx_en       <= '0';
        trg_tx_en       <= '0';
        scl_tx_en       <= '0';
        drs_tx_samples  <= (others=>'0');
        adc_tx_samples  <= (others=>'0');
        tdc_tx_samples  <= (others=>'0');
        trg_tx_samples  <= (others=>'0');
        drs_max_samples <= (others=>'0');
        adc_max_samples <= (others=>'0');
        tdc_max_samples <= (others=>'0');
        trg_max_samples <= (others=>'0');
        scl_max_samples <= (others=>'0');
      elsif STORE_TX_ENABLE_I = '1' then
        if DRS_CH_TX_EN_I = 0 then
          drs_tx_en <= '0';
        else
          drs_tx_en <= '1';
        end if;
        if ADC_CH_TX_EN_I = 0 then
          adc_tx_en <= '0';
        else
          adc_tx_en <= '1';
        end if;
        if TDC_CH_TX_EN_I = 0 then
          tdc_tx_en <= '0';
        else
          tdc_tx_en <= '1';
        end if;
        trg_tx_en       <= TRG_TX_EN_I;
        scl_tx_en       <= SCL_TX_EN_I;
        drs_tx_samples  <= "00000"  & DRS_TX_SAMPLES_I;
        adc_tx_samples  <= "0000"   & ADC_TX_SAMPLES_I;
        tdc_tx_samples  <= "000"    & TDC_TX_SAMPLES_I;
        trg_tx_samples  <= "000000" & TRG_TX_SAMPLES_I;
        drs_max_samples <= "00" & DRS_MAX_SAMPLES_PER_PKG_I;
        adc_max_samples <= "00" & ADC_MAX_SAMPLES_PER_PKG_I;
        tdc_max_samples <= TDC_MAX_SAMPLES_PER_PKG_I;
        trg_max_samples <= "00" & TRG_MAX_SAMPLES_PER_PKG_I;
        scl_max_samples <= "00" & SCL_MAX_SAMPLES_PER_PKG_I;
      end if;
    end if;
  end process;

  -- register tx_en
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        drs_ch_tx_en <= (others=>'0');
        adc_ch_tx_en <= (others=>'0');
        tdc_ch_tx_en <= (others=>'0');
      elsif STORE_TX_ENABLE_I = '1' then
        drs_ch_tx_en <= DRS_CH_TX_EN_I;
        adc_ch_tx_en <= ADC_CH_TX_EN_I;
        tdc_ch_tx_en <= TDC_CH_TX_EN_I;
      end if;
    end if;
  end process;

  -- if there is nothing at all to transmit:
  -- transmit the dummy (header only) package
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if trg_tx_en = '0' and 
         scl_tx_en = '0' and
         drs_ch_tx_en_zs = 0 and
         adc_ch_tx_en_zs = 0 and
         tdc_ch_tx_en_zs = 0 and
         (drs_tx_en = '1' or adc_tx_en = '1' or tdc_tx_en = '1') then -- if everything is disabled in registers, do not send dummy packet
        dmy_tx_en <= '1';
      else
        dmy_tx_en <= '0';
      end if;
    end if;
  end process;

  -- register zero suppression
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        for i in 0 to CPK_NR_OF_BUFFERS-1 loop
          zero_suppr_mask(i) <= (others=>'0');
        end loop;
      elsif store_z_suppr = '1' then
        zero_suppr_mask(CONV_INTEGER(WR_BUF_SEL_I)) <= ZERO_SUPPRESSION_MASK_I;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      store_z_suppr_sr <= STORE_ZERO_SUPPRESSION_I & store_z_suppr_sr(3 downto 1);
    end if;
  end process;
  store_z_suppr <= store_z_suppr_sr(0);

  -- decode last data type to transmit (needed to define last packet)
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        last_type <= (others=>'0');
      else
        if dmy_tx_en = '1' then
          last_type <= CPK_PKG_TYPE_DMY;
        elsif scl_tx_en = '1' then
          last_type <= CPK_PKG_TYPE_SCL;
        elsif trg_tx_en = '1' then
          last_type <= CPK_PKG_TYPE_TRG;
        elsif tdc_tx_en = '1' then
          last_type <= CPK_PKG_TYPE_TDC;
        elsif adc_tx_en = '1' then
          last_type <= CPK_PKG_TYPE_ADC;
        else -- elsif drs_tx_en(CONV_INTEGER(RD_BUF_SEL_I)) = '1' then
          last_type <= CPK_PKG_TYPE_DRS;
        end if;
      end if;
    end if;
  end process;
  
  LAST_TYPE_O <= '1' when RD_DATA_TYPE_SEL_I = last_type else '0';
  
  -- full flag register
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        ring_buffer_ready <= (others=>'0');
      else
        if CLR_BUFFER_I = '1' then
          ring_buffer_ready(CONV_INTEGER(WR_BUF_SEL_I)) <= '0';
        elsif adc_ring_buf_ready(CONV_INTEGER(WR_BUF_SEL_I)) = '1' and 
              tdc_ring_buf_ready(CONV_INTEGER(WR_BUF_SEL_I)) = '1' and
              trg_ring_buf_ready(CONV_INTEGER(WR_BUF_SEL_I)) = '1' then
          ring_buffer_ready(CONV_INTEGER(WR_BUF_SEL_I)) <= '1';
        end if;
      end if;
    end if;
  end process;

  -- sent flag register
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        buffer_sent <= (others=>'1');
      else
        if SET_BUFFER_SENT_I = '1' then
          buffer_sent(CONV_INTEGER(RD_BUF_SEL_I)) <= '1';
        end if;
        if CLR_BUFFER_SENT_I = '1' then
          buffer_sent(CONV_INTEGER(WR_BUF_SEL_I)) <= '0';
        end if;
      end if;
    end if;
  end process;

  --------------------------------------------------------------------------
  -- DRS Buffer                                                           --
  --------------------------------------------------------------------------
  DRS_DATA_BUFFERS : entity wd2_pkg_gen_v2_00_a.drs_data_buffer
  generic map
  (
    CGN_BUFFER_SELECT_WIDTH  => C_BUF_SEL_WIDTH,
    CGN_READ_CH_SELECT_WIDTH => 5,
    CGN_NR_OF_DRS_CHANNELS   => CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS,
    CGN_NR_OF_ADC_CHANNELS   => CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC,
    CGN_WR_ADDR_WIDTH        => C_DRS_WR_AWIDTH,
    CGN_RD_ADDR_WIDTH        => C_DRS_RD_AWIDTH,
    CGN_WR_CH_DATA_WIDTH     => CPK_DRS_ADC_CH_WR_DWIDTH,
    CGN_RD_CH_DATA_WIDTH     => CPK_DRS_ADC_RD_DWIDTH
  )
  port map
  (
    WR_BUF_SEL_I     => WR_BUF_SEL_I,
    CLR_BUF_I        => CLR_BUFFER_I,
    WR_EN_I          => DRS_BUF_WEN_I,
    DATA_I           => DRS_ADC_DATA_I,
    RD_BUF_SEL_I     => RD_BUF_SEL_I,
    RD_CH_SEL_I      => RD_CH_SEL_I,
    RD_INIT_I        => RD_INIT_I,
    RD_EN_I          => drs_buf_ren,
    DATA_VALID_O     => drs_data_valid,
    DATA_O           => drs_buf_dout,
    BUF_0TO7_READY_O => drs_buf_0to7_ready,
    BUF_8_READY_O    => drs_buf_8_ready,
    CLK_I            => CLK_I,
    RST_I            => RST_I
  );

  --------------------------------------------------------------------------
  -- ADC Ring Buffer                                                      --
  --------------------------------------------------------------------------
  ADC_RING_BUFFERS : entity wd2_pkg_gen_v2_00_a.multi_channel_ring_buffer
  generic map
  (
    CGN_BUFFER_SELECT_WIDTH  => C_BUF_SEL_WIDTH,
    CGN_READ_CH_SELECT_WIDTH => 4,
    CGN_NR_OF_CHANNELS       => CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC,
    CGN_WR_ADDR_WIDTH        => C_ADC_WR_AWIDTH,
    CGN_RD_ADDR_WIDTH        => C_ADC_RD_AWIDTH,
    CGN_WR_CH_DATA_WIDTH     => CPK_DRS_ADC_CH_WR_DWIDTH,
    CGN_RD_CH_DATA_WIDTH     => CPK_DRS_ADC_RD_DWIDTH,
    CGN_RAM_CONFIG           => "8kx12_4kx24"
  )
  port map
  (
    WR_BUF_SEL_I => WR_BUF_SEL_I,
    CLR_BUF_I    => CLR_BUFFER_I,
    WR_EN_I      => adc_buf_wen,
    DATA_I       => DRS_ADC_DATA_I,
    RD_BUF_SEL_I => RD_BUF_SEL_I,
    RD_CH_SEL_I  => RD_CH_SEL_I(3 downto 0),
    RD_INIT_I    => RD_INIT_I,
    RD_EN_I      => adc_buf_ren,
    DATA_VALID_O => adc_data_valid,
    DATA_O       => adc_buf_dout,
    SATURATED_O  => adc_ring_buf_ready,
    CLK_I        => CLK_I,
    RST_I        => RST_I
  );
  
  --------------------------------------------------------------------------
  -- TDC Ring Buffer                                                      --
  --------------------------------------------------------------------------
  TDC_RING_BUFFERS : entity wd2_pkg_gen_v2_00_a.multi_channel_ring_buffer
  generic map
  (
    CGN_BUFFER_SELECT_WIDTH  => C_BUF_SEL_WIDTH,
    CGN_READ_CH_SELECT_WIDTH => 4,
    CGN_NR_OF_CHANNELS       => CPK_NR_OF_TDC_CHNLS,
    CGN_WR_ADDR_WIDTH        => C_TDC_WR_AWIDTH,
    CGN_RD_ADDR_WIDTH        => C_TDC_RD_AWIDTH,
    CGN_WR_CH_DATA_WIDTH     => CPK_TDC_WR_DWIDTH,
    CGN_RD_CH_DATA_WIDTH     => CPK_TDC_RD_DWIDTH,
    CGN_RAM_CONFIG           => "2kx08_1kx16"
  )
  port map
  (
    WR_BUF_SEL_I => WR_BUF_SEL_I,
    CLR_BUF_I    => CLR_BUFFER_I,
    WR_EN_I      => TDC_BUF_WEN_I,
    DATA_I       => TDC_DATA_I,
    RD_BUF_SEL_I => RD_BUF_SEL_I,
    RD_CH_SEL_I  => RD_CH_SEL_I(3 downto 0),
    RD_INIT_I    => RD_INIT_I,
    RD_EN_I      => tdc_buf_ren,
    DATA_VALID_O => tdc_data_valid,
    DATA_O       => tdc_buf_dout,
    SATURATED_O  => tdc_ring_buf_ready,
    CLK_I        => CLK_I,
    RST_I        => RST_I
  );

  --------------------------------------------------------------------------
  -- Trigger Ring Buffer                                                  --
  --------------------------------------------------------------------------
  TRG_RING_BUFFERS : entity wd2_pkg_gen_v2_00_a.multi_channel_ring_buffer
  generic map
  (
    CGN_BUFFER_SELECT_WIDTH  => C_BUF_SEL_WIDTH,
    CGN_READ_CH_SELECT_WIDTH => 1,
    CGN_NR_OF_CHANNELS       => 1,
    CGN_WR_ADDR_WIDTH        => C_TRG_WR_AWIDTH,
    CGN_RD_ADDR_WIDTH        => C_TRG_RD_AWIDTH,
    CGN_WR_CH_DATA_WIDTH     => CPK_TRG_WR_DWIDTH,
    CGN_RD_CH_DATA_WIDTH     => CPK_TRG_RD_DWIDTH,
    CGN_RAM_CONFIG           => "2kx64_8kx16"
  )
  port map
  (
    WR_BUF_SEL_I => WR_BUF_SEL_I,
    CLR_BUF_I    => CLR_BUFFER_I,
    WR_EN_I      => TRG_BUF_WEN_I,
    DATA_I       => TRG_DATA_I,
    RD_BUF_SEL_I => RD_BUF_SEL_I,
    RD_CH_SEL_I  => "0",
    RD_INIT_I    => RD_INIT_I,
    RD_EN_I      => trg_buf_ren,
    DATA_VALID_O => trg_data_valid,
    DATA_O       => trg_buf_dout,
    SATURATED_O  => trg_ring_buf_ready,
    CLK_I        => CLK_I,
    RST_I        => RST_I
  );

  --------------------------------------------------------------------------
  -- Scaler Buffer                                                        --
  --------------------------------------------------------------------------
  -- channel order
  -- leave extra channels as they are
  scl_data  ((CPK_MAX_NR_OF_ADC_CHNLS+2)*CPK_SCL_BITS_PER_SCALER-1 downto CPK_MAX_NR_OF_ADC_CHNLS*CPK_SCL_BITS_PER_SCALER) <=
  SCL_DATA_I((CPK_MAX_NR_OF_ADC_CHNLS+2)*CPK_SCL_BITS_PER_SCALER-1 downto CPK_MAX_NR_OF_ADC_CHNLS*CPK_SCL_BITS_PER_SCALER);
  -- swap adc channels from (15:0) to (0:15)
  process(SCL_DATA_I)
  begin
    for i in CPK_MAX_NR_OF_ADC_CHNLS-1 downto 0 loop
      scl_data((i+1)*CPK_SCL_BITS_PER_SCALER-1 downto i*CPK_SCL_BITS_PER_SCALER)
        <= SCL_DATA_I((CPK_MAX_NR_OF_ADC_CHNLS-i)*CPK_SCL_BITS_PER_SCALER-1 downto (CPK_MAX_NR_OF_ADC_CHNLS-i-1)*CPK_SCL_BITS_PER_SCALER);
    end loop;
  end process;

  SCALER_DATA_BUFFER : entity wd2_pkg_gen_v2_00_a.scaler_data_buffer
  generic map
  (
    CGN_BUFFER_SELECT_WIDTH => C_BUF_SEL_WIDTH
  )
  port map
  (
    WR_BUF_SEL_I => WR_BUF_SEL_I,
    CLR_BUF_I    => CLR_BUFFER_I,
    WR_EN_I      => SCL_BUF_WEN_I,
    DATA_I       => scl_data,
    RD_BUF_SEL_I => RD_BUF_SEL_I,
    RD_INIT_I    => RD_INIT_I,
    RD_EN_I      => scl_buf_ren,
    DATA_VALID_O => scl_data_valid,
    DATA_O       => scl_buf_dout,
    READY_O      => open,
    CLK_I        => CLK_I,
    RST_I        => RST_I
  );

end struct;
