---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 DRS Data Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  18.10.2017 11:09
--
--  Description : Synchronous DRS data buffer.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;
use wd2_pkg_gen_v2_00_a.bram_512x16_512x16;

entity scaler_data_buffer is
generic
(
  CGN_BUFFER_SELECT_WIDTH  : integer := 2
);
port
(
  -- Write
  WR_BUF_SEL_I     : in  std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  CLR_BUF_I        : in  std_logic;
  WR_EN_I          : in  std_logic;
  DATA_I           : in  std_logic_vector(CPK_NR_OF_SCALERS*CPK_SCL_WR_DWIDTH-1 downto 0);
  
  -- Read
  RD_BUF_SEL_I     : in  std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  RD_INIT_I        : in  std_logic;
  RD_EN_I          : in  std_logic;
  DATA_VALID_O     : out std_logic;
  DATA_O           : out std_logic_vector(CPK_SCL_RD_DWIDTH-1 downto 0);
  
  -- Flags
  READY_O          : out std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');
  
  CLK_I            : in  std_logic;
  RST_I            : in  std_logic
);
end scaler_data_buffer;

architecture behavioral of scaler_data_buffer is

  constant C_WR_WORDS_PER_SCL : integer := CPK_SCL_WR_DWIDTH / CPK_SCL_RD_DWIDTH;
  constant C_TOTAL_WR_WORDS   : integer := CPK_NR_OF_SCALERS * C_WR_WORDS_PER_SCL;
  constant C_ADDR_WIDTH       : integer := log2ceil(C_TOTAL_WR_WORDS);
  constant C_END_ADDR         : std_logic_vector(C_ADDR_WIDTH-1 downto 0) := CONV_STD_LOGIC_VECTOR(C_TOTAL_WR_WORDS-1, C_ADDR_WIDTH);

  type scaler_buf_type is array (C_TOTAL_WR_WORDS-1 downto 0) of std_logic_vector(CPK_SCL_RD_DWIDTH-1 downto 0);
  signal scaler_buf : scaler_buf_type := (others=>(others=>'0'));

  signal wr_buf_sel   : std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');
  signal bram_wen     : std_logic := '0';
  signal ren_s        : std_logic := '0';
  signal data_valid   : std_logic := '0';
  signal data_valid_s : std_logic := '0';

  signal wr_adr       : std_logic_vector(C_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal rd_adr       : std_logic_vector(C_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal bram_adr_a   : std_logic_vector(CGN_BUFFER_SELECT_WIDTH+C_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal bram_adr_b   : std_logic_vector(CGN_BUFFER_SELECT_WIDTH+C_ADDR_WIDTH-1 downto 0) := (others=>'0');

  signal data_s       : std_logic_vector(CPK_SCL_RD_DWIDTH-1 downto 0) := (others=>'0');
  signal bram_dout    : std_logic_vector(CPK_SCL_RD_DWIDTH-1 downto 0) := (others=>'0');

begin

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      wr_buf_sel <= WR_BUF_SEL_I;
    end if;
  end process;  

  -- writing (shift register structure)
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if WR_EN_I = '1' then
        -- load buffer into shift register
        for i in CPK_NR_OF_SCALERS-1 downto 0 loop
          for j in C_WR_WORDS_PER_SCL-1 downto 0 loop
            scaler_buf(i*C_WR_WORDS_PER_SCL+j) <= DATA_I((i*C_WR_WORDS_PER_SCL+(j+1))*CPK_SCL_RD_DWIDTH-1 downto (i*C_WR_WORDS_PER_SCL+j)*CPK_SCL_RD_DWIDTH);
          end loop;
        end loop;
      elsif bram_wen = '1' then
        -- shift
        for i in C_TOTAL_WR_WORDS-1 downto 1 loop
          scaler_buf(i) <= scaler_buf(i-1);
        end loop;
        scaler_buf(0) <= (others=>'0');
      end if;
    end if;
  end process;

  -- manage write address and ready flag
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        wr_adr   <= (others=>'0');
        bram_wen <= '0';
        READY_O  <= (others=>'0');
      else
        if CLR_BUF_I = '1' then
          wr_adr   <= (others=>'0');
          bram_wen <= '0';
          READY_O(CONV_INTEGER(wr_buf_sel)) <= '0';
        elsif bram_wen = '1' then
          if wr_adr = C_END_ADDR then
            bram_wen <= '0';
            READY_O(CONV_INTEGER(wr_buf_sel)) <= '1';
          else
            wr_adr <= wr_adr + 1;
          end if;
        elsif WR_EN_I = '1' then
          bram_wen <= '1';
        end if;
      end if;
    end if;
  end process;

  -- manage read address
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RD_INIT_I = '1' or RST_I = '1' then
        data_valid <= '0';
        rd_adr     <= (others=>'0');
      elsif RD_EN_I = '1' then
        data_valid <= '1';
        rd_adr     <= rd_adr + 1;
      end if;
    end if;
  end process;

  -- output register and valid flag
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      ren_s        <= RD_EN_I;
      data_valid_s <= data_valid;
      if RD_INIT_I = '1' then
        DATA_VALID_O <= '0';
        DATA_O       <= (others=>'0');
      elsif RD_EN_I = '1' and ren_s = '0' then
        DATA_VALID_O <= data_valid_s;
        DATA_O       <= data_s;
      elsif RD_EN_I = '1' then
        DATA_VALID_O <= data_valid;
        DATA_O       <= bram_dout;
      end if;
      if ren_s = '1' then
        data_s <= bram_dout;
      end if;
    end if;
  end process;

  -- manage buffer addresses
  bram_adr_a <= wr_buf_sel & wr_adr;
  bram_adr_b <= RD_BUF_SEL_I & rd_adr;

  bram_512x16_512x16_inst : entity wd2_pkg_gen_v2_00_a.bram_512x16_512x16
  port map
  (
    WR_A_DATA_I => scaler_buf(C_TOTAL_WR_WORDS-1),
    WR_A_ADDR_I => bram_adr_a,
    WR_A_EN_I   => bram_wen,
    RD_B_DATA_O => bram_dout,
    RD_B_ADDR_I => bram_adr_b,
    CLK_I       => CLK_I
  );

end behavioral;

--        scaler_buf(71) <= DATA_I(1151 downto 1136); -- 17*4+3 <= (17*4+(3+1))*16-1 downto (17*4+3)*16
--        scaler_buf(70) <= DATA_I(1135 downto 1120); -- 17*4+2 <= (17*4+(2+1))*16-1 downto (17*4+2)*16
--        scaler_buf(69) <= DATA_I(1119 downto 1104); -- 17*4+1 <= (17*4+(1+1))*16-1 downto (17*4+1)*16
--        scaler_buf(68) <= DATA_I(1103 downto 1088); -- 17*4+0 <= (17*4+(0+1))*16-1 downto (17*4+0)*16
--        scaler_buf(67) <= DATA_I(1187 downto 1172); -- 16*4+3 <= (16*4+(3+1))*16-1 downto (16*4+3)*16
--        scaler_buf(66) <= DATA_I(1171 downto 1156); -- 16*4+2 <= (16*4+(2+1))*16-1 downto (16*4+2)*16
--        scaler_buf(65) <= DATA_I(1155 downto 1140); -- 16*4+1 <= (16*4+(1+1))*16-1 downto (16*4+1)*16
--        scaler_buf(64) <= DATA_I(1139 downto 1024); -- 16*4+0 <= (16*4+(0+1))*16-1 downto (16*4+0)*16
--        .
--        .
--        .
--        scaler_buf(7)  <= DATA_I( 127 downto  112); -- 1*4+3 <= (1*4+(3+1))*16-1 downto (1*4+3)*16
--        scaler_buf(6)  <= DATA_I( 111 downto   96); -- 1*4+2 <= (1*4+(2+1))*16-1 downto (1*4+2)*16
--        scaler_buf(5)  <= DATA_I(  95 downto   80); -- 1*4+1 <= (1*4+(1+1))*16-1 downto (1*4+1)*16
--        scaler_buf(4)  <= DATA_I(  79 downto   64); -- 1*4+0 <= (1*4+(0+1))*16-1 downto (1*4+0)*16
--        scaler_buf(3)  <= DATA_I(  63 downto   48); -- 0*4+3 <= (0*4+(3+1))*16-1 downto (0*4+3)*16
--        scaler_buf(2)  <= DATA_I(  47 downto   32); -- 0*4+2 <= (0*4+(2+1))*16-1 downto (0*4+2)*16
--        scaler_buf(1)  <= DATA_I(  31 downto   16); -- 0*4+1 <= (0*4+(1+1))*16-1 downto (0*4+1)*16
--        scaler_buf(0)  <= DATA_I(  15 downto    0); -- 0*4+0 <= (0*4+(0+1))*16-1 downto (0*4+0)*16
