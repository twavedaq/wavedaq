---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Ring Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  17.10.2017 11:19:34
--
--  Description : Synchronous general purpose multi channel ringbuffer.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

--library psi_3205_v1_00_a;
--use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;
use wd2_pkg_gen_v2_00_a.bram_8kx12_4kx24;
use wd2_pkg_gen_v2_00_a.bram_2kx8_1kx16;
use wd2_pkg_gen_v2_00_a.bram_2kx64_8kx16;

entity multi_channel_ring_buffer is
generic
(
  CGN_BUFFER_SELECT_WIDTH  : integer :=  2;
  CGN_READ_CH_SELECT_WIDTH : integer :=  2;
  CGN_NR_OF_CHANNELS       : integer := 16;
  CGN_WR_ADDR_WIDTH        : integer := 10;
  CGN_RD_ADDR_WIDTH        : integer := 10;
  CGN_WR_CH_DATA_WIDTH     : integer :=  8;
  CGN_RD_CH_DATA_WIDTH     : integer :=  8;
  CGN_RAM_CONFIG           : string  := "none" -- "8kx12_4kx24", "2kx8_1kx16", "2kx64_8kx16" 
);
port
(
  -- Write
  WR_BUF_SEL_I : in  std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  CLR_BUF_I    : in  std_logic;
  WR_EN_I      : in  std_logic;
  DATA_I       : in  std_logic_vector(CGN_NR_OF_CHANNELS*CGN_WR_CH_DATA_WIDTH-1 downto 0);
  
  -- Read
  RD_BUF_SEL_I : in  std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  RD_CH_SEL_I  : in  std_logic_vector(CGN_READ_CH_SELECT_WIDTH-1 downto 0);
  RD_INIT_I    : in  std_logic;
  RD_EN_I      : in  std_logic;
  DATA_VALID_O : out std_logic;
  DATA_O       : out std_logic_vector(CGN_RD_CH_DATA_WIDTH-1 downto 0);
  
  -- Flags
  SATURATED_O  : out std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0);
  
  CLK_I        : in  std_logic;
  RST_I        : in  std_logic
);
end multi_channel_ring_buffer;

architecture behavioral of multi_channel_ring_buffer is

  constant C_DELTA_WIDTH : integer := abs(CGN_RD_ADDR_WIDTH-CGN_WR_ADDR_WIDTH);

  type data_in_type  is array (CGN_NR_OF_CHANNELS-1 downto 0) of std_logic_vector(CGN_WR_CH_DATA_WIDTH-1 downto 0);
  type data_out_type is array (CGN_NR_OF_CHANNELS-1 downto 0) of std_logic_vector(CGN_RD_CH_DATA_WIDTH-1 downto 0);
  signal buf_din       : data_in_type  := (others=>(others=>'0'));
  signal buf_dout      : data_out_type := (others=>(others=>'0'));
  signal buf_adr_a     : std_logic_vector(CGN_BUFFER_SELECT_WIDTH+CGN_WR_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal buf_adr_b     : std_logic_vector(CGN_BUFFER_SELECT_WIDTH+CGN_RD_ADDR_WIDTH-1 downto 0) := (others=>'0');

  signal wr_adr        : std_logic_vector(CGN_WR_ADDR_WIDTH-1 downto 0) := (others=>'0');
  signal rd_adr        : std_logic_vector(CGN_RD_ADDR_WIDTH-1 downto 0) := (others=>'0');
  
  type mem_ptr_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(CGN_WR_ADDR_WIDTH downto 0);
  signal wr_ptr        : mem_ptr_type := (others=>(others=>'0'));
  signal rd_ptr        : mem_ptr_type := (others=>(others=>'0'));

  signal wr_buf_sel    : std_logic_vector(CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');
  
  signal saturated     : std_logic_vector(2**CGN_BUFFER_SELECT_WIDTH-1 downto 0) := (others=>'0');
  signal data_valid    : std_logic := '0';

  signal ren_s         : std_logic := '0';
  signal data_s        : std_logic_vector(CGN_RD_CH_DATA_WIDTH-1 downto 0) := (others=>'0');
  signal data_valid_s  : std_logic := '0';

begin

  SATURATED_O <= saturated;
  
  FLAGS : for buf in 2**CGN_BUFFER_SELECT_WIDTH-1 downto 0 generate
    saturated(buf) <= '1' when ( wr_ptr(buf)(CGN_WR_ADDR_WIDTH-1 downto 0) = rd_ptr(buf)(CGN_WR_ADDR_WIDTH-1 downto 0) ) and
                               ( wr_ptr(buf)(CGN_WR_ADDR_WIDTH) /= rd_ptr(buf)(CGN_WR_ADDR_WIDTH) )
                          else '0';
  end generate;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      wr_buf_sel <= WR_BUF_SEL_I;
    end if;
  end process;  
  
  -- manage write pointer
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        wr_ptr <= (others=>(others=>'0'));
      else
        if CLR_BUF_I = '1' then
          wr_ptr(CONV_INTEGER(wr_buf_sel)) <= (others=>'0');
        elsif WR_EN_I = '1' then
          wr_ptr(CONV_INTEGER(wr_buf_sel)) <= wr_ptr(CONV_INTEGER(wr_buf_sel)) + 1;
        end if;
      end if;
    end if;
  end process;  

  -- manage read pointer
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        rd_ptr <= (others=>(others=>'0'));
      else
        if CLR_BUF_I = '1' then
          rd_ptr(CONV_INTEGER(wr_buf_sel)) <= (others=>'0');
        elsif WR_EN_I = '1' and saturated(CONV_INTEGER(wr_buf_sel)) = '1' then
          rd_ptr(CONV_INTEGER(wr_buf_sel)) <= rd_ptr(CONV_INTEGER(wr_buf_sel)) + 1;
        end if;
      end if;
    end if;
  end process;

  -- manage write address
  wr_adr <= wr_ptr(CONV_INTEGER(wr_buf_sel))(CGN_WR_ADDR_WIDTH-1 downto 0);
  
  -- manage read address
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RD_INIT_I = '1' or RST_I = '1' then
        data_valid <= '0';
        if CGN_WR_ADDR_WIDTH >= CGN_RD_ADDR_WIDTH then
          rd_adr <= (others=>'0');
          rd_adr <= rd_ptr(CONV_INTEGER(RD_BUF_SEL_I))(CGN_WR_ADDR_WIDTH-1 downto C_DELTA_WIDTH);
        else --CGN_WR_ADDR_WIDTH < CGN_RD_ADDR_WIDTH then
          rd_adr <= (others=>'0');
          rd_adr(CGN_RD_ADDR_WIDTH-1 downto C_DELTA_WIDTH) <= rd_ptr(CONV_INTEGER(RD_BUF_SEL_I))(CGN_WR_ADDR_WIDTH-1 downto 0);
        end if;
      elsif RD_EN_I = '1' then
        data_valid <= '1';
        rd_adr <= rd_adr + 1;
      end if;
    end if;
  end process;
  
  -- manage buffer addresses
  buf_adr_a <= wr_buf_sel & wr_adr;
  buf_adr_b <= RD_BUF_SEL_I  & rd_adr;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      ren_s        <= RD_EN_I;
      data_valid_s <= data_valid;
      if RD_INIT_I = '1' then
        DATA_VALID_O <= '0';
        DATA_O       <= (others=>'0');
      elsif RD_EN_I = '1' and ren_s = '0' then
        DATA_VALID_O <= data_valid_s;
        DATA_O       <= data_s;
      elsif RD_EN_I = '1' then
        DATA_VALID_O <= data_valid;
        DATA_O       <= buf_dout(CONV_INTEGER(RD_CH_SEL_I));
      end if;
      if ren_s = '1' then
        data_s <= buf_dout(CONV_INTEGER(RD_CH_SEL_I));
      end if;
    end if;
  end process;
  
  -- buffers
  CH_BUFFERS : for ch in 0 to CGN_NR_OF_CHANNELS-1 generate

    buf_din(ch) <= DATA_I((ch+1)*CGN_WR_CH_DATA_WIDTH-1 downto ch*CGN_WR_CH_DATA_WIDTH);

    adc_buffer_inst : if CGN_RAM_CONFIG = "8kx12_4kx24" generate
      bram_8kx12_4kx24_inst : entity wd2_pkg_gen_v2_00_a.bram_8kx12_4kx24
      port map
      (
        WR_A_DATA_I => buf_din(ch),
        WR_A_ADDR_I => buf_adr_a,
        WR_A_EN_I   => WR_EN_I,
        RD_B_DATA_O => buf_dout(ch),
        RD_B_ADDR_I => buf_adr_b,
        CLK_I       => CLK_I
      );
    end generate;

    tdc_buffer_inst : if CGN_RAM_CONFIG = "2kx08_1kx16" generate
      bram_2kx8_1kx16_inst : entity wd2_pkg_gen_v2_00_a.bram_2kx8_1kx16
      port map
      (
        WR_A_DATA_I => buf_din(ch),
        WR_A_ADDR_I => buf_adr_a,
        WR_A_EN_I   => WR_EN_I,
        RD_B_DATA_O => buf_dout(ch),
        RD_B_ADDR_I => buf_adr_b,
        CLK_I       => CLK_I
      );
    end generate;

    trg_buffers_inst : if CGN_RAM_CONFIG = "2kx64_8kx16" generate
      bram_2kx64_8kx16_inst : entity wd2_pkg_gen_v2_00_a.bram_2kx64_8kx16
      port map
      (
        WR_A_DATA_I => buf_din(ch),
        WR_A_ADDR_I => buf_adr_a,
        WR_A_EN_I   => WR_EN_I,
        RD_B_DATA_O => buf_dout(ch),
        RD_B_ADDR_I => buf_adr_b,
        CLK_I       => CLK_I
      );
    end generate;

  end generate CH_BUFFERS;
  
end behavioral;
