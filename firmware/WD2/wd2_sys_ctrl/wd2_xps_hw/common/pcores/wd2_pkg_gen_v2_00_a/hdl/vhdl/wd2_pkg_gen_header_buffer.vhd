---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Header Buffer
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.08.2017 14:02:21
--
--  Description : Buffer for the header data of the package generator on the
--                WaveDream2 (WD2) board.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;

entity wd2_pkg_gen_header_buffer is
port
(
  -- Read control interface
  RD_EN_I                     : in  std_logic;
  LOAD_I                      : in  std_logic;
  START_OF_HEADER_O           : out std_logic;
  END_OF_HEADER_O             : out std_logic;
  SET_BUFFER_SENT_I           : in  std_logic;

  -- Write control interface
  HEADER_CLR_I                : in  std_logic;

  -- Internal header data
  DATA_TYPE_I                 : in  std_logic_vector( 7 downto 0);
  CH_SEL_I                    : in  std_logic_vector( 4 downto 0);
  DATA_CHUNK_OFFSET_I            : in  std_logic_vector(15 downto 0);
  PAYLOAD_LENGTH_I            : in  std_logic_vector(15 downto 0);
  START_OF_TYPE_I             : in  std_logic;
  END_OF_TYPE_I               : in  std_logic;
  START_OF_EVENT_I            : in  std_logic;
  END_OF_EVENT_I              : in  std_logic;

  -- External header data (static)
  PROTOCOL_VERSION_O          : out std_logic_vector( 7 downto 0);
  BOARD_TYPE_I                : in  std_logic_vector( 3 downto 0);
  BOARD_REVISION_I            : in  std_logic_vector( 3 downto 0);
  CRATE_ID_I                  : in  std_logic_vector( 7 downto 0);
  SLOT_ID_I                   : in  std_logic_vector( 7 downto 0);
  SERIAL_NUMBER_I             : in  std_logic_vector(15 downto 0);
  CH_TX_EN_I                  : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0);
  ZERO_SUPPRESSION_MASK_I     : in  std_logic_vector(15 downto 0);
  -- External header data (dynamic)
  ZERO_SUPPRESSION_EN_I       : in  std_logic;
  ZERO_SUPPR_INHIBIT_STORE_I  : in  std_logic;
  ZERO_SUPPRESSION_INHIBIT_I  : in  std_logic;
  ZERO_SUPPRESSION_INHIBIT_O  : out std_logic;
  BOARD_VARIANT_I             : in  std_logic_vector( 1 downto 0);
  TRIGGER_INFO_PERR_I         : in  std_logic;                     -- stored on TRIGGER_INFO_NEW_I
  TRIGGER_INFO_NEW_I          : in  std_logic;                     -- cleared at beginning and stored on TRIGGER_INFO_NEW_I
  DAQ_PLL_LOCK_I              : in  std_logic;
  DRS_PLL_LOCK_I              : in  std_logic_vector( 1 downto 0); -- stored on trigger, selected by CH_SEL_I
  DRS_DATA_VALID_I            : in  std_logic;
  TRIGGER_SOURCE_I            : in  std_logic_vector( 3 downto 0);
  CH_EVENT_SAMPLES_DRS_I      : in  std_logic_vector(10 downto 0);
  CH_EVENT_SAMPLES_ADC_I      : in  std_logic_vector(11 downto 0);
  CH_EVENT_SAMPLES_TDC_I      : in  std_logic_vector(12 downto 0);
  CH_EVENT_SAMPLES_TRG_I      : in  std_logic_vector( 9 downto 0);
  TRIGGER_INFO_I              : in  std_logic_vector(47 downto 0); -- stored on TRIGGER_INFO_NEW_I
  EVENT_NUMBER_I              : in  std_logic_vector(31 downto 0); -- stored on trigger
  TIMESTAMP_I                 : in  std_logic_vector(63 downto 0);
  SAMPLING_FREQ_DRS_I         : in  std_logic_vector(31 downto 0); -- stored on trigger, selected by DATA_TYPE_I
  SAMPLING_FREQ_ADC_I         : in  std_logic_vector(31 downto 0); -- stored on trigger, selected by DATA_TYPE_I (adapted with ADC_SAMPLING_DIV_I)
  SAMPLING_FREQ_TDC_I         : in  std_logic_vector(31 downto 0); -- stored on trigger, selected by DATA_TYPE_I (adapted with ADC_SAMPLING_DIV_I)
  SAMPLING_FREQ_TRG_I         : in  std_logic_vector(31 downto 0); -- stored on trigger, selected by DATA_TYPE_I (adapted with ADC_SAMPLING_DIV_I)
  DRS_TRIGGER_CELL_WE_I       : in  std_logic;
  DRS_TRIGGER_CELL_I          : in  std_logic_vector(19 downto 0); -- stored on DRS_TRIGGER_CELL_WE_I, selected by CH_SEL_I
  TEMPERATURE_I               : in  std_logic_vector(15 downto 0); -- stored on trigger
  DAC_OFS_I                   : in  std_logic_vector(15 downto 0);
  DAC_ROFS_I                  : in  std_logic_vector(15 downto 0);
  DAC_PZC_I                   : in  std_logic_vector(15 downto 0);
  FRONTEND_SETTINGS_I         : in  std_logic_vector(CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC*9-1 downto 0);

  DATA_O                      : out std_logic_vector(15 downto 0);
  RD_BUF_SEL_I                : in  std_logic_vector( 1 downto 0);
  WR_BUF_SEL_I                : in  std_logic_vector( 1 downto 0);
  CAPTURE_DATA_I              : in  std_logic;
  CLK_I                       : in  std_logic;
  RST_I                       : in  std_logic
);
end wd2_pkg_gen_header_buffer;

architecture behavioral of wd2_pkg_gen_header_buffer is

  constant C_HEADER_ADR_WIDTH  : integer := log2ceil((CPK_WD2_HEADER_LEN)/2);

  type frontend_settings_array_type is array (CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC-1 downto 0) of std_logic_vector(8 downto 0);

  type dyn_head_vec_2_type  is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector( 1 downto 0);
  type dyn_head_vec_4_type  is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector( 3 downto 0);
  type dyn_head_vec_10_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector( 9 downto 0);
  type dyn_head_vec_11_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(10 downto 0);
  type dyn_head_vec_12_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(11 downto 0);
  type dyn_head_vec_13_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(12 downto 0);
  type dyn_head_vec_16_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(15 downto 0);
  type dyn_head_vec_20_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(19 downto 0);
  type dyn_head_vec_32_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(31 downto 0);
  type dyn_head_vec_48_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(47 downto 0);
  type dyn_head_vec_64_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of std_logic_vector(63 downto 0);
  type dyn_head_vec_fe_type is array (CPK_NR_OF_BUFFERS-1 downto 0) of frontend_settings_array_type;

  signal zero_suppression_inhibit : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0'); -- stored on TRIGGER_INFO_NEW_I, cleared with SET_BUFFER_SENT_I
  signal trigger_info_perr        : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0'); -- stored on TRIGGER_INFO_NEW_I
  signal trigger_info_new         : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0'); -- cleared at beginning and stored on TRIGGER_INFO_NEW_I
  signal daq_pll_lock             : std_logic_vector(CPK_NR_OF_BUFFERS-1 downto 0) := (others=>'0'); -- stored on trigger
  signal drs_pll_lock             : dyn_head_vec_2_type  := (others=>(others=>'0')); -- stored on trigger, selected by CH_SEL_I
  signal trigger_source           : dyn_head_vec_4_type  := (others=>(others=>'0')); -- stored on trigger
  signal ch_event_samples_drs     : dyn_head_vec_11_type := (others=>(others=>'0')); -- stored on trigger, selected by DATA_TYPE_I
  signal ch_event_samples_adc     : dyn_head_vec_12_type := (others=>(others=>'0')); -- stored on trigger, selected by DATA_TYPE_I
  signal ch_event_samples_tdc     : dyn_head_vec_13_type := (others=>(others=>'0')); -- stored on trigger, selected by DATA_TYPE_I
  signal ch_event_samples_trg     : dyn_head_vec_10_type := (others=>(others=>'0')); -- stored on trigger, selected by DATA_TYPE_I
  signal trigger_info             : dyn_head_vec_48_type := (others=>(others=>'0')); -- stored on TRIGGER_INFO_NEW_I
  signal event_number             : dyn_head_vec_32_type := (others=>(others=>'0')); -- stored on trigger
  signal timestamp                : dyn_head_vec_64_type := (others=>(others=>'0')); -- stored on trigger
  signal sampling_freq_drs        : dyn_head_vec_32_type := (others=>(others=>'0')); -- stored on trigger, selected by DATA_TYPE_I
  signal sampling_freq_adc        : dyn_head_vec_32_type := (others=>(others=>'0')); -- stored on trigger, selected by DATA_TYPE_I (adapted with ADC_SAMPLING_DIV_I)
  signal drs_trigger_cell         : dyn_head_vec_20_type := (others=>(others=>'0')); -- stored on DRS_TRIGGER_CELL_WE_I, selected by adc_sel(CH_SEL_I)
  signal temperature              : dyn_head_vec_16_type := (others=>(others=>'0')); -- stored on trigger
  signal dac_ofs                  : dyn_head_vec_16_type := (others=>(others=>'0')); -- stored on trigger
  signal dac_rofs                 : dyn_head_vec_16_type := (others=>(others=>'0')); -- stored on trigger
  signal dac_pzc                  : dyn_head_vec_16_type := (others=>(others=>'0')); -- stored on trigger
  signal frontend_settings        : dyn_head_vec_fe_type := (others=>(others=>(others=>'0'))); -- stored on trigger selected by CH_SEL_I

  signal header_adr               : std_logic_vector(C_HEADER_ADR_WIDTH-1 downto 0) := (others=>'0');

  type header_word_array_type    is array (CPK_WD2_HEADER_LEN/2 downto 0) of std_logic_vector(15 downto 0);
  signal header                   : header_word_array_type := (others=>(others=>'0'));
  signal start_of_header          : std_logic := '0';
  signal end_of_header            : std_logic := '0';
  signal end_of_header_s          : std_logic := '0';

  signal packet_number            : std_logic_vector(15 downto 0) := (others=>'0');
  signal frontend_setting_ch      : std_logic_vector( 8 downto 0) := (others=>'0');
  signal bits_per_sample          : std_logic_vector( 7 downto 0) := (others=>'0');
  signal sampling_freq_drs_in     : std_logic_vector(31 downto 0) := (others=>'0');
  signal sampling_freq            : std_logic_vector(31 downto 0) := (others=>'0');
  signal ch_event_samples         : std_logic_vector(15 downto 0) := (others=>'0');
  signal tx_enable                : std_logic_vector(31 downto 0) := (others=>'0');
  signal zero_suppression_mask    : std_logic_vector(15 downto 0) := (others=>'0');
  signal adc_sel                  : std_logic := '0';

  signal board_variant            : std_logic_vector(1 downto 0);

  signal protocol_version         : std_logic_vector(7 downto 0);

begin

  adc_sel <= '0' when ( (CH_SEL_I < "01000") or (CH_SEL_I = "10000") ) else '1';

  sampling_freq_drs_in <= SAMPLING_FREQ_DRS_I when DRS_DATA_VALID_I = '1' else (others=>'0');

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      ZERO_SUPPRESSION_INHIBIT_O <= zero_suppression_inhibit(CONV_INTEGER(RD_BUF_SEL_I));
      board_variant              <= BOARD_VARIANT_I;
      if HEADER_CLR_I = '1' then
        trigger_info_new(CONV_INTEGER(WR_BUF_SEL_I))     <= '0';
      elsif TRIGGER_INFO_NEW_I = '1' then
        trigger_info_perr(CONV_INTEGER(WR_BUF_SEL_I))    <= TRIGGER_INFO_PERR_I;
        trigger_info_new(CONV_INTEGER(WR_BUF_SEL_I))     <= TRIGGER_INFO_NEW_I;
        trigger_info(CONV_INTEGER(WR_BUF_SEL_I))         <= TRIGGER_INFO_I;
      end if;
      if ZERO_SUPPR_INHIBIT_STORE_I = '1' then
        zero_suppression_inhibit(CONV_INTEGER(WR_BUF_SEL_I)) <= ZERO_SUPPRESSION_INHIBIT_I;
      end if;
      if SET_BUFFER_SENT_I = '1' then
        zero_suppression_inhibit(CONV_INTEGER(RD_BUF_SEL_I)) <= '0';
      end if;
      if CAPTURE_DATA_I = '1' then
        daq_pll_lock(CONV_INTEGER(WR_BUF_SEL_I))         <= DAQ_PLL_LOCK_I;
        drs_pll_lock(CONV_INTEGER(WR_BUF_SEL_I))         <= DRS_PLL_LOCK_I;
        trigger_source(CONV_INTEGER(WR_BUF_SEL_I))       <= TRIGGER_SOURCE_I;
        ch_event_samples_drs(CONV_INTEGER(WR_BUF_SEL_I)) <= CH_EVENT_SAMPLES_DRS_I;
        ch_event_samples_adc(CONV_INTEGER(WR_BUF_SEL_I)) <= CH_EVENT_SAMPLES_ADC_I;
        ch_event_samples_tdc(CONV_INTEGER(WR_BUF_SEL_I)) <= CH_EVENT_SAMPLES_TDC_I;
        ch_event_samples_trg(CONV_INTEGER(WR_BUF_SEL_I)) <= CH_EVENT_SAMPLES_TRG_I;
        event_number(CONV_INTEGER(WR_BUF_SEL_I))         <= EVENT_NUMBER_I;
        timestamp(CONV_INTEGER(WR_BUF_SEL_I))            <= TIMESTAMP_I;
        sampling_freq_drs(CONV_INTEGER(WR_BUF_SEL_I))    <= sampling_freq_drs_in;
        sampling_freq_adc(CONV_INTEGER(WR_BUF_SEL_I))    <= SAMPLING_FREQ_ADC_I;
        temperature(CONV_INTEGER(WR_BUF_SEL_I))          <= TEMPERATURE_I;
        dac_ofs(CONV_INTEGER(WR_BUF_SEL_I))              <= DAC_OFS_I;
        dac_rofs(CONV_INTEGER(WR_BUF_SEL_I))             <= DAC_ROFS_I;
        dac_pzc(CONV_INTEGER(WR_BUF_SEL_I))              <= DAC_PZC_I;
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(0) <= FRONTEND_SETTINGS_I(  8 downto   0);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(1) <= FRONTEND_SETTINGS_I( 17 downto   9);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(2) <= FRONTEND_SETTINGS_I( 26 downto  18);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(3) <= FRONTEND_SETTINGS_I( 35 downto  27);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(4) <= FRONTEND_SETTINGS_I( 44 downto  36);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(5) <= FRONTEND_SETTINGS_I( 53 downto  45);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(6) <= FRONTEND_SETTINGS_I( 62 downto  54);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(7) <= FRONTEND_SETTINGS_I( 71 downto  63);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(8) <= FRONTEND_SETTINGS_I( 80 downto  72);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(9) <= FRONTEND_SETTINGS_I( 89 downto  81);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(10)<= FRONTEND_SETTINGS_I( 98 downto  90);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(11)<= FRONTEND_SETTINGS_I(107 downto  99);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(12)<= FRONTEND_SETTINGS_I(116 downto 108);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(13)<= FRONTEND_SETTINGS_I(125 downto 117);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(14)<= FRONTEND_SETTINGS_I(134 downto 126);
        frontend_settings(CONV_INTEGER(WR_BUF_SEL_I))(15)<= FRONTEND_SETTINGS_I(143 downto 135);
      end if;
      if DRS_TRIGGER_CELL_WE_I = '1' then
        drs_trigger_cell(CONV_INTEGER(WR_BUF_SEL_I))     <= DRS_TRIGGER_CELL_I;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      tx_enable <= (others=>'0');
      tx_enable(CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS-1 downto 0) <= CH_TX_EN_I;
      zero_suppression_mask <= ZERO_SUPPRESSION_MASK_I;
      frontend_setting_ch <= frontend_settings(CONV_INTEGER(RD_BUF_SEL_I))(CONV_INTEGER(CH_SEL_I(3 downto 0)));
      case DATA_TYPE_I is
      when CPK_PKG_TYPE_DRS =>
        bits_per_sample  <= CONV_STD_LOGIC_VECTOR(CPK_DRS_ADC_BITS_PER_SAMPLE, bits_per_sample'length);
        ch_event_samples <= "00000" & ch_event_samples_drs(CONV_INTEGER(RD_BUF_SEL_I));
        sampling_freq    <= sampling_freq_drs(CONV_INTEGER(RD_BUF_SEL_I));
      when CPK_PKG_TYPE_ADC =>
        bits_per_sample  <= CONV_STD_LOGIC_VECTOR(CPK_DRS_ADC_BITS_PER_SAMPLE, bits_per_sample'length);
        ch_event_samples <= "0000" & ch_event_samples_adc(CONV_INTEGER(RD_BUF_SEL_I));
        sampling_freq    <= sampling_freq_adc(CONV_INTEGER(RD_BUF_SEL_I));
      when CPK_PKG_TYPE_TDC =>
        bits_per_sample  <= CONV_STD_LOGIC_VECTOR(CPK_TDC_BITS_PER_SAMPLE, bits_per_sample'length);
        ch_event_samples <= "000" & ch_event_samples_tdc(CONV_INTEGER(RD_BUF_SEL_I));
        sampling_freq    <= SAMPLING_FREQ_TDC_I;
      when CPK_PKG_TYPE_TRG =>
        bits_per_sample  <= CONV_STD_LOGIC_VECTOR(CPK_TRG_BITS_PER_SAMPLE, bits_per_sample'length);
        ch_event_samples <= "000000" & ch_event_samples_trg(CONV_INTEGER(RD_BUF_SEL_I));
        sampling_freq    <= SAMPLING_FREQ_TRG_I;
      when CPK_PKG_TYPE_SCL =>
        bits_per_sample  <= CONV_STD_LOGIC_VECTOR(CPK_SCL_BITS_PER_SCALER, bits_per_sample'length);
        ch_event_samples <= CONV_STD_LOGIC_VECTOR(1, ch_event_samples'length);
        sampling_freq    <= (others=>'0');
      when others =>
        bits_per_sample  <= (others=>'0');
        ch_event_samples <= (others=>'0');
        sampling_freq    <= (others=>'0');
      end case;
    end if;
  end process;

  ----------------------------------------------------------
  -- !!!!! Update protocol version when header changes !!!!!
  ----------------------------------------------------------
  protocol_version   <= CONV_STD_LOGIC_VECTOR(7, protocol_version'length);
  PROTOCOL_VERSION_O <= protocol_version;
  ----------------------------------------------------------
  -- !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! --
  ----------------------------------------------------------

  -- Header content mapping
  -- WDAQ header
  header(0)  <= protocol_version & BOARD_TYPE_I & BOARD_REVISION_I;
  header(1)  <= x"C0DE"; -- reserved
  header(2)  <= SERIAL_NUMBER_I;
  header(3)  <= CRATE_ID_I & SLOT_ID_I;
  header(4)  <= packet_number;
  header(5)  <= DATA_TYPE_I &
                "0000" &
                START_OF_TYPE_I &
                END_OF_TYPE_I &
                START_OF_EVENT_I &
                END_OF_EVENT_I;
  header(6)  <= PAYLOAD_LENGTH_I;
  header(7)  <= DATA_CHUNK_OFFSET_I;
  -- WDB header
  header(8)  <= adc_sel &
                "00" &
                CH_SEL_I &
                board_variant &
                zero_suppression_inhibit(CONV_INTEGER(RD_BUF_SEL_I)) &
                ZERO_SUPPRESSION_EN_I &
                trigger_info_perr(CONV_INTEGER(RD_BUF_SEL_I)) &
                trigger_info_new(CONV_INTEGER(RD_BUF_SEL_I))  &
                daq_pll_lock(CONV_INTEGER(RD_BUF_SEL_I)) &
                drs_pll_lock(CONV_INTEGER(RD_BUF_SEL_I))(CONV_INTEGER(adc_sel));
  header(9)  <= ch_event_samples;
  header(10) <= sampling_freq(31 downto 16);
  header(11) <= sampling_freq(15 downto  0);
  header(12) <= bits_per_sample & "0000" & trigger_source(CONV_INTEGER(RD_BUF_SEL_I));
  header(13) <= zero_suppression_mask;
  header(14) <= tx_enable(31 downto 16);
  header(15) <= tx_enable(15 downto  0);
  header(16) <= "000000" & drs_trigger_cell(CONV_INTEGER(RD_BUF_SEL_I))(19 downto 10) when adc_sel = '1' else
                "000000" & drs_trigger_cell(CONV_INTEGER(RD_BUF_SEL_I))( 9 downto  0);
  header(17) <= trigger_info(CONV_INTEGER(RD_BUF_SEL_I))(47 downto 32);
  header(18) <= trigger_info(CONV_INTEGER(RD_BUF_SEL_I))(31 downto 16);
  header(19) <= trigger_info(CONV_INTEGER(RD_BUF_SEL_I))(15 downto  0);
  header(20) <= timestamp(CONV_INTEGER(RD_BUF_SEL_I))(63 downto 48);
  header(21) <= timestamp(CONV_INTEGER(RD_BUF_SEL_I))(47 downto 32);
  header(22) <= timestamp(CONV_INTEGER(RD_BUF_SEL_I))(31 downto 16);
  header(23) <= timestamp(CONV_INTEGER(RD_BUF_SEL_I))(15 downto  0);
  header(24) <= X"2211";--(others=>'0'); -- reserved
  header(25) <= dac_pzc(CONV_INTEGER(RD_BUF_SEL_I));
  header(26) <= event_number(CONV_INTEGER(RD_BUF_SEL_I))(31 downto 16);
  header(27) <= event_number(CONV_INTEGER(RD_BUF_SEL_I))(15 downto  0);
  header(28) <= temperature(CONV_INTEGER(RD_BUF_SEL_I));
  header(29) <= dac_ofs(CONV_INTEGER(RD_BUF_SEL_I));
  header(30) <= dac_rofs(CONV_INTEGER(RD_BUF_SEL_I));
  header(31) <= "0000000" & frontend_setting_ch;

  -- output registers
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if LOAD_I = '1' or RST_I = '1' then
        -- load packet length
        -- address
        header_adr <= (others=>'0');
        -- data
        DATA_O <= PAYLOAD_LENGTH_I + CONV_STD_LOGIC_VECTOR(CPK_WD2_HEADER_LEN, DATA_O'length);
        -- flags
        start_of_header <= '1';
        end_of_header   <= '0';
      elsif RD_EN_I = '1' then
        -- address
        if header_adr < CONV_STD_LOGIC_VECTOR(CPK_WD2_HEADER_LEN/2-1, header_adr'length) then
          header_adr <= header_adr + 1;
        end if;
        -- data
        DATA_O <= header(CONV_INTEGER(header_adr));
        -- flags
        start_of_header <= '0';
        if header_adr = CONV_STD_LOGIC_VECTOR(CPK_WD2_HEADER_LEN/2-1, header_adr'length) then
          end_of_header <= '1';
        else
          end_of_header <= '0';
        end if;
      end if;
    end if;
  end process;

  START_OF_HEADER_O <= start_of_header;
  END_OF_HEADER_O   <= end_of_header;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      end_of_header_s <= end_of_header;
      if RST_I = '1' then
        packet_number <= (others=>'0');
      elsif end_of_header = '1' and end_of_header_s = '0' then
        packet_number <= packet_number + 1;
      end if;
    end if;
  end process;


end behavioral;
