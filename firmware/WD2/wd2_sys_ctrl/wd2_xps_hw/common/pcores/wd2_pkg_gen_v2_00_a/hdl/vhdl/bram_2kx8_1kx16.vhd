---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 BRAM instance 2kx8 1kx16
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202E)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  07.11.2017 15:51:19
--
--  Description : Block RAM instance for event buffer in WaveDream2.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity bram_2kx8_1kx16 is
port
(
  -- Write
  WR_A_DATA_I : in  std_logic_vector( 7 downto 0);
  WR_A_ADDR_I : in  std_logic_vector(10 downto 0);
  WR_A_EN_I   : in  std_logic;

  -- Read
  RD_B_DATA_O : out std_logic_vector(15 downto 0);
  RD_B_ADDR_I : in  std_logic_vector( 9 downto 0);
  
  CLK_I       : in  std_logic
);
end bram_2kx8_1kx16;

architecture behavioral of bram_2kx8_1kx16 is
  
  constant C_DWIDTH_IN  : integer :=  8;
  constant C_DWIDTH_OUT : integer := 16;

  signal rd_b_data      : std_logic_vector(15 downto 0) := (others => '0');

begin

  -- shuffling
  -- WR_A_DATA_I 0: A[7:0]
  --             1: B[7:0]
  --                 15:8      7:0
  -- rd_b_data   0: B[7:0] & A[7:0]
  -- RD_B_DATA_O 0: A[7:0] & B[7:0]
  RD_B_DATA_O <= rd_b_data(7 downto 0) & rd_b_data(15 downto 8);

  -- buffers
  BRAM_2kx8_1kx16 : entity psi_3205_v1_00_a.ram_dp
  generic map
  (
    CGN_DATA_WIDTH_A  => C_DWIDTH_IN,
    CGN_ADDR_WIDTH_A  => 11,
    CGN_READ_PORT_A   => false,
    CGN_WRITE_PORT_A  => true,
    CGN_READ_FIRST_A  => false,
    CGN_USE_OUTREG_A  => false,
    CGN_DATA_WIDTH_B  => C_DWIDTH_OUT,
    CGN_ADDR_WIDTH_B  => 10,
    CGN_READ_PORT_B   => true,
    CGN_WRITE_PORT_B  => false,
    CGN_READ_FIRST_B  => true,
    CGN_USE_OUTREG_B  => false,
    CGN_RAM_STYLE     => "block"
  )
  port map
  (
    PA_CLK_I   => CLK_I,
    PA_EN_I    => '1',
    PA_ADDR_I  => WR_A_ADDR_I,
    PA_DATA_I  => WR_A_DATA_I,
    PA_WR_EN_I => WR_A_EN_I,
    PA_DATA_O  => open,
    PB_CLK_I   => CLK_I,
    PB_EN_I    => '1',
    PB_ADDR_I  => RD_B_ADDR_I,
    PB_DATA_I  => (others=>'0'),
    PB_WR_EN_I => '0',
    PB_DATA_O  => rd_b_data
  );
  
end behavioral;
