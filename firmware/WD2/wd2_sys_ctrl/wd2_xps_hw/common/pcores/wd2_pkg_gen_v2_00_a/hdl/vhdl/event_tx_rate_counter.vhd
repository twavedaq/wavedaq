---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Event Transmission Rate Counter
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  11.01.2018 12:19:22
--
--  Description :  Counts the number of event data sets transmitted per second.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

-- Add timing constraints to ignore cross clock domain paths
-- between microblaze and daq:
-- TIMESPEC TS_UC_TO_DAQ = FROM "TG_UC" TO "TG_DAQ" TIG;
-- TIMESPEC TS_DAQ_TO_UC = FROM "TG_DAQ" TO "TG_UC" TIG;


entity event_tx_rate_counter is
  generic (
    CGN_TICK_TIME_NS  : real :=  1.0e9; -- ns
    CGN_CLK_PERIOD_NS : real := 12.5    -- ns
  );
  port (
    EVENT_TX_RATE_O : out std_logic_vector(31 downto 0);
    EVENT_TX_DONE_I : in  std_logic;
    RST_I           : in  std_logic;
    CLK_I           : in  std_logic
  );
end event_tx_rate_counter;

architecture behavioral of event_tx_rate_counter is
  
  constant C_TIMER_LOAD     : integer := integer(CGN_TICK_TIME_NS/CGN_CLK_PERIOD_NS) - 1;
  constant C_TIMER_SIZE     : integer := log2ceil(C_TIMER_LOAD);
  constant C_TIMER_LOAD_SLV : std_logic_vector(C_TIMER_SIZE-1 downto 0) := CONV_STD_LOGIC_VECTOR(C_TIMER_LOAD, C_TIMER_SIZE);

  signal tx_rate_count      : std_logic_vector(31 downto 0) := (others=>'0');
  signal timer_count_msbs   : std_logic_vector(C_TIMER_SIZE-1 downto 2) := (others=>'0');
  signal timer_count_lsbs   : std_logic_vector(1 downto 0)  := (others=>'0');
  signal timer_count        : std_logic_vector(C_TIMER_SIZE-1 downto 0) := (others=>'0');
  
begin
  
  event_counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' or timer_count = 0 then
        EVENT_TX_RATE_O <= tx_rate_count;
        tx_rate_count   <= (others=>'0');
      elsif EVENT_TX_DONE_I = '1' then
        tx_rate_count   <= tx_rate_count + 1;
      end if;
    end if;
  end process;
  
  timer_count <= timer_count_msbs & timer_count_lsbs;
  
  timer : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' or timer_count = 0 then
        timer_count_msbs <= C_TIMER_LOAD_SLV(C_TIMER_SIZE-1 downto 2);
        timer_count_lsbs <= C_TIMER_LOAD_SLV(1 downto 0);
      else
        timer_count_lsbs <= timer_count_lsbs - 1;
        if timer_count_lsbs = "00" then
          timer_count_msbs <= timer_count_msbs - 1;
        end if;
      end if;
    end if;
  end process;

end architecture behavioral;