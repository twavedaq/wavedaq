---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Settings Package
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  16.08.2017 11:03:37
--
--  Description : Package generator for the WaveDream2 (WD2) board. Prepares the
--                packages over ethernet or serdes. The unit generates the WD2
--                specific header and stores a certain set of event data to
--                allow for momentary higher event rates and retransmission.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

package wd2_pkg_gen_cfg_package is

  -- Buffers
  constant CPK_NR_OF_BUFFERS           : integer :=     4;
  constant CPK_DRS_SAMPLES_PER_EVENT   : integer :=  1024;
  constant CPK_ADC_SAMPLES_PER_EVENT   : integer :=  2048;
  constant CPK_TDC_SAMPLES_PER_EVENT   : integer := 8*512;
  constant CPK_TRG_SAMPLES_PER_EVENT   : integer :=   512;
  constant CPK_MAX_SAMPLES_PER_EVENT   : integer := CPK_ADC_SAMPLES_PER_EVENT;
  constant CPK_MAX_TX_BYTES            : integer := CPK_TRG_SAMPLES_PER_EVENT*8;
  -- DRS & ADC
  constant CPK_NR_OF_ADCS              : integer :=  2;
  constant CPK_CHNLS_PER_ADC           : integer :=  8;
  constant CPK_CHNLS_PER_DRS           : integer :=  9;
  constant CPK_DRS_ADC_BITS_PER_SAMPLE : integer := 12;
  constant CPK_DRS_ADC_CH_WR_DWIDTH    : integer := CPK_DRS_ADC_BITS_PER_SAMPLE;
  constant CPK_DRS_ADC_RD_DWIDTH       : integer := 24;
  -- TDC
  constant CPK_NR_OF_TDC_CHNLS         : integer := 16;
  constant CPK_TDC_BITS_PER_SAMPLE     : integer :=  1;
  constant CPK_TDC_WR_DWIDTH           : integer :=  8;
  constant CPK_TDC_RD_DWIDTH           : integer := 16;
  -- Advanced Trigger Output
  constant CPK_TRG_BITS_PER_SAMPLE     : integer := 64;
  constant CPK_TRG_WR_DWIDTH           : integer := CPK_TRG_BITS_PER_SAMPLE;
  constant CPK_TRG_RD_DWIDTH           : integer := 16;
  -- Scaler
  constant CPK_NR_OF_SCALERS           : integer := 19;
  constant CPK_SCL_BITS_PER_SCALER     : integer := 64;
  constant CPK_SCL_WR_DWIDTH           : integer := CPK_SCL_BITS_PER_SCALER;
  constant CPK_SCL_RD_DWIDTH           : integer := 16;

  constant CPK_MAX_NR_OF_ADC_CHNLS     : integer := CPK_NR_OF_ADCS*CPK_CHNLS_PER_ADC;
  constant CPK_MAX_NR_OF_DRS_CHNLS     : integer := CPK_NR_OF_ADCS*CPK_CHNLS_PER_DRS;
  constant CPK_MAX_NR_OF_CHNLS         : integer := CPK_MAX_NR_OF_DRS_CHNLS;

  -- Dataset encoding
  constant CPK_NR_OF_PKG_TYPES         : integer := 5; -- only types handled in firmware
  constant CPK_PKG_TYPE_DRS            : std_logic_vector(7 downto 0) := X"00"; -- DRS data
  constant CPK_PKG_TYPE_ADC            : std_logic_vector(7 downto 0) := X"01"; -- ADC data
  constant CPK_PKG_TYPE_TDC            : std_logic_vector(7 downto 0) := X"02"; -- TDC data
  constant CPK_PKG_TYPE_TRG            : std_logic_vector(7 downto 0) := X"03"; -- Advanced Trigger output data
  constant CPK_PKG_TYPE_SCL            : std_logic_vector(7 downto 0) := X"04"; -- Scaler Data
  constant CPK_PKG_TYPE_DMY            : std_logic_vector(7 downto 0) := X"05"; -- Dummy Data (Header Only)
  constant CPK_PKG_TYPE_UBL            : std_logic_vector(7 downto 0) := X"0F"; -- Microblaze generated package
  constant CPK_LAST_DATA_TYPE          : std_logic_vector(7 downto 0) := X"0F";

  -- Packet specs
  constant CPK_MAX_ETH_PAYLOAD_LEN     : integer := 1500;
  constant CPK_IP_HEADER_LEN           : integer :=   20;
  constant CPK_UDP_HEADER_LEN          : integer :=    8;
  constant CPK_WD2_HEADER_LEN          : integer :=   64;
  constant CPK_MAX_WD2_PAYLOAD_LEN     : integer := CPK_MAX_ETH_PAYLOAD_LEN-CPK_IP_HEADER_LEN-CPK_UDP_HEADER_LEN-CPK_WD2_HEADER_LEN;

end wd2_pkg_gen_cfg_package;

--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body wd2_pkg_gen_cfg_package is
end package body;
