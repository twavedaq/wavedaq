---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  WaveDream2 Package Generator Read Control
--
--  Project :  WaveDream2
--
--  PCB  :  DRS4 WaveDream Board II (121202C)
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  11.08.2017 11:08:34
--
--  Description : Buffer read controller for the package generator of the
--                WaveDream2 (WD2) board.
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library wd2_pkg_gen_v2_00_a;
use wd2_pkg_gen_v2_00_a.wd2_pkg_gen_cfg_package.all;

entity wd2_pkg_gen_read_control is
port
(
  -- Header buffer control interface
  HEADER_REN_O          : out std_logic;
  HEADER_LOAD_O         : out std_logic;
  START_OF_HEADER_I     : in  std_logic;
  END_OF_HEADER_I       : in  std_logic;

  -- Buffer interface
  BUFFER_SEL_O          : out std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0);
  BUFFER_READY_I        : in  std_logic;
  BUFFER_SENT_I         : in  std_logic;
  SET_BUFFER_SENT_O     : out std_logic;
  BUFFER_REN_O          : out std_logic;
  BUFFER_READ_INIT_O    : out std_logic;
  STORE_TX_PARAMS_O     : out std_logic;
  DATASET_TX_SAMPLES_I  : in  std_logic_vector(15 downto 0);
  ITEM_TX_EN_I          : in  std_logic;
  LAST_TYPE_I           : in  std_logic;
  LAST_CH_I             : in  std_logic;
  DATA_VALID_I          : in  std_logic;

  -- COM control interface
  SRC_RDY_N_O           : out std_logic;
  DST_RDY_N_I           : in  std_logic;

  -- Header and side data
  PKG_TYPE_O            : out std_logic_vector( 7 downto 0);
  CH_SEL_O              : out std_logic_vector( 4 downto 0);
  DATA_CHUNK_OFFSET_O   : out std_logic_vector(15 downto 0);
  PAYLOAD_LENGTH_O      : out std_logic_vector(15 downto 0);
  START_OF_PACKET_O     : out std_logic;
  START_OF_TYPE_O       : out std_logic;
  START_OF_EVENT_O      : out std_logic;
  END_OF_PACKET_O       : out std_logic;
  END_OF_TYPE_O         : out std_logic;
  END_OF_EVENT_O        : out std_logic;
  TX_COMPLETE_O         : out std_logic;

  -- Read control
  MAX_PAYLOAD_SAMPLES_I : in  std_logic_vector(17 downto 0);
  FIRST_PKG_DLY_I       : in  std_logic_vector(13 downto 0);
  ENABLE_I              : in  std_logic;
  CLK_I                 : in  std_logic;
  RST_I                 : in  std_logic
);
end wd2_pkg_gen_read_control;

architecture behavioral of wd2_pkg_gen_read_control is

  type type_state is (s_idle, s_calc, s_check, s_load, s_set_ready, s_send_header, s_send_data, s_select_next, s_switch_buffer);
  signal state               : type_state;

  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

  signal buff_sel            : std_logic_vector(log2ceil(CPK_NR_OF_BUFFERS)-1 downto 0) := (others=>'0');
  signal type_sel            : std_logic_vector( 7 downto 0) := (others=>'0');
  signal ch_sel              : std_logic_vector( 4 downto 0) := (others=>'0');
  signal payload_offset      : std_logic_vector(15 downto 0) := (others=>'0');

  signal tot_tx_word_count   : std_logic_vector(15 downto 0) := (others=>'0');
  signal pkg_tx_word_count   : std_logic_vector(15 downto 0) := (others=>'0');
  signal max_payload_words   : std_logic_vector(15 downto 0) := (others=>'0');
  signal payload_word_length : std_logic_vector(15 downto 0) := (others=>'0');

  signal first_pkg_dly_cnt   : std_logic_vector(20 downto 0) := (others=>'0');

begin

  BUFFER_SEL_O      <= buff_sel;
  PKG_TYPE_O        <= type_sel;
  CH_SEL_O          <= ch_sel;
  START_OF_PACKET_O <= START_OF_HEADER_I;
  END_OF_PACKET_O   <= '1' when (state = s_send_data and pkg_tx_word_count = 0) else '0';

  HEADER_REN_O <= not(DST_RDY_N_I) when state = s_send_header else '0';

  process(state, DATA_VALID_I, DST_RDY_N_I)
  begin
    if state = s_send_data then
      if DATA_VALID_I = '1' then
        BUFFER_REN_O <= not(DST_RDY_N_I);
      else
        BUFFER_REN_O <= '1';
      end if;
    else
      BUFFER_REN_O <= '0';
    end if;
  end process;

  process(state, DATA_VALID_I)
  begin
    case state is
    when s_send_header =>
      SRC_RDY_N_O <= '0';
    when s_send_data =>
      SRC_RDY_N_O <= not DATA_VALID_I;
    when others =>
      SRC_RDY_N_O <= '1';
    end case;
  end process;

  payload_word_length <= pkg_tx_word_count + 1;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      case type_sel is
      when CPK_PKG_TYPE_DRS | CPK_PKG_TYPE_ADC =>
        -- 2 samples = 1 readout word
        DATA_CHUNK_OFFSET_O <= payload_offset + (payload_offset(payload_offset'length-2 downto 0) & '0');
        PAYLOAD_LENGTH_O    <= payload_word_length + (payload_word_length(payload_word_length'length-2 downto 0) & '0');
      when CPK_PKG_TYPE_TDC =>
        -- 8*2 samples = 1 readout word (1 bit per sample handled bytewise)
        DATA_CHUNK_OFFSET_O <= payload_offset(payload_offset'length-2 downto 0) & '0';
        PAYLOAD_LENGTH_O    <= payload_word_length(payload_word_length'length-2 downto 0) & '0';
      when CPK_PKG_TYPE_TRG | CPK_PKG_TYPE_SCL=>
        -- 4 readout words = 1 sample
        DATA_CHUNK_OFFSET_O <= payload_offset(payload_offset'length-2 downto 0) & '0';
        PAYLOAD_LENGTH_O    <= payload_word_length(payload_word_length'length-2 downto 0) & '0';
      when others =>
        DATA_CHUNK_OFFSET_O <= (others=>'0');
        PAYLOAD_LENGTH_O    <= (others=>'0');
        PAYLOAD_LENGTH_O(1) <= '1';
      end case;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RST_I = '1' then
        buff_sel          <= (others=>'0');
        type_sel          <= (others=>'0');
        ch_sel            <= (others=>'0');
        payload_offset    <= (others=>'0');
        first_pkg_dly_cnt <= (others=>'0');
        STORE_TX_PARAMS_O <= '0';
        START_OF_EVENT_O  <= '0';
        START_OF_TYPE_O   <= '0';
        END_OF_EVENT_O    <= '0';
        END_OF_TYPE_O     <= '0';
        TX_COMPLETE_O     <= '0';
        state <= s_idle;
      else
        -- defaults
        STORE_TX_PARAMS_O  <= '0';
        HEADER_LOAD_O      <= '0';
        BUFFER_READ_INIT_O <= '0';
        SET_BUFFER_SENT_O  <= '0';
        TX_COMPLETE_O      <= '0';

        case state is
        when s_idle =>
          STORE_TX_PARAMS_O <= '1';
          START_OF_EVENT_O  <= '1';
          START_OF_TYPE_O   <= '1';
          END_OF_TYPE_O     <= '0';
          END_OF_EVENT_O    <= '0';
          payload_offset    <= (others=>'0');
          first_pkg_dly_cnt <= FIRST_PKG_DLY_I & "0000000";
          if ENABLE_I = '1' and BUFFER_READY_I = '1' and BUFFER_SENT_I  = '0' then
            -- type select
            -- channel select
            state <= s_calc;
          end if;

        when s_calc =>
          if payload_offset = 0 then -- first packet of channel
            case type_sel is
            when CPK_PKG_TYPE_DRS | CPK_PKG_TYPE_ADC =>
              -- 2 samples = 1 readout word
              tot_tx_word_count <= ('0' & DATASET_TX_SAMPLES_I(DATASET_TX_SAMPLES_I'length-1 downto 1)) - 1;
              max_payload_words <= ('0' & MAX_PAYLOAD_SAMPLES_I(MAX_PAYLOAD_SAMPLES_I'length-3 downto 1)) - 1;
            when CPK_PKG_TYPE_TDC =>
              -- 8*2 samples = 1 readout word
              tot_tx_word_count <= ("0000" & DATASET_TX_SAMPLES_I(DATASET_TX_SAMPLES_I'length-1 downto 4)) - 1;
              max_payload_words <= ("00"   & MAX_PAYLOAD_SAMPLES_I(MAX_PAYLOAD_SAMPLES_I'length-1 downto 4)) - 1;
            when CPK_PKG_TYPE_TRG | CPK_PKG_TYPE_SCL=>
              -- 4 readout words = 1 sample
              tot_tx_word_count <= (DATASET_TX_SAMPLES_I(DATASET_TX_SAMPLES_I'length-3 downto 0) & "00") - 1;
              max_payload_words <= (MAX_PAYLOAD_SAMPLES_I(MAX_PAYLOAD_SAMPLES_I'length-5 downto 0) & "00") - 1;
            when others =>
              tot_tx_word_count <= (others=>'0');
              max_payload_words <= (others=>'0');
            end case;
          end if;
          state <= s_check;

        when s_check =>
          if ITEM_TX_EN_I = '1' then
            if payload_offset = 0 then -- first packet of channel
              BUFFER_READ_INIT_O <= '1';
            end if;
            if tot_tx_word_count > max_payload_words then
              pkg_tx_word_count <= max_payload_words;
            else
              pkg_tx_word_count <= tot_tx_word_count;
              END_OF_TYPE_O     <= LAST_CH_I;
              END_OF_EVENT_O    <= LAST_TYPE_I and LAST_CH_I;
            end if;
            state <= s_load;
          else
            state <= s_select_next;
          end if;

        when s_load =>
          HEADER_LOAD_O <= '1';
          state <= s_set_ready;

        when s_set_ready =>
        if first_pkg_dly_cnt = 0 then
          state <= s_send_header;
        else
          first_pkg_dly_cnt <= first_pkg_dly_cnt - 1;
        end if;

        when s_send_header =>
          -- state tranistion
          if END_OF_HEADER_I = '1' and DST_RDY_N_I = '0' then
            state <= s_send_data;
          end if;

        when s_send_data =>
          START_OF_EVENT_O <= '0';
          START_OF_TYPE_O  <= '0';
          END_OF_TYPE_O    <= '0';
          END_OF_EVENT_O   <= '0';
          if DST_RDY_N_I = '0' and DATA_VALID_I = '1' then
            if pkg_tx_word_count = 0 then
              if tot_tx_word_count = 0 then
                state <= s_select_next;
              else
                -- setup for the next data segment
                tot_tx_word_count <= tot_tx_word_count - 1;
                payload_offset    <= payload_offset + 1;
                state <= s_calc;
              end if;
            else
              tot_tx_word_count <= tot_tx_word_count - 1;
              payload_offset    <= payload_offset + 1;
              pkg_tx_word_count <= pkg_tx_word_count - 1;
            end if;
          end if;

        when s_select_next =>
          payload_offset <= (others=>'0');
          if LAST_CH_I = '1' or ch_sel = CONV_STD_LOGIC_VECTOR(CPK_MAX_NR_OF_CHNLS-1, ch_sel'length) then
            -- end of transmission for this data type in this buffer
            ch_sel <= (others=>'0');
            if LAST_TYPE_I = '1' then
              SET_BUFFER_SENT_O <= '1';
              type_sel <= (others=>'0');
              state <= s_switch_buffer;
            else
              START_OF_TYPE_O <= '1';
              type_sel <= type_sel + 1;
              state <= s_calc;
            end if;
          else
            ch_sel <= ch_sel + 1;
            state <= s_calc;
          end if;

        when s_switch_buffer =>
          TX_COMPLETE_O <= '1';
          buff_sel <= buff_sel + 1;
          state <= s_idle;

        when others =>
          state <= s_idle;
        end case;
      end if;
    end if;
  end process;

end behavioral;
