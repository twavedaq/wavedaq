---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Synchronization Register
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  18.11.2015 07:54:45
--
--  Description :  Generic tristate buffer to be insertet in xps designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_tristate_buffer is
  generic (
    CGN_TRISTATE_POLARITY : integer   := 0; -- 1 = highlevel tristate, 0 = lowlevel tristate
    CGN_DATA_WIDTH        : integer   := 8
  );
  port (
    BUFFER_I   : in  std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    BUFFER_O   : out std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    BUFFER_T   : out std_logic;
    INPUT_O    : out std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    OUTPUT_I   : in  std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    TRISTATE_I : in  std_logic
  );
end util_tristate_buffer;

architecture behavioral of util_tristate_buffer is


begin

  standard_tristate : if CGN_TRISTATE_POLARITY = 0 generate
    BUFFER_T <= TRISTATE_I;
  end generate standard_tristate;

  inverted_tristate : if CGN_TRISTATE_POLARITY = 1 generate
    BUFFER_T <= not TRISTATE_I;
  end generate inverted_tristate;

  INPUT_O  <= BUFFER_I;
  BUFFER_O <= OUTPUT_I;

end architecture behavioral;