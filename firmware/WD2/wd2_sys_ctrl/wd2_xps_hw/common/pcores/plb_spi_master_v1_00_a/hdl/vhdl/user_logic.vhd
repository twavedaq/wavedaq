------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Mon Jul 21 12:53:06 2014 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_misc.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------
    CGN_MAX_NUM_BYTES              : integer := 1; -- allowable values: 1,2,3,4
    CGN_NUM_SS_BITS                : integer := 1;
    CGN_SCK_RATIO                  : integer := 4; -- allowable values: 2,4,8,10,12...
    CGN_MULTI_MASTER               : integer := 0;
    CGN_HAS_RX_FIFO                : integer := 1;
    CGN_HAS_TX_FIFO                : integer := 1;
    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer := 32;
    C_NUM_REG                      : integer := 6
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    -- ADD USER PORTS ABOVE THIS LINE ------------------
    MOSI_O                         : out  std_logic;
    MOSI_I                         : in   std_logic;
    MOSI_T                         : out  std_logic;
    MISO_I                         : in   std_logic;
    SCK_O                          : out  std_logic;
    SS_N_O                         : out  std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);
    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  COMPONENT spi_master
  generic (
    CGN_MAX_NUM_BYTES  : integer := 1; -- allowable values: 1,2,3,4
    CGN_NUM_SS_BITS    : integer := 1;
    CGN_SCK_RATIO      : integer := 4; -- allowable values: 2,4,8,10,12...
    CGN_MULTI_MASTER   : integer := 0;
    CGN_HAS_RX_FIFO    : integer := 1;
    CGN_HAS_TX_FIFO    : integer := 1
  );
  port(
    MOSI_O             : out std_logic;
    MISO_I             : in  std_logic;
    SCK_O              : out std_logic;
    SS_N_O             : out std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);
    NUM_BITS_I         : in  std_logic_vector(5 downto 0);
    CPOL_I             : in  std_logic;
    CPHA_I             : in  std_logic;
    LSB_FIRST_I        : in  std_logic;
    MANUAL_SLAVE_SEL_I : in  std_logic;
    SLAVE_SEL_N_I      : in  std_logic_vector(CGN_NUM_SS_BITS-1 downto 0);
    LOOP_I             : in  std_logic;
    RX_FIFO_RESET_I    : in  std_logic;
    RX_FIFO_AEMPTY_O   : out std_logic;
    RX_FIFO_EMPTY_O    : out std_logic;
    RX_FIFO_RE_I       : in  std_logic;
    RX_FIFO_DATA_O     : out std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0);
    TX_FIFO_RESET_I    : in  std_logic;
    TX_FIFO_AFULL_O    : out std_logic;
    TX_FIFO_FULL_O     : out std_logic;
    TX_FIFO_WE_I       : in  std_logic;
    TX_FIFO_DATA_I     : in  std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0);
    RESET_I            : in  std_logic;
    CLK_I              : in  std_logic
  );
  END COMPONENT;

  signal spi_enable           : std_logic := '0';
  signal spi_num_bits         : std_logic_vector(5 downto 0) := (others=>'0');
  signal spi_cpol             : std_logic := '0';
  signal spi_cpha             : std_logic := '0';
  signal spi_lsb_first        : std_logic := '0';
  signal spi_manual_slave_sel : std_logic := '0';
  signal spi_slave_sel_n      : std_logic_vector(CGN_NUM_SS_BITS-1 downto 0) := (others=>'0');
  signal slave_selected_n     : std_logic := '0';
  signal spi_loop             : std_logic := '0';
  signal spi_rx_fifo_reset    : std_logic := '0';
  signal spi_rx_fifo_aempty   : std_logic := '0';
  signal spi_rx_fifo_empty    : std_logic := '0';
  signal spi_rx_fifo_re       : std_logic := '0';
  signal spi_rx_fifo_data     : std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0) := (others=>'0');
  signal spi_tx_fifo_reset    : std_logic := '0';
  signal spi_tx_fifo_afull    : std_logic := '0';
  signal spi_tx_fifo_full     : std_logic := '0';
  signal spi_tx_fifo_we       : std_logic := '0';
  signal spi_tx_fifo_data     : std_logic_vector((CGN_MAX_NUM_BYTES*8)-1 downto 0) := (others=>'0');
  signal spi_soft_reset_trg   : std_logic := '0';
  signal spi_soft_reset       : std_logic := '0';
  signal spi_reset            : std_logic := '0';
  signal spi_clk              : std_logic := '0';

  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  --signal slv_reg3                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  --signal slv_reg4                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg5                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel              : std_logic_vector(0 to 5);
  signal slv_reg_read_sel               : std_logic_vector(0 to 5);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;

begin

  --USER logic implementation added here
  spi_master_inst : spi_master
  GENERIC MAP (
    CGN_MAX_NUM_BYTES => CGN_MAX_NUM_BYTES,
    CGN_NUM_SS_BITS   => CGN_NUM_SS_BITS,
    CGN_SCK_RATIO     => CGN_SCK_RATIO,
    CGN_MULTI_MASTER  => 0, -- tristate implemented in this entity
    CGN_HAS_RX_FIFO   => CGN_HAS_RX_FIFO,
    CGN_HAS_TX_FIFO   => CGN_HAS_TX_FIFO
  )
  PORT MAP (
    MOSI_O             => MOSI_O,
    MISO_I             => MISO_I,
    SCK_O              => SCK_O,
    SS_N_O             => SS_N_O,
    NUM_BITS_I         => spi_num_bits,
    CPOL_I             => spi_cpol,
    CPHA_I             => spi_cpha,
    LSB_FIRST_I        => spi_lsb_first,
    MANUAL_SLAVE_SEL_I => spi_manual_slave_sel,
    SLAVE_SEL_N_I      => spi_slave_sel_n,
    LOOP_I             => spi_loop,
    RX_FIFO_RESET_I    => spi_rx_fifo_reset,
    RX_FIFO_AEMPTY_O   => spi_rx_fifo_aempty,
    RX_FIFO_EMPTY_O    => spi_rx_fifo_empty,
    RX_FIFO_RE_I       => spi_rx_fifo_re,
    RX_FIFO_DATA_O     => spi_rx_fifo_data,
    TX_FIFO_RESET_I    => spi_tx_fifo_reset,
    TX_FIFO_AFULL_O    => spi_tx_fifo_afull,
    TX_FIFO_FULL_O     => spi_tx_fifo_full,
    TX_FIFO_WE_I       => spi_tx_fifo_we,
    TX_FIFO_DATA_I     => spi_tx_fifo_data,
    RESET_I            => spi_reset,
    CLK_I              => spi_clk
  );

  spi_reset        <= Bus2IP_Reset or spi_soft_reset;
  spi_clk          <= Bus2IP_Clk;
  slave_selected_n <= AND_REDUCE(spi_slave_sel_n);
  MOSI_T           <= '0' when CGN_MULTI_MASTER = 0 else slave_selected_n;

  -- Register Mapping --
  -- Control Register
  spi_loop                 <= slv_reg0(C_SLV_DWIDTH-1);
  spi_enable               <= slv_reg0(C_SLV_DWIDTH-2);
  spi_cpol                 <= slv_reg0(C_SLV_DWIDTH-3);
  spi_cpha                 <= slv_reg0(C_SLV_DWIDTH-4);
  spi_tx_fifo_reset        <= slv_reg0(C_SLV_DWIDTH-5);
  spi_rx_fifo_reset        <= slv_reg0(C_SLV_DWIDTH-6);
  spi_manual_slave_sel     <= slv_reg0(C_SLV_DWIDTH-7);
  spi_lsb_first            <= slv_reg0(C_SLV_DWIDTH-8);
  spi_soft_reset           <= slv_reg0(C_SLV_DWIDTH-9);
  -- Number of transfer bits register
  spi_num_bits             <= slv_reg1(C_SLV_DWIDTH-6 to C_SLV_DWIDTH-1);
  -- Status Register
  slv_reg2(C_SLV_DWIDTH-1) <= spi_rx_fifo_empty;
  slv_reg2(C_SLV_DWIDTH-2) <= spi_rx_fifo_aempty;
  slv_reg2(C_SLV_DWIDTH-3) <= spi_tx_fifo_full;
  slv_reg2(C_SLV_DWIDTH-4) <= spi_tx_fifo_afull;
  -- Slave select register
  spi_slave_sel_n          <= slv_reg5(C_SLV_DWIDTH-CGN_NUM_SS_BITS to C_SLV_DWIDTH-1);

  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  -- 
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  -- 
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  -- 
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 5);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 5);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4) or Bus2IP_WrCE(5);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3) or Bus2IP_RdCE(4) or Bus2IP_RdCE(5);

  spi_tx_fifo_data  <= Bus2IP_Data;

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      spi_tx_fifo_we <= '0';
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
        slv_reg1 <= (others => '0');
        --slv_reg2 <= (others => '0');
        --slv_reg3 <= (others => '0');
        spi_tx_fifo_we   <= '0';
        --slv_reg4 <= (others => '0');
        slv_reg5 <= (others => '0');
      else
        case slv_reg_write_sel is
          when "100000" =>
            -- control register
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "010000" =>
            -- number of transfer bits register
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg1(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "001000" =>
            null;
            -- status register (read only)
            --for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
            --  if ( Bus2IP_BE(byte_index) = '1' ) then
            --    slv_reg2(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
            --  end if;
            --end loop;
          when "000100" =>
            -- data transmit register
            spi_tx_fifo_we   <= '1';
            --for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
            --  if ( Bus2IP_BE(byte_index) = '1' ) then
            --    slv_reg3(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
            --  end if;
            --end loop;
          when "000010" =>
            null;
            -- data receive register (read only)
            --for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
            --  if ( Bus2IP_BE(byte_index) = '1' ) then
            --    slv_reg4(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
            --  end if;
            --end loop;
          when "000001" =>
            -- slave select register
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg5(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1, slv_reg2, spi_rx_fifo_data, slv_reg5) is
  begin

    spi_rx_fifo_re  <= '0';
    case slv_reg_read_sel is
      when "100000" => slv_ip2bus_data <= slv_reg0;
      when "010000" => slv_ip2bus_data <= slv_reg1;
      when "001000" => slv_ip2bus_data <= slv_reg2;
      when "000100" => slv_ip2bus_data <= (others => '0'); --slv_ip2bus_data <= slv_reg3;
      when "000010" => slv_ip2bus_data <= spi_rx_fifo_data;
                       spi_rx_fifo_re  <= '1';
      when "000001" => slv_ip2bus_data <= slv_reg5;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

end IMP;
