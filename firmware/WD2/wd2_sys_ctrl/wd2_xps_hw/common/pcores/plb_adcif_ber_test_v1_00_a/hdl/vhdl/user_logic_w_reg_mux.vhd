------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Mon Feb  1 12:22:10 2016 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

--DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here
library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH                   : integer              := 32;
    C_NUM_REG                      : integer              := 34
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    DATA_I                         : in  std_logic_vector((8*12)-1 downto 0); -- 8_ADC_Channels x 12_Data_Bits
    DATA_VALID_I                   : in  std_logic;
    DATA_CLK_I                     : in  std_logic;
    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute MAX_FANOUT : string;
  attribute SIGIS : string;

  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic
  component ber_tester_8ch_top is
    port
    (
      DATA_I           : in  std_logic_vector((8*12)-1 downto 0); -- 8_ADC_Channels x 12_Data_Bits
      DATA_VALID_I     : in  std_logic;
      ENABLE_TEST_I    : in  std_logic;
      PN_SELECT_I      : in  std_logic; -- 0 = PN9, 1 = PN23
      INJECT_ERROR_I   : in  std_logic;
      RUNNING_O        : out std_logic;
      REG_SEL_I        : in  std_logic_vector(31 downto 0);
      RD_ACK_O         : out std_logic;
      COUNTER_REG_O    : out std_logic_vector(31 downto 0);
      RESET_TEST_I     : in  std_logic;
      DATA_CLK_I       : in  std_logic
    );
  end component;

  -- control
  signal enable_test            : std_logic := '0';
  signal reset_test             : std_logic := '0';
  signal inject_error           : std_logic := '0';
  signal pn_select              : std_logic := '0';
  -- status
  signal running                : std_logic := '0';
  -- others
  signal counter_reg            : std_logic_vector(31 downto 0);
  signal slv_reg_counter        : std_logic_vector(31 downto 0);
  signal bert_read_sel          : std_logic_vector(0 to 31);
  signal bert_read_ack          : std_logic := '0';
  signal counter_read_ack       : std_logic := '0';


  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1                       : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel              : std_logic_vector(0 to 33);
  signal slv_reg_read_sel               : std_logic_vector(0 to 33);
  signal slv_ip2bus_data                : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack                   : std_logic;
  signal slv_write_ack                  : std_logic;

begin

  --USER logic implementation added here
  
  -- control register bits
  cdc_sync_uc_to_adcif: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  36,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  2,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => DATA_CLK_I,

       PORT_A_I(0)           => slv_reg0(C_SLV_DWIDTH-1),
       PORT_A_I(1)           => slv_reg0(C_SLV_DWIDTH-2),
       PORT_A_I(2)           => slv_reg0(C_SLV_DWIDTH-3),
       PORT_A_I(3)           => slv_reg0(C_SLV_DWIDTH-4),
       PORT_A_I(35 downto 4) => slv_reg_read_sel(2 to 33),
       PORT_B_O(0)           => enable_test,
       PORT_B_O(1)           => reset_test,
       PORT_B_O(2)           => inject_error,
       PORT_B_O(3)           => pn_select,
       PORT_B_O(35 downto 4) => bert_read_sel
    );

  -- status register bits
  cdc_sync_adcif_to_uc: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  34,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  2,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
     )
    PORT MAP
    (
       CLK_A_I => DATA_CLK_I,
       CLK_B_I => Bus2IP_Clk,

       PORT_A_I(0)            => running,
       PORT_A_I(1)            => bert_read_ack,
       PORT_A_I(33 downto  2) => counter_reg,
       PORT_B_O(0)            => slv_reg1(C_SLV_DWIDTH-1),
       PORT_B_O(1)            => counter_read_ack,
       PORT_B_O(33 downto  2) => slv_reg_counter
    );

  ber_tester_inst : ber_tester_8ch_top
    port map
    (
      DATA_I         => DATA_I,
      DATA_VALID_I   => DATA_VALID_I,
      ENABLE_TEST_I  => enable_test,
      PN_SELECT_I    => pn_select,
      INJECT_ERROR_I => inject_error,
      RUNNING_O      => running,
      REG_SEL_I      => bert_read_sel,
      RD_ACK_O       => bert_read_ack,
      COUNTER_REG_O  => counter_reg,
      RESET_TEST_I   => reset_test,
      DATA_CLK_I     => DATA_CLK_I
    );


  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  -- 
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  -- 
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  -- 
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 33);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 33);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4) or Bus2IP_WrCE(5) or Bus2IP_WrCE(6) or Bus2IP_WrCE(7) or Bus2IP_WrCE(8) or Bus2IP_WrCE(9) or Bus2IP_WrCE(10) or Bus2IP_WrCE(11) or Bus2IP_WrCE(12) or Bus2IP_WrCE(13) or Bus2IP_WrCE(14) or Bus2IP_WrCE(15) or Bus2IP_WrCE(16) or Bus2IP_WrCE(17) or Bus2IP_WrCE(18) or Bus2IP_WrCE(19) or Bus2IP_WrCE(20) or Bus2IP_WrCE(21) or Bus2IP_WrCE(22) or Bus2IP_WrCE(23) or Bus2IP_WrCE(24) or Bus2IP_WrCE(25) or Bus2IP_WrCE(26) or Bus2IP_WrCE(27) or Bus2IP_WrCE(28) or Bus2IP_WrCE(29) or Bus2IP_WrCE(30) or Bus2IP_WrCE(31) or Bus2IP_WrCE(32) or Bus2IP_WrCE(33);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or counter_read_ack;

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
        -- other registers are status registers (read only)
      else
        case slv_reg_write_sel is
          when "1000000000000000000000000000000000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          -- other registers are status registers (read only)
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1, slv_reg_counter) is
  begin

    case slv_reg_read_sel is
      when "1000000000000000000000000000000000" => slv_ip2bus_data <= slv_reg0;
      when "0100000000000000000000000000000000" => slv_ip2bus_data <= slv_reg1;
      when "0010000000000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0001000000000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000100000000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000010000000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000001000000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000100000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000010000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000001000000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000100000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000010000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000001000000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000100000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000010000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000001000000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000100000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000010000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000001000000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000100000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000010000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000001000000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000100000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000010000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000001000000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000100000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000010000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000001000000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000000100000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000000010000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000000001000" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000000000100" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000000000010" => slv_ip2bus_data <= slv_reg_counter;
      when "0000000000000000000000000000000001" => slv_ip2bus_data <= slv_reg_counter;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';

end IMP;
