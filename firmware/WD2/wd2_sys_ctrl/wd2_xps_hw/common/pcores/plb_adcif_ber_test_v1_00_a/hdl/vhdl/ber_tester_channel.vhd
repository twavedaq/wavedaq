--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ber_tester_channel.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  28.01.2016 13:49:43
--
--  Description :  compares incomming data to a generated pseudo random pattern
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ber_tester_channel is
  port
  (
    DATA_I           : in  std_logic_vector(11 downto 0);
    DATA_VALID_I     : in  std_logic;
    ENABLE_TEST_I    : in  std_logic;
    LOAD_LO_I        : in  std_logic;
    LOAD_HI_I        : in  std_logic;
    PN_SELECT_I      : in  std_logic; -- 0 = PN9, 1 = PN23
    INJECT_ERROR_I   : in  std_logic;
    ERROR_COUNTER_O  : out std_logic_vector(63 downto 0);
    SAMPLE_COUNTER_O : out std_logic_vector(63 downto 0);
    RESET_TEST_I     : in  std_logic;
    DATA_CLK_I       : in  std_logic
  );
end ber_tester_channel;


architecture behavioral of ber_tester_channel is

  component pn_generator is
    port
    (
      DATA_I       : in  std_logic_vector(11 downto 0);
      DATA_O       : out std_logic_vector(11 downto 0);
      LOAD_LO_I    : in  std_logic;
      LOAD_HI_I    : in  std_logic;
      ENABLE_I     : in  std_logic;
      PN_SELECT_I  : in  std_logic; -- 0 = PN9, 1 = PN23
      CLK_I        : in  std_logic
    );
  end component;

  signal comp_data      : std_logic_vector(11 downto 0) := (others=>'0');
  signal pn_ref_data    : std_logic_vector(11 downto 0) := (others=>'0');
  signal error_vector   : std_logic_vector(11 downto 0) := (others=>'0');
  signal errors_in_set  : std_logic_vector( 3 downto 0) := (others=>'0'); 
  signal error_counter  : std_logic_vector(64 downto 0) := (others=>'0');
  signal sample_counter : std_logic_vector(64 downto 0) := (others=>'0');

  signal inject_error_s : std_logic := '0';
  signal flip_data_bit  : std_logic := '0';
  signal pn_gen_en      : std_logic := '0';

begin

  pn_gen_en <= DATA_VALID_I and ENABLE_TEST_I;

  -- outputs
  output_regs : process(DATA_CLK_I)
  begin
    if rising_edge(DATA_CLK_I) then
      if error_counter(64) = '1' then -- overflow
        ERROR_COUNTER_O <= (others=>'1');
      else
        ERROR_COUNTER_O <= error_counter(63 downto 0);
      end if;
      if sample_counter(64) = '1' then -- overflow
        SAMPLE_COUNTER_O <= (others=>'1');
      else
        SAMPLE_COUNTER_O <= sample_counter(63 downto 0);
      end if;
    end if;
  end process output_regs;

  gen_inst : pn_generator
    port map
    (
      DATA_I      => DATA_I,
      DATA_O      => pn_ref_data,
      LOAD_LO_I   => LOAD_LO_I,
      LOAD_HI_I   => LOAD_HI_I,
      ENABLE_I    => pn_gen_en,
      PN_SELECT_I => PN_SELECT_I,
      CLK_I       => DATA_CLK_I
    );

  counters : process(DATA_CLK_I)
  begin
    if rising_edge(DATA_CLK_I) then
      if RESET_TEST_I = '1' then
        sample_counter <= (others=>'0');
        error_counter  <= (others=>'0');
      else
        if ENABLE_TEST_I = '1' and
           DATA_VALID_I = '1' and
           sample_counter(64) = '0' and
           error_counter(64)  = '0' then -- overflow check
          sample_counter <= sample_counter + 1;
          error_counter  <= error_counter  + errors_in_set;
        end if;
      end if;
    end if;
  end process counters;

  process(DATA_CLK_I)
  begin
    if rising_edge(DATA_CLK_I) then
      inject_error_s <= INJECT_ERROR_I;
      if INJECT_ERROR_I = '1' and inject_error_s = '0' then
        flip_data_bit <= '1';
      elsif flip_data_bit = '1' and DATA_VALID_I = '1' then
        flip_data_bit <= '0';
      end if;
    end if;
  end process;

  process(DATA_CLK_I)
    variable error_sum : std_logic_vector(3 downto 0);
  begin
    if rising_edge(DATA_CLK_I) then
      if (DATA_VALID_I = '1' and ENABLE_TEST_I = '1') or LOAD_LO_I = '1' then
        comp_data <= DATA_I;
        if flip_data_bit = '1' then
          comp_data(11) <= not DATA_I(11);
        end if;
      end if;
      if DATA_VALID_I = '1' and ENABLE_TEST_I = '1' then
        error_vector <= pn_ref_data xor comp_data;
        error_sum := (others=>'0');
        for i in 11 downto 0 loop
            error_sum := error_sum + error_vector(i);
        end loop;
        errors_in_set <= error_sum;
      end if;
    end if;
  end process;

end Behavioral;
