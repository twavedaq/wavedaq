--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ber_tester_8ch_top.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  28.01.2016 13:49:43
--
--  Description :  compares incomming data to a generated pseudo random pattern
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;
use IEEE.STD_LOGIC_MISC.ALL;

entity ber_tester_8ch_top is
  port
  (
    DATA_I           : in  std_logic_vector((8*12)-1 downto 0); -- 8_ADC_Channels x 12_Data_Bits
    DATA_VALID_I     : in  std_logic;
    ENABLE_TEST_I    : in  std_logic;
    PN_SELECT_I      : in  std_logic; -- 0 = PN9, 1 = PN23
    INJECT_ERROR_I   : in  std_logic;
    RUNNING_O        : out std_logic;
    REG_SEL_I        : in  std_logic_vector(31 downto 0);
    RD_ACK_O         : out std_logic;
    COUNTER_REG_O    : out std_logic_vector(31 downto 0);
    RESET_TEST_I     : in  std_logic;
    DATA_CLK_I       : in  std_logic
  );
end ber_tester_8ch_top;


architecture behavioral of ber_tester_8ch_top is

  component ber_tester_8ch is
    port
    (
      DATA_I           : in  std_logic_vector((8*12)-1 downto 0); -- 8_ADC_Channels x 12_Data_Bits
      DATA_VALID_I     : in  std_logic;
      ENABLE_TEST_I    : in  std_logic;
      PN_SELECT_I      : in  std_logic; -- 0 = PN9, 1 = PN23
      INJECT_ERROR_I   : in  std_logic;
      RUNNING_O        : out std_logic;
      ERROR_COUNTER_O  : out std_logic_vector((8*64)-1 downto 0);
      SAMPLE_COUNTER_O : out std_logic_vector((8*64)-1 downto 0);
      RESET_TEST_I     : in  std_logic;
      DATA_CLK_I       : in  std_logic
    );
  end component;

  signal error_counter  : std_logic_vector((8*64)-1 downto 0) := (others=>'0');
  signal sample_counter : std_logic_vector((8*64)-1 downto 0) := (others=>'0');

begin

  RD_ACK_O <= OR_REDUCE(REG_SEL_I);

  data_out_mux : process(REG_SEL_I, error_counter, sample_counter)
  begin
    case REG_SEL_I is
      when "10000000000000000000000000000000" => COUNTER_REG_O <= error_counter( (1*32)-1 downto     0);
      when "01000000000000000000000000000000" => COUNTER_REG_O <= error_counter( (2*32)-1 downto  1*32);
      when "00100000000000000000000000000000" => COUNTER_REG_O <= error_counter( (3*32)-1 downto  2*32);
      when "00010000000000000000000000000000" => COUNTER_REG_O <= error_counter( (4*32)-1 downto  3*32);
      when "00001000000000000000000000000000" => COUNTER_REG_O <= error_counter( (5*32)-1 downto  4*32);
      when "00000100000000000000000000000000" => COUNTER_REG_O <= error_counter( (6*32)-1 downto  5*32);
      when "00000010000000000000000000000000" => COUNTER_REG_O <= error_counter( (7*32)-1 downto  6*32);
      when "00000001000000000000000000000000" => COUNTER_REG_O <= error_counter( (8*32)-1 downto  7*32);
      when "00000000100000000000000000000000" => COUNTER_REG_O <= error_counter( (9*32)-1 downto  8*32);
      when "00000000010000000000000000000000" => COUNTER_REG_O <= error_counter((10*32)-1 downto  9*32);
      when "00000000001000000000000000000000" => COUNTER_REG_O <= error_counter((11*32)-1 downto 10*32);
      when "00000000000100000000000000000000" => COUNTER_REG_O <= error_counter((12*32)-1 downto 11*32);
      when "00000000000010000000000000000000" => COUNTER_REG_O <= error_counter((13*32)-1 downto 12*32);
      when "00000000000001000000000000000000" => COUNTER_REG_O <= error_counter((14*32)-1 downto 13*32);
      when "00000000000000100000000000000000" => COUNTER_REG_O <= error_counter((15*32)-1 downto 14*32);
      when "00000000000000010000000000000000" => COUNTER_REG_O <= error_counter((16*32)-1 downto 15*32);
      when "00000000000000001000000000000000" => COUNTER_REG_O <= sample_counter( (1*32)-1 downto     0);
      when "00000000000000000100000000000000" => COUNTER_REG_O <= sample_counter( (2*32)-1 downto  1*32);
      when "00000000000000000010000000000000" => COUNTER_REG_O <= sample_counter( (3*32)-1 downto  2*32);
      when "00000000000000000001000000000000" => COUNTER_REG_O <= sample_counter( (4*32)-1 downto  3*32);
      when "00000000000000000000100000000000" => COUNTER_REG_O <= sample_counter( (5*32)-1 downto  4*32);
      when "00000000000000000000010000000000" => COUNTER_REG_O <= sample_counter( (6*32)-1 downto  5*32);
      when "00000000000000000000001000000000" => COUNTER_REG_O <= sample_counter( (7*32)-1 downto  6*32);
      when "00000000000000000000000100000000" => COUNTER_REG_O <= sample_counter( (8*32)-1 downto  7*32);
      when "00000000000000000000000010000000" => COUNTER_REG_O <= sample_counter( (9*32)-1 downto  8*32);
      when "00000000000000000000000001000000" => COUNTER_REG_O <= sample_counter((10*32)-1 downto  9*32);
      when "00000000000000000000000000100000" => COUNTER_REG_O <= sample_counter((11*32)-1 downto 10*32);
      when "00000000000000000000000000010000" => COUNTER_REG_O <= sample_counter((12*32)-1 downto 11*32);
      when "00000000000000000000000000001000" => COUNTER_REG_O <= sample_counter((13*32)-1 downto 12*32);
      when "00000000000000000000000000000100" => COUNTER_REG_O <= sample_counter((14*32)-1 downto 13*32);
      when "00000000000000000000000000000010" => COUNTER_REG_O <= sample_counter((15*32)-1 downto 14*32);
      when "00000000000000000000000000000001" => COUNTER_REG_O <= sample_counter((16*32)-1 downto 15*32);
      when others => COUNTER_REG_O <= (others => '0');
    end case;
  end process data_out_mux;

  ber_8ch_inst : ber_tester_8ch
    port map
    (
      DATA_I           => DATA_I,
      DATA_VALID_I     => DATA_VALID_I,
      ENABLE_TEST_I    => ENABLE_TEST_I,
      PN_SELECT_I      => PN_SELECT_I,
      INJECT_ERROR_I   => INJECT_ERROR_I,
      RUNNING_O        => RUNNING_O,
      ERROR_COUNTER_O  => error_counter,
      SAMPLE_COUNTER_O => sample_counter,
      RESET_TEST_I     => RESET_TEST_I,
      DATA_CLK_I       => DATA_CLK_I
    );

end Behavioral;
