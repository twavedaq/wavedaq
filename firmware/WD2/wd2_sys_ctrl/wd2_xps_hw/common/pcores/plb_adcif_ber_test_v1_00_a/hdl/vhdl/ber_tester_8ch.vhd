--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ber_tester_8ch.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  se32
--  Created :  28.01.2016 13:49:43
--
--  Description :  compares incomming data to a generated pseudo random pattern
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity ber_tester_8ch is
  port
  (
    DATA_I           : in  std_logic_vector((8*12)-1 downto 0); -- 8_ADC_Channels x 12_Data_Bits
    DATA_VALID_I     : in  std_logic;
    ENABLE_TEST_I    : in  std_logic;
    PN_SELECT_I      : in  std_logic; -- 0 = PN9, 1 = PN23
    INJECT_ERROR_I   : in  std_logic;
    RUNNING_O        : out std_logic;
    ERROR_COUNTER_O  : out std_logic_vector((8*64)-1 downto 0);
    SAMPLE_COUNTER_O : out std_logic_vector((8*64)-1 downto 0);
    RESET_TEST_I     : in  std_logic;
    DATA_CLK_I       : in  std_logic
  );
end ber_tester_8ch;


architecture behavioral of ber_tester_8ch is

  component ber_tester_channel is
    port
    (
      DATA_I           : in  std_logic_vector(11 downto 0);
      DATA_VALID_I     : in  std_logic;
      ENABLE_TEST_I    : in  std_logic;
      LOAD_LO_I        : in  std_logic;
      LOAD_HI_I        : in  std_logic;
      PN_SELECT_I      : in  std_logic; -- 0 = PN9, 1 = PN23
      INJECT_ERROR_I   : in  std_logic;
      ERROR_COUNTER_O  : out std_logic_vector(63 downto 0);
      SAMPLE_COUNTER_O : out std_logic_vector(63 downto 0);
      RESET_TEST_I     : in  std_logic;
      DATA_CLK_I       : in  std_logic
    );
  end component;

  signal running        : std_logic := '0';
  signal load_lo_reg    : std_logic := '0';
  signal load_hi_reg    : std_logic := '0';

  type type_state is (idle, load_hi, load_lo, run);
  signal state          : type_state;

  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

begin

  RUNNING_O        <= running;

  ber_tester_channel_gen : for ch in 7 downto 0 generate

    ber_tester_channel_inst : ber_tester_channel
      port map
      (
        DATA_I           => DATA_I(((ch+1)*12)-1 downto (ch*12)),
        DATA_VALID_I     => DATA_VALID_I,
        ENABLE_TEST_I    => running,
        LOAD_LO_I        => load_lo_reg,
        LOAD_HI_I        => load_hi_reg,
        PN_SELECT_I      => PN_SELECT_I,
        INJECT_ERROR_I   => INJECT_ERROR_I,
        ERROR_COUNTER_O  => ERROR_COUNTER_O(((ch+1)*64)-1 downto (ch*64)),
        SAMPLE_COUNTER_O => SAMPLE_COUNTER_O(((ch+1)*64)-1 downto (ch*64)),
        RESET_TEST_I     => RESET_TEST_I,
        DATA_CLK_I       => DATA_CLK_I
      );

  end generate;

  control_fsm_state : process(DATA_CLK_I)
  begin
    if rising_edge(DATA_CLK_I) then
      case state is
        when idle =>
          if ENABLE_TEST_I = '1' then
            state <= load_hi;
          end if;
        when load_hi =>
          if DATA_VALID_I = '1' then
            state <= load_lo;
          end if;
        when load_lo =>
          if DATA_VALID_I = '1' then
            state <= run;
          end if;
        when run =>
          if ENABLE_TEST_I = '0' or RESET_TEST_I = '1' then
            state <= idle;
          end if;
      end case;
    end if;
  end process control_fsm_state;

  control_fsm_output : process(state)
  begin
    -- defaults
    load_hi_reg <= '0';
    load_lo_reg <= '0';
    running   <= '0';
    -- state outputs
    case state is
      when idle =>
        null;
      when load_hi =>
        load_hi_reg <= '1';
      when load_lo =>
        load_lo_reg <= '1';
      when run =>
        running <= '1';
    end case;
  end process control_fsm_output;


end Behavioral;
