---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Periodic Auto-Trigger
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  10.01.2018 12:44:31
--
--  Description :  Periodic automatic trigger controlled by timer
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library UNISIM;
use UNISIM.VComponents.all;

entity auto_trigger is
  Port
  (
    LOAD_VALUE_I : in  STD_LOGIC_VECTOR (31 downto 0);
    TIMER_UP_O   : out STD_LOGIC;
    RST_I        : in  STD_LOGIC;
    CLK_I        : in  STD_LOGIC
  );
end auto_trigger;

architecture behavioral of auto_trigger is

  signal count_lsbs : std_logic_vector( 1 downto 0) := (others=>'0');
  signal count_msbs : std_logic_vector(31 downto 2) := (others=>'0');
  signal count      : std_logic_vector(31 downto 0);

begin

  count <= count_msbs & count_lsbs;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      TIMER_UP_O <= '0';
      if RST_I = '1' then 
        count_msbs <= LOAD_VALUE_I(31 downto 2);
        count_lsbs <= LOAD_VALUE_I( 1 downto 0);
      else
        if count = 0 then
          count_msbs <= LOAD_VALUE_I(31 downto 2);
          count_lsbs <= LOAD_VALUE_I( 1 downto 0);
          TIMER_UP_O <= '1';
        else
          count_lsbs <= count_lsbs - 1;
          if count_lsbs = "00" then
            count_msbs <= count_msbs - 1;
          end if;
        end if;
      end if;
    end if;
  end process;

end behavioral;

