---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Active Channel Detection
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  11.12.2017 15:55:22
--
--  Description :  Detects if a channel has triggered within the time window
--                 captured by the DRS chip.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity active_ch_single is
  generic
  (
    CGN_COUNT_WIDTH   : integer := 8
  );
  port (
    EDGE_I                : in  std_logic;
    ACTIVE_CH_O           : out std_logic;
    TICKS_IN_DRS_WINDOW_I : in  std_logic_vector(CGN_COUNT_WIDTH-1 downto 0);
    CLK_I                 : in  std_logic
  );
end active_ch_single;

architecture behavioral of active_ch_single is
  
  signal count        : std_logic_vector(CGN_COUNT_WIDTH-1 downto 0) := (others=>'0');
  
begin
  
  ACTIVE_CH_O <= '0' when count = 0 else '1';
  
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if EDGE_I = '1' then
        count <= TICKS_IN_DRS_WINDOW_I;
      elsif count > 0 then
        count <= count - 1;
      end if;
    end if;
  end process;
  
end architecture behavioral;