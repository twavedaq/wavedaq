---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Edge detector/counter
--
--  Project :  WaveDream2
--
--  PCB  :  WD2
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid, Gerd Theidel
--  Created :  09.03.2018 16:13:21
--
--  Description :  Detects and counts edges in parallel data.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library local_trigger_v2_00_a;
use local_trigger_v2_00_a.edge_count;

library unisim;
use unisim.VComponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity edge_detector is
  generic
  (
    CGN_DATA_WIDTH    : integer := 8;
    CGN_COUNT_WIDTH   : integer := 3
  );
  port
  (
    DATA_I        : in  std_logic_vector(CGN_DATA_WIDTH-1  downto 0);
    POLARITY_I    : in  std_logic; -- 0 = rising, 1 = falling
    EDGE_DETECT_O : out std_logic;
    EDGE_COUNT_O  : out std_logic_vector(CGN_COUNT_WIDTH-1 downto 0);
    CLK_I         : in  std_logic
  );
end edge_detector;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture struct of edge_detector is

  signal edge_count : std_logic_vector(CGN_COUNT_WIDTH-1 downto 0) := (others=>'0');
  signal count_0    : std_logic_vector(1 downto 0) := (others=>'0');
  signal count_1    : std_logic_vector(1 downto 0) := (others=>'0');
  signal data       : std_logic_vector(CGN_DATA_WIDTH downto 0) := (others=>'0');
  
begin

  data(CGN_DATA_WIDTH-1 downto 0) <= DATA_I;
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      data(CGN_DATA_WIDTH) <= DATA_I(0);
    end if;
  end process;

  ----------------------------------------------------------------
  ec_inst_0: entity local_trigger_v2_00_a.edge_count
  generic map
  (
    CGN_DATA_WIDTH   => 5,
    CGN_COUNT_WIDTH  => 2
  )
  port map
  (
    DATA_I       => data(4 downto 0),
    POLARITY_I   => POLARITY_I,
    EDGE_COUNT_O => count_0
  );

  ec_inst_1: entity local_trigger_v2_00_a.edge_count
  generic map
  (
    CGN_DATA_WIDTH   => 5,
    CGN_COUNT_WIDTH  => 2
  )
  port map
  (
    DATA_I       => data(8 downto 4),
    POLARITY_I   => POLARITY_I,
    EDGE_COUNT_O => count_1
  );

  edge_count    <= ('0' & count_0) + ('0' & count_1);  
  EDGE_COUNT_O  <= edge_count;
  EDGE_DETECT_O <= '1' when edge_count > 0 else '0';

end architecture struct;
