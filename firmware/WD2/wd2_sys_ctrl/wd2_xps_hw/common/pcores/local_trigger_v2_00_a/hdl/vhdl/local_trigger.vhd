---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Local Trigger Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  12.03.2018 09:06:08
--
--  Description :  Local WD2 trigger unit. Reduces and forwards the comparator signals
--                 from the front end to the OSERDES lines going to the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

library local_trigger_v2_00_a;
use local_trigger_v2_00_a.single_input_pulse_shaper;
use local_trigger_v2_00_a.local_trigger_ch;
use local_trigger_v2_00_a.aux_ch;
use local_trigger_v2_00_a.mainpattern;
use local_trigger_v2_00_a.auto_trigger;
use local_trigger_v2_00_a.synchronous_delay;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

Library UNISIM;
use UNISIM.vcomponents.all;

entity local_trigger is
  generic (
    CGN_PATTERNS          : integer   := 19;
    CGN_PATTERN_SOURCES   : integer   := 19;
    CGN_DELAY_SEL_BITS    : integer   :=  8;
    CGN_COMP_DIFF_IN      : integer   :=  0; -- 1 = diff input, 0 = SE input
    CGN_CMP_CHNLS         : integer   := 16
  );
  port (
    -- Trigger Signals
    COMP_BUS_P_I               : in  std_logic_vector(CGN_CMP_CHNLS-1 downto 0);
    COMP_BUS_N_I               : in  std_logic_vector(CGN_CMP_CHNLS-1 downto 0);
    EXT_TRIG_BPL_P_I           : in  std_logic;
    EXT_TRIG_BPL_N_I           : in  std_logic;
    EXT_TRIG_MCX_I             : in  std_logic;
    EXT_TRIG_MCX_O             : out std_logic;
    EXT_TRIG_O                 : out std_logic;
    ADV_TRIG_I                 : in  std_logic;
    ADV_TRIG_VETO_I            : in  std_logic;
    TRIGGER_O                  : out std_logic;
    COMP_TO_TRG_O              : out std_logic_vector(CGN_CMP_CHNLS-1 downto 0);
    SHAPER_TO_TRG_O            : out std_logic_vector(CGN_CMP_CHNLS-1 downto 0);
    -- Trigger Configuration
    CFG_SRC_EN_PATTERNS_I      : in  std_logic_vector(CGN_PATTERNS*CGN_PATTERN_SOURCES-1 downto 0);
    CFG_STATE_PATTERNS_I       : in  std_logic_vector(CGN_PATTERNS*CGN_PATTERN_SOURCES-1 downto 0);
    CFG_PATTERN_EN_I           : in  std_logic_vector(CGN_PATTERNS-1 downto 0); -- pattern bit enable (equivalent to set pattern(i)=X"00000000")
    CFG_SRC_POLARITY_I         : in  std_logic_vector(CGN_PATTERN_SOURCES-1 downto 0); -- 0 = normal, 1 = inverted
    CFG_EXT_ASYNC_TRIGGER_EN_I : in  std_logic;
    CFG_PATTERN_TRIGGER_EN_I   : in  std_logic;
    CFG_LEAD_TRAIL_EDGE_SEL_I : in  std_logic;
    CFG_TRIG_DELAY_I          : in  std_logic_vector(CGN_DELAY_SEL_BITS-1 downto 0);
    CFG_TRIG_PULSE_LENGTH_I   : in  std_logic_vector(2 downto 0);
    CFG_AUTO_TRIG_ENABLE_I    : in  std_logic;
    CFG_AUTO_TRIG_PERIOD_I    : in  std_logic_vector(31 downto 0);
    CFG_EXT_TRIG_MCX_OUT_EN_I : in  std_logic;
    CFG_BACKPLANE_PLUGGED_I   : in  std_logic;
    -- Auxiliary Counter Channels
    --EXT_TRIG_BPL_P_I          : in  std_logic;
    --EXT_TRIG_BPL_N_I          : in  std_logic;
    --EXT_TRIG_MCX_I            : in  std_logic;
    EXT_CLK_I                  : in  std_logic;
    -- TDC
    TRG_TDC_DATA_O             : out std_logic_vector(8*CGN_CMP_CHNLS-1 downto 0);
    TRG_TDC_BIT_CLK_I          : in  std_logic;
    TRG_TDC_DIV_CLK_I          : in  std_logic;
    TRG_TDC_PLL_LOCKED_I       : in  std_logic;
    TRG_TDC_LOCKED_O           : out std_logic;
    TRG_TDC_PLL_RST_I          : in  std_logic;
    -- Triggered Channels within window
    ACTIVE_CH_O                : out std_logic_vector(CGN_CMP_CHNLS-1 downto 0);
    ACTIVE_CH_WINDOW_I         : in  std_logic_vector(7 downto 0);
    -- Trigger Counter
    RST_COUNTER_I              : in  std_logic;
    COUNT_O                    : out std_logic_vector(19*64-1 downto 0);
    -- Clock
    SYS_CLK_I                  : in  std_logic;
    CLK_I                      : in  std_logic
  );
end local_trigger;

architecture behavioral of local_trigger is

  constant C_TOTAL_PATTERN_WIDTH  : integer := CGN_PATTERNS*CGN_PATTERN_SOURCES;
  constant C_PTRN_AUX_SOURCES     : integer :=  3;
  constant C_PTRN_SOURCES         : integer :=  C_PTRN_AUX_SOURCES + CGN_CMP_CHNLS;
  constant C_SI_SHAPER_CHNLS      : integer := 2;

  signal aux_si_shaper_ch_in      : std_logic_vector(C_SI_SHAPER_CHNLS-1 downto 0) := (others=>'0');

  signal shaper_out_ch            : std_logic_vector(CGN_CMP_CHNLS-1 downto 0)      := (others=>'0');
  signal ext_shaper_out           : std_logic := '0';
  signal si_shaper_out            : std_logic_vector(C_SI_SHAPER_CHNLS-1 downto 0)  := (others=>'0');
  signal ptrn_logic_in            : std_logic_vector(C_PTRN_SOURCES-1 downto 0)     := (others=>'0');
  signal pattern_trig             : std_logic := '0';
  signal pattern_trig_dly         : std_logic := '0';
  signal trigger                  : std_logic := '0';
  signal auto_trig                : std_logic := '0';
  signal auto_trig_rst            : std_logic := '0';
  signal local_trig               : std_logic := '0';
  signal ext_trig                 : std_logic := '0';
  signal ext_trig_bpl             : std_logic := '0';
  signal drs_ch_count             : std_logic_vector(64*CGN_CMP_CHNLS-1 downto 0) := (others=>'0');
  signal ext_count                : std_logic_vector(63 downto 0) := (others=>'0');
  signal ext_clk_count            : std_logic_vector(63 downto 0) := (others=>'0');
  signal local_count              : std_logic_vector(63 downto 0) := (others=>'0');

  signal strobe                   : std_logic_vector(2 downto 0) := (others=>'0');
  signal bit_clk                  : std_logic_vector(2 downto 0) := (others=>'0');
  signal locked                   : std_logic_vector(2 downto 0) := (others=>'0');

    signal cfg_trigger_type_sel     : std_logic := '0';
  signal cfg_ext_async_trigger_en : std_logic := '0';
  signal cfg_pattern_trigger_en   : std_logic := '0';
  signal cfg_lead_trail_edge_sel  : std_logic := '0';
  signal cfg_auto_trig_en         : std_logic := '0';
  signal rst_counter              : std_logic := '0';
  signal cfg_ext_trig_mcx_out_en  : std_logic := '0';
  signal active_ch_window         : std_logic_vector(7 downto 0) := (others=>'0');
  signal cfg_trig_pulse_length    : std_logic_vector(2 downto 0) := (others=>'0');
  signal cfg_trig_delay           : std_logic_vector(CGN_DELAY_SEL_BITS-1 downto 0):= (others=>'0');
  signal cfg_src_en_patterns      : std_logic_vector(C_TOTAL_PATTERN_WIDTH-1 downto 0)  := (others=>'0');
  signal cfg_state_patterns       : std_logic_vector(C_TOTAL_PATTERN_WIDTH-1 downto 0)  := (others=>'0');
  signal cfg_pattern_en           : std_logic_vector(CGN_PATTERNS-1 downto 0):= (others=>'0');
  signal cfg_src_polarity         : std_logic_vector(CGN_PATTERN_SOURCES-1 downto 0)  := (others=>'0');

begin

  cdc_sync_sys_to_trig_cfg : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  17,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I => SYS_CLK_I,
    CLK_B_I => CLK_I,
    PORT_A_I(0)            => CFG_EXT_ASYNC_TRIGGER_EN_I,
    PORT_A_I(1)            => CFG_PATTERN_TRIGGER_EN_I,
    PORT_A_I(2)            => CFG_LEAD_TRAIL_EDGE_SEL_I,
    PORT_A_I(3)            => CFG_EXT_TRIG_MCX_OUT_EN_I,
    PORT_A_I(4)            => CFG_AUTO_TRIG_ENABLE_I,
    PORT_A_I(5)            => RST_COUNTER_I,
    PORT_A_I(13 downto  6) => ACTIVE_CH_WINDOW_I,
    PORT_A_I(16 downto 14) => CFG_TRIG_PULSE_LENGTH_I,
    PORT_B_O(0)            => cfg_ext_async_trigger_en,
    PORT_B_O(1)            => cfg_pattern_trigger_en,
    PORT_B_O(2)            => cfg_lead_trail_edge_sel,
    PORT_B_O(3)            => cfg_ext_trig_mcx_out_en,
    PORT_B_O(4)            => cfg_auto_trig_en,
    PORT_B_O(5)            => rst_counter,
    PORT_B_O(13 downto  6) => active_ch_window,
    PORT_B_O(16 downto 14) => cfg_trig_pulse_length
  );

  cdc_sync_sys_to_trigger_delay : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_DELAY_SEL_BITS,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_TRIG_DELAY_I,
    PORT_B_O => cfg_trig_delay
  );

  cdc_sync_sys_to_trig_cfg_patterns : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  C_TOTAL_PATTERN_WIDTH,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_SRC_EN_PATTERNS_I,
    PORT_B_O => cfg_src_en_patterns
  );

  cdc_sync_sys_to_trig_cfg_inhibit_patterns : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  C_TOTAL_PATTERN_WIDTH,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_STATE_PATTERNS_I,
    PORT_B_O => cfg_state_patterns
  );

  cdc_sync_sys_to_trig_cfg_pattern_en : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_PATTERNS,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_PATTERN_EN_I,
    PORT_B_O => cfg_pattern_en
  );

  cdc_sync_sys_to_trig_ch_polarity : cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  CGN_PATTERN_SOURCES,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  1,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I  => SYS_CLK_I,
    CLK_B_I  => CLK_I,
    PORT_A_I => CFG_SRC_POLARITY_I,
    PORT_B_O => cfg_src_polarity
  );

  comp_bus_ibufds_inst : IBUFDS
  generic map (
    DIFF_TERM => FALSE,
    IBUF_LOW_PWR => FALSE,
    IOSTANDARD => "LVDS_25")
  port map (
    O  => ext_trig_bpl,
    I  => EXT_TRIG_BPL_P_I,
    IB => EXT_TRIG_BPL_N_I
  );
  ext_trig <= ext_trig_bpl when CFG_BACKPLANE_PLUGGED_I = '1' else EXT_TRIG_MCX_I;

  aux_si_shaper_ch_in <= ADV_TRIG_I & ADV_TRIG_VETO_I;

  aux_shapers_inst : for i in C_SI_SHAPER_CHNLS-2 downto 0 generate

    aux_shaper_inst : entity local_trigger_v2_00_a.single_input_pulse_shaper
    generic map (
      CGN_LENGTH_BITS => 3
    )
    port map (
      IN_I            => aux_si_shaper_ch_in(i),
      PULSE_LENGTH_I  => cfg_trig_pulse_length,
      POLARITY_I      => CFG_SRC_POLARITY_I(i+CGN_CMP_CHNLS+1),
      EDGE_O          => si_shaper_out(i),
      CLK_I           => CLK_I
    );

  end generate aux_shapers_inst;

  trig_logic_inst : for i in CGN_CMP_CHNLS-1 downto 0 generate

    ch_trig_logic_inst : entity local_trigger_v2_00_a.local_trigger_ch
    generic map (
      CGN_COMP_DIFF_IN         => CGN_COMP_DIFF_IN
    )
    port map (
      COMP_P_I                 => COMP_BUS_P_I(i),
      COMP_N_I                 => COMP_BUS_N_I(i),
      COMP_TO_TRG_O            => COMP_TO_TRG_O(i),
      EDGE_O                   => shaper_out_ch(i),
      CFG_POLARITY_I           => CFG_SRC_POLARITY_I(i),
      CFG_TRIG_PULSE_LENGTH_I  => cfg_trig_pulse_length,
      TRG_TDC_DATA_O           => TRG_TDC_DATA_O(8*(i+1)-1 downto 8*i),
      TRG_TDC_BIT_CLK_I        => bit_clk(i/8),
      TRG_TDC_DIV_CLK_I        => TRG_TDC_DIV_CLK_I,
      TRG_TDC_STROBE_I         => strobe(i/8),
      ACTIVE_CH_O              => ACTIVE_CH_O(i),
      ACTIVE_CH_WINDOW_I       => active_ch_window,
      ACTIVE_CH_POLARITY_I     => cfg_src_polarity(i),
      RST_COUNTER_I            => rst_counter,
      COUNT_O                  => drs_ch_count(64*(i+1)-1 downto 64*i),
      RESET_I                  => '0',
      CLK_I                    => CLK_I
    );

  end generate trig_logic_inst;

  aux_ch_inst : entity local_trigger_v2_00_a.aux_ch
  port map(
    MCX_TRIGGER_I           => EXT_TRIG_MCX_I,
    BACKPLANE_TRIGGER_I     => ext_trig_bpl,
    EXT_CLK_I               => EXT_CLK_I,
    BACKPLANE_PLUGGED_I     => CFG_BACKPLANE_PLUGGED_I,
    EXT_SHAPER_OUT_O        => ext_shaper_out,
    CFG_POLARITY_EXT_I      => CFG_SRC_POLARITY_I(16),
    CFG_TRIG_PULSE_LENGTH_I => cfg_trig_pulse_length,
    SERDES_DIV_CLK_I        => TRG_TDC_DIV_CLK_I,
    SERDES_BIT_CLK_MCX_I    => bit_clk(2), -- bank 0
    SERDES_BIT_CLK_BPL_I    => bit_clk(1), -- bank 1
    SERDES_BIT_CLK_ECK_I    => bit_clk(1), -- bank 1
    SERDES_STROBE_MCX_I     => strobe(2),  -- bank 0
    SERDES_STROBE_BPL_I     => strobe(1),  -- bank 1
    SERDES_STROBE_ECK_I     => strobe(1),  -- bank 1
    RST_COUNTER_I           => rst_counter,
    EXT_COUNT_O             => ext_count,
    EXT_CLK_COUNT_O         => ext_clk_count,
    RESET_I                 => '0',
    CLK_I                   => CLK_I
  );

  bufpll_gen : for i in 0 to 2 generate
  --   0: bank 3   /   1: bank 1   /   2: bank 0
  begin
    BUFPLL_inst : BUFPLL
    generic map (
      DIVIDE => 8,         -- DIVCLK divider (1-8)
      ENABLE_SYNC => TRUE  -- Enable synchrnonization between PLL and GCLK (TRUE/FALSE)
    )
    port map (
      IOCLK        => bit_clk(i),            -- 1-bit output: Output I/O clock
      LOCK         => locked(i),             -- 1-bit output: Synchronized LOCK output
      SERDESSTROBE => strobe(i),             -- 1-bit output: Output SERDES strobe (connect to ISERDES2/OSERDES2)
      GCLK         => TRG_TDC_DIV_CLK_I,     -- 1-bit input: BUFG clock input
      LOCKED       => TRG_TDC_PLL_LOCKED_I,  -- 1-bit input: LOCKED input from PLL
      PLLIN        => TRG_TDC_BIT_CLK_I      -- 1-bit input: Clock input from PLL
    );
  end generate;

  TRG_TDC_LOCKED_O <= AND_REDUCE(locked);

  SHAPER_TO_TRG_O <= shaper_out_ch;
  ptrn_logic_in   <= si_shaper_out & ext_shaper_out & shaper_out_ch;

  pattern_trigger_local_inst : entity local_trigger_v2_00_a.mainpattern
  generic map(
    CGN_NR_OF_CH       => CGN_PATTERN_SOURCES,
    CGN_NR_OF_PATTERNS => CGN_PATTERNS
  )
  port map(
    SRC_EN_PATTERNS_I     => cfg_src_en_patterns,
    STATE_PATTERNS_I      => cfg_state_patterns,
    PATTERN_EN_I          => cfg_pattern_en,
    LEAD_TRAIL_EDGE_SEL_I => cfg_lead_trail_edge_sel,
    EDGE_I                => ptrn_logic_in,
    TRIGGER_O             => pattern_trig,
    CLK_I                 => CLK_I
  );

  auto_trig_rst <= not(cfg_auto_trig_en);

  auto_trigger_inst : entity local_trigger_v2_00_a.auto_trigger
  port map
  (
    LOAD_VALUE_I => CFG_AUTO_TRIG_PERIOD_I,
    TIMER_UP_O   => auto_trig,
    RST_I        => auto_trig_rst,
    CLK_I        => CLK_I
  );

  synch_delay_inst : entity local_trigger_v2_00_a.synchronous_delay
  generic map (
    CGN_DELAY_VALUE_WIDTH => CGN_DELAY_SEL_BITS
  )
  port map (
    DELAY_I => CFG_TRIG_DELAY_I,
    SIG_I   => pattern_trig,
    SIG_O   => pattern_trig_dly,
    CLK_I   => CLK_I
  );

  local_trigger_counter : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if rst_counter = '1' then
        local_count <= (others => '0');
      elsif local_trig = '1' then
        local_count <= local_count + 1;
     end if;
   end if;
  end process;

  COUNT_O       <= ext_clk_count & ext_count & local_count & drs_ch_count;

  local_trig <= pattern_trig_dly or auto_trig;
  trigger    <= (local_trig and cfg_pattern_trigger_en  ) or
                (  ext_trig and cfg_ext_async_trigger_en);
  TRIGGER_O  <= trigger;

  EXT_TRIG_MCX_O <= trigger when cfg_ext_trig_mcx_out_en = '1' else '0';
  EXT_TRIG_O     <= ext_trig;

end architecture behavioral;
