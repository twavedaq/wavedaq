---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Local Trigger Unit
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  12.03.2018 09:06:08
--
--  Description :  Local WD2 trigger unit. Reduces and forwards the comparator signals
--                 from the front end to the OSERDES lines going to the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
--use IEEE.STD_LOGIC_UNSIGNED.ALL;
--use IEEE.STD_LOGIC_MISC.ALL;

library local_trigger_v2_00_a;
use local_trigger_v2_00_a.edge_detector;
use local_trigger_v2_00_a.trigger_iserdes_channel;
use local_trigger_v2_00_a.dual_input_pulse_shaper;
use local_trigger_v2_00_a.active_ch_single;
use local_trigger_v2_00_a.counter;

Library UNISIM;
use UNISIM.vcomponents.all;

entity local_trigger_ch is
  generic (
    CGN_COMP_DIFF_IN          : integer   := 0  -- 1 = diff input, 0 = SE input
  );
  port (
    -- Trigger Signals
    COMP_P_I                  : in  std_logic;
    COMP_N_I                  : in  std_logic;
    COMP_TO_TRG_O             : out std_logic;
    -- Detected Edges (Shaped) Output
    EDGE_O                    : out std_logic;
    -- Trigger Configuration
    CFG_POLARITY_I            : in  std_logic; -- 0 = rising, 1 = falling
    CFG_TRIG_PULSE_LENGTH_I   : in  std_logic_vector(2 downto 0);
    -- TDC
    TRG_TDC_DATA_O            : out std_logic_vector(7 downto 0);
    TRG_TDC_BIT_CLK_I         : in  std_logic;
    TRG_TDC_DIV_CLK_I         : in  std_logic;
    TRG_TDC_STROBE_I          : in  std_logic;
    -- Triggered Channels within window
    ACTIVE_CH_O               : out std_logic;
    ACTIVE_CH_WINDOW_I        : in  std_logic_vector(7 downto 0);
    ACTIVE_CH_POLARITY_I      : in  std_logic; -- 1 = positive, 0 = inverted
    -- Trigger Counter
    RST_COUNTER_I             : in  std_logic;
    COUNT_O                   : out std_logic_vector(63 downto 0);
    -- Clock and Reset
    RESET_I                   : in  std_logic;
    CLK_I                     : in  std_logic
  );
end local_trigger_ch;

architecture behavioral of local_trigger_ch is

  constant C_SERDES_PWIDTH    : integer := 8;
  constant C_EDGE_COUNT_WIDTH : integer := 3;

  signal comp                : std_logic  := '0';
  signal tdc_data            : std_logic_vector(C_SERDES_PWIDTH-1 downto 0)  := (others=>'0');
  signal edge_detected   : std_logic := '0';
  signal pos_edge_count      : std_logic_vector(C_EDGE_COUNT_WIDTH-1 downto 0) := (others=>'0');

begin

  comp_diff_in_buffer : if CGN_COMP_DIFF_IN = 1 generate
    comp_bus_ibufds_inst : IBUFDS
    generic map (
      DIFF_TERM => TRUE,
      IBUF_LOW_PWR => TRUE,
      IOSTANDARD => "LVDS_25")
    port map (
      O  => comp,
      I  => COMP_P_I,
      IB => COMP_N_I
    );
  end generate comp_diff_in_buffer;

  comp_se_in_buffer : if CGN_COMP_DIFF_IN = 0 generate
    comp <= COMP_P_I;
  end generate comp_se_in_buffer;

  iserdes_inst : entity local_trigger_v2_00_a.trigger_iserdes_channel
  generic map (
    CGN_RATIO           => C_SERDES_PWIDTH
  )
  port map (
    SERIAL_TRIGGER_I    => comp,
    PARALLEL_TRIGGER_O  => tdc_data,
    BIT_CLK_I           => TRG_TDC_BIT_CLK_I,
    DIV_CLK_I           => TRG_TDC_DIV_CLK_I,
    STROBE_I            => TRG_TDC_STROBE_I,
    RESET_I             => RESET_I
  );

  TRG_TDC_DATA_O <= tdc_data;

  edge_detector_inst : entity local_trigger_v2_00_a.edge_detector
  generic map (
    CGN_DATA_WIDTH    => C_SERDES_PWIDTH,
    CGN_COUNT_WIDTH   => C_EDGE_COUNT_WIDTH
  )
  port map (
    DATA_I        => tdc_data,
    POLARITY_I    => CFG_POLARITY_I,
    EDGE_DETECT_O => edge_detected,
    EDGE_COUNT_O  => pos_edge_count,
    CLK_I         => CLK_I
  );
  
  pulse_shaper_inst : entity local_trigger_v2_00_a.dual_input_pulse_shaper
  generic map (
    CGN_LENGTH_BITS => 3
  )
  port map (
    PULSE_I         => edge_detected,
    POLARITY_I      => CFG_POLARITY_I,
    SERDES_LSB_I    => tdc_data(0),
    PULSE_LENGTH_I  => CFG_TRIG_PULSE_LENGTH_I,
    EDGE_O          => EDGE_O,
    CLK_I           => CLK_I
  );

  ------------------------------
  -- Zero Suppression Support --
  ------------------------------

  active_ch_inst : entity local_trigger_v2_00_a.active_ch_single
  generic map (
    CGN_COUNT_WIDTH       => 8
  )
  port map (
    EDGE_I                => edge_detected,
    ACTIVE_CH_O           => ACTIVE_CH_O,
    TICKS_IN_DRS_WINDOW_I => ACTIVE_CH_WINDOW_I,
    CLK_I                 => CLK_I
  );

  ------------------------------
  -- Trigger Counter (Scaler) --
  ------------------------------
  
  trig_counter_inst : entity local_trigger_v2_00_a.counter
  port map (
    CLK_I       => CLK_I,
    RST_I       => RST_COUNTER_I,
    INCREMENT_I => pos_edge_count,
    COUNT_O     => COUNT_O
  );
  
  
end architecture behavioral;
