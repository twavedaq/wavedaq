--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : edge_count.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2018.01.31
--
--  Description :
--    count rising edges in input data vector
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


library unisim;
use unisim.VComponents.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity edge_count is
  generic
  (
    CGN_DATA_WIDTH    : integer := 9;
    CGN_COUNT_WIDTH   : integer := 3
  );
  port
  (
    DATA_I        : in  std_logic_vector(CGN_DATA_WIDTH-1  downto 0);
    POLARITY_I    : in  std_logic; -- 0 = rising, 1 = falling
    EDGE_COUNT_O  : out std_logic_vector(CGN_COUNT_WIDTH-1 downto 0)
  );
end edge_count;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of edge_count is

begin

  count_proc: process (DATA_I) is
    variable count_var : std_logic_vector(CGN_COUNT_WIDTH-1 downto 0);
  begin
    count_var := (others => '0');

    for i in CGN_DATA_WIDTH-1 downto 1 loop
      if POLARITY_I = '0' then
        if DATA_I(i) = '0' and DATA_I(i-1) = '1' then
          count_var := count_var + 1;
        end if;
      else
        if DATA_I(i) = '1' and DATA_I(i-1) = '0' then
          count_var := count_var + 1;
        end if;
      end if;
    end loop;

    EDGE_COUNT_O <= count_var;
  end process count_proc;

end architecture behave;
