---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Synchronous Delay
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid, Stefan Ritt
--  Created :  24.11.2015 15:32:12
--
--  Description :  Synchronous delay with LUTs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity synchronous_delay is
  generic (
    CGN_DELAY_VALUE_WIDTH : integer := 8
  );
  port (
    DELAY_I : in  std_logic_vector(CGN_DELAY_VALUE_WIDTH-1 downto 0);
    SIG_I   : in  std_logic;
    SIG_O   : out std_logic;
    CLK_I   : in  std_logic
  );
end synchronous_delay;



architecture behavioral of synchronous_delay is

  constant C_SREG_SIZE    : integer   := 2**(CGN_DELAY_VALUE_WIDTH-1);
  
  signal falling_edge_out : std_logic := '0';
  signal dly_chain_out    : std_logic := '0';
  signal sreg             : std_logic_vector(C_SREG_SIZE-1 downto 0) := (others=>'0');
  
begin

  shift_reg_inst : process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      sreg <= sreg(C_SREG_SIZE-2 downto 0) & SIG_I;
    end if;
  end process;
  dly_chain_out <= sreg(CONV_INTEGER(DELAY_I(CGN_DELAY_VALUE_WIDTH-1 downto 1)));
  
  process(CLK_I)
  begin
    if falling_edge(CLK_I) then
      falling_edge_out <= dly_chain_out;
    end if;
  end process;
  
  SIG_O <= falling_edge_out when DELAY_I(0) = '1' else dly_chain_out;

end architecture behavioral;

--------------------------------------------------------------------------------
-- The behavioral description above resulted in slightly lower resource usage --
--------------------------------------------------------------------------------

--architecture behavioral of synchronous_delay is
--
--  constant C_SRL_AWIDTH : integer := 5;
--  constant C_MUX_AWIDTH : integer := CGN_DELAY_VALUE_WIDTH-C_SRL_AWIDTH-1;
--  constant C_NR_OF_SRLS : integer := 2**(C_MUX_AWIDTH); -- 1 bit for falling edge register
--  constant C_SRL_LEN    : integer := 32;
--
--  signal srl_in           : std_logic_vector(C_NR_OF_SRLS downto 0)   := (others=>'0');
--  signal srl_out          : std_logic_vector(C_NR_OF_SRLS-1 downto 0) := (others=>'0');
--  signal mux_addr         : std_logic_vector(C_MUX_AWIDTH-1 downto 0) := (others=>'0');
--  signal srl_addr         : std_logic_vector(C_SRL_AWIDTH-1 downto 0) := (others=>'0');
--  signal falling_edge_sel : std_logic := '0';
--  signal falling_edge_out : std_logic := '0';
--  signal dly_chain_out    : std_logic := '0';
--
--  type srl_type is array (C_NR_OF_SRLS-1 downto 0) of std_logic_vector(C_SRL_LEN-1 downto 0);
--  signal sreg             : srl_type := (others=>(others=>'0'));
--  
--begin
--
--  srl_in(0) <= SIG_IN_I;
--
--  mux_addr         <= DELAY_I(CGN_DELAY_VALUE_WIDTH-1 downto C_SRL_AWIDTH+1);
--  srl_addr         <= DELAY_I(C_SRL_AWIDTH downto 1);
--  falling_edge_sel <= DELAY_I(0);
--  
--  srl_chain : for i in 0 to C_NR_OF_SRLS-1 generate
--
--    srlc32e_inst : SRLC32E
--    generic map (
--      INIT => X"00000000"
--    )
--    port map (
--      Q   => srl_out(i),  -- SRL data output
--      Q31 => srl_in(i+1), -- SRL cascade output pin
--      A   => srl_addr,    -- 5-bit shift depth select input
--      CE  => '1',         -- Clock enable input
--      CLK => CLK_I,       -- Clock input
--      D   => srl_in(i)    -- SRL data input
--    );
--
--  end generate srl_chain;
--
--  dly_chain_out <= srl_out(CONV_INTEGER(mux_addr));
--
--  process(CLK_I)
--  begin
--    if falling_edge(CLK_I) then
--      falling_edge_out <= dly_chain_out;
--    end if;
--  end process;
--  
--  SIG_OUT_O <= falling_edge_out when falling_edge_sel = '1' else dly_chain_out;
--
--end architecture behavioral;
