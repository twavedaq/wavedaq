---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Synchronous Pulse Shaper
--
--  Project :  WaveDream2
--
--  PCB  :  WD2
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  09.03.2018 16:13:21
--
--  Description :  Synchronous pulse shaper.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
use IEEE.STD_LOGIC_MISC.ALL;

Library UNISIM;
use UNISIM.vcomponents.all;

entity single_input_pulse_shaper is
  generic (
    CGN_LENGTH_BITS : integer := 8
  );
  port (
    -- Trigger Signals
    IN_I           : in  std_logic;
    PULSE_LENGTH_I : in  std_logic_vector(CGN_LENGTH_BITS-1 downto 0);
    POLARITY_I     : in  std_logic; -- 0 = rising, 1 = falling
    EDGE_O         : out std_logic;
    CLK_I          : in  std_logic
  );
end single_input_pulse_shaper;

architecture behavioral of single_input_pulse_shaper is

  signal pcount   : std_logic_vector(CGN_LENGTH_BITS-1 downto 0) := (others=>'0');
  signal edge_out : std_logic := '0';

  signal input    : std_logic := '0';
  
begin

  input <= IN_I when POLARITY_I = '0' else not IN_I;
  
  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if input = '1' and edge_out = '0' then
        pcount <= PULSE_LENGTH_I;
      elsif pcount > 0 then
        pcount <= pcount - 1;
      end if;
    end if;
  end process;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if input = '1' then
        edge_out <= '1';
      elsif pcount = 0 then
        edge_out <= '0';
      end if;
    end if;
  end process;

  EDGE_O <= edge_out;

end architecture behavioral;
