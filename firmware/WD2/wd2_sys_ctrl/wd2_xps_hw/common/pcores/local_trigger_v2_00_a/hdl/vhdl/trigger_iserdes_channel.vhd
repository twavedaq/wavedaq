---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  ISERDES for higher trigger time resolution
--
--  Project :  WaveDream2
--
--  PCB  :  WaveDream2
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  29.11.2016 07:58:15
--
--  Description :  ISERDES unit sampling the trigger (comparator) input of each channel
--                 of the WaveDream2 Board. Single channel.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity trigger_iserdes_channel is
  Generic
  (
    CGN_RATIO          : integer :=  8
  );
  Port
  (
    SERIAL_TRIGGER_I    : in  std_logic;
    PARALLEL_TRIGGER_O  : out std_logic_vector(CGN_RATIO-1 downto 0);
    BIT_CLK_I           : in  std_logic; -- Directly from PLL
    DIV_CLK_I           : in  std_logic; -- Global Clock
    STROBE_I            : in  std_logic;
    RESET_I             : in  std_logic
  );
end trigger_iserdes_channel;

architecture structural of trigger_iserdes_channel is

  signal iserdes_q      : std_logic_vector(7 downto 0) := (others => '0');
  signal icascade       : std_logic;
  signal slave_shiftout : std_logic;

begin

  -- Instantiate the serdes primitive
  ----------------------------------
  -- declare the iserdes
  iserdes2_master : ISERDES2
   generic map (
     BITSLIP_ENABLE => FALSE,
     DATA_RATE      => "SDR",
     DATA_WIDTH     => CGN_RATIO,
     INTERFACE_TYPE => "RETIMED", --"NETWORKING_PIPELINED", --"NETWORKING",
     SERDES_MODE    => "MASTER")
   port map (
     Q1         => iserdes_q(3), -- 5. bit in time
     Q2         => iserdes_q(2), -- 6. bit in time
     Q3         => iserdes_q(1), -- 7. bit in time
     Q4         => iserdes_q(0), -- 8. bit in time
     SHIFTOUT   => icascade,
     INCDEC     => open,
     VALID      => open,
     BITSLIP    => '0',
     CE0        => '1',
     CLK0       => BIT_CLK_I,
     CLK1       => '0',
     CLKDIV     => DIV_CLK_I,
     D          => SERIAL_TRIGGER_I,
     IOCE       => STROBE_I,
     RST        => RESET_I,
     SHIFTIN    => slave_shiftout,
    -- unused connections
     FABRICOUT  => open,
     CFB0       => open,
     CFB1       => open,
     DFB        => open);
  
  iserdes2_slave : ISERDES2
   generic map (
     BITSLIP_ENABLE => FALSE,
     DATA_RATE      => "SDR",
     DATA_WIDTH     => CGN_RATIO,
     INTERFACE_TYPE => "RETIMED", --"NETWORKING_PIPELINED", --"NETWORKING",
     SERDES_MODE    => "SLAVE")
   port map (
    Q1         => iserdes_q(7), -- 1. bit in time
    Q2         => iserdes_q(6), -- 2. bit in time
    Q3         => iserdes_q(5), -- 3. bit in time
    Q4         => iserdes_q(4), -- 4. bit in time
    SHIFTOUT   => slave_shiftout,
    BITSLIP    => '0',
    CE0        => '1',
    CLK0       => BIT_CLK_I,
    CLK1       => '0',
    CLKDIV     => DIV_CLK_I,
    D          => '0',
    IOCE       => STROBE_I,
    RST        => RESET_I,
    SHIFTIN    => icascade,
    -- unused connections
    FABRICOUT  => open,
    CFB0       => open,
    CFB1       => open,
    DFB        => open);
  
  process(DIV_CLK_I)
  begin
    if rising_edge(DIV_CLK_I) then
      PARALLEL_TRIGGER_O <= iserdes_q(CGN_RATIO-1 downto 0);
    end if;
  end process;

end structural;

