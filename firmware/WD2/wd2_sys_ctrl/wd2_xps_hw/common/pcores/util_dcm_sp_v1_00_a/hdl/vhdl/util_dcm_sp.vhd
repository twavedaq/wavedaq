
library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

library Unisim;
use Unisim.vcomponents.all;

entity util_dcm_sp is
  generic (
    -- dcm_sp parameters
    CGN_CLKDV_DIVIDE          : real    := 2.0;
    CGN_CLKFX_DIVIDE          : integer := 1;
    CGN_CLKFX_MULTIPLY        : integer := 4;
    CGN_CLKIN_DIVIDE_BY_2     : boolean := false;
    CGN_CLKIN_PERIOD          : real    := 10.0;
    CGN_CLKOUT_PHASE_SHIFT    : string  := "NONE";
    CGN_CLK_FEEDBACK          : string  := "1X";
    CGN_DESKEW_ADJUST         : string  := "SYSTEM_SYNCHRONOUS";
    CGN_DLL_FREQUENCY_MODE    : string  := "LOW";
    CGN_DSS_MODE              : string  := "NONE";
    CGN_DUTY_CYCLE_CORRECTION : boolean := true;
    CGN_PHASE_SHIFT           : integer := 0;
    CGN_STARTUP_WAIT          : boolean := false;
    -- pcore specific paramters
    CGN_EXT_RESET_HIGH        : boolean := true;
    CGN_CLKIN_BUF             : boolean := false;
    CGN_CLK0_BUF              : boolean := false;
    CGN_CLK180_BUF            : boolean := false;
    CGN_CLK270_BUF            : boolean := false;
    CGN_CLK2X_BUF             : boolean := false;
    CGN_CLK2X180_BUF          : boolean := false;
    CGN_CLK90_BUF             : boolean := false;
    CGN_CLKDV_BUF             : boolean := false;
    CGN_CLKFX_BUF             : boolean := false;
    CGN_CLKFX180_BUF          : boolean := false
  );
  port (
    CLKIN_I          : in  std_logic;
    CLKFB_I          : in  std_logic;
    CLK0_O           : out std_logic;
    CLK90_O          : out std_logic;
    CLK180_O         : out std_logic;
    CLK270_O         : out std_logic;
    CLK2X_O          : out std_logic;
    CLK2X180_O       : out std_logic;
    CLKDV_O          : out std_logic;
    CLKFX_O          : out std_logic;
    CLKFX180_O       : out std_logic;
    CLK0_NOBUF_O     : out std_logic;
    CLK90_NOBUF_O    : out std_logic;
    CLK180_NOBUF_O   : out std_logic;
    CLK270_NOBUF_O   : out std_logic;
    CLK2X_NOBUF_O    : out std_logic;
    CLK2X180_NOBUF_O : out std_logic;
    CLKDV_NOBUF_O    : out std_logic;
    CLKFX_NOBUF_O    : out std_logic;
    CLKFX180_NOBUF_O : out std_logic;
    LOCKED_O         : out std_logic;
    PSDONE_O         : out std_logic;
    STATUS_O         : out std_logic_vector(7 downto 0);
    DSSEN_I          : in  std_logic;
    PSCLK_I          : in  std_logic;
    PSEN_I           : in  std_logic;
    PSINCDEC_I       : in  std_logic;
    RESET_I          : in  std_logic
  );
end util_dcm_sp;

architecture STRUCT of util_dcm_sp is

  signal rsti : std_logic;

  signal clk0_buf     : std_logic;
  signal clk180_buf   : std_logic;
  signal clk270_buf   : std_logic;
  signal clk2x_buf    : std_logic;
  signal clk2x180_buf : std_logic;
  signal clk90_buf    : std_logic;
  signal clkdv_buf    : std_logic;
  signal clkfx_buf    : std_logic;
  signal clkfx180_buf : std_logic;
  signal clkin_buf    : std_logic;

  function UpperCase_Char(char : character) return character is
  begin
    -- If char is not an upper case letter then return char
    if char < 'a' or char > 'z' then
      return char;
    end if;
    -- Otherwise map char to its corresponding lower case character and
    -- return that
    case char is
      when 'a'    => return 'A'; when 'b' => return 'B'; when 'c' => return 'C'; when 'd' => return 'D';
      when 'e'    => return 'E'; when 'f' => return 'F'; when 'g' => return 'G'; when 'h' => return 'H';
      when 'i'    => return 'I'; when 'j' => return 'J'; when 'k' => return 'K'; when 'l' => return 'L';
      when 'm'    => return 'M'; when 'n' => return 'N'; when 'o' => return 'O'; when 'p' => return 'P';
      when 'q'    => return 'Q'; when 'r' => return 'R'; when 's' => return 'S'; when 't' => return 'T';
      when 'u'    => return 'U'; when 'v' => return 'V'; when 'w' => return 'W'; when 'x' => return 'X';
      when 'y'    => return 'Y'; when 'z' => return 'Z';
      when others => return char;
    end case;
  end UpperCase_Char;

  function UpperCase_String (s : string) return string is
    variable res               : string(s'range);
  begin  -- function LoweerCase_String
    for I in s'range loop
      res(I) := UpperCase_Char(s(I));
    end loop;  -- I
    return res;
  end function UpperCase_String;

begin

  -----------------------------------------------------------------------------
  -- handle the reset
  -----------------------------------------------------------------------------
  Rst_is_Active_High : if (CGN_EXT_RESET_HIGH = true) generate
    rsti <= RESET_I;
  end generate Rst_is_Active_High;

  Rst_is_Active_Low : if (CGN_EXT_RESET_HIGH = false) generate
    rsti <= not RESET_I;
  end generate Rst_is_Active_Low;

  DCM_SP_inst : DCM_SP
  generic map (
    CLKDV_DIVIDE          => CGN_CLKDV_DIVIDE,
    CLKFX_DIVIDE          => CGN_CLKFX_DIVIDE,
    CLKFX_MULTIPLY        => CGN_CLKFX_MULTIPLY,
    CLKIN_DIVIDE_BY_2     => CGN_CLKIN_DIVIDE_BY_2,
    CLKIN_PERIOD          => CGN_CLKIN_PERIOD,
    CLKOUT_PHASE_SHIFT    => UpperCase_String(CGN_CLKOUT_PHASE_SHIFT),
    CLK_FEEDBACK          => UpperCase_String(CGN_CLK_FEEDBACK),
    DESKEW_ADJUST         => UpperCase_String(CGN_DESKEW_ADJUST),
    DLL_FREQUENCY_MODE    => UpperCase_String(CGN_DLL_FREQUENCY_MODE),
    DSS_MODE              => UpperCase_String(CGN_DSS_MODE),
    DUTY_CYCLE_CORRECTION => CGN_DUTY_CYCLE_CORRECTION,
    PHASE_SHIFT           => CGN_PHASE_SHIFT,
    STARTUP_WAIT          => CGN_STARTUP_WAIT
  )
  port map (
    CLK0     => clk0_buf,
    CLK180   => clk180_buf,
    CLK270   => clk270_buf,
    CLK2X    => clk2x_buf,
    CLK2X180 => clk2x180_buf,
    CLK90    => clk90_buf,
    CLKDV    => clkdv_buf,
    CLKFX    => clkfx_buf,
    CLKFX180 => clkfx180_buf,
    LOCKED   => LOCKED_O,
    PSDONE   => PSDONE_O,
    STATUS   => STATUS_O,
    CLKFB    => CLKFB_I,
    CLKIN    => clkin_buf,
    DSSEN    => DSSEN_I,
    PSCLK    => PSCLK_I,
    PSEN     => PSEN_I,
    PSINCDEC => PSINCDEC_I,
    RST      => rsti
  );
  
  -----------------------------------------------------------------------------
  -- Clkin
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKIN : if (CGN_CLKIN_BUF) generate
    CLKIN_BUFG_INST : BUFG
      port map (
        I => CLKIN_I,
        O => clkin_buf);
  end generate Using_BUFG_for_CLKIN;

  No_BUFG_for_CLKIN : if (not CGN_CLKIN_BUF) generate
    clkin_buf <= CLKIN_I;
  end generate No_BUFG_for_CLKIN;

  -----------------------------------------------------------------------------
  -- ClkOut0
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLK0 : if (CGN_CLK0_BUF) generate

    CLKOUT0_BUFG_INST : BUFG
      port map (
        I => clk0_buf,
        O => CLK0_O);
  end generate Using_BUFG_for_CLK0;

  No_BUFG_for_CLK0 : if (not CGN_CLK0_BUF) generate
    CLK0_O <= clk0_buf;
  end generate No_BUFG_for_CLK0;

  CLK0_NOBUF_O <= clk0_buf;

  -----------------------------------------------------------------------------
  -- Clk180
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLK180 : if (CGN_CLK180_BUF) generate

    CLKOUT1_BUFG_INST : BUFG
      port map (
        I => clk180_buf,
        O => CLK180_O);
  end generate Using_BUFG_for_CLK180;

  No_BUFG_for_CLK180 : if (not CGN_CLK180_BUF) generate
    CLK180_O <= clk180_buf;
  end generate No_BUFG_for_CLK180;

  CLK180_NOBUF_O <= clk180_buf;

  -----------------------------------------------------------------------------
  -- Clk270
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLK270 : if (CGN_CLK270_BUF) generate

    CLK270_BUFG_INST : BUFG
      port map (
        I => clk270_buf,
        O => CLK270_O);
  end generate Using_BUFG_for_CLK270;

  No_BUFG_for_CLK270 : if (not CGN_CLK270_BUF) generate
    CLK270_O <= clk270_buf;
  end generate No_BUFG_for_CLK270;

  CLK270_NOBUF_O <= clk270_buf;

  -----------------------------------------------------------------------------
  -- Clk2x
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLK2X : if (CGN_CLK2X_BUF) generate

    CLK2X_BUFG_INST : BUFG
      port map (
        I => clk2x_buf,
        O => CLK2X_O);
  end generate Using_BUFG_for_CLK2X;

  No_BUFG_for_CLK2X    : if (not CGN_CLK2X_BUF) generate
    CLK2X_O <= clk2x_buf;
  end generate No_BUFG_for_CLK2X;

  CLK2X_NOBUF_O <= clk2x_buf;

  -----------------------------------------------------------------------------
  -- Clk2x180
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLK2X180 : if (CGN_CLK2X180_BUF) generate

    CLK2X180_BUFG_INST : BUFG
      port map (
        I => clk2x180_buf,
        O => CLK2X180_O);
  end generate Using_BUFG_for_CLK2X180;

  No_BUFG_for_CLK2X180 : if (not CGN_CLK2X180_BUF) generate
    CLK2X180_O <= clk2x180_buf;
  end generate No_BUFG_for_CLK2X180;

  CLK2X180_NOBUF_O <= clk2x180_buf;

  -----------------------------------------------------------------------------
  -- Clk90
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLK90 : if (CGN_CLK90_BUF) generate

    CLK90_BUFG_INST : BUFG
      port map (
        I => clk90_buf,
        O => CLK90_O);
  end generate Using_BUFG_for_CLK90;

  No_BUFG_for_CLK90 : if (not CGN_CLK90_BUF) generate
    CLK90_O <= clk90_buf;
  end generate No_BUFG_for_CLK90;

  CLK90_NOBUF_O <= clk90_buf;

  -----------------------------------------------------------------------------
  -- Clkdv
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKDV : if (CGN_CLKDV_BUF) generate

    CLKDV_BUFG_INST : BUFG
      port map (
        I => clkdv_buf,
        O => CLKDV_O);
  end generate Using_BUFG_for_CLKDV;

  No_BUFG_for_CLKDV : if (not CGN_CLKDV_BUF) generate
    CLKDV_O <= clkdv_buf;
  end generate No_BUFG_for_CLKDV;

  CLKDV_NOBUF_O <= clkdv_buf;

  -----------------------------------------------------------------------------
  -- Clkfx
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKFX : if (CGN_CLKFX_BUF) generate

    CLKFX_BUFG_INST : BUFG
      port map (
        I => clkfx_buf,
        O => CLKFX_O);
  end generate Using_BUFG_for_CLKFX;

  No_BUFG_for_CLKFX : if (not CGN_CLKFX_BUF) generate
    CLKFX_O <= clkfx_buf;
  end generate No_BUFG_for_CLKFX;

  CLKFX_NOBUF_O <= clkfx_buf;

  -----------------------------------------------------------------------------
  -- Clkfx180
  -----------------------------------------------------------------------------
  Using_BUFG_for_CLKFX180 : if (CGN_CLKFX180_BUF) generate

    CLKFX180_BUFG_INST : BUFG
      port map (
        I => clkfx180_buf,
        O => CLKFX180_O);
  end generate Using_BUFG_for_CLKFX180;

  No_BUFG_for_CLKFX180 : if (not CGN_CLKFX180_BUF) generate
    CLKFX180_O <= clkfx180_buf;
  end generate No_BUFG_for_CLKFX180;

  CLKFX180_NOBUF_O <= clkfx180_buf;

end STRUCT;

-------------------------------------------------------------------------------
-- dcm module wrapper for clock generator
-------------------------------------------------------------------------------

