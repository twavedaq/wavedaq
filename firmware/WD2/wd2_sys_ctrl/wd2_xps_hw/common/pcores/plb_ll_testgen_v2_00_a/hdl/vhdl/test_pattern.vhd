--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : test_pattern.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.08.07
--
--  Description :  generate bytewise counter for 8, 16, 32, 64 bit wide output
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;



entity test_pattern is
  generic
  (
    CGN_DATA_WIDTH  : integer := 32
  );
  port
  (
    CLK_I    : in  std_logic;
    RD_EN_I  : in  std_logic;
    RST_I    : in  std_logic;
    SEL_12_I : in  std_logic;
    DATA_O   : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    VALID_O  : out std_logic
  );
end test_pattern;


architecture behavioral of test_pattern is

  function log2 (val: INTEGER) return natural is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function Log2;
  
  
  component counter_6x12
  port(
    CLK_I   : in std_logic;
    RD_EN_I : in std_logic;
    RST_I   : in std_logic;
    DATA_O  : out std_logic_vector(71 downto 0)
    );
  end component;


  component conv_72_64
  port
  (
    CLK_I    : in  std_logic;
    RST_I    : in  std_logic;
    D_I      : in  std_logic_vector(0 to 71);
    RD_EN_I  : in  std_logic;
    D_O      : out std_logic_vector(0 to 63);
    RD_SRC_O : out std_logic;
    VALID_O  : out std_logic
  );
  end component;


  constant C_NUM_BYTES : integer := CGN_DATA_WIDTH / 8;
  constant C_CTR_BIT   : integer := 8 - log2(C_NUM_BYTES);

  type data_byte_type is array (0 to C_NUM_BYTES-1) of std_logic_vector(7 downto 0);
  signal data_byte     : data_byte_type;
  signal data          : std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
  signal counter       : std_logic_vector(C_CTR_BIT-1 downto 0);


begin

  process(CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if (RST_I = '1') then
        counter  <=  (others => '0');
      elsif (RD_EN_I = '1') then
        counter  <=  counter + 1;
      end if;
    end if;
  end process;


  data_loop: for i in 0 to C_NUM_BYTES-1 generate 
    data_byte(i) <= counter & conv_std_logic_vector(i, 8-C_CTR_BIT) ;
    data((8*(C_NUM_BYTES-i)-1) downto (8*(C_NUM_BYTES-i-1))) <= data_byte(i);
  end generate;
  

  data_not_64_gen: if CGN_DATA_WIDTH /= 64 generate 
  begin
    DATA_O  <= data;
    VALID_O <= '1';
  end generate;

  data_64_gen: if CGN_DATA_WIDTH = 64 generate 
    signal rden_counter             : std_logic;
    signal counter_6x12_dat         : std_logic_vector(71 downto 0);
    signal data_conv_72_64          : std_logic_vector(63 downto 0);
    signal data_conv_72_64_swapped  : std_logic_vector(63 downto 0);
  begin

    counter_6x12_inst: counter_6x12
    PORT MAP
    (
      CLK_I   => CLK_I,
      RD_EN_I => rden_counter,
      RST_I   => RST_I,
      DATA_O  => counter_6x12_dat
    );

    conv_72_64_inst: conv_72_64
    PORT MAP
    (
      CLK_I    => CLK_I,
      RST_I    => RST_I,
      D_I      => counter_6x12_dat,
      D_O      => data_conv_72_64,
      RD_EN_I  => RD_EN_I,
      RD_SRC_O => rden_counter,
      VALID_O  => VALID_O
    );

    swap_loop: for i in 0 to C_NUM_BYTES-1 generate 
      data_conv_72_64_swapped((8*(C_NUM_BYTES-i)-1) downto (8*(C_NUM_BYTES-i-1))) <=  data_conv_72_64((8*i+7) downto (8*i) );
    end generate;

    DATA_O <= data_conv_72_64_swapped when (SEL_12_I = '1') else data;

  end generate;

end Behavioral;
