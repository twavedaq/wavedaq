------------------------------------------------------------------------------
-- user_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          user_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Mon Sep 12 09:32:02 2011 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

--library proc_common_v3_00_a;
--use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here
library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

library plb_ll_testgen_v2_00_a;
use plb_ll_testgen_v2_00_a.ll_testgen;

------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
-- Definition of Generics:
--   C_SLV_DWIDTH                 -- Slave interface data bus width
--   C_NUM_REG                    -- Number of software accessible registers
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity user_logic is
  generic
  (
    -- ADD USER GENERICS BELOW THIS LINE ---------------
    --USER generics added here
    -- ADD USER GENERICS ABOVE THIS LINE ---------------
    CGN_IFG_COUNT_WIDTH           : integer := 16;
    CGN_TOTAL_FRAMES_COUNT_WIDTH  : integer := 32;
    CGN_FRAMES_PER_HDR_TBL_WIDTH  : integer := 16;
    CGN_DATA_LENGTH_WIDTH         : integer := 16;
    CGN_TESTGEN_HEADER_LEN_BYTE   : integer :=  8;
    
    CGN_LL_DATA_WIDTH             : integer := 32;
    CGN_LL_REM_WIDTH              : integer :=  2;
    CGN_FRAME_LENGTH_WIDTH        : integer := 14;
    CGN_IP_HEADER_SEL_WIDTH       : integer :=  6;
    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol parameters, do not add to or delete
    C_SLV_DWIDTH            : integer := 32;
    C_NUM_REG               : integer :=  8
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    LL_CLK_I             : in   std_logic;
    LL_TX_DATA_O         : out  std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_TX_SOF_N_O        : out  std_logic;
    LL_TX_EOF_N_O        : out  std_logic;
    LL_TX_REM_O          : out  std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_TX_SRC_RDY_N_O    : out  std_logic;
    LL_TX_DST_RDY_N_I    : in   std_logic;
    FRAME_LENGTH_O       : out  std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
    IP_HEADER_SEL_O      : out  std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);

    -- ADD USER PORTS ABOVE THIS LINE ------------------

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );


end entity user_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of user_logic is

  --USER signal declarations added here, as needed for user logic

  ------------------------------------------------
  -- bit length of counters
  ------------------------------------------------


  ------------------------------------------
  -- Signals for user logic slave model s/w accessible register example
  ------------------------------------------
  signal slv_reg0            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg1            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg2            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg3            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg4            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg5            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg6            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg7            : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_reg_write_sel   : std_logic_vector(0 to 7);
  signal slv_reg_read_sel    : std_logic_vector(0 to 7);
  signal slv_ip2bus_data     : std_logic_vector(0 to C_SLV_DWIDTH-1);
  signal slv_read_ack        : std_logic;
  signal slv_write_ack       : std_logic;


  signal reg_ctrl            : std_logic_vector(31 downto 0);
  signal reg_header_id          : std_logic_vector(31 downto 0);
  signal reg_ifg_count       : std_logic_vector(CGN_IFG_COUNT_WIDTH-1 downto 0);
  signal reg_total_frames     : std_logic_vector(CGN_TOTAL_FRAMES_COUNT_WIDTH-1 downto 0);
  signal reg_frames_per_ip_header   : std_logic_vector(CGN_FRAMES_PER_HDR_TBL_WIDTH-1 downto 0);
  signal reg_data_length_byte       : std_logic_vector(CGN_DATA_LENGTH_WIDTH-1 downto 0);
  signal reg_num_of_ip_headers      : std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
  signal reg_status          : std_logic_vector(31 downto 0);
  signal reg_status_busclk   : std_logic_vector(31 downto 0);

begin

 --USER logic implementation added here

  ------------------------------------------
  -- Example code to read/write user logic slave model s/w accessible registers
  --
  -- Note:
  -- The example code presented here is to show you one way of reading/writing
  -- software accessible registers implemented in the user logic slave model.
  -- Each bit of the Bus2IP_WrCE/Bus2IP_RdCE signals is configured to correspond
  -- to one software accessible register by the top level template. For example,
  -- if you have four 32 bit software accessible registers in the user logic,
  -- you are basically operating on the following memory mapped registers:
  --
  --    Bus2IP_WrCE/Bus2IP_RdCE   Memory Mapped Register
  --                     "1000"   C_BASEADDR + 0x0
  --                     "0100"   C_BASEADDR + 0x4
  --                     "0010"   C_BASEADDR + 0x8
  --                     "0001"   C_BASEADDR + 0xC
  --
  ------------------------------------------
  slv_reg_write_sel <= Bus2IP_WrCE(0 to 7);
  slv_reg_read_sel  <= Bus2IP_RdCE(0 to 7);
  slv_write_ack     <= Bus2IP_WrCE(0) or Bus2IP_WrCE(1) or Bus2IP_WrCE(2) or Bus2IP_WrCE(3) or Bus2IP_WrCE(4) or Bus2IP_WrCE(5) or Bus2IP_WrCE(6) or Bus2IP_WrCE(7);
  slv_read_ack      <= Bus2IP_RdCE(0) or Bus2IP_RdCE(1) or Bus2IP_RdCE(2) or Bus2IP_RdCE(3) or Bus2IP_RdCE(4) or Bus2IP_RdCE(5) or Bus2IP_RdCE(6) or Bus2IP_RdCE(7);

  -- implement slave model software accessible register(s)
  SLAVE_REG_WRITE_PROC : process( Bus2IP_Clk ) is
  begin

    if Bus2IP_Clk'event and Bus2IP_Clk = '1' then
      if Bus2IP_Reset = '1' then
        slv_reg0 <= (others => '0');
        slv_reg1 <= (others => '0');
        slv_reg2 <= (others => '0');
        slv_reg3 <= (others => '0');
        slv_reg4 <= (others => '0');
        slv_reg5 <= (others => '0');
        slv_reg6 <= (others => '0');
--        slv_reg7 <= (others => '0');
      else
        case slv_reg_write_sel is
          when "10000000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg0(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "01000000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg1(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00100000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg2(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00010000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg3(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00001000" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg4(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00000100" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg5(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
          when "00000010" =>
            for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
              if ( Bus2IP_BE(byte_index) = '1' ) then
                slv_reg6(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
              end if;
            end loop;
 --         when "00000001" =>
 --           for byte_index in 0 to (C_SLV_DWIDTH/8)-1 loop
 --             if ( Bus2IP_BE(byte_index) = '1' ) then
 --               slv_reg7(byte_index*8 to byte_index*8+7) <= Bus2IP_Data(byte_index*8 to byte_index*8+7);
 --             end if;
 --           end loop;
          when others => null;
        end case;
      end if;
    end if;

  end process SLAVE_REG_WRITE_PROC;

  -- implement slave model software accessible register(s) read mux
  SLAVE_REG_READ_PROC : process( slv_reg_read_sel, slv_reg0, slv_reg1, slv_reg2, slv_reg3, slv_reg4, slv_reg5, slv_reg6, slv_reg7 ) is
  begin

    case slv_reg_read_sel is
      when "10000000" => slv_ip2bus_data <= slv_reg0;
      when "01000000" => slv_ip2bus_data <= slv_reg1;
      when "00100000" => slv_ip2bus_data <= slv_reg2;
      when "00010000" => slv_ip2bus_data <= slv_reg3;
      when "00001000" => slv_ip2bus_data <= slv_reg4;
      when "00000100" => slv_ip2bus_data <= slv_reg5;
      when "00000010" => slv_ip2bus_data <= slv_reg6;
      when "00000001" => slv_ip2bus_data <= slv_reg7;
      when others => slv_ip2bus_data <= (others => '0');
    end case;

  end process SLAVE_REG_READ_PROC;

  ------------------------------------------
  -- Example code to drive IP to Bus signals
  ------------------------------------------
  IP2Bus_Data  <= slv_ip2bus_data when slv_read_ack = '1' else
                  (others => '0');

  IP2Bus_WrAck <= slv_write_ack;
  IP2Bus_RdAck <= slv_read_ack;
  IP2Bus_Error <= '0';


  -----------------------------------------------------------------------------
  -- register usage:
  -----------------------------------------------------------------------------
  --
  -- slv_reg0 : reg_ctrl
  -- slv_reg1 : reg_header_id
  -- slv_reg2 : reg_ifg_count
  -- slv_reg3 : reg_total_frames
  -- slv_reg4 : reg_frames_per_ip_header -- packets per server num (port)
  -- slv_reg5 : reg_data_length_byte
  -- slv_reg6 : reg_num_of_ip_headers
  -- slv_reg7 : reg_status
  -----------------------------------------------------------------------------

  cdc_sync_reg_ctrl: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  32,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  2,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg0,
       PORT_B_O => reg_ctrl
    );

  --------------------------------------------------------------------------

  cdc_sync_reg_header: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  32,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg1,
       PORT_B_O => reg_header_id
    );

  --------------------------------------------------------------------------

  cdc_sync_reg_count_ifg: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  CGN_IFG_COUNT_WIDTH,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg2(32-CGN_IFG_COUNT_WIDTH to 31),
       PORT_B_O => reg_ifg_count
    );

  --------------------------------------------------------------------------

  cdc_sync_reg_count_total: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  CGN_TOTAL_FRAMES_COUNT_WIDTH,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg3(32-CGN_TOTAL_FRAMES_COUNT_WIDTH to 31),
       PORT_B_O => reg_total_frames
    );

  --------------------------------------------------------------------------
  
  cdc_sync_us_reg_loop_serv_num: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  CGN_FRAMES_PER_HDR_TBL_WIDTH,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg4(32-CGN_FRAMES_PER_HDR_TBL_WIDTH to 31),
       PORT_B_O => reg_frames_per_ip_header
    );

  --------------------------------------------------------------------------

  cdc_sync_us_reg_data_loop: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  CGN_DATA_LENGTH_WIDTH,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg5(32-CGN_DATA_LENGTH_WIDTH to 31),
       PORT_B_O => reg_data_length_byte
    );

  --------------------------------------------------------------------------

  cdc_sync_us_reg_server_num : cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  CGN_IP_HEADER_SEL_WIDTH,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0
     )
    PORT MAP
    (
       CLK_A_I => Bus2IP_Clk,
       CLK_B_I => LL_CLK_I,

       PORT_A_I => slv_reg6(32-CGN_IP_HEADER_SEL_WIDTH to 31),
       PORT_B_O => reg_num_of_ip_headers
    );

  --------------------------------------------------------------------------

  cdc_sync_running: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  1,
       CGN_USE_INPUT_REG_A      =>  0,
       CGN_USE_OUTPUT_REG_A     =>  0,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  1,
       CGN_NUM_OUTPUT_REGS_B    =>  0,
       CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
     )
    PORT MAP
    (
       CLK_A_I => LL_CLK_I,
       CLK_B_I => Bus2IP_Clk,

       PORT_A_I(0) => reg_status(31),
       PORT_B_O(0) => reg_status_busclk(31)
    );


  cdc_sync_reg_status: cdc_sync
     generic map
     (
       CGN_DATA_WIDTH           =>  31,
       CGN_USE_INPUT_REG_A      =>  1,
       CGN_USE_OUTPUT_REG_A     =>  1,
       CGN_USE_GRAY_CONVERSION  =>  0,
       CGN_NUM_SYNC_REGS_B      =>  2,
       CGN_NUM_OUTPUT_REGS_B    =>  1,
       CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
     )
    PORT MAP
    (
       CLK_A_I => LL_CLK_I,
       CLK_B_I => Bus2IP_Clk,

       PORT_A_I => reg_status(30 downto 0),
       PORT_B_O => reg_status_busclk(30 downto 0)
    );

  slv_reg7 <= reg_status_busclk;

  --------------------------------------------------------------------------

  ll_testgen_inst: entity plb_ll_testgen_v2_00_a.ll_testgen
  generic map
  (
    CGN_IFG_COUNT_WIDTH            => CGN_IFG_COUNT_WIDTH,
    CGN_TOTAL_FRAMES_COUNT_WIDTH   => CGN_TOTAL_FRAMES_COUNT_WIDTH,
    CGN_FRAMES_PER_HDR_TBL_WIDTH   => CGN_FRAMES_PER_HDR_TBL_WIDTH,
    CGN_DATA_LENGTH_WIDTH          => CGN_DATA_LENGTH_WIDTH,
    CGN_TESTGEN_HEADER_LEN_BYTE    => CGN_TESTGEN_HEADER_LEN_BYTE,

    CGN_LL_DATA_WIDTH              => CGN_LL_DATA_WIDTH,
    CGN_LL_REM_WIDTH               => CGN_LL_REM_WIDTH,
    CGN_FRAME_LENGTH_WIDTH         => CGN_FRAME_LENGTH_WIDTH,
    CGN_IP_HEADER_SEL_WIDTH        => CGN_IP_HEADER_SEL_WIDTH
  )
  port map
  (
    -- input register
    REG_CTRL_I                 => reg_ctrl,           
    REG_HEADER_ID_I            => reg_header_id,         
    REG_IFG_COUNT_I            => reg_ifg_count,      
    REG_TOTAL_FRAMES_I         => reg_total_frames,    
    REG_FRAMES_PER_IP_HEADER_I => reg_frames_per_ip_header,  
    REG_DATA_LENGTH_BYTE_I     => reg_data_length_byte,      
    REG_NUM_OF_IP_HEADERS_I    => reg_num_of_ip_headers,     

    -- output register
    REG_STATUS_O               => reg_status,
                               
    -- local link              
    LL_CLK_I                   => LL_CLK_I,           
    LL_TX_DATA_O               => LL_TX_DATA_O,       
    LL_TX_SOF_N_O              => LL_TX_SOF_N_O,      
    LL_TX_EOF_N_O              => LL_TX_EOF_N_O,      
    LL_TX_REM_O                => LL_TX_REM_O,        
    LL_TX_SRC_RDY_N_O          => LL_TX_SRC_RDY_N_O,  
    LL_TX_DST_RDY_N_I          => LL_TX_DST_RDY_N_I,  
    FRAME_LENGTH_O             => FRAME_LENGTH_O,     
    IP_HEADER_SEL_O            => IP_HEADER_SEL_O       
  );

end IMP;

