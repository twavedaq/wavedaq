--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_testgen.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.08.07
--
--  Description :
--    add a predefined udp header to data stream
--    ip checksum has to be precalculated at offset 24 and 25
--    in header table
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library plb_ll_testgen_v2_00_a;
use plb_ll_testgen_v2_00_a.test_pattern;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_testgen is
  generic
  (
    CGN_IFG_COUNT_WIDTH           : integer := 16;
    CGN_TOTAL_FRAMES_COUNT_WIDTH  : integer := 32;
    CGN_FRAMES_PER_HDR_TBL_WIDTH  : integer := 16;
    CGN_DATA_LENGTH_WIDTH         : integer := 16;
    CGN_TESTGEN_HEADER_LEN_BYTE   : integer :=  8;
    
    CGN_LL_DATA_WIDTH             : integer := 32;
    CGN_LL_REM_WIDTH              : integer :=  2;
    CGN_FRAME_LENGTH_WIDTH        : integer := 14;
    CGN_IP_HEADER_SEL_WIDTH       : integer :=  6
  );
  port
  (
    -- input register
    REG_CTRL_I                  : in  std_logic_vector(31 downto 0);
    REG_HEADER_ID_I             : in  std_logic_vector(31 downto 0);
    REG_DATA_LENGTH_BYTE_I      : in  std_logic_vector(CGN_DATA_LENGTH_WIDTH-1 downto 0);
    REG_TOTAL_FRAMES_I          : in  std_logic_vector(CGN_TOTAL_FRAMES_COUNT_WIDTH-1 downto 0);
    REG_IFG_COUNT_I             : in  std_logic_vector(CGN_IFG_COUNT_WIDTH-1 downto 0);
    REG_NUM_OF_IP_HEADERS_I     : in  std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
    REG_FRAMES_PER_IP_HEADER_I  : in  std_logic_vector(CGN_FRAMES_PER_HDR_TBL_WIDTH-1 downto 0);

    -- output register
    REG_STATUS_O                : out std_logic_vector(31 downto 0);

    -- local link
    LL_CLK_I                    : in  std_logic;
    LL_TX_DATA_O                : out std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
    LL_TX_SOF_N_O               : out std_logic;
    LL_TX_EOF_N_O               : out std_logic;
    LL_TX_REM_O                 : out std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
    LL_TX_SRC_RDY_N_O           : out std_logic;
    LL_TX_DST_RDY_N_I           : in  std_logic;
    FRAME_LENGTH_O              : out std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
    IP_HEADER_SEL_O             : out std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0)
  );

end entity ll_testgen;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of ll_testgen is

  function log2 (val: INTEGER) return natural is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function Log2;

  function max(a, b: integer) return integer is
  begin
    if (a > b) then return a; else return b; end if;
  end;
  


  constant C_HEADER_ENTRIES       : integer := (CGN_TESTGEN_HEADER_LEN_BYTE * 8) / CGN_LL_DATA_WIDTH;
  constant C_HEADER_ENTRIES_WIDTH : integer := max(log2(C_HEADER_ENTRIES), 1);

  constant C_BIT_REM              : integer := log2(CGN_LL_DATA_WIDTH / 8);

  type header_type is array (0 to C_HEADER_ENTRIES-1) of std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);

  signal header  : header_type := (others => (others => '1'));

  signal header_loop          : std_logic_vector(C_HEADER_ENTRIES_WIDTH-1 downto 0):= (others => '0');



  ------------------------------------------------
  -- bit length of counters
  ------------------------------------------------



  signal ll_tx_d                 : std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
  signal ll_tx_rem               : std_logic_vector(CGN_LL_REM_WIDTH-1 downto 0);
  signal ll_tx_sof_n             : std_logic;
  signal ll_tx_eof_n             : std_logic;
  signal ll_tx_src_rdy_n         : std_logic;
--  signal ll_tx_dst_rdy_n       : std_logic;

-- test pattern
  signal data                    : std_logic_vector(CGN_LL_DATA_WIDTH-1 downto 0);
  signal data_rd_en              : std_logic;
  signal test_pattern_rst        : std_logic;
  signal test_pattern_sel_12     : std_logic;

  signal ifg_count               : std_logic_vector(CGN_IFG_COUNT_WIDTH-1 downto 0);
  signal total_frame_count       : std_logic_vector(CGN_TOTAL_FRAMES_COUNT_WIDTH-1 downto 0);
  signal loop_serv_num           : std_logic_vector(CGN_FRAMES_PER_HDR_TBL_WIDTH-1 downto 0);
  signal data_len_count          : std_logic_vector(CGN_DATA_LENGTH_WIDTH-C_BIT_REM-1 downto 0);
  signal ip_header_sel           : std_logic_vector(CGN_IP_HEADER_SEL_WIDTH-1 downto 0);
  signal frame_num               : std_logic_vector(31 downto 0);

  signal running                 : std_logic;
  signal ctrl_reset              : std_logic;
  signal ctrl_start              : std_logic;
  signal ctrl_start_r            : std_logic;
  signal ctrl_stop               : std_logic;


  type states is (S_IDLE, S_SOF, S_HDR, S_DATA, S_EOF, S_SERV_NUM, S_TOTAL_CNT, S_IFG);
  signal state : states;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";

begin

  REG_STATUS_O(31) <= running;

  REG_STATUS_O(30 downto 0) <= frame_num(30 downto 0);

  ctrl_reset          <= REG_CTRL_I(0);
  ctrl_stop           <= REG_CTRL_I(1);
  ctrl_start          <= REG_CTRL_I(2);
  test_pattern_sel_12 <= REG_CTRL_I(3);

----------------------------------------
-- Test_pattern
----------------------------------------

  test_pattern_inst : entity plb_ll_testgen_v2_00_a.test_pattern
  generic map 
  (
    CGN_DATA_WIDTH => CGN_LL_DATA_WIDTH
  )
  port map
  (
    CLK_I    => LL_CLK_I,
    RD_EN_I  => data_rd_en,
    RST_I    => test_pattern_rst,
    SEL_12_I => test_pattern_sel_12,
    DATA_O   => data,
    VALID_O  => open
  );

----------------------------------------

----------------------------------------
-- Header
----------------------------------------

  hdr_dw8_gen: if CGN_LL_DATA_WIDTH = 8 generate 
  begin
    header(0) <= REG_HEADER_ID_I(31 downto 24);
    header(1) <= REG_HEADER_ID_I(23 downto 16);
    header(2) <= REG_HEADER_ID_I(15 downto  8);
    header(3) <= REG_HEADER_ID_I( 7 downto  0);
    header(4) <= frame_num(31 downto 24);
    header(5) <= frame_num(23 downto 16);
    header(6) <= frame_num(15 downto  8);
    header(7) <= frame_num( 7 downto  0);
    hdr_loop: for i in 8 to C_HEADER_ENTRIES-1 generate 
      header(i) <= (others => '1');
    end generate;
  end generate;


  hdr_dw16_gen: if CGN_LL_DATA_WIDTH = 16 generate 
  begin
    header(0) <= REG_HEADER_ID_I(31 downto 16);
    header(1) <= REG_HEADER_ID_I(15 downto  0);
    header(2) <= frame_num(31 downto 16);
    header(3) <= frame_num(15 downto  0);
    hdr_loop: for i in 4 to C_HEADER_ENTRIES-1 generate 
      header(i) <= (others => '1');
    end generate;
  end generate;


  hdr_dw32_gen: if CGN_LL_DATA_WIDTH = 32 generate 
  begin
    header(0) <= REG_HEADER_ID_I;
    header(1) <= frame_num;
    hdr_loop: for i in 2 to C_HEADER_ENTRIES-1 generate 
      header(i) <= (others => '1');
    end generate;
  end generate;

  hdr_dw64_gen: if CGN_LL_DATA_WIDTH = 64 generate 
  begin
    header(0) <= REG_HEADER_ID_I & frame_num;
    hdr_loop: for i in 1 to C_HEADER_ENTRIES-1 generate 
      header(i) <= (others => '1');
    end generate;
  end generate;

  hdr_dw128_gen: if CGN_LL_DATA_WIDTH = 128 generate 
  begin
    header(0) <= REG_HEADER_ID_I & frame_num & x"ffffffffffffffff";
    hdr_loop: for i in 1 to C_HEADER_ENTRIES-1 generate 
      header(i) <= (others => '1');
    end generate;
  end generate;


  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      ctrl_start_r <= ctrl_start;
    end if;
  end process;


----------------------------------------
-- State Machine
----------------------------------------

  fsm: process (LL_CLK_I) is
  begin
    if rising_edge(LL_CLK_I) then
      -- default assignment
      test_pattern_rst <= '0';

      if ctrl_reset = '1' then
        state <= S_IDLE;
        ll_tx_rem        <= (others => '1');
        ll_tx_sof_n      <= '1';
        ll_tx_eof_n      <= '1';
        ll_tx_src_rdy_n  <= '1';
        test_pattern_rst <= '1';
        running <= '0';
      else
        case state is
--------------------------------------------------------------------------------
          when S_IDLE =>      ll_tx_rem         <= (others => '1');
                              ll_tx_sof_n       <= '1';
                              ll_tx_eof_n       <= '1';
                              ll_tx_src_rdy_n   <= '1';
                              running           <= '0';
                              total_frame_count <= REG_TOTAL_FRAMES_I;
                              loop_serv_num     <= REG_FRAMES_PER_IP_HEADER_I;
                              ip_header_sel     <= (others => '0');
                              frame_num         <= (others => '0');
                              header_loop       <= (others => '0');
                              if (ctrl_start = '1') and (ctrl_start_r = '0') then  -- rising edge of ctrl_start
                                running <= '1';
                                state   <= S_SOF;
                              end if;
--------------------------------------------------------------------------------

          when S_SOF =>
                              data_len_count   <= REG_DATA_LENGTH_BYTE_I(CGN_DATA_LENGTH_WIDTH-1 downto C_BIT_REM);
                              ifg_count        <= REG_IFG_COUNT_I;
                              ll_tx_sof_n      <= '0';
                              ll_tx_src_rdy_n  <= '0';
                              ll_tx_d          <= header(conv_integer(header_loop));
                              header_loop      <= header_loop + 1;
                              if (header_loop < (C_HEADER_ENTRIES-1)) then
                                state <= S_HDR;
                              else
                                state <= S_DATA;
                              end if;
                                
--------------------------------------------------------------------------------

          when S_HDR =>       if LL_TX_DST_RDY_N_I = '0' then
                                ll_tx_sof_n  <= '1';
                                ll_tx_d     <= header(conv_integer(header_loop));
                                header_loop <= header_loop + 1;

                                if (header_loop < (C_HEADER_ENTRIES-1)) then
                                  state       <= S_HDR;
                                else
                                  state <= S_DATA;
                                end if;

                              end if;

--------------------------------------------------------------------------------

          when S_DATA =>      if LL_TX_DST_RDY_N_I = '0' then
                                ll_tx_sof_n  <= '1';
                                ll_tx_d   <= data;
                                if (data_len_count > 0) then
                                  data_len_count <= data_len_count - 1 ;
                                  state <= S_DATA;
                                else
                                  ll_tx_rem   <= REG_DATA_LENGTH_BYTE_I(CGN_LL_REM_WIDTH-1 downto 0);
                                  ll_tx_eof_n <= '0';
                                  state <= S_EOF;
                                end if;

                              end if;

--------------------------------------------------------------------------------
          when S_EOF =>       if LL_TX_DST_RDY_N_I = '0' then
                                ll_tx_eof_n      <= '1';
                                ll_tx_src_rdy_n  <= '1';
                                ll_tx_rem        <= (others => '1');
                                state <= S_SERV_NUM;
                              end if;
                              test_pattern_rst <= '1';

--------------------------------------------------------------------------------
          when S_SERV_NUM =>  if loop_serv_num > 0 then
                                loop_serv_num <= loop_serv_num -1;
                              else
                                loop_serv_num <= REG_FRAMES_PER_IP_HEADER_I;
                                if ip_header_sel < REG_NUM_OF_IP_HEADERS_I then
                                  ip_header_sel <= ip_header_sel + 1;
                                else
                                  ip_header_sel <= (others => '0');
                                end if;
                              end if;
                              state <= S_TOTAL_CNT;

--------------------------------------------------------------------------------
          when S_TOTAL_CNT => if total_frame_count > 0 then
                                total_frame_count <= total_frame_count -1;
                                frame_num  <= frame_num + 1;
                                state <= S_IFG;
                              else
                                state <= S_IDLE;
                              end if;

--------------------------------------------------------------------------------
          when S_IFG =>
                              header_loop     <= (others => '0');
                              if ctrl_stop = '1' then
                                state <= S_IDLE;
                              elsif ifg_count > 0 then
                                ifg_count <= ifg_count - 1;
                                state <= S_IFG;
                              else
                                state <= S_SOF;
                              end if;

--------------------------------------------------------------------------------
        end case;

      end if;
    end if;
  end process fsm;
--------------------------------------------------------------------------------

  data_rd_en <= '1' when ((state = S_DATA) and (LL_TX_DST_RDY_N_I = '0')) else '0';

--  data_rd_en <= '1' when (    ((state = S_HDR)  and (header_loop = (C_HEADER_ENTRIES-1)) and (LL_TX_DST_RDY_N_I = '0'))
--                           or ((state = S_DATA) and (LL_TX_DST_RDY_N_I = '0'))) else '0';

  LL_TX_DATA_O         <= ll_tx_d;
  LL_TX_REM_O          <= ll_tx_rem;
  LL_TX_SOF_N_O        <= ll_tx_sof_n;
  LL_TX_EOF_N_O        <= ll_tx_eof_n;
  LL_TX_SRC_RDY_N_O    <= ll_tx_src_rdy_n;
  IP_HEADER_SEL_O      <= ip_header_sel;

-- use reg
  FRAME_LENGTH_O       <= REG_DATA_LENGTH_BYTE_I(CGN_FRAME_LENGTH_WIDTH-1 downto 0) + 1 + CGN_TESTGEN_HEADER_LEN_BYTE;

end IMP;

