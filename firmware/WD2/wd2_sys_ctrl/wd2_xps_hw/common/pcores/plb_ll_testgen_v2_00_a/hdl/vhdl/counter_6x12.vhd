----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    12:29:11 09/16/2011
-- Design Name:
-- Module Name:    conv_72_64 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity counter_6x12 is
port
(
  CLK_I   : in   std_logic;
  RD_EN_I : in   std_logic;
  RST_I   : in   std_logic;
  DATA_O  : out  std_logic_vector (71 downto 0)
);
end counter_6x12;


architecture behavioral of counter_6x12 is

signal counter_0 : std_logic_vector (11 downto 0);
signal counter_1 : std_logic_vector (11 downto 0);
signal counter_2 : std_logic_vector (11 downto 0);
signal counter_3 : std_logic_vector (11 downto 0);
signal counter_4 : std_logic_vector (11 downto 0);
signal counter_5 : std_logic_vector (11 downto 0);

begin

  process(CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if (RST_I = '1') then
        counter_0 <= "000000000000";
        counter_1 <= "000000000001";
        counter_2 <= "000000000010";
        counter_3 <= "000000000011";
        counter_4 <= "000000000100";
        counter_5 <= "000000000101";
      elsif (RD_EN_I = '1') then
        counter_0 <= counter_0 + 6;
        counter_1 <= counter_1 + 6;
        counter_2 <= counter_2 + 6;
        counter_3 <= counter_3 + 6;
        counter_4 <= counter_4 + 6;
        counter_5 <= counter_5 + 6;
      end if;
    end if;
  end process;

  DATA_O <= counter_5 & counter_4 & counter_3 & counter_2 & counter_1 & counter_0;

end Behavioral;

