----------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:    12:29:11 09/16/2011
-- Design Name:
-- Module Name:    conv_72_64 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool versions:
-- Description:
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
----------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_unsigned.all;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity conv_72_64 is
generic
(
  CGN_USE_SHIFTREG : integer := 0
);
port
(
  CLK_I    : in   std_logic;
  RST_I    : in   std_logic;
  D_I      : in   std_logic_vector (0 to 71);
  D_O      : out  std_logic_vector (0 to 63);
  RD_EN_I  : in   std_logic;
  RD_SRC_O : out  std_logic;
  VALID_O  : out  std_logic
);
end conv_72_64;

architecture Behavioral of conv_72_64 is
  signal dc : std_logic_vector (0 to 71);
  signal dl:  std_logic_vector (0 to 63);
  signal data_out : std_logic_vector (0 to 63);
  signal valid    :  std_logic_vector (0 to 1);
  signal step_dly :  std_logic;
  signal step : std_logic_vector (3 downto 0);
  signal enable :  std_logic;
  signal rd_src_enable :  std_logic;


begin

  process(CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if (RST_I = '1') then
        step <= "0000";
        step_dly  <= '0';
      elsif (enable = '1') then
          step_dly <= step(3);
        if (step(3) = '0') then  -- step < 8
          step <= step + 1;
        else
          step <= "0000";
        end if;
      end if;
    end if;
  end process;

  process(CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if (RST_I = '1') then
        valid     <= "00";
      else
        valid(0) <='1';
        valid(1) <= valid(0);
      end if;
    end if;
  end process;


  enable <= not valid(1) or RD_EN_I;
  rd_src_enable <= (not valid(1) or RD_EN_I) and not step(3);

  gen_shiftreg: if CGN_USE_SHIFTREG > 0 generate
  begin
    process(CLK_I) is
    begin
      if rising_edge(CLK_I) then
        if enable = '1' then
          dc  <= D_I;
          D_O <= data_out;
          dl( 0 to  7) <= D_I(4 to 11);
          dl( 8 to 15) <= dl( 0 to  7);
          dl(16 to 23) <= dl( 8 to 15);
          dl(24 to 31) <= dl(16 to 23);
          dl(32 to 39) <= dl(24 to 31);
          dl(40 to 47) <= dl(32 to 39);
          dl(48 to 55) <= dl(40 to 47);
          dl(56 to 63) <= dl(48 to 55);
        end if;
      end if;
    end process;
  end generate;

  gen_no_shiftreg: if CGN_USE_SHIFTREG = 0 generate
  begin
    process(CLK_I) is
    begin
      if rising_edge(clk_I) then
        if enable = '1' then
          dc  <= D_I;
          D_O <= data_out;

          case (step) is
            when "0111" =>
              dl(0 to  7)  <= D_I(4 to 11);
            when "0110" =>
              dl(8 to 15)  <= D_I(4 to 11);
            when "0101" =>
              dl(16 to 23) <= D_I(4 to 11);
            when "0100" =>
              dl(24 to 31) <= D_I(4 to 11);
            when "0011" =>
              dl(32 to 39) <= D_I(4 to 11);
            when "0010" =>
              dl(40 to 47) <= D_I(4 to 11);
            when "0001" =>
              dl(48 to 55) <= D_I(4 to 11);
            when "0000" =>
              dl(56 to 63) <= D_I(4 to 11);
            when others =>
               null;
          end case;

        end if;
      end if;
    end process;
  end generate;

  data_out <= (dc(0 to 3) & dc(12 to 71)) when (step_dly = '0') else dl(0 to 63);

  RD_SRC_O <= rd_src_enable;
  VALID_O  <= valid(1);


end Behavioral;

