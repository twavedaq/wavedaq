--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll8_udp_gen.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.08.06
--
--  Description :
--    add a predefined udp header to data stream
--    ip checksum has to be precalculated at offset 24 and 25
--    in header table
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


--library ll8_udp_gen_v1_00_a;
--use ll8_udp_gen_v1_00_a.all;

--library psi_3205_v1_00_a;
--use psi_3205_v1_00_a.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll8_udp_gen is
  generic
  (
    CGN_FRAME_LENGTH_WIDTH         : integer := 14;
    CGN_HEADER_SEL_WIDTH           : integer := 6;
    CGN_HEADER_LENGTH_WIDTH        : integer := 6
  );
  PORT
  (
    CLK_I                   : in  std_logic;
    RESET_I                 : in  std_logic;

    LL8_DAT_I_DATA_I        : in  std_logic_vector(7 downto 0);
    LL8_DAT_I_SOF_N_I       : in  std_logic;
    LL8_DAT_I_EOF_N_I       : in  std_logic;
    LL8_DAT_I_SRC_RDY_N_I   : in  std_logic;
    LL8_DAT_I_DST_RDY_N_O   : out std_logic;
    STREAM_LENGTH_I         : in  std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0);
    HEADER_SEL_I            : in  std_logic_vector(CGN_HEADER_SEL_WIDTH-1 downto 0);


    LL8_UDP_O_DATA_O        : out std_logic_vector(7 downto 0);
    LL8_UDP_O_SOF_N_O       : out std_logic;
    LL8_UDP_O_EOF_N_O       : out std_logic;
    LL8_UDP_O_SRC_RDY_N_O   : out std_logic;
    LL8_UDP_O_DST_RDY_N_I   : in  std_logic;

    HEADER_ADDR_O           : out std_logic_vector(CGN_HEADER_SEL_WIDTH+CGN_HEADER_LENGTH_WIDTH-1 downto 0);
    HEADER_DATA_I           : in  std_logic_vector(7 downto 0)
  );
end ll8_udp_gen;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of ll8_udp_gen is

 constant C_IP_HEADER_LENGTH     : integer := 20;
 constant C_UDP_HEADER_LENGTH    : integer :=  8;

 constant C_ADDR_OFFS_IP_LEN_0   : integer := 16;
 constant C_ADDR_OFFS_IP_LEN_1   : integer := 17;
 
 constant C_ADDR_OFFS_CHECKSUM_0 : integer := 24;
 constant C_ADDR_OFFS_CHECKSUM_1 : integer := 25;
 
 constant C_ADDR_OFFS_UDP_LEN_0  : integer := 38;
 constant C_ADDR_OFFS_UDP_LEN_1  : integer := 39; 
 
 constant C_ADDR_OFFS_HEADER_LEN : integer := 42;

 
 constant C_ZERO_PAD16           : std_logic_vector(15 downto CGN_FRAME_LENGTH_WIDTH) := (others => '0');
 constant C_ZERO_PAD17           : std_logic_vector(16 downto CGN_FRAME_LENGTH_WIDTH) := (others => '0');
 
 

  signal header_sel_reg          : std_logic_vector(CGN_HEADER_SEL_WIDTH-1 downto 0) := (others => '0');
  signal stream_length_reg       : std_logic_vector(CGN_FRAME_LENGTH_WIDTH-1 downto 0) := (others => '0');

  signal addr_table_offs_out     : std_logic_vector(CGN_HEADER_LENGTH_WIDTH-1 downto 0);
  signal addr_table_offs         : std_logic_vector(CGN_HEADER_LENGTH_WIDTH-1 downto 0);
  signal addr_table_offs_prev    : std_logic_vector(CGN_HEADER_LENGTH_WIDTH-1 downto 0);

  signal ip_checksum_inv         : std_logic_vector(16 downto 0);
  signal ip_checksum_pre_calc    : std_logic_vector(15 downto 0);
  signal ip_checksum             : std_logic_vector(15 downto 0);
  
  signal ip_frame_length         : std_logic_vector(15 downto 0);
  signal udp_frame_length        : std_logic_vector(15 downto 0);

  signal ll8_dat_i_data_reg      : std_logic_vector(7 downto 0);
  signal ll8_dat_i_eof_n_reg     : std_logic;
  signal ll8_udp_o_eof_n         : std_logic;
  signal ll8_udp_o_src_rdy_n     : std_logic;
    
  signal addr_offs_ip_len_0_c    : std_logic;
  signal addr_offs_ip_len_1_c    : std_logic;
  signal addr_offs_checksum_0_c  : std_logic;
  signal addr_offs_checksum_1_c  : std_logic;
  signal addr_offs_udp_len_0_c   : std_logic;
  signal addr_offs_udp_len_1_c   : std_logic;

  signal addr_offs_ip_len_0_r    : std_logic;
  signal addr_offs_ip_len_1_r    : std_logic;
  signal addr_offs_checksum_0_r  : std_logic;
  signal addr_offs_checksum_1_r  : std_logic;
  signal addr_offs_udp_len_0_r   : std_logic;
  signal addr_offs_udp_len_1_r   : std_logic;
  
  ----------------------

  type states is (S_IDLE, S_LD_CS_0, S_LD_CS_1, S_LD_CS_2, S_HEADER_0, S_HEADER_1, S_DATA_0, S_DATA_1);
  signal state : states;
  attribute fsm_encoding : string;
  attribute fsm_encoding of state : signal is "one-hot";


begin


  ----------------------------------------
  -- State Machine
  ----------------------------------------

  fsm: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then


      if RESET_I = '1' then
        state <= S_IDLE;
        ll8_udp_o_eof_n        <= '1';
        ll8_udp_o_src_rdy_n    <= '1';
        LL8_UDP_O_SOF_N_O      <= '1';
        LL8_UDP_O_DATA_O       <= (others => '0');
      else
        case state is
          --------------------------------------------------------------------
          when S_IDLE =>
            ll8_udp_o_eof_n          <= '1';
            ll8_udp_o_src_rdy_n      <= '1';
            LL8_UDP_O_SOF_N_O        <= '1';
            addr_table_offs          <= conv_std_logic_vector(C_ADDR_OFFS_CHECKSUM_0, CGN_HEADER_LENGTH_WIDTH);
            if (LL8_DAT_I_SOF_N_I='0') and (LL8_DAT_I_SRC_RDY_N_I = '0') then
              ll8_dat_i_data_reg     <= LL8_DAT_I_DATA_I;
              ll8_dat_i_eof_n_reg    <= LL8_DAT_I_EOF_N_I;
              stream_length_reg      <= STREAM_LENGTH_I;
              header_sel_reg         <= HEADER_SEL_I;
              state <= S_LD_CS_0;
            end if;

          --------------------------------------------------------------------
          when S_LD_CS_0 =>
            state <= S_LD_CS_1;
            addr_table_offs          <= conv_std_logic_vector(C_ADDR_OFFS_CHECKSUM_1, CGN_HEADER_LENGTH_WIDTH);
          --------------------------------------------------------------------
          when S_LD_CS_1 =>
            ip_checksum_pre_calc(15 downto 8) <= HEADER_DATA_I;
            state <= S_LD_CS_2;
            -- reset addr_table_offs
            addr_table_offs        <= (others => '0');
          --------------------------------------------------------------------
          when S_LD_CS_2 =>
            ip_checksum_pre_calc( 7 downto 0) <= HEADER_DATA_I;
            state <= S_HEADER_0;
            addr_table_offs        <= addr_table_offs + 1;

          --------------------------------------------------------------------
          when S_HEADER_0 =>
            ll8_udp_o_src_rdy_n    <= '0';
            LL8_UDP_O_SOF_N_O      <= '0';
            LL8_UDP_O_DATA_O       <= HEADER_DATA_I;
            addr_table_offs_prev   <= addr_table_offs;
            addr_table_offs        <= addr_table_offs + 1;
           state <= S_HEADER_1;
          --------------------------------------------------------------------
          when S_HEADER_1 =>
            if (LL8_UDP_O_DST_RDY_N_I = '0') then
              -- ip_frame_length
              if (addr_offs_ip_len_0_r = '1') then
                LL8_UDP_O_DATA_O  <= ip_frame_length(15 downto 8);
              elsif (addr_offs_ip_len_1_r = '1') then
                LL8_UDP_O_DATA_O  <= ip_frame_length( 7 downto 0);
              -- ip_checksum
              elsif (addr_offs_checksum_0_r = '1') then
                LL8_UDP_O_DATA_O  <= ip_checksum(15 downto 8);
              elsif (addr_offs_checksum_1_r = '1') then
                LL8_UDP_O_DATA_O  <= ip_checksum( 7 downto 0);
              -- udp_frame_length
              elsif (addr_offs_udp_len_0_r = '1') then
                LL8_UDP_O_DATA_O  <= udp_frame_length(15 downto 8);
              elsif (addr_offs_udp_len_1_r = '1') then
                LL8_UDP_O_DATA_O  <= udp_frame_length( 7 downto 0);
              -- data
              else
                LL8_UDP_O_DATA_O  <= HEADER_DATA_I;
              end if;

              addr_offs_ip_len_0_r    <= addr_offs_ip_len_0_c;
              addr_offs_ip_len_1_r    <= addr_offs_ip_len_1_c;
              addr_offs_checksum_0_r  <= addr_offs_checksum_0_c;
              addr_offs_checksum_1_r  <= addr_offs_checksum_1_c;
              addr_offs_udp_len_0_r   <= addr_offs_udp_len_0_c;
              addr_offs_udp_len_1_r   <= addr_offs_udp_len_1_c;

              addr_table_offs_prev    <= addr_table_offs;
              addr_table_offs         <= addr_table_offs + 1;

              LL8_UDP_O_SOF_N_O       <= '1';

              if (addr_table_offs = C_ADDR_OFFS_HEADER_LEN) then
                state <= S_DATA_0;
              end if;

            end if;

          --------------------------------------------------------------------
          when S_DATA_0 =>
            if (LL8_UDP_O_DST_RDY_N_I = '0') then
              LL8_UDP_O_DATA_O <= ll8_dat_i_data_reg;
              ll8_udp_o_eof_n  <= ll8_dat_i_eof_n_reg;
              state <= S_DATA_1;
            end if;

          --------------------------------------------------------------------

          when S_DATA_1 =>
            if (LL8_UDP_O_DST_RDY_N_I = '0') and (ll8_udp_o_eof_n = '0') then
              ll8_udp_o_src_rdy_n    <= '1';
              ll8_udp_o_eof_n        <= '1';
              state <= S_IDLE;
            elsif ((LL8_UDP_O_DST_RDY_N_I = '0') or (ll8_udp_o_src_rdy_n = '1')) and (LL8_DAT_I_SRC_RDY_N_I = '0') then
              ll8_udp_o_src_rdy_n    <= '0';
              LL8_UDP_O_DATA_O       <= LL8_DAT_I_DATA_I;
              ll8_udp_o_eof_n        <= LL8_DAT_I_EOF_N_I;
            elsif (LL8_UDP_O_DST_RDY_N_I = '0') then
              ll8_udp_o_src_rdy_n    <= '1';
            end if;

          --------------------------------------------------------------------
          when others =>
            state <= S_IDLE;

          --------------------------------------------------------------------
        end case;

      end if;
    end if;
  end process fsm;


  ----------------------------------------
  -- address table offset calc
  ----------------------------------------

  addr_table_offs_out <= addr_table_offs_prev when ((state = S_HEADER_1) and (LL8_UDP_O_DST_RDY_N_I = '1')) else addr_table_offs;


  ----------------------------------------
  -- Length and Checksum
  ----------------------------------------

  calc_ip_cheksum_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      ip_checksum_inv          <= ("0" & ip_checksum_pre_calc) + (C_ZERO_PAD17 & stream_length_reg);
      ip_checksum(15 downto 0) <= not (ip_checksum_inv(15 downto 0) + ("000000000000000" & ip_checksum_inv(16)));
      
      ip_frame_length   <= (C_ZERO_PAD16 & stream_length_reg) + C_IP_HEADER_LENGTH + C_UDP_HEADER_LENGTH; -- Total Length of IP frame
      udp_frame_length  <= (C_ZERO_PAD16 & stream_length_reg) + C_UDP_HEADER_LENGTH;  -- UDP Message Len
       
    end if;
  end process;

  addr_offs_ip_len_0_c    <= '1' when (addr_table_offs = C_ADDR_OFFS_IP_LEN_0)   else '0';
  addr_offs_ip_len_1_c    <= '1' when (addr_table_offs = C_ADDR_OFFS_IP_LEN_1)   else '0';
  addr_offs_checksum_0_c  <= '1' when (addr_table_offs = C_ADDR_OFFS_CHECKSUM_0) else '0';
  addr_offs_checksum_1_c  <= '1' when (addr_table_offs = C_ADDR_OFFS_CHECKSUM_1) else '0';
  addr_offs_udp_len_0_c   <= '1' when (addr_table_offs = C_ADDR_OFFS_UDP_LEN_0)  else '0';
  addr_offs_udp_len_1_c   <= '1' when (addr_table_offs = C_ADDR_OFFS_UDP_LEN_1)  else '0';


  ----------------------------------------
  -- Outputs
  ----------------------------------------

  LL8_DAT_I_DST_RDY_N_O  <= '0' when (state = S_IDLE) or ( (state = S_DATA_1) and (LL8_UDP_O_DST_RDY_N_I = '0') and (ll8_udp_o_eof_n = '1')) else '1';

  LL8_UDP_O_EOF_N_O      <= ll8_udp_o_eof_n;

  LL8_UDP_O_SRC_RDY_N_O  <= ll8_udp_o_src_rdy_n;

  HEADER_ADDR_O          <= header_sel_reg & addr_table_offs_out;


END behavior;

