---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Fast Scaler LSBs
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  18.05.2016 08:33:58
--
--  Description :  Logic to count the pulses discriminated by the comparator on each
--                 channel of the WaveDream board. This part represents the fast
--                 counter for the lsbs of the scaler, clocked with the signal from the
--                 comparator.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

entity fast_scaler_lsbs is
  generic (
    CGN_COUNTER_BASE_WIDTH : integer := 8
  );
  port (
    COUNTER_VALUE_O     : out std_logic_vector(CGN_COUNTER_BASE_WIDTH-1 downto 0);
    COUNTER_CARRY_O     : out std_logic;
    COUNTER_CARRY_RST_I : in  std_logic;
    EN_I                : in  std_logic;
    RESET_I             : in  std_logic;
    CLK_I               : in  std_logic
  );
end fast_scaler_lsbs;

architecture behavioral of fast_scaler_lsbs is

  constant C_COUNTER_ONES : std_logic_vector(CGN_COUNTER_BASE_WIDTH-1 downto 0) := (others=>'1');

  signal counter : std_logic_vector(CGN_COUNTER_BASE_WIDTH-1 downto 0) := (others=>'0');
  signal carry   : std_logic := '0';

begin

  COUNTER_VALUE_O <= counter;
  COUNTER_CARRY_O <= carry;

  process(CLK_I, RESET_I)
  begin
    if RESET_I = '1' then
      counter <= (others=>'0');
    elsif rising_edge(CLK_I) then
      if EN_I = '1' then
        counter <= counter + 1;
      end if;
    end if;
  end process;

  process(CLK_I, COUNTER_CARRY_RST_I)
  begin
    if COUNTER_CARRY_RST_I = '1' then
      carry <= '0';
    elsif rising_edge(CLK_I) then
      if counter = C_COUNTER_ONES then
        carry <= '1';
      end if;
    end if;
  end process;

end architecture behavioral;