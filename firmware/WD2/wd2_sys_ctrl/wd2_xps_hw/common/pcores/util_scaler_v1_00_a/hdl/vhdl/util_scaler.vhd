---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  util_scaler
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  17.05.2016 12:46:20
--
--  Description :  Logic to count the pulses discriminated by the comparator on each
--                 channel of the WaveDream board.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity util_scaler is
  generic (
    CGN_NR_OF_SCALERS : integer := 16;
    CGN_UC_DATA_WIDTH : integer := 32;
    CGN_SCALER_WIDTH  : integer := 64;
    CGN_TICK_TIME     : real :=  1.0e9; -- ns
    CGN_CLK_PERIOD    : real := 20.0    -- ns
  );
  port (
    SCALER_CLK_I       : in  std_logic_vector(CGN_NR_OF_SCALERS-1 downto 0);
    SCALER_VALUE_DAQ_O : out std_logic_vector(CGN_NR_OF_SCALERS*CGN_SCALER_WIDTH-1 downto 0);
    SCALER_VALUE_UC_O  : out std_logic_vector(CGN_NR_OF_SCALERS*CGN_SCALER_WIDTH-1 downto 0);
    STORE_MSBS_I       : in  std_logic_vector(CGN_NR_OF_SCALERS-1 downto 0);
    RESET_I            : in  std_logic;
    CLK_DAQ_I          : in  std_logic;
    CLK_UC_I           : in  std_logic
  );
end util_scaler;

architecture behavioral of util_scaler is

  component fast_scaler_lsbs is
    generic (
      CGN_COUNTER_BASE_WIDTH : integer := 8
    );
    port (
      COUNTER_VALUE_O     : out std_logic_vector(CGN_COUNTER_BASE_WIDTH-1 downto 0);
      COUNTER_CARRY_O     : out std_logic;
      COUNTER_CARRY_RST_I : in  std_logic;
      EN_I                : in  std_logic;
      RESET_I             : in  std_logic;
      CLK_I               : in  std_logic
    );
  end component;

  component slow_scaler_lsbs is
    generic (
      CGN_COUNTER_WIDTH : integer := 56
    );
    port (
      COUNTER_VALUE_O : out std_logic_vector(CGN_COUNTER_WIDTH-1 downto 0);
      EN_I            : in  std_logic;
      RESET_I         : in  std_logic;
      CLK_I           : in  std_logic
    );
  end component;

  constant C_FAST_SCALER_WIDTH : integer := 8;
  constant C_STORE_CYCLES      : integer := 5;
  constant C_STORE_TICK        : integer := 2;
  constant C_RESET_TICK        : integer := 4;
  constant C_TIMER_LOAD        : integer := integer(CGN_TICK_TIME/CGN_CLK_PERIOD) + C_STORE_CYCLES;
  constant C_TIMER_SIZE        : integer := log2ceil(C_TIMER_LOAD);

  type counter_lsb_type  is array (CGN_NR_OF_SCALERS-1 downto 0) of std_logic_vector(C_FAST_SCALER_WIDTH-1 downto 0);
  type counter_msb_type  is array (CGN_NR_OF_SCALERS-1 downto 0) of std_logic_vector(CGN_SCALER_WIDTH-C_FAST_SCALER_WIDTH-1 downto 0);
  signal counter_lsbs : counter_lsb_type := (others=>(others=>'0'));
  signal counter_msbs : counter_msb_type := (others=>(others=>'0'));

  type counter_carry_type is array (CGN_NR_OF_SCALERS-1 downto 0) of std_logic_vector(2 downto 0);
  signal counter_carry_s   : counter_carry_type := (others=>(others=>'0'));
  signal counter_carry_rst : std_logic_vector(CGN_NR_OF_SCALERS-1 downto 0) := (others=>'0');
  signal count_en          : std_logic := '0';
  signal count_rst         : std_logic := '0';
  signal tick_timer_count  : std_logic_vector(C_TIMER_SIZE-1 downto 0) := (others=>'0');
  signal tick_s            : std_logic_vector(C_STORE_CYCLES downto 0) := (others=>'0');

  signal scaler_value      : std_logic_vector(CGN_NR_OF_SCALERS*CGN_SCALER_WIDTH-1 downto 0) := (others=>'0');
  signal scaler_value_uc   : std_logic_vector(CGN_NR_OF_SCALERS*CGN_SCALER_WIDTH-1 downto 0) := (others=>'0');

  signal we_daq_s0         : std_logic := '0';
  signal we_daq_s1         : std_logic := '0';
  
begin

  count_rst <= tick_s(C_RESET_TICK) or RESET_I;

  process(CLK_UC_I)
  begin
    if rising_edge(CLK_UC_I) then
      if RESET_I = '1' or tick_timer_count = 0 then
        count_en  <= '0';
        tick_s    <= (others=>'0');
        tick_s(0) <= '1';
        tick_timer_count <= CONV_STD_LOGIC_VECTOR(C_TIMER_LOAD, C_TIMER_SIZE);
      else
        if tick_s(C_STORE_CYCLES) = '1' then
          count_en  <= '1';
        end if;
        tick_timer_count <= tick_timer_count - 1;
        tick_s(0) <= '0';
        tick_delay : for j in 0 to C_STORE_CYCLES-1 loop
          tick_s(j+1) <= tick_s(j);
        end loop tick_delay;
      end if;
    end if;
  end process;

  scaler_inst : for i in CGN_NR_OF_SCALERS-1 downto 0 generate

    scaler_lsb_inst : fast_scaler_lsbs
    generic map(
      CGN_COUNTER_BASE_WIDTH => C_FAST_SCALER_WIDTH
    )
    port map(
      COUNTER_VALUE_O     => counter_lsbs(i),
      COUNTER_CARRY_O     => counter_carry_s(i)(0),
      COUNTER_CARRY_RST_I => counter_carry_s(i)(2),
      EN_I                => count_en,
      RESET_I             => count_rst,
      CLK_I               => SCALER_CLK_I(i)
    );

    scaler_msb_inst : slow_scaler_lsbs
    generic map(
      CGN_COUNTER_WIDTH => CGN_SCALER_WIDTH-C_FAST_SCALER_WIDTH
    )
    port map(
      COUNTER_VALUE_O => counter_msbs(i),
      EN_I            => counter_carry_s(i)(2),
      RESET_I         => count_rst,
      CLK_I           => CLK_UC_I
    );

    process(CLK_UC_I)
    begin
      if rising_edge(CLK_UC_I) then
        -- store counter value upon tick
        if tick_s(C_STORE_TICK) = '1' then
          scaler_value((i+1)*CGN_SCALER_WIDTH-1 downto i*CGN_SCALER_WIDTH) <= counter_msbs(i) & counter_lsbs(i);
        end if;
        -- generate carry to increment slow counter
        if counter_carry_s(i)(2) = '1' then
          counter_carry_s(i)(2 downto 1) <= (others=>'0');
        else
          counter_carry_s(i)(1) <= counter_carry_s(i)(0);
          counter_carry_s(i)(2) <= counter_carry_s(i)(1);
        end if;
      end if;
    end process;

    output_msb_registers : if CGN_SCALER_WIDTH > CGN_UC_DATA_WIDTH generate
      process(CLK_UC_I)
      begin
        if rising_edge(CLK_UC_I) then
          -- store all msb data when lsb word is read
          if STORE_MSBS_I(i) = '1' then
            scaler_value_uc((i+1)*CGN_SCALER_WIDTH-1 downto i*CGN_SCALER_WIDTH+CGN_UC_DATA_WIDTH) <= scaler_value((i+1)*CGN_SCALER_WIDTH-1 downto i*CGN_SCALER_WIDTH+CGN_UC_DATA_WIDTH);
          end if;
        end if;
      end process;
      -- direct output of lsb word
      scaler_value_uc((i+1)*CGN_SCALER_WIDTH-CGN_UC_DATA_WIDTH-1 downto i*CGN_SCALER_WIDTH) <= scaler_value((i+1)*CGN_SCALER_WIDTH-CGN_UC_DATA_WIDTH-1 downto i*CGN_SCALER_WIDTH);
    end generate output_msb_registers;

  end generate;

  no_msb_registers : if CGN_SCALER_WIDTH <= CGN_UC_DATA_WIDTH generate
    scaler_value_uc <= scaler_value;
  end generate no_msb_registers;

  process(CLK_DAQ_I)
  begin
    if rising_edge(CLK_DAQ_I) then
      we_daq_s0 <= tick_s(C_STORE_TICK);
      we_daq_s1 <= we_daq_s0;
      if we_daq_s1 = '1' then
        SCALER_VALUE_DAQ_O <= scaler_value;
      end if;
    end if;
  end process;

  SCALER_VALUE_UC_O <= scaler_value_uc;

end architecture behavioral;