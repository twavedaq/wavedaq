--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ram_dp.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description : generic dual port ram
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;
use ieee.std_logic_arith.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ram_dp is
  generic
  (
    CGN_DATA_WIDTH_A  : integer := 32;
    CGN_ADDR_WIDTH_A  : integer := 9;
    CGN_READ_PORT_A   : boolean := false;
    CGN_WRITE_PORT_A  : boolean := true;
    CGN_READ_FIRST_A  : boolean := false;
    CGN_USE_OUTREG_A  : boolean := true;

    CGN_DATA_WIDTH_B  : integer := 8;
    CGN_ADDR_WIDTH_B  : integer := 11;
    CGN_READ_PORT_B   : boolean := true;
    CGN_WRITE_PORT_B  : boolean := false;
    CGN_READ_FIRST_B  : boolean := true;
    CGN_USE_OUTREG_B  : boolean := false;

    CGN_RAM_STYLE     : string := "block"  -- {auto|block|distributed|pipe_distributed|block_power1|block_power2}
  );
  port
  (
    PA_CLK_I          : in  std_logic;
    PA_EN_I           : in  std_logic;
    PA_ADDR_I         : in  std_logic_vector(CGN_ADDR_WIDTH_A-1 downto 0);
    PA_DATA_I         : in  std_logic_vector(CGN_DATA_WIDTH_A-1 downto 0);
    PA_WR_EN_I        : in  std_logic;
    PA_DATA_O         : out std_logic_vector(CGN_DATA_WIDTH_A-1 downto 0);

    PB_CLK_I          : in  std_logic;
    PB_EN_I           : in  std_logic;
    PB_ADDR_I         : in  std_logic_vector(CGN_ADDR_WIDTH_B-1 downto 0);
    PB_DATA_I         : in  std_logic_vector(CGN_DATA_WIDTH_B-1 downto 0);
    PB_WR_EN_I        : in  std_logic;
    PB_DATA_O         : out std_logic_vector(CGN_DATA_WIDTH_B-1 downto 0)
  );
end ram_dp;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ram_dp is

  function max(a, b: integer) return integer is
  begin
    if (a > b) then return a; else return b; end if;
  end;

  function min(a, b: integer) return integer is
  begin
    if (a < b) then return a; else return b; end if;
  end;

  function log2 (val: INTEGER) return natural is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function Log2;

  constant C_SIZE_BIT_A   : integer := (2**CGN_ADDR_WIDTH_A) * CGN_DATA_WIDTH_A;
  constant C_SIZE_BIT_B   : integer := (2**CGN_ADDR_WIDTH_B) * CGN_DATA_WIDTH_B;

  constant C_WIDTH_MIN    : integer := min(CGN_DATA_WIDTH_A, CGN_DATA_WIDTH_B);
  constant C_WIDTH_MAX    : integer := max(CGN_DATA_WIDTH_A, CGN_DATA_WIDTH_B);


  constant C_SIZE_BIT_MAX : integer := max(C_SIZE_BIT_A, C_SIZE_BIT_B);
  constant C_SIZE_ENTRIES : integer := C_SIZE_BIT_MAX / C_WIDTH_MIN;

  constant C_RATIO_A      : integer := CGN_DATA_WIDTH_A / C_WIDTH_MIN;
  constant C_RATIO_B      : integer := CGN_DATA_WIDTH_B / C_WIDTH_MIN;

  type ramType is array (0 to C_SIZE_ENTRIES-1) of std_logic_vector(C_WIDTH_MIN-1 downto 0);
--  signal ram : ramType := (others => (others => '0'));
  shared variable ram : ramType := (others => (others => '0'));

  attribute ram_style : string;
  attribute ram_style of ram : variable is CGN_RAM_STYLE;


  signal read_a : std_logic_vector(CGN_DATA_WIDTH_A-1 downto 0):= (others => '0');
  signal read_b : std_logic_vector(CGN_DATA_WIDTH_B-1 downto 0):= (others => '0');

begin

  -----------------------------------------------------------------------------
  -- PORT A
  -----------------------------------------------------------------------------

  gen_port_a : if (CGN_READ_PORT_A or CGN_WRITE_PORT_A) generate
  begin

    process (PA_CLK_I)
     variable addr_a : integer;
    begin

      if rising_edge(PA_CLK_I) then
        if PA_EN_I = '1' then
          for i in 0 to C_RATIO_A-1 loop
            addr_a := conv_integer(PA_ADDR_I & conv_std_logic_vector(i,log2(C_RATIO_A)));

            if (CGN_READ_PORT_A and CGN_READ_FIRST_A) then
              read_a((i+1)*C_WIDTH_MIN-1 downto i*C_WIDTH_MIN) <= ram(addr_a);
            end if;

            if (CGN_WRITE_PORT_A) then
              if PA_WR_EN_I = '1' then
                ram(addr_a):= PA_DATA_I((i+1)*C_WIDTH_MIN-1 downto i*C_WIDTH_MIN);
              end if;
            end if;

            if (CGN_READ_PORT_A and (not CGN_READ_FIRST_A)) then
              read_a((i+1)*C_WIDTH_MIN-1 downto i*C_WIDTH_MIN) <= ram(addr_a);
            end if;

          end loop;
        end if;

        if (CGN_READ_PORT_A and CGN_USE_OUTREG_A) then
          PA_DATA_O <= read_a;
        end if;

      end if;
    end process;

  end generate;

  gen_outreg_a: if (CGN_READ_PORT_A and CGN_USE_OUTREG_A) generate
    process (PA_CLK_I)
    begin
      if rising_edge(PA_CLK_I) then    
        PA_DATA_O <= read_a;
      end if;
    end process;
  end generate;    

  gen_no_outreg_a: if (CGN_READ_PORT_A and (not CGN_USE_OUTREG_A)) generate
  begin
    PA_DATA_O <= read_a;
  end generate;
  
  gen_no_read_port_a : if (not CGN_READ_PORT_A) generate
  begin
    PA_DATA_O <= (others => '0');
  end generate;


  -----------------------------------------------------------------------------
  -- PORT B
  -----------------------------------------------------------------------------

  gen_port_b : if (CGN_READ_PORT_B or CGN_WRITE_PORT_B) generate
  begin

    process (PB_CLK_I)
     variable addr_b : integer;
    begin

      if rising_edge(PB_CLK_I) then
        if PB_EN_I = '1' then
          for i in 0 to C_RATIO_B-1 loop
            addr_b := conv_integer(PB_ADDR_I & conv_std_logic_vector(i,log2(C_RATIO_B)));

            if (CGN_READ_PORT_B and CGN_READ_FIRST_B) then
              read_b((i+1)*C_WIDTH_MIN-1 downto i*C_WIDTH_MIN) <= ram(addr_b);
            end if;

            if (CGN_WRITE_PORT_B) then
              if PB_WR_EN_I = '1' then
                ram(addr_b):= PB_DATA_I((i+1)*C_WIDTH_MIN-1 downto i*C_WIDTH_MIN);
              end if;
            end if;

            if (CGN_READ_PORT_B and (not CGN_READ_FIRST_B)) then
              read_b((i+1)*C_WIDTH_MIN-1 downto i*C_WIDTH_MIN) <= ram(addr_b);
            end if;

          end loop;
        end if;

      end if;
    end process;
    
  end generate;


  gen_outreg_b: if (CGN_READ_PORT_B and CGN_USE_OUTREG_B) generate
    process (PB_CLK_I)
    begin
      if rising_edge(PB_CLK_I) then    
        PB_DATA_O <= read_b;
      end if;
    end process;
  end generate;    

  gen_no_outreg_b: if (CGN_READ_PORT_B and (not CGN_USE_OUTREG_B)) generate
  begin
    PB_DATA_O <= read_b;
  end generate;  

  gen_no_read_port_b : if (not CGN_READ_PORT_B) generate
  begin
    PB_DATA_O <= (others => '0');
  end generate;


end behavioral;
