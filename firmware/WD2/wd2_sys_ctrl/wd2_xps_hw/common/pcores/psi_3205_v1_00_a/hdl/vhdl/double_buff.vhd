--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : double_buff.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    buffer to split long combinatorial paths
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity double_buff is
  generic
  (
    CGN_DIN_SRC_RDY_ACTIVE_HIGH   : boolean := true;
    CGN_DOUT_SRC_RDY_ACTIVE_HIGH  : boolean := true;
    CGN_DIN_DST_RDY_ACTIVE_HIGH   : boolean := true;
    CGN_DOUT_DST_RDY_ACTIVE_HIGH  : boolean := true;
    CGN_DATA_WIDTH                : integer := 32
  );
  port
  (
    CLK_I           : in  std_logic;
    RESET_I         : in  std_logic;

    DIN_DATA_I      : in  std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    DIN_SRC_RDY_I   : in  std_logic;
    DIN_DST_RDY_O   : out std_logic;

    DOUT_DATA_O     : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    DOUT_SRC_RDY_O  : out std_logic;
    DOUT_DST_RDY_I  : in  std_logic
  );
end double_buff ;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behave of double_buff is

  signal  buffer_0       : std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
  signal  buffer_1       : std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
  signal  dout           : std_logic_vector (CGN_DATA_WIDTH-1 downto 0);

  signal  buffer_0_valid : std_logic := '0';
  signal  buffer_1_valid : std_logic := '0';
  signal  write_sel      : std_logic := '0';
  signal  read_sel       : std_logic := '0';
  signal  din_valid      : std_logic := '0';
  signal  dout_valid     : std_logic := '0';
  signal  vacancy        : std_logic := '0';
  signal  dout_buff_rdy  : std_logic := '0';
  signal  dst_rdy        : std_logic := '0';

begin

  din_valid_active_high: if CGN_DIN_SRC_RDY_ACTIVE_HIGH generate
  begin
    din_valid <= DIN_SRC_RDY_I;
  end generate;

  din_valid_active_low: if not CGN_DIN_SRC_RDY_ACTIVE_HIGH generate
  begin
    din_valid <= not DIN_SRC_RDY_I;
  end generate;

  ------------------------------------------------------------

  dst_rdy_active_high: if CGN_DOUT_DST_RDY_ACTIVE_HIGH generate
  begin
    dst_rdy <= DOUT_DST_RDY_I;
  end generate;

  dst_rdy_active_low: if not CGN_DOUT_DST_RDY_ACTIVE_HIGH generate
  begin
    dst_rdy <= not DOUT_DST_RDY_I;
  end generate;

  ------------------------------------------------------------

  vacancy <= (not buffer_0_valid) when write_sel = '0' else (not buffer_1_valid);

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if (RESET_I = '1') then
        write_sel      <= '0';
        read_sel       <= '0';
        buffer_0_valid <= '0';
        buffer_1_valid <= '0';
        dout_valid     <= '0';
      else
        if (write_sel = '0') and (buffer_0_valid = '0') and (din_valid = '1') then
          buffer_0       <= DIN_DATA_I;
          buffer_0_valid <= '1';
          write_sel      <= '1';
        end if;
        if (write_sel = '1') and (buffer_1_valid = '0') and (din_valid = '1') then
          buffer_1       <= DIN_DATA_I;
          buffer_1_valid <= '1';
          write_sel      <= '0';
        end if;
        if dst_rdy = '1' then
          dout_valid  <= '0';
        end if;
        if (read_sel = '0') and (buffer_0_valid = '1') and (dout_buff_rdy = '1') then
          dout           <= buffer_0;
          buffer_0_valid <= '0';
          read_sel       <= '1';
          dout_valid     <= '1';
        end if;
        if (read_sel = '1') and (buffer_1_valid = '1') and (dout_buff_rdy = '1') then
          dout           <= buffer_1;
          buffer_1_valid <= '0';
          read_sel       <= '0';
          dout_valid     <= '1';
        end if;

      end if;
    end if;
  end process;

  dout_buff_rdy <= dst_rdy or (not dout_valid);

  ------------------------------------------------------------

  DOUT_DATA_O        <= dout;

  dout_valid_active_high: if CGN_DOUT_SRC_RDY_ACTIVE_HIGH generate
  begin
    DOUT_SRC_RDY_O <= dout_valid;
  end generate;

  dout_valid_active_low: if not CGN_DOUT_SRC_RDY_ACTIVE_HIGH generate
  begin
    DOUT_SRC_RDY_O <= not dout_valid;
  end generate;

  ------------------------------------------------------------

  din_rdy_active_high: if CGN_DIN_DST_RDY_ACTIVE_HIGH  generate
  begin
    DIN_DST_RDY_O <= vacancy;
  end generate;

  din_rdy_active_low: if not CGN_DIN_DST_RDY_ACTIVE_HIGH  generate
  begin
    DIN_DST_RDY_O <= not vacancy;
  end generate;

end architecture behave;
