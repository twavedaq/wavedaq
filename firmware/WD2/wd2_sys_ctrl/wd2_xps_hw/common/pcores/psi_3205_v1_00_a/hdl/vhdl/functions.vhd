--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : functions.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  function package
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Package header
--------------------------------------------------------------------------------

package functions is

  function log2ceil(n : integer) return integer;
  function log2(val: integer) return natural;
  function min(a, b: integer) return integer;
  function max(a, b: integer) return integer;
  function bit_reverse(arg: std_logic_vector) return std_logic_vector;
  function byte_swap(arg: std_logic_vector) return std_logic_vector;

end;

--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body functions is

  -----------------------------------------------------
  -- function log2ceil
  -----------------------------------------------------

  function log2ceil(n : integer) return integer
  is
    variable m, p  : integer;
  begin
    m  := 0;
    p  := 1;
    for i in 0 to n loop
      if p < n then
        m  := m+1;
        p  := p*2;
      end if;
    end loop;
    return m;
  end function log2ceil;


  -----------------------------------------------------
  -- function log2
  -----------------------------------------------------

  function log2(val: integer) return natural
  is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function log2;
  
  
  -----------------------------------------------------
  -- function min
  -----------------------------------------------------

  function min(a, b: integer) return integer is
  begin
    if (a < b) then return a; else return b; end if;
  end;
  
  
  -----------------------------------------------------
  -- function max
  -----------------------------------------------------
  
  function max(a, b: integer) return integer is
  begin
    if (a > b) then return a; else return b; end if;
  end;


  -----------------------------------------------------
  -- function bit_reverse
  -----------------------------------------------------

  function bit_reverse(arg: std_logic_vector) return std_logic_vector
  is
    variable res: std_logic_vector(arg'reverse_range);
  begin
    for i in arg'range loop
      res(i) := arg(i);
    end loop;
    return res;
  end bit_reverse;


  -----------------------------------------------------
  -- function byte_swap
  -----------------------------------------------------

  function byte_swap(arg: std_logic_vector) return std_logic_vector
  is
    constant C_LEN : integer := arg'length;
    variable din   : std_logic_vector(C_LEN-1 downto 0);
    variable dout  : std_logic_vector(C_LEN-1 downto 0);
  begin
    assert ((C_LEN mod 8) = 0) report "Vector length is no multiple of 8" severity error;
    din := arg;
    for i in 0 to ((C_LEN / 8) - 1) loop
      dout(((i*8)+7) downto (i*8)) := din((C_LEN-(i*8)-1) downto (C_LEN-(i*8)-8));
    end loop;
    return dout;
  end byte_swap;

end package body;

