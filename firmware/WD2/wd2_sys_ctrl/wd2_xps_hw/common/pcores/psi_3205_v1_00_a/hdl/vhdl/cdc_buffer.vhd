----------------------------------------------------------------------------------
-- 		Company: 
-- Engineer: 
-- 
-- Create Date:    14:18:02 05/27/2011 
-- Design Name: 
-- Module Name:    ll64_fifo - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------


-- Use timing ignore constraint in UCF:
-- NET "*cdc_ignore_timing*" TIG;


--------------------------------------------------------------------------------
-- Sync Clock Domain Crossing
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


library psi_3205_v1_00_a;
use psi_3205_v1_00_a.functions.all;


entity cdc_buffer is
  generic
  (

    CGN_PULSE_LEN_CLK_A       : integer := 5;  
    CGN_PULSE_SYNC_REGS_CLK_B : integer := 3;

    CGN_DATA_WIDTH_PORT0      : integer := 1;
    CGN_DATA_WIDTH_PORT1      : integer := 1;
    CGN_DATA_WIDTH_PORT2      : integer := 1;
    CGN_DATA_WIDTH_PORT3      : integer := 1;
    CGN_DATA_WIDTH_PORT4      : integer := 1;
    CGN_DATA_WIDTH_PORT5      : integer := 1;
    CGN_DATA_WIDTH_PORT6      : integer := 1;
    CGN_DATA_WIDTH_PORT7      : integer := 1
  );
  port
  (
    CLK_A_I   : in  std_logic;
    CLK_B_I   : in  std_logic;

    PORT0_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT0-1) := (others => '0');
    PORT0_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT0-1);
    PORT1_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT1-1) := (others => '0');
    PORT1_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT1-1);
    PORT2_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT2-1) := (others => '0');
    PORT2_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT2-1);
    PORT3_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT3-1) := (others => '0');
    PORT3_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT3-1);
    PORT4_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT4-1) := (others => '0');
    PORT4_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT4-1);
    PORT5_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT5-1) := (others => '0');
    PORT5_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT5-1);
    PORT6_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT6-1) := (others => '0');
    PORT6_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT6-1);
    PORT7_A_I : in  std_logic_vector (0 to CGN_DATA_WIDTH_PORT7-1) := (others => '0');
    PORT7_B_O : out std_logic_vector (0 to CGN_DATA_WIDTH_PORT7-1)
  );

end cdc_buffer ;

--------------------------------------------------------------------------------

architecture behave of cdc_buffer is

  constant C_PULSER_BITS  : integer := log2(CGN_PULSE_LEN_CLK_A);
  constant C_PULSER_VAL   : integer := CGN_PULSE_LEN_CLK_A-1;
  constant C_PULSER_SLV   : std_logic_vector(C_PULSER_BITS-1 downto 0) := CONV_STD_LOGIC_VECTOR(C_PULSER_VAL, C_PULSER_BITS);

  signal   pulsergen      : std_logic_vector(C_PULSER_BITS-1 downto 0) := (others => '0');
  signal   pulse          : std_logic  := '0';

  signal   pulse_cdc_ignore_timing : std_logic;
  signal   pulse_sync     : std_logic_vector(CGN_PULSE_SYNC_REGS_CLK_B-1 downto 0) := (others => '0');


  signal port0_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT0-1);
  signal port1_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT1-1);
  signal port2_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT2-1);
  signal port3_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT3-1);
  signal port4_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT4-1);
  signal port5_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT5-1);
  signal port6_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT6-1);
  signal port7_a_reg : std_logic_vector(0 to CGN_DATA_WIDTH_PORT7-1);

  signal port0_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT0-1);
  signal port1_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT1-1);
  signal port2_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT2-1);
  signal port3_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT3-1);
  signal port4_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT4-1);
  signal port5_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT5-1);
  signal port6_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT6-1);
  signal port7_cdc_ignore_timing : std_logic_vector(0 to CGN_DATA_WIDTH_PORT7-1);

  attribute keep : string;
  attribute keep of port0_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port1_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port2_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port3_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port4_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port5_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port6_cdc_ignore_timing: signal is "TRUE";
  attribute keep of port7_cdc_ignore_timing: signal is "TRUE";

  attribute keep of pulse_cdc_ignore_timing: signal is "TRUE";


begin

  ------------------------------------------------------------------------------
  -- Clock Domain A
  ------------------------------------------------------------------------------

  pulser_proc: process(CLK_A_I) is
  begin
    if rising_edge(CLK_A_I) then
      if (pulsergen > 0) then
          pulsergen <= pulsergen - 1;
      else
        pulsergen <= C_PULSER_SLV;
        if (pulse = '0') then
          pulse <= '1';
          -- register input with rising edge of sampling pulse
          port0_a_reg <= PORT0_A_I;
          port1_a_reg <= PORT1_A_I;
          port2_a_reg <= PORT2_A_I;
          port3_a_reg <= PORT3_A_I;        
          port4_a_reg <= PORT4_A_I;
          port5_a_reg <= PORT5_A_I;
          port6_a_reg <= PORT6_A_I;
          port7_a_reg <= PORT7_A_I;        
        else
          pulse <= '0';
        end if;
      end if;         

    end if;
  end process pulser_proc;



  ------------------------------------------------------------------------------
  -- Clock Domain B
  ------------------------------------------------------------------------------

  sync_proc: process(CLK_B_I) is
  begin
    if rising_edge(CLK_B_I) then
      pulse_cdc_ignore_timing <= pulse;
      pulse_sync <= pulse_cdc_ignore_timing & pulse_sync(CGN_PULSE_SYNC_REGS_CLK_B-1 downto 1);
    
      if ((pulse_sync(1) = '1') and  (pulse_sync(0) = '0')) then
        -- register output with rising edge of sampled pulse
      port0_cdc_ignore_timing <= port0_a_reg;
      port1_cdc_ignore_timing <= port1_a_reg;
      port2_cdc_ignore_timing <= port2_a_reg;
      port3_cdc_ignore_timing <= port3_a_reg;        
      port4_cdc_ignore_timing <= port4_a_reg;
      port5_cdc_ignore_timing <= port5_a_reg;
      port6_cdc_ignore_timing <= port6_a_reg;
      port7_cdc_ignore_timing <= port7_a_reg;        
      end if;         

    end if;
  end process sync_proc;

  PORT0_B_O <= port0_cdc_ignore_timing;
  PORT1_B_O <= port1_cdc_ignore_timing;
  PORT2_B_O <= port2_cdc_ignore_timing;
  PORT3_B_O <= port3_cdc_ignore_timing;
  PORT4_B_O <= port4_cdc_ignore_timing;
  PORT5_B_O <= port5_cdc_ignore_timing;
  PORT6_B_O <= port6_cdc_ignore_timing;
  PORT7_B_O <= port7_cdc_ignore_timing;


end architecture behave;
