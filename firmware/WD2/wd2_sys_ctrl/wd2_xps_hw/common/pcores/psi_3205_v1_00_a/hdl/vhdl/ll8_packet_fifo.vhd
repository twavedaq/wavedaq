--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll8_packet_fifo.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  synchronous local link packet fifo for data width 8
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll8_packet_fifo is
  generic
  (
    CGN_ADDR_WIDTH     : integer := 11;
    CGN_AUTO_OK        : boolean := false;
    CGN_RAM_STYLE      : string  := "block"  -- style of implemented ram type:
                                             -- {auto|block|distributed|pipe_distributed};    
  );
  port
  (
    CLK_I              : in  std_logic;
    RESET_I            : in  std_logic;

    -- ll us interface
    LL8_I_DATA_I       : in  std_logic_vector(7 downto 0);
    LL8_I_REM_I        : in  std_logic_vector(0 downto 0) := (others => '1');    
    LL8_I_SOF_N_I      : in  std_logic;
    LL8_I_EOF_N_I      : in  std_logic;
    LL8_I_SRC_RDY_N_I  : in  std_logic;
    LL8_I_DST_RDY_N_O  : out std_logic;
    LL8_I_PKT_OK_I     : in  std_logic;
    LL8_I_PKT_ERR_I    : in  std_logic;

    -- ll ds interface
    LL8_O_DATA_O       : out std_logic_vector(7 downto 0);
    LL8_O_REM_O        : out std_logic_vector(0 downto 0);    
    LL8_O_SOF_N_O      : out std_logic;
    LL8_O_EOF_N_O      : out std_logic;
    LL8_O_SRC_RDY_N_O  : out std_logic;
    LL8_O_DST_RDY_N_I  : in  std_logic
  );
end ll8_packet_fifo;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of ll8_packet_fifo is

  signal wr_rdy        : std_logic;
  signal wr_en         : std_logic;
  signal rd_en         : std_logic;
  signal full          : std_logic;
  signal empty         : std_logic;
  signal ll8_ds_sof_n  : std_logic;
  signal ll8_ds_eof_n  : std_logic;

  signal frame_active  : std_logic;
  signal pkt_ok        : std_logic;

begin

  ------------------------------------------------------------------------------
  
  gen_no_auto_ok: if CGN_AUTO_OK = false generate
    pkt_ok <= LL8_I_PKT_OK_I;
  end generate;
  
  gen_auto_ok: if CGN_AUTO_OK = true generate
    pkt_ok <= '1' when ((LL8_I_EOF_N_I = '0') and  (LL8_I_SRC_RDY_N_I = '0') and (full = '0')) else '0';
  end generate;



  packet_fifo_inst: entity psi_3205_v1_00_a.packet_fifo
  generic map
  (
    CGN_ADDR_WIDTH => CGN_ADDR_WIDTH,
    CGN_DATA_WIDTH => 9,
    CGN_RAM_STYLE  => CGN_RAM_STYLE
  )
  port map
  (
      CLK_I                    => CLK_I,
      RESET_I                  => RESET_I,

      WR_EN_I                  => wr_en,
      WR_PKT_OK_I              => pkt_ok,
      WR_PKT_ERR_I             => LL8_I_PKT_ERR_I,
      WR_DATA_I(7 downto 0)    => LL8_I_DATA_I,
      WR_DATA_I(8)             => LL8_I_EOF_N_I,
      WR_FULL_O                => full,

      RD_EN_I                  => rd_en,
      RD_DATA_O(7 downto 0)    => LL8_O_DATA_O,
      RD_DATA_O(8)             => ll8_ds_eof_n,
      RD_EMPTY_O               => empty
  );

  ------------------------------------------------------------------------------

  wr_rdy <= '1' when (full = '0' and LL8_I_SRC_RDY_N_I = '0') else '0';

  -- only write to Fifo after "Start Of Frame"
  wr_en <= '1' when (wr_rdy = '1') and ((LL8_I_SOF_N_I = '0') or (frame_active = '1')) else '0';


  rd_en <= '1' when (empty = '0' and LL8_O_DST_RDY_N_I = '0') else '0';


  ll8_ds_sof_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if ( (RESET_I='1') or ((rd_en = '1') and (ll8_ds_eof_n = '0')) ) then
        ll8_ds_sof_n <=  '0';
      elsif rd_en = '1' then
        ll8_ds_sof_n <=  '1';
      end if;
    end if;
  end process ll8_ds_sof_proc;


  frame_active_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if ( (RESET_I='1') or ((wr_rdy = '1') and (LL8_I_EOF_N_I = '0')) ) then
        frame_active <= '0';
      elsif ((wr_rdy = '1') and (LL8_I_SOF_N_I = '0')) then
        frame_active <= '1';
      end if;
    end if;
  end process frame_active_proc;

  ------------------------------------------------------------------------------
  LL8_O_REM_O <= (others => '1');

  LL8_I_DST_RDY_N_O <= full;
  LL8_O_SRC_RDY_N_O <= empty;

  LL8_O_SOF_N_O     <= ll8_ds_sof_n;
  LL8_O_EOF_N_O     <= ll8_ds_eof_n;

end architecture behavior;
