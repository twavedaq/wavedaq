--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : packet_fifo.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    synchronous packet fifo
--
--    data output becomes valid as soon as data is available,
--    indicated by valid flag (not empty)
--    also known as FWFT (First Word Fall Through)
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity packet_fifo is
  generic
  (
    CGN_ADDR_WIDTH  : integer := 10;
    CGN_DATA_WIDTH  : integer := 36;
    CGN_RAM_STYLE   : string  := "block"  -- style of implemented ram type:
                                          -- {auto|block|distributed|pipe_distributed};
  );
  port
  (
    CLK_I           : in  std_logic;
    RESET_I         : in  std_logic;

    WR_DATA_I       : in  std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    WR_EN_I         : in  std_logic;
    WR_PKT_OK_I     : in  std_logic;
    WR_PKT_ERR_I    : in  std_logic;
    WR_FULL_O       : out std_logic;
    WR_OVERFLOW_O   : out std_logic;

    RD_EMPTY_O      : out std_logic;
    RD_EN_I         : in  std_logic;
    RD_DATA_O       : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
  );
end packet_fifo;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of packet_fifo is

  signal wr_ptr      : std_logic_vector (CGN_ADDR_WIDTH downto 0);
  signal wr_ptr_p    : std_logic_vector (CGN_ADDR_WIDTH downto 0);
  signal rd_ptr      : std_logic_vector (CGN_ADDR_WIDTH downto 0);

  signal data        : std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
  signal fifo_empty  : std_logic;
  signal fifo_full   : std_logic;
  signal fifo_full_r : std_logic;
  signal rd_valid    : std_logic;
  signal ram_wr_en   : std_logic;
  signal ram_rd_en   : std_logic;

  signal rst            : std_logic;
  signal fifo_overflow  : std_logic;

begin


  rst <= RESET_I or fifo_overflow;


  ------------------------------------------------------------------------------
  -- dual port RAM
  ------------------------------------------------------------------------------

  ram_sdp_inst: entity psi_3205_v1_00_a.ram_sdp
  generic map
  (
    CGN_ADDR_WIDTH => CGN_ADDR_WIDTH,
    CGN_DATA_WIDTH => CGN_DATA_WIDTH,
    CGN_RAM_STYLE  => CGN_RAM_STYLE
  )
  port map
  (
     PA_CLK_I    => CLK_I,
     PA_WR_EN_I  => ram_wr_en,
     PA_ADDR_I   => wr_ptr(CGN_ADDR_WIDTH-1 downto 0),
     PA_DATA_I   => WR_DATA_I,

     PB_CLK_I    => CLK_I,
     PB_RD_EN_I  => ram_rd_en,
     PB_ADDR_I   => rd_ptr(CGN_ADDR_WIDTH-1 downto 0),
     PB_DATA_O   => data
  );

  ram_wr_en <= WR_EN_I and not fifo_full;
  ram_rd_en <= rd_valid and (fifo_empty or RD_EN_I);

  ------------------------------------------------------------------------------
  -- write pointer
  ------------------------------------------------------------------------------

  write_ptr_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if rst='1' then
        wr_ptr    <= (others => '0');
        wr_ptr_p  <= (others => '0');
      else

        if WR_PKT_ERR_I = '1' then
          wr_ptr <= wr_ptr_p;
        elsif ram_wr_en = '1' then
          wr_ptr <= wr_ptr + 1;
        end if;

        if WR_PKT_OK_I = '1' then
          if ram_wr_en = '1' then
            wr_ptr_p <= wr_ptr + 1;
          else
            wr_ptr_p <= wr_ptr;
          end if;
        end if;

      end if;
    end if;
  end process write_ptr_proc;


--------------------------------------------------------------------------------
-- read pointer
--------------------------------------------------------------------------------

  read_ptr_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if rst='1' then
        rd_ptr <= (others => '0');
      elsif ram_rd_en = '1' then
        rd_ptr <= rd_ptr + 1;
      end if;
    end if;
  end process read_ptr_proc;


  ------------------------------------------------------------------------------
  -- full and empty logic
  ------------------------------------------------------------------------------

  rd_valid  <= '1' when (rd_ptr /= wr_ptr_p) else '0';

  fifo_full <= '1' when ( ((wr_ptr(CGN_ADDR_WIDTH-1 downto 0) = rd_ptr(CGN_ADDR_WIDTH-1 downto 0)) and
                           (wr_ptr(CGN_ADDR_WIDTH) /= rd_ptr(CGN_ADDR_WIDTH))) or RESET_I = '1')   else '0';

  fifo_empty_proc : process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if rst = '1' then
        fifo_empty <= '1';
      elsif ram_rd_en = '1' then
        fifo_empty <= '0';
      elsif RD_EN_I = '1' and rd_valid ='0' then
        fifo_empty <= '1';
      end if;
    end if;
  end process fifo_empty_proc;


  ------------------------------------------------------------------------------
  -- overflow if written package is larger than fifo depth
  ------------------------------------------------------------------------------

  fifo_overflow_proc: process (CLK_I) is
  begin
    if rising_edge(CLK_I) then
      if rst='1' then
        fifo_full_r   <=  '0';
        fifo_overflow <=  '0';
      else
        fifo_full_r   <= fifo_full;
        fifo_overflow <= fifo_full_r and fifo_empty;
      end if;
    end if;
  end process fifo_overflow_proc;


  ------------------------------------------------------------------------------
  -- outputs
  ------------------------------------------------------------------------------

  RD_EMPTY_O     <= fifo_empty;
  WR_FULL_O      <= fifo_full;
  WR_OVERFLOW_O  <= fifo_overflow;
  RD_DATA_O      <= data;

end architecture behavior;

