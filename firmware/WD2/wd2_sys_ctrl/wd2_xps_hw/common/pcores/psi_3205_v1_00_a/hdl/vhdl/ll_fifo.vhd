--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_fifo.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    generic local link fifo for data_width 8, 16, 32, ...
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

library UNISIM;
use UNISIM.VComponents.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


entity ll_fifo is
  generic
  (
    C_FAMILY                 : string  := "spartan6"; -- "spartan6" -- "virtex5";
    CGN_ISE_12_4             : boolean := true;       -- true
    CGN_ISE_14_7             : boolean := false;      -- false
    CGN_MEMORY_TYPE          : integer := 1;          -- 0 = distributed RAM, 1 = BRAM
    CGN_DATA_WIDTH           : integer := 32;         -- 8, 16, 32, ...
    CGN_REM_WIDTH            : integer := 2;
    CGN_ALMOST_EMPTY_OFFSET  : integer := 128;        -- Sets the almost empty threshold
    CGN_ALMOST_FULL_OFFSET   : integer := 128;        -- Sets almost full threshold
    CGN_FIFO_DEPTH           : integer := 2**11;
    CGN_EOF_FIFO_DEPTH       : integer := 0           -- remove empty flag after eof is written to fifo
  );
  port
  (
    RESET_I                  : in  std_logic;

    LL_I_CLK_I               : in  std_logic;
    LL_I_DATA_I              : in  std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_I_SOF_N_I             : in  std_logic;
    LL_I_EOF_N_I             : in  std_logic;
    LL_I_REM_I               : in  std_logic_vector(CGN_REM_WIDTH-1 downto 0);
    LL_I_SRC_RDY_N_I         : in  std_logic;
    LL_I_DST_RDY_N_O         : out std_logic;
    LL_I_ALMOST_FULL_O       : out std_logic;
    LL_I_ERR_O               : out std_logic;

    LL_O_CLK_I               : in  std_logic;
    LL_O_DATA_O              : out std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_O_SOF_N_O             : out std_logic;
    LL_O_EOF_N_O             : out std_logic;
    LL_O_REM_O               : out std_logic_vector(CGN_REM_WIDTH-1 downto 0);
    LL_O_SRC_RDY_N_O         : out std_logic;
    LL_O_ALMOST_EMPTY_O      : out std_logic;
    LL_O_ERR_O               : out std_logic;
    LL_O_DST_RDY_N_I         : in  std_logic
  );
end ll_fifo;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll_fifo is

  constant C_FIFO_WIDTH   : integer := CGN_DATA_WIDTH / 8 * 9;

  ------------------------------------------
  -- Fifo signals
  ------------------------------------------

  signal fifo_din         : std_logic_vector(C_FIFO_WIDTH-1 downto 0);
  signal fifo_full        : std_logic;
  signal fifo_wren        : std_logic;
  signal fifo_wr_rdy      : std_logic;

  signal fifo_dout        : std_logic_vector(C_FIFO_WIDTH-1 downto 0);
  signal fifo_empty       : std_logic;
  signal fifo_rden        : std_logic;

  signal fifo_wrerr       : std_logic;
  signal fifo_rderr       : std_logic;

  signal almost_full      : std_logic;
  signal almost_empty     : std_logic;

  signal eof_fifo_full    : std_logic;
  signal eof_fifo_empty   : std_logic;
  signal eof_fifo_wr_en   : std_logic;
  signal eof_fifo_rd_en   : std_logic;

  signal ll_i_eof_n       : std_logic;
  signal ll_o_eof_n       : std_logic;

  signal ll_i_rst         : std_logic;
  signal ll_o_rst         : std_logic;

begin


  ------------------------------------------------------------------------------
  --  sync reset to in and out clock domains
  ------------------------------------------------------------------------------

  cdc_sync_ll_i_rst: entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  port map
  (
    CLK_A_I    => '0',
    CLK_B_I    => LL_I_CLK_I,
    PORT_A_I(0) => RESET_I,
    PORT_B_O(0) => ll_i_rst
  );

  cdc_sync_ll_o_rst: entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  port map
  (
    CLK_A_I    => '0',
    CLK_B_I    => LL_O_CLK_I,
    PORT_A_I(0) => RESET_I,
    PORT_B_O(0) => ll_o_rst
  );

  ------------------------------------------------------------------------------
  -- for all data widths:

  fifo_din(CGN_DATA_WIDTH-1 downto 0) <= LL_I_DATA_I;
  fifo_din(CGN_DATA_WIDTH)            <= LL_I_EOF_N_I;

  LL_O_DATA_O <= fifo_dout(CGN_DATA_WIDTH-1 downto 0);
  ll_o_eof_n  <= fifo_dout(CGN_DATA_WIDTH);


  ------------------------------------------------------------------------------
  -- CGN_DATA_WIDTH  8, 16
  ------------------------------------------------------------------------------

  gen_data_width_8_16: if CGN_DATA_WIDTH < 32 generate

    signal frame_active : std_logic;

  begin
    -- emulate start of frame signal, since we have not enough parity bits to store it

    -- only write to Fifo after "Start Of Frame"
    fifo_wren <= fifo_wr_rdy and ((not LL_I_SOF_N_I) or frame_active);

    frame_active_proc: process (LL_I_CLK_I) is
    begin
      if rising_edge(LL_I_CLK_I) then
        if ( (ll_i_rst='1') or ((fifo_wr_rdy = '1') and (LL_I_EOF_N_I = '0')) ) then
          frame_active <= '0';
        elsif ((fifo_wr_rdy = '1') and (LL_I_SOF_N_I = '0')) then
          frame_active <= '1';
        end if;
      end if;
    end process frame_active_proc;

    ll_rd_sof_proc: process (LL_O_CLK_I) is
    begin
      if rising_edge(LL_O_CLK_I) then
        if ( (ll_o_rst='1') or ((fifo_rden = '1') and (ll_o_eof_n = '0')) ) then
          LL_O_SOF_N_O <=  '0';
        elsif fifo_rden = '1' then
          LL_O_SOF_N_O <=  '1';
        end if;
      end if;
    end process ll_rd_sof_proc;

    gen_data_width_8: if CGN_DATA_WIDTH = 8 generate
    begin
      LL_O_REM_O <= (others => '0');
    end generate gen_data_width_8;

    gen_data_width_16: if CGN_DATA_WIDTH = 16 generate
    begin
      fifo_din(CGN_DATA_WIDTH+1) <= LL_I_REM_I(0);
      LL_O_REM_O(0) <= fifo_dout(CGN_DATA_WIDTH+1);
    end generate gen_data_width_16;

  end generate;

  ------------------------------------------------------------------------------
  -- CGN_DATA_WIDTH 32, 64, ..
  ------------------------------------------------------------------------------

  gen_data_width_ge_32: if CGN_DATA_WIDTH >= 32 generate
  begin
    fifo_din(CGN_DATA_WIDTH+1)                                       <= LL_I_SOF_N_I;
    fifo_din(CGN_DATA_WIDTH+CGN_REM_WIDTH+1 downto CGN_DATA_WIDTH+2) <= LL_I_REM_I;
    fifo_din(C_FIFO_WIDTH-1 downto CGN_DATA_WIDTH+CGN_REM_WIDTH+2)   <= (others => '0');

    fifo_wren    <= fifo_wr_rdy;

    LL_O_SOF_N_O <= fifo_dout(CGN_DATA_WIDTH+1);
    LL_O_REM_O   <= fifo_dout(CGN_DATA_WIDTH+1+CGN_REM_WIDTH downto CGN_DATA_WIDTH+2);
  end generate gen_data_width_ge_32;

  ------------------------------------------------------------------------------

  fifo_wr_rdy         <= (not fifo_full) and (not eof_fifo_full) and (not LL_I_SRC_RDY_N_I);
  fifo_rden           <= (not fifo_empty) and (not eof_fifo_empty) and (not LL_O_DST_RDY_N_I);

  ll_i_eof_n          <= LL_I_EOF_N_I;
  LL_O_EOF_N_O        <= ll_o_eof_n;

  LL_I_DST_RDY_N_O    <= fifo_full or eof_fifo_full;
  LL_I_ALMOST_FULL_O  <= almost_full;
  LL_I_ERR_O          <= fifo_wrerr;

  LL_O_SRC_RDY_N_O    <= fifo_empty or eof_fifo_empty;
  LL_O_ALMOST_EMPTY_O <= almost_empty;
  LL_O_ERR_O          <= fifo_rderr;


  ------------------------------------------------------------------------------
  -- eof fifo
  ------------------------------------------------------------------------------

  generate_eof_fifo: if CGN_EOF_FIFO_DEPTH > 0 generate
  begin

    eof_fifo_wr_en <= '1' when (fifo_wren = '1') and (ll_i_eof_n = '0') else '0';
    eof_fifo_rd_en <= '1' when (fifo_rden = '1') and (ll_o_eof_n = '0') else '0';

    eof_fifo_inst : entity psi_3205_v1_00_a.eof_fifo
    generic map
    (
      CGN_DEPTH => CGN_EOF_FIFO_DEPTH
    )
    port map
    (
      WR_CLK_I   => LL_I_CLK_I,
      WR_EN_I    => eof_fifo_wr_en,
      WR_FULL_O  => eof_fifo_full,
      RD_CLK_I   => LL_O_CLK_I,
      RD_EN_I    => eof_fifo_rd_en,
      RD_EMPTY_O => eof_fifo_empty,
      RESET_I    => RESET_I
    );
  end generate;

  ------------------------------------------------------------------------------
  -- no eof fifo
  ------------------------------------------------------------------------------

  generate_no_eof_fifo: if CGN_EOF_FIFO_DEPTH = 0 generate
  begin
    eof_fifo_full  <= '0';
    eof_fifo_empty <= '0';
  end generate;

  ------------------------------------------------------------------------------
  -- main fifo
  ------------------------------------------------------------------------------

  fifo_inst : entity psi_3205_v1_00_a.fifo_gen_async
  generic map
  (
    -- fpga family and tool version
    C_FAMILY            => C_FAMILY,
    C_ISE_12_4          => CGN_ISE_12_4,
    C_ISE_14_7          => CGN_ISE_14_7,
    
    -- fifo size and type
    C_DATA_WIDTH        => C_FIFO_WIDTH,
    C_FIFO_DEPTH        => CGN_FIFO_DEPTH,
    C_MEMORY_TYPE       => CGN_MEMORY_TYPE,     -- 0 = distributed RAM, 1 = BRAM

    C_USE_EMBEDDED_REG  => 0,     -- Valid only for BRAM based FIFO, otherwise needs to be set to 0
    C_PRELOAD_REGS      => 1,     -- 1 = first word fall through
    C_PRELOAD_LATENCY   => 0,     -- 0 = first word fall through,  needs to be set 2 when C_USE_EMBEDDED_REG = 1

    -- write port
    C_HAS_WR_COUNT      => 0,
    C_WR_COUNT_WIDTH    => 4,
    C_HAS_ALMOST_FULL   => 0,
    C_HAS_PROG_FULL     => 1,                        -- 0 or 1, 1 for constant prog full
    C_PROG_FULL_OFFSET  => CGN_ALMOST_FULL_OFFSET,   -- Setting determines the difference between FULL and PROG_FULL conditions
    C_HAS_WR_ACK        => 0,
    C_WR_ACK_LOW        => 0,
    C_HAS_WR_ERR        => 1,
    C_WR_ERR_LOW        => 0,

    -- read port
    C_HAS_RD_COUNT      => 0,
    C_RD_COUNT_WIDTH    => 4,
    C_HAS_ALMOST_EMPTY  => 0,
    C_HAS_PROG_EMPTY    => 1,                       -- 0 or 1, 1 for constant prog empty
    C_PROG_EMPTY_OFFSET => CGN_ALMOST_EMPTY_OFFSET, -- Setting determines the difference between EMPTY and PROG_EMPTY conditions
    C_HAS_RD_ACK        => 0,
    C_RD_ACK_LOW        => 0,
    C_HAS_RD_ERR        => 1,
    C_RD_ERR_LOW        => 0
  )
  port map
  (
    -- Common
    RESET_ASYNC_I     => RESET_I,

    -- Write Port
    WR_CLK_I          => LL_I_CLK_I,
    WR_EN_I           => fifo_wren,
    WR_DATA_I         => fifo_din,
    WR_FULL_O         => fifo_full,
    WR_ALMOST_FULL_O  => open,
    WR_PROG_FULL_O    => almost_full,
    WR_COUNT_O        => open,
    WR_ACK_O          => open,
    WR_ERR_O          => fifo_wrerr,

    -- Read Port
    RD_CLK_I          => LL_O_CLK_I,
    RD_EN_I           => fifo_rden,
    RD_DATA_O         => fifo_dout,
    RD_EMPTY_O        => fifo_empty,
    RD_ALMOST_EMPTY_O => open,
    RD_PROG_EMPTY_O   => almost_empty,
    RD_COUNT_O        => open,
    RD_ACK_O          => open,
    RD_ERR_O          => fifo_rderr
  );

end behavioral;
