--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_reg.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description : simple local link register
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_reg is
  generic
  (
    CGN_DATA_WIDTH    : integer  := 32;
    CGN_REM_WIDTH     : integer  := 2
  );
  port
  (
    LL_CLK_I          : in   std_logic;
    RESET_I           : in   std_logic;
    LL_I_DATA_I       : in   std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_I_SOF_N_I      : in   std_logic;
    LL_I_EOF_N_I      : in   std_logic;
    LL_I_REM_I        : in   std_logic_vector(CGN_REM_WIDTH-1 downto 0);
    LL_I_SRC_RDY_N_I  : in   std_logic;
    LL_I_DST_RDY_N_O  : out  std_logic;
    LL_O_DATA_O       : out  std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_O_SOF_N_O      : out  std_logic;
    LL_O_EOF_N_O      : out  std_logic;
    LL_O_REM_O        : out  std_logic_vector(CGN_REM_WIDTH-1 downto 0);
    LL_O_SRC_RDY_N_O  : out  std_logic;
    LL_O_DST_RDY_N_I  : in   std_logic
  );
end ll_reg;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll_reg is
  signal ll_data     : std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
  signal ll_sof_n    : std_logic;
  signal ll_eof_n    : std_logic;
  signal ll_rem      : std_logic_vector(CGN_REM_WIDTH-1 downto 0);
  signal reg_valid   : std_logic;
  signal reg_enable  : std_logic;

begin

  process(LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if(RESET_I = '1') then
        ll_data   <=(others => '0');
        ll_sof_n  <= '1';
        ll_eof_n  <= '1';
        ll_rem    <=(others => '1');
      elsif(reg_enable = '1') then
        ll_data   <= LL_I_DATA_I;
        ll_sof_n  <= LL_I_SOF_N_I;
        ll_eof_n  <= LL_I_EOF_N_I;
        ll_rem    <= LL_I_REM_I;
      end if;
    end if;
  end process;

  process(LL_CLK_I)

  begin
    if rising_edge(LL_CLK_I) then
      if(RESET_I = '1') then
        reg_valid  <= '0';
      elsif(LL_I_SRC_RDY_N_I = '0') then
        reg_valid  <= '1';
      elsif(LL_O_DST_RDY_N_I = '0') then
        reg_valid  <= '0';
      end if;
    end if;

  end process;

  reg_enable        <=(not LL_I_SRC_RDY_N_I) and((not reg_valid) or(not LL_O_DST_RDY_N_I));

  LL_I_DST_RDY_N_O  <= reg_valid and LL_O_DST_RDY_N_I;
  LL_O_SRC_RDY_N_O  <= not reg_valid;
  LL_O_DATA_O       <= ll_data;
  LL_O_SOF_N_O      <= ll_sof_n;
  LL_O_EOF_N_O      <= ll_eof_n;
  LL_O_REM_O        <= ll_rem;

end behavioral;
