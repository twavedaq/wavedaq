--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : eof_fifo.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    Async Fifo for counting End Of Frame signals
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity eof_fifo is
  generic
  (
    CGN_DEPTH : integer := 3
  );
  port
  (
    WR_CLK_I   : in  std_logic;
    WR_EN_I    : in  std_logic;
    WR_FULL_O  : out std_logic;

    RD_CLK_I   : in  std_logic;
    RD_EN_I    : in  std_logic;
    RD_EMPTY_O : out std_logic;

    RESET_I    : in  std_logic
  );
end eof_fifo;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavior of eof_fifo is

  function log2 (val: integer) return natural is
    variable res : natural;
  begin
    for i in 0 to 31 loop
      if (val <= (2**i)) then
        res := i;
        exit;
      end if;
    end loop;
    return res;
  end function Log2;

  constant C_NUM_BITS : integer := log2(CGN_DEPTH);

  signal wr_ptr     : std_logic_vector (C_NUM_BITS downto 0);
  signal wr_ptr_cdc : std_logic_vector (C_NUM_BITS downto 0);
  signal rd_ptr     : std_logic_vector (C_NUM_BITS downto 0);
  signal rd_ptr_cdc : std_logic_vector (C_NUM_BITS downto 0);
  ----
  signal rst_wr     : std_logic;
  signal rst_rd     : std_logic;
  ----
  signal full       : std_logic;
  signal empty      : std_logic;

begin

  ------------------------------------------------------------------------------
  -- WR CLK DOMAIN
  ------------------------------------------------------------------------------

  -- sync reset
  cdc_sync_wr_rst: entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I    => '0',
    CLK_B_I    => WR_CLK_I,
    PORT_A_I(0) => RESET_I,
    PORT_B_O(0) => rst_wr
  );

  -- write pointer
  wr_ptr_proc: process (WR_CLK_I)
  begin
    if rising_edge(WR_CLK_I) then
      if rst_wr = '1' then
        wr_ptr  <= (others => '0');
      elsif WR_EN_I = '1' then
        wr_ptr  <= wr_ptr + 1;
      end if;
    end if;
  end process wr_ptr_proc;

  ------------------------------------------------------------------------------
  -- RD CLK DOMAIN
  ------------------------------------------------------------------------------

  -- sync reset
  cdc_sync_rd_rst: entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  0,
    CGN_USE_GRAY_CONVERSION  =>  0,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  0,
    CGN_GRAY_TO_BIN_STYLE    =>  0
  )
  PORT MAP
  (
    CLK_A_I    => '0',
    CLK_B_I    => RD_CLK_I,
    PORT_A_I(0) => RESET_I,
    PORT_B_O(0) => rst_rd
  );

  -- read pointer
  rd_ptr_proc: process (RD_CLK_I)
  begin
    if rising_edge(RD_CLK_I) then
      if rst_rd = '1' then
        rd_ptr  <= (others => '0');
      elsif RD_EN_I = '1' then
        rd_ptr  <= rd_ptr + 1;
      end if;
    end if;
  end process rd_ptr_proc;

  ------------------------------------------------------------------------------
  -- CLK DOMAIN CROSSING
  ------------------------------------------------------------------------------

  cdc_sync_wr_ptr: entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  C_NUM_BITS+1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  1,
    CGN_USE_GRAY_CONVERSION  =>  1,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  1,
    CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
  )
  PORT MAP
  (
    CLK_A_I => WR_CLK_I,
    CLK_B_I => RD_CLK_I,

    PORT_A_I => wr_ptr,
    PORT_B_O => wr_ptr_cdc
  );


  cdc_sync_rd_ptr: entity psi_3205_v1_00_a.cdc_sync
  generic map
  (
    CGN_DATA_WIDTH           =>  C_NUM_BITS+1,
    CGN_USE_INPUT_REG_A      =>  0,
    CGN_USE_OUTPUT_REG_A     =>  1,
    CGN_USE_GRAY_CONVERSION  =>  1,
    CGN_NUM_SYNC_REGS_B      =>  2,
    CGN_NUM_OUTPUT_REGS_B    =>  1,
    CGN_GRAY_TO_BIN_STYLE    =>  0 --  0: serial, 1: Kogge-Stone, 2: Sklansky
  )
  PORT MAP
  (
    CLK_A_I => RD_CLK_I,
    CLK_B_I => WR_CLK_I,

    PORT_A_I => rd_ptr,
    PORT_B_O => rd_ptr_cdc
  );

  ------------------------------------------------------------------------------
  -- full and empty flags
  ------------------------------------------------------------------------------

  empty <= '1' when (rd_ptr = wr_ptr_cdc) else '0';

  full  <= '1' when ( ((wr_ptr(C_NUM_BITS-1 downto 0) = rd_ptr_cdc(C_NUM_BITS-1 downto 0)) and
                       (wr_ptr(C_NUM_BITS) /= rd_ptr_cdc(C_NUM_BITS)))  ) else
           '0';

  ------------------------------------------------------------------------------
  -- outputs
  ------------------------------------------------------------------------------

  RD_EMPTY_O <= empty;
  WR_FULL_O  <= full;

end architecture behavior;
