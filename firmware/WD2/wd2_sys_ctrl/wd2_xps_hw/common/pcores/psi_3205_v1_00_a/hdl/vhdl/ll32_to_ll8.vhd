--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll32_to_ll8.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  local link width conversion
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll32_to_ll8 is
  generic
  (
    C_INPUTREG          : integer := 1
  );
  port
  (
    LL_CLK_I            : in  std_logic;
    RESET_I             : in  std_logic;

    LL32_I_DATA_I       : in  std_logic_vector (31 downto 0);
    LL32_I_SOF_N_I      : in  std_logic;
    LL32_I_EOF_N_I      : in  std_logic;
    LL32_I_REM_I        : in  std_logic_vector (1 downto 0);
    LL32_I_SRC_RDY_N_I  : in  std_logic;
    LL32_I_DST_RDY_N_O  : out std_logic;

    LL8_O_DATA_O        : out std_logic_vector(7 downto 0);
    LL8_O_SOF_N_O       : out std_logic;
    LL8_O_EOF_N_O       : out std_logic;
    LL8_O_SRC_RDY_N_O   : out std_logic;
    LL8_O_DST_RDY_N_I   : in  std_logic
  );

end ll32_to_ll8;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll32_to_ll8 is

  signal byte_sel           : std_logic_vector (1 downto 0);
  signal ll32_rem           : std_logic_vector (1 downto 0);

  signal si_ll32_data       : std_logic_vector (31 downto 0);
  signal si_ll32_sof_n      : std_logic;
  signal si_ll32_eof_n      : std_logic;
  signal si_ll32_rem        : std_logic_vector (1 downto 0);
  signal si_ll32_src_rdy_n  : std_logic;
  signal so_ll32_dst_rdy_n  : std_logic;

begin

  use_input_reg: if C_INPUTREG > 0 generate
  begin
    ll_reg_inst: entity psi_3205_v1_00_a.ll_reg
      generic map
      (
       CGN_DATA_WIDTH => 32,
       CGN_REM_WIDTH  => 2
      )
      port map
      (
        LL_CLK_I          => LL_CLK_I,
        RESET_I           => RESET_I,

        LL_I_DATA_I       => LL32_I_DATA_I,
        LL_I_SOF_N_I      => LL32_I_SOF_N_I,
        LL_I_EOF_N_I      => LL32_I_EOF_N_I,
        LL_I_REM_I        => LL32_I_REM_I,
        LL_I_SRC_RDY_N_I  => LL32_I_SRC_RDY_N_I,
        LL_I_DST_RDY_N_O  => LL32_I_DST_RDY_N_O,

        LL_O_DATA_O       => si_ll32_data,
        LL_O_SOF_N_O      => si_ll32_sof_n,
        LL_O_EOF_N_O      => si_ll32_eof_n,
        LL_O_REM_O        => si_ll32_rem,
        LL_O_SRC_RDY_N_O  => si_ll32_src_rdy_n,
        LL_O_DST_RDY_N_I  => so_ll32_dst_rdy_n
      );
    end generate use_input_reg;

  no_input_reg: if C_INPUTREG = 0 generate
  begin
    si_ll32_data       <= LL32_I_DATA_I;
    si_ll32_sof_n      <= LL32_I_SOF_N_I;
    si_ll32_eof_n      <= LL32_I_EOF_N_I;
    si_ll32_rem        <= LL32_I_REM_I;
    si_ll32_src_rdy_n  <= LL32_I_SRC_RDY_N_I;
    LL32_I_DST_RDY_N_O   <= so_ll32_dst_rdy_n;
  end generate no_input_reg;

-- valid bytes my be <4 only allowed if EOF is true
  ll32_rem  <=  si_ll32_rem when si_ll32_eof_n ='0' else "11";

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (si_ll32_src_rdy_n ='1') or ((LL8_O_DST_RDY_N_I = '0') and (byte_sel = ll32_rem))  then
        byte_sel <= (others => '0');
      elsif (LL8_O_DST_RDY_N_I = '0') then
        byte_sel <= byte_sel + 1;
      end if;
    end if;
  end process;

  so_ll32_dst_rdy_n <= '1' when RESET_I = '1' else
                       '0' when (byte_sel = ll32_rem) and (LL8_O_DST_RDY_N_I = '0') else
                       '0' when (si_ll32_src_rdy_n ='1') else
                       '1';

  LL8_O_SRC_RDY_N_O  <= '0' when byte_sel /= "00" else
                        si_ll32_src_rdy_n;

--  LL8_O_DATA_O <= si_ll32_data(31 downto 24) when (byte_sel = "11") else
--                si_ll32_data(23 downto 16) when (byte_sel = "10") else
--                si_ll32_data(15 downto  8) when (byte_sel = "01") else
--                si_ll32_data( 7 downto  0);

  LL8_O_DATA_O  <= si_ll32_data(31 downto 24) when(byte_sel = "00") else
                   si_ll32_data(23 downto 16) when(byte_sel = "01") else
                   si_ll32_data(15 downto  8) when(byte_sel = "10") else
                   si_ll32_data( 7 downto  0);

  LL8_O_SOF_N_O <= '0' when (si_ll32_sof_n = '0') and (byte_sel = "00") else '1';

  LL8_O_EOF_N_O <= '0' when (si_ll32_eof_n = '0') and (byte_sel = ll32_rem) else '1';

end behavioral;
