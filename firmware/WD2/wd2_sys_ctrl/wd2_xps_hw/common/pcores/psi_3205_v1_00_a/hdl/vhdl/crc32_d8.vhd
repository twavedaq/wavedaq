--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : crc32_d8.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    CRC-32 (IEEE 802.3) genarator
--    x^32 + x^26 + x^23 + x^22 + x^16 + x^12 + x^11 + x^10 + x^8 + x^7 + x^5 + x^4 + x^2 + x + 1
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity crc32_d8 is
port
(
  CLK_I        : in  std_logic;
  RESET_I      : in  std_logic;
  DATA_I       : in  std_logic_vector(7 downto 0);
  DATA_VALID_I : in  std_logic;
  CRC_O        : out std_logic_vector(31 downto 0);
  CRC_VALID_O  : out std_logic
);
end crc32_d8;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behaviour of crc32_d8 is

  signal crc : std_logic_vector(31 downto 0);
  constant C_CRC32_RESIDUE : std_logic_vector(31 downto 0) := x"2144DF1C";
begin

  crc_proc: process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if (RESET_I = '1') then
        crc <= (others => '0');
      elsif (DATA_VALID_I = '1') then
        -- grouping xor terms with parentheses helped Xilinx ISE (14.7) to use fewer resources
        crc(31) <= not (((not crc(7)) xor DATA_I(7)) xor ((not crc(1)) xor DATA_I(1)));
        crc(30) <= not (((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(29) <= not (((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(28) <= not (((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(0)) xor DATA_I(0)));
        crc(27) <= not (((not crc(7)) xor DATA_I(7)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(1)) xor DATA_I(1)));
        crc(26) <= not (((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(25) <= not (((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(24) <= not (((not crc(7)) xor DATA_I(7)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(0)) xor DATA_I(0)));
        crc(23) <= not ((not crc(31)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)));
        crc(22) <= not ((not crc(30)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)));
        crc(21) <= not ((not crc(29)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(2)) xor DATA_I(2)));
        crc(20) <= not ((not crc(28)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)));
        crc(19) <= not ((not crc(27)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(1)) xor DATA_I(1)));
        crc(18) <= not ((not crc(26)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(17) <= not ((not crc(25)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(16) <= not ((not crc(24)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(0)) xor DATA_I(0)));
        crc(15) <= not ((not crc(23)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)));
        crc(14) <= not ((not crc(22)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(1)) xor DATA_I(1)));
        crc(13) <= not ((not crc(21)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(12) <= not ((not crc(20)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(0)) xor DATA_I(0)));
        crc(11) <= not ((not crc(19)) xor ((not crc(3)) xor DATA_I(3)));
        crc(10) <= not ((not crc(18)) xor ((not crc(2)) xor DATA_I(2)));
        crc(9)  <= not ((not crc(17)) xor ((not crc(7)) xor DATA_I(7)));
        crc(8)  <= not ((not crc(16)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(1)) xor DATA_I(1)));
        crc(7)  <= not ((not crc(15)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(0)) xor DATA_I(0)));
        crc(6)  <= not ((not crc(14)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(4)) xor DATA_I(4)));
        crc(5)  <= not ((not crc(13)) xor ((not crc(7)) xor DATA_I(7)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(1)) xor DATA_I(1)));
        crc(4)  <= not ((not crc(12)) xor ((not crc(6)) xor DATA_I(6)) xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(0)) xor DATA_I(0)));
        crc(3)  <= not ((not crc(11)) xor ((not crc(5)) xor DATA_I(5)) xor ((not crc(2)) xor DATA_I(2)) xor ((not crc(1)) xor DATA_I(1)));
        crc(2)  <= not ((not crc(10)) xor ((not crc(4)) xor DATA_I(4)) xor ((not crc(1)) xor DATA_I(1)) xor ((not crc(0)) xor DATA_I(0)));
        crc(1)  <= not ((not crc(9))  xor ((not crc(3)) xor DATA_I(3)) xor ((not crc(0)) xor DATA_I(0)));
        crc(0)  <= not ((not crc(8))  xor ((not crc(2)) xor DATA_I(2)));
      end if;
    end if;
  end process;

  CRC_O <= crc;
  CRC_VALID_O <= '1' when crc = C_CRC32_RESIDUE else '0';

end architecture behaviour;
