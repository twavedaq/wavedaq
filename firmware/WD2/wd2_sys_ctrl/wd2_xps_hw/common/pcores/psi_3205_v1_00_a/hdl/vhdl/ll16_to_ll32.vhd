--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll16_to_ll32.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  local link width conversion
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll16_to_ll32 is
  generic
  (
    CGN_LL_I_BUFF       : integer := 1;
    CGN_LL_O_BUFF       : integer := 1
  );
  port
  (
    LL_CLK_I            : in  std_logic;
    RESET_I             : in  std_logic;

    LL16_I_DATA_I       : in  std_logic_vector(15 downto 0);
    LL16_I_SOF_N_I      : in  std_logic;
    LL16_I_EOF_N_I      : in  std_logic;
    LL16_I_REM_I        : in  std_logic_vector(0 downto 0);
    LL16_I_SRC_RDY_N_I  : in  std_logic;
    LL16_I_DST_RDY_N_O  : out std_logic;

    LL32_O_DATA_O       : out std_logic_vector(31 downto 0);
    LL32_O_SOF_N_O      : out std_logic;
    LL32_O_EOF_N_O      : out std_logic;
    LL32_O_REM_O        : out std_logic_vector(1 downto 0);
    LL32_O_SRC_RDY_N_O  : out std_logic;
    LL32_O_DST_RDY_N_I  : in  std_logic
  );
end ll16_to_ll32;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll16_to_ll32 is

  signal sword_sel    : std_logic;
  signal ll32_rem     : std_logic_vector (1 downto 0);
  signal ll32_sof     : std_logic;
  signal ll32_eof     : std_logic;
  signal reg_valid    : std_logic;

  signal shortword0   : std_logic_vector (15 downto 0);
  signal shortword1   : std_logic_vector (15 downto 0);

  signal sword_en0    : std_logic;
  signal sword_en1    : std_logic;

  signal enable_gate  : std_logic;

  ------------------------------------------------------------------------------

  signal ll16_us_data       : std_logic_vector (15 downto 0);
  signal ll16_us_sof_n      : std_logic;
  signal ll16_us_eof_n      : std_logic;
  signal ll16_us_rem        : std_logic_vector (0 downto 0);
  signal ll16_us_src_rdy_n  : std_logic;
  signal ll16_us_dst_rdy_n  : std_logic;

  signal ll32_ds_data       : std_logic_vector (31 downto 0);
  signal ll32_ds_sof_n      : std_logic;
  signal ll32_ds_eof_n      : std_logic;
  signal ll32_ds_rem        : std_logic_vector (1 downto 0);
  signal ll32_ds_src_rdy_n  : std_logic;
  signal ll32_ds_dst_rdy_n  : std_logic;

begin

  use_buff_us: if CGN_LL_I_BUFF > 0 generate
  begin

    dbuff_us : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 16,
     CGN_REM_WIDTH  => 1
    )
    port map
    (
      CLK_I             => LL_CLK_I,
      RESET_I           => RESET_I,

      LL_I_DATA_I      => LL16_I_DATA_I,
      LL_I_SOF_N_I     => LL16_I_SOF_N_I,
      LL_I_EOF_N_I     => LL16_I_EOF_N_I,
      LL_I_REM_I       => LL16_I_REM_I,
      LL_I_SRC_RDY_N_I => LL16_I_SRC_RDY_N_I,
      LL_I_DST_RDY_N_O => LL16_I_DST_RDY_N_O,

      LL_O_DATA_O      => ll16_us_data,
      LL_O_SOF_N_O     => ll16_us_sof_n,
      LL_O_EOF_N_O     => ll16_us_eof_n,
      LL_O_REM_O       => ll16_us_rem,
      LL_O_SRC_RDY_N_O => ll16_us_src_rdy_n,
      LL_O_DST_RDY_N_I => ll16_us_dst_rdy_n
    );
  end generate use_buff_us;

  no_buff_us: if CGN_LL_I_BUFF = 0 generate
  begin
    ll16_us_data         <= LL16_I_DATA_I;
    ll16_us_sof_n        <= LL16_I_SOF_N_I;
    ll16_us_eof_n        <= LL16_I_EOF_N_I;
    ll16_us_rem          <= LL16_I_REM_I;
    ll16_us_src_rdy_n    <= LL16_I_SRC_RDY_N_I;
    LL16_I_DST_RDY_N_O  <= ll16_us_dst_rdy_n;
  end generate no_buff_us;

  ------------------------------------------------------------------------------

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        shortword0 <= x"0000";
      elsif (sword_en0 = '1') then
        shortword0 <= ll16_us_data;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (sword_en0 = '1') then
        shortword1 <= x"0000";
      elsif (sword_en1 = '1') then
        shortword1 <= ll16_us_data;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll32_rem <= "11";
      elsif (enable_gate = '1') then
        ll32_rem(0) <= ll16_us_rem(0);
        ll32_rem(1) <= sword_sel;
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll32_sof <= '1';
      elsif ((enable_gate = '1') and (ll16_us_sof_n = '0')) then
        ll32_sof <= '0';
      elsif (sword_en0 = '1') then
        ll32_sof <= '1';
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        ll32_eof <= '1';
      elsif ((enable_gate = '1') and (ll16_us_eof_n = '0')) then
        ll32_eof <= '0';
      elsif (sword_en0 = '1') then
        ll32_eof <= '1';
      end if;
    end if;
  end process;

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') then
        reg_valid <= '0';
      elsif (sword_en1 = '1') or ((enable_gate = '1') and (ll16_us_eof_n = '0')) then
        reg_valid <= '1';
      elsif (ll32_ds_dst_rdy_n = '0') then
        reg_valid <= '0';
      end if;
    end if;
  end process;

  sword_sel <= '0' when (ll16_us_sof_n = '0') else
                not ll32_rem(1);

  enable_gate <= (not ll16_us_src_rdy_n) and (not reg_valid or not ll32_ds_dst_rdy_n);

  sword_en0 <= '1' when (enable_gate = '1') and ( sword_sel = '0') else '0';
  sword_en1 <= '1' when (enable_gate = '1') and ( sword_sel = '1') else '0';


--ll16_us_dst_rdy_n  <= not (not reg_valid or not ll32_ds_dst_rdy_n);
  ll16_us_dst_rdy_n  <= reg_valid and ll32_ds_dst_rdy_n;

  ll32_ds_src_rdy_n <= not reg_valid;
  ll32_ds_data      <= shortword0 & shortword1;
  ll32_ds_sof_n     <= ll32_sof;
  ll32_ds_eof_n     <= ll32_eof;
  ll32_ds_rem       <= ll32_rem;

  ------------------------------------------------------------------------------

  use_buff_ds: if CGN_LL_O_BUFF > 0 generate
  begin
    dbuff_ds : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 32,
     CGN_REM_WIDTH  => 2
    )
    port map
    (
      CLK_I             => LL_CLK_I,
      RESET_I           => RESET_I,

      LL_I_DATA_I      => ll32_ds_data,
      LL_I_SOF_N_I     => ll32_ds_sof_n,
      LL_I_EOF_N_I     => ll32_ds_eof_n,
      LL_I_REM_I       => ll32_ds_rem,
      LL_I_SRC_RDY_N_I => ll32_ds_src_rdy_n,
      LL_I_DST_RDY_N_O => ll32_ds_dst_rdy_n,

      LL_O_DATA_O      => LL32_O_DATA_O,
      LL_O_SOF_N_O     => LL32_O_SOF_N_O,
      LL_O_EOF_N_O     => LL32_O_EOF_N_O,
      LL_O_REM_O       => LL32_O_REM_O,
      LL_O_SRC_RDY_N_O => LL32_O_SRC_RDY_N_O,
      LL_O_DST_RDY_N_I => LL32_O_DST_RDY_N_I
    );
  end generate use_buff_ds;

  no_buff_ds: if CGN_LL_O_BUFF = 0 generate
  begin
    LL32_O_DATA_O      <= ll32_ds_data;
    LL32_O_SOF_N_O     <= ll32_ds_sof_n;
    LL32_O_EOF_N_O     <= ll32_ds_eof_n;
    LL32_O_REM_O       <= ll32_ds_rem;
    LL32_O_SRC_RDY_N_O <= ll32_ds_src_rdy_n;
    ll32_ds_dst_rdy_n   <= LL32_O_DST_RDY_N_I;
  end generate no_buff_ds;

end behavioral;
