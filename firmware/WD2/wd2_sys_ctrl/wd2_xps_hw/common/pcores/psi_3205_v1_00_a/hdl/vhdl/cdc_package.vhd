--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : cdc_package.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  Clock Domain Crossing
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Package header
--------------------------------------------------------------------------------

package cdc_package is

  function log2ceil(n : integer) return integer;
  ----------------------------------------------------------
  component bin2gray is
    generic
    (
      CGN_DATA_WIDTH  : integer := 16
    );
    port
    (
      DATA_BIN_I   : in     std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
      DATA_GRAY_O  : out    std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
    );
  end component ;
  ----------------------------------------------------------
  component gray2bin is
    generic
    (
      CGN_DATA_WIDTH : integer := 16;
      CGN_STYLE      : integer := 0   --  0: serial structure
                                      --  1: Kogge-Stone parallel prefix
                                      --  2: Sklansky parallel-prefix propagate-lookahead structure
    );
    port
    (
      DATA_GRAY_I : in     std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
      DATA_BIN_O  : out    std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
    );
  end component;
  ----------------------------------------------------------
  component cdc_sync is
  generic
  (
    CGN_DATA_WIDTH           : integer := 16;

    CGN_USE_INPUT_REG_A      : integer := 1;
    CGN_USE_OUTPUT_REG_A     : integer := 1;
    CGN_USE_GRAY_CONVERSION  : integer := 1;
    CGN_NUM_SYNC_REGS_B      : integer := 2;
    CGN_NUM_OUTPUT_REGS_B    : integer := 1;

    CGN_GRAY_TO_BIN_STYLE    : integer := 0   --  0: serial structure
                                              --  1: Kogge-Stone parallel prefix
                                              --  2: Sklansky parallel-prefix propagate-lookahead structure
  );
  port
  (
    CLK_A_I : in  std_logic;
    CLK_B_I : in  std_logic;

    PORT_A_I : in  std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    PORT_B_O : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
  );
  end component ;

end;

--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body cdc_package is

  -----------------------------------------------------
  -- purpose: computes ceil(log2(n))
  -----------------------------------------------------

  function log2ceil(n : integer) return integer is
    variable m, p  : integer;
  begin
    m  := 0;
    p  := 1;
    for i in 0 to n loop
      if p < n then
        m  := m+1;
        p  := p*2;
      end if;
    end loop;
    return m;
  end function log2ceil;

end package body;

----------------------------------------------------------------------------------
-- Binary to Gray Conversion
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

entity bin2gray is
  generic
  (
    CGN_DATA_WIDTH  : integer := 15
  );
  port
  (
    DATA_BIN_I   : in     std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    DATA_GRAY_O  : out    std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
  );
end bin2gray;

----------------------------------------------------------------------------------

architecture behave of bin2gray is
begin

  gen_single: if (CGN_DATA_WIDTH = 1) generate
  begin
    DATA_GRAY_O <= DATA_BIN_I;
  end generate gen_single;

  gen_vector: if (CGN_DATA_WIDTH > 1) generate
  begin
    DATA_GRAY_O <= DATA_BIN_I(CGN_DATA_WIDTH-1 downto 0)  xor '0' & DATA_BIN_I(CGN_DATA_WIDTH-1 downto 1) ;
  end generate gen_vector;

end architecture behave;

----------------------------------------------------------------------------------
-- Gray to Binary Conversion
----------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity gray2bin is
  generic
  (
    CGN_DATA_WIDTH   : integer := 15;
    CGN_STYLE        : integer := 0
  );
  port
  (
    DATA_GRAY_I : in     std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    DATA_BIN_O  : out    std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
  );
end gray2bin ;

----------------------------------------------------------------------------------

architecture behave of gray2bin is
  constant CLC_M  : natural  := log2ceil(CGN_DATA_WIDTH); -- prefix structure depth
begin

  gen_single: if (CGN_DATA_WIDTH = 1) generate
  begin
    DATA_BIN_O <= DATA_GRAY_I;
  end generate;


  gen_vector: if (CGN_DATA_WIDTH > 1) generate
  begin
    ----------------------------------------------------------------------------
    -- serial structure

    gen_serial: if (CGN_STYLE = 0) generate
      signal temp_sig  : std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    begin

      temp_sig   <= DATA_GRAY_I xor '0' & temp_sig(CGN_DATA_WIDTH-1 downto 1);
      DATA_BIN_O  <= temp_sig;

    end generate gen_serial;

    ----------------------------------------------------------------------------
    -- Kogge-Stone parallel prefix

    gen_kogge_stone: if (CGN_STYLE = 1) generate
    begin

      process (DATA_GRAY_I)
        variable temp_var  : std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
      begin
        temp_var  := DATA_GRAY_I;
        levels : for l in 0 to CLC_M-1 loop
          temp_var(CGN_DATA_WIDTH-2**l-1 downto 0) := temp_var(CGN_DATA_WIDTH-2**l-1 downto 0) xor temp_var(CGN_DATA_WIDTH-1 downto 2**l);
        end loop;
        DATA_BIN_O  <= temp_var;
      end process;

    end generate gen_kogge_stone;

    ----------------------------------------------------------------------------
    -- Sklansky parallel-prefix propagate-lookahead structure

    gen_sklansky: if (CGN_STYLE = 2) generate
    begin

      process (DATA_GRAY_I)
        variable temp_var  : std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
      begin
        temp_var  := DATA_GRAY_I;
        levels : for l in 1 to CLC_M loop
          groups : for k in 0 to 2**(CLC_M-l)-1 loop
            bits : for i in 0 to 2**(l-1)-1 loop
              black : if(k*2**l+2**(l-1)+i) < CGN_DATA_WIDTH then temp_var(CGN_DATA_WIDTH-1-k*2**l-2**(l-1)-i) := temp_var(CGN_DATA_WIDTH-1-k*2**l-2**(l-1)-i) xor temp_var(CGN_DATA_WIDTH-k*2**l-2**(l-1));
              end if black;
            end loop bits;
          end loop groups;
        end loop levels;
        DATA_BIN_O  <= temp_var;
      end process;

    end generate gen_sklansky;
    --------------------------------------------------------------------------

  end generate gen_vector;

end architecture behave;


--------------------------------------------------------------------------------
-- Sync Clock Domain Crossing
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;


entity cdc_sync is
  generic
  (
    -- Total_Latency = CGN_USE_INPUT_REG_A + CGN_USE_OUTPUT_REG_A + CGN_NUM_SYNC_REGS_B + CGN_NUM_OUTPUT_REGS_B
    -- Use timing ignore constraint in UCF:
    -- NET "*sig_cdc_ignore_timing*" TIG;

    CGN_DATA_WIDTH           : integer := 16;

    CGN_USE_INPUT_REG_A      : integer := 1;
    CGN_USE_OUTPUT_REG_A     : integer := 1;
    CGN_USE_GRAY_CONVERSION  : integer := 1;
    CGN_NUM_SYNC_REGS_B      : integer := 2;
    CGN_NUM_OUTPUT_REGS_B    : integer := 1;

    CGN_GRAY_TO_BIN_STYLE    : integer := 0   --  0: serial structure
                                              --  1: Kogge-Stone parallel prefix
                                              --  2: Sklansky parallel-prefix propagate-lookahead structure
  );
  port
  (
    CLK_A_I : in  std_logic;
    CLK_B_I : in  std_logic;

    PORT_A_I : in  std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    PORT_B_O : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
  );

end cdc_sync ;

--------------------------------------------------------------------------------

architecture behave of cdc_sync is

  subtype sig_vector_type is std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
  type sig_s2_b_type is array(0 to CGN_NUM_OUTPUT_REGS_B) of sig_vector_type;

  signal sig_s1_a              : sig_vector_type;
  signal sig_s2_a              : sig_vector_type;
  signal sig_cdc_ignore_timing : sig_vector_type;
  signal sig_s1_b_out          : sig_vector_type;

  signal sig_s2_b              : sig_s2_b_type;

  attribute keep : string;
  attribute keep of sig_cdc_ignore_timing: signal is "TRUE";
--  attribute keep of sig_s1_b:              signal is "TRUE";

begin

  ------------------------------------------------------------------------------
  -- Clock Domain A
  ------------------------------------------------------------------------------

  ---- stage 1

  gen_no_input_reg_a: if (CGN_USE_INPUT_REG_A = 0) generate
  begin
    sig_s1_a <= PORT_A_I;
  end generate gen_no_input_reg_a;


  gen_use_input_reg_a: if (CGN_USE_INPUT_REG_A = 1) generate
  begin

    process (CLK_A_I)
    begin
      if rising_edge(CLK_A_I) then
        sig_s1_a <= PORT_A_I;
      end if;
    end process;

  end generate gen_use_input_reg_a;

  ---- stage 2

  gen_no_gray_conversion_a: if (CGN_USE_GRAY_CONVERSION = 0) generate
  begin
    sig_s2_a <= sig_s1_a;
  end generate gen_no_gray_conversion_a;

  gen_use_gray_conversion_a: if (CGN_USE_GRAY_CONVERSION = 1) generate
  begin

    bin2gray_inst : bin2gray
    generic map
    (
      CGN_DATA_WIDTH  => CGN_DATA_WIDTH
    )
    port map
    (
      DATA_BIN_I  => sig_s1_a,
      DATA_GRAY_O => sig_s2_a
    );

  end generate gen_use_gray_conversion_a;

  ---- stage 3

  gen_no_output_reg_a: if (CGN_USE_OUTPUT_REG_A = 0) generate
  begin
    sig_cdc_ignore_timing <= sig_s2_a;
  end generate gen_no_output_reg_a;

  gen_use_output_reg_a: if (CGN_USE_OUTPUT_REG_A = 1) generate
  begin

    process (CLK_A_I)
    begin
      if rising_edge(CLK_A_I) then
        sig_cdc_ignore_timing <= sig_s2_a;
      end if;
    end process;

  end generate gen_use_output_reg_a;

  ------------------------------------------------------------------------------
  -- Clock Domain B
  ------------------------------------------------------------------------------

  ----  stage 1

  gen_no_sync_reg_b : if CGN_NUM_SYNC_REGS_B=0 generate
    sig_s1_b_out <= sig_cdc_ignore_timing;
  end generate gen_no_sync_reg_b;
  
  gen_sync_reg_b : if CGN_NUM_SYNC_REGS_B>0 generate
    type sig_s1_b_type is array(0 to CGN_NUM_SYNC_REGS_B-1)   of sig_vector_type;
    signal sig_s1_b              : sig_s1_b_type;
    attribute keep : string;
    attribute keep of sig_s1_b:  signal is "TRUE";
  begin

    process (CLK_B_I)
    begin
      if rising_edge(CLK_B_I) then
        sig_s1_b(0) <= sig_cdc_ignore_timing;
        if CGN_NUM_SYNC_REGS_B > 1 then
          for i in 1 to CGN_NUM_SYNC_REGS_B-1 loop
            sig_s1_b(i) <= sig_s1_b(i-1);
          end loop;
        end if;
      end if;
    end process;
    sig_s1_b_out <= sig_s1_b(CGN_NUM_SYNC_REGS_B-1);
  end generate gen_sync_reg_b;


  ----  stage 2
  gen_no_gray_conversion_b: if (CGN_USE_GRAY_CONVERSION = 0) generate
  begin
    sig_s2_b(0) <= sig_s1_b_out;
  end generate gen_no_gray_conversion_b;

  gen_use_gray_conversion_b: if (CGN_USE_GRAY_CONVERSION = 1) generate
  begin

    gray2bin_inst : gray2bin
    generic map
    (
      CGN_DATA_WIDTH  => CGN_DATA_WIDTH,
      CGN_STYLE       => CGN_GRAY_TO_BIN_STYLE
    )
    port map
    (
      DATA_GRAY_I => sig_s1_b_out,
      DATA_BIN_O  => sig_s2_b(0)
    );

  end generate gen_use_gray_conversion_b;

  ----  stage 3

  gen_outreg : if CGN_NUM_OUTPUT_REGS_B>0 generate
    gen_sig_s2_b:  for i in 1 to CGN_NUM_OUTPUT_REGS_B generate
      process (CLK_B_I)
      begin
        if rising_edge(CLK_B_I) then
          sig_s2_b(i) <= sig_s2_b(i-1);
        end if;
      end process;
    end generate gen_sig_s2_b;
  end generate gen_outreg;

  PORT_B_O <= sig_s2_b(CGN_NUM_OUTPUT_REGS_B);


end architecture behave;
