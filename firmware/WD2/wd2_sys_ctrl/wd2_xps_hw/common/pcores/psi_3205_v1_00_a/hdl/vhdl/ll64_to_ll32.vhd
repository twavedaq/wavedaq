--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll64_to_ll32.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :  local link width conversion
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll64_to_ll32 is
  generic
  (
    CGN_LL_I_BUFF       : integer := 1;
    CGN_LL_O_BUFF       : integer := 1
  );
  port
  (
    LL_CLK_I            : in  std_logic;
    RESET_I             : in  std_logic;

    LL64_I_DATA_I       : in  std_logic_vector(63 downto 0);
    LL64_I_SOF_N_I      : in  std_logic;
    LL64_I_EOF_N_I      : in  std_logic;
    LL64_I_REM_I        : in  std_logic_vector(2 downto 0);
    LL64_I_SRC_RDY_N_I  : in  std_logic;
    LL64_I_DST_RDY_N_O  : out std_logic;

    LL32_O_DATA_O       : out std_logic_vector(31 downto 0);
    LL32_O_SOF_N_O      : out std_logic;
    LL32_O_EOF_N_O      : out std_logic;
    LL32_O_REM_O        : out std_logic_vector(1 downto 0);
    LL32_O_SRC_RDY_N_O  : out std_logic;
    LL32_O_DST_RDY_N_I  : in  std_logic
  );
end ll64_to_ll32;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll64_to_ll32 is

  signal word_sel           : std_logic;
  signal ll64_rem_int       : std_logic_vector (2 downto 0);

  signal ll64_us_data       : std_logic_vector (63 downto 0);
  signal ll64_us_sof_n      : std_logic;
  signal ll64_us_eof_n      : std_logic;
  signal ll64_us_rem        : std_logic_vector (2 downto 0);
  signal ll64_us_src_rdy_n  : std_logic;
  signal ll64_us_dst_rdy_n  : std_logic;

  signal ll32_ds_data       : std_logic_vector (31 downto 0);
  signal ll32_ds_sof_n      : std_logic;
  signal ll32_ds_eof_n      : std_logic;
  signal ll32_ds_rem        : std_logic_vector (1 downto 0);
  signal ll32_ds_src_rdy_n  : std_logic;
  signal ll32_ds_dst_rdy_n  : std_logic;

begin

  use_buff_us: if CGN_LL_I_BUFF > 0 generate
  begin
    dbuff_us : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 64,
     CGN_REM_WIDTH  => 3
    )
    port map
    (
      CLK_I            => LL_CLK_I,
      RESET_I          => RESET_I,

      LL_I_DATA_I      => LL64_I_DATA_I,
      LL_I_SOF_N_I     => LL64_I_SOF_N_I,
      LL_I_EOF_N_I     => LL64_I_EOF_N_I,
      LL_I_REM_I       => LL64_I_REM_I,
      LL_I_SRC_RDY_N_I => LL64_I_SRC_RDY_N_I,
      LL_I_DST_RDY_N_O => LL64_I_DST_RDY_N_O,

      LL_O_DATA_O      => ll64_us_data,
      LL_O_SOF_N_O     => ll64_us_sof_n,
      LL_O_EOF_N_O     => ll64_us_eof_n,
      LL_O_REM_O       => ll64_us_rem,
      LL_O_SRC_RDY_N_O => ll64_us_src_rdy_n,
      LL_O_DST_RDY_N_I => ll64_us_dst_rdy_n
    );
  end generate use_buff_us;

  no_buff_us: if CGN_LL_I_BUFF = 0 generate
  begin
    ll64_us_data         <= LL64_I_DATA_I;
    ll64_us_sof_n        <= LL64_I_SOF_N_I;
    ll64_us_eof_n        <= LL64_I_EOF_N_I;
    ll64_us_rem          <= LL64_I_REM_I;
    ll64_us_src_rdy_n    <= LL64_I_SRC_RDY_N_I;
    LL64_I_DST_RDY_N_O   <= ll64_us_dst_rdy_n;
  end generate no_buff_us;

  ------------------------------------------------------------------------------
  -- valid bytes may be <4 only allowed if EOF is true
  ll64_rem_int  <=  ll64_us_rem when ll64_us_eof_n ='0' else "111";

  process (LL_CLK_I)
  begin
    if rising_edge(LL_CLK_I) then
      if (RESET_I = '1') or (ll64_us_src_rdy_n ='1') or ((ll32_ds_dst_rdy_n = '0') and (word_sel = ll64_rem_int(2)))  then
        word_sel <= '0';
      elsif (ll32_ds_dst_rdy_n = '0') then
        word_sel <= not word_sel;
      end if;
    end if;
  end process;

  ll64_us_dst_rdy_n <= '1' when RESET_I = '1' else
                       '0' when (word_sel = ll64_rem_int(2)) and (ll32_ds_dst_rdy_n = '0') else
                       '0' when (ll64_us_src_rdy_n ='1') else
                       '1';

  ll32_ds_src_rdy_n <= '0' when word_sel = '1' else ll64_us_src_rdy_n;
  ll32_ds_data      <= ll64_us_data(63 downto 32) when (word_sel = '0') else ll64_us_data(31 downto  0);
  ll32_ds_sof_n     <= '0' when (ll64_us_sof_n = '0') and (word_sel = '0') else '1';
  ll32_ds_eof_n     <= '0' when (ll64_us_eof_n = '0') and (word_sel = ll64_rem_int(2) ) else '1';
  ll32_ds_rem(1 downto 0)  <= ll64_rem_int(1 downto 0);

  ------------------------------------------------------------------------------

  use_buff_ds: if CGN_LL_O_BUFF > 0 generate
  begin
    dbuff_ds : entity psi_3205_v1_00_a.ll_double_buff
    generic map
    (
     CGN_DATA_WIDTH => 32,
     CGN_REM_WIDTH  => 2
    )
    port map
    (
      CLK_I             => LL_CLK_I,
      RESET_I           => RESET_I,

      LL_I_DATA_I      => ll32_ds_data,
      LL_I_SOF_N_I     => ll32_ds_sof_n,
      LL_I_EOF_N_I     => ll32_ds_eof_n,
      LL_I_REM_I       => ll32_ds_rem,
      LL_I_SRC_RDY_N_I => ll32_ds_src_rdy_n,
      LL_I_DST_RDY_N_O => ll32_ds_dst_rdy_n,

      LL_O_DATA_O      => LL32_O_DATA_O,
      LL_O_SOF_N_O     => LL32_O_SOF_N_O,
      LL_O_EOF_N_O     => LL32_O_EOF_N_O,
      LL_O_REM_O       => LL32_O_REM_O,
      LL_O_SRC_RDY_N_O => LL32_O_SRC_RDY_N_O,
      LL_O_DST_RDY_N_I => LL32_O_DST_RDY_N_I
    );
  end generate use_buff_ds;

  no_buff_ds: if CGN_LL_O_BUFF = 0 generate
  begin
    LL32_O_DATA_O      <= ll32_ds_data;
    LL32_O_SOF_N_O     <= ll32_ds_sof_n;
    LL32_O_EOF_N_O     <= ll32_ds_eof_n;
    LL32_O_REM_O       <= ll32_ds_rem;
    LL32_O_SRC_RDY_N_O <= ll32_ds_src_rdy_n;
    ll32_ds_dst_rdy_n   <= LL32_O_DST_RDY_N_I;
  end generate no_buff_ds;

end behavioral;
