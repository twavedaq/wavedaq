--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ll_double_buff.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description :
--    local link buffer to split long combinatorial paths
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ll_double_buff is
  generic
  (
    CGN_DATA_WIDTH    : integer := 32;
    CGN_REM_WIDTH     : integer := 2
  );
  port
  (
    CLK_I             : in  std_logic;
    RESET_I           : in  std_logic;

    LL_I_DATA_I       : in  std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_I_SOF_N_I      : in  std_logic;
    LL_I_EOF_N_I      : in  std_logic;
    LL_I_REM_I        : in  std_logic_vector(CGN_REM_WIDTH-1 downto 0);
    LL_I_SRC_RDY_N_I  : in  std_logic;
    LL_I_DST_RDY_N_O  : out std_logic;

    LL_O_DATA_O       : out std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    LL_O_SOF_N_O      : out std_logic;
    LL_O_EOF_N_O      : out std_logic;
    LL_O_REM_O        : out std_logic_vector(CGN_REM_WIDTH-1 downto 0);
    LL_O_SRC_RDY_N_O  : out std_logic;
    LL_O_DST_RDY_N_I  : in  std_logic
  );
end ll_double_buff;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behavioral of ll_double_buff is

  component double_buff
  generic
  (
    CGN_DIN_SRC_RDY_ACTIVE_HIGH   : boolean := true;
    CGN_DOUT_SRC_RDY_ACTIVE_HIGH  : boolean := true;
    CGN_DIN_DST_RDY_ACTIVE_HIGH   : boolean := true;
    CGN_DOUT_DST_RDY_ACTIVE_HIGH  : boolean := true;
    CGN_DATA_WIDTH                : integer := 32
  );
  port
  (
    CLK_I           : in  std_logic;
    RESET_I         : in  std_logic;

    DIN_DATA_I      : in  std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    DIN_SRC_RDY_I   : in  std_logic;
    DIN_DST_RDY_O   : out std_logic;

    DOUT_DATA_O     : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    DOUT_SRC_RDY_O  : out std_logic;
    DOUT_DST_RDY_I  : in  std_logic
  );
  end component ;

  signal din  : std_logic_vector ((CGN_DATA_WIDTH+CGN_REM_WIDTH+1) downto 0);
  signal dout : std_logic_vector ((CGN_DATA_WIDTH+CGN_REM_WIDTH+1) downto 0);

begin

  din(CGN_DATA_WIDTH-1 downto 0)                              <= LL_I_DATA_I;
  din(CGN_DATA_WIDTH)                                         <= LL_I_EOF_N_I;
  din(CGN_DATA_WIDTH+1)                                       <= LL_I_SOF_N_I;
  din(CGN_DATA_WIDTH+CGN_REM_WIDTH+1 downto CGN_DATA_WIDTH+2) <= LL_I_REM_I;

  double_buff_inst: double_buff
  generic MAP
  (
    CGN_DIN_SRC_RDY_ACTIVE_HIGH   => false,
    CGN_DOUT_SRC_RDY_ACTIVE_HIGH  => false,
    CGN_DIN_DST_RDY_ACTIVE_HIGH   => false,
    CGN_DOUT_DST_RDY_ACTIVE_HIGH  => false,
    CGN_DATA_WIDTH                => CGN_DATA_WIDTH+CGN_REM_WIDTH+2
  )
  PORT MAP
  (
    CLK_I           => CLK_I,
    RESET_I         => RESET_I,

    DIN_DATA_I      => din,
    DIN_SRC_RDY_I   => LL_I_SRC_RDY_N_I,
    DIN_DST_RDY_O   => LL_I_DST_RDY_N_O,

    DOUT_DATA_O     => dout,
    DOUT_SRC_RDY_O  => LL_O_SRC_RDY_N_O,
    DOUT_DST_RDY_I  => LL_O_DST_RDY_N_I
  );

  LL_O_DATA_O  <= dout(CGN_DATA_WIDTH-1 downto 0);
  LL_O_EOF_N_O <= dout(CGN_DATA_WIDTH);
  LL_O_SOF_N_O <= dout(CGN_DATA_WIDTH+1);
  LL_O_REM_O   <= dout(CGN_DATA_WIDTH+CGN_REM_WIDTH+1 downto CGN_DATA_WIDTH+2);

end behavioral;
