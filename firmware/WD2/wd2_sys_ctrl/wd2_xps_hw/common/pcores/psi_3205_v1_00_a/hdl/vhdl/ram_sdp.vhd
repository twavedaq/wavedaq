--------------------------------------------------------------------------------
--  Paul Scherrer Institut
--  www.psi.ch
--------------------------------------------------------------------------------
--
--  Unit : ram_sdp.vhd
--
--  Tool Version :  Xilinx 14.7
--
--  Author  :  tg32
--  Created :  2014.07.30
--
--  Description : generic simple dual port ram
--
--------------------------------------------------------------------------------
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;


--------------------------------------------------------------------------------
-- Entity section
--------------------------------------------------------------------------------

entity ram_sdp is
  generic
  (
    CGN_ADDR_WIDTH : integer := 10;
    CGN_DATA_WIDTH : integer := 36;
    CGN_RAM_STYLE  : string  := "block"  -- style of implemented ram type:
                                         -- {auto|block|distributed|pipe_distributed};
  );
  port
  (
    PA_CLK_I    : in  std_logic;
    PA_ADDR_I   : in  std_logic_vector (CGN_ADDR_WIDTH-1 downto 0);
    PA_DATA_I   : in  std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
    PA_WR_EN_I  : in  std_logic;

    PB_CLK_I    : in  std_logic;
    PB_ADDR_I   : in  std_logic_vector (CGN_ADDR_WIDTH-1 downto 0);
    PB_RD_EN_I  : in  std_logic;
    PB_DATA_O   : out std_logic_vector (CGN_DATA_WIDTH-1 downto 0)
  );
end entity ram_sdp;


--------------------------------------------------------------------------------
-- Architecture section
--------------------------------------------------------------------------------

architecture behaviour of ram_sdp is
  type ram_type is array (0 to (2**CGN_ADDR_WIDTH)-1) of std_logic_vector (CGN_DATA_WIDTH-1 downto 0);
  signal ram: ram_type;

  attribute ram_style: string;
  attribute ram_style of ram: signal is CGN_RAM_STYLE;

--  attribute block_ram : boolean;
--  attribute block_ram of RAM : signal is TRUE;

begin

  write_proc: process (PA_CLK_I)
  begin
    if rising_edge(PA_CLK_I) then
      if (PA_WR_EN_I = '1') then
        ram(conv_integer(PA_ADDR_I)) <= PA_DATA_I;
      end if;
    end if;
  end process write_proc;

  read_proc: process (PB_CLK_I)
  begin
    if rising_edge(PB_CLK_I) then
      if (PB_RD_EN_I = '1') then
        PB_DATA_O <= ram(conv_integer(PB_ADDR_I));
      end if;
    end if;
  end process read_proc;

end behaviour;
