/******************************************************************************/
/*                                                                            */
/*  file: network_if.h                                                     */
/*                                                                            */
/*  (c) 2008 PSI tg14                                                         */
/*                                                                            */
/******************************************************************************/

#ifndef __NETWORK_IF_H__
#define __NETWORK_IF_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "plb_ll_fifo.h"
#include "utilities.h"
#include <string.h>
#include "ether_com.h"
#include "gmac_lite.h"


#define NW_IF_NAME_LENGTH 9


/******************************************************************************/
/* function  prototypes                                                       */
/******************************************************************************/


typedef struct
{
      plb_ll_fifo_type *ll_fifo;
      unsigned char mac_addr[6];
      unsigned char ip_addr[4];
/*      unsigned int src_port; */

      int  ip_address_valid;
      int  do_dhcp;
      char if_name[NW_IF_NAME_LENGTH];
/*      dhcp_type *dhcp; */
      void *dhcp;
      gmac_lite_type *gmac_lite;
      unsigned char *com_buff;
      unsigned int com_buff_len;


/*  char trailer[18]; */
} network_if_type;

/******************************************************************************/

typedef struct
{
  unsigned char dst_mac_addr[6];
  unsigned char dst_ip_addr[4];
  unsigned int  dst_port;
} network_target_type;

/******************************************************************************/

void    nw_if_construct(network_if_type *self, const char *if_name, plb_ll_fifo_type *ll_fifo, void *dhcp, gmac_lite_type *gmac_lite, unsigned char *com_buff, unsigned int com_buff_len);
void    nw_if_config(network_if_type *self, const unsigned char *mac_addr, const unsigned char *ip_addr, const unsigned int dhcp);
xfs_u32 nw_if_init(network_if_type *self, const unsigned char *mac_addr, const unsigned char *ip_addr, const unsigned int dhcp);
unsigned char* nw_if_get_ip_address(network_if_type *self, unsigned char *ip_addr);
void    nw_if_set_ip_address(network_if_type *self, const unsigned char *ip_addr);
unsigned char* nw_if_get_mac_address(network_if_type *self, unsigned char *mac_addr);
void    nw_if_set_mac_address(network_if_type *self, const unsigned char *mac_addr);
int     nw_if_get_dhcp(network_if_type *self);
void    nw_if_set_dhcp(network_if_type *self, int i_dhcp);
void    nw_if_set_name(network_if_type *self, char *if_name);


int   handle_arp(network_if_type *nw_if,  unsigned char* frame, int len);
int   handle_ping(network_if_type *nw_if, unsigned char* frame, int len);
int   check_udp(network_if_type *nw_if, unsigned char* frame, int len);
void  prepare_udp_frame(network_if_type *nw_if, void* frame);
void  udp_send_frame(network_if_type *nw_if, udp_header_type* frame, int payload_len);
void  nw_if_print_status(network_if_type *self);


#endif /* __NETWORK_IF_H__ */
