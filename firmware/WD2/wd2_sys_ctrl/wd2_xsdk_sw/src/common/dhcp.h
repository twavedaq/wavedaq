#ifndef __DHCP_H__
#define __DHCP_H__

#include "ether_com.h"
#include "network_if.h"

#define   NAME_BUF_LEN           17

#define   DHCP_OPT_LEN           64  /*   Size (in bytes) of option array               */

#define   DHCP_UDP_SERVER_PORT   67  /*   UDP port where DHCP requests should be sent.  */
#define   DHCP_UDP_CLIENT_PORT   68  /*   UDP port clients will receive DHCP replies.   */

#define   DHCP_OPT_PAD            0  /*   token padding value (make be skipped)         */
#define   DHCP_OPT_NETMASK        1  /*   subnet mask client should use (4 byte mask)   */
#define   DHCP_OPT_ROUTERS        3  /*   routers client should use (IP addr list)      */
#define   DHCP_OPT_TIMESERVERS    4  /*   time servers client should use (IP addr list) */
#define   DHCP_OPT_NAMESERVERS    5  /*   name servers client should use (IP addr list) */
#define   DHCP_OPT_DNSSERVERS     6  /*   DNS servers client should use (IP addr list). */
#define   DHCP_OPT_HOSTNAME      12  /*   host name client should use (string)          */
#define   DHCP_OPT_DOMAINNAME    15  /*   domain name client should use (string)        */
#define   DHCP_OPT_BROADCAST     28  /*   broadcast IP address                          */
#define   DHCP_OPT_REQUESTEDIP   50  /*   IP address requested by client (IP address).  */
#define   DHCP_OPT_LEASETIME     51  /*   DHCP Lease Time (uint32 seconds).             */
#define   DHCP_OPT_DHCPMSGTYPE   53  /*   DHCP message type (1 byte).                   */
#define   DHCP_OPT_SERVERID      54  /*   Server Identifier (IP address).               */
#define   DHCP_OPT_PARAMREQLIST  55  /*   Paramerter Request List (n OPT codes).        */
#define   DHCP_OPT_RENEWALTIME   58  /*   DHCP Lease Renewal Time (uint32 seconds).     */
#define   DHCP_OPT_REBINDTIME    59  /*   DHCP Lease Rebinding Time (uint32 seconds).   */
#define   DHCP_OPT_END          255  /*   token end value (marks end of options list)   */

#define BOOTREQUEST     1
#define BOOTREPLY       2


#define DHCP_DISCOVER   1
#define DHCP_OFFER      2
#define DHCP_REQUEST    3
#define DHCP_DECLINE    4
#define DHCP_ACK        5
#define DHCP_NAK        6
#define DHCP_RELEASE    7
#define DHCP_INFORM     8

#define DHCP_STATE_INIT        0x01
#define DHCP_STATE_SELECTING   0x02
#define DHCP_STATE_REQUESTING  0x04
#define DHCP_STATE_BOUND       0x08
#define DHCP_STATE_RENEWING    0x10
#define DHCP_STATE_REBINDING   0x20
#define DHCP_STATE_INIT_REBOOT 0x40
#define DHCP_STATE_REBOOTING   0x80

#define DHCP_SENDBUFF_LEN  (sizeof(udp_header_type) + sizeof(dhcp_frame_type))

typedef struct
{
  unsigned char op[1];
  unsigned char htype[1];
  unsigned char hlen[1];
  unsigned char hops[1];
  unsigned char xid[4];
  unsigned char secs[2];
  unsigned char flags[2];
  unsigned char ciaddr[4];
  unsigned char yiaddr[4];
  unsigned char siaddr[4];
  unsigned char giaddr[4];
  unsigned char chaddr[16];
  unsigned char sname[64];
  unsigned char file[128];
  unsigned char options[DHCP_OPT_LEN];
} dhcp_frame_type;


typedef struct
{
  unsigned char   send_buff[DHCP_SENDBUFF_LEN];
  unsigned char   dhcp_server_mac_addr[6];
  unsigned char   server_ip_addr[4];
  unsigned char   subnet_mask[4];
  unsigned char   broadcast_address[4];
  unsigned int    lease_time;
  unsigned int    renewal_time;
  unsigned int    rebinding_time;
  unsigned int    last_dhcp_ack_time;
  unsigned int    last_req;
  unsigned int    initial_req;
  unsigned int    init_reboot_retries;
  int             dhcp_state;
  network_if_type *nw_if;
} dhcp_type;

  void dhcp_construct(dhcp_type *self, network_if_type *nw_if_i);
  void dhcp_init(dhcp_type *self);
  void dhcp_send_frame(dhcp_type *self);
  void dhcp_request(dhcp_type *self);
  int  dhcp_handle_req(dhcp_type *self, unsigned char* frame, int len);
  void dhcp_print_times(dhcp_type *self);


  int dhcp_opt_magic_cookie(unsigned char* cptr);
  int dhcp_opt_message_type(unsigned char* cptr, unsigned char message);
  int dhcp_opt_server_ip(unsigned char* cptr, unsigned char* server_ip);
  int dhcp_opt_requested_ip(unsigned char* cptr, unsigned char* requested_ip);
  int dhcp_opt_host_name(unsigned char* cptr, char* name);
  int dhcp_opt_end(unsigned char* cptr);
  int dhcp_get_message_type(unsigned char* cptr);
  int dhcp_parse_options(dhcp_type *self, unsigned char* cptr);


#endif /* __DHCP_H__ */
