/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Driver vor custom pcore plb_gpio.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "drv_plb_gpio.h"
#include "utilities.h"

/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

void plb_gpio_init(plb_gpio_type *self, xfs_u32 b_address)
{
  self->base_address = io_remap(b_address);
}

/******************************************************************************/

void plb_gpio_set(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_SET, value);
}

/******************************************************************************/

void plb_gpio_clr(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_CLR, value);
}

/******************************************************************************/

void plb_gpio_write(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_WR, value);
}

/******************************************************************************/

xfs_u32 plb_gpio_rb_out_reg(plb_gpio_type *self, xfs_u32 io_register)
{
  return xfs_in32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_WR);
}

/******************************************************************************/

xfs_u32 plb_gpio_get(plb_gpio_type *self, xfs_u32 io_register)
{
  return xfs_in32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_RD);
}

/******************************************************************************/

void plb_gpio_tristate_set(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_SET, value);
}

/******************************************************************************/

void plb_gpio_tristate_clr(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_CLR, value);
}

/******************************************************************************/

void plb_gpio_tristate_write(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_WR, value);
}

/******************************************************************************/

xfs_u32 plb_gpio_tristate_get(plb_gpio_type *self, xfs_u32 io_register)
{
  return xfs_in32(self->base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_WR);
}

/******************************************************************************/
