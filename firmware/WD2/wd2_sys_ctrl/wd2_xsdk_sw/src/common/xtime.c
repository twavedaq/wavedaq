/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, tg32
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Code to use with a simple 32 bit timer running continuously.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */
 

#include "xtime.h"

/******************************************************************************/

static unsigned int time_seconds = 0;


/******************************************************************************/

void time_check_seconds(void)
{
  static unsigned int last_tic = 0;
  unsigned int current_tic;

  current_tic = time_tic_read();
  if ((current_tic - last_tic) > TIME_FREQUENCY)
  {
    last_tic = current_tic;
    time_seconds++;
  }
}


/******************************************************************************/

unsigned int time_get_sec(void)
{
  return time_seconds;
}

/******************************************************************************/

void usleep(unsigned int usec)
{
  unsigned int tic;
  unsigned int tic_to_wait;

  tic = time_tic_read();

/* tbd: improve precision and prevent overflow */
  tic_to_wait = (TIME_FREQUENCY/1000000) * usec;
  while ( (time_tic_read()-tic) < tic_to_wait);
}

/******************************************************************************/

void msleep(unsigned int msec)
{
  unsigned int i;
  for (i=0; i < msec; i++)
  {
    usleep(1000);
  }
}

/******************************************************************************/
/* 
  void sleep(unsigned int sec)
  {
    int i;
    for (i=0; i < 2*sec; i++)
    {
      msleep(500);
      time_check_seconds();
    }
  }
*/ 
/******************************************************************************/

