/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  28.11.2017 12:49:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __WD2_FLASH_MEMORY_MAP_H__
#define __WD2_FLASH_MEMORY_MAP_H__

/* SPI Flash Memory Map */
#define SPI_FLASH_BITSTREAM_ADDR          0x00000000
#define SPI_FLASH_FW_HEADER_OFFS          0x0041F000
#define SPI_FLASH_BITSTREAM_SIZE          0x00420000
#ifdef WD2_REV_F
#define WD2_FPGA_TYPE                     "6slx100fgg484"
#else
#define WD2_FPGA_TYPE                     "6slx150fgg484"
#endif
#define WD2_FPGA_UID                      0
#define SPI_FLASH_BITSTREAM_BLOCKS64      66
#define SPI_FLASH_BITSTREAM_BLOCKS32      132
#define SPI_FLASH_BITSTREAM_SECTORS       1056

#define SPI_FLASH_SOFTWARE_ADDR           0x00420000
#define SPI_FLASH_SW_HEADER_OFFS          0x001FF000
#define SPI_FLASH_SOFTWARE_SIZE           0x00200000
#define SPI_FLASH_SOFTWARE_BLOCKS64       32
#define SPI_FLASH_SOFTWARE_BLOCKS32       64
#define SPI_FLASH_SOFTWARE_SECTORS        512

#define SPI_FLASH_DRS_CALIB_ADDR          0x006E0000
#define SPI_FLASH_DRS_CALIB_SIZE          0x00100000
#define SPI_FLASH_DRS_CALIB_BLOCKS64      16
#define SPI_FLASH_DRS_CALIB_BLOCKS32      32
#define SPI_FLASH_DRS_CALIB_SECTORS       256

#define SPI_FLASH_MSCB_CONFIG_ADDR        0x007E0000
#define SPI_FLASH_MSCB_CONFIG_SIZE        0x00001000
#define SPI_FLASH_MSCB_CONFIG_SECTORS     1

#define SPI_FLASH_REG_CONTENTS_ADDR       0x007F0000
#define SPI_FLASH_REG_CONTENTS_SIZE       0x00001000
#define SPI_FLASH_REG_CONTENTS_SECTORS    1

#define SPI_FLASH_ENVIRONMENT_ADDR        0x007F8000
#define SPI_FLASH_ENVIRONMENT_SIZE        0x00001000
#define SPI_FLASH_ENVIRONMENT_SECTORS     1

/******************************************************************************/

#endif /* __WD2_FLASH_MEMORY_MAP_H__  */
