/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  17.02.2015 09:12:14
 *
 *  Description :  SPI driver for communication with W25Q64xx SPI Flash
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_SPI_FLASH_W25Q64_H__
#define __DRV_SPI_FLASH_W25Q64_H__

#include "plb_spi_master_v2_00_a.h"

/************************************************************/
/* W25Q64 defines                                           */
/************************************************************/
#define  W25Q64_CPOL        0
#define  W25Q64_CPHA        0
#define  W25Q64_LSB_FIRST   0

#define W25Q64_READ_SIZE  256

#define W25Q64_TIMEOUT_US   10000

/*                      | Sectors | Bytes/Sector | Pages/Sector | Bytes/Page | Total Bytes |
 * Winbond W25Q64FV     |   2048  |      4096    |      16      |    256     |   8388608   |
 *
 *                      | Blocks 32kB | Bytes/Block32 | Sectors/Block32 | Pages/Block32 |
 * Winbond W25Q64FV     |     256     |     32768     |        8        |      128      |
 *
 *                      | Blocks 64kB | Bytes/Block64 | Sectors/Block64 | Pages/Block64 |
 * Winbond W25Q64FV     |     128     |     65536     |       16        |      256      |
 */
 
#define  W25Q64_SECTORS            2048
#define  W25Q64_PAGES_PER_SECTOR     16
#define  W25Q64_BYTES_PER_PAGE      256
#define  W25Q64_BYTES_PER_SECTOR   (W25Q64_PAGES_PER_SECTOR * W25Q64_BYTES_PER_PAGE)
#define  W25Q64_BYTES              (W25Q64_SECTORS * W25Q64_PAGES_PER_SECTOR * W25Q64_BYTES_PER_PAGE)
#define  W25Q64_SECTOR_MASK        (W25Q64_BYTES_PER_SECTOR - 1)


#define  W25Q64_MANUFACTURER_ID       0xEF
#define  W25Q64_DEVICE_ID             0x16
#define  W25Q64_DEVICE_ID_JEDEC_SPI   0x4017
#define  W25Q64_DEVICE_ID_JEDEC_QPI   0x6017

/************************************************************/

/* Commands Standard SPI */
#define  W25Q64_CMD_WEN               0x06
#define  W25Q64_CMD_VOL_SR_WEN        0x50
#define  W25Q64_CMD_WDE               0x04
#define  W25Q64_CMD_READ_SR1          0x05   /* Status Register 1 (S7:S0)  */
#define  W25Q64_CMD_READ_SR2          0x35   /* Status Register 2 (S15:S8) */
#define  W25Q64_CMD_WRITE_SR          0x01
#define  W25Q64_CMD_PAGE_PROG         0x02
#define  W25Q64_CMD_SECTOR_ERASE      0x20
#define  W25Q64_CMD_BLOCK32_ERASE     0x52
#define  W25Q64_CMD_BLOCK64_ERASE     0xD8
#define  W25Q64_CMD_CHIP_ERASE        0xC7   /* both commands have the same effect */
#define  W25Q64_CMD_CHIP_ERASE_ALT    0x60   /* both commands have the same effect */
#define  W25Q64_CMD_ERASE_PROG_SUSP   0x75
#define  W25Q64_CMD_ERASE_PROG_RESU   0x7A
#define  W25Q64_CMD_POWER_DOWN        0xB9
#define  W25Q64_CMD_READ_DATA         0x03
#define  W25Q64_CMD_FAST_READ         0x0B
#define  W25Q64_CMD_REL_POWER_DOWN    0xAB
#define  W25Q64_CMD_MANUF_DEV_ID      0x90
#define  W25Q64_CMD_JEDEC_ID          0x9F
#define  W25Q64_CMD_UNIQUE_ID         0x4B
#define  W25Q64_CMD_READ_SFDP_REG     0x5A
#define  W25Q64_CMD_ERASE_SECU_REG    0x44
#define  W25Q64_CMD_PROG_SECU_REG     0x42
#define  W25Q64_CMD_READ_SECU_REG     0x48
#define  W25Q64_CMD_EN_QPI            0x38
#define  W25Q64_CMD_EN_RESET          0x66
#define  W25Q64_CMD_RESET             0x99

/* Commands Dual SPI */
#define  W25Q64_CMD_DUAL_FAST_READ      0x3B
#define  W25Q64_CMD_DUAL_FAST_IO        0xBB
#define  W25Q64_CMD_DUAL_MANUF_DEV_ID   0x92

/* Commands Quad SPI */
#define  W25Q64_CMD_QUAD_PAGE_PROG      0x32
#define  W25Q64_CMD_QUAD_FAST_READ      0x6B
#define  W25Q64_CMD_QUAD_FAST_IO        0xEB
#define  W25Q64_CMD_QUAD_WORD_READ_IO   0xE7
#define  W25Q64_CMD_QUAD_WORD_OCTR_IO   0xE3
#define  W25Q64_CMD_QUAD_BURST_WRAP     0x77
#define  W25Q64_CMD_QUAD_MANUF_DEV_ID   0x94

/* Commands QPI */
#define  W25Q64_CMD_QPI_SET_RD_PARAM      0xC0
#define  W25Q64_CMD_QPI_BURST_WRAP        0x0C
#define  W25Q64_CMD_QPI_FAST_RD_QUAD_IO   0x6B
#define  W25Q64_CMD_QPI_DISABLE_QPI       0xFF
/* Other QPI commands are equal to Standard SPI */

/************************************************************/

/* Status Register 1 Contents (S7:S0) */
#define  W25Q64_SR1_SRP0   0x80
#define  W25Q64_SR1_SEC    0x40
#define  W25Q64_SR1_TB     0x20
#define  W25Q64_SR1_BP2    0x10
#define  W25Q64_SR1_BP1    0x08
#define  W25Q64_SR1_BP0    0x04
#define  W25Q64_SR1_WEL    0x02
#define  W25Q64_SR1_BUSY   0x01

/* Status Register 2 Contents (S15:S8) */
#define  W25Q64_SR2_SUS    0x80
#define  W25Q64_SR2_CMP    0x40
#define  W25Q64_SR2_LB3    0x20
#define  W25Q64_SR2_LB2    0x10
#define  W25Q64_SR2_LB1    0x08
#define  W25Q64_SR2_RSVD   0x04
#define  W25Q64_SR2_QE     0x02
#define  W25Q64_SR2_SRP1   0x01

/**************************** Type Definitions *****************************/

/**
 * This typedef contains configuration information for one slave device.
 */
typedef struct {
  plb_spi_salve_settings   *spi_slave_ptr;
} spi_flash_w25q64;

/************************************************************/

/**************************** Variables *****************************/
/************************************************************/

/* SPI functions */
int spi_flash_is_busy(spi_flash_w25q64 *instance_ptr);
int spi_flash_wait_not_busy(spi_flash_w25q64 *instance_ptr, unsigned int timeout_us);

void spi_flash_init(spi_flash_w25q64 *instance_ptr, plb_spi_salve_settings   *slave_settings_ptr);
void spi_flash_get_jedec_id(spi_flash_w25q64 *instance_ptr, unsigned char *id_ptr);
unsigned char spi_flash_get_status_reg1(spi_flash_w25q64 *instance_ptr);
unsigned char spi_flash_get_status_reg2(spi_flash_w25q64 *instance_ptr);
void spi_flash_write_status_regs(spi_flash_w25q64 *instance_ptr, unsigned char stat_reg1, unsigned char stat_reg2);
void spi_flash_set_qe(spi_flash_w25q64 *instance_ptr, unsigned int qe);
void spi_flash_read(spi_flash_w25q64 *instance_ptr, unsigned char *rx_buffer, unsigned int address, unsigned int nr_of_bytes);
void spi_flash_write(spi_flash_w25q64 *instance_ptr, unsigned char *tx_buffer, unsigned int address, unsigned int nr_of_bytes);
void spi_flash_chip_erase(spi_flash_w25q64 *instance_ptr);
void spi_flash_sector_erase(spi_flash_w25q64 *instance_ptr, unsigned int address);
void spi_flash_block32k_erase(spi_flash_w25q64 *instance_ptr, unsigned int address);
void spi_flash_block64k_erase(spi_flash_w25q64 *instance_ptr, unsigned int address);
void spi_flash_range_erase(spi_flash_w25q64 *instance_ptr, unsigned int address, unsigned int len);
void spi_flash_sect_start_range_erase(spi_flash_w25q64 *instance_ptr, unsigned int address, unsigned int len);
void spi_flash_reset(spi_flash_w25q64 *instance_ptr);
int spi_flash_write_enable(spi_flash_w25q64 *instance_ptr, unsigned char enable);

#endif /* __DRV_SPI_FLASH_W25Q64_H__ */
