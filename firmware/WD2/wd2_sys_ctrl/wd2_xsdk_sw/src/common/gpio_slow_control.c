/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  14.05.2014 13:22:20
 *
 *  Description :  Software interface for gpio slow control signal inputs and outputs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "system.h"
#include "xfs_printf.h"
#include "gpio_slow_control.h"

/******************************************************************************/

void gpio_sc_init(void)
{
  gpio_sc_reset_sw_status();
}

/******************************************************************************/

void gpio_sc_set_sw_status(unsigned int val)
{
  plb_gpio_set(SYSPTR(gpio_sys), GPIO_LED_SW_STATUS, val);
}

/******************************************************************************/

void gpio_sc_clr_sw_status(unsigned int val)
{
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_LED_SW_STATUS, val);
}

/******************************************************************************/

void gpio_sc_flash_sw_status(unsigned int val)
{
  plb_gpio_set(SYSPTR(gpio_sys), GPIO_LED_SW_STATUS, val);
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_LED_SW_STATUS, val);
}

/******************************************************************************/

void gpio_sc_reset_sw_status()
{
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_LED_SW_STATUS, 0xFFFFFFFF);
}

/******************************************************************************/
/*
void gpio_sc_set_master_reset(unsigned int val)
{
  if(val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_MASTER_RST);
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_MASTER_RST);
  }
}
*/
/******************************************************************************/

void gpio_sc_set_drs_fsm_reset(unsigned int val)
{
  /* drs fsm reset is active low */
  if(val)
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_DRS_FSM_SOFT_RST_N);
  }
  else
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_DRS_FSM_SOFT_RST_N);
  }
}

/******************************************************************************/

unsigned int gpio_sc_get_drs_fsm_reset()
{
  /* drs fsm reset is active low */
  if(plb_gpio_rb_out_reg(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS) & GPIO_SC_OUT_DRS_FSM_SOFT_RST_N) return 0;
  else return 1;
}

/******************************************************************************/

void gpio_sc_set_eth_dst_valid()
{
  plb_gpio_set(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_ETH_DST_VALID);
}

/******************************************************************************/

void gpio_sc_disable_event_data_transmission()
{
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_ETH_DST_VALID);
}

/******************************************************************************/
/******************************************************************************/
