/******************************************************************************/
/*                                                                            */
/*  file: ether_com.h                                                         */
/*                                                                            */
/*  (c) 2008 PSI tg14                                                         */
/*                                                                            */
/******************************************************************************/

#ifndef __ETHER_COM_H__
#define __ETHER_COM_H__



/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/


#define ETHERNET_HEADER_LEN  (14)
#define IP_HEADER_LEN  (ETHERNET_HEADER_LEN + 20)

#define UDP_HEADER_LEN  (sizeof(udp_header_type))

#define htons(s) (s)
#define ntohs(s) (s)


extern const unsigned char ar_op_request[2];
extern const unsigned char ar_op_reply[2];

extern const unsigned char ether_typ_arp[2];
extern const unsigned char ether_typ_ip[2];




/******************************************************************************/
/* macro definitions                                                          */
/******************************************************************************/



/******************************************************************************/
/* type definitions                                                           */
/******************************************************************************/


/* UDP Header */
typedef struct
{
  /* ethternet frame  (14 byte) */
  unsigned char dst_mac[6];
  unsigned char src_mac[6];
  unsigned char len_type[2];

  /* ip header    (20 byte) */
  unsigned char ver_headerlen[1];
  unsigned char service_type[1];
  unsigned char total_length[2];
  unsigned char identification[2];
  unsigned char flags[1];
  unsigned char frag_offset[1];
  unsigned char time_to_live[1];
  unsigned char protocol[1];
  unsigned char ip_header_checksum[2];
  unsigned char src_ip[4];
  unsigned char dst_ip[4];

  /* udp header   (8 byte) */
  unsigned char src_port[2];
  unsigned char dst_port[2];
  unsigned char udp_message_len[2];
  unsigned char udp_checksum[2];

} udp_header_type;


/******************************************************************************/

/* ICMP Frame */
typedef struct
{
  /* ethternet frame */
  unsigned char dst_mac[6];
  unsigned char src_mac[6];
  unsigned char len_type[2];

  /* ip header */
  unsigned char ver_headerlen[1];
  unsigned char service_type[1];
  unsigned char total_length[2];
  unsigned char identification[2];
  unsigned char flags[1];
  unsigned char frag_offset[1];
  unsigned char time_to_live[1];
  unsigned char protocol[1];
  unsigned char ip_header_checksum[2];
  unsigned char src_ip[4];
  unsigned char dst_ip[4];

  /* ip header */
  unsigned char icmp_type[1];
  unsigned char icmp_code[1];
  unsigned char icmp_chksum[2];
  unsigned char icmp_id[2];
  unsigned char icmp_seq[2];
} icmp_header_type;


/******************************************************************************/


/* ARP Frame */
typedef struct
{
  /* ethternet frame */
  unsigned char dst_mac[6];  /* 48.bit: Ethernet address of destination                     */
  unsigned char src_mac[6];  /* 48.bit: Ethernet address of sender                          */
  unsigned char len_type[2]; /* 16.bit: Protocol type = 08 06 ether_type$ADDRESS_RESOLUTION */

  /* Ethernet packet data: */
  unsigned char ar_hrd[2];   /*   16.bit: (ar$hrd) Hardware address space (e.g., Ethernet, Packet Radio Net.)                                                */
  unsigned char ar_pro[2];   /*   16.bit: (ar$pro) Protocol address space.  For Ethernet hardware, this is from the set of type fields ether_typ$<protocol>. */
  unsigned char ar_hln[1];   /*    8.bit: (ar$hln) byte length of each hardware address                                                                      */
  unsigned char ar_pln[1];   /*    8.bit: (ar$pln) byte length of each protocol address                                                                      */
  unsigned char ar_op [2];   /*   16.bit: (ar$op)  opcode (ares_op$REQUEST | ares_op$REPLY)                                                                  */
  unsigned char ar_sha[6];   /*   nbytes: (ar$sha) Hardware address of sender of this packet, n from the ar$hln field.                                       */
  unsigned char ar_spa[4];   /*   mbytes: (ar$spa) Protocol address of sender of this packet, m from the ar$pln field.                                       */
  unsigned char ar_tha[6];   /*   nbytes: (ar$tha) Hardware address of target of this packet (if known).                                                     */
  unsigned char ar_tpa[4];   /*   mbytes: (ar$tpa) Protocol address of target.                                                                               */
/*  char trailer[18]; */
} arp_frame_type;


/******************************************************************************/
/* function  prototypes                                                       */
/******************************************************************************/

/*void  ncpy(char *cp1, const char *cp2, int len);       */
/*int   ncmp(const char *cp1, const char *cp2, int len); */

void  adjust_icmp_checksum(unsigned char* fp);
void  adjust_ip_checksum(udp_header_type* fp);
void  adjust_header(udp_header_type *fp, int frame_len);
void* udp_payload_ptr_get(void* frame);
int udp_payload_len_get(void* frame);

void adjust_ip_checksum_bram(udp_header_type *ip);


#endif  /* __ETHER_COM_H__ */

