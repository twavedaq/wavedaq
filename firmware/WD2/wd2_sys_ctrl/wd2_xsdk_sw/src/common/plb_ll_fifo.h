/******************************************************************************/
/*                                                                            */
/*  file: plb_ll_fifo.h                                                       */
/*                                                                            */
/*  (c) 2010 PSI tg32                                                         */
/*                                                                            */
/******************************************************************************/

#ifndef __PLB_LL_FIFO_H__
#define __PLB_LL_FIFO_H__


/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/


/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

/* Registers Offsets */
#define PLB_LL_FIFO_REG_CTRL      0x00
#define PLB_LL_FIFO_REG_STATUS    0x04
#define PLB_LL_FIFO_REG_FIFO      0x08



#define PLB_LL_FIFO_CTRL_LL_REM_SHIFT    30
#define PLB_LL_FIFO_CTRL_LL_REM          0xC0000000
#define PLB_LL_FIFO_CTRL_LL_EOF          0x20000000
#define PLB_LL_FIFO_CTRL_LL_SOF          0x10000000
#define PLB_LL_FIFO_CTRL_LL_MASK         0xF0000000


#define PLB_LL_FIFO_CTRL_TX_RESET        0x08000000
#define PLB_LL_FIFO_CTRL_RX_RESET        0x04000000

#define PLB_LL_FIFO_CTRL_RESET_STATUS    0x00800000
#define PLB_LL_FIFO_CTRL_RESET_USER      0x00400000
#define PLB_LL_FIFO_CTRL_RESET_LINK      0x00200000
#define PLB_LL_FIFO_CTRL_RESET_GT        0x00100000

#define PLB_LL_FIFO_CTRL_RESET_ALL       0x0CF00000

/* do not reset complete gtx dual in std. case */
/* cause this would reset PLL and stop LL clk  */
#define PLB_LL_FIFO_CTRL_RESET_STD       0x0CE00000

/* reset Rx and Tx Fifo and set User Reset */
#define PLB_LL_FIFO_CTRL_RESET_FIFO      0x0C400000


#define PLB_LL_FIFO_CTRL_CONFIG_VECTOR   0x000FFFFF


#define PLB_LL_FIFO_STATUS_LL_REM_SHIFT     30
#define PLB_LL_FIFO_STATUS_LL_REM           0xC0000000
#define PLB_LL_FIFO_STATUS_LL_EOF           0x20000000
#define PLB_LL_FIFO_STATUS_LL_SOF           0x10000000

#define PLB_LL_FIFO_STATUS_RX_EMPTY         0x08000000
#define PLB_LL_FIFO_STATUS_RX_ALMOSTEMPTY   0x04000000
#define PLB_LL_FIFO_STATUS_RX_FULL          0x02000000
#define PLB_LL_FIFO_STATUS_RX_ALMOSTFULL    0x01000000

#define PLB_LL_FIFO_STATUS_TX_EMPTY         0x00800000
#define PLB_LL_FIFO_STATUS_TX_ALMOSTEMPTY   0x00400000
#define PLB_LL_FIFO_STATUS_TX_FULL          0x00200000
#define PLB_LL_FIFO_STATUS_TX_ALMOSTFULL    0x00100000


#define PLB_LL_FIFO_STATUS_VECTOR        0x000FFFFF

/* #define PLB_LL_FIFO_ALMOST_FULL_THRESHOLD_WORDS    100 */  /* virtex 5 */
#define PLB_LL_FIFO_ALMOST_FULL_THRESHOLD_WORDS    2  /* s6 standard fifo */

/******************************************************************************/
/*struct plb_ll_fifo_struct; */
typedef struct plb_ll_fifo_struct plb_ll_fifo_type;

typedef int (*plb_ll_fifo_send_fcn) (plb_ll_fifo_type *, const void *, unsigned int);
typedef int (*plb_ll_fifo_rcv_fcn)  (plb_ll_fifo_type *, void *, unsigned int);


/******************************************************************************/

struct plb_ll_fifo_struct
{
    unsigned int ll_fifo_badr;
    unsigned int buffer_ptr;
    plb_ll_fifo_send_fcn send;
    plb_ll_fifo_rcv_fcn receive;
};



/******************************************************************************/


void plb_ll_fifo_init(plb_ll_fifo_type *self, unsigned int ll_fifo_badr_i);

int plb_ll_fifo_reset(plb_ll_fifo_type *self, unsigned int rst_mask);
int plb_ll_fifo_reset_set(plb_ll_fifo_type *self, unsigned int rst_mask);
int plb_ll_fifo_reset_clr(plb_ll_fifo_type *self, unsigned int rst_mask);
int plb_ll_fifo_ctrl_reg_set(plb_ll_fifo_type *self, unsigned int bit_mask);
int plb_ll_fifo_ctrl_reg_clr(plb_ll_fifo_type *self, unsigned int bit_mask);
unsigned int plb_ll_fifo_ctrl_reg_write_mask(plb_ll_fifo_type *self, unsigned int mask, unsigned int val);
unsigned int plb_ll_fifo_get_status_vector(plb_ll_fifo_type *self);
unsigned int plb_ll_fifo_get_status(plb_ll_fifo_type *self);
void plb_ll_fifo_status(plb_ll_fifo_type *self);
void plb_ll_fifo_write_word(plb_ll_fifo_type *self, unsigned int word, unsigned int sof, unsigned int eof, unsigned int reminder);
int plb_ll_fifo_send_part(plb_ll_fifo_type *self, const void *buffer, unsigned int frame_len, unsigned int part_sof, unsigned int part_eof);
int plb_ll_fifo_send(plb_ll_fifo_type *self, const void *buffer, unsigned int frame_len);
int plb_ll_fifo_receive(plb_ll_fifo_type *self, void *buffer, unsigned int bufflen);
int plb_ll_fifo_test (plb_ll_fifo_type *self);



/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/


#endif /* __PLB_LL_FIFO_H__ */
