/*---------------------------------------------------*/
/*                                                   */
/* file: xfs_printf.c                                */
/*                                                   */
/* Modified from :                                   */
/* Public Domain version of printf                   */
/* Rud Merriam, Compsult, Inc. Houston, Tx.          */
/* For Embedded Systems Programming, 1991            */
/*                                                   */
/*---------------------------------------------------*/
#include <stdio.h>
#include <ctype.h>
/* #include <string.h> */
/*#include <stdarg.h> */

#include "system.h"
#include "xfs_printf.h"

#ifdef SW_APP /* only in application */
#include "udp_terminal.h"
#endif

unsigned int xfs_printf_targets = XFS_PRINTF_TARGET_LOCAL;
unsigned int cmd_sender         = XFS_PRINTF_TARGET_LOCAL;
unsigned int former_cmd_sender  = XFS_PRINTF_TARGET_LOCAL;
unsigned int cmd_reply_mode     = XFS_PRINTF_REPLY_MODE_SENDER;

/*----------------------------------------------------*/
/* Use the following parameter passing structure to   */
/* make xfs_printf re-entrant.                        */
/*----------------------------------------------------*/
typedef struct params_s {
    int len;
    int num1;
    int num2;
    char pad_character;
    int do_padding;
    int left_flag;
    char *outbuf;
    int outbuf_size;
    int outbuf_ptr;
} params_t;

/*---------------------------------------------------*/
/* The purpose of this routine is to output data the */
/* same as the standard printf function without the  */
/* overhead most run-time libraries involve. Usually */
/* the printf brings in many kilobytes of code and   */
/* that is unacceptable in most embedded systems.    */
/*---------------------------------------------------*/

/*typedef char* charptr;          */
/*typedef int (*func_ptr)(int c); */
void xfs_outbyte(params_t *parptr, char byte)
{
  if (parptr->outbuf_ptr < parptr->outbuf_size-1)
  {
    parptr->outbuf[parptr->outbuf_ptr++] = byte;
  }
/* else if (parptr->outbuf_ptr == parptr->outbuf_size-1) */
/* {                                                     */
/*   parptr->outbuf[parptr->outbuf_ptr++] = 0;           */
/* }                                                     */
  
}



/*---------------------------------------------------*/
/*                                                   */
/* This routine puts pad characters into the output  */
/* buffer.                                           */
/*                                                   */
static void padding( const int l_flag, params_t *parptr)
{
    int i;

    if (parptr->do_padding && l_flag && (parptr->len < parptr->num1))
        for (i=parptr->len; i<parptr->num1; i++)
            xfs_outbyte(parptr, parptr->pad_character);
}

/*---------------------------------------------------*/
/*                                                   */
/* This routine moves a string to the output buffer  */
/* as directed by the padding and positioning flags. */
/*                                                   */
static void outs( charptr lp, params_t *parptr)
{
    /* pad on left if needed                         */
    parptr->len = strlen( lp);
    padding( !(parptr->left_flag), parptr);

    /* Move string to the buffer                     */
    while (*lp && (parptr->num2)--)
        xfs_outbyte(parptr, *lp++);

    /* Pad on right if needed                        */
    /* CR 439175 - elided next stmt. Seemed bogus.   */
    /* parptr->len = strlen( lp);                    */
    padding( parptr->left_flag, parptr);
}

/*---------------------------------------------------*/
/*                                                   */
/* This routine moves a number to the output buffer  */
/* as directed by the padding and positioning flags. */
/*                                                   */


static void outnum( const long n, const long base, params_t *parptr, int signed_num)
{
    charptr cp;
    int negative;
    char outbuf[32];
    const char digits[] = "0123456789ABCDEF";
    unsigned long num;

    /* Check if number is negative                   */
    if (signed_num && base == 10 && n < 0L) {
        negative = 1;
        num = -(n);
    }
    else{
        num = (n);
        negative = 0;
    }
   
    /* Build number (backwards) in outbuf            */
    cp = outbuf;
    do {
        *cp++ = digits[(int)(num % base)];
    } while ((num /= base) > 0);
    if (negative)
        *cp++ = '-';
    *cp-- = 0;

    /* Move the converted number to the buffer and   */
    /* add in the padding where needed.              */
    parptr->len = strlen(outbuf);
    padding( !(parptr->left_flag), parptr);
    while (cp >= outbuf)
        xfs_outbyte(parptr, *cp--);
    padding( parptr->left_flag, parptr);
}

/*---------------------------------------------------*/
/*                                                   */
/* This routine gets a number from the format        */
/* string.                                           */
/*                                                   */
static int getnum( charptr* linep)
{
    int n;
    charptr cp;

    n = 0;
    cp = *linep;
    while (isdigit((int)(*cp)))
        n = n*10 + ((*cp++) - '0');
    *linep = cp;
    return(n);
}

/*---------------------------------------------------*/
/*                                                   */
/* This routine operates just like a printf/sprintf  */
/* routine. It outputs a set of data under the       */
/* control of a formatting string. Not all of the    */
/* standard C format control are supported. The ones */
/* provided are primarily those needed for embedded  */
/* systems work. Primarily the floaing point         */
/* routines are omitted. Other formats could be      */
/* added easily by following the examples shown for  */
/* the supported formats.                            */
/*                                                   */

/* void esp_printf( const func_ptr f_ptr,
   const charptr ctrl1, ...) */



int xfs_vsnprintf(char *outbuf, int outbuf_size, const char *format, va_list argp)
{

    int long_flag;
    int dot_flag;
    int signed_num = 0;
    
    params_t par;
    params_t *parptr = &par;

    char ch;
/*    va_list argp;         */
/*    charptr ctrl = ctrl1; */
    char *ctrl = (char*)format;

    parptr->outbuf      = outbuf;
    parptr->outbuf_size = outbuf_size;
    parptr->outbuf_ptr  = 0;

/*    va_start( argp, format); */

    for ( ; *ctrl; ctrl++) {

        /* move format string chars to buffer until a  */
        /* format control is found.                    */
        if (*ctrl != '%') {
            xfs_outbyte(parptr,*ctrl);
            continue;
        }

        /* initialize all the flags for this format.   */
        dot_flag   = long_flag = par.left_flag = par.do_padding = 0;
        par.pad_character = ' ';
        par.num2=32767;

 try_next:
        ch = *(++ctrl);

        if (isdigit((int)ch)) {
            if (dot_flag)
                par.num2 = getnum(&ctrl);
            else {
                if (ch == '0')
                    par.pad_character = '0';

                par.num1 = getnum(&ctrl);
                par.do_padding = 1;
            }
            ctrl--;
            goto try_next;
        }

        switch (tolower((int)ch)) {
            case '%':
                xfs_outbyte(parptr, '%');
                continue;

            case '-':
                par.left_flag = 1;
                break;

            case '.':
                dot_flag = 1;
                break;

            case 'l':
                long_flag = 1;
                break;

            case 'd':
                signed_num = 1;
                /* continue with 'u' case... */
            case 'u':
                /* signed_num was initialized to zero */
                if (long_flag || ch == 'D' || ch == 'U') {
                    outnum( va_arg(argp, long), 10L, parptr, signed_num);
                    continue;
                }
                else {
                    outnum( va_arg(argp, int), 10L, parptr, signed_num);
                    continue;
                }
            case 'x':
                outnum((long)va_arg(argp, int), 16L, parptr, 0);
                continue;

            case 's':
                outs( va_arg( argp, charptr), parptr);
                continue;

            case 'c':
                xfs_outbyte(parptr, va_arg( argp, int));
                continue;

            case '\\':
                switch (*ctrl) {
                    case 'a':
                        xfs_outbyte(parptr, 0x07);
                        break;
                    case 'h':
                        xfs_outbyte(parptr, 0x08);
                        break;
                    case 'r':
                        xfs_outbyte(parptr, 0x0D);
                        break;
                    case 'n':
                        xfs_outbyte(parptr, 0x0D);
                        xfs_outbyte(parptr, 0x0A);
                        break;
                    default:
                        xfs_outbyte(parptr, *ctrl);
                        break;
                }
                ctrl++;
                break;

            default:
                continue;
        }
        goto try_next;
    }
    parptr->outbuf[parptr->outbuf_ptr] = 0;
    va_end( argp);

    return (parptr->outbuf_ptr);
}

/*---------------------------------------------------*/

int xfs_snprintf(char *outbuf, int max_buffer_size, const char *format, ...)
{
  int len;

  /* Create var_arg pointer */
  va_list argp;
  /* initialize var_arg pointer */
  va_start( argp, format);

  /* Generate String */
  len = xfs_vsnprintf(outbuf, max_buffer_size, format, argp);

  va_end( argp );

  return len;
}

/*---------------------------------------------------*/

int xfs_local_printf(const char *format, ...)
{
  char out_string[STRING_BUFFER_SIZE];
  int len;

  /* Create var_arg pointer */
  va_list argp;
  /* initialize var_arg pointer */
  va_start( argp, format);

  /* Generate String */
  len = xfs_vsnprintf(out_string, STRING_BUFFER_SIZE, format, argp);
  /* xfs_printf("\r\n----xfs_printf len=%d : %c\r\n",len, out_string[0]); */

  /* Output to terminal */
  #ifdef SW_APP /* only in application */
  if (!SYSPTR(term_stdin)->refresh)
  {
    SYSPTR(term_stdin)->refresh = 1;  
    term_erase_line();
    term_cursor_return();
  }
  #endif
  print(out_string);

  va_end(argp);
  return len;
}
/*---------------------------------------------------*/

int xfs_terminal_printf(const char *format, ...)
{
  char out_string[STRING_BUFFER_SIZE];
  int len;

  /* Create var_arg pointer */
  va_list argp;
  /* initialize var_arg pointer */
  va_start( argp, format);

  /* Generate String */
  len = xfs_vsnprintf(out_string, STRING_BUFFER_SIZE, format, argp);
  /* Output to terminal */
  if( (xfs_printf_targets & XFS_PRINTF_TARGET_LOCAL)  || (xfs_printf_targets == XFS_PRINTF_TARGET_NONE))
  {
    print(out_string);
  }

  #ifdef SW_APP /* only in application */
  /* UDP Terminal Output */
  if( xfs_printf_targets & XFS_PRINTF_TARGET_UDP )
  {
    udp_terminal_output(ALL_UDP_TERMINALS, out_string, len);
  }
  #endif

  va_end( argp );
  return len;
}

/*---------------------------------------------------*/


int xfs_printf(const char *format, ...)
{
  char out_string[STRING_BUFFER_SIZE];
  int len;

  /* Create var_arg pointer */
  va_list argp;
  /* initialize var_argpointer */
  va_start( argp, format);

  /* Generate String */
  len = xfs_vsnprintf(out_string, STRING_BUFFER_SIZE, format, argp);
  /* Output to terminal */
  if( (xfs_printf_targets & XFS_PRINTF_TARGET_LOCAL)  || (xfs_printf_targets == XFS_PRINTF_TARGET_NONE))
  {
    #ifdef SW_APP /* only in application */
    if (!SYSPTR(term_stdin)->refresh)
    {
      SYSPTR(term_stdin)->refresh = 1;  
      term_erase_line();
      term_cursor_return();
    }
    #endif
    
    print(out_string);
  }

  #ifdef SW_APP /* only in application */
  /* UDP Terminal Output */
  if( xfs_printf_targets & XFS_PRINTF_TARGET_UDP )
  {
    udp_terminal_output(ALL_UDP_TERMINALS, out_string, len);
  }

  /* SPI Output */
  if( xfs_printf_targets & XFS_PRINTF_TARGET_SPI )
  {
    sspi_transmit(SYSPTR(spi_syslink_slave), (unsigned char*)(out_string), len);
  }
  #endif

  va_end( argp );
  return len;
}

/*---------------------------------------------------*/

void set_cmd_sender(unsigned int sender)
{
  cmd_sender = sender;
  if( cmd_reply_mode == XFS_PRINTF_REPLY_MODE_SENDER ) xfs_printf_targets = cmd_sender;
  else                                                 xfs_printf_targets = XFS_PRINTF_TARGET_ALL;
}

/*---------------------------------------------------*/

void no_cmd_sender()
{
  if(cmd_sender != XFS_PRINTF_TARGET_NONE)
  {
    former_cmd_sender = cmd_sender;
    cmd_sender = XFS_PRINTF_TARGET_NONE;
    if( cmd_reply_mode == XFS_PRINTF_REPLY_MODE_SENDER ) xfs_printf_targets = cmd_sender;
    else                                                 xfs_printf_targets = XFS_PRINTF_TARGET_ALL;
  }
}

/*---------------------------------------------------*/

void reapply_cmd_sender()
{
  cmd_sender = former_cmd_sender;
  former_cmd_sender = XFS_PRINTF_TARGET_NONE;
  if( cmd_reply_mode == XFS_PRINTF_REPLY_MODE_SENDER ) xfs_printf_targets = cmd_sender;
  else                                                 xfs_printf_targets = XFS_PRINTF_TARGET_ALL;
}

/*---------------------------------------------------*/

unsigned int get_cmd_sender()
{
  return cmd_sender;
}

/*---------------------------------------------------*/

void set_cmd_reply_mode(unsigned int reply_mode)
{
  cmd_reply_mode = reply_mode;
  if( cmd_reply_mode == XFS_PRINTF_REPLY_MODE_SENDER ) xfs_printf_targets = cmd_sender;
  else                                                 xfs_printf_targets = XFS_PRINTF_TARGET_ALL;
}

/*---------------------------------------------------*/

unsigned int get_xfs_printf_targets()
{
  return xfs_printf_targets;
}

/*---------------------------------------------------*/

