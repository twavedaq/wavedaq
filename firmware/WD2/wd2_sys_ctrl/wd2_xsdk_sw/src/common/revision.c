/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e (Author of generation script)
 *  Created :  13.06.2018 15:31
 *
 *  Description :  Revision handling functions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */


/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "revision.h"
#include "system.h"
#include "drv_plb_wd2_reg_bank.h"
#include "register_map_wd2.h"

/******************************************************************************/
/* functions                                                                  */
/******************************************************************************/

unsigned int get_fw_fcl(void)
{
  unsigned int fcl;
  
  fcl = reg_bank_get(WD2_FW_COMPAT_LEVEL_REG);
  fcl = (fcl & WD2_FW_COMPAT_LEVEL_MASK) >> WD2_FW_COMPAT_LEVEL_OFS;
  
  return fcl;
}

/******************************************************************************/

unsigned int get_fw_rcl(void)
{
  unsigned int rcl;
  
  rcl = reg_bank_get(WD2_REG_LAYOUT_COMP_LEVEL_REG);
  rcl = (rcl & WD2_REG_LAYOUT_COMP_LEVEL_MASK) >> WD2_REG_LAYOUT_COMP_LEVEL_OFS;
  
  return rcl;
}

/******************************************************************************/

unsigned int get_sw_fcl(char* hdr_data)
{
  unsigned int fcl;
  char* cptr = NULL;
  
  cptr = strstr(hdr_data, "_fcl");
  if(cptr) fcl = strtoul(cptr+4, 0, 0);
  else     fcl = 0;
  
  return fcl;
}

/******************************************************************************/

unsigned int get_sw_rcl(char* hdr_data)
{
  unsigned int rcl;
  char* cptr = NULL;
  
  cptr = strstr(hdr_data, "_rcl");
  if(cptr) rcl = strtoul(cptr+4, 0, 0);
  else     rcl = 0;
  
  return rcl;
}

/******************************************************************************/
