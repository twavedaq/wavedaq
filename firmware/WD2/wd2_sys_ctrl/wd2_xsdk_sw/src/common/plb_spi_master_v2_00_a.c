/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_master_v2_00_a/src/plb_spi_master.c
* Version:           2.00.a
* Description:       plb_spi_master Driver Source File
* Date:              Mon Feb 16 08:46:32 2015
*****************************************************************************/


/***************************** Include Files *******************************/

#include "plb_spi_master_v2_00_a.h"
#include "xtime.h"
#include "xfs_printf.h"


/************************** Function Definitions ***************************/

int mspi_init(plb_spi_master *instance_ptr, unsigned int base_address, unsigned char num_slaves)
{
  /* Set some default values  */
  instance_ptr->base_address = base_address;
  instance_ptr->is_started   = FALSE;
  instance_ptr->num_slaves   = num_slaves;

  /* to be changed (funktioniert nur bis max 31 slaves) */
  instance_ptr->slave_select_mask     = (1 << instance_ptr->num_slaves) - 1;
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_SLVSEL_REG_OFFSET, instance_ptr->slave_select_mask);
  mspi_reset(instance_ptr);
  mspi_reset_rxfifo(instance_ptr);
  mspi_reset_txfifo(instance_ptr);
  mspi_reset_nobfifo(instance_ptr);

  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, PLB_SPI_MASTER_CONFIG_ENABLE);

  instance_ptr->is_started   = TRUE;

  return 1;
}

/***************************************************************************/

int mspi_slv_init(plb_spi_master *instance_ptr, plb_spi_salve_settings *slave_setting_ptr, unsigned int slave_select,
                  unsigned char slave_cpol, unsigned char slave_cpha, unsigned char slave_lsb_first)
{
  slave_setting_ptr->master           = instance_ptr;
  slave_setting_ptr->slave_select     = slave_select;
  slave_setting_ptr->slave_cpol       = slave_cpol;
  slave_setting_ptr->slave_cpha       = slave_cpha;
  slave_setting_ptr->slave_lsb_first  = slave_lsb_first;

  return 1;
}

/***************************************************************************/

int mspi_send(plb_spi_salve_settings *slave_setting_ptr, unsigned char *tx_buffer_ptr, unsigned int nr_of_bytes)
{
  mspi_transceive(slave_setting_ptr, NULL, 0, tx_buffer_ptr, NULL, nr_of_bytes);

  return 1;
}

/***************************************************************************/

void mspi_transceive(plb_spi_salve_settings *slave_setting_ptr, unsigned char *instr_buffer_ptr, unsigned int nr_of_instr_bytes, unsigned char *tx_buffer_ptr, unsigned char *rx_buffer_ptr, unsigned int nr_of_bytes)
{
  unsigned int i;
  unsigned int val;

  /* check if initialization is done  */
  if(!slave_setting_ptr->master->is_started)
  {
    return;
  }

  /* configure master with slave parameters  */
  mspi_configure_for_slave(slave_setting_ptr);

  /* flush rx fifo */
  while(!mspi_rx_fifo_is_empty(slave_setting_ptr->master))
  {
    mspi_mReadReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_RXD_REG_OFFSET);
  }

  /* write instruction loop */
  for(i=0;i<nr_of_instr_bytes;i++)
  {
    /* wait if fifo is almost full */
    while(mspi_tx_fifo_is_almost_full(slave_setting_ptr->master));
    /* write to tx fifo */
    mspi_mWriteReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_TXD_REG_OFFSET, instr_buffer_ptr ? instr_buffer_ptr[i] : 0);
  }

  /* write payload loop */
  for(i=0;i<nr_of_bytes;i++)
  {
    /* wait if fifo is almost full */
    while(mspi_tx_fifo_is_almost_full(slave_setting_ptr->master));
    /* write to tx fifo */
    mspi_mWriteReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_TXD_REG_OFFSET, tx_buffer_ptr ? tx_buffer_ptr[i] : 0);
  }
  mspi_mWriteReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_NO_BYTES_REG_OFFSET, nr_of_instr_bytes+nr_of_bytes);

  /* if there is rx data left */
  for(i=0;i<(nr_of_instr_bytes+nr_of_bytes);i++)
  {
    /* wait for rx data */
    while(mspi_rx_fifo_is_empty(slave_setting_ptr->master));
    /* read from rx fifo (only store payload) */
    val = mspi_mReadReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_RXD_REG_OFFSET);

    if ((i >= nr_of_instr_bytes) && rx_buffer_ptr) rx_buffer_ptr[i-nr_of_instr_bytes] = val;
    /* if(DBG_ALL) xfs_printf("RX Byte %d: 0x%02X\r\n", rx_bytes, rx_buffer_ptr[rx_bytes]); */
  }
}

/***************************************************************************/

void mspi_configure_for_slave(plb_spi_salve_settings *slave_setting_ptr)
{
  unsigned int ctrl_reg_val;

  /* read control register */
  ctrl_reg_val = mspi_mReadReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  
  /* clear configuration bits and fifo resets */
  ctrl_reg_val &= ~(PLB_SPI_MASTER_CONFIG_CPOL |
                    PLB_SPI_MASTER_CONFIG_CPHA |
                    PLB_SPI_MASTER_CONFIG_LSB_FIRST |
                    PLB_SPI_MASTER_CONFIG_TXFIFO_RST |
                    PLB_SPI_MASTER_CONFIG_RXFIFO_RST);

  /* configure for slave */
  if(slave_setting_ptr->slave_cpol)
  {
    ctrl_reg_val |= PLB_SPI_MASTER_CONFIG_CPOL;
  }
  if(slave_setting_ptr->slave_cpha)
  {
    ctrl_reg_val |= PLB_SPI_MASTER_CONFIG_CPHA;
  }
  if(slave_setting_ptr->slave_lsb_first)
  {
    ctrl_reg_val |= PLB_SPI_MASTER_CONFIG_LSB_FIRST;
  }
  mspi_mWriteReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, ctrl_reg_val);
  mspi_mWriteReg(slave_setting_ptr->master->base_address, PLB_SPI_MASTER_SLVSEL_REG_OFFSET, slave_setting_ptr->slave_select);
}

/***************************************************************************/


void mspi_manual_slv_sel(plb_spi_master *instance_ptr, unsigned int manual_slv_sel)
{
  /* wait until fsm is not busy */
  while(mspi_is_busy(instance_ptr));

  /* write manual slave select bit mask */
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_MAN_SLVSEL_REG_OFFSET, manual_slv_sel);
}


/***************************************************************************/

int mspi_tx_fifo_is_full(plb_spi_master *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_STATUS_REG_OFFSET) & PLB_SPI_MASTER_STATUS_TXFIFO_FULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_tx_fifo_is_almost_full(plb_spi_master *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_STATUS_REG_OFFSET) & PLB_SPI_MASTER_STATUS_TXFIFO_AFULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_is_busy(plb_spi_master *instance_ptr)
{
  return !!(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_STATUS_REG_OFFSET) & PLB_SPI_MASTER_STATUS_SPI_BUSY);
}

/***************************************************************************/

int mspi_rx_fifo_is_empty(plb_spi_master *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_STATUS_REG_OFFSET) & PLB_SPI_MASTER_STATUS_RXFIFO_EMPTY)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_rx_fifo_is_almost_empty(plb_spi_master *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_STATUS_REG_OFFSET) & PLB_SPI_MASTER_STATUS_RXFIFO_AEMPTY)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_get_status(plb_spi_master *instance_ptr)
{
  return mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_STATUS_REG_OFFSET);
}

/***************************************************************************/

void mspi_set_loopback(plb_spi_master *instance_ptr, int loopback_enable)
{
  unsigned int ctrl_reg_val;

  /* read control register */
  ctrl_reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  if(loopback_enable)
  {
    ctrl_reg_val |=  PLB_SPI_MASTER_CONFIG_LOOP;
  }
  else
  {
    ctrl_reg_val &= ~PLB_SPI_MASTER_CONFIG_LOOP;
  }
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, ctrl_reg_val);
}

/***************************************************************************/

void mspi_reset(plb_spi_master *instance_ptr)
{
  unsigned int reg_val;

  reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val | PLB_SPI_MASTER_CONFIG_SW_RESET);
  usleep(100);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val & PLB_SPI_MASTER_CONFIG_SW_RESET);
}

/***************************************************************************/

void mspi_reset_rxfifo(plb_spi_master *instance_ptr)
{
  unsigned int reg_val;

  reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val | PLB_SPI_MASTER_CONFIG_RXFIFO_RST);
  usleep(100);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val & PLB_SPI_MASTER_CONFIG_RXFIFO_RST);
}

/***************************************************************************/

void mspi_reset_txfifo(plb_spi_master *instance_ptr)
{
  unsigned int reg_val;

  reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val | PLB_SPI_MASTER_CONFIG_TXFIFO_RST);
  usleep(100);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val & PLB_SPI_MASTER_CONFIG_TXFIFO_RST);
}


/***************************************************************************/

void mspi_reset_nobfifo(plb_spi_master *instance_ptr)
{
  unsigned int reg_val;

  reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val | PLB_SPI_MASTER_CONFIG_NOBFIFO_RST);
  usleep(100);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val & PLB_SPI_MASTER_CONFIG_NOBFIFO_RST);
}

/***************************************************************************/
/***************************************************************************/
