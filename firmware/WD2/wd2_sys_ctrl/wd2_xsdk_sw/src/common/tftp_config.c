/******************************************************************************/
/*                                                                            */
/*  file: tftp_config.c                                                       */
/*                                                                            */
/*  (c) 2016 PSI tg32                                                         */
/*                                                                            */
/******************************************************************************/


/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "dbg.h"
#include "xfs_printf.h"

#include "tftp_config.h"
#include "wd2_flash_memory_map.h"
#include "drv_plb_wd2_reg_bank.h"
#include "register_map_wd2.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xtime.h"

#include "xilinx_cfg.h"
#include "revision.h"

/******************************************************************************/
/* global vars 1/2                                                            */
/******************************************************************************/

/*tftp_target_bitfile_spi_flash_type  cb_spi_fbfw =            */
/*  {                                                          */
    /* spi_flash           */  /* SYSPTR(spi_flash),       */
    /* manual_slave_sel    */  /* PLB_SPI_MASTER_SLVSEL_NONE,  */
    /* fpga_type           */  /* CB_FPGA_TYPE,                */
    /* fpga_uid            */  /* CB_FPGA_UID,                 */
    /* flash_start_addr    */  /* CB_SPI_FLASH_FBFW_ADDR,      */
    /* bitfile_info_offset */  /* CB_SPI_FLASH_FW_HEADER_OFFS, */
    /* bitfile_valid       */  /* 0,                           */
    /* header_len          */  /* 0,                           */
    /* data_len            */  /* 0                            */
/*  };                                                         */


tftp_target_bitfile_spi_flash_type  wd_spi_fw0 =
  {
    /* spi_flash           */  0,
    /* manual_slave_sel    */  PLB_SPI_MASTER_SLVSEL_NONE,     
    /* fpga_type           */  (char*)WD2_FPGA_TYPE,
    /* fpga_uid            */  (char*)WD2_FPGA_UID,
    /* flash_start_addr    */  SPI_FLASH_BITSTREAM_ADDR,
    /* bitfile_info_offset */  SPI_FLASH_FW_HEADER_OFFS, 
    /* bitfile_valid       */  0,
    /* header_len          */  0, 
    /* data_len            */  0
  };

tftp_target_sw_spi_flash_type  wd_spi_sw0 =
  {
    /* spi_flash           */  0,
    /* manual_slave_sel    */  PLB_SPI_MASTER_SLVSEL_NONE,
    /* fw_comp_lvl         */  0,
    /* reg_layout_comp_lvl */  0,
    /* flash_start_addr    */  SPI_FLASH_SOFTWARE_ADDR,
    /* sw_info_offset      */  SPI_FLASH_SW_HEADER_OFFS,
    /* sw_file_valid       */  0,
    /* header_len          */  0, 
    /* max_data_len        */  SPI_FLASH_SW_HEADER_OFFS,
    /* sw_file_len         */  0
  };

tftp_target_reboot_type  wd_reboot =
  {
    /* code_reg            */  WD2_REG_TIME_LSB,
    /* nr_of_code_regs     */  2,
    /* reset_reg           */  WD2_RECONFIGURE_FPGA_REG,
    /* fpga_reset_reg_mask */  WD2_RECONFIGURE_FPGA_MASK,
    /* code_valid          */  0
  };

/******************************************************************************/
/* functions                                                                  */
/******************************************************************************/

void tftp_config_init()
{
  wd_spi_fw0.spi_flash = SYSPTR(spi_flash);
  wd_spi_sw0.spi_flash = SYSPTR(spi_flash);
}

/****************************************************************************************************/

int tftp_target_bitfile_spi_flash(tftp_server_type *tftp_server, unsigned int cmd, unsigned char* buff, unsigned int len)
{
  tftp_target_bitfile_spi_flash_type *par = (tftp_target_bitfile_spi_flash_type *)tftp_server->target->param_ptr;
  unsigned int   fpos = tftp_server->fpos;
  
  unsigned int   flash_addr;
  unsigned int   flash_len; 
  unsigned char* flash_buff;

  int            header_len;
  int            str_len;
  bitfile_info_type bit_inf;
  int i;


  if (!par) return -1;

  switch (cmd)
  {
    /* READ */
    case TFTP_TARGET_HANDLER_CMD_RD_PREP:

      par->bitfile_valid = 0;    
      
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, par->manual_slave_sel);
      }

      par->header_len = 0;
/*    unsigned int bitfile_info_checksum(bitfile_info_type *biptr); */
            flash_addr = par->flash_start_addr + par->bitfile_info_offset;
            flash_len  = sizeof(bitfile_info_type);
            flash_buff = (unsigned char*) &bit_inf;

            spi_flash_read(par->spi_flash, flash_buff, flash_addr, flash_len);

            if (bit_inf.info.checksum == bitfile_info_checksum(&bit_inf))
            {
              par->header_len = bit_inf.info.head_len;
              par->data_len   = bit_inf.info.data_len;
              par->bitfile_valid = 1;
/*              if (DBG_INF1) xfs_local_printf("FOUND Header: header_len %d  \r\n", par->header_len); */
              
            }
            else
            {
              par->bitfile_valid = 0;
              tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "No valid Bitfile found"); 
              return -1;
            }

      break;


    case TFTP_TARGET_HANDLER_CMD_RD:
      
      if (!par->bitfile_valid)
      {
              tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "No valid Bitfile found"); 
         
         return -1;
      }
      
      if (fpos < par->header_len)
      {
        
        flash_addr = par->flash_start_addr + par->bitfile_info_offset +  sizeof(bitfile_info_type) + fpos;
        header_len  = par->header_len - fpos;
        if (header_len > (int)len) header_len = len;
        flash_buff = buff;
        spi_flash_read(par->spi_flash, flash_buff, flash_addr, header_len);
      }
      else
      {
        header_len = 0;
      }

      if (header_len < (int)len)
      {
        flash_buff = buff + header_len;
        flash_len  = len - header_len;
        
        if (par->data_len < (fpos - par->header_len + flash_len))
        {
          flash_len = par->data_len - fpos + par->header_len - header_len;
        }
        
        flash_addr = par->flash_start_addr + fpos - par->header_len + header_len;
        spi_flash_read(par->spi_flash, flash_buff, flash_addr, flash_len);        
      }
      else
      {
        flash_len = 0;
      }
      

      return (flash_len + header_len);
      break;

    case TFTP_TARGET_HANDLER_CMD_RD_DONE:
    case TFTP_TARGET_HANDLER_CMD_RD_CLEANUP:
      par->bitfile_valid = 0;
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, PLB_SPI_MASTER_SLVSEL_NONE);
      }
    
      break;


    /* WRITE */
    case TFTP_TARGET_HANDLER_CMD_WR_PREP:
      gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_FW_UPDATE);
      par->bitfile_valid = 0;    
      par->header_len = 0;
      
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, par->manual_slave_sel);
      }      
    
      break;

    case TFTP_TARGET_HANDLER_CMD_WR:

      gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_FW_UPDATE);
      if (fpos == 0)
      {

         
         header_len = parse_bitfile(buff, len, &bit_inf);
         if (header_len > 0)
         {

            for (i=0; i<4; i++)
            {
              if( DBG_INF4 ) xfs_local_printf("field %d  : %s \r\n", i, buff + bit_inf.field[i]);
            }
            if( DBG_INF4 ) xfs_local_printf("size = %d\r\n", bit_inf.info.data_len);
            par->header_len = header_len;
            
            /* got valid header */

            /* check fpga type */
            if (par->fpga_type)
            {
              str_len = bit_inf.info.date_offs - bit_inf.info.fpga_offs;
              if (strncmp(par->fpga_type, (const char*) buff + bit_inf.info.fpga_offs, str_len) != 0)
              {
                if (DBG_WARN) xfs_local_printf("Warning: Bitfile for wrong FPGA type: %s  expected: %s\r\n", buff + bit_inf.info.fpga_offs, par->fpga_type);
                tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "Wrong FPGA type");
                return -1;
              }
            }

            /* check fpga type */
            if (par->fpga_uid)
            {
/*
              str_len = bit_inf.info.date_offs - bit_inf.info.fpga_offs;
              if (strncmp(par->fpga_type, (const char*) buff + bit_inf.info.fpga_offs, str_len) != 0)
              {
                if (DBG_WARN) xfs_local_printf("Warning: Bitfile for wrong FPGA type: %s  expected: %s\r\n", buff + bit_inf.info.fpga_offs, par->fpga_type);
                tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "Wrong FPGA type");
                return -1;
              }
 */
 /* tbd */
            }


            par->bitfile_valid = 1;
            
            flash_addr = par->flash_start_addr + par->bitfile_info_offset;
            flash_len  = sizeof(bitfile_info_type);
            flash_buff = (unsigned char*) &bit_inf;

            spi_flash_sector_erase(par->spi_flash, flash_addr);
            spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);

            flash_addr += flash_len;
            flash_len   = header_len;
            flash_buff  = buff;
            spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);

            /* write buffer excluding header */
            flash_addr = par->flash_start_addr + fpos;
            flash_len  = len  - header_len;
            flash_buff = buff + header_len;              
            spi_flash_sect_start_range_erase(par->spi_flash, flash_addr, flash_len);
            spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);
         }
         else
         {
           tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "Invalid Bitfile"); 
           return -1;
         }
      }
      else
      {
        if (!par->bitfile_valid)
        {
           tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "Invalid Bitfile"); 
           return -1;
        }
        /* write whole buffer */

        flash_addr = par->flash_start_addr + fpos - par->header_len;
        flash_len  = len;
        flash_buff = buff;
        spi_flash_sect_start_range_erase(par->spi_flash, flash_addr, flash_len);
        spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);
      }
        

   
      break;
   
    case TFTP_TARGET_HANDLER_CMD_WR_LAST:
    case TFTP_TARGET_HANDLER_CMD_WR_CLEANUP:
      par->bitfile_valid = 0;
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, PLB_SPI_MASTER_SLVSEL_NONE);
      }
      gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_FW_UPDATE);

      break;

    case TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK:
      break;

    default:
      break;
  }
  
  return 0;
}

/****************************************************************************************************/


int tftp_target_sw_spi_flash(tftp_server_type *tftp_server, unsigned int cmd, unsigned char* buff, unsigned int len)
{
  tftp_target_sw_spi_flash_type *par = (tftp_target_sw_spi_flash_type *)tftp_server->target->param_ptr;
  unsigned int   fpos = tftp_server->fpos;
  
  unsigned int   flash_addr;
  unsigned int   flash_len; 
  unsigned char* flash_buff;

  int            header_len;
  
  unsigned char sr_header_buf[SREC_MAX_BYTES];
  char hdr_ckh_warning[250];

  sw_file_info_type sw_inf;
        

  if (!par) return -1;

  switch (cmd)
  {
    /* READ */
    case TFTP_TARGET_HANDLER_CMD_RD_PREP:

      par->sw_file_valid = 0;    
      par->sw_file_len   = 0;    
      
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, par->manual_slave_sel);
      }

      par->header_len = 0;
      
      flash_addr = par->flash_start_addr + par->sw_info_offset;
      flash_len  = sizeof(sw_file_info_type);
      flash_buff = (unsigned char*) &sw_inf;
      
      spi_flash_read(par->spi_flash, flash_buff, flash_addr, flash_len);
      
      if (sw_inf.info.checksum == sw_file_info_checksum(&sw_inf))
      {
        par->header_len    = sw_inf.info.head_len;
        par->sw_file_len   = sw_inf.info.data_len;
        par->sw_file_valid = 1;
        /*if (DBG_INFO) xfs_local_printf("FOUND Header for sw file %s \r\n", xxxx); */
      }
      else
      {
        par->sw_file_valid = 0;
        tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "No valid software found"); 
        return -1;
      }

      if(par->max_data_len < par->sw_file_len)
      {
        par->sw_file_valid = 0;
        tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "No valid software found"); 
        return -1;
      }

      break;


    case TFTP_TARGET_HANDLER_CMD_RD:
      
      if (!par->sw_file_valid)
      {
        tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "No valid software found"); 
        return -1;
      }
      
      if(len > (par->sw_file_len - fpos) )
      {
        len = par->sw_file_len - fpos;
      }
      flash_buff = buff;
      flash_len  = len;
      flash_addr = par->flash_start_addr + fpos;
      spi_flash_read(par->spi_flash, flash_buff, flash_addr, flash_len);

      return flash_len;
      
      break;

    case TFTP_TARGET_HANDLER_CMD_RD_DONE:
    case TFTP_TARGET_HANDLER_CMD_RD_CLEANUP:
      par->sw_file_valid = 0;
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, PLB_SPI_MASTER_SLVSEL_NONE);
      }
    
      break;

      
    /* WRITE */
    case TFTP_TARGET_HANDLER_CMD_WR_PREP:
      gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);
      par->sw_file_valid = 0;    
      par->header_len = 0;
      
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, par->manual_slave_sel);
      }      
    
      break;

    case TFTP_TARGET_HANDLER_CMD_WR:

      gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);
      if (fpos == 0)
      {
         header_len = parse_srec(buff, sr_header_buf);
         if (header_len > 0)
         {
            if( DBG_INF0 ) xfs_local_printf("SREC header: %s \r\n", sr_header_buf);
            par->header_len = header_len;
            par->fw_comp_lvl = get_sw_fcl((char*)sr_header_buf);
            par->reg_layout_comp_lvl = get_sw_rcl((char*)sr_header_buf);
            
            /* got valid header */
            par->sw_file_valid = 1;
            
            /* write header info plain (filename ASCII) */
            flash_addr = par->flash_start_addr + par->sw_info_offset + sizeof(sw_file_info_type);
            flash_len  = header_len+1;
            flash_buff = sr_header_buf;
            spi_flash_sector_erase(par->spi_flash, flash_addr);
            spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);
         }
         else
         {
           tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "Invalid SW file"); 
           return -1;
         }
      }

      if (!par->sw_file_valid)
      {
         tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "Invalid SW file"); 
         return -1;
      }

      /* write whole buffer */
      flash_addr = par->flash_start_addr + fpos;
      flash_len  = len;
      flash_buff = buff;
      spi_flash_sect_start_range_erase(par->spi_flash, flash_addr, flash_len);
      spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);
   
      break;
   
    case TFTP_TARGET_HANDLER_CMD_WR_LAST:

      sw_inf.info.name_offs = sizeof(sw_file_info_type);
      sw_inf.info.data_len  = fpos;
      sw_inf.info.head_len  = par->header_len;
      sw_inf.info.checksum  = sw_file_info_checksum(&sw_inf);
      
      flash_addr = par->flash_start_addr + par->sw_info_offset;
      flash_len  = sizeof(sw_file_info_type);
      flash_buff = (unsigned char*) &sw_inf;

      spi_flash_sector_erase(par->spi_flash, flash_addr);
      spi_flash_write(par->spi_flash, flash_buff, flash_addr, flash_len);
      /* no break; */

    case TFTP_TARGET_HANDLER_CMD_WR_CLEANUP:
      par->sw_file_valid = 0;
      if (par->manual_slave_sel != PLB_SPI_MASTER_SLVSEL_NONE)
      {
        mspi_manual_slv_sel(par->spi_flash->spi_slave_ptr->master, PLB_SPI_MASTER_SLVSEL_NONE);
      }
      gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);

      break;

    case TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK:
      if ( get_fw_fcl() != par->fw_comp_lvl ||
           get_fw_rcl() != par->reg_layout_comp_lvl )
      {
        xfs_snprintf(hdr_ckh_warning, 250, "(Warning) compatibility levels of running firmware and uploaded software do not match:\r\nFW: FCL=0x%02X RCL=0x%04X\r\nSW: FCL=0x%02X RCL=0x%04X\r\nUpload matching fw before reboot (if not already done)!", get_fw_fcl(), get_fw_rcl(), par->fw_comp_lvl, par->reg_layout_comp_lvl);
        tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, hdr_ckh_warning); 
        return -1;
      }
        
      break;

    default:
      break;
  }
  
  return 0;
}

/****************************************************************************************************/

int tftp_target_reboot(tftp_server_type *tftp_server, unsigned int cmd, unsigned char* buff, unsigned int len)
{
  tftp_target_reboot_type *par = (tftp_target_reboot_type *)tftp_server->target->param_ptr;
  unsigned int  fpos = tftp_server->fpos;
  
  unsigned int  nr_of_regs = par->nr_of_code_regs;
  unsigned int  reset_val = 0;
  
  static unsigned char code_target_buff[2*4]  = {0,0,0,0,0,0,0,0};
  static unsigned char code_receive_buff[2*4] = {0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF,0xFF};
  
  unsigned int i;
  
  if (!par) return -1;

  switch (cmd)
  {
    case TFTP_TARGET_HANDLER_CMD_RD_PREP:

      if (DBG_INFO) xfs_local_printf("\r\nTFTP reboot code requested...\r\n");

      reg_bank_read(par->code_reg, (unsigned int*)code_target_buff, nr_of_regs);
      par->code_valid = 1;

      break;

    case TFTP_TARGET_HANDLER_CMD_RD:
    
      if (!par->code_valid)
      {
        tftp_send_error_str(tftp_server, TFTP_ERR_NOTDEFINED, "No valid reboot code found"); 
        return -1;
      }
      
      if(len > (par->nr_of_code_regs*4 - fpos) )
      {
        len = par->nr_of_code_regs*4 - fpos;
      }

      for( i=0; i<len; i++) buff[i] = code_target_buff[fpos+i];

      return len;
      
      break;    

    case TFTP_TARGET_HANDLER_CMD_RD_DONE:

      if (DBG_INFO) xfs_local_printf("TFTP reboot code sent\r\n");

    case TFTP_TARGET_HANDLER_CMD_RD_CLEANUP:
    
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_PREP:

      break;

    case TFTP_TARGET_HANDLER_CMD_WR:

      if ( (fpos >= par->nr_of_code_regs*4) && (len > 0) )
      {
        /* Invalidate receive buffer if length does not match */
        code_receive_buff[0] = ~code_target_buff[0];
        break;
      }

      if ( fpos+len >= par->nr_of_code_regs*4 ) len = par->nr_of_code_regs*4-fpos;
      
      for( i=0 ; i<len ; i++)
      {
        code_receive_buff[fpos+i] = buff[i];
      }
   
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_LAST:
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_CLEANUP:
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_DONE:

      if (!par->code_valid)
      {
        if (DBG_WARN) xfs_local_printf("TFTP no reboot code requested\r\n");
        return -1;
      }

      for( i=0 ; i<sizeof(code_receive_buff) ; i++)
      {
        if( code_receive_buff[i] != code_target_buff[i] )
        {
          if (DBG_WARN) xfs_local_printf("TFTP reboot code missmatch\r\n");
          return -1;
        }
      }

      if (DBG_INFO) xfs_local_printf("\r\n\r\nREBOOTING...\r\n\r\n\r\n");
      
      usleep(100000);

      reset_val = par->fpga_reset_reg_mask;
      reg_bank_set(par->reset_reg, &reset_val, 1);

      break;

    case TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK:
      break;

    default:
      break;
  }
  
  return 0;
}

/****************************************************************************************************/

int tftp_target_t1(tftp_server_type *tftp_server, unsigned int cmd, unsigned char* buff, unsigned int len)
{
  /*tftp_target_spi_flash_type *par = (tftp_target_spi_flash_type *)tftp_server->target->param_ptr; */
  unsigned int   fpos = tftp_server->fpos;
  int i;
  
  switch (cmd)
  {
    case TFTP_TARGET_HANDLER_CMD_RD_PREP:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_RD_PREP\r\n");
      break;

    case TFTP_TARGET_HANDLER_CMD_RD:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_RD   len=%d   fpos=%d\r\n",len, fpos);

      for(i=0; i<(int)len; i++) buff[i] = i;
      
      if (fpos < 4000) return TFTP_DATA_PACKET_MSG_LEN;

      return 333;
      break;

    case TFTP_TARGET_HANDLER_CMD_RD_DONE:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_RD_DONE\r\n");
      break;    

    case TFTP_TARGET_HANDLER_CMD_WR_PREP:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_WR_PREP\r\n");
      break;

    case TFTP_TARGET_HANDLER_CMD_WR:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_WR   len=%d   fpos=%d\r\n",len, fpos);
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_LAST:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_WR_LAST \r\n");
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_CLEANUP:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_WR_CLEANUP \r\n");
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_DONE:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_WR_DONE \r\n");
      break;

    case TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK:
      xfs_local_printf("tftp_target_t1 TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK \r\n");
      break;

    default:
      break;
  }
  
  return 0;
}

/****************************************************************************************************/

/******************************************************************************/
/* global vars 2/2                                                            */
/******************************************************************************/

tftp_target_type tftp_targets[] = 
  {
    {"/fw",     tftp_target_bitfile_spi_flash, &wd_spi_fw0},
    /* {"/fbfw", tftp_target_bitfile_spi_flash, &cb_spi_fbfw}, */
    {"/sw",     tftp_target_sw_spi_flash,      &wd_spi_sw0},
    {"/reboot", tftp_target_reboot,            &wd_reboot},
    {"\0",      NULL,                          NULL},
  };


/******************************************************************************/

