
/*#include <stdio.h> */
#include "ether_com.h"
/*#include "pg_com.h" */
#include "plb_ll_fifo.h"
#include "dhcp.h"
#include "xtime.h"
#include "string.h"
#include "utilities.h"
#include "xfs_printf.h"
#include "dbg.h"
#include "xfs_printf.h"
#include "gpio_slow_control.h"
/*#include "udp_pack_gen.h" */
#include "system.h"

/******************************************************************************/

unsigned int get_uint_val(unsigned char* cp)
{
  unsigned val = 0;
  int i;

  for (i=0;i<4;i++)
  {
    val = (val << 8) | cp[i];
  }
  return val;
}

/******************************************************************************/

void dhcp_construct(dhcp_type *self, network_if_type *nw_if_i)
{
  self->nw_if = nw_if_i;
  dhcp_init(self);
}

/******************************************************************************/

void dhcp_init(dhcp_type *self)
{
  self->lease_time = 0;
  self->renewal_time = 0;
  self->rebinding_time = 0;
  self->last_dhcp_ack_time = 0;
  self->last_req = 0;
  self->initial_req = 1;
  self->init_reboot_retries = 0;

  self->dhcp_state = DHCP_STATE_INIT;
}



/******************************************************************************/

int dhcp_opt_magic_cookie(unsigned char* cptr)
{
  cptr[0] = 0x63;
  cptr[1] = 0x82;
  cptr[2] = 0x53;
  cptr[3] = 0x63;
  return 4;
}

/******************************************************************************/

int dhcp_opt_message_type(unsigned char* cptr, unsigned char message)
{
  cptr[0] = DHCP_OPT_DHCPMSGTYPE;  /* DHCP Message Type */
  cptr[1] = 1;                     /* len = 1           */
  cptr[2] = message;
  return 3;
}

/******************************************************************************/

int dhcp_opt_server_ip(unsigned char* cptr, unsigned char* server_ip)
{
  int i;
  cptr[0] = DHCP_OPT_SERVERID; /* code */
  cptr[1] = 4;                 /* len  */
  for (i=0;i<4;i++)
  {
    cptr[2+i] = server_ip[i];
  }
  return 6;
}

/******************************************************************************/

int dhcp_opt_requested_ip(unsigned char* cptr, unsigned char* requested_ip)
{
  int i;
  cptr[0] = DHCP_OPT_REQUESTEDIP; /* code */
  cptr[1] = 4;                    /* len  */
  for (i=0;i<4;i++)
  {
    cptr[2+i] = requested_ip[i];
  }
  return 6;
}

/******************************************************************************/

int dhcp_opt_host_name(unsigned char* cptr, char* name)
{
  int i=0;
  cptr[0] = DHCP_OPT_HOSTNAME; /* code */

  while(*name)
  {
    cptr[2 + i++] = *name++;
  }

  cptr[1] = i;  /* len */
  return (2+i);
}

/******************************************************************************/

int dhcp_opt_end(unsigned char* cptr)
{
  cptr[0] = DHCP_OPT_END; /* code */
  return 1;
}


/******************************************************************************/

int dhcp_get_message_type(unsigned char* cptr)
{
  int i=4;
  int len;

  /* check magic cookie */
  if (cptr[0] != 0x63) return 0;
  if (cptr[1] != 0x82) return 0;
  if (cptr[2] != 0x53) return 0;
  if (cptr[3] != 0x63) return 0;

  while(i<(DHCP_OPT_LEN-1) && cptr[i]!= DHCP_OPT_END)
  {
    len = cptr[i+1];
    switch (cptr[i])
    {
      case DHCP_OPT_PAD :
           i += 1;              /* just skip PAD */
           continue;

      case DHCP_OPT_DHCPMSGTYPE :
           if (len != 0x01) return 0;
/*           xfs_local_printf("DHCP_OPT_DHCPMSGTYPE %d \r\n",cptr[i+2]); */
           return cptr[i+2];

      default   :  break;
    }
    i = i + len + 2;
  }

  return 0;
}



/******************************************************************************/

int dhcp_parse_options(dhcp_type *self, unsigned char* cptr)
{
  int i=4;  /* skip magic cookie */
  int len;
/*  unsigned char  subnet_mask[4];       */
/*  unsigned char  broadcast_address[4]; */


  while(i<(DHCP_OPT_LEN-1) && cptr[i]!= DHCP_OPT_END)
  {
    len = cptr[i+1];
    switch (cptr[i])
    {

      case DHCP_OPT_PAD :
           i += 1;              /* just skip PAD */
           continue;



      case DHCP_OPT_SERVERID :
                    if (len==4)
                    {
                      ncpy((char*)self->server_ip_addr, (char*)&cptr[i+2],4);
/*                      xfs_local_printf("DHCP Server: %d.%d.%d.%d\r\n",
 *                                self->server_ip_addr[0],self->server_ip_addr[1],self->server_ip_addr[2],self->server_ip_addr[3]);
 */
                    }
                    break;

      case DHCP_OPT_NETMASK :
                    if (len==4)
                    {
                      ncpy((char*)self->subnet_mask, (char*)&cptr[i+2],4);
/*                      xfs_local_printf("subnet_mask : %d.%d.%d.%d\r\n", 
 *                                subnet_mask[0],subnet_mask[1],subnet_mask[2],subnet_mask[3]);
 */
                    }
                    break;

      case DHCP_OPT_BROADCAST :
                    if (len==4)
                    {
                      ncpy((char*)self->broadcast_address, (char*)&cptr[i+2],4);
/*                      xfs_local_printf("broadcast_address : %d.%d.%d.%d\r\n", 
 *                                broadcast_address[0], broadcast_address[1], broadcast_address[2], broadcast_address[3]);
 */
                    }
                    break;

      case DHCP_OPT_LEASETIME :
                   self->lease_time = get_uint_val(&cptr[i+2]);
/*                   xfs_local_printf("Lease Time: %d seconds  \r\n",lease_time); 
 */
                   break;

      case DHCP_OPT_RENEWALTIME :
                   self->renewal_time = get_uint_val(&cptr[i+2]);
/*                   xfs_local_printf("Renewal Time: %d seconds  \r\n",renewal_time); 
 */
                   break;

      case DHCP_OPT_REBINDTIME :
                   self->rebinding_time = get_uint_val(&cptr[i+2]);
/*                   xfs_local_printf("Rebinding Time: %d seconds  \r\n",rebinding_time); 
 */
                   break;


      default   :  break;
    }
    i = i + len + 2;
  }


  return 1;
}


/******************************************************************************/
/*
   ---------------------------------------------------------------------
   |              |INIT-REBOOT  |SELECTING    |RENEWING     |REBINDING |
   ---------------------------------------------------------------------
   |broad/unicast |broadcast    |broadcast    |unicast      |broadcast |
   |server-ip     |MUST NOT     |MUST         |MUST NOT     |MUST NOT  |
   |requested-ip  |MUST         |MUST         |MUST NOT     |MUST NOT  |
   |ciaddr        |zero         |zero         |IP address   |IP address|
   ---------------------------------------------------------------------
*/


void dhcp_send_frame(dhcp_type *self)
{
  int i;
  int len;
  unsigned char* opt_ptr;
  udp_header_type *p = (udp_header_type *)self->send_buff;
  dhcp_frame_type *d = (dhcp_frame_type *)&((self->send_buff)[sizeof(udp_header_type)]);
  char name[17];
  for (i=0; i < (int)DHCP_SENDBUFF_LEN; i++) self->send_buff[i] = 0;

/* start building request... */


  fcp(p->src_mac, self->nw_if->mac_addr);

  p->len_type[0]       = 0x08;
  p->ver_headerlen[0]  = 0x45;
  p->service_type[0]   = 0x10;
  p->identification[0] = 0x00;
  p->identification[1] = 0x00;
  p->flags[0]          = 0x40;
  p->frag_offset[0]    = 0x00;
  p->time_to_live[0]   = 0x40;

  if (self->dhcp_state & DHCP_STATE_RENEWING)
  {
    /* unicast */
    fcp(p->dst_mac, self->dhcp_server_mac_addr);

    fcp(p->src_ip, self->nw_if->ip_addr);
    fcp(p->dst_ip, self->server_ip_addr);


  }
  else
  {
    /* broadcast */
    for (i=0; i < 6; i++) p->dst_mac[i] = 0xff;
    p->dst_ip[0]         = 0xff;
    p->dst_ip[1]         = 0xff;
    p->dst_ip[2]         = 0xff;
    p->dst_ip[3]         = 0xff;
  }

  p->protocol[0]       = 0x11; /* UDP protocol */
  p->dst_port[0]       = (DHCP_UDP_SERVER_PORT >> 8) & 0xff;
  p->dst_port[1]       = (DHCP_UDP_SERVER_PORT) & 0xff;
  p->src_port[0]       = (DHCP_UDP_CLIENT_PORT >> 8) & 0xff;
  p->src_port[1]       = (DHCP_UDP_CLIENT_PORT) & 0xff;

  d->op[0]    = BOOTREQUEST;
  d->htype[0] = 0x01;
  d->hlen[0]  = 0x06;
  d->hops[0]  = 0x00;
  ncpy((char*)d->xid, (char*)&(self->nw_if->mac_addr[2]),4);
  d->secs[0]  = 0x0;
  d->flags[0] = 0x0;
  fcp(d->chaddr, self->nw_if->mac_addr);

  opt_ptr  = &(d->options[0]);
  opt_ptr += dhcp_opt_magic_cookie(opt_ptr);



  if (self->dhcp_state & (DHCP_STATE_INIT))
  {
    opt_ptr += dhcp_opt_message_type(opt_ptr, DHCP_DISCOVER);
    if (self->nw_if->ip_addr[3] != 0)
    {
      opt_ptr += dhcp_opt_requested_ip(opt_ptr, self->nw_if->ip_addr);
    }
    if (DBG_INF3)
      xfs_local_printf("\r\nDHCP_STATE_INIT: sending DHCP_DISCOVER...\r\n");
  }
  else if (self->dhcp_state & (DHCP_STATE_INIT_REBOOT))
  {
    opt_ptr += dhcp_opt_message_type(opt_ptr, DHCP_REQUEST);
    opt_ptr += dhcp_opt_requested_ip(opt_ptr, self->nw_if->ip_addr);
    if (DBG_INF3)
      xfs_local_printf("\r\nDHCP_STATE_INIT_REBOOT: sending DHCP_REQUEST...\r\n");
  }
  else if (self->dhcp_state & (DHCP_STATE_SELECTING))
  {
    opt_ptr += dhcp_opt_message_type(opt_ptr, DHCP_REQUEST);
    opt_ptr += dhcp_opt_server_ip(opt_ptr, self->server_ip_addr);
    opt_ptr += dhcp_opt_requested_ip(opt_ptr, self->nw_if->ip_addr);
    if (DBG_INF3)
      xfs_local_printf("\r\nDHCP_STATE_SELECTING: sending DHCP_REQUEST...\r\n");
  }
  else if (self->dhcp_state & (DHCP_STATE_RENEWING | DHCP_STATE_REBINDING))
  {
    opt_ptr += dhcp_opt_message_type(opt_ptr, DHCP_REQUEST);
    fcp(d->ciaddr, self->nw_if->ip_addr);
    if (DBG_INF3) {
      if (self->dhcp_state & DHCP_STATE_RENEWING)
        xfs_local_printf("\r\nDHCP_STATE_RENEWING: sending DHCP_REQUEST...\r\n");
      if (self->dhcp_state & DHCP_STATE_REBINDING)
        xfs_local_printf("\r\nDHCP_STATE_REBINDING: sending DHCP_REQUEST...\r\n");
    }
  }
  else
  {
    if (DBG_ERR) xfs_local_printf("\r\nERROR: illegal dhcp state\r\n");
  }

  strncpy(name, SYSPTR(cfg)->hostname, NAME_BUF_LEN);

  if ( self->nw_if->if_name && self->nw_if->if_name[0])
  {
    strncat(name, "-", NAME_BUF_LEN);
    strncat(name, self->nw_if->if_name, NAME_BUF_LEN);
  }

  opt_ptr += dhcp_opt_host_name(opt_ptr, name);
  opt_ptr += dhcp_opt_end(opt_ptr);

  len = sizeof(udp_header_type) + sizeof(dhcp_frame_type);
  adjust_header(p, len);

  self->nw_if->ll_fifo->send(self->nw_if->ll_fifo, (unsigned char*)p, len);

}

/******************************************************************************/

void dhcp_request(dhcp_type *self)
{
  unsigned int request_intervall = 2; /* seconds */

  if (self->dhcp_state & (DHCP_STATE_BOUND | DHCP_STATE_RENEWING | DHCP_STATE_REBINDING))
  {
    request_intervall = 60; /* seconds */
  }


/*
 *  if (!self->nw_if->xtemac->link_up)
 *  {
 *    if (self->dhcp_state & (DHCP_STATE_BOUND | DHCP_STATE_RENEWING | DHCP_STATE_REBINDING | DHCP_STATE_INIT_REBOOT))
 *    {
 *       self->dhcp_state = DHCP_STATE_INIT_REBOOT;
 *       init_reboot_retries = 0;
 *    }
 *    else
 *    {
 *      self->dhcp_state = DHCP_STATE_INIT;
 *    }
 *    last_req = 0;
 *    initial_req = 1;
 *    return;
 *  }
 */

  if (self->dhcp_state == DHCP_STATE_BOUND)
  {
    if ((time_get_sec() - self->last_dhcp_ack_time) >= self->renewal_time)
    {
      self->dhcp_state = DHCP_STATE_RENEWING;
      self->last_req = 0;
      self->initial_req = 1;
    }
  }

  if (self->dhcp_state == DHCP_STATE_RENEWING)
  {
    if ((time_get_sec() - self->last_dhcp_ack_time) >= self->rebinding_time)
    {
      self->dhcp_state = DHCP_STATE_REBINDING;
      self->last_req = 0;
      self->initial_req = 1;
    }
  }

  if ((time_get_sec() - self->last_dhcp_ack_time) >= self->lease_time)
  {
    self->nw_if->ip_address_valid = 0;
    self->dhcp_state = DHCP_STATE_INIT;
  }


  if (self->dhcp_state & (DHCP_STATE_BOUND)) return;
  gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_DHCP_REQ);

  if (self->initial_req)
  {
    /* use tics for initial delay of 500 ms */
    if (self->last_req == 0) self->last_req = time_tic_read();
    if (msec_since_tic(self->last_req) < 500) return;
  }
  else
  {
    /* use second counter for larger intervalls */
    if (time_get_sec() - self->last_req < request_intervall) return;
  }


  dhcp_send_frame(self);

  if (self->dhcp_state & (DHCP_STATE_INIT_REBOOT))
  {
    if (++self->init_reboot_retries >= 3)
    {
      self->init_reboot_retries = 0;
      self->dhcp_state = DHCP_STATE_INIT;
    }
  };

  self->last_req = time_get_sec();
  self->initial_req = 0;
}

/******************************************************************************/


int dhcp_handle_req(dhcp_type *self, unsigned char* frame, int len)
{
  /*udp_gen_header_entry_type udp_gen_entry; */
  udp_header_type *ip_fp = (udp_header_type *)frame;
  dhcp_frame_type *d     = (dhcp_frame_type *)(frame+sizeof(udp_header_type));
  int message_type;

  /* print_frame(frame, len); */

  if ((unsigned int)len < sizeof(udp_header_type))     return 0;
  if (!fcmp(ip_fp->dst_mac, self->nw_if->mac_addr))  return 0;
  /* if (!fcmp(ip_fp->dst_ip,  my_ip_addr))  return 0;*/ /* not yet set! */


  if (ip_fp->protocol[0] != 0x11) return 0;  /* UDP protocol */
  if (ip_fp->dst_port[0] != ((DHCP_UDP_CLIENT_PORT >> 8) & 0xff)) return 0;  /* UDP port */
  if (ip_fp->dst_port[1] != ((DHCP_UDP_CLIENT_PORT)      & 0xff)) return 0;  /* UDP port */

  if (d->op[0]    != BOOTREPLY) return 0;
  if (d->htype[0] != 0x01) return 0;
  if (d->hlen[0]  != 0x06) return 0;
  if (!ncmp((char*)d->xid,  (char*)&(self->nw_if->mac_addr[2]), 4)) return 0;

  if (self->dhcp_state == DHCP_STATE_BOUND) return 1; /* ignore other offers */

  message_type = dhcp_get_message_type(d->options);

  if (message_type == DHCP_OFFER)
  {
     dhcp_parse_options(self, d->options);

/*    xfs_local_printf("DHCP Offer: %d.%d.%d.%d\r\n", d->yiaddr[0],d->yiaddr[1],d->yiaddr[2],d->yiaddr[3]); */

    fcp(self->nw_if->ip_addr, d->yiaddr);

    self->dhcp_state = DHCP_STATE_SELECTING;
    dhcp_send_frame(self);
    return 1;
  }
  else if (message_type == DHCP_ACK)
  {
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_DHCP_REQ);

    self->lease_time = 0;
    self->renewal_time = 0;
    self->rebinding_time = 0;

    dhcp_parse_options(self, d->options);


    if (DBG_INIT)
      xfs_local_printf("\r\nIP address          : %d.%d.%d.%d\r\n",
              self->nw_if->ip_addr[0],self->nw_if->ip_addr[1],self->nw_if->ip_addr[2],self->nw_if->ip_addr[3]);

    fcp(self->dhcp_server_mac_addr, ip_fp->src_mac);

    if (self->lease_time == 0)
    {
       self->lease_time = 21600; /* 6 hours */
       if (DBG_WARN) xfs_local_printf("\r\nWarning: did not get lease_time, setting to %d\r\n",self->lease_time);
    }

    if (self->renewal_time == 0)   self->renewal_time   = self->lease_time / 2;
    if (self->rebinding_time == 0) self->rebinding_time = self->lease_time / 8 * 7;

/* some tests          */
/*---------------------*/
/* renewal_time=60;    */
/* rebinding_time=100; */
/* lease_time = 120;   */

/*  if (self->dhcp_state & (DHCP_STATE_RENEWING)) return;  */
/*  if (self->dhcp_state & (DHCP_STATE_REBINDING)) return; */

    self->last_dhcp_ack_time = time_get_sec();

    self->dhcp_state = DHCP_STATE_BOUND;
    self->nw_if->ip_address_valid = 1;
    
    if (DBG_INF0) dhcp_print_times(self);
    
    return 1;
  }

 return 1;
}

/******************************************************************************/

void dhcp_print_times(dhcp_type *self)
{
  xfs_local_printf("Lease Time          : %6d seconds  \r\n", self->lease_time);
  xfs_local_printf("Renewal Time        : %6d seconds  \r\n", self->renewal_time);
  xfs_local_printf("Rebinding Time      : %6d seconds  \r\n", self->rebinding_time);

  if (self->dhcp_state != DHCP_STATE_INIT)
  {
    xfs_local_printf("Time since last Ack : %6d seconds  \r\n", (time_get_sec() - self->last_dhcp_ack_time));
  }

}

/******************************************************************************/
