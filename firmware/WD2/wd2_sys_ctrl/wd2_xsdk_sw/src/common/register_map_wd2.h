/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e (Author of generation script)
 *  Created :  24.07.2019 11:41:38
 *
 *  Description :  Register map definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __REGISTER_MAP_H__
#define __REGISTER_MAP_H__

#ifdef WD2_REV_F
#define WD2_REGISTER_MAP_V8
#include "register_map_wd2_v8.h"
#endif
#ifdef WD2_REV_G
#define WD2_REGISTER_MAP_V9
#include "register_map_wd2_v9.h"
#endif

/******************************************************************************/

#endif /* __REGISTER_MAP_H__ */

/******************************************************************************/
