/*
 * (C) Copyright 2002-2008
 * Wolfgang Denk, DENX Software Engineering, wd@denx.de.
 *
 * See file CREDITS for list of people who contributed to this
 * project.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston,
 * MA 02111-1307 USA
 */


#ifndef __FW_ENV_H__
#define __FW_ENV_H__



/******************************************************************************/

typedef unsigned long ulong;
typedef unsigned int  uint32_t;
typedef unsigned char uint8_t;

/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/

#define FW_ENV_FLAG_ACTIVE    1
#define FW_ENV_FLAG_OBSOLETE  0   /* obsolete_flag must be 0 to efficiently set it on NOR flash without erasing */

#define FW_ENV_STORAGE_OK     0
#define FW_ENV_STORAGE_ERROR  1

/* commands for storage handler */
#define FW_ENV_STORAGE_READ   0
#define FW_ENV_STORAGE_WRITE  1

#define FW_ENV_CLEAN_BYTE     0xff

#define FW_ENV_CRC32_POLYNOMIAL  0xEDB88320

/******************************************************************************/
/* type definitions                                                           */
/******************************************************************************/

/* env layout differs for single and redundant environments (Unfortunately) */

struct env_image_single 
{
  uint32_t      crc;      /* CRC32 over data bytes    */
  char          data[];
};


struct env_image_redundant 
{
  uint32_t      crc;      /* CRC32 over data bytes  */
  unsigned char flags;    /* active or obsolete     */
  char          data[];
};


/******************************************************************************/
/* storage description                                                        */
/******************************************************************************/

typedef void (*fw_env_trigger_func)(const char*, const char*);


typedef struct
{
  const char           *var_name;
  fw_env_trigger_func  trigger_func;
} fw_env_trigger_entry_type;


/******************************************************************************/
/* storage description                                                        */
/******************************************************************************/

typedef struct fw_env_storage_desc_struct fw_env_storage_desc_type;

typedef int (*fw_env_storage_handler_type)(fw_env_storage_desc_type*, unsigned int, char*, unsigned int);


/******************************************************************************/

struct fw_env_storage_desc_struct
{
  void *storage_ptr;
  unsigned int addr;
  fw_env_storage_handler_type storage_handler;
};

/******************************************************************************/

typedef struct
{
  char *env_mem;
  ulong env_size;

  uint32_t *crc;
  unsigned char *flags;
  char *data;
  ulong data_size_max;

  char *default_env;
  ulong default_env_size;
  
  ulong stg_entries;
  ulong env_sel;
  fw_env_storage_desc_type *env_stg;
  fw_env_trigger_entry_type *trigger_list;
  
} fw_env_type;



/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/

ulong fw_env_init(fw_env_type *self, char *mem, unsigned int size, char *default_env, unsigned int default_env_size, fw_env_storage_desc_type *env_stg, unsigned int storage_entries, fw_env_trigger_entry_type *trigger_list);
int   fw_env_load(fw_env_type *self);
int   fw_env_save(fw_env_type *self);
void  fw_env_default(fw_env_type *self);
void  fw_env_clean(fw_env_type *self);
int   fw_printenv(fw_env_type *self, int argc, char *argv[]);
int   fw_setenv(fw_env_type *self, int argc, char *argv[]);
char* fw_getenv(fw_env_type *self, const char *name);
int   fw_env_write(fw_env_type *self, char *name, char *value);
void  fw_env_trigger_var(fw_env_type *self, char *env_entry);
void  fw_env_trigger_all(fw_env_type *self);
uint32_t fw_env_crc32(uint32_t crc, const unsigned char *data, int len);


#endif  /*  __FW_ENV_H__  */
