/******************************************************************************/
/*                                                                            */
/*  file: tftp_config.h                                                       */
/*                                                                            */
/*  (c) 2016 PSI tg32                                                         */
/*                                                                            */
/******************************************************************************/

#ifndef __TFTP_CONFIG_H__
#define __TFTP_CONFIG_H__


/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "tftp_server.h"
#include "system.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/



extern tftp_target_type tftp_targets[];

typedef struct
{
  spi_flash_w25q64 *spi_flash;  
  unsigned int manual_slave_sel;
  char* fpga_type;
  char* fpga_uid;
  unsigned int flash_start_addr;
  unsigned int bitfile_info_offset;
  unsigned int bitfile_valid;
  unsigned int header_len;
  unsigned int data_len;
} tftp_target_bitfile_spi_flash_type;

typedef struct
{
  spi_flash_w25q64 *spi_flash;  
  unsigned int manual_slave_sel;
  unsigned int fw_comp_lvl;
  unsigned int reg_layout_comp_lvl;
  unsigned int flash_start_addr;
  unsigned int sw_info_offset;
  unsigned int sw_file_valid;
  unsigned int header_len;
  unsigned int max_data_len;
  unsigned int sw_file_len;
} tftp_target_sw_spi_flash_type;

typedef struct
{
  unsigned int code_reg;
  unsigned int nr_of_code_regs;
  unsigned int reset_reg;
  unsigned int fpga_reset_reg_mask;
  unsigned int code_valid;
} tftp_target_reboot_type;

/******************************************************************************/

void tftp_config_init();

/******************************************************************************/

#endif /* __TFTP_CONFIG_H__ */
