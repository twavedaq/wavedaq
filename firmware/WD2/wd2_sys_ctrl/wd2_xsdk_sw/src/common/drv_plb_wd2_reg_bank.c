/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_wd2_reg_bank_v1_00_a/src/plb_wd2_reg_bank.c
* Version:           1.00.a
* Description:       plb_wd2_reg_bank Driver Source File
* Date:              Thu Aug 14 12:00:46 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/


/***************************** Include Files *******************************/

#include "drv_plb_wd2_reg_bank.h"
#include "drv_spi_flash_w25q64.h"
#include "crc32.h"
#include "wd2_flash_memory_map.h"
#include "system.h"
#include "register_map_wd2.h"
#include "xfs_printf.h"
#include "dbg.h"
#include "fw_env.h"
#include "utilities.h"
//#include <stdlib.h>

#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS

#ifdef WD2_REV_F
#include "drv_soft_spi_adc_ad9637.h"
#else
#include "drv_soft_spi_adc_ltm9009.h"
#endif

#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */

/************************** Function Definitions ***************************/

void reg_bank_init(unsigned int stat_reg_baseaddr, unsigned int ctrl_reg_baseaddr)
{
  SYSPTR(reg_bank)->status_reg_base_address = stat_reg_baseaddr;
  SYSPTR(reg_bank)->control_reg_base_address = ctrl_reg_baseaddr;
}

/***************************************************************************/

#ifdef WD2_DONT_INCLUDE_REG_ACCESS_VARS

unsigned int reg_read(unsigned int offs)
{
  unsigned int val;

  val = wd2_hw_reg(WD2_REG_READ, 0, offs, 0);

  if (DBG_ALL) xfs_printf("simple reg_read:  reg[0x%04X] => 0x%08X (%d)\r\n", offs,val,val);
  return val;
}

#else

unsigned int reg_read(unsigned int offs)
{
  unsigned int val;
  unsigned int i_reg;

  if (offs > REG_CTRL_START)
  {
    i_reg = ((offs-REG_CTRL_START)/4) % REG_NR_OF_CTRL_REGS;
    val = wd2_ctrl_reg_func_list[i_reg].func(WD2_REG_READ, wd2_ctrl_reg_func_list[i_reg].par, offs, 0);
  }
  else
  {
    i_reg = ((offs-REG_STAT_START)/4) % REG_NR_OF_STAT_REGS;
    val = wd2_stat_reg_func_list[i_reg].func(WD2_REG_READ, wd2_stat_reg_func_list[i_reg].par, offs, 0);
  }

  if (DBG_ALL) xfs_printf("reg_read:  reg[0x%04X] => 0x%08X (%d)\r\n", offs,val,val);
  return val;
}

#endif

/***************************************************************************/

#ifdef WD2_DONT_INCLUDE_REG_ACCESS_VARS

void reg_write(unsigned int offs, unsigned int data)
{
  wd2_hw_reg(WD2_REG_WRITE, 0, offs, data);

  if (DBG_ALL) xfs_printf("simple reg_write: reg[0x%04X] <= 0x%08x (%d)\r\n", offs,data,data);
}

#else

void reg_write(unsigned int offs, unsigned int data)
{
  unsigned int i_reg;

  if (offs > REG_CTRL_START)
  {
    i_reg = ((offs-REG_CTRL_START)/4) % REG_NR_OF_CTRL_REGS;
    wd2_ctrl_reg_func_list[i_reg].func(WD2_REG_WRITE, wd2_ctrl_reg_func_list[i_reg].par, offs, data);
  }
  else
  {
    i_reg = ((offs-REG_STAT_START)/4) % REG_NR_OF_STAT_REGS;
    wd2_stat_reg_func_list[i_reg].func(WD2_REG_WRITE, wd2_stat_reg_func_list[i_reg].par, offs, data);
  }

  if (DBG_ALL) xfs_printf("reg_write: reg[0x%04X] <= 0x%08x (%d)\r\n", offs,data,data);
}

#endif

/***************************************************************************/

void reg_bank_write(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words)
{
  unsigned int i;

  for(i=0;i<no_words;i++)
  {
    reg_write(offset+(i*4), buffer_ptr[i]);
  }
}

/***************************************************************************/

void reg_bank_set(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words)
{
  unsigned int i;
  int reg_content;

  for(i=0;i<no_words;i++)
  {
    /* read */
    reg_content = reg_read(offset+(i*4));
    /* modify */
    reg_content = reg_content | buffer_ptr[i];
    /* write */
    reg_write(offset+(i*4), reg_content);
  }
}

/***************************************************************************/

void reg_bank_clr(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words)
{
  unsigned int i;
  int reg_content;

  for(i=0;i<no_words;i++)
  {
    /* read */
    reg_content = reg_read(offset+(i*4));
    /* modify */
    reg_content = reg_content & ~buffer_ptr[i];
    /* write */
    reg_write(offset+(i*4), reg_content);
  }
}

/***************************************************************************/

void reg_bank_read(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words)
{
  unsigned int i;

  for(i=0;i<no_words;i++)
  {
    buffer_ptr[i] = reg_read(offset+(i*4));
  }
}

/***************************************************************************/

unsigned int reg_bank_get(unsigned int offset)
{
  return reg_read(offset);
}

/***************************************************************************/

void reg_bank_load()
{
  unsigned int reg_buffer[REG_NR_OF_CTRL_REGS];
  unsigned int checksum;
  int snr;
  char *cp;

  /* read register contents from SPI flash */
  spi_flash_read(SYSPTR(spi_flash), (unsigned char*)(reg_buffer), SPI_FLASH_REG_CONTENTS_ADDR, REG_NR_OF_CTRL_REGS*4);

  /* verify crc32 checksum */
  checksum = crc32 (0, (unsigned char*)(reg_buffer), (REG_NR_OF_CTRL_REGS-1)*4);

  /* if checksum us ok, copy contents into register bank else issue warning */
  if(checksum == reg_buffer[REG_NR_OF_CTRL_REGS-1])
  {
    /* memcpy((unsigned char*)(SYSPTR(reg_bank)->control_reg_base_address), reg_buffer_cptr, REG_NR_OF_CTRL_REGS*4); */
    reg_bank_write(REG_CTRL_START, reg_buffer, REG_NR_OF_CTRL_REGS);
  }
  else
  {
    if(DBG_WARN)
    {
      xfs_printf("Warning: CRC checksum of register bank invalid.\r\n");
      xfs_printf("         Calculated: 0x%08X\r\n", checksum);
      xfs_printf("         From flash: 0x%08X\r\n", reg_buffer[REG_NR_OF_CTRL_REGS-1]);
    }

    if ((cp = fw_getenv(SYSPTR(env), "sn")))
    {
      snr = xfs_atoi(cp);
      if(DBG_WARN)
      {
        xfs_printf("         Valid serial number found.\r\n");
        xfs_printf("         Board is being re-initialized with \"init %d\".\r\n\r\n", snr);
      }
      init_settings(snr);
    }
    else
    {
      if(DBG_WARN)
      {
        xfs_printf("         No valid serial number found.\r\n");
        xfs_printf("         Register contents are not updated.\r\n\r\n");
      }
    }
  }
}

/***************************************************************************/

#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS

void reg_bank_store()
{
  unsigned int reg_buffer[REG_NR_OF_CTRL_REGS];
  unsigned int checksum;

  reg_bank_read(REG_CTRL_START, reg_buffer, REG_NR_OF_CTRL_REGS);

  /* calculate and store crc32 checksum */
  checksum = crc32 (0, (unsigned char*)(reg_buffer), (REG_NR_OF_CTRL_REGS-1)*4);
  if(DBG_INFO) xfs_printf("\r\nRegister CRC = 0x%08X\r\n", checksum);
  reg_bank_write(WD2_REG_CRC32_REG_BANK, &checksum, 1);
  reg_buffer[REG_NR_OF_CTRL_REGS-1] = checksum;

  /* erase SPI flash sector */
  spi_flash_sector_erase(SYSPTR(spi_flash), SPI_FLASH_REG_CONTENTS_ADDR);
  /* store register bank contents */
  spi_flash_write(SYSPTR(spi_flash), (unsigned char*)(reg_buffer), SPI_FLASH_REG_CONTENTS_ADDR, REG_NR_OF_CTRL_REGS*4);
}

#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */

/***************************************************************************/
/* register handler functions                                              */
/***************************************************************************/

unsigned int wd2_hw_reg(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  unsigned int base_address;

  base_address = SYSPTR(reg_bank)->status_reg_base_address;

  if ( cmd == WD2_REG_WRITE )
  {
    PLB_WD2_REG_BANK_mWriteReg(base_address, offs, data);
  }
  else if ( cmd == WD2_REG_READ )
  {
    return PLB_WD2_REG_BANK_mReadReg(base_address, offs);
  }

  return 0;
}

/***************************************************************************/
/* Only use special functions if function list in register map is active */
#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS
/***************************************************************************/
#ifdef WD2_REV_F
/***************************************************************************/

unsigned int wd2_reg_adc_glob(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  /* Note on AD9637 operation:
   * Specific bits of registers 0x0D, 0x10 and 0x14 are global (apply to full device) some are
   * local (channel specific). Using this function (i.e. writing to the ADC_TEST_OFFS_OM register)
   * simultaneously overwrites the settings of all channels.
   * To write the local settings of a single the wd2_reg_adc0_ch and wd2_reg_adc1_ch functions have
   * to be called which is done by writing to the corresponding channel register
   * (ADC_0_CH_A_CFG to ADC_1_CH_FCO_CFG). The global bits are read-only in these register.
   */
  int i;
  unsigned int i_reg;
  unsigned int rd_data;

  i_reg = ((offs-WD2_REG_ADC_CFG_IND_TX)/4) % AD9637_NO_GLOB_REGS;

  /* setting channel address to "all channels" */
  ad9637_adc_01_wr_cfg(0x05, AD9637_CFG_DI1_ALL);
  ad9637_adc_01_wr_cfg(0x04, AD9637_CFG_DI2_ALL);

  if ( cmd == WD2_REG_WRITE )
  {
    for( i=3 ; i>=0 ; i-- )
    {
      if( (ad9637_addr[i_reg][i] != 0x05) && (ad9637_addr[i_reg][i] != 0x04) )
      /* don't overwrite the channel address registers */
      {
        ad9637_adc_01_wr_cfg(ad9637_addr[i_reg][i], (unsigned char)(data&0xFF));
      }
      data >>= 8;
    }
  }
  else if ( cmd == WD2_REG_READ )
  {
    rd_data = 0;
    for( i=0 ; i<=3 ; i++ )
    {
      rd_data <<= 8;
      rd_data |= ad9637_adc_0_rd_cfg(ad9637_addr[i_reg][i]);
    }
    return rd_data;
  }

  return 0;
}

/***************************************************************************/

unsigned int wd2_reg_adc0_ch(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  /* Note on AD9637 operation:
   * Specific bits of registers 0x0D, 0x10 and 0x14 are global (apply to full device) some are
   * local (channel specific). Using this function (i.e. writing to the ADC_TEST_OFFS_OM register)
   * simultaneously overwrites the settings of all channels.
   * To write the local settings of a single the wd2_reg_adc0_ch and wd2_reg_adc1_ch functions have
   * to be called which is done by writing to the corresponding channel register
   * (ADC_0_CH_A_CFG to ADC_1_CH_FCO_CFG). The global bits are read-only in these register.
   */
  int i;
  unsigned int rd_data;
  unsigned int glob_data;

  /* setting channel address */
  ad9637_adc_0_wr_cfg(0x05, ad9637_di1_ch[par]);
  ad9637_adc_0_wr_cfg(0x04, ad9637_di2_ch[par]);

  if ( cmd == WD2_REG_WRITE )
  {
    /* read global data */
    glob_data = 0;
    for( i=1 ; i<=3 ; i++ )
    {
      glob_data <<= 8;
      glob_data |= ad9637_adc_0_rd_cfg(ad9637_addr[2][i]);
    }

    data = (data & AD9637_LOCAL_MASK) | (glob_data & AD9637_GLOBAL_MASK);

    for( i=3 ; i>=1 ; i-- )
    {
      ad9637_adc_0_wr_cfg(ad9637_addr[2][i], (unsigned char)(data&0xFF));
      data >>= 8;
    }
  }
  else if ( cmd == WD2_REG_READ )
  {
    rd_data = 0;
    for( i=1 ; i<=3 ; i++ )
    {
      rd_data <<= 8;
      rd_data |= ad9637_adc_0_rd_cfg(ad9637_addr[2][i]);
    }
    return rd_data;
  }

  /* resetting channel address */
  ad9637_adc_01_wr_cfg(0x05, AD9637_CFG_DI1_ALL);
  ad9637_adc_01_wr_cfg(0x04, AD9637_CFG_DI2_ALL);

  return 0;
}

/***************************************************************************/

unsigned int wd2_reg_adc1_ch(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  /* Note on AD9637 operation:
   * Specific bits of registers 0x0D, 0x10 and 0x14 are global (apply to full device) some are
   * local (channel specific). Using this function (i.e. writing to the ADC_TEST_OFFS_OM register)
   * simultaneously overwrites the settings of all channels.
   * To write the local settings of a single the wd2_reg_adc0_ch and wd2_reg_adc1_ch functions have
   * to be called which is done by writing to the corresponding channel register
   * (ADC_0_CH_A_CFG to ADC_1_CH_FCO_CFG). The global bits are read-only in these register.
   */
  int i;
  unsigned int rd_data;
  unsigned int glob_data;

  /* setting channel address */
  ad9637_adc_1_wr_cfg(0x05, ad9637_di1_ch[par]);
  ad9637_adc_1_wr_cfg(0x04, ad9637_di2_ch[par]);

  if ( cmd == WD2_REG_WRITE )
  {
    /* read global data */
    glob_data = 0;
    for( i=1 ; i<=3 ; i++ )
    {
      glob_data <<= 8;
      glob_data |= ad9637_adc_1_rd_cfg(ad9637_addr[2][i]);
    }

    data = (data & AD9637_LOCAL_MASK) | (glob_data & AD9637_GLOBAL_MASK);

    for( i=3 ; i>=1 ; i-- )
    {
      ad9637_adc_1_wr_cfg(ad9637_addr[2][i], (unsigned char)(data&0xFF));
      data >>= 8;
    }
  }
  else if ( cmd == WD2_REG_READ )
  {
    rd_data = 0;
    for( i=1 ; i<=3 ; i++ )
    {
      rd_data <<= 8;
      rd_data |= ad9637_adc_1_rd_cfg(ad9637_addr[2][i]);
    }
    return rd_data;
  }

  /* resetting channel address */
  ad9637_adc_01_wr_cfg(0x05, AD9637_CFG_DI1_ALL);
  ad9637_adc_01_wr_cfg(0x04, AD9637_CFG_DI2_ALL);

  return 0;
}

/***************************************************************************/
#else /* WD2_REV_F */
/***************************************************************************/

unsigned int wd2_reg_adc_0_1458(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  if ( cmd == WD2_REG_WRITE )
  {
    ltm9009_adc_0_1458_wr_cfg(data);
  }
  else if ( cmd == WD2_REG_READ )
  {
    return ltm9009_adc_0_1458_rd_cfg();
  }

  return 0;
}

/***************************************************************************/

unsigned int wd2_reg_adc_0_2367(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  if ( cmd == WD2_REG_WRITE )
  {
    ltm9009_adc_0_2367_wr_cfg(data);
  }
  else if ( cmd == WD2_REG_READ )
  {
    return ltm9009_adc_0_2367_rd_cfg();
  }

  return 0;
}

/***************************************************************************/

unsigned int wd2_reg_adc_1_1458(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  if ( cmd == WD2_REG_WRITE )
  {
    ltm9009_adc_1_1458_wr_cfg(data);
  }
  else if ( cmd == WD2_REG_READ )
  {
    return ltm9009_adc_1_1458_rd_cfg();
  }

  return 0;
}

/***************************************************************************/

unsigned int wd2_reg_adc_1_2367(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data)
{
  if ( cmd == WD2_REG_WRITE )
  {
    ltm9009_adc_1_2367_wr_cfg(data);
  }
  else if ( cmd == WD2_REG_READ )
  {
    return ltm9009_adc_1_2367_rd_cfg();
  }

  return 0;
}

/***************************************************************************/
#endif /* WD2_REV_F */
/***************************************************************************/
#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */
/***************************************************************************/
/***************************************************************************/
