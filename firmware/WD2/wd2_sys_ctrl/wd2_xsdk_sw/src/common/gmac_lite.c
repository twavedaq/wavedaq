/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  tg32
 *  Created :  15.08.2014
 *
 *  Description :  gmac lite functions
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "utilities.h"
#include "gmac_lite.h"
#include "xfs_printf.h"


/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/


/******************************************************************************/
/* global var definitions                                                     */
/******************************************************************************/


/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

void gmac_lite_construct(gmac_lite_type *s, unsigned int base_addr)
{
  s->base_addr = base_addr;
}

/******************************************************************************/

void gmac_lite_init(gmac_lite_type *s)
{
  gmac_lite_phy_reset(s, 1);

  xfs_out32(s->base_addr + GMAC_LITE_REG_CONFIG, GMAC_LITE_CONFIG_DEFAULT); 
  usleep(2);
  gmac_lite_ifg_set(s, 12);
  /* enable broadcast, disable promiscuos mode */
  /* set mac to 00:00:00:00:00:00              */
  xfs_out32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_LOW),  0x00);
  xfs_out32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH), GMAC_LITE_MAC_FILTER_HIGH_BRDCST_EN);
  gmac_lite_phy_reset(s, 0);
}

/******************************************************************************/

void gmac_lite_mac_filter_set(gmac_lite_type *s, const unsigned char *mac_addr)
{
  int i;
  unsigned int mac_r0 = 0;
  unsigned int mac_r1 = 0;
  unsigned int mode_prev ;

  mode_prev = xfs_in32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH)) & 0xFFFF0000;

  for (i=0;i<4;i++) mac_r0 =  (mac_r0 << 8) | mac_addr[i];
  for (i=4;i<6;i++) mac_r1 =  (mac_r1 << 8) | mac_addr[i];

  mac_r1 |= mode_prev; /* set previous broadcast and promiscuous mode; */

  xfs_out32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_LOW),  mac_r0);
  xfs_out32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH), mac_r1);
}

/******************************************************************************/

int gmac_lite_mac_filter_get(gmac_lite_type *s, unsigned char *mac_addr)
{
  int i, enabled;
  unsigned int mac_r0 = 0;
  unsigned int mac_r1 = 0;

  mac_r0 = xfs_in32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_LOW) );
  mac_r1 = xfs_in32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH));

  for (i=0;i<4;i++) mac_addr[i] = ((mac_r0 >> ((3-i)*8)) & 0xff) ;
  for (i=4;i<6;i++) mac_addr[i] = ((mac_r1 >> ((5-i)*8)) & 0xff) ;

  enabled = (mac_r1 & GMAC_LITE_MAC_FILTER_HIGH_PROMISC_EN) ? 0 : 1;

  return enabled;

}
/******************************************************************************/

int gmac_lite_promiscuos_mode(gmac_lite_type *s, int promisc)
{
  unsigned int mode ;
  unsigned int prev ;

  mode = xfs_in32(s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH);

  prev = !!(mode & GMAC_LITE_MAC_FILTER_HIGH_PROMISC_EN);
  
  if (promisc >= 0)
  {
    if (promisc) mode |=  GMAC_LITE_MAC_FILTER_HIGH_PROMISC_EN;
    else         mode &= ~GMAC_LITE_MAC_FILTER_HIGH_PROMISC_EN;

    xfs_out32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH), mode);
  }
  
  return prev;
}

/******************************************************************************/

int gmac_lite_rcv_broadcast(gmac_lite_type *s, int broadcast)
{
  unsigned int mode ;
  unsigned int prev ;

  mode = xfs_in32(s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH);

  prev = !!(mode & GMAC_LITE_MAC_FILTER_HIGH_BRDCST_EN);
  
  if (broadcast >= 0)
  {  
    if (broadcast) mode |=  GMAC_LITE_MAC_FILTER_HIGH_BRDCST_EN;
    else           mode &= ~GMAC_LITE_MAC_FILTER_HIGH_BRDCST_EN;

    xfs_out32((s->base_addr + GMAC_LITE_REG_MAC_FILTER_HIGH), mode);
  }
  
  return prev;
}

/******************************************************************************/

void gmac_lite_ifg_set(gmac_lite_type *s, unsigned int ifg)
{
  xfs_out32((s->base_addr + GMAC_LITE_REG_IFG), ifg);
}


/******************************************************************************/

void gmac_lite_phy_reset(gmac_lite_type *s, unsigned int rst)
{
  unsigned int reg;

  reg = (rst) ? GMAC_LITE_REG_CTRL_SET : GMAC_LITE_REG_CTRL_CLR;
  xfs_out32((s->base_addr + reg), GMAC_LITE_CTRL_RESET);  /* set or clear clear Reset PIN */
}

/******************************************************************************/


unsigned int gmac_lite_status(gmac_lite_type *s) 
{
  unsigned int status;

  status = xfs_in32(s->base_addr + GMAC_LITE_REG_STATUS);
  
  return status;
}
