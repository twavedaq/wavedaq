/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/wd2_sys_ctrl/wd2_xps_hw/drivers/plb_wd2_reg_bank_v1_00_a/src/plb_wd2_reg_bank.h
* Version:           1.00.a
* Description:       plb_wd2_reg_bank Driver Header File
* Date:              Thu Aug 14 12:00:46 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef DRV_PLB_WD2_REG_BANK_H
#define DRV_PLB_WD2_REG_BANK_H

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "utilities.h"
/*#include "xil_io.h" */

/************************** Constant Definitions ***************************/

#define REG_STAT_START 0x0000
#define REG_CTRL_START 0x1000
#define WD2_REG_READ     0x01
#define WD2_REG_WRITE    0x02

/**************************** Type Definitions *****************************/

typedef struct {
  unsigned int  control_reg_base_address;         /**< Base address of Control Register (IPIF) */
  unsigned int  status_reg_base_address;          /**< Base address of Status Register (IPIF) */
} plb_register_bank;

/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_WD2_REG_BANK register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_WD2_REG_BANK device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 *  void PLB_WD2_REG_BANK_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PLB_WD2_REG_BANK_mWriteReg(BaseAddress, RegOffset, Data) \
  xfs_out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a PLB_WD2_REG_BANK register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_WD2_REG_BANK device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 *  Xuint32 PLB_WD2_REG_BANK_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_WD2_REG_BANK_mReadReg(BaseAddress, RegOffset) \
  xfs_in32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_WD2_REG_BANK user logic memory (BRAM).
 *
 * @param   Address is the memory address of the PLB_WD2_REG_BANK device.
 * @param   Data is the value written to user logic memory.
 *
 * @return  The data from the user logic memory.
 *
 * @note
 * C-style signature:
 *  void PLB_WD2_REG_BANK_mWriteMemory(Xuint32 Address, Xuint32 Data)
 *  Xuint32 PLB_WD2_REG_BANK_mReadMemory(Xuint32 Address)
 *
 */
#define PLB_WD2_REG_BANK_mWriteMemory(Address, Data) \
  xfs_out32(Address, (Xuint32)(Data))
#define PLB_WD2_REG_BANK_mReadMemory(Address) \
  xfs_in32(Address)

/************************** Function Prototypes ****************************/

void reg_bank_init(unsigned int stat_reg_baseaddr, unsigned int ctrl_reg_baseaddr);
void reg_bank_write(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words);
void reg_bank_set(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words);
void reg_bank_clr(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words);
void reg_bank_read(unsigned int offset, unsigned int *buffer_ptr, unsigned int no_words);
unsigned int reg_bank_get(unsigned int offset);
void reg_bank_load();

#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS

void reg_bank_store();

#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */

/***************************************************************************/

unsigned int wd2_hw_reg(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);

#ifndef WD2_DONT_INCLUDE_REG_ACCESS_VARS

#ifdef WD2_REV_F
unsigned int wd2_reg_adc_glob(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
unsigned int wd2_reg_adc0_ch(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
unsigned int wd2_reg_adc1_ch(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
#else /* WD2_REV_F */
unsigned int wd2_reg_adc_0_1458(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
unsigned int wd2_reg_adc_0_2367(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
unsigned int wd2_reg_adc_1_1458(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
unsigned int wd2_reg_adc_1_2367(unsigned int cmd, unsigned int par, unsigned int offs, unsigned int data);
#endif /* WD2_REV_F */

#endif /* WD2_DONT_INCLUDE_REG_ACCESS_VARS */

#endif /** DRV_PLB_WD2_REG_BANK_H */
