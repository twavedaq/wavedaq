/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Driver vor custom pcore plb_gpio.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_PLB_GPIO_H__
#define __DRV_PLB_GPIO_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "utilities.h"

/******************************************************************************/
/* macro definitions                                                          */
/******************************************************************************/

#define NUM_OF_REG_PER_SET  8

#define OFFSET_REG_IO  0x00 
#define OFFSET_REG_T   0x10 
#define OFFSET_OP_WR   0x00 
#define OFFSET_OP_RD   0x04 
#define OFFSET_OP_SET  0x08 
#define OFFSET_OP_CLR  0x0C 


/******************************************************************************/
/* type definitions                                                           */
/******************************************************************************/


typedef struct {
  unsigned int  base_address;         /**< Base address of device (IPIF) */
} plb_gpio_type;


typedef void (*plb_gpio_func)(plb_gpio_type *, xfs_u32, xfs_u32); 



void plb_gpio_init(plb_gpio_type *self, xfs_u32 b_address);
void plb_gpio_set(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value);
void plb_gpio_clr(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value);
void plb_gpio_write(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value);
xfs_u32 plb_gpio_rb_out_reg(plb_gpio_type *self, xfs_u32 io_register);
xfs_u32 plb_gpio_get(plb_gpio_type *self, xfs_u32 io_register);
void plb_gpio_tristate_set(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value);
void plb_gpio_tristate_clr(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value);
void plb_gpio_tristate_write(plb_gpio_type *self, xfs_u32 io_register, xfs_u32 value);
xfs_u32 plb_gpio_tristate_get(plb_gpio_type *self, xfs_u32 io_register);

/******************************************************************************/

#endif /* __DRV_PLB_GPIO_H__ */
