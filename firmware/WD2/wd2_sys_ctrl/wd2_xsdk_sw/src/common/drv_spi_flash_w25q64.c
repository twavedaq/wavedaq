/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  17.02.2015 09:12:14
 *
 *  Description :  SPI driver for communication with W25Q64xx SPI Flash
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_spi_flash_w25q64.h"
#include "xtime.h"
#include "xfs_printf.h"
#include "dbg.h"

/******************************************************************************/

void spi_flash_init(spi_flash_w25q64 *instance_ptr, plb_spi_salve_settings  *slave_settings_ptr)
{
  instance_ptr->spi_slave_ptr = slave_settings_ptr;
}

/******************************************************************************/

void spi_flash_transceive(spi_flash_w25q64 *instance_ptr, unsigned char *instr_buffer_ptr, unsigned int nr_of_instr_bytes, unsigned char *tx_buffer_ptr, unsigned char *rx_buffer_ptr, unsigned int nr_of_bytes)
{
  if(spi_flash_wait_not_busy(instance_ptr, W25Q64_TIMEOUT_US))
  {
    mspi_transceive(instance_ptr->spi_slave_ptr, instr_buffer_ptr, nr_of_instr_bytes, tx_buffer_ptr, rx_buffer_ptr, nr_of_bytes);
  }
}

/******************************************************************************/

void spi_flash_transceive_no_timeout(spi_flash_w25q64 *instance_ptr, unsigned char *instr_buffer_ptr, unsigned int nr_of_instr_bytes, unsigned char *tx_buffer_ptr, unsigned char *rx_buffer_ptr, unsigned int nr_of_bytes)
{
  mspi_transceive(instance_ptr->spi_slave_ptr, instr_buffer_ptr, nr_of_instr_bytes, tx_buffer_ptr, rx_buffer_ptr, nr_of_bytes);
}

/******************************************************************************/

int spi_flash_write_enable(spi_flash_w25q64 *instance_ptr, unsigned char enable)
{
  unsigned char cmd_buffer;

  if (enable)
  {
    cmd_buffer = W25Q64_CMD_WEN;
  }
  else
  {
    cmd_buffer = W25Q64_CMD_WDE;
  }

  spi_flash_transceive(instance_ptr, &cmd_buffer, 1, NULL, NULL, 0);

  return 1;
}

/******************************************************************************/

int spi_flash_is_busy(spi_flash_w25q64 *instance_ptr)
{
  if (W25Q64_SR1_BUSY & spi_flash_get_status_reg1(instance_ptr))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

int spi_flash_wait_not_busy(spi_flash_w25q64 *instance_ptr, unsigned int timeout_us)
{
  unsigned int timer = timeout_us;
  
  while( timer && spi_flash_is_busy(instance_ptr) )
  {
    usleep(1);
    timer--;
  }

  if(!timer)
  {
    if (DBG_ERR) xfs_local_printf("SPI flash timeout\r\n");
    return -1;
  }

  return 1;
}

/******************************************************************************/

int spi_flash_is_suspended(spi_flash_w25q64 *instance_ptr)
{
  if (W25Q64_SR2_SUS & spi_flash_get_status_reg2(instance_ptr))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void spi_flash_get_jedec_id(spi_flash_w25q64 *instance_ptr, unsigned char *id_ptr)
{
  unsigned char cmd_buffer = W25Q64_CMD_JEDEC_ID;

  spi_flash_transceive_no_timeout(instance_ptr, &cmd_buffer, 1, NULL, id_ptr, 3);
}

/******************************************************************************/

unsigned char spi_flash_get_status_reg1(spi_flash_w25q64 *instance_ptr)
{
  unsigned char cmd_buffer = W25Q64_CMD_READ_SR1;
  unsigned char rx_buffer;

  spi_flash_transceive_no_timeout(instance_ptr, &cmd_buffer, 1, NULL, &rx_buffer, 1);

  return rx_buffer;
}

/******************************************************************************/

unsigned char spi_flash_get_status_reg2(spi_flash_w25q64 *instance_ptr)
{
  unsigned char cmd_buffer = W25Q64_CMD_READ_SR2;
  unsigned char rx_buffer;

  spi_flash_transceive_no_timeout(instance_ptr, &cmd_buffer, 1, NULL, &rx_buffer, 1);

  return rx_buffer;
}

/******************************************************************************/

void spi_flash_write_status_regs(spi_flash_w25q64 *instance_ptr, unsigned char stat_reg1, unsigned char stat_reg2)
{
  unsigned char cmd_buffer;
  unsigned char tx_buffer[2];

  cmd_buffer = W25Q64_CMD_WRITE_SR;
  tx_buffer[0] = stat_reg1;
  tx_buffer[1] = stat_reg2;

  spi_flash_write_enable(instance_ptr, 1);

  spi_flash_transceive(instance_ptr, &cmd_buffer, 1, tx_buffer, NULL, 2);

  spi_flash_write_enable(instance_ptr, 0);
}

/******************************************************************************/

void spi_flash_set_qe(spi_flash_w25q64 *instance_ptr, unsigned int qe)
{
  unsigned char sr1, sr2;
  
  sr1 = spi_flash_get_status_reg1(instance_ptr);
  sr2 = spi_flash_get_status_reg2(instance_ptr);
  
  if (qe) sr2 |=  W25Q64_SR2_QE;
  else    sr2 &= ~W25Q64_SR2_QE;
  
  spi_flash_write_status_regs(instance_ptr, sr1, sr2);
}

/******************************************************************************/

void spi_flash_read(spi_flash_w25q64 *instance_ptr, unsigned char *rx_buffer, unsigned int address, unsigned int nr_of_bytes)
{
  unsigned char cmd_buffer[4];
  unsigned int nr_of_transfers;
  unsigned int read_address = address;
  unsigned int remainder;
  unsigned int i;
  int j;

/*  if (DBG_INFO) xfs_printf("spi_flash_read at 0x%08X  nr_of_bytes = %d\r\n", address, nr_of_bytes); */


  cmd_buffer[0] = W25Q64_CMD_READ_DATA;

  /*
   * Read in 256 byte steps
   * note: this is not a restriction of the memory. The implemented fifos are 512 bytes deep.
   *       in order to make sure the rx fifo does not overflow, memory access is organized in
   *       small steps.
   */
  nr_of_transfers = nr_of_bytes/W25Q64_READ_SIZE;
  remainder       = nr_of_bytes%W25Q64_READ_SIZE;
  for(i=0; i<nr_of_transfers; i++)
  {
    for(j=1;j<=3;j++)
    {
      cmd_buffer[j] = (unsigned char)((read_address>>((3-j)*8)) & 0xFF);
    }
    spi_flash_transceive(instance_ptr, cmd_buffer, 4, NULL, &rx_buffer[i*W25Q64_READ_SIZE], W25Q64_READ_SIZE);
    read_address += W25Q64_READ_SIZE;
  }
  /* if there are bytes left to transceive, go ahead */
  if (remainder)
  {
    for(j=1; j<=3; j++)
    {
      cmd_buffer[j] = (unsigned char)((read_address>>((3-j)*8)) & 0xFF);
    }
    spi_flash_transceive(instance_ptr, cmd_buffer, 4, NULL, &rx_buffer[nr_of_transfers*W25Q64_READ_SIZE], remainder);
  }
}

/******************************************************************************/

void spi_flash_write(spi_flash_w25q64 *instance_ptr, unsigned char *tx_buffer, unsigned int address, unsigned int nr_of_bytes)
{
  unsigned char cmd_buffer[4];
  unsigned int write_len;
  unsigned int bytes_to_write = nr_of_bytes;
  unsigned int write_addr = address;
  unsigned char *data_ptr = tx_buffer;
  int j;
  
  cmd_buffer[0] = W25Q64_CMD_PAGE_PROG;

  while(bytes_to_write)
  {
    for(j=1;j<=3;j++)
    {
      cmd_buffer[j] = (unsigned char)((write_addr>>((3-j)*8)) & 0xFF);
    }
    write_len = W25Q64_BYTES_PER_PAGE - (write_addr & 0xFF ); /* take care of page alignment */
    if (write_len > bytes_to_write) write_len = bytes_to_write;
    /* Write */
    spi_flash_write_enable(instance_ptr, 1);
    spi_flash_transceive(instance_ptr, cmd_buffer, 4, data_ptr, NULL, write_len);
    bytes_to_write -= write_len;
    write_addr     += write_len;
    data_ptr       += write_len;
  }

  spi_flash_write_enable(instance_ptr, 0);
}

/******************************************************************************/

void spi_flash_erase(spi_flash_w25q64 *instance_ptr, unsigned char cmd, unsigned int address)
{
  unsigned int nr_of_bytes;
  unsigned char cmd_buffer[4];
  int i;
  
  spi_flash_write_enable(instance_ptr, 1);

  cmd_buffer[0] = cmd;
  if ((cmd == W25Q64_CMD_CHIP_ERASE) || (cmd == W25Q64_CMD_CHIP_ERASE_ALT))
  {
    nr_of_bytes = 1;
  }
  else
  {
    nr_of_bytes = 4;
    for(i=3;i>0;i--)
    {
      cmd_buffer[i] = (unsigned char)(address & 0x000000FF);
      address >>= 8;
    }
  }

  spi_flash_transceive(instance_ptr, cmd_buffer, nr_of_bytes, NULL, NULL, 0);

  spi_flash_write_enable(instance_ptr, 0);
}

/******************************************************************************/

void spi_flash_chip_erase(spi_flash_w25q64 *instance_ptr)
{
  if (DBG_INFO) xfs_local_printf("\r\nSPI flash chip erase...\r\n");
  spi_flash_erase(instance_ptr, W25Q64_CMD_CHIP_ERASE, 0);
  if (DBG_INFO) xfs_local_printf("done\r\n");
}

/******************************************************************************/

void spi_flash_sector_erase(spi_flash_w25q64 *instance_ptr, unsigned int address)
{
  if (DBG_INFO) xfs_local_printf("\r\nSPI flash sector erase at 0x%08X...\r\n", address);
  spi_flash_erase(instance_ptr, W25Q64_CMD_SECTOR_ERASE, address);
  if (DBG_INFO) xfs_local_printf("done\r\n");
}

/******************************************************************************/

void spi_flash_block32k_erase(spi_flash_w25q64 *instance_ptr, unsigned int address)
{
  if (DBG_INFO)  xfs_local_printf("\r\nSPI flash 32kB block erase at 0x%08X...\r\n", address);
  spi_flash_erase(instance_ptr, W25Q64_CMD_BLOCK32_ERASE, address);
  if (DBG_INFO)  xfs_local_printf("done\r\n");
}

/******************************************************************************/

void spi_flash_block64k_erase(spi_flash_w25q64 *instance_ptr, unsigned int address)
{
  if (DBG_INFO) xfs_local_printf("\r\nSPI flash 64kB block erase at 0x%08X...\r\n", address);
  spi_flash_erase(instance_ptr, W25Q64_CMD_BLOCK64_ERASE, address);
  if (DBG_INFO) xfs_local_printf("done\r\n");
}

/******************************************************************************/

void spi_flash_range_erase(spi_flash_w25q64 *instance_ptr, unsigned int address, unsigned int len)
{
  unsigned int sect_addr;
  /* erase all sectors touched by range */
  for (sect_addr=address & ~W25Q64_SECTOR_MASK; sect_addr < address + len; sect_addr+= W25Q64_BYTES_PER_SECTOR)
  {
    spi_flash_sector_erase(instance_ptr, sect_addr);
  }
}

/******************************************************************************/

void spi_flash_sect_start_range_erase(spi_flash_w25q64 *instance_ptr, unsigned int address, unsigned int len)
{
  unsigned int sect_addr;
  /* erase sectors whose start address is included in range range */
  
  for (sect_addr=address & ~W25Q64_SECTOR_MASK; sect_addr < address + len; sect_addr+= W25Q64_BYTES_PER_SECTOR)
  {
    if (address <= sect_addr) spi_flash_sector_erase(instance_ptr, sect_addr);
  }
}

/******************************************************************************/

void spi_flash_reset(spi_flash_w25q64 *instance_ptr)
{
  unsigned char cmd_buffer;
  unsigned int wait_cycles = 0;

  while (spi_flash_is_suspended(instance_ptr))
  {
    usleep(1);
    wait_cycles++;
  }
  while (spi_flash_is_busy(instance_ptr))
  {
    usleep(1);
    wait_cycles++;
  }

  cmd_buffer = W25Q64_CMD_EN_RESET;
  spi_flash_transceive(instance_ptr, &cmd_buffer, 1, NULL, NULL, 0);

  while (spi_flash_is_busy(instance_ptr))
  {
    usleep(1);
    wait_cycles++;
  }

  cmd_buffer = W25Q64_CMD_RESET;
  spi_flash_transceive(instance_ptr, &cmd_buffer, 1, NULL, NULL, 0);
}

/******************************************************************************/
/******************************************************************************/
