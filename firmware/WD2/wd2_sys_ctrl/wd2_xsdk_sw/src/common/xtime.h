/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Code to use with a simple 32 bit timer running continuously.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __XTIME_H__
#define __XTIME_H__

/******************************************************************************/

#include "xparameters.h"
#include "xfs_types.h"
#include "utilities.h"

#ifdef LINUX_COMPILE

#include <sys/time.h>

static inline xfs_u32 time_tic_read(void)
{
  unsigned long long temp_usec;
  xfs_u32   usec;
  struct timeval  tv;

  gettimeofday(&tv, (timezone*)0);
  temp_usec = (tv.tv_sec * 1000000) + tv.tv_usec;

  usec = (xfs_u32) temp_usec;

  return usec;
}

//  double time_in_mill = (tv.tv_sec) * 1000 + (tv.tv_usec) / 1000 ; // convert tv_sec & tv_usec to millisecond

#define msec_since_tic(tic) ((time_tic_read() - (tic) ) / 1000)

/******************************************************************************/
#else  // no LINUX_COMPILE
/******************************************************************************/

/*#define TIME_FREQUENCY 125000000 */
#define TIME_FREQUENCY XPAR_PROC_BUS_0_FREQ_HZ

/* beware of rounding problems !!! */
#define TIME_PICO_SEC_PER_TIC (1000000000 / (TIME_FREQUENCY/1000))

/* #define time_tic_read() (mfspr(XWD2_REG_SPR_TBL_READ)) */
#define time_tic_read() (xfs_in32(XPAR_PLB_TIMEBASE_0_BASEADDR))



#define msec_since_tic(tic) (((time_tic_read()-(tic))/1000) * (TIME_PICO_SEC_PER_TIC/100) / 10000)
#define usec_since_tic(tic) ((time_tic_read()-(tic)) * (TIME_PICO_SEC_PER_TIC/100) / 10000)
#define nsec_since_tic(tic) ((time_tic_read()-(tic)) * (TIME_PICO_SEC_PER_TIC/100) / 10 )

void usleep(unsigned int usec);
void msleep(unsigned int msec);
/*void sleep(unsigned int sec); */
void time_check_seconds(void);
unsigned int time_get_sec(void);

void msleep(unsigned int msec);

/******************************************************************************/
#endif /* LINUX_COMPILE */

/******************************************************************************/

#endif /* __XTIME_H__ */
