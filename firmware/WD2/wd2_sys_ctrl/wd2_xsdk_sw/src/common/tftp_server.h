/*******************************************************************************
*
* generic tftp server 
*
*******************************************************************************/

#ifndef __TFTP_SERVER_H__
#define __TFTP_SERVER_H__


#include "ether_com.h"
#include "network_if.h"
#include "tftp_server.h"


/******************************************************************************/
/* CONSTANT DEFINITIONS                                                       */
/******************************************************************************/

#define TFTP_DATA_PACKET_MSG_LEN   512
#define TFTP_DATA_PACKET_HDR_LEN     4
#define TFTP_DATA_PACKET_LEN     (TFTP_DATA_PACKET_MSG_LEN + TFTP_DATA_PACKET_HDR_LEN)
#define TFTP_ACK_PACKET_LEN          4

#define TFTP_MAX_RETRIES            10
#define TFTP_TIMEOUT_INTERVAL_MS  2000

#define TFTP_REQUEST_PORT           69
#define TFTP_READ_PORT            4096
#define TFTP_WRITE_PORT           4098


/******************************************************************************/
/* TFTP TARGET HANDLER COMMAND TYPEs                                          */
/******************************************************************************/

#define TFTP_TARGET_HANDLER_CMD_RD_PREP      1
#define TFTP_TARGET_HANDLER_CMD_RD           2
#define TFTP_TARGET_HANDLER_CMD_RD_DONE      3
#define TFTP_TARGET_HANDLER_CMD_RD_CLEANUP   4

#define TFTP_TARGET_HANDLER_CMD_WR_PREP      5
#define TFTP_TARGET_HANDLER_CMD_WR           6
#define TFTP_TARGET_HANDLER_CMD_WR_LAST      7
#define TFTP_TARGET_HANDLER_CMD_WR_CLEANUP   8
#define TFTP_TARGET_HANDLER_CMD_WR_DONE      9
#define TFTP_TARGET_HANDLER_CMD_WR_HDR_CHK  10

/******************************************************************************/

typedef enum
{
  TFTP_RRQ   = 1,
  TFTP_WRQ   = 2,
  TFTP_DATA  = 3,
  TFTP_ACK   = 4,
  TFTP_ERROR = 5,
  TFTP_OACK  = 6
} tftp_opcode;

/******************************************************************************/

typedef enum
{
  TFTP_ERR_NOTDEFINED,
  TFTP_ERR_FILE_NOT_FOUND,
  TFTP_ERR_ACCESS_VIOLATION,
  TFTP_ERR_DISKFULL,
  TFTP_ERR_ILLEGALOP,
  TFTP_ERR_UKNOWN_TRANSFER_ID,
  TFTP_ERR_FILE_ALREADY_EXISTS,
  TFTP_ERR_NO_SUCH_USER,
} tftp_errorcode;

/******************************************************************************/


typedef struct tftp_target_struct tftp_target_type;
typedef struct tftp_server_struct tftp_server_type;

typedef int (*tftp_target_handler_type)(tftp_server_type*, unsigned int, unsigned char*, unsigned int);

struct tftp_target_struct
{
  const char *name;
  tftp_target_handler_type target_handler;
  void         *param_ptr;
};


/******************************************************************************/


struct tftp_server_struct
{
    network_if_type *nw_if;

    unsigned char    udp_tx_frame[UDP_HEADER_LEN + TFTP_DATA_PACKET_LEN];
    udp_header_type *udp_tx_fp;
    unsigned char   *udp_tx_data_ptr;
    unsigned int     udp_tx_len;
    unsigned int     udp_tx_time;        /* time when last packet was sent and timer fot rdy_rsnd_last_ack */

    udp_header_type *udp_rx_fp;
    unsigned char   *udp_rx_data_ptr;
    unsigned int     udp_rx_len;

    unsigned int     busy;
    unsigned int     retries;
    unsigned int     rdy_rsnd_last_ack;  /* ready for resending last ack if client did not get it and resends last data packet */

    tftp_target_type *target_table;
    tftp_target_type *target;

    int              block;              /*  block number */
    unsigned int     fpos;

};

/******************************************************************************/

tftp_opcode tftp_decode_op(unsigned char *ptr);
unsigned short tftp_extract_block(unsigned char *ptr);
void tftp_set_opcode(unsigned char *ptr, tftp_opcode op);
void tftp_set_errorcode(unsigned char *ptr, tftp_errorcode err);
void tftp_set_errormsg(unsigned char *ptr, const char *errormsg);
int  tftp_is_correct_ack(unsigned char *ptr, int block);
void tftp_set_block(unsigned char *ptr, int block);
void tftp_handle_request(tftp_server_type *self);
int  tftp_handle_read(tftp_server_type *self);
int  tftp_handle_write(tftp_server_type *self);
int  handle_tftp(tftp_server_type *self, unsigned char *frame, int frame_len, unsigned int udp_port);
void tftp_send_ack_packet(tftp_server_type *self);
int  tftp_send_error_str(tftp_server_type *self, tftp_errorcode err, const char* error_string);
int  tftp_send_error_message(tftp_server_type *self, tftp_errorcode err);
void tftp_cleanup(tftp_server_type *self);
void tftp_err_cleanup(tftp_server_type *self, unsigned int cmd);
void tftp_send_data_packet(tftp_server_type *self);
int  tftp_prepare_read_write(tftp_server_type *self, tftp_opcode op);
void tftp_check_timeout(tftp_server_type *self);
void tftp_server_construct(tftp_server_type *self, network_if_type *nw_if_i, tftp_target_type *tftp_targets);

/******************************************************************************/

#endif /* __TFTP_SERVER_H__ */

