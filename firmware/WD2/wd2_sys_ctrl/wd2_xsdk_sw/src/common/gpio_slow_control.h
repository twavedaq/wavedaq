/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  14.05.2014 13:22:15
 *
 *  Description :  Software interface for gpio slow control signal inputs and outputs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __GPIO_SLOW_CONTROL_H__
#define __GPIO_SLOW_CONTROL_H__

/* GPIO Slow Control Inputs */
#define  GPIO_SC_IN_INIT            0x00000001
#define  GPIO_SC_IN_TRG_SYNC        0x00000002
#define  GPIO_SC_IN_TRG_SPARE       0x00000004
#define  GPIO_SC_IN_CFM_WP_N        0x00000008
#define  GPIO_SC_IN_CFM_HOLD_N      0x00000010

/* GPIO Slow Control Outputs */
#define  GPIO_LED_SW_STATUS_WDB_ACCESS    0x0001
#define  GPIO_LED_SW_STATUS_SW_UPDATE     0x0002
#define  GPIO_LED_SW_STATUS_FW_UPDATE     0x0004
#define  GPIO_LED_SW_STATUS_BL_LOAD       0x0008
#define  GPIO_LED_SW_STATUS_BL_FAIL       0x0010
#define  GPIO_LED_SW_STATUS_DHCP_REQ      0x0020
#define  GPIO_LED_SW_STATUS_MARKER        0x0040
#define  GPIO_LED_SW_STATUS_ERROR         0x0080

/* GPIO Slow Control Outputs */
#define  GPIO_SC_OUT_ETH_DST_VALID        0x0001
#define  GPIO_SC_OUT_DRS_FSM_SOFT_RST_N   0x0002

/* GPIO Slow Control Outputs */
#define  GPIO_SC_IO_HV_1WIRE_DIR    0x0001
#define  GPIO_SC_IO_HV_1WIRE        0x0002

/* PORT 0 IO: CS_TEMP_O & CS_DAC_O & LE_LMK_O & SDO_I & SDI_O & SCLK_O */
#define  GPIO_SOFT_SPI_TEMP_DAC_LMK   0
/* PORT 1 O : SR_CLK33_O & SR_ST33_O & SR_DI33_O & SR_CLK25_O & SR_ST25_O & SR_DI25_O */
#define  GPIO_SOFT_SHIFT_REGISTER     1
/* PORT 2 I : CFM_HOLD_N_I & CFM_WP_N_I & tr_spare & tr_sync & INIT_I */
#define  GPIO_SLOWCONTROL_INPUTS      2
/* PORT 3 O : led_ctrl_sw_status (C_SW_ERROR_IDX / C_SW_MARKER_IDX / C_SW_BL_FAIL_IDX / C_SW_BL_LOAD_IDX / C_SW_FW_UPDATE_IDX / C_SW_UPDATE_IDX) */
#define  GPIO_LED_SW_STATUS           3
/* PORT 4 O : drs_fsm_soft_rst_n & data_tx_enable */
#define  GPIO_SLOW_CONTROL_OUTPUTS    4
/* PORT 5 IO: HV_1WIRE_IO & HV_1WIRE_DIR_O */
#define  GPIO_HV_BOARD                5
/* Rev == F: PORT 6 IO: ADC_1_SYNC_O & ADC_0_SYNC_O & CSB_1_O & SDIO_1_IO & SCLK_1_O & CSB_0_O & SDIO_0_IO & SCLK_0_O */
/* Rev >= G: PORT IO_PORT6_IO = CSA_0_O & CSB_0_O & SDI_0_I & SDOA_0_O & SDOB_0_O & SCK_0_O & CSA_1_O & CSB_1_O & SDI_1_I & SDOA_1_O & SDOB_1_O & SCK_1_O */
#define  GPIO_SOFT_SPI_ADC            6
/* port 9  Inputs: mscb_i */
#define   GPIO_MSCB                   9

/************************************************************/

void gpio_sc_init(void);

/* Slow control functions */
void gpio_sc_set_sw_status(unsigned int val);
void gpio_sc_clr_sw_status(unsigned int val);
void gpio_sc_flash_sw_status(unsigned int val);
void gpio_sc_reset_sw_status();
/*void gpio_sc_set_master_reset(unsigned int val);*/
void gpio_sc_set_drs_fsm_reset(unsigned int val);
unsigned int gpio_sc_get_drs_fsm_reset();
void gpio_sc_set_eth_dst_valid();
void gpio_sc_disable_event_data_transmission();

#endif /* __GPIO_SLOW_CONTROL_H__ */
