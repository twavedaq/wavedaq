/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __SYSTEM_H__
#define __SYSTEM_H__


/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "xparameters.h"
#include "term_cmd_input.h"
#include "xfs_types.h"
#include "plb_ll_fifo.h"
#include "network_if.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "drv_plb_gpio.h"
#include "drv_plb_adcif_ber_test.h"
#include "plb_spi_slave.h"
#include "drv_spi_flash_w25q64.h"
#include "udp_pack_gen.h"
#include "gmac_lite.h"
#include "dhcp.h"
#include "drv_plb_wd2_reg_bank.h"
#include "plb_idelay_control.h"
#include "drv_drp_pll.h"
#include "plb_serdes_cal_if.h"
#include "plb_serdes_via_eth_testgen.h"
#include "drv_plb_uart.h"
#include "adc_serdes_calibration.h"
#include "side_data.h"
#include "tftp_server.h"
#include "fw_env.h"

/******************************************************************************/
/* macro definitions                                                          */
/******************************************************************************/


/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

/* #define SYSTEM system_ptr            */
/* #define SYSPTR(x) (&(system_ptr->x)) */
#define SYSPTR(x) (&(system_hw.x))

#define SIMULATION   0

/* frame buffer size */
#define GMAC_COM_BUFF_LEN    1600

#define WD2_HEADER_SIZE       64 /* Byte */

#define WD2_DAQ_FREQ    80000000 /* Hz */

#define SW_APP "SW_APP"

#define XPAR_XPS_SYSMON_ADC_PPC440_INCLUDE_INTR 0

#define MAX_HOSTNAME_LENGTH  62

#define MAX_SIDE_DATA_BANKS  5

/* static buffer in SRAM */

#define STATIC_RAM_ADDR_ENV0          (XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR + 0x002FF000)
#define STATIC_RAM_ADDR_ENV1          (XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR + 0x002FE000)

/*#define STATIC_RAM_ADDR_READ_BUFFER   0x02000000 */
/*#define STATIC_RAM_ADDR_WRITE_BUFFER  0x04000000 */

/*#define STATIC_RAM_ADDR_TFTP_BUFFER   0x06000000 */

#define MEM_SIZE_SRAM              0x00400000

/*
Buffer in SRAM
extern unsigned char *STATIC_READ_BUFF  ;
extern unsigned char *STATIC_WRITE_BUFF ;
*/

/*
 Cache
 Enable Cache
 A value of 0x80000000 or 0x40000000 or 0xC0000000 enables the cache for the first 256 MB of memory (0 - 0x7FFFFFF).
 A value of 0x1 or 0x2 or 0x3 enables the cache for the last 256 MB of memory (0xF0000000 - 0xFFFFFFFF).
 if you are migrating software from a PowerPC 405 processor design, be aware that each bit enables 128 MB more of memory for caching.
 e.g. XCache_EnableICache(0x00000001);
 e.g. XCache_EnableDCache(0x00000001);
*/
/* const xfs_u32 I_CACHE_REGION = 0x00800000; */
/* const xfs_u32 D_CACHE_REGION = 0x00800000; */

/*------------------------------------------------------------------------------*/
/* ADC IDELAY CE BITS                                                           */
/*------------------------------------------------------------------------------*/
#define IDELAY_BUSY_TIMEOUT     100000

#define ADC_A_IDELAY_CE_D0      IDELAY_CE0
#define ADC_A_IDELAY_CE_D1      IDELAY_CE1
#define ADC_A_IDELAY_CE_D2      IDELAY_CE2
#define ADC_A_IDELAY_CE_D3      IDELAY_CE3
#define ADC_A_IDELAY_CE_D4      IDELAY_CE4
#define ADC_A_IDELAY_CE_D5      IDELAY_CE5
#define ADC_A_IDELAY_CE_D6      IDELAY_CE6
#define ADC_A_IDELAY_CE_D7      IDELAY_CE7
#define ADC_A_IDELAY_CE_FRAME   IDELAY_CE8
#define ADC_A_IDELAY_CE_CLK     IDELAY_CE9

#define ADC_B_IDELAY_CE_D0      IDELAY_CE10
#define ADC_B_IDELAY_CE_D1      IDELAY_CE11
#define ADC_B_IDELAY_CE_D2      IDELAY_CE12
#define ADC_B_IDELAY_CE_D3      IDELAY_CE13
#define ADC_B_IDELAY_CE_D4      IDELAY_CE14
#define ADC_B_IDELAY_CE_D5      IDELAY_CE15
#define ADC_B_IDELAY_CE_D6      IDELAY_CE16
#define ADC_B_IDELAY_CE_D7      IDELAY_CE17
#define ADC_B_IDELAY_CE_FRAME   IDELAY_CE18
#define ADC_B_IDELAY_CE_CLK     IDELAY_CE19

/*------------------------------------------------------------------------------*/
/* ADC CALIBRATION INTERFACE                                                    */
/*------------------------------------------------------------------------------*/
#define SERDES_CAL_TIMEOUT      100000

/*------------------------------------------------------------------------------*/
/* ENVIRONMENT STORAGE                                                          */
/*------------------------------------------------------------------------------*/
#define ENV_STORAGE_ENTRIES     1

/*------------------------------------------------------------------------------*/
/* System TIME                                                                  */
/*------------------------------------------------------------------------------*/

extern const char system_sw_build_date[];
extern const char system_sw_build_time[];
extern const char *system_month_str[];

/*------------------------------------------------------------------------------*/
/* Global Vars                                                                  */
/*------------------------------------------------------------------------------*/

extern unsigned int do_mscb;


/*------------------------------------------------------------------------------*/
/* System Register Bank                                                         */
/*------------------------------------------------------------------------------*/
const xfs_u32 REG_BANK_CTRL_REGISTERS = 0;
const xfs_u32 REG_BANK_STAT_REGISTERS = 1;

/******************************************************************************/
/* System Environment                                                         */
/******************************************************************************/

/* environment type */
typedef struct
{
  unsigned int   serial_no;

  char           hostname[MAX_HOSTNAME_LENGTH];
  unsigned int   nw_data_src_port;

  /*unsigned char  eth_mac_addr[6]; */
  /*unsigned char  eth_ip_addr[4];  */
  /*unsigned int   eth_dhcp;        */

  side_data_bank_type  side_data[MAX_SIDE_DATA_BANKS];
  /*unsigned int   global_dbg_level; */
} sys_cfg_type;

/******************************************************************************/

typedef struct 
{
  term_cmd_input_type       term_stdin;
  plb_uart_type             MSCB_uart;
  plb_gpio_type             gpio_sys;
  plb_adcif_ber_test_type   adcif_pn_test_a;
  plb_adcif_ber_test_type   adcif_pn_test_b;
  plb_spi_master            spi_hv_master;
  plb_spi_slave             spi_syslink_slave;
  drp_pll_type              adc_pll_control;
  plb_serdes_cal_control    serdes_cal_ctrl[2];
  plb_spi_master            cfg_spi_master;
  plb_spi_salve_settings    cfg_spi_slave_settings;
  spi_flash_w25q64          spi_flash;
  plb_ll_fifo_type          plb_fifo_gmac_0;
  network_if_type           nw_if_gmac_0;
  gmac_lite_type            gmac_lite_0;
  dhcp_type                 dhcp_gmac_0;
  tftp_server_type          tftp_gmac_0;
  network_target_type       nw_data_target;
  udp_pack_gen_type         udp_pack_gen;
  plb_register_bank         reg_bank;
  adc_eye_cal_type          adc_cal_info;
  /*plb_idelay_control        adc_idelay_ctrl; */
  /*serdes_eth_testgen_type   serdes_eth_testgen; */
  
  fw_env_storage_desc_type env_storage_description[ENV_STORAGE_ENTRIES];
  fw_env_type  env;      
  sys_cfg_type cfg;
} system_type;

extern system_type system_hw;

void system_construct(void);
xfs_u32 system_init(void);
unsigned int reg_sw_build_date();
unsigned int reg_sw_build_time();
xfs_u32 init_env(void);
void init_settings(int snr);
void init_env_settings(int snr);
void init_reg_settings();
void print_sys_info(void);

/******************************************************************************/

#endif /* __SYSTEM_H__ */
