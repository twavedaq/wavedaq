/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  15.03.2017 11:12:02
 *
 *  Description :  Support for side data handling in WDB
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __SIDE_DATA_H__
#define __SIDE_DATA_H__

/***************************** Include Files *******************************/

/***************************** type definitions ****************************/

/* side data bank type */
typedef struct
{
  char         id[5];
  unsigned int mem_offset;
  unsigned int mem_length;
  char         trigger_type;
  unsigned int trigger_period;
} side_data_bank_type;

/************************** Function Prototypes ****************************/

int print_side_data_bank_def();
int side_data_timer(unsigned int time);
int side_data_event();
int parse_side_data(const char *str, side_data_bank_type *side_data);

#endif /** __SIDE_DATA_H__ */
