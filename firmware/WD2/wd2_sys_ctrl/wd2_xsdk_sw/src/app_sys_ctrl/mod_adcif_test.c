/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  03.02.2016 08:03:59
 *
 *  Description :  Module to control the pseudo random pattern test
 *                 on the ADC interface.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_plb_adcif_ber_test.h"
#include "wd2_config.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "system.h"
#include "dbg.h"
#include <stdlib.h>

/************************************************************/

  int pn_test_start(int argc, char **argv)
  {
    CMD_HELP("",
             "start the test",
             "Starts the ADC pseudo random pattern test."  
            );

    if(DBG_INFO) xfs_printf("\r\nStarting pseudo random pattern test\r\n");
    plb_adcif_ber_test_start(SYSPTR(adcif_pn_test_a));
    plb_adcif_ber_test_start(SYSPTR(adcif_pn_test_b));

    return 0;
  }

/************************************************************/

  int pn_test_stop(int argc, char **argv)
  {
    CMD_HELP("",
             "stop the test",
             "tops the ADC pseudo random pattern test."  
            );


    if(DBG_INFO) xfs_printf("\r\nStopping pseudo random pattern test\r\n");
    plb_adcif_ber_test_stop(SYSPTR(adcif_pn_test_a));
    plb_adcif_ber_test_stop(SYSPTR(adcif_pn_test_b));

    return 0;
  }

/************************************************************/

  int pn_test_reset(int argc, char **argv)
  {
    CMD_HELP("",
             "reset counters",
             "Resets the ADC pseudo random pattern test counters.\r\n"  
            );

    if(DBG_INFO) xfs_printf("\r\nResetting pseudo random pattern test registers\r\n");
    plb_adcif_ber_test_reset(SYSPTR(adcif_pn_test_a));
    plb_adcif_ber_test_reset(SYSPTR(adcif_pn_test_b));

    return 0;
  }

/************************************************************/

  int pn_test_inject_error(int argc, char **argv)
  {
    CMD_HELP("",
             "inject a single error",
             "Injects a bit error in the ADC pseudo random pattern test."
            );

    if(DBG_INFO) xfs_printf("\r\nInjecting error in pseudo random pattern test\r\n");
    plb_adcif_ber_test_inject_error(SYSPTR(adcif_pn_test_a));
    plb_adcif_ber_test_inject_error(SYSPTR(adcif_pn_test_b));

    return 0;
  }

/************************************************************/

  int pn_test_select_pattern(int argc, char **argv)
  {
    unsigned int pattern;

    CMD_HELP("<pattern>",
             "select the pseudo random pattern",
             "Selects the pseudo random pattern for the test.\r\n"  
             "  <pattern> : 0 = PN9 pattern, 1 = PN23\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    pattern = (unsigned int)(xfs_atoi(argv[1]));

    if(pattern)
    {
      if(DBG_INFO) xfs_printf("\r\nSetting test pattern to PN23\r\n");
    }
    else
    {
      if(DBG_INFO) xfs_printf("\r\nSetting test pattern to PN9\r\n");
    }
    plb_adcif_ber_test_set_pn_pattern(SYSPTR(adcif_pn_test_a), pattern);
    plb_adcif_ber_test_set_pn_pattern(SYSPTR(adcif_pn_test_b), pattern);


    return 0;
  }

/************************************************************/

  int pn_test_print_status(int argc, char **argv)
  {
    unsigned int data[2];

    CMD_HELP("",
             "print the counter values",
             "Description: Prints the status and counter values of the ADC\r\n"  
             "interface pseudo random pattern test.\r\n"  
            );


    if(DBG_INFO) xfs_printf("\r\nADC Interface Pseudo Random Pattern Test Status:\r\n\r\n");
    
    xfs_printf("Test is currently ");
    if(plb_adcif_ber_test_is_running(SYSPTR(adcif_pn_test_a)))
    {
      xfs_printf("running\r\n\r\n");
    }
    else
    {
      xfs_printf("stopped\r\n\r\n");
    }

    for(unsigned int i=0;i<8;i++)
    {
      xfs_printf("ADC A channel %d ", i);
      plb_adcif_ber_test_get_errors(SYSPTR(adcif_pn_test_a), i, data);
      xfs_printf("errors: 0x%08X_%08X", data[1], data[0]);
      plb_adcif_ber_test_get_samples(SYSPTR(adcif_pn_test_a), i, data);
      xfs_printf("   samples: 0x%08X_%08X\r\n", data[1], data[0]);
    }
    xfs_printf("\r\n");
    for(unsigned int i=0;i<8;i++)
    {
      xfs_printf("ADC B channel %d ", i);
      plb_adcif_ber_test_get_errors(SYSPTR(adcif_pn_test_b), i, data);
      xfs_printf("errors: 0x%08X_%08X", data[1], data[0]);
      plb_adcif_ber_test_get_samples(SYSPTR(adcif_pn_test_b), i, data);
      xfs_printf("   samples: 0x%08X_%08X\r\n", data[1], data[0]);
    }
    xfs_printf("\r\n");

    return 0;
  }

/************************************************************/

  int module_pn_help(int argc, char **argv)
  {
    CMD_HELP("",
             "ADC pseudo random pattern test module",
             "Controls the ADC interfac pseudo random pattern test.\r\n"
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type pn_test_cmd_table[] =
  {
    {0, "pn", module_pn_help},
    {0, "pnstart", pn_test_start},
    {0, "pnstop", pn_test_stop},
    {0, "pnrst", pn_test_reset},
    {0, "pninj", pn_test_inject_error},
    {0, "pnpat", pn_test_select_pattern},
    {0, "pnstat", pn_test_print_status},
    {0, NULL, NULL}
  };

/************************************************************/
