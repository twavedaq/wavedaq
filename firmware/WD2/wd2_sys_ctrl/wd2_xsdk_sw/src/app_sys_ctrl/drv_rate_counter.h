/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  26.03.2018 10:39:17
 *
 *  Description :  Driver for trigger rate counter(scaler).
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_RATE_COUNTER_H__
#define __DRV_RATE_COUNTER_H__

/************************************************************/

/* Rate Counter functions */
void rate_counter_init(unsigned int reg_base_address, unsigned int mem_base_address);
void rate_counter_update_rates();
void print_counter_values();

#endif /* __DRV_RATE_COUNTER_H__ */
