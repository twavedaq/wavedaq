/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  14.05.2014 12:47:20
 *
 *  Description :  Module for controling the onboard led.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "xtime.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>

/************************************************************/

  int led_test(int argc, char **argv)
  {
    unsigned int delay = 4e6;

    CMD_HELP("",
             "set RGB LED colors one after another",
             "Applies all possible sw status settings to the onboard LED."
            );


    gpio_sc_reset_sw_status();
    xfs_printf("LED status \"SW Update\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_SW_UPDATE);
    xfs_printf("LED status \"FW Update\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_FW_UPDATE);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_FW_UPDATE);
    xfs_printf("LED status \"Load SW\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_BL_LOAD);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_BL_LOAD);
    xfs_printf("LED status \"Bootloader Failure\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_BL_FAIL);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_BL_FAIL);
    xfs_printf("LED status \"Mark Board\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_MARKER);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_MARKER);
    xfs_printf("LED status \"DHCP Request\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_DHCP_REQ);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_DHCP_REQ);
    xfs_printf("LED status \"SW Error\"\r\n");
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_ERROR);
    usleep(delay);
    gpio_sc_clr_sw_status(GPIO_LED_SW_STATUS_ERROR);
    xfs_printf("LED status \"Normal Operation\"\r\n");
    gpio_sc_reset_sw_status();

    return 0;
  }

/************************************************************/

  int module_led_help(int argc, char **argv)
  {
    CMD_HELP("",
             "LED Control Module",
             "Can be used to set the RGB LED"
            );

    return 0;
  }


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type led_cmd_table[] =
  {
    {0, "led", module_led_help},
    {0, "ledtest", led_test},
    {0, NULL, NULL}
  };

/************************************************************/
