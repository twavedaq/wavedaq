/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  22.09.2014 09:03:44
 *
 *  Description :  Controller for the IDELAY of the Spartan6 FPGA.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include "plb_idelay_control.h"
#include "xtime.h"

/************************** Function Definitions ***************************/

void plb_idelay_init(plb_idelay_control *instance_ptr, unsigned int base_addr, unsigned int timeout)
{
  instance_ptr->base_address = base_addr;
  instance_ptr->busy_timeout = timeout;
  PLB_IDELAY_CONTROL_mWriteReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS, 0);
}

/***************************************************************************/

int plb_idelay_inc(plb_idelay_control *instance_ptr, int ce)
{
  unsigned int timeout = instance_ptr->busy_timeout;

  PLB_IDELAY_CONTROL_mWriteReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS, IDELAY_INC | ce);
  while( (PLB_IDELAY_CONTROL_mReadReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS)&IDELAY_BUSY) && timeout )
  {
    usleep(1);
    timeout--;
  }

  if(timeout)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int plb_idelay_dec(plb_idelay_control *instance_ptr, int ce)
{
  unsigned int timeout = instance_ptr->busy_timeout;

  PLB_IDELAY_CONTROL_mWriteReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS, ce);
  while( (PLB_IDELAY_CONTROL_mReadReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS)&IDELAY_BUSY) && timeout )
  {
    usleep(1);
    timeout--;
  }

  if(timeout)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int plb_idelay_cal(plb_idelay_control *instance_ptr, int ce)
{
  unsigned int timeout = instance_ptr->busy_timeout;

  PLB_IDELAY_CONTROL_mWriteReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS, IDELAY_CAL | ce);
  while( (PLB_IDELAY_CONTROL_mReadReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS)&IDELAY_BUSY) && timeout )
  {
    usleep(50);
    timeout--;
  }

  if(timeout)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int plb_idelay_reset(plb_idelay_control *instance_ptr, int ce)
{
  unsigned int timeout = instance_ptr->busy_timeout;

  PLB_IDELAY_CONTROL_mWriteReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS, IDELAY_RST | ce);
  while( (PLB_IDELAY_CONTROL_mReadReg(instance_ptr->base_address, PLB_IDELAY_CONTROL_SLV_REG0_OFS)&IDELAY_BUSY) && timeout )
  {
    usleep(100);
    timeout--;
  }

  if(timeout)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/
/***************************************************************************/
