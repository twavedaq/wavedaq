/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  22.09.2014 13:22:26
 *
 *  Description :  Interface to read iserdes data from ADCs for calibration.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include "plb_serdes_cal_if.h"
#include "xfs_printf.h"
#include "dbg.h"
#include "xtime.h"

/************************** Function Definitions ***************************/

void plb_serdes_cal_init(plb_serdes_cal_control *instance_ptr, unsigned int base_addr, unsigned int timeout)
{
  instance_ptr->base_address   = base_addr;
  instance_ptr->update_timeout = timeout;
  PLB_SERDES_CAL_IF_mWriteReg(instance_ptr->base_address, PLB_SERDES_CAL_IF_CTRL_OFFSET, 0);
}

/***************************************************************************/

void plb_serdes_cal_trigger_read(plb_serdes_cal_control *instance_ptr, int no_words)
{
  PLB_SERDES_CAL_IF_mWriteReg(instance_ptr->base_address, PLB_SERDES_CAL_IF_CTRL_OFFSET, no_words & PLB_SERDES_CAL_IF_CTRL_COUNT_MASK);
}

/***************************************************************************/

int plb_serdes_cal_update(plb_serdes_cal_control *instance_ptr)
{
  if( plb_serdes_cal_data_is_available(instance_ptr) )
  {
    PLB_SERDES_CAL_IF_mWriteReg(instance_ptr->base_address, PLB_SERDES_CAL_IF_CTRL_OFFSET, PLB_SERDES_CAL_IF_CTRL_UPDATE_BIT);
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int plb_serdes_cal_data_is_available(plb_serdes_cal_control *instance_ptr)
{
  if( PLB_SERDES_CAL_IF_mReadReg(instance_ptr->base_address, PLB_SERDES_CAL_IF_STATUS_OFFSET)&PLB_SERDES_CAL_IF_STATUS_IN_EMPTY_BIT )
  {
    return 0;
  }
  else
  {
    return 1;
  }
}

/***************************************************************************/

int plb_serdes_cal_get(plb_serdes_cal_control *instance_ptr, int reg_number)
{
  int offset;

  offset = 4*reg_number + 8;
  return PLB_SERDES_CAL_IF_mReadReg(instance_ptr->base_address, offset);
}

/***************************************************************************/

int plb_serdes_cal_status(plb_serdes_cal_control *instance_ptr)
{
  return PLB_SERDES_CAL_IF_mReadReg(instance_ptr->base_address, PLB_SERDES_CAL_IF_STATUS_OFFSET);
}

/***************************************************************************/
/***************************************************************************/
