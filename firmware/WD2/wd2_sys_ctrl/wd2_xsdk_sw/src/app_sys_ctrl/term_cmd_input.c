/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Capturing commands from terminal.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/


#include "cmd_processor.h"
#include "xuartlite_l.h"
#include "system.h"
#include "term_cmd_input.h"

#define DBG_HIST 0

/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

void term_cmd_input_init(term_cmd_input_type* self, unsigned int baseaddr)
{
  self->baseaddr     = baseaddr;
  self->prompt       = 1;
  self->refresh      = 0;
  self->esc          = 0;
  self->esc_seq_ptr  = 0;
  self->blen         = 0;
  self->bptr         = 0;
  self->hist_wr_ptr  = 0;
  self->hist_rd_ptr  = 0;
  self->hist_entries = 0;
}

/******************************************************************************/

void term_cmd_input_chk_cmd(term_cmd_input_type* self)
{
  int len;

  len = term_cmd_input_get_cmd(self);
  if (len>0)
  {
    self->buffer[len]=0;
    /* xfs_terminal_printf("CMD received: %s\r\n",self->buffer); */
    cmd_process(self->buffer, len);
  }
}


/******************************************************************************/

char getkey(term_cmd_input_type* self)
{
  char c;

  if (!XUartLite_IsReceiveEmpty(self->baseaddr))
  {
    c = (xfs_u8)XUartLite_ReadReg(self->baseaddr, XUL_RX_FIFO_OFFSET);
  }
  else
  {
    c = 0;
  }

  return c;
}


/******************************************************************************/

#if DBG_HIST
void dbg_hist(term_cmd_input_type* self)
{
  int i;
  xfs_terminal_printf("self->blen  = %d\r\n",       self->blen );
  xfs_terminal_printf("self->bptr  = %d\r\n",       self->bptr );

  xfs_terminal_printf("\r\n" "self->hist_wr_ptr = %d\r\n",  self->hist_wr_ptr);
  xfs_terminal_printf("self->hist_rd_ptr = %d\r\n",       self->hist_rd_ptr);
  xfs_terminal_printf("self->hist_entries = %d\r\n",      self->hist_entries);
  for (i=0; i<self->hist_entries; i++)
  {
    xfs_terminal_printf("self->history[%d] = %s\r\n",  i, self->history[i]);
  }
}
#endif


/******************************************************************************/

void term_histo_select(term_cmd_input_type* self, int direction)
{
  int next_rd_ptr;

  if (direction < 0)
  {
    /* previous command */
    next_rd_ptr = self->hist_rd_ptr - 1;
    if (next_rd_ptr <  0)                 next_rd_ptr = self->hist_entries;
    if (next_rd_ptr >=  MAX_HISTORY_SIZE) next_rd_ptr = MAX_HISTORY_SIZE-1;
    if (next_rd_ptr == self->hist_wr_ptr) return;  /* already on oldest entry */
  }
  else if (self->hist_rd_ptr != self->hist_wr_ptr) /* if not current command  */
  {
    /* next command */
    next_rd_ptr = self->hist_rd_ptr + 1;
    if (next_rd_ptr > self->hist_entries) next_rd_ptr = 0;
    if (next_rd_ptr >=  MAX_HISTORY_SIZE) next_rd_ptr = 0;
  }
  else
  {
    return;
  }

  /* save current command to history */
  self->buffer[self->blen] = 0;
  strncpy(self->history[self->hist_rd_ptr], self->buffer, MAX_CMD_LEN);

  /* delete the terminal output */
  term_cursor_left(self->bptr);   /* move cursor left to start */
  term_erase_to_end();

  /* use next read pointer */
  self->hist_rd_ptr = next_rd_ptr;

  /* copy the history command to the buffer */
  strncpy(self->buffer, self->history[self->hist_rd_ptr], MAX_CMD_LEN);
  self->blen = strnlen(self->buffer, MAX_CMD_LEN);
  self->bptr = self->blen;

#if DBG_HIST
  dbg_hist(self);
#endif

  /* print the buffer */
  xfs_terminal_printf(self->buffer);
}


/******************************************************************************/

void term_histo_update(term_cmd_input_type* self)
{
  int matches_latest_hist_entry = 0;

  if(self->hist_entries > 0)
  {
    int tmp_wr_ptr = self->hist_wr_ptr - 1;
    if (tmp_wr_ptr <  0) tmp_wr_ptr = self->hist_entries-1;
    matches_latest_hist_entry = fstrcmp(self->history[tmp_wr_ptr], self->buffer);
  }

  if( (self->blen > 0) && !matches_latest_hist_entry )
  {
    /* if there is a command and it's not the same as the latest entry, add it to the history */
    strncpy(self->history[self->hist_wr_ptr], self->buffer, MAX_CMD_LEN);
    if (self->hist_entries < MAX_HISTORY_SIZE) ++self->hist_entries;
    if ((++self->hist_wr_ptr) >= MAX_HISTORY_SIZE) self->hist_wr_ptr = 0;
  }
  self->hist_rd_ptr = self->hist_wr_ptr;

#if DBG_HIST
  dbg_hist(self);
#endif

}


/******************************************************************************/

int is_word_char(char c)
{
  if (c >= '0' && c <= '9') return 1;
  if (c >= 'A' && c <= 'Z') return 1;
  if (c >= 'a' && c <= 'z') return 1;
  return 0;
}


/******************************************************************************/

int skip_char_l(char* buf, int max, int pos, int non)
{
  if (pos > 0) pos--;
  while ((pos > 0) && (pos <= max) && (is_word_char(buf[pos])^non) ) pos--;
  if ((pos > 0) && (pos < max)) pos++;
  return pos;
}


/******************************************************************************/

int skip_char_r(char* buf, int max, int pos, int non)
{
  while ((pos >= 0) && (pos < max) && (is_word_char(buf[pos])^non) ) pos++;
  return pos;
}


/******************************************************************************/

void term_prompt(term_cmd_input_type* self, int action)
{
  if (action == TERM_CLEAR_LINE)
  {
    term_erase_line();
    term_cursor_return();
  }
  else if (action == TERM_CLEAR_SCREEN)
  {
    term_erase_screen();
    term_cursor_home();
  }
  else if (action == TERM_NEW_LINE)
  {
    term_cursor_newline();
  }

  xfs_terminal_printf("%s > %s", SYSPTR(cfg)->hostname, self->buffer);
  term_cursor_left(self->blen - self->bptr);
}


/******************************************************************************/

int term_cmd_input_get_cmd(term_cmd_input_type* self)
{
  int len = -1;
  int c;
  unsigned int n;

  if (self->prompt)
  {
    self->buffer[0] = 0;
    term_prompt(self, 0);
    self->prompt = 0;
    /* c = 0x03; */
    /* sspi_transmit(SYSPTR(spi_syslink_slave), (unsigned char*)(&c), 1); */
    /* xfs_local_printf("%s > ",env_data.name); */
    self->prompt=0;
    self->refresh = 0;
  }
  else if (self->refresh)
  {
    term_prompt(self, TERM_NEW_LINE);
    self->refresh = 0;
  }

  if (!XUartLite_IsReceiveEmpty(self->baseaddr))
  {
    c = (xfs_u8)XUartLite_ReadReg(self->baseaddr, XUL_RX_FIFO_OFFSET);
    /* xfs_terminal_printf("c =   %c    (%d)   self->blen=%d \r\n",c,c,self->blen); */

    if (c==0x01b)  /* escape */
    {
      self->esc = 200;
      self->esc_seq_ptr = 0;
    }
    else if (self->esc)
    {
      if (self->esc_seq_ptr < MAX_ESC_SEQ)
      {
        self->esc_seq_buf[self->esc_seq_ptr++] = c;
        self->esc_seq_buf[self->esc_seq_ptr  ] = 0;
      }

      /* End of Escape Sequence */
      if ( ((c >= 'A') && (c <='H')) || ((c >= 'P') && (c <='Z')) || (c=='~') || ((c >= 'a') && (c <='z')) )
      {
        /* xfs_terminal_printf("Escape Sequence : %s\r\n", self->esc_seq_buf); */
        self->esc = 0;

        if (fstrcmp(self->esc_seq_buf, ESC_SEQ_UP))                    /* Arrow-Up */
        {
          term_histo_select(self, -1);
        }
        else if (fstrcmp(self->esc_seq_buf, ESC_SEQ_DOWN))             /* Arrow-Down */
        {
          term_histo_select(self, +1);
        }
        else if (fstrcmp(self->esc_seq_buf, ESC_SEQ_RIGHT))            /* Arrow-Right */
        {
          if (self->bptr < self->blen)
          {
            term_cursor_right(1);
            self->bptr++;
          }
        }
        else if (fstrcmp(self->esc_seq_buf, ESC_SEQ_LEFT))             /* Arrow-Left */
        {
          if (self->bptr > 0)
          {
            term_cursor_left(1);
            self->bptr--;
          }
        }
        else if (fstrcmp(self->esc_seq_buf, ESC_SEQ_CTRL_RIGHT)   ||
                 fstrcmp(self->esc_seq_buf, ESC_SEQ_CTRL_RIGHT_2)  )   /* Ctrl-Arrow-Right */
        {
          n = skip_char_r(self->buffer, self->blen, self->bptr, 1);
          n = skip_char_r(self->buffer, self->blen, n,          0);
          term_cursor_right(n - self->bptr);
          self->bptr = n;
        }
        else if (fstrcmp(self->esc_seq_buf, ESC_SEQ_CTRL_LEFT  ) ||
                 fstrcmp(self->esc_seq_buf, ESC_SEQ_CTRL_LEFT_2)  )    /* Ctrl-Arrow-Left */
        {
          n = skip_char_l(self->buffer, self->blen, self->bptr, 1);
          n = skip_char_l(self->buffer, self->blen, n,          0);
          term_cursor_left(self->bptr - n);
          self->bptr = n;
        }

        else if ( fstrcmp(self->esc_seq_buf, ESC_SEQ_HOME)   ||
                  fstrcmp(self->esc_seq_buf, ESC_SEQ_HOME_2) ||
                  fstrcmp(self->esc_seq_buf, ESC_SEQ_HOME_3)  )        /* Home */
        {
          term_cursor_left(self->bptr);
          self->bptr= 0;
        }
        else if ( fstrcmp(self->esc_seq_buf, ESC_SEQ_END)   ||
                  fstrcmp(self->esc_seq_buf, ESC_SEQ_END_2) ||
                  fstrcmp(self->esc_seq_buf, ESC_SEQ_END_3)  )         /* End */
        {
          term_cursor_right(self->blen - self->bptr);
          self->bptr = self->blen;
        }
        else if (fstrcmp(self->esc_seq_buf, ESC_SEQ_DEL))              /* Delete */
        {
          if(self->bptr < self->blen)
          {
            for(n=self->bptr; n < self->blen; n++) self->buffer[n] = self->buffer[n+1];
            --self->blen;
            xfs_terminal_printf(&self->buffer[self->bptr]);
            term_erase_to_end();
            term_cursor_left(self->blen - self->bptr);
          }
        }
      }
    }
    else if ((c==0x08) || (c==0x7f))                                   /* Backspace */
    {
      if(self->bptr > 0)
      {
        for(n=self->bptr; n <= self->blen; n++) self->buffer[n-1] = self->buffer[n];
        --self->blen;
        --self->bptr;
        term_cursor_left(1);
        xfs_terminal_printf(&self->buffer[self->bptr]);
        term_erase_to_end();
        term_cursor_left(self->blen - self->bptr);
      }
    }
    else if ((c==0x0a) || (c==0x0d))                                   /* Enter */
    {
      xfs_terminal_printf("\r\n");

      term_histo_update(self);

      len = self->blen;
      self->blen = 0;
      self->bptr = 0;
      self->prompt = 1;
    }
    else if (c==0x0b)                                                  /* Ctrl-K : clear to end of line */
    {
      self->buffer[self->bptr] = 0;
      self->blen = self->bptr;
      term_erase_to_end();
    }
    else if (c==0x0c)                                                  /* Ctrl-L : clear screen and refresh prompt */
    {
      term_erase_screen();
      term_cursor_home();
      term_prompt(self, TERM_CLEAR_SCREEN);
    }
    else if (c==0x07)                                                  /* Ctrl-G : clear line and refresh prompt */
    {
      term_erase_line();
      term_cursor_return();
      term_prompt(self, TERM_CLEAR_LINE);
    }

    else if (c==0x15)                                                  /* Ctrl-U : clear to start of line */
    {
      if(self->bptr > 0)
      {
        for(n=self->bptr; n <= self->blen; n++) self->buffer[n-self->bptr] = self->buffer[n];
        term_cursor_left(self->bptr);
        term_erase_to_end();
        xfs_terminal_printf(self->buffer);
        self->blen -= self->bptr;
        self->bptr = 0;
        term_cursor_left(self->blen);
      }
    }
    else if  ((c >= 32) && (c <= 126))                                 /* writable character */
    {
      if (self->blen < MAX_CMD_LEN)
      {
        for(n=self->blen+1; n > self->bptr; n--) self->buffer[n] = self->buffer[n-1];
        self->buffer[self->bptr] = c;
        self->buffer[++self->blen] = 0;
        xfs_terminal_printf(&self->buffer[self->bptr++]);
        term_cursor_left(self->blen - self->bptr);
      }
    }
  }
  else if (self->esc)
  {
    self->esc--;
    if (!self->esc) return -1;    /* abort ESC Sequence after timeout */
    usleep(10);
  }

  return len;
}


/******************************************************************************/

