/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  24.02.2015 14:30:48
 *
 *  Description :  Module for FW environment access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "fw_env.h"
#include "system.h"
#include "xfs_printf.h"
#include "dbg.h"
#include "cmd_processor.h"

/************************************************************/

  int fw_env_printenv(int argc, char **argv)
  {
    CMD_HELP("<option> [<name>]",
             "print environment variables",
             "prints the environment variables."  
             "  <option> : -q prints the environment variable\r\n"
             "                values in single quotes.\r\n"
             "             -c prints setenv templates to modify\r\n"
             "                environment variables using copy/paste.\r\n"
             "             -n <name> prints the environment variable with\r\n"
             "                the name specified.\r\n"
             "  <name>   : environment variable name\r\n"
            );


    fw_printenv(SYSPTR(env), argc, argv);

    return 1;
  }

/************************************************************/

  int fw_env_setenv(int argc, char **argv)
  {
    CMD_HELP("<name> <value>",
             "set environment variables",
             "set environment variables"  
             "  <name>  : name of the variable to set/clear\r\n"
             "  <value> : value of the variable to set. If left\r\n"
             "            blank, the variable with the corresponding\r\n"
             "            name is cleared.\r\n"
            );




    fw_setenv(SYSPTR(env), argc, argv);

    return 1;
  }

/************************************************************/

  int fw_env_saveenv(int argc, char **argv)
  {
    CMD_HELP("",
             "store environment variables",
             "stores the current set of environment variables in the SPI flash."  
            );

    fw_env_save(SYSPTR(env));

    return 1;
  }

/************************************************************/

  int fw_env_reloadenv(int argc, char **argv)
  {
    CMD_HELP("",
             "reload environment variables",
             "reloads the environment variables from the SPI flash."  
            );

    fw_env_load(SYSPTR(env));
    
    return 1;
  }

/************************************************************/

  int module_env_help(int argc, char **argv)
  {
    CMD_HELP("",
             "FW Environment module",
             "Allows to print, set and modify environment variables"  
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type fw_env_cmd_table[] =
  {
    {0, "env", module_env_help},
    {0, "printenv", fw_env_printenv},
    {0, "setenv", fw_env_setenv},
    {0, "saveenv", fw_env_saveenv},
    {0, "reloadenv", fw_env_reloadenv},
    {0, NULL, NULL}
  };

/************************************************************/
