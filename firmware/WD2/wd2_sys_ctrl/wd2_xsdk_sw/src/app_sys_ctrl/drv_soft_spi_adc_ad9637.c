/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.09.2014 08:57:53
 *
 *  Description :  Software controlled SPI controller for communication with
 *                 ADCs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "xtime.h"
#include "drv_soft_spi_adc_ad9637.h"
#include "dbg.h"

/******************************************************************************/
/* Global Variables                                                           */
/******************************************************************************/
static unsigned int gpio_sclk  = 0;
static unsigned int gpio_slave = 0;
static unsigned int gpio_sdio  = 0;
/******************************************************************************/


void ad9637_soft_spi_adc_init(void)
{
  plb_gpio_write(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, SPI_AD9637_CSB_B_O | SPI_AD9637_CSB_A_O);
  plb_gpio_tristate_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, SPI_AD9637_CSB_B_O | SPI_AD9637_SDIO_B_IO | SPI_AD9637_SCLK_B_O |
                                          SPI_AD9637_CSB_A_O | SPI_AD9637_SDIO_A_IO | SPI_AD9637_SCLK_A_O |
                                          CTRL_AD9637_SYNC_B_O | CTRL_AD9637_SYNC_A_O);
  /*plb_gpio_tristate_set(GPIO_SOFT_SPI_ADC, ); */
  ad9637_sync();
}

/******************************************************************************/

static void soft_spi_adc_sck(unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sclk );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sclk );
  }
}

/******************************************************************************/

static void soft_spi_adc_cs_n(unsigned char slave, unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, slave );
  }
  else
  {
    gpio_sclk  = 0;
    gpio_slave = 0;
    gpio_sdio  = 0;
    if(slave & SPI_AD9637_CSB_A_O)
    {
      gpio_sclk  |= SPI_AD9637_SCLK_A_O;
      gpio_slave |= SPI_AD9637_CSB_A_O;
      gpio_sdio  |= SPI_AD9637_SDIO_A_IO;
    }
    if(slave & SPI_AD9637_CSB_B_O)
    {
      gpio_sclk  |= SPI_AD9637_SCLK_B_O;
      gpio_slave |= SPI_AD9637_CSB_B_O;
      gpio_sdio  |= SPI_AD9637_SDIO_B_IO;
    }

    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, slave );
  }
}

/******************************************************************************/

static void soft_spi_adc_sdo(unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sdio );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sdio );
  }
}

/******************************************************************************/

static unsigned int soft_spi_adc_sdi()
{
  if (plb_gpio_get(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC) & gpio_sdio)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

static void soft_spi_adc_wait(void)
{
  usleep(1);
}

/******************************************************************************/

static void soft_spi_adc_set_data_direction(unsigned int val)
{
  if(val == SPI_DATA_DIR_OUT)
  {
    plb_gpio_tristate_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sdio);
  }
  else
  {
    plb_gpio_tristate_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sdio);
  }
}

/******************************************************************************/

static void soft_spi_adc_transmit(unsigned char slave, unsigned char* tx_buffer, unsigned int nr_of_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_adc_set_data_direction(SPI_DATA_DIR_OUT);

  soft_spi_adc_cs_n(slave, 0);
  usleep(1);

  for (i=0; i<nr_of_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_adc_sck(0);
      soft_spi_adc_sdo(value & 0x80);
      soft_spi_adc_wait();
      soft_spi_adc_sck(1);
      soft_spi_adc_wait();
      value <<= 1;
    }
  }
  soft_spi_adc_sck(0);

  soft_spi_adc_wait();
  soft_spi_adc_cs_n(slave, 1);
  usleep(1);
  /*soft_spi_adc_sck(1); */
}

/******************************************************************************/

static void soft_spi_adc_transceive(unsigned char slave, unsigned char* tx_buffer, unsigned char* rx_buffer, unsigned int nr_of_tx_bytes, unsigned int nr_of_rx_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_adc_cs_n(slave, 0);
  usleep(1);

  /* transmit */
  soft_spi_adc_set_data_direction(SPI_DATA_DIR_OUT);

  for (i=0; i<nr_of_tx_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_adc_sck(0);
      soft_spi_adc_sdo(value & 0x80);
      soft_spi_adc_wait();
      soft_spi_adc_sck(1);
      soft_spi_adc_wait();
      value <<= 1;
    }
    soft_spi_adc_sck(0);
  }

  /* receive */
  usleep(1);
  soft_spi_adc_set_data_direction(SPI_DATA_DIR_IN);
  usleep(1);

  for (i=0; i<nr_of_rx_bytes; i++)
  {
    value = 0;

    for (j=0; j<8; j++)
    {
      value <<= 1;
      soft_spi_adc_sck(0);
      soft_spi_adc_wait();
      /* Sample data prior to edge due to tristate timing of */
      /* the SDIO line at the end of the transmission !!!    */
      value |= soft_spi_adc_sdi();
      soft_spi_adc_sck(1);
      soft_spi_adc_wait();
    }
    soft_spi_adc_sck(0);

    rx_buffer[i] = value;
  }

  soft_spi_adc_wait();
  soft_spi_adc_cs_n(slave, 1);
  /* reset data direction after deselecting slave since slave only stops */
  /* driving the line when deselcted.                                    */
  soft_spi_adc_wait();
  soft_spi_adc_set_data_direction(SPI_DATA_DIR_OUT);
  usleep(1);
  /*soft_spi_adc_sck(1); */
}

/******************************************************************************/
/******************************************************************************/

void ad9637_write_single_cfg(unsigned int slave, unsigned int address, unsigned char reg_value)
{
  unsigned char data[3];

  if(address!=AD9637_OMIT_TXRX_ADDR)
  {
    data[0] = AD9637_CMD_WRITE | AD9637_CMD_CFG_1BYTE | ( (address>>8) & 0x1F );
    data[1] = (unsigned char)(address & 0xFF);
    data[2] = reg_value;

    /*if(DBG_INFO) xfs_printf("AD9637 Register Write Single [0x%03X]: 0x%02X\r\n", address, reg_value); */
    soft_spi_adc_transmit(slave, data, 3);
  }
}

/******************************************************************************/

unsigned int ad9637_read_single_cfg(unsigned int slave, unsigned int address)
{
  unsigned char tx_data[2];
  unsigned char rx_data;

  if(address!=AD9637_OMIT_TXRX_ADDR)
  {
    tx_data[0] = AD9637_CMD_READ | AD9637_CMD_CFG_1BYTE | ( (address>>8) & 0x1F );
    tx_data[1] = (unsigned char)(address & 0xFF);

    soft_spi_adc_transceive(slave, tx_data, &rx_data, 2, 1);
    /*if(DBG_INFO) xfs_printf("AD9637 Register Read Single [0x%03X]: 0x%02X\r\n", address, rx_data); */
  }
  else
  {
    rx_data = 0;
  }

  return (unsigned int)rx_data;
}

/******************************************************************************/
/******************************************************************************/

void ad9637_adc_0_wr_cfg(unsigned int address, unsigned char reg_value)
{
  /*if(DBG_INFO) xfs_printf("AD9637 A Register Write      [0x%03X]: 0x%02X\r\n", address, reg_value); */
  ad9637_write_single_cfg(SPI_AD9637_CSB_A_O, address, reg_value);
}

/******************************************************************************/

void ad9637_adc_1_wr_cfg(unsigned int address, unsigned char reg_value)
{
  /*if(DBG_INFO) xfs_printf("AD9637 B Register Write      [0x%03X]: 0x%02X\r\n", address, reg_value); */
  ad9637_write_single_cfg(SPI_AD9637_CSB_B_O, address, reg_value);
}

/******************************************************************************/

void ad9637_adc_01_wr_cfg(unsigned int address, unsigned char reg_value)
{
  /*if(DBG_INFO) xfs_printf("AD9637 A&B Register Write    [0x%03X]: 0x%02X\r\n", address, reg_value); */
  ad9637_write_single_cfg(SPI_AD9637_CSB_A_O | SPI_AD9637_CSB_B_O, address, reg_value);
}

/******************************************************************************/

unsigned int ad9637_adc_0_rd_cfg(unsigned int address)
{
  unsigned int reg_value;

  reg_value = ad9637_read_single_cfg(SPI_AD9637_CSB_A_O, address);
  /*if(DBG_INFO) xfs_printf("AD9637 A Register Read      [0x%03X]: 0x%02X\r\n", address, reg_value); */

  return reg_value;
}

/******************************************************************************/

unsigned int ad9637_adc_1_rd_cfg(unsigned int address)
{
  unsigned int reg_value;

  reg_value = ad9637_read_single_cfg(SPI_AD9637_CSB_B_O, address);
  /*if(DBG_INFO) xfs_printf("AD9637 B Register Read      [0x%03X]: 0x%02X\r\n", address, reg_value); */

  return reg_value;
}

/******************************************************************************/

void ad9637_sync()
{
  plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, CTRL_AD9637_SYNC_A_O | CTRL_AD9637_SYNC_B_O );
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, CTRL_AD9637_SYNC_A_O | CTRL_AD9637_SYNC_B_O );
}

/******************************************************************************/

void ad9637_reset()
{
  unsigned int reg_value;

  reg_value = ad9637_adc_0_rd_cfg(0x08);
  reg_value |= 0x03;
  ad9637_adc_01_wr_cfg(0x08, reg_value);
  usleep(100000);
  reg_value &= ~0x03;
  ad9637_adc_01_wr_cfg(0x08, reg_value);
}

/******************************************************************************/
/******************************************************************************/
