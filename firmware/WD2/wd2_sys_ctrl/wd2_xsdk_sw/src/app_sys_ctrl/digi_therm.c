/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institute
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDREAM2
 *
 *  Author  :  ritt
 *  Created :  26.06.2015
 *
 *  Description :  Readout of 1-wire temperatur sensor DS28EA00
 *
 *-------------------------------------------------------------------------------------
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "mscb.h"
#include "dbg.h"
#include "xtime.h"
#include "hv_board_com.h"

#include "digi_therm.h"

/* DS28EA00 command codes */

#define CMD_WRITE_SCRATCHPAD     0x4E
#define CMD_READ_SCRATCHPAD      0xBE
#define CMD_COPY_SCRATCHPAD      0x48
#define CMD_CONVERT_TEMP         0x44
#define CMD_READ_POWER_MODE      0xB4
#define CMD_RECALL_EEPROM        0xB8
#define CMD_PIO_ACCESS_READ      0xF5
#define CMD_PIO_ACCESS_WRITE     0xA5

#define CMD_CHAIN                0x99
#define CMD_CHAIN_OFF            0x3C
#define CMD_CHAIN_OFF_INV        0xC3
#define CMD_CHAIN_ON             0x5A
#define CMD_CHAIN_ON_INV         0xA5
#define CMD_CHAIN_DONE           0x96
#define CMD_CHAIN_DONE_INV       0x69

#define CMD_READ_ROM             0x33
#define CMD_MATCH_ROM            0x55
#define CMD_SEARCH_ROM           0xF0
#define CMD_COND_SEARCH_ROM      0xEC
#define CMD_COND_READ_ROM        0x0F
#define CMD_SKIP_ROM             0xCC

#define CMD_OVERDRIVE_SKIP_ROM   0x3C
#define CMD_OVERDRIVE_MATCH_ROM  0x69

/* ROM address list */

static unsigned char rom[DIGI_TERM_MAX_N_DEVICES][8];

/*------------------------------------------------------------------*/

int DS28EA00_reset()
{
   int i;

   /* set for output */
   hv_digi_therm_setio(0);

   /* reset pulse 500 us */
   hv_digi_therm_set(0);
   usleep(500); /* tRSTL */
   hv_digi_therm_set(1);

   /* set for input */
   hv_digi_therm_setio(1);

   /* wait for presence pulse */
   usleep(75); /* tMSP */

   /* chip present if line low */
   i = (hv_digi_therm_get() == 0);

   usleep(500); /* tRSTH */

   /* set high */
   hv_digi_therm_set(1);

   /* set for output */
   hv_digi_therm_setio(0);

   return i;
}

/*------------------------------------------------------------------*/

void DS28EA00_write_bit(int bit)
{
   /* set high */
   hv_digi_therm_set(1);

   /* set for output */
   hv_digi_therm_setio(0);

   /* set low */
   hv_digi_therm_set(0);

   if (bit)
      usleep(15);   /* tW1L */
   else
      usleep(105);  /* tW0L */

   /* set high */
   hv_digi_therm_set(1);

   if (bit)
      usleep(105);  /* tSLOT(120us) - tW1L */
   else
      usleep(15);   /* tSLOT(120us) - tW0L */
}

/*------------------------------------------------------------------*/

void DS28EA00_write_byte(unsigned char byte)
{
   int i;

   for (i=0 ; i<8 ; i++)
      DS28EA00_write_bit((byte & (1<<i)) > 0);
}

/*------------------------------------------------------------------*/

int DS28EA00_read_bit()
{
   int bit;

   /* set high */
   hv_digi_therm_set(1);

   /* set for output */
   hv_digi_therm_setio(0);

   /* set low */
   hv_digi_therm_set(0);

   usleep(10); /* tRL */

   /* set for input */
   hv_digi_therm_setio(1);

   usleep(20); /* tMSR - tRL, 10 is too short */

   bit = hv_digi_therm_get();

   usleep(80); /* tSLOT - tMSR */

   return bit;
}

/*------------------------------------------------------------------*/

void DS28EA00_low_impedance_on()
{
   /* set high */
   hv_digi_therm_set(1);

   /* set for output */
   hv_digi_therm_setio(0);

}

/*------------------------------------------------------------------*/

unsigned char DS28EA00_read_byte()
{
   unsigned char i, d;

   for (i=d=0 ; i<8 ; i++)
      d = d | (DS28EA00_read_bit() ? (1<<i) : 0);

   return d;
}

/*------------------------------------------------------------------*/

int digi_therm_read(float temp[DIGI_TERM_MAX_N_DEVICES])
{
   static int n_dev = 0;
   static unsigned int last_convert = 0;
   static unsigned int last_scan = 0;

   int i, d, dev;
   unsigned char sp[10];

   /* if no devices in list, scan bus every second */
   if (n_dev == 0 && time_diff(time(), last_scan) > 1) {
/*      if (DBG_INFO) */
/*         printf("Scan  1-wire bus\r\n"); */
      last_scan = time();
      DS28EA00_reset();
      DS28EA00_write_byte(CMD_SKIP_ROM);
      DS28EA00_write_byte(CMD_CHAIN);
      DS28EA00_write_byte(CMD_CHAIN_ON);
      DS28EA00_write_byte(CMD_CHAIN_ON_INV);
      DS28EA00_low_impedance_on();
      d = DS28EA00_read_byte();
      usleep(500); /* wait for chain to charge */

      for (n_dev=0 ; n_dev<DIGI_TERM_MAX_N_DEVICES ; n_dev++) {
         DS28EA00_reset();
         DS28EA00_write_byte(CMD_COND_READ_ROM);
         for (i=0 ; i<8 ; i++)
            rom[n_dev][i] = DS28EA00_read_byte();

         if (rom[n_dev][0] == 0xFF) {
            break;
         }

         if (DBG_INFO) {
            printf("1-wire found device ");
            for (i=0 ; i<8 ; i++)
               printf("%02X ", rom[n_dev][i]);
         }

         DS28EA00_write_byte(CMD_CHAIN);
         DS28EA00_write_byte(CMD_CHAIN_DONE);
         DS28EA00_write_byte(CMD_CHAIN_DONE_INV);

         d = DS28EA00_read_byte();
         if (DBG_INFO)
            printf(" - %02X\r\n", d);
      }

      usleep(10000);
      DS28EA00_reset();
      DS28EA00_write_byte(CMD_SKIP_ROM);
      DS28EA00_write_byte(CMD_CHAIN);
      DS28EA00_write_byte(CMD_CHAIN_OFF);
      DS28EA00_write_byte(CMD_CHAIN_OFF_INV);
      DS28EA00_low_impedance_on();
      d = DS28EA00_read_byte();
   }

   if (temp != NULL && time_diff(time(), last_convert) > 0.75) {

      /* read each device sequentially */
      for (dev=0 ; dev<n_dev ; dev++) {
         d = DS28EA00_reset();
         if (!d) {
            /* no devices on bus -> reset device list */
            memset(rom, 0xFF, sizeof(rom));
            n_dev = 0;
            return 0;
         }
         DS28EA00_write_byte(CMD_MATCH_ROM);
         for (i=0 ; i<8 ; i++)
            DS28EA00_write_byte(rom[dev][i]);
         DS28EA00_write_byte(CMD_READ_SCRATCHPAD);
         sp[0] = DS28EA00_read_byte();
         sp[1] = DS28EA00_read_byte();
         temp[dev] = (float) ((short)(sp[0] | (sp[1] << 8))) * 85 / 0x550;
         if (DBG_INFO && dev == 0)
            printf("CH0: %f deg. C\r\n", temp[0]);
      }

      /* start new conversion */
      DS28EA00_reset();
      DS28EA00_write_byte(CMD_SKIP_ROM);
      DS28EA00_write_byte(CMD_CONVERT_TEMP);
      DS28EA00_low_impedance_on();

      last_convert = time();
      return n_dev;
   }

   return -1;
}
