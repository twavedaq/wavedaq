/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  19.08.2015 15:06:50
 *
 *  Description :  Software interface for the DRS4 control signals.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_drs4_control.h"
#include "register_map_wd2.h"
#include "system.h"
#include "xfs_printf.h"

/******************************************************************************/

void drs4_init(void)
{
  drs4_init_rsr();
}

/******************************************************************************/

int drs4_get_pll_lock(unsigned int device)
{
  int drs_lock = 0;

  if(device == DRS4_DEVICE_A)
  {
    drs_lock  = WD2_DRS_PLL_LOCK_0_MASK & reg_bank_get(WD2_DRS_PLL_LOCK_0_REG);
  }
  else if(device == DRS4_DEVICE_B)
  {
    drs_lock  = WD2_DRS_PLL_LOCK_1_MASK & reg_bank_get(WD2_DRS_PLL_LOCK_1_REG);
  }

  if(drs_lock)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void drs4_init_rsr()
{
  unsigned int reg_content;
  unsigned int data;

  data = WD2_DRS_READOUT_MODE_MASK;
  /* Get original state of readout mode */
  reg_content = reg_bank_get(WD2_REG_CTRL);
  /* Trigger init RSR */
  reg_bank_set(WD2_REG_CTRL, &data, 1);
  reg_bank_clr(WD2_REG_CTRL, &data, 1);
  /* Set readout mode to original state */
  reg_bank_write(WD2_REG_CTRL, &reg_content, 1);
}

/******************************************************************************/

void drs4_set_cfr(unsigned int value)
{
  unsigned int reg_content;

  reg_content = value;
  reg_bank_set(WD2_REG_DRS_CTRL, &reg_content, 1);
}

/******************************************************************************/

void drs4_clr_cfr(unsigned int value)
{
  unsigned int reg_content;

  reg_content = value;
  reg_bank_clr(WD2_REG_DRS_CTRL, &reg_content, 1);
}

/******************************************************************************/

void drs4_set_wsr(unsigned char value)
{
  unsigned int reg_content;

  reg_content = reg_bank_get(WD2_REG_DRS_CTRL);
  reg_content &= ~WD2_DRS_WSR_MASK;
  reg_content |= ((value << WD2_DRS_WSR_OFS) & WD2_DRS_WSR_MASK);
  reg_bank_write(WD2_REG_DRS_CTRL, &reg_content, 1);
}

/******************************************************************************/

void drs4_set_wcr(unsigned char value)
{
  unsigned int reg_content;

  reg_content = reg_bank_get(WD2_REG_DRS_CTRL);
  reg_content &= ~WD2_DRS_WCR_MASK;
  reg_content |= ((value << WD2_DRS_WCR_OFS) & WD2_DRS_WCR_MASK);
  reg_bank_write(WD2_REG_DRS_CTRL, &reg_content, 1);
}

/******************************************************************************/

void drs4_set_valid_delay(unsigned char delay)
{
  unsigned int reg_content;

  reg_content = reg_bank_get(WD2_REG_CTRL);
  reg_content &= ~WD2_VALID_DELAY_ADC_MASK;
  reg_content |= ((delay << WD2_VALID_DELAY_ADC_OFS) & WD2_VALID_DELAY_ADC_MASK);
  reg_bank_write(WD2_REG_CTRL, &reg_content, 1);
}

/******************************************************************************/

void drs4_start_daq()
{
  unsigned int data;

  data = WD2_DAQ_SINGLE_MASK;
  reg_bank_set(WD2_REG_CTRL, &data, 1);
  reg_bank_clr(WD2_REG_CTRL, &data, 1);
}

/******************************************************************************/

void drs4_soft_trigger()
{
  unsigned int data;

  data = WD2_DAQ_SOFT_TRIGGER_MASK;
  reg_bank_set(WD2_REG_CTRL, &data, 1);
  reg_bank_clr(WD2_REG_CTRL, &data, 1);
}

/******************************************************************************/

void drs4_fsm_reset()
{
  unsigned int data;

  data = WD2_DRS_CTRL_FSM_RST_MASK;
  reg_bank_set(WD2_REG_CTRL, &data, 1);
  reg_bank_clr(WD2_REG_CTRL, &data, 1);
}

/******************************************************************************/
/******************************************************************************/
