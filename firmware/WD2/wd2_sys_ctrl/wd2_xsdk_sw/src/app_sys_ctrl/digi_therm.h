/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  ritt
 *  Created :  26.06.2015
 *
 *  Description :  Readout of 1-wire temperatur sensor DS28EA00
 *
 *-------------------------------------------------------------------------------------
 */
 
#ifndef __DIGI_TERM_H__
#define __DIGI_TERM_H__

/************************************************************/

#define DIGI_TERM_MAX_N_DEVICES 16

int digi_therm_read(float temperature[DIGI_TERM_MAX_N_DEVICES]);

#endif /* __DIGI_TERM_H__ */
