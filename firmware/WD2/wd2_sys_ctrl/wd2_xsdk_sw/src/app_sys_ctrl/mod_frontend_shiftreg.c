/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  07.05.2014 08:55:59
 *
 *  Description :  Module for SPI access to LTC2600 temperature sensor.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "system.h"
#include "gpio_shift_register.h"
#include "register_map_wd2.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>


/************************************************************/

  int frontend_set(int argc, char **argv)
  {
    unsigned int ch_nr;
    unsigned int value;
    unsigned int reg_val;
    unsigned int mask = 0xFFFF0000;
    int i;

    CMD_HELP("<frontend_channel> <settings_value>",
             "configure one or all channels of the frontend",
             "Configures the coresponding channel of the frontend.\r\n"
             "  <frontend_channel> : frontend channel to set (range = 0..15 or all).\r\n"
             "  <settings_value>   : hex value representing the settings.\r\n"
             "                       Bit layout:\r\n"
             "                       D8    D7    D6    D5    D4    D3    D2    D1    D0   \r\n"
             "                       ACDC  COMP2 OP2   COMP1 OP1   ATT1  ATT0  CAL1  CAL0 \r\n"
             "                       ^MSB                                            ^LSB\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    value = hatoi((const char*)argv[2]);

    if(fstrcmp(argv[1],"all")) /* write setting to all channels */
    {
      value = (value << 16) | value;
      for(i=0;i<8;i++)
      {
        reg_bank_write(WD2_REG_FE_CFG_0_1 + i*0x04, &value, 1);
      }
    }
    else  /* write setting to one channel */
    {
      ch_nr = xfs_atoi(argv[1]);
      /* Validate argument range */
      if(ch_nr > 15)
      {
        xfs_printf("E%02X: Invalid arguments. Type \"feset help\" for help.\r\n", ERR_INVALID_ARGS);
        return 0;
      }

      /* Even channels are stored in the MSBs */
      if( !(ch_nr&0x01) )
      {
        value = value << 16;
        mask  = 0x0000FFFF;
      }

      /* Read */
      reg_val = reg_bank_get(WD2_REG_FE_CFG_0_1 + (ch_nr/2)*0x04);
      /* Modify */
      reg_val = (reg_val & mask) | value;
      /* Write */
      reg_bank_write(WD2_REG_FE_CFG_0_1 + (ch_nr/2)*0x04, &reg_val, 1);
    }
    
    return 0;
  }

/************************************************************/

  int module_fe_help(int argc, char **argv)
  {
    CMD_HELP("",
             "Frontend Configuration Module",
             "Can be used to program the shift registers configuring the frontend"
            );

    return 0;
  }


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type frontend_shiftreg_cmd_table[] =
  {
    {0, "fe", module_fe_help},
    {0, "feset", frontend_set},
    {0, NULL, NULL}
  };

/************************************************************/
