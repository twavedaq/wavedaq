/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.09.2014 08:58:04
 *
 *  Description :  Software controlled SPI controller for communication with
 *                 ADCs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_SOFT_SPI_ADC_H__
#define __DRV_SOFT_SPI_ADC_H__

/*
*  Format :
*           A3 A2 A1 A0             - Reg Address
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*
*  out_cfg: A4 A3 A2 A1 A0 D5 D4 D3 D2 D1 D0
*                                         ^lsb send first
*/
#define  SPI_DATA_DIR_OUT   0
#define  SPI_DATA_DIR_IN    1

/* PORT IO_PORT8_IO = CSB_B_O & SDIO_B_IO & SCLK_B_O & CSB_A_O & SDIO_A_IO & SCLK_A_O */

#define  SPI_AD9637_SCLK_A_O    0x01
#define  SPI_AD9637_SDIO_A_IO   0x02
#define  SPI_AD9637_CSB_A_O     0x04
#define  SPI_AD9637_SCLK_B_O    0x08
#define  SPI_AD9637_SDIO_B_IO   0x10
#define  SPI_AD9637_CSB_B_O     0x20
#define  CTRL_AD9637_SYNC_A_O   0x40
#define  CTRL_AD9637_SYNC_B_O   0x80

/************************************************************/
/*AD9637 defines                                            */
/************************************************************/
#define  AD9637_CMD_READ         0x80
#define  AD9637_CMD_WRITE        0x00

#define  AD9637_CMD_CFG_1BYTE    0x00
#define  AD9637_CMD_CFG_2BYTES   0x20
#define  AD9637_CMD_CFG_3BYTES   0x40
#define  AD9637_CMD_CFG_STREAM   0x60

#define  AD9637_CFG_DI1_CH_A     0x01
#define  AD9637_CFG_DI1_CH_B     0x02
#define  AD9637_CFG_DI1_CH_C     0x04
#define  AD9637_CFG_DI1_CH_D     0x08
#define  AD9637_CFG_DI1_CH_E     0x00
#define  AD9637_CFG_DI1_CH_F     0x00
#define  AD9637_CFG_DI1_CH_G     0x00
#define  AD9637_CFG_DI1_CH_H     0x00
#define  AD9637_CFG_DI1_CH_FCO   0x10
#define  AD9637_CFG_DI1_CH_DCO   0x20

#define  AD9637_CFG_DI2_CH_A     0x00
#define  AD9637_CFG_DI2_CH_B     0x00
#define  AD9637_CFG_DI2_CH_C     0x00
#define  AD9637_CFG_DI2_CH_D     0x00
#define  AD9637_CFG_DI2_CH_E     0x01
#define  AD9637_CFG_DI2_CH_F     0x02
#define  AD9637_CFG_DI2_CH_G     0x04
#define  AD9637_CFG_DI2_CH_H     0x08
#define  AD9637_CFG_DI2_CH_FCO   0x00
#define  AD9637_CFG_DI2_CH_DCO   0x00

#define  AD9637_CFG_DI1_ALL      0x3F
#define  AD9637_CFG_DI2_ALL      0x0F

#define  AD9637_LOCAL_MASK       0x00CFFF04
#define  AD9637_GLOBAL_MASK      0x003000FB

#define  AD9637_NO_GLOB_REGS         7
#define  AD9637_NO_CH               10

#define AD9637_OMIT_TXRX_ADDR    0x1FF

/*** constants **********************************************/

const unsigned int ad9637_addr[7][4] =
{
  { 0x000                 , 0x004 , 0x005 , AD9637_OMIT_TXRX_ADDR }, /* 0x0FF, */
  { 0x008                 , 0x009 , 0x00B , 0x00C                 },
  { AD9637_OMIT_TXRX_ADDR , 0x00D , 0x010 , 0x014                 },
  { AD9637_OMIT_TXRX_ADDR , 0x015 , 0x016 , 0x018                 },
  { 0x01A                 , 0x019 , 0x01C , 0x01B                 },
  { AD9637_OMIT_TXRX_ADDR , 0x021 , 0x022 , AD9637_OMIT_TXRX_ADDR }, /*0x100, */
  { AD9637_OMIT_TXRX_ADDR , 0x101 , 0x102 , 0x109                 }
};
/* Note: addresses 0x0FF and 0x100 have been removed. Writing to address 0x100 and subsequently
 *       writing to address 0x0FF (no matter what the data was) resulted in clearing of other
 *       registers,i.e. addresses 0x19 to 0x1C. There is no explanation in the datasheet for this
 *       behavior. According to the datasheet, writing to 0xFF should only affect operations on
 *       register 0x100.
 */
const unsigned char ad9637_di1_ch[10] = { AD9637_CFG_DI1_CH_A,
                                          AD9637_CFG_DI1_CH_B,
                                          AD9637_CFG_DI1_CH_C,
                                          AD9637_CFG_DI1_CH_D,
                                          AD9637_CFG_DI1_CH_E,
                                          AD9637_CFG_DI1_CH_F,
                                          AD9637_CFG_DI1_CH_G,
                                          AD9637_CFG_DI1_CH_H,
                                          AD9637_CFG_DI1_CH_DCO,
                                          AD9637_CFG_DI1_CH_FCO };

const unsigned char ad9637_di2_ch[10] = { AD9637_CFG_DI2_CH_A,
                                          AD9637_CFG_DI2_CH_B,
                                          AD9637_CFG_DI2_CH_C,
                                          AD9637_CFG_DI2_CH_D,
                                          AD9637_CFG_DI2_CH_E,
                                          AD9637_CFG_DI2_CH_F,
                                          AD9637_CFG_DI2_CH_G,
                                          AD9637_CFG_DI2_CH_H,
                                          AD9637_CFG_DI2_CH_DCO,
                                          AD9637_CFG_DI2_CH_FCO };

/************************************************************/

void ad9637_soft_spi_adc_init(void);

/* AD9637 functions */
void ad9637_write_single_cfg(unsigned int slave, unsigned int address, unsigned char reg_value);
unsigned int ad9637_read_single_cfg(unsigned int slave, unsigned int address);
void ad9637_adc_0_wr_cfg(unsigned int address, unsigned char reg_value);
void ad9637_adc_1_wr_cfg(unsigned int address, unsigned char reg_value);
void ad9637_adc_01_wr_cfg(unsigned int address, unsigned char reg_value);
unsigned int ad9637_adc_0_rd_cfg(unsigned int address);
unsigned int ad9637_adc_1_rd_cfg(unsigned int address);
void ad9637_sync();
void ad9637_reset();
#endif /* __DRV_SOFT_SPI_ADC_H__ */
