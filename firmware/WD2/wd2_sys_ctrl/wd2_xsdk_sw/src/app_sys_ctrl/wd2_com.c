/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  21.03.2017 16:29:40
 *
 *  Description :  Support for WaveDream2 specific communication.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include "wd2_com.h"
#include "xfs_printf.h"
#include "system.h"
#include "ether_com.h"
#include "drv_plb_wd2_reg_bank.h"
#include "register_map_wd2.h"
#include "utilities.h"

/************************** Variable Definitions ***************************/


/************************** Function Definitions ***************************/

void prepare_wd2_frame(network_if_type *nw_if, void* frame)
{
  unsigned int reg_val;
  udp_header_type* udp_fp = (udp_header_type*)frame;
  wd2_header_type* wd2_fp = (wd2_header_type*)&udp_fp[1]; /* (wd2_header_type*)((char*)&frame[UDP_HEADER_LEN]); */

  prepare_udp_frame(nw_if, udp_fp);

  reg_bank_read(WD2_REG_PROT_VER, &reg_val, 1);
  wd2_fp->protocol_version[0]       = (unsigned char)reg_val;

  reg_bank_read(WD2_REG_HW_VER, &reg_val, 1);
  wd2_fp->board_revision[0]         = (unsigned char)reg_val;

  wd2_fp->board_id[0]               = 0; /* ??? */
  wd2_fp->board_id[1]               = 0; /* ??? */

  reg_bank_read(WD2_REG_WDB_LOC, &reg_val, 1);
  wd2_fp->crate_id[0]               = (unsigned char)(reg_val>>16);
  wd2_fp->slot_id[0]                = (unsigned char)reg_val;

  wd2_fp->channel_info[0]           = 0;
  wd2_fp->ch_segment_pkg_type[0]    = 0x0F;

  wd2_fp->event_number[0]           = 0; /* tbd add event number register and */
  wd2_fp->event_number[1]           = 0; /* fill information here */
  wd2_fp->event_number[2]           = 0; /* how do we make sure the timing is correct and */
  wd2_fp->event_number[3]           = 0; /* the event number really belongs to the infomation? */

  wd2_fp->samping_frequency[0]      = 0;
  wd2_fp->samping_frequency[1]      = 0;

  wd2_fp->payload_length[0]         = 0; /* has to be updated later */
  wd2_fp->payload_length[1]         = 0;

  reg_bank_read(WD2_REG_TRB_INFO_MSB, &reg_val, 1);
  wd2_fp->trigger_type[0]           = (unsigned char)reg_val;       /* how do we make sure the timing is correct and */     
  wd2_fp->trigger_type[1]           = (unsigned char)(reg_val>>8);  /* the trigger number really belongs to the infomation? */
  reg_bank_read(WD2_REG_TRB_INFO_LSB, &reg_val, 1);
  wd2_fp->trigger_number[0]         = (unsigned char)reg_val;      /* how do we make sure the timing is correct and */     
  wd2_fp->trigger_number[1]         = (unsigned char)(reg_val>>8); /* the trigger number really belongs to the infomation? */

  reg_bank_read(WD2_REG_DRS_STOP_CELL, &reg_val, 1);
  wd2_fp->drs0_trigger_cell[0]      = (unsigned char)(reg_val>>16);
  wd2_fp->drs0_trigger_cell[1]      = (unsigned char)(reg_val>>24);
  wd2_fp->drs1_trigger_cell[0]      = (unsigned char)reg_val;
  wd2_fp->drs1_trigger_cell[1]      = (unsigned char)(reg_val>>8);

  reg_bank_read(WD2_REG_STATUS, &reg_val, 1);
  wd2_fp->temperature[0]            = (unsigned char)(reg_val>>24);
  wd2_fp->temperature[1]            = (unsigned char)(reg_val>>16);

  wd2_fp->reserved[0]               = 0;
  wd2_fp->reserved[1]               = 0;
  wd2_fp->reserved[2]               = 0;
  wd2_fp->reserved[3]               = 0;

  wd2_fp->packet_sequence_number[0] = 0; /* ??? how update ??? */
  wd2_fp->packet_sequence_number[1] = 0;

  /*fcp(udp_fp->src_mac ,  nw_if->mac_adr); */
}

/***************************************************************************/

void adjust_wd2_header(wd2_header_type *wd2_fp, int wd2_payload_len)
{
  wd2_fp->payload_length[0] = (wd2_payload_len >> 8) & 0xff ;
  wd2_fp->payload_length[1] = wd2_payload_len & 0xff ;
}

/**********************************************************************************/
#define WD2_UDP_BUFFER_LEN 1500
char wd2_udp_buff[WD2_UDP_BUFFER_LEN];

int wd2_send_frame(network_if_type *nw_if, char* buffer, int len)
{
  udp_header_type *wd2_udp_fp = (udp_header_type *)wd2_udp_buff;

  prepare_wd2_frame(nw_if, wd2_udp_fp);
  /* insert ports, mac and ip address */
  wd2_udp_fp->src_port[0] = SYSPTR(cfg)->nw_data_src_port >> 0x08;
  wd2_udp_fp->src_port[1] = SYSPTR(cfg)->nw_data_src_port  & 0xff;
  wd2_udp_fp->dst_port[0] = SYSPTR(nw_data_target)->dst_port >> 0x08;
  wd2_udp_fp->dst_port[1] = SYSPTR(nw_data_target)->dst_port  & 0xff;
  fcp(wd2_udp_fp->dst_mac,  SYSPTR(nw_data_target)->dst_mac_addr);
  fcp(wd2_udp_fp->dst_ip ,  SYSPTR(nw_data_target)->dst_ip_addr);

  xfs_local_printf("Message Length: %d, UDP header: %d, WD2 header: %d\r\n", len, UDP_HEADER_LEN, WD2_HEADER_LEN);
  len += UDP_HEADER_LEN + WD2_HEADER_LEN;
  /* limit output to max term buffer size */
  if (len > (int)(WD2_UDP_BUFFER_LEN)) len = (WD2_UDP_BUFFER_LEN);
  
  /* copy data to udp buffer */
  ncpy((char*)&wd2_udp_buff[UDP_HEADER_LEN+WD2_HEADER_LEN], buffer, len-UDP_HEADER_LEN-WD2_HEADER_LEN);
  
  adjust_wd2_header((wd2_header_type*)(&wd2_udp_fp[1]), len-UDP_HEADER_LEN-WD2_HEADER_LEN);

  /* print_frame((unsigned char*)wd2_udp_fp, len); */
  udp_send_frame(nw_if, wd2_udp_fp, len-UDP_HEADER_LEN);

  return 0;
}

/***************************************************************************/

int wd2_side_data_frame(network_if_type *nw_if, side_data_bank_type** sd_banks_ptr, int nr_of_sd_banks)
{
  char* tx_buffer_ptr;
  char* sd_data_ptr = NULL;
  int len;
  int id_size, adr_size, len_size;
  unsigned int sd_adr, sd_data_len;
  udp_header_type *wd2_udp_fp = (udp_header_type *)wd2_udp_buff;

  tx_buffer_ptr = (char*)(&wd2_udp_buff);

  prepare_wd2_frame(nw_if, wd2_udp_fp);
  /* insert ports, mac and ip address */
  wd2_udp_fp->src_port[0] = SYSPTR(cfg)->nw_data_src_port >> 0x08;
  wd2_udp_fp->src_port[1] = SYSPTR(cfg)->nw_data_src_port  & 0xff;
  wd2_udp_fp->dst_port[0] = SYSPTR(nw_data_target)->dst_port >> 0x08;
  wd2_udp_fp->dst_port[1] = SYSPTR(nw_data_target)->dst_port  & 0xff;
  fcp(wd2_udp_fp->dst_mac,  SYSPTR(nw_data_target)->dst_mac_addr);
  fcp(wd2_udp_fp->dst_ip ,  SYSPTR(nw_data_target)->dst_ip_addr);

  for(int i=0;i<nr_of_sd_banks;i++)
  {
    unsigned int sd_transmitted_len =0;
    len = UDP_HEADER_LEN + WD2_HEADER_LEN;
 
    sd_adr = sd_banks_ptr[i]->mem_offset;
    sd_data_ptr = (char*)sd_adr;
    sd_data_len = sd_banks_ptr[i]->mem_length*4;
    id_size  = sizeof(sd_banks_ptr[i]->id)-1;
    len_size = sizeof(sd_data_len);
    adr_size = sizeof(sd_adr);
    /*sd_bank_header_len = id_size + len_size + adr_size; */
    /*unsigned int sd_max_size = WD2_UDP_BUFFER_LEN - (UDP_HEADER_LEN+ WD2_HEADER_LEN + sd_bank_header_len); */
    unsigned int sd_max_size = 1024;      

    while(sd_transmitted_len < sd_data_len){

      if( sd_data_len - sd_transmitted_len > sd_max_size )
      {
        sd_data_len = sd_max_size;
      } else {
       sd_data_len -= sd_transmitted_len;
      }
      
      /* bank header */
      /* ID */
      ncpy(&tx_buffer_ptr[len], sd_banks_ptr[i]->id, id_size);
      len += id_size;
      /* start address */
      ncpy(&tx_buffer_ptr[len], (char*)(&sd_data_ptr), adr_size);
      len += adr_size;
      /* length (number of 32 bit words)*/
      ncpy(&tx_buffer_ptr[len], (char*)(&sd_data_len), len_size); /* limit output to max term buffer size */
      len += len_size;
      /* bank data */
      ncpy(&tx_buffer_ptr[len], sd_data_ptr, sd_data_len);
      len += sd_data_len;
      
      adjust_wd2_header((wd2_header_type*)(&wd2_udp_fp[1]), len-UDP_HEADER_LEN-WD2_HEADER_LEN);
      udp_send_frame(nw_if, wd2_udp_fp, len-UDP_HEADER_LEN);

      sd_transmitted_len += sd_data_len;
      if(sd_data_len==sd_max_size){
         sd_data_ptr += sd_data_len;
      }
      sd_data_len = sd_banks_ptr[i]->mem_length*4;
      len = UDP_HEADER_LEN + WD2_HEADER_LEN;
      
    }
  }

  return 0;
}

/***************************************************************************/
