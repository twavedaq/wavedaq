/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  21.03.2017 16:29:45
 *
 *  Description :  Support for WaveDream2 specific communication.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __WD2_COM_H__
#define __WD2_COM_H__

/***************************** Include Files *******************************/
#include "network_if.h"
#include "side_data.h"

/***************************** definitions *********************************/

#define WD2_HEADER_LEN  (sizeof(wd2_header_type))

/***************************** type definitions ****************************/

/* WD2 Header (32 byte) */
typedef struct
{
  unsigned char protocol_version[1];
  unsigned char board_revision[1];
  unsigned char board_id[2];
  unsigned char crate_id[1];
  unsigned char slot_id[1];
  unsigned char channel_info[1];
  unsigned char ch_segment_pkg_type[1];
  unsigned char event_number[4];
  unsigned char samping_frequency[2];
  unsigned char payload_length[2];
  unsigned char trigger_number[2];
  unsigned char drs0_trigger_cell[2];
  unsigned char drs1_trigger_cell[2];
  unsigned char trigger_type[2];
  unsigned char temperature[2];
  unsigned char reserved[4];
  unsigned char packet_sequence_number[2];
} wd2_header_type;

/************************** Function Prototypes ****************************/

void prepare_wd2_frame(network_if_type *nw_if, void* frame);
int wd2_send_frame(network_if_type *nw_if, char* buffer, int len);
int wd2_side_data_frame(network_if_type *nw_if, side_data_bank_type** sd_banks_ptr, int nr_of_sd_banks);

#endif /** __WD2_COM_H__ */
