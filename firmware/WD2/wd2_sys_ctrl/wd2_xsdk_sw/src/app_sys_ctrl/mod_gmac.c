/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.05.2014 13:24:53
 *
 *  Description :  Module for SPI access to MAX6662 temperature sensor.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xfs_printf.h"
#include "utilities.h"
#include "system.h"
#include "cmd_processor.h"

#include "network_if.h"
#include "dhcp.h"

/************************************************************/

int gmac_cmd(int argc, char **argv)
{
  unsigned char buff[6];
  int val;
  
  CMD_HELP("<cmd>",
           "gmac dbg commands",
           "  <cmd> : rst          reset  fifos\r\n"
           "          rst_rx       reset  rx\r\n"
           "          rst_tx       reset  tx\r\n"
           "          promisc 0|1  set promiscuos mode\r\n"
           "          bcast   0|1  set rcv broadcast mode\r\n"
           "          status       show status\r\n"
          );

  if(fstrcmp(argv[1],"rst"))
  {
    xfs_printf("Resetting LL_FIFO \r\n");
    plb_ll_fifo_reset(SYSPTR(plb_fifo_gmac_0), PLB_LL_FIFO_CTRL_RESET_ALL);
  }
  else if(fstrcmp(argv[1],"rst_rx"))
  {
    xfs_printf("Resetting RX\r\n");
    plb_ll_fifo_reset(SYSPTR(plb_fifo_gmac_0), PLB_LL_FIFO_CTRL_RX_RESET);
  }
  else if(fstrcmp(argv[1],"rst_tx"))
  {
    xfs_printf("Resetting TX\r\n");
    plb_ll_fifo_reset(SYSPTR(plb_fifo_gmac_0), PLB_LL_FIFO_CTRL_TX_RESET);
  }
  else if ((argc > 2) & (fstrcmp(argv[1],"promisc")))
  {
    val = xfs_atoi(argv[2]);
    xfs_printf("Setting promiscuos mode to %d\r\n", val);
    gmac_lite_promiscuos_mode(SYSPTR(gmac_lite_0), val);
  }
  else if ((argc > 2) & (fstrcmp(argv[1],"promisc")))
  {
    val = xfs_atoi(argv[2]);
    xfs_printf("Setting rcv broadcast to %d\r\n", val);
    gmac_lite_rcv_broadcast(SYSPTR(gmac_lite_0), val);
  }
  else /* if(fstrcmp(argv[1],"status")) */
  {
    plb_ll_fifo_status(SYSPTR(plb_fifo_gmac_0));
    nw_if_get_mac_address(SYSPTR(nw_if_gmac_0), buff);
    xfs_printf("MAC Address     : %02X:%02X:%02X:%02X:%02X:%02X\r\n", buff[0], buff[1], buff[2], buff[3], buff[4], buff[5] );
    gmac_lite_mac_filter_get(SYSPTR(gmac_lite_0), buff);
    xfs_printf("MAC Filter      : %02X:%02X:%02X:%02X:%02X:%02X\r\n", buff[0], buff[1], buff[2], buff[3], buff[4], buff[5] );
    val = gmac_lite_promiscuos_mode(SYSPTR(gmac_lite_0), -1);
    xfs_printf("promiscuos mode : %d\r\n", val);
    val = gmac_lite_rcv_broadcast(SYSPTR(gmac_lite_0), -1);
    xfs_printf("rcv broadcast   : %d\r\n", val);
  }
  
  return 0;  
}

/************************************************************/

int dhcp_cmd(int argc, char **argv)
{
  CMD_HELP("",
           "trigger dhcp"
          );
          
  dhcp_init((dhcp_type*)(SYSPTR(nw_if_gmac_0)->dhcp));
  nw_if_set_dhcp(SYSPTR(nw_if_gmac_0), 1);

  return 0;
}

/************************************************************/

int module_gmac_lite_help(int argc, char **argv)
{
  CMD_HELP("",
           "Gigabit Ethernet Lite Module"
          );

  return 0;
}


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

cmd_table_entry_type gmac_cmd_table[] =
{
  {0, "gmac_lite", module_gmac_lite_help},
  {0, "gmac", gmac_cmd},
  {0, "dhcp", dhcp_cmd},
  {0, NULL, NULL}
};

/************************************************************/
