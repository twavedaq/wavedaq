/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Software controlled SPI controller for communication with
 *                 temperature sensor MAX6662, DAC LTC2600 and LMK03000.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "xtime.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "register_map_wd2.h"
#include "update_config.h"
#include "drv_plb_wd2_reg_bank.h"
#include "dbg.h"

/******************************************************************************/

/************************************************************/
/* LTC2600 variables                                        */
/************************************************************/
/* Initial DAC voltages in nV */
unsigned int LTC2600_INIT_NV[24] = {1600*1e6,  /* DAC0 VOUTA ROFS */
                                    1250*1e6,  /* DAC0 VOUTB OFS */
                                    1250*1e6,  /* DAC0 VOUTC CAL_DC */
                                    1250*1e6,  /* DAC0 VOUTD PULSE_AMP */
                                    1250*1e6,  /* DAC0 VOUTE PCZ_LEVEL */
                                       0*1e6,  /* DAC0 VOUTF RESERVED */
                                       0*1e6,  /* DAC0 VOUTG RESERVED */
                                     800*1e6,  /* DAC0 VOUTH BIAS */
                                     700*1e6,  /* DAC1 VOUTA TLEVEL_0 */
                                     800*1e6,  /* DAC1 VOUTB TLEVEL_1 */
                                     800*1e6,  /* DAC1 VOUTC TLEVEL_2 */
                                     800*1e6,  /* DAC1 VOUTD TLEVEL_3 */
                                     800*1e6,  /* DAC1 VOUTE TLEVEL_4 */
                                     800*1e6,  /* DAC1 VOUTF TLEVEL_5 */
                                     800*1e6,  /* DAC1 VOUTG TLEVEL_6 */
                                     800*1e6,  /* DAC1 VOUTH TLEVEL_7 */
                                     800*1e6,  /* DAC2 VOUTA TLEVEL_8 */
                                     800*1e6,  /* DAC2 VOUTB TLEVEL_9 */
                                     800*1e6,  /* DAC2 VOUTC TLEVEL_10 */
                                     800*1e6,  /* DAC2 VOUTD TLEVEL_11 */
                                     800*1e6,  /* DAC2 VOUTE TLEVEL_12 */
                                     800*1e6,  /* DAC2 VOUTF TLEVEL_13 */
                                     800*1e6,  /* DAC2 VOUTG TLEVEL_14 */
                                     800*1e6}; /* DAC2 VOUTH TLEVEL_15 */
/************************************************************/

void max31723_init();
void lmk03000_init();
void ltc2600_init();

void soft_spi_temp_dac_lmk_init(void)
{
  plb_gpio_write(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_DAC_CS | SPI_LMK_CS); /* SPI_TEMP_CS is high active !!! */
  plb_gpio_tristate_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SCLK_O | SPI_TEMP_DAC_LMK_SDI_O |
                                          SPI_TEMP_CS | SPI_DAC_CS | SPI_LMK_CS);
  plb_gpio_tristate_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDO_I);

  max31723_init();
  lmk03000_init();
  ltc2600_init();
}

/******************************************************************************/

void soft_spi_sck(unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SCLK_O );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SCLK_O );
  }
}

/******************************************************************************/

void soft_spi_cs_n(unsigned char slave, unsigned int val)
{
  if(slave&SPI_TEMP_CS)
  {
    if (val)
    {
      plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_CS );
    }
    else
    {
      plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_CS );
    }
  }
  else
  {
    if (val)
    {
      plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, slave );
    }
    else
    {
      plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, slave );
    }
  }
}

/******************************************************************************/

void soft_spi_sdo(unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDI_O );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDI_O );
  }
}

/******************************************************************************/

unsigned int soft_spi_sdi()
{
  if (plb_gpio_get(SYSPTR(gpio_sys), GPIO_SOFT_SPI_TEMP_DAC_LMK) & SPI_TEMP_DAC_LMK_SDO_I)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void soft_spi_wait(void)
{
  usleep(1);
}

/******************************************************************************/

void soft_spi_transmit(unsigned char slave, unsigned char* tx_buffer, unsigned int nr_of_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_cs_n(slave, 0);
  usleep(1);

  for (i=0; i<nr_of_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_sck(0);
      soft_spi_sdo(value & 0x80);
      soft_spi_wait();
      soft_spi_sck(1);
      soft_spi_wait();
      value <<= 1;
    }
  }
  soft_spi_sck(0);

  soft_spi_wait();
  soft_spi_cs_n(slave, 1);

}

/******************************************************************************/

void soft_spi_transmit(unsigned char slave, unsigned int tx_word)
{
  unsigned char tx_data[4];
  
  for(int i=3;i>=0;i--)
  {
    tx_data[i] = (unsigned char)(tx_word&0x000000FF);
    tx_word >>= 8;
  }

  soft_spi_transmit(slave, tx_data, 4);
}

/******************************************************************************/

void soft_spi_transceive(unsigned char slave, unsigned char* tx_buffer, unsigned char* rx_buffer, unsigned int nr_of_tx_bytes, unsigned int nr_of_rx_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_cs_n(slave, 0);
  usleep(1);

  /* transmit */
  for (i=0; i<nr_of_tx_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_sck(0);
      soft_spi_sdo(value & 0x80);
      soft_spi_wait();
      soft_spi_sck(1);
      /* soft_spi_sdo(value & 0x80); */
      soft_spi_wait();
      value <<= 1;
    }
  }

  /* receive */
  for (i=0; i<nr_of_rx_bytes; i++)
  {
    value = 0;

    for (j=0; j<8; j++)
    {
      value <<= 1;
      soft_spi_sck(0);
      soft_spi_wait();
      soft_spi_sck(1);
      value |= soft_spi_sdi();
      soft_spi_wait();
    }

    rx_buffer[i] = value;
  }

  soft_spi_sck(0);
  soft_spi_wait();
  soft_spi_cs_n(slave, 1);
}



/******************************************************************************/
/******************************************************************************/

void max31723_init()
{
  unsigned char tx_buffer[2];

  /* Write Configuration Register Command */
  tx_buffer[0] = MAX31723_CMD_WRITE | MAX31723_ADR_CFG_STAT;
  /* Configuration: 12Bit Resolution */
  tx_buffer[1] = MAX31723_CFG_RES_12BIT;

  soft_spi_transceive(SPI_TEMP_CS, tx_buffer, NULL, 2, 0);
}

/******************************************************************************/

int max31723_get_temp()
{
  unsigned char tx_buffer[1] = {MAX31723_CMD_READ | MAX31723_ADR_TEMP_LSB};
  unsigned char rx_buffer[2];
  int temp;

  soft_spi_transceive(SPI_TEMP_CS, tx_buffer, rx_buffer, 1, 2);

  temp = ( ((int)rx_buffer[1] << 4) | ((int)rx_buffer[0] >> 4) ) & 0x0FFF;

  if(temp & 0x00000800) /* sign bit is set */
  {
    temp |= 0xFFFFF800; /* negative value: set msbs to 1 */
  }
  else
  {
    temp &= 0x000007FF; /* positivie value: set msbs to 0 */
  }

  return temp;
}

/******************************************************************************/

void max31723_print_temp()
{
  int temp;
  char sign = ' ';
  unsigned int decimals;

  temp = max31723_get_temp();

  decimals = temp & 0x0F;
  if(temp < 0)
  {
    sign = '-';
    decimals = (~(decimals-1) & 0x0F);
  }
  decimals = decimals * 625;

  /* round to one digit */
  xfs_printf("%c%d.%01d", sign, temp/16 , (decimals+500)/1000);
}

/******************************************************************************/
/******************************************************************************/

void lmk03000_init()
{
  soft_spi_transmit(SPI_LMK_CS, WD2_LMK0_RESET_MASK);

  for(unsigned int i=0;i<14;i++)
  {
    soft_spi_transmit(SPI_LMK_CS, ctrl_reg_default[(WD2_REG_LMK_0-REG_CTRL_START)/4 + i]);
  }
}

/******************************************************************************/

void lmk03000_upload_configuration()
{
  /* If register entries are valid set all LMK device registers to the corresponding values */
  /* if register entries are invalid set all LMK device registers to the default values */
  unsigned int lmk_reg_addr[14] = {0,1,2,3,4,5,6,7,8,9,11,13,14,15}; /* Registers 10 and 12 do are not writable on LMK */
  unsigned int reg_value;
  unsigned int mod_flag_mask;
  unsigned int mod_flag_offset;
  unsigned char valid = 1;

  /* check if all register bank contents can be assumed to be valid */
  for(unsigned int i=0;i<14;i++)
  {
    reg_value = reg_bank_get(WD2_REG_LMK_0+(i*4));
    if( lmk_reg_addr[i] != (reg_value&0x0F) )
    {
      if(DBG_INFO) xfs_local_printf("\r\n"
                                    "Error: Invalid LMK setting [0x%04X]: 0x%08X in register bank\r\n"
                                    "Setting default LMK Configuration...\r\n"
                                    "\r\n", WD2_REG_LMK_0+(i*4), reg_value);
      valid = 0;
      break;
    }
  }

  /* if all register bank are assumed invalid: set them to the default values */
  if(!valid)
  {
    for(unsigned int i=0;i<14;i++)
    {
      reg_value = ctrl_reg_default[(WD2_REG_LMK_0-REG_CTRL_START)/4 + i];
      reg_bank_write(WD2_REG_LMK_0+(i*4), &reg_value, 1);
    }
  }

  /* reset register configuration */
  reg_value = WD2_LMK0_RESET_MASK;
  soft_spi_transmit(SPI_LMK_CS, reg_value);

  /* configure the LMK */
  mod_flag_mask = WD2_LMK_0_MOD_MASK;
  mod_flag_offset = WD2_REG_LMK_0_7_MOD_FLAG;
  
  for(unsigned int i=0;i<14;i++)
  {
    reg_bank_write(mod_flag_offset, &mod_flag_mask, 1); /* clears modified flag !!! */
    reg_value = reg_bank_get(WD2_REG_LMK_0+(i*4));

    soft_spi_transmit(SPI_LMK_CS, reg_value);

    if(i==7)
    {
      mod_flag_mask = WD2_LMK_8_MOD_MASK;
      mod_flag_offset = WD2_REG_LMK_8_15_MOD_FLAG;
    }
    else
    {
      mod_flag_mask >>= 4;
    }
  }
}

/******************************************************************************/

void lmk03000_reset()
{
  unsigned int tx_val = WD2_LMK0_RESET_MASK;

  /* reset register configuration */
  soft_spi_transmit(SPI_LMK_CS, tx_val);
}

/******************************************************************************/

void lmk03000_set_sync(int val)
{
  unsigned int data;

  data = WD2_LMK_SYNC_LOCAL_MASK;
  if(val)
  {
    reg_bank_set(WD2_REG_COM_CTRL, &data, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_COM_CTRL, &data, 1);
  }
}

/******************************************************************************/

void lmk03000_sync(void)
{
  unsigned int data;

  data = WD2_LMK_SYNC_LOCAL_MASK;
  reg_bank_set(WD2_LMK_SYNC_LOCAL_REG, &data, 1);
  usleep(100);
  reg_bank_clr(WD2_LMK_SYNC_LOCAL_REG, &data, 1);
}

/******************************************************************************/

int lmk03000_get_ld()
{
  if( reg_bank_get(WD2_REG_STATUS) & WD2_LMK_PLL_LOCK_MASK )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

int lmk03000_set_channel(unsigned int value)
{
  if( (value & 0x0F) > 7) return -1; /* there are only channels 0..7 */

  soft_spi_transmit(SPI_LMK_CS, value);

  return 0;
}

/******************************************************************************/

void lmk03000_set_clkout_en(unsigned int ch_nr, unsigned int clkout_en)
{
  unsigned int reg_value = WD2_LMK0_CLKOUT_EN_MASK; /* it's the same bit for all channels */

  ch_nr &= 0x07; /* there are only channels 0..7 */

  if(clkout_en)
  {
    reg_bank_set(WD2_REG_LMK_0+(ch_nr*4), &reg_value, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_LMK_0+(ch_nr*4), &reg_value, 1);
  }

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_vboost(unsigned int vboost)
{
  unsigned int reg_value = WD2_LMK9_VBOOST_MASK;

  if(vboost)
  {
    reg_bank_set(WD2_REG_LMK_9, &reg_value, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_LMK_9, &reg_value, 1);
  }

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_div4(unsigned int div4)
{
  unsigned int reg_value = WD2_LMK11_DIV4_MASK;

  if(div4)
  {
    reg_bank_set(WD2_REG_LMK_11, &reg_value, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_LMK_11, &reg_value, 1);
  }

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_loop_filter(unsigned int r4_lf, unsigned int r3_lf, unsigned int c3_c4_lf)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_13);
  reg_value &= ~(WD2_LMK13_VCO_R4_LF_MASK | WD2_LMK13_VCO_R3_LF_MASK | WD2_LMK13_VCO_C3_C4_LF_MASK | 0x0F);

  reg_value |= (r4_lf    << WD2_LMK13_VCO_R4_LF_OFS)    & WD2_LMK13_VCO_R4_LF_MASK;
  reg_value |= (r3_lf    << WD2_LMK13_VCO_R3_LF_OFS)    & WD2_LMK13_VCO_R3_LF_MASK;
  reg_value |= (c3_c4_lf << WD2_LMK13_VCO_C3_C4_LF_OFS) & WD2_LMK13_VCO_C3_C4_LF_MASK;
  reg_value |= 0x0D; /* Register Number */

  reg_bank_write(WD2_REG_LMK_13, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_oscin_freq(unsigned int oscin_freq)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_13);
  reg_value &= ~(WD2_LMK13_OSCIN_FREQ_MASK | 0x0F);

  reg_value |= (oscin_freq << WD2_LMK13_OSCIN_FREQ_OFS) & WD2_LMK13_OSCIN_FREQ_MASK;
  reg_value |= 0x0D; /* Register Number */

  reg_bank_write(WD2_REG_LMK_13, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_pll_mux(unsigned int pll_mux)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_14);
  reg_value &= ~(WD2_LMK14_PLL_MUX_MASK | 0x0F);

  reg_value |= (pll_mux << WD2_LMK14_PLL_MUX_OFS) & WD2_LMK14_PLL_MUX_MASK;
  reg_value |= 0x0E; /* Register Number */

  reg_bank_write(WD2_REG_LMK_14, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/

unsigned char lmk03000_get_pll_mux()
{
  unsigned int reg_val;

  reg_val = reg_bank_get(WD2_REG_LMK_14);

  return (unsigned char)((reg_val & WD2_LMK14_PLL_MUX_MASK) >> WD2_LMK14_PLL_MUX_OFS);
}

/******************************************************************************/

void lmk03000_set_en_fout(unsigned int en_fout)
{
  unsigned int reg_value = WD2_LMK14_EN_FOUT_MASK;

  if(en_fout)
  {
    reg_bank_set(WD2_REG_LMK_14, &reg_value, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_LMK_14, &reg_value, 1);
  }

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_en_clkout_global(unsigned int en_clkout_global)
{
  unsigned int reg_value = WD2_LMK14_EN_CLKOUT_GLOBAL_MASK;

  if(en_clkout_global)
  {
    reg_bank_set(WD2_REG_LMK_14, &reg_value, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_LMK_14, &reg_value, 1);
  }

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_powerdown(unsigned int powerdown)
{
  unsigned int reg_value = WD2_LMK14_POWERDOWN_MASK;

  if(powerdown)
  {
    reg_bank_set(WD2_REG_LMK_14, &reg_value, 1);
  }
  else
  {
    reg_bank_clr(WD2_REG_LMK_14, &reg_value, 1);
  }

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_pll_r(unsigned int pll_r)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_14);
  reg_value &= ~(WD2_LMK14_PLL_R_MASK | 0x0F);

  reg_value |= (pll_r << WD2_LMK14_PLL_R_OFS) & WD2_LMK14_PLL_R_MASK;
  reg_value |= 0x0E; /* Register Number */

  reg_bank_write(WD2_REG_LMK_14, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_pll_n(unsigned int pll_n)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_15);
  reg_value &= ~(WD2_LMK15_PLL_N_MASK | 0x0F);

  reg_value |= (pll_n << WD2_LMK15_PLL_N_OFS) & WD2_LMK15_PLL_N_MASK;
  reg_value |= 0x0F; /* Register Number */

  reg_bank_write(WD2_REG_LMK_15, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_pll_cp_gain(unsigned int pll_cp_gain)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_15);
  reg_value &= ~(WD2_LMK15_PLL_CP_GAIN_MASK | 0x0F);

  reg_value |= (pll_cp_gain << WD2_LMK15_PLL_CP_GAIN_OFS) & WD2_LMK15_PLL_CP_GAIN_MASK;
  reg_value |= 0x0F; /* Register Number       */

  reg_bank_write(WD2_REG_LMK_15, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/

void lmk03000_set_vco_div(unsigned int vco_div)
{
  unsigned int reg_value;

  reg_value = reg_bank_get(WD2_REG_LMK_15);

  reg_value |= (vco_div << WD2_LMK15_VCO_DIV_OFS) & WD2_LMK15_VCO_DIV_MASK;
  reg_value |= 0x0F; /* Register Number   */

  reg_bank_write(WD2_REG_LMK_15, &reg_value, 1);

  update_lmk();
}

/******************************************************************************/
/******************************************************************************/

unsigned short ltc2600_bin_voltage(unsigned int voltage_nv)
{
  unsigned int value;
  
  /* calculate DAC value from voltage */
  value = voltage_nv/LTC2600_RES_NV;
  if(value > 0x0000FFFF)
  {
    value = 0x0000FFFF;
  }
  
  return (unsigned short)value;
}

/******************************************************************************/

void ltc2600_set(unsigned int channel, unsigned int command, unsigned int voltage_nv)
{
  unsigned int  value;
  unsigned char data[12];

  /* calculate DAC value from voltage */
  value = ltc2600_bin_voltage(voltage_nv);

  data[0] = 0;
  data[1] = LTC2600_CMD_NOP;
  data[2] = (unsigned char)((value >> 8) & 0xFF);
  data[3] = (unsigned char)(value & 0xFF);

  for(int i=1;i<3;i++)
  {
    data[i*4+0] = data[0];
    data[i*4+1] = data[1];
    data[i*4+2] = data[2];
    data[i*4+3] = data[3];
  }
  data[(4*((23-channel)/8))+1] = command | (channel%8);

  soft_spi_transmit(SPI_DAC_CS, data, 12);
}

/******************************************************************************/

void ltc2600_write(unsigned int channel, unsigned int command, unsigned int voltage)
{
  unsigned char data[12];

  data[0] = 0;
  data[1] = LTC2600_CMD_NOP;
  data[2] = (unsigned char)((voltage >> 8) & 0xFF);
  data[3] = (unsigned char)(voltage & 0xFF);

  for(int i=1;i<3;i++)
  {
    data[i*4+0] = data[0];
    data[i*4+1] = data[1];
    data[i*4+2] = data[2];
    data[i*4+3] = data[3];
  }
  data[(4*((23-channel)/8))+1] = command | (channel%8);

  soft_spi_transmit(SPI_DAC_CS, data, 12);
}

/******************************************************************************/

void ltc2600_init()
{
  int i;

  /* Set values for each channel */
  for(i=0;i<24;i++)
  {
    ltc2600_set(23-i, LTC2600_CMD_WR_N, LTC2600_INIT_NV[i]);
  }

  /* Power Up all channels */
  ltc2600_set(LTC2600_DAC_ADR_ALL, LTC2600_CMD_PU_N, 0);
}

/******************************************************************************/
/******************************************************************************/
