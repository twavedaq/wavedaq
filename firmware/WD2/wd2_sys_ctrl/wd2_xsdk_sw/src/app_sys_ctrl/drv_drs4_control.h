/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  19.08.2015 15:06:44
 *
 *  Description :  Software interface for the DRS4 control signals.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __GPIO_DRS4_CONTROL_H__
#define __GPIO_DRS4_CONTROL_H__

#include "register_map_wd2.h"

#define DRS4_DEVICE_A    0
#define DRS4_DEVICE_B    1

#define DRS4_CFG_WSRLOOP   WD2_DRS_WSRLOOP_MASK
#define DRS4_CFG_PLLEN     WD2_DRS_PLLEN_MASK
#define DRS4_CFG_DMODE     WD2_DRS_DMODE_MASK

/************************************************************/

void drs4_init(void);

/* DRS4 configuration functions */
int drs4_get_pll_lock(unsigned int device);
void drs4_init_rsr();
void drs4_set_cfr(unsigned int value);
void drs4_clr_cfr(unsigned int value);
void drs4_set_wsr(unsigned char value);
void drs4_set_wcr(unsigned char value);
void drs4_set_valid_delay(unsigned char delay);
void drs4_start_daq();
void drs4_soft_trigger();
void drs4_fsm_reset();

#endif /* __GPIO_DRS4_CONTROL_H__ */
