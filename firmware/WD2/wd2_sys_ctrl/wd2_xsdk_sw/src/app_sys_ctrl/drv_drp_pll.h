/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e (Author of generation script)
 *  Created :  11.08.2016 12:36:32
 *
 *  Description :  Register map definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRP_PLL_CMD_H__
#define __DRP_PLL_CMD_H__

#include <stdio.h>
#include "xfs_types.h"
#include "drp_pll_param.h"

#define CLKFB_SRC_CLKFBOUT   0
#define CLKFB_SRC_CLKOUT0    1

#define PLL_BANDWIDTH_HIGH   0
#define PLL_BANDWIDTH_LOW    1

#define DRP_CHANNEL_NONE     0
#define DRP_CHANNEL_CLK0     1
#define DRP_CHANNEL_CLK1     2
#define DRP_CHANNEL_CLK2     3
#define DRP_CHANNEL_CLK3     4
#define DRP_CHANNEL_CLK4     5
#define DRP_CHANNEL_CLK5     6
#define DRP_CHANNEL_CLKFB    7
#define DRP_CHANNEL_DIVCLK   8

typedef struct
{
  xfs_u32 base_address;
  xfs_u32 clkin_frequency;
  xfs_u32 clkfb_source;
  xfs_u32 vco_frequency;
  xfs_u8  pll_bandwidth;
} drp_pll_type;
  
void drp_pll_init(drp_pll_type* self, xfs_u32 b_address, xfs_u32 f_clkin_mhz, xfs_u32 clkfb_src, xfs_u8 pll_bw);
unsigned int drp_pll_set_bandwidth(drp_pll_type* self, unsigned char bandwidth);
unsigned int drp_pll_set_phase_step(drp_pll_type* self, unsigned int channel, unsigned int phase_step);
unsigned int drp_pll_get_phase_step(drp_pll_type* self, unsigned int channel, unsigned int &phase_step);
unsigned int drp_pll_set_phase(drp_pll_type* self, unsigned int channel, unsigned int phase);
unsigned int drp_pll_get_phase(drp_pll_type* self, unsigned int channel, unsigned int &phase);
unsigned int drp_pll_get_phase_steps(drp_pll_type* self, unsigned int channel);
unsigned int drp_pll_set_divider_and_dutycycle(drp_pll_type* self, unsigned int channel, unsigned int divider, unsigned int dutycycle_percent);
unsigned int drp_pll_get_divider_and_dutycycle(drp_pll_type* self, unsigned int channel, unsigned int& divider, unsigned int& dutycycle_percent);
int drp_pll_change_fin(drp_pll_type* self, unsigned int fin_new_mhz);
int drp_pll_reset_pll(drp_pll_type* self, xfs_u32 value);
unsigned int drp_pll_is_locked(drp_pll_type* self);

/******************************************************************************/

#endif /* __DRP_PLL_CMD_H__ */

/******************************************************************************/