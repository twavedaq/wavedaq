/******************************************************************************/
/*                                                                            */
/*  file: plb_ll_testgen.h                                                    */
/*                                                                            */
/*  (c) 2011 PSI tg32                                                         */
/*                                                                            */
/******************************************************************************/

#ifndef __PLB_LL64_TESTGEN_H__
#define __PLB_LL64_TESTGEN_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "xparameters.h"
#include "stdio.h"
#include "xutil.h"

/******************************************************************************/
/* plb_ll_testgen                                                             */
/******************************************************************************/

#define PLB_LL64_TESTGEN_REG_CTRL             0
#define PLB_LL64_TESTGEN_REG_HEADER           1
#define PLB_LL64_TESTGEN_REG_COUNT_IFG        2
#define PLB_LL64_TESTGEN_REG_COUNT_TOTAL      3
#define PLB_LL64_TESTGEN_REG_LOOP_SERV_NUM    4
#define PLB_LL64_TESTGEN_REG_DATA_LOOP        5
#define PLB_LL64_TESTGEN_REG_SERVER_NUM       6
#define PLB_LL64_TESTGEN_REG_STATUS           7

#define PLB_LL64_TESTGEN_CTRL_RESET          0x00000001
#define PLB_LL64_TESTGEN_CTRL_STOP           0x00000002
#define PLB_LL64_TESTGEN_CTRL_START          0x00000004
#define PLB_LL64_TESTGEN_CTRL_SEL_12         0x00000008

#define PLB_LL64_TESTGEN_STAUS_RUNNING       0x80000000


typedef struct 
{
    unsigned int base_addr;
} plb_ll_testgen_type;



void plb_ll_testgen_init(plb_ll_testgen_type *self, unsigned int base_addr);
void plb_ll_testgen_reset(plb_ll_testgen_type *self);
void plb_ll_testgen_stop(plb_ll_testgen_type *self);
int  plb_ll_testgen_running(plb_ll_testgen_type *self);
int  plb_ll_testgen_start(plb_ll_testgen_type *self, 
           unsigned int header,
           unsigned int data_len,
           unsigned int total_frames,
           unsigned int server_num,
           unsigned int frames_per_server,
           unsigned int ifg,
           unsigned int sel_12
          );


/******************************************************************************/
#endif /* __PLB_LL64_TESTGEN_H__ */
/******************************************************************************/

