/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  07.05.2014 12:23:47
 *
 *  Description :  Software controlled shift register.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "register_map_wd2.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "xtime.h"
#include "gpio_shift_register.h"

/******************************************************************************/

/****************************************************************/
/* Shift register variables                                     */
/****************************************************************/
/*   Bit layout:                                                */
/*   D9    D8    D7    D6    D5    D4    D3    D2    D1    D0   */
/*   ACDC  COMP2 OP2   COMP1 OP1   RSRVD ATT1  ATT0  CAL1  CAL0 */
/*   ^MSB                                      ^LSB             */
/****************************************************************/

void gpio_sreg_init(void)
{
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, GPIO_SREG_DI25_O | GPIO_SREG_ST25_O | GPIO_SREG_CLK25_O);
  plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, GPIO_SREG_DI33_O | GPIO_SREG_ST33_O | GPIO_SREG_CLK33_O);

  /* Load default value to shift registers */
  /* frontend_setting_apply();  */ /* is called by system.cpp via update_configurations */
}

/******************************************************************************/

void gpio_sreg_clk(unsigned int reg_sel, unsigned int val)
{
  unsigned char sregclk;

  if (reg_sel == SREG25)
  {
    sregclk = GPIO_SREG_CLK25_O;
  }
  else
  {
    sregclk = GPIO_SREG_CLK33_O;
  }

  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, sregclk );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, sregclk );
  }
}

/******************************************************************************/

void gpio_sreg_store(unsigned int reg_sel, unsigned int val)
{
  unsigned char sreg_st;

  if (reg_sel == SREG25)
  {
    sreg_st = GPIO_SREG_ST25_O;
  }
  else
  {
    sreg_st = GPIO_SREG_ST33_O;
  }

  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, sreg_st );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, sreg_st );
  }
}

/******************************************************************************/

void gpio_sreg_di(unsigned int reg_sel, unsigned int val)
{
  unsigned char sreg_di;

  if (reg_sel == SREG25)
  {
    sreg_di = GPIO_SREG_DI25_O;
  }
  else
  {
    sreg_di = GPIO_SREG_DI33_O;
  }

  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, sreg_di );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SHIFT_REGISTER, sreg_di );
  }
}

/******************************************************************************/

void gpio_sreg_wait()
{
  usleep(10);
}

/******************************************************************************/

void gpio_sreg_transmit(unsigned int reg_sel, unsigned int first_bit, int nr_of_bits)
{
  unsigned int reg_val;
  unsigned int value;
  unsigned int mod_flag_mask;
  int i,j,k;

/*
*  Format :
*           D8 D7 D6 D5 D4 D3 D2 D1 D0
*           ^first                  ^last
*/

  gpio_sreg_store(reg_sel, 0);

  if(reg_sel == SREG25)
  {
    mod_flag_mask = 0x00000005;
  }
  else /* reg_sel == SREG33 */
  {
    mod_flag_mask = 0x0000000A;
  }

  for (i=7; i>=0; i--)
  {
    reg_bank_write(WD2_REG_FE_0_15_MOD_FLAG, &mod_flag_mask, 1); /* clears modified flag !!! */
    reg_val = reg_bank_get(WD2_REG_FE_CFG_0_1 + i*0x04);
    /* invert pole-zero cancellation bits */
    reg_val ^= (WD2_FE0_PZC_EN_MASK | WD2_FE1_PZC_EN_MASK); /* same bits for other channels */

    for (k=0; k<=1; k++)
    {
      reg_val = reg_val >> (k*16);
      value = (reg_val << (15-first_bit));
  
      for (j=0; j<nr_of_bits; j++)
      {
        gpio_sreg_clk(reg_sel, 0);
        gpio_sreg_di(reg_sel, value & 0x8000);
        gpio_sreg_wait();
        gpio_sreg_clk(reg_sel, 1);
        gpio_sreg_wait();
        value <<= 1;
      }
    }
    mod_flag_mask <<= 4;
  }
  gpio_sreg_clk(reg_sel, 0);
  gpio_sreg_wait();
  gpio_sreg_store(reg_sel, 1);
  gpio_sreg_wait();
  gpio_sreg_store(reg_sel, 0);
}

/******************************************************************************/

void frontend_setting_apply()
{
  gpio_sreg_transmit(SREG25, 3, 4);
  gpio_sreg_transmit(SREG33, 8, 5);
}

/******************************************************************************/
/******************************************************************************/
