/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  15.03.2017 11:11:11
 *
 *  Description :  Support for side data handling in WDB
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/***************************** Include Files *******************************/

#include <stdlib.h>
#include "side_data.h"
#include "xfs_printf.h"
#include "system.h"
#include "wd2_com.h"

/************************** Variable Definitions ***************************/


/************************** Function Definitions ***************************/

int print_side_data_bank_def()
{
  int nr_of_valid_banks = 0;
  
  xfs_local_printf("\r\nSide Data Bank Definitions:\r\n");
  for(int i=0;i<MAX_SIDE_DATA_BANKS;i++)
  {
    xfs_local_printf("Bank %d: ", i);
    if(SYSPTR(cfg)->side_data[i].mem_length)
    {
      xfs_local_printf("%c%c%c%c   ",     SYSPTR(cfg)->side_data[i].id[0], SYSPTR(cfg)->side_data[i].id[1], SYSPTR(cfg)->side_data[i].id[2], SYSPTR(cfg)->side_data[i].id[3]);
      xfs_local_printf("start 0x%08X   ", SYSPTR(cfg)->side_data[i].mem_offset);
      xfs_local_printf("length %4d   ",   SYSPTR(cfg)->side_data[i].mem_length);
      xfs_local_printf("type %c   ",      SYSPTR(cfg)->side_data[i].trigger_type);
      xfs_local_printf("period %4d\r\n",  SYSPTR(cfg)->side_data[i].trigger_period);
      nr_of_valid_banks++;
    }
    else
    {
      xfs_local_printf("NONE\r\n");
    }
  }
  
  return nr_of_valid_banks;
}

/***************************************************************************/

int side_data_timer(unsigned int time)
{
  static unsigned int prev_time[MAX_SIDE_DATA_BANKS] = {0,0,0,0,0}; /* change for variable number of banks (init) */
  side_data_bank_type* side_data_to_send[MAX_SIDE_DATA_BANKS];
  int nr_of_banks = 0;
  
  /* go through side data defitions and check time */
  for(int i=0;i<MAX_SIDE_DATA_BANKS;i++)
  {
    if( (SYSPTR(cfg)->side_data[i].trigger_type   == 't') && 
        (SYSPTR(cfg)->side_data[i].trigger_period <= time - prev_time[i]) )
    {
      /*xfs_local_printf("timer up for %s, time %ds, prev time %ds\r\n", SYSPTR(cfg)->side_data[i].id, time, prev_time[i]); */
      side_data_to_send[nr_of_banks] = &(SYSPTR(cfg)->side_data[i]);
      nr_of_banks++;
      prev_time[i] = time;
    }
  }

  /* if time is up for at least one bank, transmit it */
  if(nr_of_banks)
  {
    /*xfs_local_printf("transmitting %d banks...\r\n", nr_of_banks); */
    wd2_side_data_frame(SYSPTR(nw_if_gmac_0), side_data_to_send, nr_of_banks);
  }
  
  return nr_of_banks;
}

/***************************************************************************/

int side_data_event()
{
  static unsigned int event_counter[MAX_SIDE_DATA_BANKS] = {0,0,0,0,0}; /* change for variable number of banks (init) */
  side_data_bank_type* side_data_to_send[MAX_SIDE_DATA_BANKS];
  int nr_of_banks = 0;
  
  /* go through side data defitions and check events */
  for(int i=0;i<MAX_SIDE_DATA_BANKS;i++)
  {
    if( (SYSPTR(cfg)->side_data[i].trigger_type   == 'e') && 
        (SYSPTR(cfg)->side_data[i].trigger_period < ++event_counter[i]) )
    /*if( (SYSPTR(cfg)->side_data[i].trigger_type   == 'e'))  */
    {
      side_data_to_send[nr_of_banks] = &(SYSPTR(cfg)->side_data[i]);
      nr_of_banks++;
      event_counter[i] = 0;
    }
  }

  /* if any bank reached their goal, transmit them */
  if(nr_of_banks)
  {
    wd2_side_data_frame(SYSPTR(nw_if_gmac_0), side_data_to_send, nr_of_banks);
  }

  return nr_of_banks;
}

/***************************************************************************/

int parse_side_data(const char *str, side_data_bank_type *side_data)
{
  int i, j;
  int count;
  int valid = 1;
  char *sd_str[5];

  if (!str) return 1;

  /* initialize side data bank definitions */
  for (i=0; i<MAX_SIDE_DATA_BANKS; i++)
  {
    for(j=0; j<5; j++) side_data[i].id[j] = '\0';
    side_data[i].mem_offset     = 0;
    side_data[i].mem_length     = 0;
    side_data[i].trigger_type   = '\0';
    side_data[i].trigger_period = 0;
  }
  
  for (i=0; i<MAX_SIDE_DATA_BANKS && *str ; i++)
  {
    sd_str[0] = (char*)str;
    count = 0;
    while ( *str && count<=5 )
    {
      if (*str == ';')
      {
        ++str;
        break;
      }
      else if ( *str=='\t' || *str== ' ' )
      {
         ++str;
      }
      else if (*str == ',')
      {
        if (++count < 5)
        {
          sd_str[count] = (char*)++str;
        }
      }
      else if ( (*str>='0' &&  *str<='9') || (*str>='a' &&  *str<='z') || (*str>='A' &&  *str<='Z') )
      {
        ++str;
      }
      else
      {
        count = 999;
        while(*str && *str++!=';');
      }
    }
    
    if(count >= 2 && count < 999)
    {
      for(j=0; j<4; j++)
      {
        if(sd_str[0][j]!=',') side_data[i].id[j] = sd_str[0][j];
      }
      side_data[i].mem_offset     = strtoul(sd_str[1], NULL, 0);
      side_data[i].mem_length     = strtoul(sd_str[2], NULL, 0);
    }

    if(count == 2 || count == 3)
    {
      side_data[i].trigger_type = 'm';
    }
    else if(count == 4)
    {
      side_data[i].trigger_type   = *sd_str[3];
      side_data[i].trigger_period = strtoul(sd_str[4], NULL, 0);
    }
    else
    {
      valid = 0;
    }

  }

  /*
  xfs_local_printf("Parsing result of: %s\r\n",sd_str[0]);
  for (i=0; i<MAX_SIDE_DATA_BANKS; i++)
  {
    xfs_local_printf("%2d: id=%s   ofs=0x%08X   len=%d   type=%d   per=%d\r\n", 
               i,
               side_data[i].id, side_data[i].mem_offset,
               side_data[i].mem_length,
               side_data[i].trigger_type,
               side_data[i].trigger_period);
  }
  */

  if(valid) return 0;
  else      return 1;
}

/******************************************************************************/
/***************************************************************************/
