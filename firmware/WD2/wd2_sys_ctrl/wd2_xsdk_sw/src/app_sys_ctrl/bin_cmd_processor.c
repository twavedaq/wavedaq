/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Processing the commands entered via the terminal command line
 *                 using the modular command implementation.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "bin_cmd_processor.h"
#include "xfs_printf.h"
#include <stdlib.h>
#include "utilities.h"
#include "system.h"
#include "plb_spi_slave.h"
#include "dbg.h"
#include "register_map_wd2.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#ifdef WD2_REV_F
#include "drv_soft_spi_adc_ad9637.h"
#else
#include "drv_soft_spi_adc_ltm9009.h"
#endif
#include "update_config.h"
#include "gpio_slow_control.h"

/*#include "wd2_config.h" */

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/


/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

int wd2_udp_bincmd_handle_request(network_if_type *nw_if, unsigned char *frame, int frame_len)
{
  int udp_payload_len;
  int udp_reply_payload_len;
  int max_payload_len;
  char *udp_payload_ptr;
  udp_header_type *udp_fp;
  unsigned char dst_mac[6];
  unsigned char dst_ip[4];
  unsigned char dst_port[2];

  udp_payload_ptr = (char *)udp_payload_ptr_get(frame);
  udp_payload_len = udp_payload_len_get(frame);
  udp_payload_ptr[udp_payload_len] = 0;
  udp_fp = (udp_header_type *)frame;
  max_payload_len = nw_if->com_buff_len-UDP_HEADER_LEN;



  fcp(dst_mac, udp_fp->src_mac);
  fcp(dst_ip, udp_fp->src_ip);
  fcp(dst_port, udp_fp->src_port);

  /*if (DBG_INFO) xfs_local_printf("\r\nUDP Received %d bytes: 0x%02X%02X%02X%02X 0x%02X%02X%02X%02X\r\n", udp_payload_len, udp_payload_ptr[0], udp_payload_ptr[1], udp_payload_ptr[2], udp_payload_ptr[3], udp_payload_ptr[4], udp_payload_ptr[5], udp_payload_ptr[6], udp_payload_ptr[7], udp_payload_ptr[8]); */

  udp_reply_payload_len = bin_cmd_process(udp_payload_ptr, udp_payload_len, max_payload_len);

  if(udp_reply_payload_len)
  {
    prepare_udp_frame(nw_if, udp_fp);
    udp_fp->src_port[0] = UDP_BINARY_CMD_PORT >> 0x08;
    udp_fp->src_port[1] = UDP_BINARY_CMD_PORT  & 0xff;
    udp_fp->dst_port[0] = dst_port[0];
    udp_fp->dst_port[1] = dst_port[1];

    /* limit output to max term buffer size */
    if (udp_reply_payload_len > max_payload_len) udp_reply_payload_len = max_payload_len;

    /* insert mac and ip address */
    fcp(udp_fp->dst_mac, dst_mac);
    fcp(udp_fp->dst_ip , dst_ip);

    udp_send_frame(nw_if, udp_fp, udp_reply_payload_len);
  }

  return 1;
}

/****************************************************************************************************/

int bin_cmd_process(char *buffer_i, int len, int max_payload_len)
{
  int nr_of_words;
  unsigned int cmd;
  int nr_of_read_bytes = 0;
  unsigned int offset;
  unsigned int mem_val;
  unsigned char *buffer_ptr;
  int reply_len;

  gpio_sc_flash_sw_status(GPIO_LED_SW_STATUS_WDB_ACCESS);

  buffer_ptr = (unsigned char*)buffer_i;

  cmd = buffer_ptr[INDEX_CMD];
  /*implicitly set: seq_nr = buffer_ptr[INDEX_CMD_ACK_SEQ_NR] & BINCMD_SEQ_NR_MASK; */
  offset = 0;
  for(int i=0;i<4;i++)
  {
    offset <<= 8;
    offset |= buffer_ptr[INDEX_ADR+i];
  }

  if(cmd == BINCMD_READ)
  {
    nr_of_read_bytes = 0;
    for(int i=0;i<4;i++)
    {
      nr_of_read_bytes <<= 8;
      nr_of_read_bytes |= buffer_ptr[INDEX_NR_OF_BYTES+i];
    }
    nr_of_read_bytes -= nr_of_read_bytes%4;
  }

  if(DBG_INFO) xfs_local_printf("\r\nReceived binary command 0x%02X%02X%02X%02X 0x%08X\r\n", cmd, buffer_ptr[INDEX_ACK], buffer_ptr[INDEX_SEQ_NR], buffer_ptr[INDEX_SEQ_NR+1], offset);

  /* add acknowledge bit for reply (read and write) */
  buffer_ptr[INDEX_ACK] |= BINCMD_ACK;

  switch ( cmd )
  {
    case BINCMD_WRITE:
      if( len < 8 ) len = 8;
      nr_of_words = (len-8)/4;
      if(DBG_INFO) xfs_local_printf("writing %d words starting from address 0x%08X...\r\n", nr_of_words, offset);
      for(int j=0;j<nr_of_words;j++)
      {
        mem_val = 0;
        for(int i=0;i<4;i++)
        {
          mem_val <<= 8;
          mem_val |= buffer_ptr[INDEX_WRITE_DATA+j*4+i];
        }
        reg_bank_write(offset, &mem_val, 1);
        /* To access absolute memory addresses use: */
        /* xfs_out32(offset, mem_val); */
        if(DBG_INFO) xfs_local_printf("[0x%08X]: 0x%08X\r\n", offset, mem_val);
        offset += 4;
      }
      reply_len = 4;
      if(DBG_INFO) xfs_local_printf("done\r\n");
      break;
    case BINCMD_READ:
      /* check buffer size !!! */
      if (nr_of_read_bytes > (max_payload_len - 12)) nr_of_read_bytes = max_payload_len - 12;
      nr_of_words = nr_of_read_bytes/4;
      if(DBG_INFO) xfs_local_printf("reading %d words starting from address 0x%08X...\r\n", nr_of_words, offset);
      for(int j=0;j<nr_of_words;j++)
      {
        reg_bank_read(offset, &mem_val, 1);
        /* To access absolute memory addresses use: */
        /* mem_val = xfs_in32(offset); */
        if(DBG_INFO) xfs_local_printf("[0x%08X]: 0x%08X\r\n", offset, mem_val);
        for(int i=3;i>=0;i--)
        {
          buffer_ptr[INDEX_READ_DATA+j*4+i] = (char)(mem_val & 0xFF);
          mem_val >>= 8;
        }
        offset += 4;
      }
      reply_len = nr_of_read_bytes+4;
      if(DBG_INFO) xfs_local_printf("done\r\n");
      break;
    default:
      if(DBG_ERR)  xfs_local_printf("\r\nError: unknown binary command 0x%08X\r\n\r\n", cmd);
      reply_len = 0;
      break;
  }

  return reply_len;
}

/******************************************************************************/
/******************************************************************************/
