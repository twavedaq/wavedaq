/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  05.05.2014 14:11:28
 *
 *  Description :  Module for SPI access to LMK03000 configurable pll clock
 *                 distributor.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_soft_spi_temp_dac_lmk.h"
#include "system.h"
#include "drv_plb_wd2_reg_bank.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "cmd_processor.h"
#include "update_config.h"
#include "register_map_wd2.h"
#include <stdlib.h>


/************************************************************/

  int lmk_reset(int argc, char **argv)
  {
    CMD_HELP("",
             "resets the lmk register settings",
             "Resets the LMK register content."  
            );

    lmk03000_reset();

    return 0;
  }

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_channel(int argc, char **argv)
  {
    unsigned int ch_nr;
    unsigned int mux;
    unsigned int divider;
    unsigned int delay;
    unsigned int enable;
    unsigned int reg_value = 0;

    CMD_HELP("<ch> <mux> <div> <dly> <en>",
             "apply settings for specific channel",
             "Sets the configuration for one of the LMK channels.\r\n"
             "Changes are directly applied to the LMK.\r\n"
             "  <ch>  : channel (range = 0..7)\r\n"
             "  <mux> : clock output mux   0 = bypassed, 1 = divided, \r\n"
             "                             2 = delayed,  3 = divided and delayed\r\n"
             "  <div> : channel clock divider value (range = 2..510)\r\n"
             "                                      (even values only)\r\n"
             "  <dly> : channel output delay value in ps (range = 0..2250)\r\n"
             "                                           (resolution 150ps)\r\n"
             "  <en>  : channel enable  0 = disabled, 1 = enabled\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 6)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    ch_nr   = xfs_atoi(argv[1]);
    mux     = xfs_atoi(argv[2]);
    divider = xfs_atoi(argv[3]) / 2;
    delay   = xfs_atoi(argv[4]) / 150;
    enable  = xfs_atoi(argv[5]);

    /* Validate arguments range */
    if( (ch_nr > 7) || (mux > 3) || (divider == 0) || (delay > 15) )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmksetch help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    /* Setup the LMK channel */
    reg_value |= (mux     << WD2_LMK0_CLKOUT_MUX_OFS) & WD2_LMK0_CLKOUT_MUX_MASK;
    reg_value |= (enable  << WD2_LMK0_CLKOUT_EN_OFS)  & WD2_LMK0_CLKOUT_EN_MASK;
    reg_value |= (divider << WD2_LMK0_CLKOUT_DIV_OFS) & WD2_LMK0_CLKOUT_DIV_MASK;
    reg_value |= (delay   << WD2_LMK0_CLKOUT_DLY_OFS) & WD2_LMK0_CLKOUT_DLY_MASK;
    reg_value |= (ch_nr   & 0x0F); /* Register Number */

    reg_bank_write(WD2_REG_LMK_0+(ch_nr*4), &reg_value, 1);

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_clkout_en(int argc, char **argv)
  {
    unsigned int ch_nr;
    unsigned int enable;

    CMD_HELP("<channel_number>  on | off",
             "enables or disables one of the LMK channel outputs",
             "Enables or disables one of the LMK channel outputs.\r\n"
             "Changes are directly applied to the LMK.\r\n"
             "  <channel_number>:   selects channel (range = 0..7)\r\n"
             "  on | off:           channel setting on or off\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    
    ch_nr   = xfs_atoi(argv[1]);

    if(fstrcmp(argv[2],"on"))
    {
      enable = 1;
    }
    else if(fstrcmp(argv[2],"off"))
    {
      enable = 0;
    }
    else
    {
      enable = 2;
    }

    /* Validate argument range */
    if( (ch_nr > 7) || (enable == 2) )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmkchen help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    lmk03000_set_clkout_en(ch_nr, enable);

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_loop_filter(int argc, char **argv)
  {
    unsigned int r4_lf;
    unsigned int r3_lf;
    unsigned int c3_c4_lf;

    r3_lf    = xfs_atoi(argv[1]);
    r4_lf    = xfs_atoi(argv[2]);
    c3_c4_lf = xfs_atoi(argv[3]);

    CMD_HELP("<R3> <R4> <C3_C4>",
             "sets the component values for the internal loop filter",
             "Sets the component values for the internal loop filter.\r\n"
             "See LMK datasheet for details. Changes are directly applied to the LMK.\r\n"  
             "  <R3>    : resistor value ID for R3 (range = 0..4)\r\n"
             "  <R4>    : resistor value ID for R4 (range = 0..4)\r\n"
             "  <C3_C4> : capacitor value ID for C3 and C4 (range = 0..11)\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 4)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    /* Validate argument range */
    if( (r3_lf > 4) || (r4_lf > 4) || (c3_c4_lf > 11) )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmkchen help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    lmk03000_set_loop_filter(r4_lf, r3_lf, c3_c4_lf);

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_oscin_freq(int argc, char **argv)
  {
    unsigned int oscin_freq;

    oscin_freq = xfs_atoi(argv[1]);

    CMD_HELP("<frequency>",
             "sets the oscillator input frequency of the LMK",
             "Sets the oscillator input frequency of the LMK.\r\n"
             "Changes are directly applied to the LMK.\r\n"
             " <frequency> : oscillator frequency in multiples of 1 MHz\r\n"
             "               (range = 1..200MHz)\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    /* Validate argument range */
    if(oscin_freq > 200)
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmkoif help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    lmk03000_set_oscin_freq(oscin_freq);

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_pll(int argc, char **argv)
  {
    unsigned int pll_r;
    unsigned int pll_n;
    unsigned int vco_div;
    unsigned int pll_mux;
    unsigned int pll_cp_gain;

    CMD_HELP("<r_div> <n_div> <vco_div> <pll_mux> <cp_gain>",
             "sets the pll configuration parameters of the LMK",
             "Sets the pll configuration parameters of the LMK.\r\n"
             "See LMK datasheet for details.\r\n"
             "Changes are directly applied to the LMK.\r\n"
             "  <r_div>   : R divider value (range = 1..4095)\r\n"
             "  <n_div>   : N divider value (range = 1..262143)\r\n"
             "  <vco_div> : VCO divider value (range = 2..8)\r\n"
             "  <pll_mux> : output mode of the LD pin (range = 0..11)\r\n"
             "  <cp_gain> : charge pump gain (range = 0..3)\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 6)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    pll_r       = xfs_atoi(argv[1]);
    pll_n       = xfs_atoi(argv[2]);
    vco_div     = xfs_atoi(argv[3]);
    pll_mux     = xfs_atoi(argv[4]);
    pll_cp_gain = xfs_atoi(argv[5]);

    /* Validate arguments range */
    if( (pll_r < 1)    || (pll_r > 4095)    ||
        (pll_n < 1)    || (pll_n > 262143)  ||
        (vco_div < 2)  || (vco_div > 8)     ||
        (pll_mux > 11) || (pll_cp_gain > 3) )
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmksetch help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    /* Setup the LMK pll */
    lmk03000_set_pll_mux(pll_mux);
    lmk03000_set_pll_cp_gain(pll_cp_gain);
    lmk03000_set_vco_div(vco_div);
    lmk03000_set_pll_r(pll_r);
    lmk03000_set_pll_n(pll_n);

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set(int argc, char **argv)
  {
    unsigned char enable;

    CMD_HELP("<identifier>  on | off",
             "sets single bit register flags",
             "sets single bit register flags.\r\n"
             "Changes are directly applied to the LMK.\r\n"
             "  <identifier> : configuration bit id (vboost, div4, fout, clkoutglobal, pwrdn)\r\n"
             "  on | off     : configuration bit setting on or off\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(fstrcmp(argv[2],"on"))
    {
      enable = 1;
    }
    else if(fstrcmp(argv[2],"off"))
    {
      enable = 0;
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmkset help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    if(fstrcmp(argv[1],"vboost"))
    {
      lmk03000_set_vboost(enable);
    }
    else if(fstrcmp(argv[1],"div4"))
    {
      lmk03000_set_div4(enable);
    }
    else if(fstrcmp(argv[1],"fout"))
    {
      lmk03000_set_en_fout(enable);
    }
    else if(fstrcmp(argv[1],"clkoutglobal"))
    {
      lmk03000_set_en_clkout_global(enable);
    }
    else if(fstrcmp(argv[1],"pwrdn"))
    {
      lmk03000_set_powerdown(enable);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"lmkset help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }
#endif

/************************************************************/

  int lmk_configure(int argc, char **argv)
  {
    CMD_HELP("",
             "configure LMK with register bank settings"
            );


    xfs_printf("Configuring LMK\r\n\r\n");
    update_lmk();

    return 0;
  }

/************************************************************/

  int lmk_upload(int argc, char **argv)
  {
    unsigned int reg_val;
    unsigned int reg_offset;
    unsigned char reg_nr[16] = {0,1,2,3,4,5,6,7,8,9,10,10,11,11,12,13};
    int i,j;

    CMD_HELP("<lmk_configuration_file>",
             "tranmitts a complete configuration set to the  LMK",
             "sets the lmk registers in the register bank and tranmits\r\n"
             "the contents of the text file generated by the lmk tool to the LMK.\r\n"
             "Note: linebreaks have to be replaced by a blank space in the file.\r\n"
             "  <lmk_configuration_file> : txt file exportet from LMK tool\r\n"
             "                             containing the configuration of all registers\r\n"
            );

    for(i=3;i<17;i++)
    {
      j = 0;
      while( (argv[i][j]!='x') && (argv[i][j]!='X') )
      {
        j++;
      }
      j++;
      reg_val = hatoi((const char*)(&argv[i][j]));
      reg_offset = WD2_REG_LMK_0 + reg_nr[reg_val&0x0F] * 4;
      xfs_printf("Setting LMK Control Register [0x%04X]: 0x%08X\r\n", reg_offset, reg_val);
      reg_bank_write(reg_offset, &reg_val, 1);
    }
    xfs_printf("Uploading contents to LMK\r\n");
    update_lmk();
    xfs_printf("\r\n");

    return 0;
  }

/************************************************************/

  int lmk_sync(int argc, char **argv)
  {
    CMD_HELP("",
             "Triggers a double shot pulse at the SYNC input of the LMK"  
            );

    lmk03000_sync();

    return 0;
  }

/************************************************************/

  int lmk_get_lock(int argc, char **argv)
  {
    unsigned char pll_mux;
    unsigned char ld_state;

    CMD_HELP("",
             "reads the LOCK output of the LMK"
            );


    pll_mux  = lmk03000_get_pll_mux();
    ld_state = lmk03000_get_ld();

    if( ( (pll_mux == 3) && (ld_state == 1) ) ||
        ( (pll_mux == 4) && (ld_state == 0) ) )
    {
      xfs_printf("LMK PLL Locked\r\n");
    }
    else if( ( (pll_mux == 3) && (ld_state == 0) ) ||
             ( (pll_mux == 4) && (ld_state == 1) ) )
    {
      xfs_printf("LMK PLL  NOT  Locked\r\n");
    }
    else
    {
      xfs_printf("LD pin of LMK not configured for\r\nDigital Lock Detect\r\n");
    }

    return 0;
  }

/************************************************************/

  int module_lmk_help(int argc, char **argv)
  {
    CMD_HELP("",
             "SPI LMK Module",
             "Allows access to LMK pll clock distributor configuration via SPI"  
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type lmk_cmd_table[] =
  {
    {0, "lmk", module_lmk_help},
    {0, "lmkrst", lmk_reset},
#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
    {0, "lmksetch", lmk_set_channel},
    {0, "lmkchen", lmk_set_clkout_en},
    {0, "lmklf", lmk_set_loop_filter},
    {0, "lmkoif", lmk_set_oscin_freq},
    {0, "lmksetpll", lmk_set_pll},
    {0, "lmkset", lmk_set},
#endif
    {0, "lmksync", lmk_sync},
    {0, "lmklock", lmk_get_lock},
    {0, "lmkupload", lmk_upload},
    {0, "lmkcfg", lmk_configure},
    {0, NULL, NULL}
  };

/************************************************************/
