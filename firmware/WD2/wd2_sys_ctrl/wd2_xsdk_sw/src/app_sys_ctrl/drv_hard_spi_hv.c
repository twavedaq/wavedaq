/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  18.05.2015 15:00:45
 *
 *  Description :  SPI pcore driver for communication with
 *                 HV ADC and DACs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "xtime.h"
#include "drv_hard_spi_hv.h"

/******************************************************************************/

/************************************************************/
/* variables                                                */
/************************************************************/

plb_spi_salve_settings slv_settings[8];

/* Initial ADC configuration */
unsigned int  LTC2656_ADC_INIT    = LTC2494_CMD_EN  | LTC2494_CMD_SGL    | 
                                    LTC2494_CMD_EN2 | LTC2494_CMD_RJ50HZ | LTC2494_CMD_GS1;

unsigned char LTC2494_ADC_INIT[2] = { LTC2494_CMD_EN  | LTC2494_ADR_CH0 ,
                                      LTC2494_CMD_EN2 | LTC2494_CMD_RJ50HZ | LTC2494_CMD_GS1 };

/*                             PORT       A     B  */
unsigned char MCP23S17_CONTENTS[22] = {0xFF, 0xFF,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00,
                                       0x00, 0x00};

/************************************************************/

void spi_hv_init(unsigned int base_address)
{
  mspi_init(SYSPTR(spi_hv_master), base_address, 8);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_DAC1_LV], SPI_HV_CS_DAC1_LV_O, LTC2656_CPOL  , LTC2656_CPHA  , LTC2656_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_DAC2_LV], SPI_HV_CS_DAC2_LV_O, LTC2656_CPOL  , LTC2656_CPHA  , LTC2656_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_ADC1_LV], SPI_HV_CS_ADC1_LV_O, LTC2494_CPOL  , LTC2494_CPHA  , LTC2494_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_ADC2_LV], SPI_HV_CS_ADC2_LV_O, LTC2494_CPOL  , LTC2494_CPHA  , LTC2494_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_HV_ADC] , SPI_HV_CS_HV_ADC_O , LTC2484_CPOL  , LTC2484_CPHA  , LTC2484_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_GPIO]   , SPI_HV_CS_GPIO_O   , MCP23S17_CPOL , MCP23S17_CPHA , MCP23S17_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_HV_DCDC], SPI_HV_CS_HV_DCDC_O, LTC2601_CPOL  , LTC2601_CPHA  , LTC2601_LSB_FIRST);
  mspi_slv_init(SYSPTR(spi_hv_master), &slv_settings[SLV_SETTINGS_EEPROM] , SPI_HV_CS_EEPROM_O , HV_EEPROM_CPOL, HV_EEPROM_CPHA, HV_EEPROM_LSB_FIRST);

  plb_gpio_tristate_set(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_PLUGIN_I);
}

/******************************************************************************/

void hv_ltc2656_write(plb_spi_salve_settings *slave_setting_ptr, unsigned char channel, unsigned char command, unsigned int voltage)
{
  unsigned char data[3];

  data[0] = command | channel;
  data[1] = (unsigned char)(voltage >> 4);
  data[2] = (unsigned char)(voltage << 4);

  mspi_send(slave_setting_ptr, data, 3);
  /*soft_spi_hv_transmit(slave_select, data, 3, LTC2656_CPOL, LTC2656_CPHA); */
}

/******************************************************************************/
/*
void hv_ltc2656_dac1_write(unsigned char channel, unsigned char command, float voltage)
{
  hv_ltc2656_write(SYSPTR(spi_hv_master), SLV_SETTINGS_DAC1_LV, channel, command, voltage);
}
*/
/******************************************************************************/
/*
void hv_ltc2656_dac2_write(unsigned char channel, unsigned char command, float voltage)
{
  hv_ltc2656_write(SYSPTR(spi_hv_master), SLV_SETTINGS_DAC2_LV, channel, command, voltage);
}
*/
/******************************************************************************/

void hv_ltc2656_set(plb_spi_salve_settings *slave_setting_ptr, unsigned char channel, unsigned char command, float voltage)
{
  unsigned int value;

  /*calculate DAC value from voltage */
  value = (unsigned int)(voltage/LTC2656_RES_V);

  hv_ltc2656_write(slave_setting_ptr, channel, command, value);
}

/******************************************************************************/

void hv_ltc2656_dac1_set(unsigned char channel, unsigned char command, float voltage)
{
  hv_ltc2656_set(&slv_settings[SLV_SETTINGS_DAC1_LV], channel, command, voltage);
}

/******************************************************************************/

void hv_ltc2656_dac2_set(unsigned char channel, unsigned char command, float voltage)
{
  hv_ltc2656_set(&slv_settings[SLV_SETTINGS_DAC2_LV], channel, command, voltage);
}

/******************************************************************************/

void hv_ltc2656_cfg(plb_spi_salve_settings *slave_setting_ptr, unsigned char command)
{
  unsigned char data[3];

  data[0] = command;
  data[1] = 0;
  data[2] = 0;

  mspi_send(slave_setting_ptr, data, 3);
  /*soft_spi_hv_transmit(slave_select, data, 3, LTC2656_CPOL, LTC2656_CPHA); */
}

/******************************************************************************/

void hv_ltc2656_dac1_cfg(unsigned char command)
{
  hv_ltc2656_cfg(&slv_settings[SLV_SETTINGS_DAC1_LV], command);
}

/******************************************************************************/

void hv_ltc2656_dac2_cfg(unsigned char command)
{
  hv_ltc2656_cfg(&slv_settings[SLV_SETTINGS_DAC2_LV], command);
}

/******************************************************************************/

void hv_ltc2656_init()
{
  hv_ltc2656_cfg(&slv_settings[SLV_SETTINGS_DAC1_LV], LTC2656_ADC_INIT);
  usleep(1);
  hv_ltc2656_cfg(&slv_settings[SLV_SETTINGS_DAC2_LV], LTC2656_ADC_INIT);
  usleep(1);

  /*Update Up all channels */
  hv_ltc2656_set(&slv_settings[SLV_SETTINGS_DAC1_LV], LTC2656_ADR_ALL, LTC2656_CMD_WRN_PUN, 0);
  usleep(1);
  hv_ltc2656_set(&slv_settings[SLV_SETTINGS_DAC2_LV], LTC2656_ADR_ALL, LTC2656_CMD_WRN_PUN, 0);
}

/******************************************************************************/

unsigned int hv_ltc2494_transceive(plb_spi_salve_settings *slave_setting_ptr, unsigned char *adc_config)
{
  unsigned int adc_value = 0;
  unsigned char tx_data[3];
  unsigned char rx_data[3];

  tx_data[0] = LTC2494_CMD_PREAMBLE | adc_config[0];
  tx_data[1] = adc_config[1];
  tx_data[2] = 0;

  mspi_transceive(slave_setting_ptr, NULL, 0, tx_data, rx_data, 3);
  /*soft_spi_hv_transceive(SPI_HV_CS_ADC_LV_O, tx_data, rx_data, 3, LTC2494_CPOL, LTC2494_CPHA); */

  for(int i=0;i<3;i++)
  {
    adc_value <<= 8;
    adc_value |= (unsigned int)(rx_data[i]);
  }

  return adc_value;
}

/******************************************************************************/

unsigned int hv_ltc2494_adc1_transceive(unsigned char *adc_config)
{
  return hv_ltc2494_transceive(&slv_settings[SLV_SETTINGS_ADC1_LV], adc_config);
}

/******************************************************************************/

unsigned int hv_ltc2494_adc2_transceive(unsigned char *adc_config)
{
  return hv_ltc2494_transceive(&slv_settings[SLV_SETTINGS_ADC2_LV], adc_config);
}

/******************************************************************************/

void hv_ltc2494_init()
{
  /* hv_ltc2494_adc_transceive(LTC2494_ADC_INIT); */
  /* usleep(1); */
  /* hv_ltc2494_adc_transceive(LTC2494_ADC_INIT); */
  /* usleep(1); */
  hv_ltc2494_adc1_transceive(LTC2494_ADC_INIT);
  hv_ltc2494_adc2_transceive(LTC2494_ADC_INIT);
}

/******************************************************************************/

void hv_ltc2601_write(unsigned int command, unsigned int voltage)
{
  unsigned char data[3];

  data[0] = command;
  data[1] = (unsigned char)((voltage >> 8) & 0xFF);
  data[2] = (unsigned char)(voltage & 0xFF);

  mspi_send(&slv_settings[SLV_SETTINGS_HV_DCDC], data, 3);
  /*soft_spi_hv_transmit(SPI_HV_CS_HV_DCDC_O, data, 3, LTC2601_CPOL, LTC2601_CPHA); */
}

/******************************************************************************/

void hv_ltc2601_set(unsigned int command, float voltage)
{
  unsigned int  value;

  if (voltage > LTC2484_VREF_V)
    voltage = LTC2484_VREF_V;

  if(voltage < 0)
    voltage = 0;

  /* calculate DAC value from voltage */
  value = (unsigned int)(voltage/LTC2601_RES_V+0.5);
  hv_ltc2601_write(command, value);
}

/******************************************************************************/

void hv_ltc2601_init()
{
  hv_ltc2601_set(LTC2601_CMD_WR_PU, 0); /* set output to zero (is mid-range after power-up) */
}

/******************************************************************************/

unsigned int hv_ltc2484_transceive(unsigned char adc_config)
{
  unsigned int adc_value = 0;
  unsigned char tx_data[4] = {0,0,0,0};
  unsigned char rx_data[4];

  tx_data[0] = adc_config;

  mspi_transceive(&slv_settings[SLV_SETTINGS_HV_ADC], NULL, 0, tx_data, rx_data, 4);
  /*soft_spi_hv_transceive(SPI_HV_CS_HV_ADC_O, tx_data, rx_data, 4, LTC2484_CPOL, LTC2484_CPHA); */

  for(int i=0;i<4;i++)
  {
    adc_value <<= 8;
    adc_value |= (unsigned int)(rx_data[i]);
  }

  return adc_value;
}

/******************************************************************************/

unsigned char hv_mcp23s17_transceive(unsigned char dev_opcode, unsigned char reg_address, unsigned char tx_byte)
{
  unsigned char tx_data[3] = {0,0,0};
  unsigned char rx_data[3];

  tx_data[0] = dev_opcode;
  tx_data[1] = reg_address;
  tx_data[2] = tx_byte;

  mspi_transceive(&slv_settings[SLV_SETTINGS_GPIO], NULL, 0, tx_data, rx_data, 3);
  /*soft_spi_hv_transceive(SPI_HV_CS_GPIO_O, tx_data, rx_data, 3, MCP23S17_CPOL, MCP23S17_CPHA); */

  return rx_data[2];
}

/******************************************************************************/

void hv_mcp23s17_init()
{
  /* Set output latches */
  hv_mcp23s17_write(MCP23S17_OLATA_WD2_REG_ADR, 0xFF);
  hv_mcp23s17_write(MCP23S17_OLATB_WD2_REG_ADR, 0x00);
  /* Set io directions */
  hv_mcp23s17_write(MCP23S17_IODIRA_WD2_REG_ADR, 0x00);
  hv_mcp23s17_write(MCP23S17_IODIRB_WD2_REG_ADR, MCP23S17_GPB_BOARD_A1_I | MCP23S17_GPB_BOARD_A0_I | MCP23S17_GPB_PLUGIN_I);
  /* Set configurations */
  hv_mcp23s17_write(MCP23S17_IOCONA_WD2_REG_ADR, 0x00);
  hv_mcp23s17_write(MCP23S17_IOCONB_WD2_REG_ADR, 0x00);
  /* Enable Switches */
  hv_mcp23s17_write(MCP23S17_OLATB_WD2_REG_ADR, MCP23S17_GPB_TSD_ALL_O);
}

/******************************************************************************/

unsigned char hv_mcp23s17_write(unsigned char reg_address, unsigned char tx_byte)
{
  MCP23S17_CONTENTS[reg_address] = tx_byte;
  return hv_mcp23s17_transceive(MCP23S17_CMD_WRITE, reg_address, MCP23S17_CONTENTS[reg_address]);
}

/******************************************************************************/

unsigned char hv_mcp23s17_set(unsigned char reg_address, unsigned char tx_byte)
{
  MCP23S17_CONTENTS[reg_address] |= tx_byte;
  return hv_mcp23s17_transceive(MCP23S17_CMD_WRITE, reg_address, MCP23S17_CONTENTS[reg_address]);
}

/******************************************************************************/

unsigned char hv_mcp23s17_clr(unsigned char reg_address, unsigned char tx_byte)
{
  MCP23S17_CONTENTS[reg_address] &= ~tx_byte;
  return hv_mcp23s17_transceive(MCP23S17_CMD_WRITE, reg_address, MCP23S17_CONTENTS[reg_address]);
}

/******************************************************************************/

unsigned char hv_mcp23s17_read(unsigned char reg_address)
{
  MCP23S17_CONTENTS[reg_address] = hv_mcp23s17_transceive(MCP23S17_CMD_READ, reg_address, 0);
  return MCP23S17_CONTENTS[reg_address];
}

/******************************************************************************/

void hv_eeprom_transceive(unsigned char cmd, unsigned char addr, unsigned char* tx_buffer, unsigned char* rx_buffer, unsigned int nr_of_bytes)
{
  unsigned char command[2];

  command[0] = cmd;
  command[1] = addr;

  mspi_transceive(&slv_settings[SLV_SETTINGS_EEPROM], command, 2, tx_buffer, rx_buffer, nr_of_bytes);
  /* soft_spi_hv_cmd_transceive(SPI_HV_CS_EEPROM_O, cmd, addr, tx_buffer, rx_buffer, nr_of_bytes, HV_EEPROM_CPOL, HV_EEPROM_CPHA); */
}

/******************************************************************************/

unsigned char hv_eeprom_status_get()
{
  unsigned char command;
  /* unsigned char tx_byte; */
  unsigned char rx_byte;

  command = HV_EEPROM_RDSR;
  /* tx_byte = 0; */

  mspi_transceive(&slv_settings[SLV_SETTINGS_EEPROM], &command, 1, NULL, &rx_byte, 1);
  return rx_byte;
  /* return soft_spi_hv_cmd_transceive(SPI_HV_CS_EEPROM_O, HV_EEPROM_RDSR, 0, NULL, NULL, 2, HV_EEPROM_CPOL, HV_EEPROM_CPHA); */
}

/******************************************************************************/
/******************************************************************************/
