/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.11.2015 15:47:31
 *
 *  Description :  Driver for PLB UART (non stdio).
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_PLB_UART_H__
#define __DRV_PLB_UART_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

/******************************************************************************/
/* Defines                                                                    */
/******************************************************************************/

#define UI_RX_FIFO_OFS         0  /* receive FIFO, read only */
#define UI_TX_FIFO_OFS         4  /* transmit FIFO, write only */
#define UI_STATUS_WD2_REG_OFS      8  /* status register, read only */
#define UI_CONTROL_WD2_REG_OFS    12  /* control reg, write only */

/* Control Register bit positions */

#define UI_CR_ENABLE_INTR      0x10  /* enable interrupt */
#define UI_CR_FIFO_RX_RESET    0x02  /* reset receive FIFO */
#define UI_CR_FIFO_TX_RESET    0x01  /* reset transmit FIFO */

/* Status Register bit positions */

#define UI_SR_PARITY_ERROR        0x80
#define UI_SR_FRAMING_ERROR       0x40
#define UI_SR_OVERRUN_ERROR       0x20
#define UI_SR_INTR_ENABLED        0x10  /* interrupt enabled */
#define UI_SR_TX_FIFO_FULL        0x08  /* transmit FIFO full */
#define UI_SR_TX_FIFO_EMPTY       0x04  /* transmit FIFO empty */
#define UI_SR_RX_FIFO_FULL        0x02  /* receive FIFO full */
#define UI_SR_RX_FIFO_VALID_DATA  0x01  /* data in receive FIFO */

/* The following constant specifies the size of the Transmit/Receive FIFOs.
 * The FIFO size is fixed to 16 in the Uartlite IP and the size is not
 * configurable. This constant is not used in the driver.
 */
#define UI_FIFO_SIZE   16

/* Stop bits are fixed at 1. Baud, parity, and data bits are fixed on a
 * per instance basis
 */
#define UI_STOP_BITS    1

/* Parity definitions
 */
#define UI_PARITY_NONE  0
#define UI_PARITY_ODD   1
#define UI_PARITY_EVEN  2

/******************************************************************************/
/* struct definition                                                          */
/******************************************************************************/

typedef void (*uart_intc_handler)(void *CallBackRef, unsigned int ByteCount);

typedef struct {
  unsigned char *NextBytePtr;
  unsigned int RequestedBytes;
  unsigned int RemainingBytes;
} uart_buffer;

typedef struct {
  unsigned int TransmitInterrupts;    /**< Number of transmit interrupts */
  unsigned int ReceiveInterrupts;    /**< Number of receive interrupts */
  unsigned int CharactersTransmitted;  /**< Number of characters transmitted */
  unsigned int CharactersReceived;    /**< Number of characters received */
  unsigned int ReceiveOverrunErrors;  /**< Number of receive overruns */
  unsigned int ReceiveParityErrors;  /**< Number of receive parity errors */
  unsigned int ReceiveFramingErrors;  /**< Number of receive framing errors */
} uart_stats;

typedef struct {
    unsigned int base_address;
    uart_stats stats;
    uart_buffer send_buffer;
    uart_buffer receive_buffer;
    uart_intc_handler RecvHandler;
    void *RecvCallBackRef;    /* Callback ref for recv handler */
    uart_intc_handler SendHandler;
    void *SendCallBackRef;    /* Callback ref for send handler */
} plb_uart_type;

/******************************************************************************/

void plb_uart_init(plb_uart_type *self, unsigned int base_address);
void plb_uart_enable_interrupt(plb_uart_type *self);
void plb_uart_disable_interrupt(plb_uart_type *self);
unsigned int plb_uart_send(plb_uart_type *self, unsigned char *DataBufferPtr, unsigned int NumBytes);
unsigned int plb_uart_receive(plb_uart_type *self, unsigned char *DataBufferPtr, unsigned int NumBytes);
void plb_uart_reset_fifos(plb_uart_type *self);
int plb_uart_is_sending(plb_uart_type *self);
unsigned int plb_uart_send_buffer(plb_uart_type *self);
unsigned int plb_uart_recieve_buffer(plb_uart_type *self);
void plb_uart_set_receive_handler(plb_uart_type *self, uart_intc_handler FuncPtr, void *CallBackRef);
void plb_uart_set_send_handler(plb_uart_type *self, uart_intc_handler FuncPtr, void *CallBackRef);
void plb_uart_interrupt_handler(plb_uart_type *self, void *CallBackRef);
void plb_uart_get_status(plb_uart_type *self, uart_stats *StatsPtr);
void plb_uart_clear_status(plb_uart_type *self);

/******************************************************************************/

#endif /* __DRV_PLB_UART_H__ */
