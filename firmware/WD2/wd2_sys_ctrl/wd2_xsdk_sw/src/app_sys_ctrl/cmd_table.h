/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  24.04.2014 11:53:05
 *
 *  Description :  Structure of table to list names of implemented commands that can be
 *                 interpreted plus a pointer to the function to be called by this
 *                 command.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __CMD_TABLE_H__
#define __CMD_TABLE_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

  typedef int (*cmd_func_ptr_type)(int argc, char **argv);

  typedef struct
  {
    const char         *cmd_name;
    cmd_func_ptr_type   cmd_func_ptr;
  } cmd_table_entry;

#endif /* __CMD_TABLE_H__ */
