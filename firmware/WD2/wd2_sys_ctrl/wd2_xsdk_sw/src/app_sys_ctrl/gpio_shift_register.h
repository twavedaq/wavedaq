/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  07.05.2014 12:23:51
 *
 *  Description :  Software controlled shift register.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __GPIO_SHIFT_REGISTER_H__
#define __GPIO_SHIFT_REGISTER_H__

#define  SREG25   0
#define  SREG33   1

#define  GPIO_SREG_DI25_O    0x01
#define  GPIO_SREG_ST25_O    0x02
#define  GPIO_SREG_CLK25_O   0x04
#define  GPIO_SREG_DI33_O    0x08
#define  GPIO_SREG_ST33_O    0x10
#define  GPIO_SREG_CLK33_O   0x20

/**************************************************************/
/* Shift register variables                                   */
/**************************************************************/

/*
*  Format :
*           D9    D8    D7    D6    D5    D4    D3    D2    D1    D0
*           ACDC  COMP2 OP2   COMP1 OP1   RSRVD ATT1  ATT0  CAL1  CAL0
*           ^first                                                ^last
*/

#define  FRONTEND_CFG_BIT_ACDC    0x0200
#define  FRONTEND_CFG_BIT_COMP2   0x0100
#define  FRONTEND_CFG_BIT_OP2     0x0080
#define  FRONTEND_CFG_BIT_COMP1   0x0040
#define  FRONTEND_CFG_BIT_OP1     0x0020
#define  FRONTEND_CFG_BIT_RSRVD   0x0010
#define  FRONTEND_CFG_BIT_ATT1    0x0008
#define  FRONTEND_CFG_BIT_ATT0    0x0004
#define  FRONTEND_CFG_BIT_CAL1    0x0002
#define  FRONTEND_CFG_BIT_CAL0    0x0001

/************************************************************/

void gpio_sreg_init(void);

/* Frontend configuration functions */
void frontend_setting_apply();
#endif /* __GPIO_SHIFT_REGISTER_H__ */
