/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  22.09.2014 09:03:56
 *
 *  Description :  Controller for the IDELAY of the Spartan6 FPGA.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __PLB_IDELAY_CONTROL_H__
#define __PLB_IDELAY_CONTROL_H__

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/


/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 */
#define PLB_IDELAY_CONTROL_USER_SLV_SPACE_OFS (0x00000000)
#define PLB_IDELAY_CONTROL_SLV_REG0_OFS (PLB_IDELAY_CONTROL_USER_SLV_SPACE_OFS + 0x00000000)
/**************************** Type Definitions *****************************/

#define IDELAY_CE0    0x00000001
#define IDELAY_CE1    0x00000002
#define IDELAY_CE2    0x00000004
#define IDELAY_CE3    0x00000008
#define IDELAY_CE4    0x00000010
#define IDELAY_CE5    0x00000020
#define IDELAY_CE6    0x00000040
#define IDELAY_CE7    0x00000080
#define IDELAY_CE8    0x00000100
#define IDELAY_CE9    0x00000200
#define IDELAY_CE10   0x00000400
#define IDELAY_CE11   0x00000800
#define IDELAY_CE12   0x00001000
#define IDELAY_CE13   0x00002000
#define IDELAY_CE14   0x00004000
#define IDELAY_CE15   0x00008000
#define IDELAY_CE16   0x00010000
#define IDELAY_CE17   0x00020000
#define IDELAY_CE18   0x00040000
#define IDELAY_CE19   0x00080000
#define IDELAY_CE20   0x00100000
#define IDELAY_CE21   0x00200000
#define IDELAY_CE22   0x00400000
#define IDELAY_CE23   0x00800000
#define IDELAY_CE24   0x01000000
#define IDELAY_CE25   0x02000000
#define IDELAY_CE26   0x04000000
#define IDELAY_CE27   0x08000000

#define IDELAY_RST    0x10000000
#define IDELAY_INC    0x20000000
#define IDELAY_CAL    0x40000000
#define IDELAY_BUSY   0x80000000

/**************************** Type Definitions *****************************/
typedef struct {
  unsigned int  base_address;     /**< Base address of device (IPIF) */
  unsigned int  busy_timeout;     /**< Timeout waiting for busy signal */
} plb_idelay_control;

/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_IDELAY_CONTROL register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_IDELAY_CONTROL device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PLB_IDELAY_CONTROL_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PLB_IDELAY_CONTROL_mWriteReg(BaseAddress, RegOffset, Data) \
 	Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data))

/**
 *
 * Read a value from a PLB_IDELAY_CONTROL register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_IDELAY_CONTROL device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 PLB_IDELAY_CONTROL_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_IDELAY_CONTROL_mReadReg(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_IDELAY_CONTROL user logic slave registers.
 *
 * @param   BaseAddress is the base address of the PLB_IDELAY_CONTROL device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 * 	void PLB_IDELAY_CONTROL_mWriteSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Value)
 * 	Xuint32 PLB_IDELAY_CONTROL_mReadSlaveRegn(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_IDELAY_CONTROL_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
 	Xil_Out32((BaseAddress) + (PLB_IDELAY_CONTROL_SLV_REG0_OFS) + (RegOffset), (Xuint32)(Value))

#define PLB_IDELAY_CONTROL_mReadSlaveReg0(BaseAddress, RegOffset) \
 	Xil_In32((BaseAddress) + (PLB_IDELAY_CONTROL_SLV_REG0_OFS) + (RegOffset))

/************************** Function Prototypes ****************************/

void plb_idelay_init(plb_idelay_control *instance_ptr, unsigned int base_addr, unsigned int timeout);
int plb_idelay_inc(plb_idelay_control *instance_ptr, int ce);
int plb_idelay_dec(plb_idelay_control *instance_ptr, int ce);
int plb_idelay_cal(plb_idelay_control *instance_ptr, int ce);
int plb_idelay_reset(plb_idelay_control *instance_ptr, int ce);

#endif /** __PLB_IDELAY_CONTROL_H__ */
