/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  22.09.2014 13:23:22
 *
 *  Description :  Interface to read iserdes data from ADCs for calibration.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __PLB_SERDES_CAL_IF_H__
#define __PLB_SERDES_CAL_IF_H__

/***************************** Include Files *******************************/

#include "xbasic_types.h"
#include "xstatus.h"
#include "utilities.h"

/************************** Constant Definitions ***************************/


/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 * -- SLV_REG2 : user logic slave module register 2
 * -- SLV_REG3 : user logic slave module register 3
 * -- SLV_REG4 : user logic slave module register 4
 * -- SLV_REG5 : user logic slave module register 5
 * -- SLV_REG6 : user logic slave module register 6
 * -- SLV_REG7 : user logic slave module register 7
 * -- SLV_REG8 : user logic slave module register 8
 * -- SLV_REG9 : user logic slave module register 9
 */
#define PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET (0x00000000)
#define PLB_SERDES_CAL_IF_CTRL_OFFSET   (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000000)
#define PLB_SERDES_CAL_IF_STATUS_OFFSET (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000004)
#define PLB_SERDES_CAL_IF_DATA0_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000008)
#define PLB_SERDES_CAL_IF_DATA1_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x0000000C)
#define PLB_SERDES_CAL_IF_DATA2_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000010)
#define PLB_SERDES_CAL_IF_DATA3_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000014)
#define PLB_SERDES_CAL_IF_DATA4_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000018)
#define PLB_SERDES_CAL_IF_DATA5_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x0000001C)
#define PLB_SERDES_CAL_IF_DATA6_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000020)
#define PLB_SERDES_CAL_IF_DATA7_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000024)
#define PLB_SERDES_CAL_IF_FRAME_OFFSET  (PLB_SERDES_CAL_IF_USER_SLV_SPACE_OFFSET + 0x00000028)

#define PLB_SERDES_CAL_IF_BITSLIP_BIT            0x00001000
#define PLB_SERDES_CAL_IF_HALFWORD_BIT           0x00002000
#define PLB_SERDES_CAL_IF_ALIGNED_BIT            0x00004000
#define PLB_SERDES_CAL_IF_VALID_BIT              0x00008000

#define PLB_SERDES_CAL_IF_STATUS_IN_EMPTY_BIT    0x00010000
#define PLB_SERDES_CAL_IF_STATUS_OUT_FULL_BIT    0x00020000
#define PLB_SERDES_CAL_IF_STATUS_IN_UFLOW_BIT    0x00040000
#define PLB_SERDES_CAL_IF_STATUS_OUT_OFLOW_BIT   0x00080000
#define PLB_SERDES_CAL_IF_STATUS_COUNT_MASK      0x0000000F

#define PLB_SERDES_CAL_IF_CTRL_RST_BIT           0x00010000
#define PLB_SERDES_CAL_IF_CTRL_UPDATE_BIT        0x00020000
#define PLB_SERDES_CAL_IF_CTRL_COUNT_MASK        0x0000000F

/**************************** Type Definitions *****************************/
typedef struct {
  unsigned int  base_address;     /**< Base address of device (IPIF) */
  unsigned int  update_timeout;   /**< Timeout waiting for busy signal */
} plb_serdes_cal_control;

/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_SERDES_CAL_IF register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_SERDES_CAL_IF device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 * 	void PLB_SERDES_CAL_IF_mWriteReg(Xuint32 BaseAddress, unsigned RegOffset, Xuint32 Data)
 *
 */
#define PLB_SERDES_CAL_IF_mWriteReg(BaseAddress, RegOffset, Data) \
 	xfs_out32((xfs_u32)((BaseAddress) + (RegOffset)), (xfs_u32)(Data))
 	/*Xil_Out32((BaseAddress) + (RegOffset), (Xuint32)(Data)) */

/**
 *
 * Read a value from a PLB_SERDES_CAL_IF register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_SERDES_CAL_IF device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 * 	Xuint32 PLB_SERDES_CAL_IF_mReadReg(Xuint32 BaseAddress, unsigned RegOffset)
 *
 */
#define PLB_SERDES_CAL_IF_mReadReg(BaseAddress, RegOffset) \
  xfs_in32((xfs_u32)((BaseAddress) + (RegOffset)))
 	/*Xil_In32((BaseAddress) + (RegOffset)) */

/************************** Function Prototypes ****************************/
void plb_serdes_cal_init(plb_serdes_cal_control *instance_ptr, unsigned int base_addr, unsigned int timeout);
void plb_serdes_cal_trigger_read(plb_serdes_cal_control *instance_ptr, int no_words);
int plb_serdes_cal_update(plb_serdes_cal_control *instance_ptr);
int plb_serdes_cal_data_is_available(plb_serdes_cal_control *instance_ptr);
int plb_serdes_cal_get(plb_serdes_cal_control *instance_ptr, int reg_number);
int plb_serdes_cal_status(plb_serdes_cal_control *instance_ptr);

#endif /** __PLB_SERDES_CAL_IF_H__ */
