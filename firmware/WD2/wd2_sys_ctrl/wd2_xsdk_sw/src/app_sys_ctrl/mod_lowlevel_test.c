/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  20.11.2014 15:52:40
 *
 *  Description :  Module for lowlevel test functions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xfs_printf.h"
#include "utilities.h"
#include "system.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>


/************************************************************/

  int ll_write(int argc, char **argv)
  {
    xfs_u32 address;
    xfs_u32 data;

    CMD_HELP("<address> <data>",
             "write to an address",
             "Writes the data value to the corresponding address."
             "  <address> : 32bit hex address\r\n"
             "  <data>    : 32bit hex value to write\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    address = strtoul(argv[1], NULL, 0);
    data    = strtoul(argv[2], NULL, 0);

    xfs_printf("[0x%08X]: 0x%08X\r\n", address, data);
    xfs_out32(address, data);

    return 0;
  }

/************************************************************/

  int ll_set(int argc, char **argv)
  {
    xfs_u32 address;
    xfs_u32 data;

    CMD_HELP("<address> <data>",
             "set bits of an address",
             "Sets the data bits of the corresponding address."
             "  <address> : 32bit hex address\r\n"
             "  <data>    : 32bit hex value to write\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    address = (xfs_u32)strtoul(argv[1], NULL, 0);
    data    = (xfs_u32)strtoul(argv[2], NULL, 0) | xfs_in32(address);

    xfs_printf("[0x%08X]: 0x%08X\r\n", address, data);
    xfs_out32(address, data);

    return 0;
  }

/************************************************************/

  int ll_clr(int argc, char **argv)
  {
    xfs_u32 address;
    xfs_u32 data;

    CMD_HELP("<address> <data>",
             "clear bits of an address",
             "Clears the data bits of the corresponding register.\r\n"
             "  <address> : hex address from base address\r\n"
             "  <data>    : 32bit hex value to write\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    address =   (xfs_u32)strtoul(argv[1], NULL, 0);
    data    = ~((xfs_u32)strtoul(argv[2], NULL, 0)) & xfs_in32(address);

    xfs_printf("[0x%08X]: 0x%08X\r\n", address, data);
    xfs_out32(address, data);

    return 0;
  }

/************************************************************/

  int ll_read(int argc, char **argv)
  {
    xfs_u32 address;
    xfs_u32 data;
    int no_words;

    CMD_HELP("<address> <number_of_words>",
             "read from an address",
             "Reads a specified number of words starting at address.\r\n"
             "  <address>         : hex start address for read\r\n"
             "  <number_of_words> : dec number of 32bit values to read\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    address  = (xfs_u32)strtoul(argv[1], NULL, 0);
    address  = address - address%4; /* align to full word */
    no_words = strtol(argv[2], NULL, 0);

    for(int i=0;i<no_words;i++)
    {
      data = xfs_in32(address);
      xfs_printf("[0x%08X]: 0x%08X\r\n", address, data);
      address = address + 4;
    }

    return 0;
  }

/************************************************************/

  int module_ll_help(int argc, char **argv)
  {
    CMD_HELP("",
             "Lowlevel Test Module",
             "Can be used for lowlevel testing"
            );

    return 0;
  }


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type ll_cmd_table[] =
  {
    {0, "ll", module_ll_help},
    {0, "llwr", ll_write},
    {0, "llrd", ll_read},
    {0, "llset", ll_set},
    {0, "llclr", ll_clr},
    {0, NULL, NULL}
  };

/************************************************************/
