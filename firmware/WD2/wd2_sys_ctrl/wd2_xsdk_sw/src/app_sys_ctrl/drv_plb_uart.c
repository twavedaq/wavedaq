/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.11.2015 15:47:28
 *
 *  Description :  Driver for PLB UART (non stdio).
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "drv_plb_uart.h"
#include "utilities.h"
#include "xfs_printf.h"

/******************************************************************************/

void plb_uart_init(plb_uart_type *self, unsigned int base_address)
{
  self->base_address = base_address;
}

/****************************************************************************/

void receive_data_handler(plb_uart_type *self)
{
  /*
   * If there are bytes still to be received in the specified buffer
   * go ahead and receive them
   */
  xfs_printf("r");
  if (self->receive_buffer.RemainingBytes != 0)
  {
    plb_uart_recieve_buffer(self);
  }

  /*
   * If the last byte of a message was received then call the application
   * handler, this code should not use an else from the previous check of
   * the number of bytes to receive because the call to receive the buffer
   * updates the bytes to receive
   */
  if (self->receive_buffer.RemainingBytes == 0)
  {
    self->RecvHandler(self->RecvCallBackRef, self->receive_buffer.RequestedBytes - self->receive_buffer.RemainingBytes);
  }

  /*
   * Update the receive stats to reflect the receive interrupt
   */
  self->stats.ReceiveInterrupts++;
}

/****************************************************************************/

void send_data_handler(plb_uart_type *self)
{
  /*
   * If there are not bytes to be sent from the specified buffer,
   * call the callback function
   */
  xfs_printf("s");
  if (self->send_buffer.RemainingBytes == 0)
  {
    int SaveReq;

    /*
     * Save and zero the requested bytes since transmission
     * is complete
     */
    SaveReq = self->send_buffer.RequestedBytes;
    self->send_buffer.RequestedBytes = 0;

    /*
     * Call the application handler to indicate
     * the data has been sent
     */
    self->SendHandler(self->SendCallBackRef, SaveReq);
  }
  /*
   * Otherwise there is still more data to send in the specified buffer
   * so go ahead and send it
   */
  else
  {
    plb_uart_send_buffer(self);
  }

  /*
   * Update the transmit stats to reflect the transmit interrupt
   */
  self->stats.TransmitInterrupts++;
}

/****************************************************************************/

void write_reg(plb_uart_type *self, unsigned int RegOffset, unsigned int Data)
{
  xfs_out32(self->base_address + RegOffset, Data);
}

/****************************************************************************/

unsigned int read_reg(plb_uart_type *self, unsigned int RegOffset)
{
   return xfs_in32(self->base_address + RegOffset);
}

/****************************************************************************/

void set_control_reg(plb_uart_type *self, unsigned int Mask)
{
  write_reg(self, UI_CONTROL_WD2_REG_OFS, Mask);
}

/****************************************************************************/

unsigned int get_status_reg(plb_uart_type *self)
{
  return read_reg(self, UI_STATUS_WD2_REG_OFS);
}

/****************************************************************************/

int is_receive_empty(plb_uart_type *self)
{
  if((get_status_reg(self) & UI_SR_RX_FIFO_VALID_DATA) != UI_SR_RX_FIFO_VALID_DATA)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/****************************************************************************/

int is_transmit_full(plb_uart_type *self)
{
  if((get_status_reg(self) & UI_SR_TX_FIFO_FULL) == UI_SR_TX_FIFO_FULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/****************************************************************************/

int is_interrupt_enabled(plb_uart_type *self)
{
  if((get_status_reg(self) & UI_SR_INTR_ENABLED) == UI_SR_INTR_ENABLED)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/****************************************************************************/

void update_stats(plb_uart_type *self)
{
  unsigned int StatusRegister;

  StatusRegister = get_status_reg(self);

  if ((StatusRegister) & UI_SR_OVERRUN_ERROR);
  {
    self->stats.ReceiveOverrunErrors++;
  }
  if ((StatusRegister) & UI_SR_PARITY_ERROR)
  {
    self->stats.ReceiveParityErrors++;
  }
  if ((StatusRegister) & UI_SR_FRAMING_ERROR)
  {
    self->stats.ReceiveFramingErrors++;
  }
}

/******************************************************************************/

unsigned int plb_uart_send(plb_uart_type *self, unsigned char *DataBufferPtr, unsigned int NumBytes)
{
  unsigned int BytesSent;
  unsigned int StatusRegister;

  /*
   * Assert validates the input arguments
   */
  /*Xil_AssertNonvoid(DataBufferPtr != NULL); */
  /*Xil_AssertNonvoid(((signed)NumBytes) >= 0); */
  if( (DataBufferPtr == NULL) || (NumBytes == 0) ) return 0;

  /*
   * Enter a critical region by disabling the UART interrupts to allow
   * this call to stop a previous operation that may be interrupt driven.
   */
  StatusRegister = get_status_reg(self);

  write_reg(self, UI_CONTROL_WD2_REG_OFS, 0);

  /*
   * Setup the specified buffer to be sent by setting the instance
   * variables so it can be sent with polled or interrupt mode
   */
  self->send_buffer.RequestedBytes = NumBytes;
  self->send_buffer.RemainingBytes = NumBytes;
  self->send_buffer.NextBytePtr = DataBufferPtr;

  /*
   * Restore the interrupt enable register to it's previous value such
   * that the critical region is exited.
   * This is done here to minimize the amount of time the interrupt is
   * disabled since there is only one interrupt and the receive could
   * be filling up while interrupts are blocked.
   */

  StatusRegister &= UI_CR_ENABLE_INTR;
  set_control_reg(self, StatusRegister);

  /*
   * Send the buffer using the UART and return the number of bytes sent
   */
  BytesSent = plb_uart_send_buffer(self);

  return BytesSent;
}


/****************************************************************************/

unsigned int plb_uart_receive(plb_uart_type *self, unsigned char *DataBufferPtr, unsigned int NumBytes)
{
  unsigned int ReceivedCount;
  unsigned int StatusRegister;

  /*
   * Assert validates the input arguments
   */
  /*Xil_AssertNonvoid(DataBufferPtr != NULL); */
  /*Xil_AssertNonvoid(((signed)NumBytes) >= 0); */
  if( (DataBufferPtr == NULL) || (NumBytes == 0) ) return 0;

  /*
   * Enter a critical region by disabling all the UART interrupts to allow
   * this call to stop a previous operation that may be interrupt driven
   */
  StatusRegister = get_status_reg(self);
  set_control_reg(self, 0);

  /*
   * Setup the specified buffer to be received by setting the instance
   * variables so it can be received with polled or interrupt mode
   */
  self->receive_buffer.RequestedBytes = NumBytes;
  self->receive_buffer.RemainingBytes = NumBytes;
  self->receive_buffer.NextBytePtr = DataBufferPtr;

  /*
   * Restore the interrupt enable register to it's previous value such
   * that the critical region is exited
   */
  StatusRegister &= UI_CR_ENABLE_INTR;
  set_control_reg(self, StatusRegister);

  /*
   * Receive the data from the UART and return the number of bytes
   * received
   * This is done here to minimize the amount of time the interrupt is
   * disabled since there is only one interrupt and the transmit could
   * be emptying out while interrupts are blocked.
   */
  ReceivedCount = plb_uart_recieve_buffer(self);
  /*if (ReceivedCount) */
  /*   xil_printf("ReceivedCount = %d\r\n", ReceivedCount); */

  return ReceivedCount;

}

/****************************************************************************/

void plb_uart_reset_fifos(plb_uart_type *self)
{
  unsigned int StatusRegister;

  /*
   * Read the status register 1st such that the next write to the control
   * register won't destroy the state of the interrupt enable bit
   */
  StatusRegister = get_status_reg(self);

  /*
   * Mask off the interrupt enable bit to maintain it's state.
   */
  StatusRegister &= UI_SR_INTR_ENABLED;

  /*
   * Write to the control register to reset both FIFOs, these bits are
   * self-clearing such that there's no need to clear them
   */
  set_control_reg(self, StatusRegister | UI_CR_FIFO_TX_RESET | UI_CR_FIFO_RX_RESET);
}

/****************************************************************************/

int plb_uart_is_sending(plb_uart_type *self)
{
  unsigned int StatusRegister;

  /*
   * Read the status register to determine if the transmitter is empty
   */
  StatusRegister = get_status_reg(self);

  /*
   * If the transmitter is not empty then indicate that the UART is still
   * sending some data
   */
  return ((StatusRegister & UI_SR_TX_FIFO_EMPTY) == 0);
}


/****************************************************************************/

unsigned int plb_uart_send_buffer(plb_uart_type *self)
{
  unsigned int SentCount = 0;
  unsigned char StatusRegister;
  unsigned char IntrEnableStatus;

  /*
   * Read the status register to determine if the transmitter is full
   */
  StatusRegister = get_status_reg(self);

  /*
   * Enter a critical region by disabling all the UART interrupts to allow
   * this call to stop a previous operation that may be interrupt driven
   */
  set_control_reg(self, 0);

  /*
   * Save the status register contents to restore the interrupt enable
   * register to it's previous value when that the critical region is
   * exited
   */
  IntrEnableStatus = StatusRegister;

  /*
   * Fill the FIFO from the the buffer that was specified
   */

  while (((StatusRegister & UI_SR_TX_FIFO_FULL) == 0) && (SentCount < self->send_buffer.RemainingBytes))
  {
    write_reg(self, UI_TX_FIFO_OFS, self->send_buffer.NextBytePtr[SentCount]);
    SentCount++;
    StatusRegister = get_status_reg(self);
  }

  /*
   * Update the buffer to reflect the bytes that were sent from it
   */
  self->send_buffer.NextBytePtr += SentCount;
  self->send_buffer.RemainingBytes -= SentCount;

  /*
   * Increment associated counters
   */
   self->stats.CharactersTransmitted += SentCount;

  /*
   * Restore the interrupt enable register to it's previous value such
   * that the critical region is exited
   */
  IntrEnableStatus &= UI_CR_ENABLE_INTR;
  set_control_reg(self, IntrEnableStatus);

  /*
   * Return the number of bytes that were sent, althought they really were
   * only put into the FIFO, not completely sent yet
   */
  return SentCount;
}

/****************************************************************************/

unsigned int plb_uart_recieve_buffer(plb_uart_type *self)
{
  unsigned char StatusRegister;
  unsigned int ReceivedCount = 0;

  /*
   * Loop until there is not more data buffered by the UART or the
   * specified number of bytes is received
   */

  while (ReceivedCount < self->receive_buffer.RemainingBytes) {
    /*
     * Read the Status Register to determine if there is any data in
     * the receiver/FIFO
     */
    StatusRegister = get_status_reg(self);

    /*
     * If there is data ready to be removed, then put the next byte
     * received into the specified buffer and update the stats to
     * reflect any receive errors for the byte
     */
    if (StatusRegister & UI_SR_RX_FIFO_VALID_DATA)
    {
      self->receive_buffer.NextBytePtr[ReceivedCount++]= read_reg(self, UI_RX_FIFO_OFS);
      update_stats(self);
    }
    /*
     * There's no more data buffered, so exit such that this
     * function does not block waiting for data
     */
    else
    {
      break;
    }
  }

  /*
   * Enter a critical region by disabling all the UART interrupts to allow
   * this call to stop a previous operation that may be interrupt driven
   */
  StatusRegister = get_status_reg(self);
  set_control_reg(self, 0);

  /*
   * Update the receive buffer to reflect the number of bytes that was
   * received
   */
  self->receive_buffer.NextBytePtr += ReceivedCount;
  self->receive_buffer.RemainingBytes -= ReceivedCount;

  /*
   * Increment associated counters in the statistics
   */
  self->stats.CharactersReceived += ReceivedCount;

  /*
   * Restore the interrupt enable register to it's previous value such
   * that the critical region is exited
   */
  StatusRegister &= UI_CR_ENABLE_INTR;
  set_control_reg(self, StatusRegister);

  return ReceivedCount;
}

/****************************************************************************/

void plb_uart_set_receive_handler(plb_uart_type *self, uart_intc_handler FuncPtr, void *CallBackRef)
{
  /*
   * Assert validates the input arguments
   * CallBackRef not checked, no way to know what is valid
   */
  /*Xil_AssertVoid(FuncPtr != NULL); */
  if( FuncPtr == NULL ) return;

  self->RecvHandler = FuncPtr;
  self->RecvCallBackRef = CallBackRef;
}

/****************************************************************************/

void plb_uart_set_send_handler(plb_uart_type *self, uart_intc_handler FuncPtr, void *CallBackRef)
{
  /*
   * Assert validates the input arguments
   * CallBackRef not checked, no way to know what is valid
   */
  /*Xil_AssertVoid(FuncPtr != NULL); */
  if( FuncPtr == NULL ) return;

  self->SendHandler = FuncPtr;
  self->SendCallBackRef = CallBackRef;
}

/****************************************************************************/

void plb_uart_interrupt_handler(plb_uart_type *self, void *CallBackRef)
{
  unsigned int IsrStatus;

  /*
   * Read the status register to determine which, coulb be both
   * interrupt is active
   */
  IsrStatus = get_status_reg(self);

  xfs_printf("i");
  if ((IsrStatus & (UI_SR_RX_FIFO_FULL | UI_SR_RX_FIFO_VALID_DATA)) != 0)
  {
    receive_data_handler(self);
  }

  if (((IsrStatus & UI_SR_TX_FIFO_EMPTY) != 0) && (self->send_buffer.RequestedBytes > 0))
  {
    send_data_handler(self);
  }
}

/*****************************************************************************/

void plb_uart_disable_interrupt(plb_uart_type *self)
{
  /*
   * Write to the control register to disable the interrupts, the only
   * other bits in this register are the FIFO reset bits such that
   * writing them to zero will not affect them.
   */
  set_control_reg(self, 0);
}

/*****************************************************************************/

void plb_uart_enable_interrupt(plb_uart_type *self)
{
  /*
   * Write to the control register to enable the interrupts, the only
   * other bits in this register are the FIFO reset bits such that
   * writing them to zero will not affect them.
   */
  set_control_reg(self, UI_CR_ENABLE_INTR);
}

/****************************************************************************/

void plb_uart_get_status(plb_uart_type *self, uart_stats *StatsPtr)
{
  /*
   * Assert validates the input arguments
   */
  /*Xil_AssertVoid(StatsPtr != NULL); */
  if( StatsPtr == NULL ) return;

  /* Copy the stats from the instance to the specified stats */

  StatsPtr->TransmitInterrupts    = self->stats.TransmitInterrupts;
  StatsPtr->ReceiveInterrupts     = self->stats.ReceiveInterrupts;
  StatsPtr->CharactersTransmitted = self->stats.CharactersTransmitted;
  StatsPtr->CharactersReceived    = self->stats.CharactersReceived;
  StatsPtr->ReceiveOverrunErrors  = self->stats.ReceiveOverrunErrors;
  StatsPtr->ReceiveFramingErrors  = self->stats.ReceiveFramingErrors;
  StatsPtr->ReceiveParityErrors   = self->stats.ReceiveParityErrors;
}

/****************************************************************************/

void plb_uart_clear_status(plb_uart_type *self)
{
  self->stats.TransmitInterrupts    = 0;
  self->stats.ReceiveInterrupts     = 0;
  self->stats.CharactersTransmitted = 0;
  self->stats.CharactersReceived    = 0;
  self->stats.ReceiveOverrunErrors  = 0;
  self->stats.ReceiveFramingErrors  = 0;
  self->stats.ReceiveParityErrors   = 0;
}

/****************************************************************************/
/****************************************************************************/
