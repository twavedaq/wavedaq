/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  09.08.2016 11:19:01
 *
 *  Description :  Module for accessing the DRP of the PLL.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "system.h"
#include "drv_drp_pll.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "cmd_processor.h"
#include <stdlib.h>


/************************************************************/

  unsigned int decode_channel(char *channel)
  {
    if(fstrcmp(channel,"0"))           return DRP_CHANNEL_CLK0;
    else if(fstrcmp(channel,"1"))      return DRP_CHANNEL_CLK1;
    else if(fstrcmp(channel,"2"))      return DRP_CHANNEL_CLK2;
    else if(fstrcmp(channel,"3"))      return DRP_CHANNEL_CLK3;
    else if(fstrcmp(channel,"4"))      return DRP_CHANNEL_CLK4;
    else if(fstrcmp(channel,"5"))      return DRP_CHANNEL_CLK5;
    else if(fstrcmp(channel,"clkfb"))  return DRP_CHANNEL_CLKFB;
    else if(fstrcmp(channel,"divclk")) return DRP_CHANNEL_DIVCLK;
    return DRP_CHANNEL_NONE;
  }

/************************************************************/

  int drp_pll_divider_dutycycle(int argc, char **argv)
  {
    unsigned int divider, dutycycle;

    CMD_HELP("<channel> <divider> <dutycycle>",
             "read or write pll divider and dutycycle via the drp",
             "If divider and dutycycle are specified, the corresponding\r\n"
             "values are written to the PLL via the DRP (Dynamic Reconfigruation Port).\r\n"
             "If they are omitted, the divider and dutycycle settings are read from the\r\n"
             "PLL and reported.\r\n"
             "  <channel>   : channel to set (range = 0..5, clkfb, divclk).\r\n"
             "  <divider>   : integer divider value.\r\n"
             "  <dutycycle> : dutycycle in percent.\r\n"
            );


    /* Check for minimum number of arguments */
    if( (argc < 2) || (argc==3) )
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    else if(argc == 2)
    {
      /* Read PLL settings via DRP */
      drp_pll_get_divider_and_dutycycle(SYSPTR(adc_pll_control), decode_channel(argv[1]), divider, dutycycle);
      xil_printf("PLL channel %s: divider = %d,   duty cycle = %d %%\r\n\r\n",argv[1], divider, dutycycle);
    }
    else
    {
      /* Write PLL settings via DRP */
      divider   = (unsigned int)(xfs_atoi(argv[2]));
      dutycycle = (unsigned int)(xfs_atoi(argv[3]));
      drp_pll_set_divider_and_dutycycle(SYSPTR(adc_pll_control), decode_channel(argv[1]), divider, dutycycle);
      xil_printf("Wrote PLL channel %s: divider = %d,   duty cycle = %d %%\r\n\r\n",argv[1], divider, dutycycle);
    }

    return 0;
  }

/************************************************************/

  int drp_pll_phase(int argc, char **argv)
  {
    unsigned int phase;

    CMD_HELP("<channel> <phase>",
             "read or write pll phase via the drp",
             "If phase is specified, the corresponding\r\n"
             "values is written to the PLL via the DRP (Dynamic Reconfigruation Port).\r\n"
             "If it is omitted, the phase setting is read from the\r\n"
             "PLL and reported.\r\n"
             "  <channel> : output channel to set (range = 0..5).\r\n"
             "  <phase>   : integer phase value (range = 0..360).\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    else if(argc == 2)
    {
      /* Read PLL settings via DRP */
      drp_pll_get_phase(SYSPTR(adc_pll_control), decode_channel(argv[1]), phase);
      xil_printf("PLL channel %s: phase = %d deg\r\n\r\n",argv[1], phase);
    }
    else
    {
      /* Write PLL settings via DRP */
      phase = (unsigned int)(xfs_atoi(argv[2]));
      drp_pll_set_phase(SYSPTR(adc_pll_control), decode_channel(argv[1]), phase);
      xil_printf("Wrote PLL channel %s: phase = %d deg\r\n\r\n",argv[1], phase);
    }

    return 0;
  }

/************************************************************/

  int drp_pll_get_lock(int argc, char **argv)
  {
    CMD_HELP("",
             "reports the lock status of the PLL",
             "reports the lock status of the PLL"  
            );


    xil_printf("PLL ");
    if(drp_pll_is_locked(SYSPTR(adc_pll_control)))
    {
      xil_printf("locked\r\n\r\n");
    }
    else
    {
      xil_printf("NOT locked !!!\r\n\r\n");
    }

    return 0;
  }

/************************************************************/

  int drp_pll_set_loop_bw(int argc, char **argv)
  {
    CMD_HELP("<bandwidth>",
             "set the pll loop filter bandwidth",
             "set the pll loop filter bandwidth.\r\n"  
             "  <bandwidth> : loop filter bandwidth (high, low).\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    else
    {
      if(fstrcmp(argv[1],"high"))
      {
        xil_printf("Setting PLL loop filter bandwidth HIGH\r\n");
        drp_pll_set_bandwidth(SYSPTR(adc_pll_control), PLL_BANDWIDTH_HIGH);
      }
      else if(fstrcmp(argv[1],"low"))
      {
        xil_printf("Setting PLL loop filter bandwidth LOW\r\n");
        drp_pll_set_bandwidth(SYSPTR(adc_pll_control), PLL_BANDWIDTH_LOW);
      }
      else
      {
        xfs_printf("E%02X: Invalid arguments\r\n", ERR_INVALID_ARGS);
      }
    }

    return 0;
  }

/************************************************************/

  int module_drp_pll_help(int argc, char **argv)
  {
    CMD_HELP("",
             "DRP PLL configuration module",
             "Can be used to reconfigure the parameters of a pll"  
            );

    return 0;
  }


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type drp_pll_cmd_table[] =
  {
    {0, "drp", module_drp_pll_help},
    {0, "drpdd", drp_pll_divider_dutycycle},
    {0, "drpph", drp_pll_phase},
    {0, "drplock", drp_pll_get_lock},
    {0, "drpbw", drp_pll_set_loop_bw},
    {0, NULL, NULL}
  };

/************************************************************/
