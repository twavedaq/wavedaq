/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  18.05.2015 15:00:40
 *
 *  Description :  SPI pcore driver for communication with
 *                 HV ADC and DACs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_HARD_SPI_HV_H__
#define __DRV_HARD_SPI_HV_H__

#include "plb_spi_master_v2_00_a.h"

/*
*  Format :
*           A3 A2 A1 A0             - Reg Address
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*
*  out_cfg: A4 A3 A2 A1 A0 D5 D4 D3 D2 D1 D0
*                                         ^lsb send first
*/
#define  SPI_DATA_DIR_OUT   0
#define  SPI_DATA_DIR_IN    1

#define  HV_PLUGIN_I         0x0001

#define  SPI_HV_DOUT_I       0x0008
#define  SPI_HV_DIN_O        0x0010
#define  SPI_HV_SCLK_O       0x0020
#define  SPI_HV_CS_M0_O      0x0040
#define  SPI_HV_CS_M1_O      0x0080
#define  SPI_HV_CS_M2_O      0x0100
#define  SPI_HV_CSE_O        0x0200

#define  SPI_HV_CS_DAC1_LV_O   0x00
#define  SPI_HV_CS_DAC2_LV_O   0x01
#define  SPI_HV_CS_ADC1_LV_O   0x02
#define  SPI_HV_CS_ADC2_LV_O   0x03
#define  SPI_HV_CS_HV_ADC_O    0x04
#define  SPI_HV_CS_GPIO_O      0x05
#define  SPI_HV_CS_HV_DCDC_O   0x06
#define  SPI_HV_CS_EEPROM_O    0x07

/************************************************************/

#define SLV_SETTINGS_DAC1_LV   0
#define SLV_SETTINGS_DAC2_LV   1
#define SLV_SETTINGS_ADC1_LV   2
#define SLV_SETTINGS_ADC2_LV   3
#define SLV_SETTINGS_HV_ADC    4
#define SLV_SETTINGS_GPIO      5
#define SLV_SETTINGS_HV_DCDC   6
#define SLV_SETTINGS_EEPROM    7

/************************************************************/

/* high voltage divider */
#define HV_VOLTAGE_DIVIDER ((2E6+10E3)/10E3)

/************************************************************/
/* LTC2656 DAC defines                                      */
/************************************************************/
#define  LTC2656_CPOL        0
#define  LTC2656_CPHA        0
#define  LTC2656_LSB_FIRST   0

/* DAC General Commands */
#define  LTC2656_CMD_WRN        0x00 /* Write n */
#define  LTC2656_CMD_PUN        0x10 /* Update (Power Up) n */
#define  LTC2656_CMD_WRN_PUA    0x20 /* Write n, update (Power Up) all */
#define  LTC2656_CMD_WRN_PUN    0x30 /* Write n, update (Power Up) n */
#define  LTC2656_CMD_PDN        0x40 /* Power Down n */
#define  LTC2656_CMD_PDC        0x50 /* Power Down Chip (All DACs and Reference) */
#define  LTC2656_CMD_SEL_IREF   0x60 /* Select Internal Reference */
#define  LTC2656_CMD_SEL_EREF   0x70 /* Select External Reference (Power Down Reference) */
#define  LTC2656_CMD_NOP        0xF0 /* No Operation */

/* DAC Addresses */
#define  LTC2656_ADR_A       0x00
#define  LTC2656_ADR_B       0x01
#define  LTC2656_ADR_C       0x02
#define  LTC2656_ADR_D       0x03
#define  LTC2656_ADR_E       0x04
#define  LTC2656_ADR_F       0x05
#define  LTC2656_ADR_G       0x06
#define  LTC2656_ADR_H       0x07
#define  LTC2656_ADR_ALL     0x0F

/* DAC Parameters */
#define  LTC2656_VREF_V                5.0       /* Vref 2.5V*2 */
#define  LTC2656_RES_V                 1.221e-3  /* Voltage resolution [V] = Vref/2^N = 5/4096 */

/************************************************************/

/************************************************************/
/* LTC2494 ADC defines                                      */
/************************************************************/
#define  LTC2494_CPOL        0
#define  LTC2494_CPHA        0
#define  LTC2494_LSB_FIRST   0

/* ADC Commands */
#define  LTC2494_CMD_PREAMBLE       0x80
#define  LTC2494_CMD_EN             0x20
#define  LTC2494_CMD_SGL            0x10 /* SGL=1: Single ended inputs */
#define  LTC2494_CMD_ODD            0x08 /* Polarity  */
#define  LTC2494_CMD_ADR            0x07 /* Channel selection */
#define  LTC2494_CMD_EN2            0x80 /* EN2=0: Use previous configuration of following bits */
#define  LTC2494_CMD_IM             0x40 /* IM=1: next conversion on temp sensor */
#define  LTC2494_CMD_FA_FB          0x30 /* Select rejection frequency */
#define  LTC2494_CMD_SPD            0x08 /* SPD=1: 2x output speed (no offset correction) */
#define  LTC2494_CMD_GS             0x07 /* Gain select */
/* ADC Frequency Reject Select Commands */
#define  LTC2494_CMD_RJ50HZ         0x10 /* Reject 50Hz frequency */
#define  LTC2494_CMD_RJ60HZ         0x20 /* Reject 60Hz frequency */
#define  LTC2494_CMD_RJ50HZ60HZ     0x00 /* Reject both 50Hz and 60Hz frequency */
/* ADC Gain Select Commands */
#define  LTC2494_CMD_GS1            0x00 /* Gain=1   */
#define  LTC2494_CMD_GS4            0x01 /* Gain=4   */
#define  LTC2494_CMD_GS8            0x02 /* Gain=8   */
#define  LTC2494_CMD_GS16           0x03 /* Gain=16  */
#define  LTC2494_CMD_GS32           0x04 /* Gain=32  */
#define  LTC2494_CMD_GS64           0x05 /* Gain=64  */
#define  LTC2494_CMD_GS128          0x06 /* Gain=128 */
#define  LTC2494_CMD_GS256          0x07 /* Gain=256 */

/* ADC Addresses */
#define  LTC2494_ADR_CH0         0x00 /* polarity positive */
/*#define  LTC2494_ADR_CH0         0x08 */ /* polarity negative */
#define  LTC2494_ADR_CH1         0x01 /* polarity positive */
/*#define  LTC2494_ADR_CH1         0x09 */ /* polarity negative */
#define  LTC2494_ADR_CH2         0x02 /* polarity positive */
/*#define  LTC2494_ADR_CH2         0x0A */ /* polarity negative */
#define  LTC2494_ADR_CH3         0x03 /* polarity positive */
/*#define  LTC2494_ADR_CH3         0x0B */ /* polarity negative */
#define  LTC2494_ADR_CH4         0x04 /* polarity positive */
/*#define  LTC2494_ADR_CH4         0x0C */ /* polarity negative */
#define  LTC2494_ADR_CH5         0x05 /* polarity positive */
/*#define  LTC2494_ADR_CH5         0x0D */ /* polarity negative */
#define  LTC2494_ADR_CH6         0x06 /* polarity positive */
/*#define  LTC2494_ADR_CH6         0x0E */ /* polarity negative */
#define  LTC2494_ADR_CH7         0x07 /* polarity positive */
/*#define  LTC2494_ADR_CH7         0x0F */ /* polarity negative */

/* ADC Status */
#define  LTC2494_STAT_EOC_N   0x00800000
#define  LTC2494_STAT_SIG     0x00200000
#define  LTC2494_STAT_MSB     0x00100000

/* ADC Parameters */
#define  LTC2494_VREF_V        2.500                        /* Vref = 2.5V */
#define  LTC2494_RES_V        (0.5*LTC2494_VREF_V/(1<<16))
#define  LTC2494_VCOM_V        0.000                        /* Vcom = 0V */

/* ADC Parameters */
#define  LTC2494_R_SHUNT               10.0e3
#define  LTC2494_ADC_BUFF_GAIN         85.0

/************************************************************/

/************************************************************/
/*LTC2601 defines                                           */
/************************************************************/
#define  LTC2601_CPOL        0
#define  LTC2601_CPHA        0
#define  LTC2601_LSB_FIRST   0

/* DAC Commands */
#define  LTC2601_CMD_WR        0x00
#define  LTC2601_CMD_PU        0x10
#define  LTC2601_CMD_WR_PU     0x30
#define  LTC2601_CMD_PD        0x40
#define  LTC2601_CMD_NOP       0xF0

/* DAC Parameters */
#define  LTC2601_VREF_V        2.500                    /* Vref = 2.5V */
#define  LTC2601_RES_V        (LTC2484_VREF_V/(1<<16))  /* 1 LSB */
#define  LTC2601_V_SCL_FCTR    HV_VOLTAGE_DIVIDER

/************************************************************/
/* LTC2484 defines                                          */
/************************************************************/
#define  LTC2484_CPOL        0
#define  LTC2484_CPHA        0
#define  LTC2484_LSB_FIRST   0

/* ADC Commands */
#define  LTC2484_CMD_SPD    0x01   /* Speed Mode (skip offset calibration) */
#define  LTC2484_CMD_FOB    0x02   /* Reject 50Hz using digital filter */
#define  LTC2484_CMD_FOA    0x04   /* Reject 60Hz using digital filter */
#define  LTC2484_CMD_IM     0x08   /* Temperature input */
#define  LTC2484_CMD_EN     0x80   /* Apply settings of current transmission */

/* ADC Status */
#define  LTC2484_STAT_SIG     0x20000000
#define  LTC2484_STAT_MSB     0x10000000
#define  LTC2484_STAT_EOC_N   0x80000000

/* ADC Parameters */
#define  LTC2484_VREF_V       2.500                       /* Vref = 2.5V */
#define  LTC2484_RES_V       (LTC2484_VREF_V*0.5/(1<<28)) /* 1 LSB */
#define  LTC2484_V_SCL_FCTR   HV_VOLTAGE_DIVIDER

/************************************************************/
/* MCP23S17 defines                                         */
/************************************************************/
#define  MCP23S17_CPOL        0
#define  MCP23S17_CPHA        0
#define  MCP23S17_LSB_FIRST   0

/* Addresses IOCON.BANK = 0 */                  /* (IOCON.BANK = 1) */
#define  MCP23S17_DEV_ADDR           0x00
#define  MCP23S17_IODIRA_WD2_REG_ADR     0x00   /* (0x00) */
#define  MCP23S17_IODIRB_WD2_REG_ADR     0x01   /* (0x10) */
#define  MCP23S17_IOPOLA_WD2_REG_ADR     0x02   /* (0x01) */
#define  MCP23S17_IOPOLB_WD2_REG_ADR     0x03   /* (0x11) */
#define  MCP23S17_GPINTENA_WD2_REG_ADR   0x04   /* (0x02) */
#define  MCP23S17_GPINTENB_WD2_REG_ADR   0x05   /* (0x12) */
#define  MCP23S17_DEFVALA_WD2_REG_ADR    0x06   /* (0x03) */
#define  MCP23S17_DEFVALB_WD2_REG_ADR    0x07   /* (0x13) */
#define  MCP23S17_INTCONA_WD2_REG_ADR    0x08   /* (0x04) */
#define  MCP23S17_INTCONB_WD2_REG_ADR    0x09   /* (0x14) */
#define  MCP23S17_IOCONA_WD2_REG_ADR     0x0A   /* (0x05) */
#define  MCP23S17_IOCONB_WD2_REG_ADR     0x0B   /* (0x15) */
#define  MCP23S17_GPPUA_WD2_REG_ADR      0x0C   /* (0x06) */
#define  MCP23S17_GPPUB_WD2_REG_ADR      0x0D   /* (0x16) */
#define  MCP23S17_INTFA_WD2_REG_ADR      0x0E   /* (0x07) */
#define  MCP23S17_INTFB_WD2_REG_ADR      0x0F   /* (0x17) */
#define  MCP23S17_INTCAPA_WD2_REG_ADR    0x10   /* (0x08) */
#define  MCP23S17_INTCAPB_WD2_REG_ADR    0x11   /* (0x18) */
#define  MCP23S17_GPIOA_WD2_REG_ADR      0x12   /* (0x09) */
#define  MCP23S17_GPIOB_REG_ADR          0x13   /* (0x19) */
#define  MCP23S17_OLATA_WD2_REG_ADR      0x14   /* (0x0A) */
#define  MCP23S17_OLATB_WD2_REG_ADR      0x15   /* (0x1A) */

/* Commands (opcodes) */
#define  MCP23S17_CMD_WRITE   0x40
#define  MCP23S17_CMD_READ    0x41

/* IOCON Register Bits */
#define  MCP23S17_IOCON_INTPOL         0x02
#define  MCP23S17_IOCON_ODR            0x04
#define  MCP23S17_IOCON_HAEN           0x08
#define  MCP23S17_IOCON_DISSLW         0x10
#define  MCP23S17_IOCON_SEQOP          0x20
#define  MCP23S17_IOCON_MIRROR         0x40
#define  MCP23S17_IOCON_BANK           0x80
/* GPIO/OLAT Register Bits (defined by PCB connections) */
#define  MCP23S17_GPA_L0_1_O           0x01
#define  MCP23S17_GPA_L2_3_O           0x02
#define  MCP23S17_GPA_L4_5_O           0x04
#define  MCP23S17_GPA_L6_7_O           0x08
#define  MCP23S17_GPA_L8_9_O           0x10
#define  MCP23S17_GPA_L10_11_O         0x20
#define  MCP23S17_GPA_L12_13_O         0x40
#define  MCP23S17_GPA_L14_15_O         0x80
#define  MCP23S17_GPB_IN1_O            0x01
#define  MCP23S17_GPB_IN2_O            0x02
#define  MCP23S17_GPB_TSD_ALL_O        0x04
#define  MCP23S17_GPB_HV_ON_O          0x08
#define  MCP23S17_GPB_BOARD_A0_I       0x10
#define  MCP23S17_GPB_BOARD_A1_I       0x20
#define  MCP23S17_GPB_PWR_ON_O         0x40
#define  MCP23S17_GPB_PLUGIN_I         0x80

/************************************************************/
/* 25AA02E48 defines */
/************************************************************/
#define  HV_EEPROM_CPOL   0
#define  HV_EEPROM_CPHA   0
#define  HV_EEPROM_LSB_FIRST   0

#define  HV_EEPROM_BYTES            256
#define  HV_EEPROM_BYTES_PER_PAGE    16

/* SF One-byte Op-Codes */
#define  HV_EEPROM_READ    0x03  /* 0000 x011 Read data from memory array beginning at selected address */
#define  HV_EEPROM_WRITE   0x02  /* 0000 x010 Write data to memory array beginning at selected address */
#define  HV_EEPROM_WRDI    0x04  /* 0000 x100 Reset the write enable latch (disable write operations) */
#define  HV_EEPROM_WREN    0x06  /* 0000 x110 Set the write enable latch (enable write operations) */
#define  HV_EEPROM_RDSR    0x05  /* 0000 x101 Read STATUS register */
#define  HV_EEPROM_WRSR    0x01  /* 0000 x001 Write STATUS register */

/* STATUS Register Bits */
#define  HV_EEPROM_STAT_BIT_WIP    0x01  /* Write In Progress if 1 (read only) */
#define  HV_EEPROM_STAT_BIT_WEL    0x02  /* Write Enable Latch (read only) */
#define  HV_EEPROM_STAT_BIT_BP0    0x04  /* Block Protection Bits BP1/BP0 */
#define  HV_EEPROM_STAT_BIT_BP1    0x08  /* 00 (none), 01 (0xC0-0xFF), 10 (0x80-0xFF), 11 (all) */

/************************************************************/

/* SPI functions */
void spi_hv_init(unsigned int base_address);

/* LTC2656 functions */
void hv_ltc2656_write(plb_spi_salve_settings *slave_setting_ptr, unsigned char channel, unsigned char command, unsigned int voltage);
void hv_ltc2656_dac1_write(unsigned char channel, unsigned char command, float voltage);
void hv_ltc2656_dac2_write(unsigned char channel, unsigned char command, float voltage);
void hv_ltc2656_set(plb_spi_salve_settings *slave_setting_ptr, unsigned char channel, unsigned char command, float voltage);
void hv_ltc2656_dac1_set(unsigned char channel, unsigned char command, float voltage);
void hv_ltc2656_dac2_set(unsigned char channel, unsigned char command, float voltage);
void hv_ltc2656_cfg(plb_spi_salve_settings *slave_setting_ptr, unsigned char command);
void hv_ltc2656_dac1_cfg(unsigned char command);
void hv_ltc2656_dac2_cfg(unsigned char command);
void hv_ltc2656_init();

/* LTC2494 functions */
unsigned int hv_ltc2494_transceive(plb_spi_salve_settings *slave_setting_ptr, unsigned char *adc_config);
unsigned int hv_ltc2494_adc1_transceive(unsigned char *adc_config);
unsigned int hv_ltc2494_adc2_transceive(unsigned char *adc_config);
void hv_ltc2494_init();

/* LTC2601 functions */
void hv_ltc2601_init();
void hv_ltc2601_write(unsigned int command, unsigned int voltage);
void hv_ltc2601_set(unsigned int command, float voltage);

/* LTC2484 functions */
unsigned int hv_ltc2484_transceive(unsigned char adc_config);

/* MCP23S17 functions */
void hv_mcp23s17_init();
/*unsigned char hv_mcp23s17_transceive(unsigned char dev_opcode, unsigned char reg_address, unsigned char tx_byte); */
unsigned char hv_mcp23s17_write(unsigned char reg_address, unsigned char tx_byte);
unsigned char hv_mcp23s17_set(unsigned char reg_address, unsigned char tx_byte);
unsigned char hv_mcp23s17_clr(unsigned char reg_address, unsigned char tx_byte);
unsigned char hv_mcp23s17_read(unsigned char reg_address);

/* eeprom functions */
void hv_eeprom_transceive(unsigned char cmd, unsigned char addr, unsigned char* tx_buffer, unsigned char* rx_buffer, unsigned int nr_of_bytes);
unsigned char hv_eeprom_status_get();

#endif /* __DRV_HARD_SPI_HV_H__ */
