/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  24.04.2014 11:50:06
 *
 *  Description :  Test application for a command line interpreter
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xfs_printf.h"
#include "spi_cmd_input.h"
#include "register_map_wd2.h"
#include "cmd_processor.h"
#include "bin_cmd_processor.h"
#include "udp_terminal.h"
#include "system.h"
#include "dbg.h"
#include "ether_com.h"
#include "dhcp.h"
#include "update_config.h"
#include "plb_ll_fifo.h"
#include "xtime.h"
#include "crc32.h"
#include "xparameters.h"
#include "xil_cache.h"
#include "mscb.h"
#include "udp_pack_gen.h"
#include "fw_env.h"
#include "drv_plb_uart.h"
#include "gpio_slow_control.h"
#include "update_config.h"
#include "side_data.h"
#include "tftp_server.h"
#include "tftp_config.h"

/******************************************************************************/
/* constant definitions                                                       */
/******************************************************************************/

/* frame buffer size */
#define COM_BUFF_SER_LEN 255



/******************************************************************************/
/* global var definitions                                                     */
/******************************************************************************/
spi_cmd_input_type  spi_cmd_input;

/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

void nw_if_handler(network_if_type *nw_if)
{
  int k;
  unsigned int udp_port;

  if (nw_if->do_dhcp)
  {
    dhcp_request((dhcp_type*)(nw_if->dhcp));
  }

  tftp_check_timeout(SYSPTR(tftp_gmac_0));
/*  display_ether_status(); */

  k = nw_if->ll_fifo->receive(nw_if->ll_fifo, nw_if->com_buff, nw_if->com_buff_len);


  if (k > 4)
  {
    k -= 4; /* subtract FCS length */

    /* unsigned int crc; 
     * xfs_printf("received: %d bytes\r\n",k); 
     * print_frame(nw_if->com_buff, k); 
     * nw_if->ll_fifo->send(nw_if->com_buff, k); 
     * buff_insert_crc32(buffer, len-4); 
     * crc = crc32 (0, (const unsigned char *) nw_if->com_buff, k-4); 
     * xfs_printf("crc32 len-4 : 0x%08x\r\n",crc); 
     * crc = crc32 (0, (const unsigned char *) nw_if->com_buff, k); 
     * xfs_printf("crc32 len   : 0x%08x\r\n\r\n",crc); 
     */ 

    if (dhcp_handle_req((dhcp_type*)(nw_if->dhcp), nw_if->com_buff, k))
    {
      /* xfs_printf("handle_dhcp\r\n\r\n"); */
      /* nothing to do here */
    }

    else if (nw_if->ip_address_valid)
    {
      if (handle_arp(nw_if , nw_if->com_buff, k))
      {
        /* xfs_printf("ARP_REQUEST\r\n\r\n"); */
        /* nothing to do here */
      }
      else if (handle_ping(nw_if , nw_if->com_buff, k))
      {
        /* xfs_printf("handle_ping\r\n\r\n"); */
        /* nothing to do here */
      }
      else if ((udp_port = check_udp(nw_if , nw_if->com_buff, k)) > 0 )
      {
         /*  xfs_local_printf("UDP REQUEST udp_port = %d\r\n",udp_port);
          *  if ( udp_port == UPKG_UDP_LISTEN_PORT)
          *  {
          *    udp_pack_gen_gmac->handle_frame(nw_if , nw_if->com_buff, k);
          *  }
          */
          
         if ( udp_port == MSCB_UDP_LISTEN_PORT)
         {
           mscb_handler(nw_if , nw_if->com_buff, k);
         }
         else if ( udp_port == FC_UDP_TERMINAL_CMD_PORT)
         {
           udp_terminal_handle_request(nw_if , nw_if->com_buff, k);
         }
         else if ( udp_port == UDP_BINARY_CMD_PORT)
         {
           wd2_udp_bincmd_handle_request(nw_if, nw_if->com_buff, k);
         }
         else if (handle_tftp(SYSPTR(tftp_gmac_0) , nw_if->com_buff, k, udp_port))
         {
           gpio_sc_flash_sw_status(GPIO_LED_SW_STATUS_WDB_ACCESS);
           /* xfs_local_printf("handle_tftp\r\n\r\n"); */
           /* nothing to do here */
         }
      }
    }
  }
}

/******************************************************************************/

void mscb_uart_ping_chk()
{
  unsigned char data;
  int iterations = 256;
  int ping_received = 0;

  /* If there are bytes received by MSCB Uart (maximum 256 bytes) */
  while( plb_uart_receive(SYSPTR(MSCB_uart), &data, 1) && iterations)
  {
    /*xfs_printf("MSCB UART RX: 0x%02X\r\n",data); */
    /* Check if it's a ping request (0x1A, Addr MSB, Addr LSB, CRC) */
    if(data == 0x1A)
    {
      ping_received = 1;
      /* stop after reading 3 more bytes */
      iterations = 4;
    }
    iterations--;
  }

  /*data = 0x78;
  xfs_printf("MSCB UART TX (blind): 0x%02X\r\n",data);
  plb_uart_send(SYSPTR(MSCB_uart), &data, 1);
  */
  
  /* If yes, check if I am selected */
  if( ping_received ) /* && gpio_is_selected() )  */
  {
    /* If yes, respond (0x78) */
    xfs_printf("Ping\r\n"); /* to delay output */
    data = 0x78;
    plb_uart_send(SYSPTR(MSCB_uart), &data, 1);
    /*xfs_printf("MSCB UART TX: 0x%02X\r\n",data); */
  }

}

/******************************************************************************/

int main()
{
  int an_check = 0;
  unsigned int time_s;
  unsigned int time_temp_s = 0;
  unsigned int time_rate_s = 0;
  char old_running_status = 0;

#ifdef XPAR_MICROBLAZE_USE_ICACHE
  Xil_ICacheEnable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
  Xil_DCacheEnable();
#endif

  udp_terminal_init(); /* needed to make xfs_printf() work! */

  system_construct();

  xfs_printf("\r\n");
  if(DBG_ERR) xfs_printf("\r\n");

  gmac_lite_init(SYSPTR(gmac_lite_0)); /* init gmac lite before init_env */
  
  init_env();  /* initialize environment in order to set variables (e.g. debug level) */

  if (DBG_INIT)
  {
    xfs_printf("\r\n");
    xfs_printf("---------------------------------------------------------\r\n");
    xfs_printf("-- WaveDREAM2 Firmware                                 --\r\n");
    xfs_printf("---------------------------------------------------------\r\n\r\n");
  }
  system_init();
  if (DBG_INIT)
  {
    xfs_printf("\r\n");
    xfs_printf("---------------------------------------------------------\r\n");
    xfs_printf("-- Version Information:\r\n");
    print_sys_info();
    xfs_printf("---------------------------------------------------------\r\n\r\n");
  }

  term_cmd_input_init(SYSPTR(term_stdin), STDIN_BASEADDRESS);
  spi_cmd_input_init(&spi_cmd_input);

  /*bin_cmd_init(); */

  mscb_init();

/*------------------------------------------------------------------------------*/

  if (!SYSPTR(nw_if_gmac_0)->do_dhcp)
  {
    if (DBG_INIT) xfs_printf("Using Static IP Address: %d.%d.%d.%d\r\n",
              SYSPTR(nw_if_gmac_0)->ip_addr[0],SYSPTR(nw_if_gmac_0)->ip_addr[1],SYSPTR(nw_if_gmac_0)->ip_addr[2],SYSPTR(nw_if_gmac_0)->ip_addr[3]);
  }

/*------------------------------------------------------------------------------*/

  tftp_server_construct(SYSPTR(tftp_gmac_0), SYSPTR(nw_if_gmac_0), tftp_targets);

/*------------------------------------------------------------------------------*/

  /*init_default_udpgen_header(SYSPTR(nw_if_gmac_0)); */
  /*udp_pack_gen_gmac->udpgen_init_default_header(SYSPTR(nw_if_gmac_0)); */

/*------------------------------------------------------------------------------*/
  /*print_side_data_bank_def(); */
  if (DBG_INF4) xfs_printf("Entering main loop\r\n");
  gpio_sc_reset_sw_status();
  while(1)   /* main loop */
  {
    time_check_seconds();

    if (++an_check > 100)
    {
      an_check = 0;
    }

    /* event polling */
    char running = WD2_DRS_CTRL_BUSY_MASK & reg_bank_get(WD2_DRS_CTRL_BUSY_REG);
    if(running == 1 && old_running_status == 0){
      side_data_event();
    }
    old_running_status = running;

    time_s = time_get_sec();

    side_data_timer(time_s);
    /* update temperature status register about every 10 seconds */
    if(time_s-time_temp_s >= 10)
    {
      time_temp_s = time_s;
      update_temperature();
    }
    /* update count rate (scaler) status register about every second */
    if(time_s-time_rate_s >= 1)
    {
      time_rate_s = time_s;
      update_count_rate();
    }

    set_cmd_sender(XFS_PRINTF_TARGET_UDP);
    nw_if_handler(SYSPTR(nw_if_gmac_0));

    set_cmd_sender(XFS_PRINTF_TARGET_LOCAL);
    term_cmd_input_chk_cmd(SYSPTR(term_stdin));
    set_cmd_sender(XFS_PRINTF_TARGET_SPI);
    spi_cmd_input_chk_cmd(&spi_cmd_input);
    /* set_cmd_sender(???); */
    /* chk_binary_cmd(); */

    if (do_mscb) mscb_loop();
    
    auto_update_configurations();
    trigger_update_configurations();
  }

#ifdef XPAR_MICROBLAZE_USE_ICACHE
  Xil_ICacheDisable();
#endif
#ifdef XPAR_MICROBLAZE_USE_DCACHE
  Xil_DCacheDisable();
#endif

  return 0; /* never reached, just to avoid compiler warning */
}
