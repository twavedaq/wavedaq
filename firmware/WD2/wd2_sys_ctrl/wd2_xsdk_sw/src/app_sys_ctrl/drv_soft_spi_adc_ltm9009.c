/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  18.09.2019 16:59:35
 *
 *  Description :  Software controlled SPI controller for communication with
 *                 ADCs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "xtime.h"
#include "drv_soft_spi_adc_ltm9009.h"
#include "dbg.h"

/******************************************************************************/
/* Global Variables                                                           */
/******************************************************************************/

static unsigned int gpio_sclk  = 0;
static unsigned int gpio_slave = 0;
static unsigned int gpio_sdo   = 0;
static unsigned int gpio_sdi   = 0;

/******************************************************************************/

void ltm9009_soft_spi_adc_init(void)
{
  plb_gpio_write(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, SPI_LTM9009_CSA_0_N_O | SPI_LTM9009_CSB_0_N_O |
                                                      SPI_LTM9009_CSA_1_N_O | SPI_LTM9009_CSB_1_N_O);

  plb_gpio_tristate_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC,
                        SPI_LTM9009_CSA_0_N_O | SPI_LTM9009_CSB_0_N_O | SPI_LTM9009_SDI_0_O | SPI_LTM9009_SCK_0_O |
                        SPI_LTM9009_CSA_1_N_O | SPI_LTM9009_CSB_1_N_O | SPI_LTM9009_SDI_1_O | SPI_LTM9009_SCK_1_O);
  plb_gpio_tristate_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC,
                        SPI_LTM9009_SDOA_0_I | SPI_LTM9009_SDOB_0_I | SPI_LTM9009_SDOA_1_I | SPI_LTM9009_SDOB_1_I);

  ltm9009_reset();
}

/******************************************************************************/

static void soft_spi_adc_sck(unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sclk );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sclk );
  }
}

/******************************************************************************/

static void soft_spi_adc_cs_n(unsigned int slave, unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, slave );
  }
  else
  {
    gpio_sclk  = 0;
    gpio_slave = 0;
    gpio_sdo   = 0;
    gpio_sdi   = 0;
    if(slave & SPI_LTM9009_CSA_0_N_O)
    {
      gpio_sclk  |= SPI_LTM9009_SCK_0_O;
      gpio_slave |= SPI_LTM9009_CSA_0_N_O;
      gpio_sdo   |= SPI_LTM9009_SDI_0_O;
      gpio_sdi   |= SPI_LTM9009_SDOA_0_I;
    }
    if(slave & SPI_LTM9009_CSB_0_N_O)
    {
      gpio_sclk  |= SPI_LTM9009_SCK_0_O;
      gpio_slave |= SPI_LTM9009_CSB_0_N_O;
      gpio_sdo   |= SPI_LTM9009_SDI_0_O;
      gpio_sdi   |= SPI_LTM9009_SDOB_0_I;
    }
    if(slave & SPI_LTM9009_CSA_1_N_O)
    {
      gpio_sclk  |= SPI_LTM9009_SCK_1_O;
      gpio_slave |= SPI_LTM9009_CSA_1_N_O;
      gpio_sdo   |= SPI_LTM9009_SDI_1_O;
      gpio_sdi   |= SPI_LTM9009_SDOA_1_I;
    }
    if(slave & SPI_LTM9009_CSB_1_N_O)
    {
      gpio_sclk  |= SPI_LTM9009_SCK_1_O;
      gpio_slave |= SPI_LTM9009_CSB_1_N_O;
      gpio_sdo   |= SPI_LTM9009_SDI_1_O;
      gpio_sdi   |= SPI_LTM9009_SDOB_1_I;
    }

    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, slave );
  }
}

/******************************************************************************/

static void soft_spi_adc_sdo(unsigned int val)
{
  if (val)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sdo  );
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC, gpio_sdo  );
  }
}

/******************************************************************************/

static unsigned int soft_spi_adc_sdi()
{
  if (plb_gpio_get(SYSPTR(gpio_sys), GPIO_SOFT_SPI_ADC) & gpio_sdi )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

static void soft_spi_adc_wait(void)
{
  usleep(1);
}

/******************************************************************************/

static void soft_spi_adc_transmit(unsigned int slave, unsigned char* tx_buffer, unsigned int nr_of_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/
//  if(DBG_INFO) xfs_printf("SPI Transmit: slave = 0x%08X, bytes = %d\r\n", slave, nr_of_bytes);

  soft_spi_adc_cs_n(slave, 0);
  usleep(1);

  for (i=0; i<nr_of_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_adc_sck(0);
      soft_spi_adc_sdo(value & 0x80);
      soft_spi_adc_wait();
      soft_spi_adc_sck(1);
      soft_spi_adc_wait();
      value <<= 1;
    }
  }
  soft_spi_adc_sck(0);

  soft_spi_adc_wait();
  soft_spi_adc_cs_n(slave, 1);
  usleep(1);
}

/******************************************************************************/

static void soft_spi_adc_transceive(unsigned int slave, unsigned char* tx_buffer, unsigned char* rx_buffer, unsigned int nr_of_tx_bytes, unsigned int nr_of_rx_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_adc_cs_n(slave, 0);
  usleep(1);

  /* transmit */
  for (i=0; i<nr_of_tx_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_adc_sck(0);
      soft_spi_adc_sdo(value & 0x80);
      soft_spi_adc_wait();
      soft_spi_adc_sck(1);
      soft_spi_adc_wait();
      value <<= 1;
    }
    soft_spi_adc_sck(0);
    soft_spi_adc_sdo(0);
  }

  /* receive */
  for (i=0; i<nr_of_rx_bytes; i++)
  {
    value = 0;

    for (j=0; j<8; j++)
    {
      value <<= 1;
      soft_spi_adc_sck(0);
      soft_spi_adc_wait();
      /* Sample data prior to edge due to tristate timing of */
      /* the SDI line at the end of the transmission !!!     */
      value |= soft_spi_adc_sdi();
      soft_spi_adc_sck(1);
      soft_spi_adc_wait();
    }
    soft_spi_adc_sck(0);

    rx_buffer[i] = value;
  }

  soft_spi_adc_wait();
  soft_spi_adc_cs_n(slave, 1);
}

/******************************************************************************/
/******************************************************************************/

void ltm9009_write_single_cfg(unsigned int slave, unsigned char address, unsigned char value)
{
  unsigned char data[2];

  if( (address>=0x00) && (address<0x05) )
  {
    data[0] = LTM9009_CMD_WRITE | address;
    data[1] = value;

    soft_spi_adc_transmit(slave, data, 2);
  }
}

/******************************************************************************/

void ltm9009_write_cfg(unsigned int slave, unsigned int value)
{
  unsigned char data;
  unsigned char address;

  for( address=4 ; address>0 ; address-- )
  {
    data = (unsigned char)(value&0xFF);
    ltm9009_write_single_cfg(slave, address, data);
    value = value>>8;
  }
}

/******************************************************************************/

unsigned char ltm9009_read_single_cfg(unsigned int slave, unsigned char address)
{
  unsigned char tx_data;
  unsigned char rx_data;

  if( (address>0x00) && (address<0x05) )
  {
    tx_data = LTM9009_CMD_READ | address;
    soft_spi_adc_transceive(slave, &tx_data, &rx_data, 1, 1);
  }
  else
  {
    rx_data = 0;
  }

  return rx_data;
}

/******************************************************************************/

unsigned int ltm9009_read_cfg(unsigned int slave)
{
  unsigned int rx_data = 0;
  unsigned char address;

  for( address=1 ; address<5 ; address++ )
  {
    rx_data  = rx_data << 8;
    rx_data |= (unsigned int)(ltm9009_read_single_cfg(slave, address));
  }
  return rx_data;
}

/******************************************************************************/
/******************************************************************************/

void ltm9009_adc_0_1458_wr_cfg(unsigned int reg_value)
{
  ltm9009_write_cfg(SPI_LTM9009_CSA_0_N_O, reg_value);
}

/******************************************************************************/

void ltm9009_adc_0_2367_wr_cfg(unsigned int reg_value)
{
  ltm9009_write_cfg(SPI_LTM9009_CSB_0_N_O, reg_value);
}

/******************************************************************************/

void ltm9009_adc_1_1458_wr_cfg(unsigned int reg_value)
{
  ltm9009_write_cfg(SPI_LTM9009_CSA_1_N_O, reg_value);
}

/******************************************************************************/

void ltm9009_adc_1_2367_wr_cfg(unsigned int reg_value)
{
  ltm9009_write_cfg(SPI_LTM9009_CSB_1_N_O, reg_value);
}

/******************************************************************************/

unsigned int ltm9009_adc_0_1458_rd_cfg()
{
  unsigned int reg_value;

  reg_value = ltm9009_read_cfg(SPI_LTM9009_CSA_0_N_O);
  /*if(DBG_INFO) xfs_printf("LTM9009 0 Channels 1458 Register Read [0x%02X]: 0x%02X\r\n", address, reg_value); */

  return reg_value;
}

/******************************************************************************/

unsigned int ltm9009_adc_0_2367_rd_cfg()
{
  unsigned int reg_value;

  reg_value = ltm9009_read_cfg(SPI_LTM9009_CSB_0_N_O);
  /*if(DBG_INFO) xfs_printf("LTM9009 0 Channels 2367 Register Read [0x%02X]: 0x%02X\r\n", address, reg_value); */

  return reg_value;
}

/******************************************************************************/

unsigned int ltm9009_adc_1_1458_rd_cfg()
{
  unsigned int reg_value;

  reg_value = ltm9009_read_cfg(SPI_LTM9009_CSA_1_N_O);
  /*if(DBG_INFO) xfs_printf("LTM9009 1 Channels 1458 Register Read [0x%02X]: 0x%02X\r\n", address, reg_value); */

  return reg_value;
}

/******************************************************************************/

unsigned int ltm9009_adc_1_2367_rd_cfg()
{
  unsigned int reg_value;

  reg_value = ltm9009_read_cfg(SPI_LTM9009_CSB_1_N_O);
  /*if(DBG_INFO) xfs_printf("LTM9009 1 Channels 2367 Register Read [0x%02X]: 0x%02X\r\n", address, reg_value); */

  return reg_value;
}

/******************************************************************************/

void ltm9009_write_all_cfg(unsigned int value)
{
  ltm9009_write_cfg(SPI_LTM9009_CSA_0_N_O |
                    SPI_LTM9009_CSB_0_N_O |
                    SPI_LTM9009_CSA_1_N_O |
                    SPI_LTM9009_CSB_1_N_O,
                    value);
}

/******************************************************************************/

void ltm9009_reset()
{
  unsigned int adc_config[2][2]; /* ...[adc][channel] */

  /* backup configuration */
  adc_config[0][0] = ltm9009_read_cfg(SPI_LTM9009_CSA_0_N_O);
  adc_config[0][1] = ltm9009_read_cfg(SPI_LTM9009_CSB_0_N_O);
  adc_config[1][0] = ltm9009_read_cfg(SPI_LTM9009_CSA_1_N_O);
  adc_config[1][1] = ltm9009_read_cfg(SPI_LTM9009_CSB_1_N_O);

  /* reset */
  ltm9009_full_reset();

  /* restore configuration */
  ltm9009_write_cfg(SPI_LTM9009_CSA_0_N_O, adc_config[0][0]);
  ltm9009_write_cfg(SPI_LTM9009_CSB_0_N_O, adc_config[0][1]);
  ltm9009_write_cfg(SPI_LTM9009_CSA_1_N_O, adc_config[1][0]);
  ltm9009_write_cfg(SPI_LTM9009_CSB_1_N_O, adc_config[1][1]);
}

/******************************************************************************/

void ltm9009_full_reset()
{
  /* Write reset value 0xFF to reset register 0x00 */
  ltm9009_write_single_cfg(SPI_LTM9009_CSA_0_N_O |
                           SPI_LTM9009_CSB_0_N_O |
                           SPI_LTM9009_CSA_1_N_O |
                           SPI_LTM9009_CSB_1_N_O,
                           LTM9009_RESET_REG,
                           LTM9009_RESET_VAL);
}

/******************************************************************************/
/******************************************************************************/
