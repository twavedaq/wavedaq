/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  02.05.2014 13:24:35
 *
 *  Description :  Software controlled SPI controller for communication with
 *                 temperature sensor MAX6662, DAC LTC2600 and LMK03000.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_SOFT_SPI_TEMP_DAC_LMK_H__
#define __DRV_SOFT_SPI_TEMP_DAC_LMK_H__

/*
*  Format :
*           A3 A2 A1 A0             - Reg Address
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*
*  out_cfg: A4 A3 A2 A1 A0 D5 D4 D3 D2 D1 D0
*                                         ^lsb send first
*/

#define  SPI_TEMP_DAC_LMK_SCLK_O   0x01
#define  SPI_TEMP_DAC_LMK_SDI_O    0x02
#define  SPI_TEMP_DAC_LMK_SDO_I    0x04
#define  SPI_LMK_CS                0x08
#define  SPI_DAC_CS                0x10
#define  SPI_TEMP_CS               0x20

/************************************************************/
/* MAX31723 defines                                         */
/************************************************************/
#define MAX31723_CMD_READ       0x00
#define MAX31723_CMD_WRITE      0x80

#define MAX31723_ADR_CFG_STAT   0x00
#define MAX31723_ADR_TEMP_LSB   0x01
#define MAX31723_ADR_TEMP_MSB   0x02
#define MAX31723_ADR_THI_LSB    0x03
#define MAX31723_ADR_THI_MSB    0x04
#define MAX31723_ADR_TLO_LSB    0x05
#define MAX31723_ADR_TLO_MSB    0x06

#define MAX31723_CFG_SD         0x01
#define MAX31723_CFG_RES_9BIT   0x00
#define MAX31723_CFG_RES_10BIT  0x02
#define MAX31723_CFG_RES_11BIT  0x04
#define MAX31723_CFG_RES_12BIT  0x06
#define MAX31723_CFG_TM         0x08
#define MAX31723_CFG_1SHOT      0x10
#define MAX31723_CFG_NVB        0x20
#define MAX31723_CFG_MEMW       0x40

/************************************************************/

/************************************************************/
/* LMK03000 defines                                         */
/************************************************************/
#define  LMK03000_RESET                0x80
#define  LMK03000_CLKOUT_EN            0x01
#define  LMK03000_VBOOST               0x01
#define  LMK03000_DIV4                 0x80
#define  LMK03000_EN_FOUT              0x10
#define  LMK03000_EN_CLKOUT_GLOBAL     0x08
#define  LMK03000_POWERDOWN            0x04

/*#define  LMK03000_CLKOUT_MUX_MASK      0x06 */
/*#define  LMK03000_CLKOUT_DIV_MASK      0x7F */
/*#define  LMK03000_CLKOUT_DLY_MASK      0xF0 */
/*#define  LMK03000_OSC_IN_FREQ_1_MASK   0x3F */
/*#define  LMK03000_OSC_IN_FREQ_2_MASK   0xC0 */
/*#define  LMK03000_R4_LF_MASK           0x38 */
/*#define  LMK03000_R3_LF_MASK           0x07 */
/*#define  LMK03000_C3_C4_LF_MASK        0xF0 */
/*#define  LMK03000_PLL_MUX_MASK         0xF0 */
/*#define  LMK03000_PLL_R_1_MASK         0x0F */
/*#define  LMK03000_PLL_R_2_MASK         0xFF */
/*#define  LMK03000_PLL_CP_GAIN_MASK     0xC0 */
/*#define  LMK03000_VCO_DIV_MASK         0x3C */
/*#define  LMK03000_PLL_N_0_MASK         0x03 */
/*#define  LMK03000_PLL_N_1_MASK         0xFF */
/*#define  LMK03000_PLL_N_2_MASK         0xFF */

#define  LMK03000_REG0       0x00
#define  LMK03000_REG1       0x01
#define  LMK03000_REG2       0x02
#define  LMK03000_REG3       0x03
#define  LMK03000_REG4       0x04
#define  LMK03000_REG5       0x05
#define  LMK03000_REG6       0x06
#define  LMK03000_REG7       0x07
#define  LMK03000_REG8       0x08
#define  LMK03000_REG9       0x09
#define  LMK03000_REG11      0x0B
#define  LMK03000_REG13      0x0D
#define  LMK03000_REG14      0x0E
#define  LMK03000_REG15      0x0F

/************************************************************/

/************************************************************/
/* LTC2600 defines                                          */
/************************************************************/
/* DAC Commands */
#define  LTC2600_CMD_WR_N        0x00
#define  LTC2600_CMD_PU_N        0x10
#define  LTC2600_CMD_WR_N_PU_AL  0x20
#define  LTC2600_CMD_WR_PU_N     0x30
#define  LTC2600_CMD_PD_N        0x40
#define  LTC2600_CMD_NOP         0xF0

/* DAC Addresses */
#define  LTC2600_DAC_ADR_A       0x00
#define  LTC2600_DAC_ADR_B       0x01
#define  LTC2600_DAC_ADR_C       0x02
#define  LTC2600_DAC_ADR_D       0x03
#define  LTC2600_DAC_ADR_E       0x04
#define  LTC2600_DAC_ADR_F       0x05
#define  LTC2600_DAC_ADR_G       0x06
#define  LTC2600_DAC_ADR_H       0x07
#define  LTC2600_DAC_ADR_ALL     0x0F

/* DAC Parameters */
#define  LTC2600_VREF_MV         2500
#define  LTC2600_RES_NV         38147 /*voltage resolution [nV] = Vref*10^9/2^N = 2'500'000'000/65536 */

/************************************************************/

void soft_spi_temp_dac_lmk_init(void);

/* MAX31723 functions */
int max31723_get_temp();
void max31723_print_temp();

/* LMK03000 functions */
void lmk03000_reset();
void lmk03000_set_sync(int val);
void lmk03000_sync(void);
int  lmk03000_get_ld();
int lmk03000_set_channel(unsigned int value);
void lmk03000_set_clkout_en(unsigned int ch_nr, unsigned int clkout_en);
void lmk03000_set_vboost(unsigned int vboost);
void lmk03000_set_div4(unsigned int div4);
void lmk03000_set_loop_filter(unsigned int r4_lf, unsigned int r3_lf, unsigned int c3_c4_lf);
void lmk03000_set_oscin_freq(unsigned int oscin_freq);
void lmk03000_set_en_fout(unsigned int en_fout);
void lmk03000_set_en_clkout_global(unsigned int en_clkout_global);
void lmk03000_set_powerdown(unsigned int powerdown);
void lmk03000_set_pll_mux(unsigned int pll_mux);
unsigned char lmk03000_get_pll_mux();
void lmk03000_set_pll_r(unsigned int pll_r);
void lmk03000_set_pll_cp_gain(unsigned int pll_cp_gain);
void lmk03000_set_vco_div(unsigned int vco_div);
void lmk03000_set_pll_n(unsigned int pll_n);
void lmk03000_upload_configuration();
unsigned short ltc2600_bin_voltage(unsigned int voltage_nv);
void ltc2600_set(unsigned int channel, unsigned int command, unsigned int voltage_nv);
void ltc2600_write(unsigned int channel, unsigned int command, unsigned int voltage);

/************************************************************/
#endif /* __DRV_SOFT_SPI_TEMP_DAC_LMK_H__ */
/************************************************************/
