/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  02.02.2016 12:42:33
 *
 *  Description :  BER test for ADC interface on WaveDream2.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_PLB_ADCIF_BER_TEST_H__
#define __DRV_PLB_ADCIF_BER_TEST_H__

/******************************************************************************/

#include "utilities.h"

/******************************************************************************/

#define ADCIF_BER_TEST_REG_OFS_CONTROL         0x00000000
#define ADCIF_BER_TEST_REG_OFS_STATUS          0x00000004
#define ADCIF_BER_TEST_REG_BASE_OFS_ERROR      0x00000008
#define ADCIF_BER_TEST_REG_BASE_OFS_SAMPLE     0x00000048

#define ADCIF_BER_TEST_BIT_CTRL_ENABLE         0x00000001
#define ADCIF_BER_TEST_BIT_CTRL_RESET          0x00000002
#define ADCIF_BER_TEST_BIT_CTRL_INJECT_ERROR   0x00000004
#define ADCIF_BER_TEST_BIT_CTRL_PN_PATTERN     0x00000008
#define ADCIF_BER_TEST_BIT_STAT_RUNNING        0x00000001

typedef struct
{
    xfs_u32 base_address;
} plb_adcif_ber_test_type;

void plb_adcif_ber_test_init(plb_adcif_ber_test_type *self, xfs_u32 b_address);
void plb_adcif_ber_test_reset(plb_adcif_ber_test_type *self);
void plb_adcif_ber_test_start(plb_adcif_ber_test_type *self);
void plb_adcif_ber_test_stop(plb_adcif_ber_test_type *self);
xfs_u32 plb_adcif_ber_test_is_running(plb_adcif_ber_test_type *self);
void plb_adcif_ber_test_inject_error(plb_adcif_ber_test_type *self);
void plb_adcif_ber_test_set_pn_pattern(plb_adcif_ber_test_type *self, xfs_u32 pn_pattern);
void plb_adcif_ber_test_get_errors(plb_adcif_ber_test_type *self, xfs_u32 channel, xfs_u32* errors);
void plb_adcif_ber_test_get_samples(plb_adcif_ber_test_type *self, xfs_u32 channel, xfs_u32* samples);

/******************************************************************************/

#endif /* __DRV_PLB_ADCIF_BER_TEST_H__ */
