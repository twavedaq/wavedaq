/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  18.09.2019 16:59:20
 *
 *  Description :  Software controlled SPI controller for communication with
 *                 ADCs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __DRV_SOFT_SPI_ADC_H__
#define __DRV_SOFT_SPI_ADC_H__

/*
*  Format :
*           RNW                     - Read not(Write)
*           A6 A5 A4 A3 A2 A1 A0    - Reg Address
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*
*  out_cfg: RNW A6 A5 A4 A3 A2 A1 A0 D7 D6 D5 D4 D3 D2 D1 D0
*            ^msb send first
*/

/* LTM9009 latches SDI data on rising edge
 * SDO data is updated on the falling edge
 */

/* PORT IO_PORT6_IO = CSA_0_N_O & CSB_0_N_O & SDI_0_O & SDOA_0_I & SDOB_0_I & SCK_0_O & CSA_1_N_O & CSB_1_N_O & SDI_1_O & SDOA_1_I & SDOB_1_I & SCK_1_O */
#define SPI_LTM9009_SCK_1_O     0x0001
#define SPI_LTM9009_SDOB_1_I    0x0002
#define SPI_LTM9009_SDOA_1_I    0x0004
#define SPI_LTM9009_SDI_1_O     0x0008
#define SPI_LTM9009_CSB_1_N_O   0x0010
#define SPI_LTM9009_CSA_1_N_O   0x0020
#define SPI_LTM9009_SCK_0_O     0x0040
#define SPI_LTM9009_SDOB_0_I    0x0080
#define SPI_LTM9009_SDOA_0_I    0x0100
#define SPI_LTM9009_SDI_0_O     0x0200
#define SPI_LTM9009_CSB_0_N_O   0x0400
#define SPI_LTM9009_CSA_0_N_O   0x0800

/************************************************************/
/*LTM9009 defines                                           */
/************************************************************/
#define LTM9009_CMD_READ        0x80
#define LTM9009_CMD_WRITE       0x00

#define LTM9009_RESET_REG       0x00
#define LTM9009_RESET_VAL       0xFF

/*** constants **********************************************/

void ltm9009_soft_spi_adc_init(void);

/* LTM9009 functions */
void ltm9009_write_single_cfg(unsigned int slave, unsigned char address, unsigned char value);
void ltm9009_write_cfg(unsigned int slave, unsigned int value);
unsigned char ltm9009_read_single_cfg(unsigned int slave, unsigned char address);
unsigned int ltm9009_read_cfg(unsigned int slave);
void ltm9009_adc_0_1458_wr_cfg(unsigned int reg_value);
void ltm9009_adc_0_2367_wr_cfg(unsigned int reg_value);
void ltm9009_adc_1_1458_wr_cfg(unsigned int reg_value);
void ltm9009_adc_1_2367_wr_cfg(unsigned int reg_value);
unsigned int ltm9009_adc_0_1458_rd_cfg();
unsigned int ltm9009_adc_0_2367_rd_cfg();
unsigned int ltm9009_adc_1_1458_rd_cfg();
unsigned int ltm9009_adc_1_2367_rd_cfg();
void ltm9009_write_all_cfg(unsigned int value);
void ltm9009_reset();
void ltm9009_full_reset();
#endif /* __DRV_SOFT_SPI_ADC_H__ */
