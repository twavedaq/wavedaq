

#include "xparameters.h"
#include "system.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "ether_com.h"
#include "udp_terminal.h"
#include "cmd_processor.h"


udp_terminal_target_type udp_terminal_targets[NUM_UDP_TERMINAL_CMD_PORTS];
unsigned int udp_terminal_num = 0;


void udp_terminal_init(void)
{
  int i;

  for (i=0; i< NUM_UDP_TERMINAL_CMD_PORTS; i++)
  {
    udp_terminal_targets[i].valid = 0;
    udp_terminal_targets[i].current_target = 0;
  }

}

#define STR_BUFF_LEN 100

int udp_terminal_handle_request(network_if_type *nw_if, unsigned char *frame, int frame_len)
{
  int udp_payload_len ;
  unsigned char *udp_payload_ptr;
  int ip_already_in_list = 0;
  udp_header_type *rec_fp;
  int target_num;
  int current_terminal_num = 0;
  char str_buff[STR_BUFF_LEN];

  udp_payload_ptr = (unsigned char *)udp_payload_ptr_get(frame);
  udp_payload_len = udp_payload_len_get(frame);

  udp_payload_ptr[udp_payload_len] = 0;

  rec_fp = (udp_header_type *)frame;

  /*xfs_printf("\r\nUDP Received %d bytes: %s\r\n", udp_payload_len, udp_payload_ptr); */

  /* store ip addresses for terminal output */
  for (target_num=0; target_num < NUM_UDP_TERMINAL_CMD_PORTS; target_num++)
  {
    udp_terminal_targets[target_num].current_target = 0;
    if (    (udp_terminal_targets[target_num].valid)
         && (ncmp((char*)udp_terminal_targets[target_num].dst_ip,   (char*)rec_fp->src_ip,   4 ))
         && (ncmp((char*)udp_terminal_targets[target_num].dst_port, (char*)rec_fp->src_port, 2 )) )
    {
      ip_already_in_list = 1;
      udp_terminal_targets[target_num].current_target = 1;
      current_terminal_num = target_num;
    }
  }

  if (!ip_already_in_list)
  {
    current_terminal_num = udp_terminal_num;
    if ( udp_terminal_targets[udp_terminal_num].valid == 1)
    {
      xfs_snprintf(str_buff,STR_BUFF_LEN,"\r\n\r\n==== Terminal Disconnected due to request from %d.%d.%d.%d ====\r\n",rec_fp->src_ip[0],rec_fp->src_ip[1],rec_fp->src_ip[2],rec_fp->src_ip[3]);
      udp_terminal_output(current_terminal_num, str_buff, strlen(str_buff));
    }

    fcp(udp_terminal_targets[udp_terminal_num].dst_mac, rec_fp->src_mac);
    fcp(udp_terminal_targets[udp_terminal_num].dst_ip, rec_fp->src_ip);
    fcp(udp_terminal_targets[udp_terminal_num].dst_port, rec_fp->src_port);
    udp_terminal_targets[udp_terminal_num].nw_if = nw_if;
    udp_terminal_targets[udp_terminal_num].current_target = 1;
    udp_terminal_targets[udp_terminal_num].valid = 1;


    if (++udp_terminal_num >= NUM_UDP_TERMINAL_CMD_PORTS) udp_terminal_num = 0;
  }


  if ( !  (    ( (udp_payload_len ==1) && ( (udp_payload_ptr[0]==0xa) || (udp_payload_ptr[0]==0xd) ) )
            || ( (udp_payload_len ==2) &&   (udp_payload_ptr[0]==0xa) && (udp_payload_ptr[1]==0xd)   )
            || ( (udp_payload_len ==2) &&   (udp_payload_ptr[0]==0xd) && (udp_payload_ptr[1]==0xa)   ) ) )
  {
    if(DBG_INF2) xfs_printf("\r\n>>>>---- UDP Terminal %d.%d.%d.%d --------\r\n",rec_fp->src_ip[0],rec_fp->src_ip[1],rec_fp->src_ip[2],rec_fp->src_ip[3]);
    cmd_process((char *)udp_payload_ptr,udp_payload_len);
    if(DBG_INF2) xfs_printf("-------- UDP Terminal %d.%d.%d.%d ----<<<<\r\n",rec_fp->src_ip[0],rec_fp->src_ip[1],rec_fp->src_ip[2],rec_fp->src_ip[3]);
  }

  xfs_snprintf(str_buff,50,"%s > ",SYSPTR(cfg)->hostname);
  udp_terminal_output(current_terminal_num, str_buff, strlen(str_buff));

  return 1;
}

#define UDP_TERM_BUFFER_LEN 500
char udp_term_buff[UDP_TERM_BUFFER_LEN];


int udp_terminal_output(int terminal_num, char* buffer, int len)
{
  int target_num;
  udp_header_type *udp_fp = (udp_header_type *)udp_term_buff;

  int any_valid = 0;

  for (target_num=0; target_num< NUM_UDP_TERMINAL_CMD_PORTS; target_num++)
  {
    any_valid |= udp_terminal_targets[target_num].valid;
  }

/*  xfs_printf("any_valid = %d \r\n",any_valid); */
  if (!any_valid) return 0;



  for (target_num=0; target_num< NUM_UDP_TERMINAL_CMD_PORTS; target_num++)
  {
    if ((terminal_num == ALL_UDP_TERMINALS) || ( terminal_num == target_num))
    {
      if (udp_terminal_targets[target_num].valid)
      {
        if ( get_xfs_printf_targets() == XFS_PRINTF_TARGET_ALL || udp_terminal_targets[target_num].current_target == 1 )
        {
          prepare_udp_frame(udp_terminal_targets[target_num].nw_if, udp_fp);
          udp_fp->src_port[0] = FC_UDP_TERMINAL_CMD_PORT >> 0x08;
          udp_fp->src_port[1] = FC_UDP_TERMINAL_CMD_PORT  & 0xff;
          udp_fp->dst_port[0] = udp_terminal_targets[target_num].dst_port[0];
          udp_fp->dst_port[1] = udp_terminal_targets[target_num].dst_port[1];
          /*udp_fp->dst_port[0] = FC_UDP_TERMINAL_CMD_PORT >> 0x08; */
          /*udp_fp->dst_port[1] = FC_UDP_TERMINAL_CMD_PORT  & 0xff; */
  
          /* limit output to max term buffer size */
          if (len > (int)(UDP_TERM_BUFFER_LEN-UDP_HEADER_LEN)) len = (UDP_TERM_BUFFER_LEN-UDP_HEADER_LEN);
  
          /* copy string to udp buffer */
          ncpy((char*)&udp_term_buff[UDP_HEADER_LEN],buffer,len);
  
          /* insert mac and ip address */
          fcp(udp_fp->dst_mac,  udp_terminal_targets[target_num].dst_mac);
          fcp(udp_fp->dst_ip ,  udp_terminal_targets[target_num].dst_ip);
  
          udp_send_frame(udp_terminal_targets[target_num].nw_if, udp_fp, len);
          /*xfs_local_printf("udp_send_frame of len %d\r\n",len); */
        }
      }
    }
  }

  return 0;
}


#if 0
int udp_terminal_apply_dst_settings()
{
  int cmd_buf_size = 10;
  int name_buf_size = 12;
  int val_buf_size = 20;
  char command[cmd_buf_size];
  char name[name_buf_size];
  char value[val_buf_size];
  char *buffer_ptr[3] = {command, name, value};
  int target_num;

  xfs_snprintf(command, cmd_buf_size, "setenv\0");

  /* search for latest communication target */
  for (target_num=0; target_num < NUM_UDP_TERMINAL_CMD_PORTS; target_num++)
  {
    /* if target found, set com destination variables in environment */
    if ( udp_terminal_targets[target_num].valid && udp_terminal_targets[target_num].current_target )
    {
      /* set envirnment destination mac address */
      xfs_snprintf(name,  name_buf_size, "ethaddrdst\0");
      xfs_snprintf(value, val_buf_size,  "%02X:%02X:%02X:%02X:%02X:%02X\0", udp_terminal_targets[target_num].dst_mac[0],
                                                                            udp_terminal_targets[target_num].dst_mac[1],
                                                                            udp_terminal_targets[target_num].dst_mac[2],
                                                                            udp_terminal_targets[target_num].dst_mac[3],
                                                                            udp_terminal_targets[target_num].dst_mac[4],
                                                                            udp_terminal_targets[target_num].dst_mac[5]);
      xfs_local_printf("%s %s %s\r\n", command, name, value);
      fw_setenv(3, buffer_ptr);
      /* set envirnment destination ip address */
      xfs_snprintf(name,  name_buf_size, "ipaddrdst\0");
      xfs_snprintf(value, val_buf_size,  "%d.%d.%d.%d\0", udp_terminal_targets[target_num].dst_ip[0],
                                                          udp_terminal_targets[target_num].dst_ip[1],
                                                          udp_terminal_targets[target_num].dst_ip[2],
                                                          udp_terminal_targets[target_num].dst_ip[3]);
      xfs_local_printf("%s %s %s\r\n", command, name, value);
      fw_setenv(3, buffer_ptr);
      /* set envirnment udp destination port */
      /*
        xfs_snprintf(name,  name_buf_size, "dstport\0");
        xfs_snprintf(value, val_buf_size,  "%d\0",   ((unsigned int)(udp_terminal_targets[target_num].dst_port[0]) << 8)
                                                     |(unsigned int)(udp_terminal_targets[target_num].dst_port[1]));
        xfs_local_printf("%s %s %s\r\n", command, name, value);
        fw_setenv(3, buffer_ptr);
      */
      
      return 1;
    }
  }

  return 0;
}
#endif
