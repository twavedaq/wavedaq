

#ifndef  __UDP_TERMINAL_H__
#define  __UDP_TERMINAL_H__

#include "network_if.h"

#define FC_UDP_TERMINAL_CMD_PORT    3000
#define NUM_UDP_TERMINAL_CMD_PORTS     4
#define ALL_UDP_TERMINALS             -1

typedef struct
{
  /* ethternet frame  (14 byte) */
  unsigned char dst_mac[6];
  unsigned char dst_ip[4];
  unsigned char dst_port[2];
  network_if_type *nw_if;
  unsigned int current_target;
  unsigned int valid;
} udp_terminal_target_type;

extern udp_terminal_target_type udp_terminal_targets[NUM_UDP_TERMINAL_CMD_PORTS];

void udp_terminal_init(void);
int udp_terminal_handle_request(network_if_type *nw_if, unsigned char *frame, int frame_len);
int udp_terminal_output(int terminal_num, char* buffer, int len);
int udp_terminal_apply_dst_settings();

#endif /* __UDP_TERMINAL_H__ */

