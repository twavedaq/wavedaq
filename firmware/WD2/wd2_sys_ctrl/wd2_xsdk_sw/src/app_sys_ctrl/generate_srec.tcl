#! /usr/local/bin/tclsh
# Script to generate srec bootloader file from .elf
# containing version information
# generate_srec <executable.elf> <register_map_wd2.h>

# mb-objcopy Originally done in XSDK 14.7 app_sys_ctrl Post-build steps
# Properties: C/C++ Build->Settings->Build Steps
# Command:
# mb-objcopy -O srec ${BuildArtifactFilePrefix}${BuildArtifactFileName} app_sys_ctrl.srec
# Description:
# Generate SREC file for bootloader

# check elf name from command argument
puts " "
puts "*************************"
puts "Starting SREC generation"
puts " "

if { $argc != 3 } {
    puts "Invalid arguments"
    puts "Usage: generate_srec <WDB revision> <executable.elf> <register_map_wd2.h>"
    exit
}

set wdb_rev_def "WD2_REV_[string toupper [lindex $argv 0] ]"
puts "WDB revision $wdb_rev_def"
puts " "

puts "Checking files..."
set elf_file [lindex $argv 1]
set elf_file_name [file tail $elf_file]
set elf_path_name [file dirname $elf_file]
if { $elf_path_name == 0 } {
  set elf_path_name "."
}
#set elf_file "$elf_path_name/$elf_file_name"
set elf_file [file normalize $elf_path_name/$elf_file_name]
puts "ELF file:"
puts "$elf_file"
if { ![file exists $elf_file] } {
  puts "ERROR: File does not exist!"
  exit
}

set h_file_master [lindex $argv 2]
set h_file_master_name [file tail $h_file_master]
set h_path_name [file dirname $h_file_master]
if { $h_path_name == 0 } {
  set h_path_name "."
}
#set h_file_master "$h_path_name/$h_file_master_name"
set h_file_master [file normalize $h_path_name/$h_file_master_name]
puts "Master h File:"
puts "$h_file_master"
if { ![file exists $h_file_master] } {
  puts "ERROR: File does not exist!"
  exit
}

set h_file_name ""
set h_master_fp [open $h_file_master r]
while { [gets $h_master_fp current_line] != -1 } {
  if { [string match "*#ifdef*$wdb_rev_def*" $current_line] } {
    while { [gets $h_master_fp current_line] != -1 } {
      if { [string match "*#include*register_map_wd2_v*" $current_line] } {
        regexp {register_map_wd2_v\d+.h} $current_line h_file_name
        break
      } elseif { [string match "*#endif*" $current_line] } {
        puts "ERROR: Register map version not found in rev includes"
        exit
      }
    }
    if { $h_file_name != "" } {
      break
    }
  }
}
if { $h_file_name == "" } {
  puts "ERROR: Register map version not found in h file"
  exit
}

set h_file [file normalize $h_path_name/$h_file_name]
puts "h File:"
puts "$h_file"
if { ![file exists $h_file] } {
  puts "ERROR: File does not exist!"
  exit
}
puts " "


# get register layout compatibility level
# get firmware compatibility level
set reg_layout_comp_level 0
set firmware_comp_level   0

puts "Searching $h_file_name for version information..."
set h_fp [open $h_file r]
while { [gets $h_fp current_line] != -1 && ( $reg_layout_comp_level == 0 || $firmware_comp_level == 0 ) } {
  #puts $current_line
  if { [string match "*#define *WD2_REG_LAYOUT_VERSION_CONST*" $current_line] } {
    #puts $current_line
    foreach i [split $current_line] {
      if { [string match "0x*" $i] } {
        set reg_layout_comp_level $i
        puts "=> Register Layout Compatibility Level = $reg_layout_comp_level"
      }
    }
  }
  if { [string match "*#define *WD2_FW_COMPAT_LEVEL_CONST*" $current_line] } {
    #puts $current_line
    foreach i [split $current_line] {
      if { [string match "0x*" $i] } {
        set firmware_comp_level $i
        puts "=> Firmware Compatibility Level        =   $firmware_comp_level"
      }
    }
  }
}
puts " "

# exit if h-file does not contain values
if { $reg_layout_comp_level == 0 || $firmware_comp_level == 0 } {
  puts "ERROR: Values not found in $h_file file"
  exit
}

# build new filename
#set new_elf_file [ join [ list "$elf_path_name/" [file rootname $elf_file_name] "_fcl" $firmware_comp_level "_rcl" $reg_layout_comp_level ".elf" ] "" ]
puts "Building SREC file..."
set srec_file [ join [ list "$elf_path_name/" [file rootname $elf_file_name] ".srec" ] "" ]
set long_srec_file [ join [ list "$elf_path_name/" [file rootname $elf_file_name] "_fcl" $firmware_comp_level "_rcl" $reg_layout_comp_level ".srec" ] "" ]
#puts $long_srec_file

# copy and rename elf file
#file copy $elf_file $new_elf_file

# generate srec file using objcopy
file delete -force $long_srec_file
exec mb-objcopy -O srec $elf_file $long_srec_file

# rename srec file
puts "Removing temporary files..."
file delete -force $srec_file
file rename -force $long_srec_file $srec_file

# remove renamed elf file
#file delete -force $new_elf_file

puts " "
puts "Wrote generated SREC file to"
puts [file normalize $srec_file]
puts " "
puts "SREC generation completed"
puts "*************************"
puts " "