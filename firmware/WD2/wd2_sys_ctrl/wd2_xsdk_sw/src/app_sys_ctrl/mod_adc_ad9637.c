/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  23.09.2014 15:06:42
 *
 *  Description :  Module for ADC control.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifdef WD2_REV_F

#include "drv_drs4_control.h"
#include "xfs_printf.h"
#include "update_config.h"
#include "dbg.h"
#include "register_map_wd2.h"
#include "adc_serdes_calibration.h"
#include "system.h"
#include "drv_soft_spi_adc_ad9637.h"
#include "plb_serdes_cal_if.h"
#include "drv_plb_wd2_reg_bank.h"
#include <stdlib.h>
#include "cmd_processor.h"

/************************************************************/

  unsigned char get_di1(char *channel)
  {
    if( fstrcmp(channel,"a") )
    {
      return AD9637_CFG_DI1_CH_A;
    }
    else if( fstrcmp(channel,"b") )
    {
      return AD9637_CFG_DI1_CH_B;
    }
    else if( fstrcmp(channel,"c") )
    {
      return AD9637_CFG_DI1_CH_C;
    }
    else if( fstrcmp(channel,"d") )
    {
      return AD9637_CFG_DI1_CH_D;
    }
    else if( fstrcmp(channel,"e") )
    {
      return AD9637_CFG_DI1_CH_E;
    }
    else if( fstrcmp(channel,"f") )
    {
      return AD9637_CFG_DI1_CH_F;
    }
    else if( fstrcmp(channel,"g") )
    {
      return AD9637_CFG_DI1_CH_G;
    }
    else if( fstrcmp(channel,"h") )
    {
      return AD9637_CFG_DI1_CH_H;
    }
    else if( fstrcmp(channel,"fco") )
    {
      return AD9637_CFG_DI1_CH_FCO;
    }
    else if( fstrcmp(channel,"dco") )
    {
      return AD9637_CFG_DI1_CH_DCO;
    }
    else
    {
      return AD9637_CFG_DI1_ALL;
    }
  }

/************************************************************/

  unsigned char get_di2(char *channel)
  {
    if( fstrcmp(channel,"a") )
    {
      return AD9637_CFG_DI2_CH_A;
    }
    else if( fstrcmp(channel,"b") )
    {
      return AD9637_CFG_DI2_CH_B;
    }
    else if( fstrcmp(channel,"c") )
    {
      return AD9637_CFG_DI2_CH_C;
    }
    else if( fstrcmp(channel,"d") )
    {
      return AD9637_CFG_DI2_CH_D;
    }
    else if( fstrcmp(channel,"e") )
    {
      return AD9637_CFG_DI2_CH_E;
    }
    else if( fstrcmp(channel,"f") )
    {
      return AD9637_CFG_DI2_CH_F;
    }
    else if( fstrcmp(channel,"g") )
    {
      return AD9637_CFG_DI2_CH_G;
    }
    else if( fstrcmp(channel,"h") )
    {
      return AD9637_CFG_DI2_CH_H;
    }
    else if( fstrcmp(channel,"fco") )
    {
      return AD9637_CFG_DI2_CH_FCO;
    }
    else if( fstrcmp(channel,"dco") )
    {
      return AD9637_CFG_DI2_CH_DCO;
    }
    else
    {
      return AD9637_CFG_DI2_ALL;
    }
  }

/************************************************************/

  int adc_check_calibration(int argc, char **argv)
  {
    /* unsigned int data; */
    adc_eye_cal_type adc_cal;

    CMD_HELP("",
             "check if common local data clock phase is valid",
             "check the calibration of the pll phase delay of the common internal data clock"
             );

    /* Start the calibration */
    if(DBG_INFO) xfs_printf("Checking calibration...\r\n");
    ads_timing_cal(&adc_cal, ADC_CAL_MODE_CHECK, 1);

    return 0;
  }

/************************************************************/

  int adc_recalibrate(int argc, char **argv)
  {
    adc_eye_cal_type adc_cal;

//      xfs_printf("adcrecal:   recalibrate common local data clock phase\r\n");


    CMD_HELP("",
             "recalibrate common local data clock phase",
             "calibrate the pll phase delay of the common internal data clock"
            );

    /* Start the calibration */
    if(DBG_INFO) xfs_printf("Starting calibration...\r\n");
    ads_timing_cal(&adc_cal, ADC_CAL_MODE_CALIBRATE, 1);

    return 0;
  }

/************************************************************/

  int adc_set_tm(int argc, char **argv)
  {
    unsigned char channels = 0;
    unsigned char channels_a = 0;
    unsigned char channels_b = 0;
    unsigned int  mode;
    unsigned int  data;
    unsigned int  mask;

    CMD_HELP("<adc> <channel> <mode>",
             "set adc test patterns to specific adc/channel",
             "configures the specified ADC channels for the corresponding test mode.\r\n"
             "  <adc>     : adc to set pattern (0, 1, a, b, all)\r\n"
             "  <channel> : adc channel to set pattern (a, b, c, d, e, f, g, h, all)\r\n"
             "  <mode>    : hex adc test pattern code (see datasheet)\r\n"
             "              (multiple values possible separated by a space)\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 4)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    mode = (unsigned int)(hatoi((const char*)argv[argc-1]));
    /* shift mode bits in correct position and hide invalid settings with mask */
    mode = (mode << WD2_ADC_TM_OUT_0_A_OFS) & WD2_ADC_TM_OUT_0_A_MASK;


    for(int i=2;i<argc-1;i++)
    {
      if( fstrcmp(argv[i],"all") )
      {
        channels = 0xFF;
      }
      else if( ((unsigned char)argv[i][0] < 97) || ((unsigned char)argv[i][0] > 104) )
      {
        xfs_printf("E%02X: Invalid arguments. Type \"adcsettm help\" for information.\r\n", ERR_INVALID_ARGS);
      }
      else
      {
        channels |= 0x01 << ((unsigned char)argv[i][0]-97);
      }
    }

    if( fstrcmp(argv[1],"a") || fstrcmp(argv[1],"a"))
    {
      channels_a = channels;
    }
    else if( fstrcmp(argv[1],"b") || fstrcmp(argv[1],"1"))
    {
      channels_b = channels;
    }
    else if( fstrcmp(argv[1],"all") )
    {
      channels_a = channels;
      channels_b = channels;
    }

    mask = 0x01;
    for(int i=0;i<8;i++)
    {
      if(mask & channels_a)
      {
        reg_bank_read(WD2_REG_ADC_0_CH_A_CFG+i*4, &data, 1);
        data &= ~WD2_ADC_TM_OUT_0_A_MASK; /* clear current mode */
        data |= mode;                     /* set new mode */
        reg_bank_write(WD2_REG_ADC_0_CH_A_CFG+i*4, &data, 1);
      }
      if(mask & channels_b)
      {
        reg_bank_read(WD2_REG_ADC_1_CH_A_CFG+i*4, &data, 1);
        data &= ~WD2_ADC_TM_OUT_1_A_MASK; /* clear current mode */
        data |= mode;                     /* set new mode */
        reg_bank_write(WD2_REG_ADC_1_CH_A_CFG+i*4, &data, 1);
      }
      mask <<= 1;
    }

    return 0;
  }

/************************************************************/

  int adc_rst_tm(int argc, char **argv)
  {
    unsigned int  data;

    CMD_HELP("",
             "reset adc test patterns on all adcs/channels",
             "clears the test mode settings for all ADC channels\r\n"
             "to configure the ADCs for normal operation."
            );

    for(int i=0;i<8;i++)
    {
      /* Reset ADC A channel */
      reg_bank_read(WD2_REG_ADC_0_CH_A_CFG+i*4, &data, 1);
      data &= ~WD2_ADC_TM_OUT_0_A_MASK; /* clear current mode */
      reg_bank_write(WD2_REG_ADC_0_CH_A_CFG+i*4, &data, 1);
      /* Reset ADC B channel */
      reg_bank_read(WD2_REG_ADC_1_CH_A_CFG+i*4, &data, 1);
      data &= ~WD2_ADC_TM_OUT_1_A_MASK; /* clear current mode */
      reg_bank_write(WD2_REG_ADC_1_CH_A_CFG+i*4, &data, 1);
    }
    return 0;
  }

/************************************************************/

  int adc_get(int argc, char **argv)
  {
    unsigned int data;
    unsigned int no_words;
    unsigned int timeout;
    int adc_data;

    CMD_HELP("<nr_of_words> <adc_pattern>",
             "show a specified number of sampled data words",
             "reads the specified number of words from the ADC.\r\n"
             "Use adcsettm to configure specific channels for test outputs.\r\n"
             "    <nr_of_words> : number of data words to read (1..15)\r\n"
             "    <adc_pattern> : adc test pattern code (optional, see datasheet)\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    no_words = (unsigned int)(xfs_atoi(argv[1]));

    /* Setup ADC configuration */
    if(DBG_INFO) xfs_printf("\r\nSetting up ADC for calibration...\r\n");
    reg_bank_read(WD2_REG_ADC_USR_PATT, &data, 1);
    if(DBG_INFO) xfs_printf("ADC User Test Pattern: 0x%08X\r\n", data);
    /* Start the calibration */
    if(DBG_INFO) xfs_printf("Reading input data from ADC...\r\n");
    plb_serdes_cal_trigger_read(SYSPTR(serdes_cal_ctrl[0]), no_words);
    plb_serdes_cal_trigger_read(SYSPTR(serdes_cal_ctrl[1]), no_words);
    timeout = 1000000;
    while( !plb_serdes_cal_data_is_available(SYSPTR(serdes_cal_ctrl[1])) && timeout)
    {
      timeout--;
    }
    if(!timeout)
    {
      return 0;
    }
    for(unsigned int i=0;i<no_words;i++)
    {
      if (DBG_INFO) xfs_printf("\r\n");
      for(int adc=0; adc<2; adc++)
      {
        if (DBG_INFO) xfs_printf("ADC %d access number %d\r\n", adc, i);
        for(int adc_ch=0; adc_ch<8; adc_ch++)
        {
          adc_data = plb_serdes_cal_get(SYSPTR(serdes_cal_ctrl[adc]), adc_ch);
          if (DBG_INFO) xfs_printf("adc %d   channel %d   0x%04X\r\n", adc, adc_ch, adc_data);
        }
        adc_data = plb_serdes_cal_get(SYSPTR(serdes_cal_ctrl[adc]), 8);
        if (DBG_INFO) xfs_printf("adc %d   frame(0x%01X)  0x%04X\r\n", adc, (adc_data>>12)&0x0F, adc_data&0x0FFF);
        if( !plb_serdes_cal_update(SYSPTR(serdes_cal_ctrl[adc])) )
        {
          return 0;
        }
      }
    }

    return 0;
  }


/************************************************************/

  int adc_get_via_eth(int argc, char **argv)
  {
    CMD_HELP("",
             "transmit a data frame via ethernet",
             "reads one frame from the ADC and\r\n"
             "ransmits the corresponding data via ethernet.\r\n"
            );

    if(DBG_INFO) xfs_printf("\r\nSending one ADC data frame via ethernet.\r\n");
    drs4_start_daq();
    /* delay to the domino wave stabilize and    */
    /* make sure the drs fsm is ready to trigger */
    usleep(500);
    /* t >= 200us !!! */
    drs4_soft_trigger();

    return 0;
  }

/************************************************************/

/*  int adc_stop_eth_tx(int argc, char **argv)
 *  {
 *     CMD_HELP("",
 *              "stop drs data transmission via ethernet",
 *              "stops a running transmission of adc data via ethernet."
 *             );
 *
 *    SYSPPTR(udp_pack_gen_gmac)->stop();
 *    xfs_printf("\r\nEthernet transmission stopped\r\n");
 *
 *    return 0;
 *  }
 */

/************************************************************/

  int adc_read_cfg(int argc, char **argv)
  {
    unsigned int data;
    unsigned int address;

    CMD_HELP("<address> <channel>",
             "read the content of an adc configuration register",
             "Reads the adc confguration register at the specified address.\r\n"
             "  <address> : configuration register address of the adc (hex).\r\n"
             "  <channel> : local configuration register address of the adc (hex).\r\n"
             "              Of the corresponding adc channel (a,b,c,d,e,f,g,h,dco,fco).\r\n"
             "              Only for register addresses 0x0D, 0x10 and 0x14.\r\n"
             "              Omitting this parameter returns the global register content.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }
    else if(argc >= 3)
    {
      xfs_printf("\r\nConfiguring for local Register %S",argv[2]);
      ad9637_adc_01_wr_cfg(0x04, get_di2(argv[2]));
      ad9637_adc_01_wr_cfg(0x05, get_di1(argv[2]));
    }

    /* Read ADC configuration */
    address = (unsigned int)(hatoi((const char*)argv[1]));
    xfs_printf("\r\nReading ADC Register...\r\n");
    data = ad9637_adc_0_rd_cfg(address);
    xfs_printf("ADC 0 (A) Config Register [0x%04X] = 0x%02X\r\n", address, data);
    data = ad9637_adc_1_rd_cfg(address);
    xfs_printf("ADC 1 (B) Config Register [0x%04X] = 0x%02X\r\n\r\n", address, data);
    /* Reset ADC local channel selection */
    ad9637_adc_01_wr_cfg(0x04, AD9637_CFG_DI2_ALL);
    ad9637_adc_01_wr_cfg(0x05, AD9637_CFG_DI1_ALL);

    return 0;
  }

/************************************************************/

  int adc_sync(int argc, char **argv)
  {

    CMD_HELP("",
             "simultaneously resets the clock divider counter of both ADCs"
            );

    ad9637_sync();
    xfs_printf("\r\nSimultanoues ADC sync signal applied\r\n");

    return 0;
  }

/************************************************************/

  int adc_reset(int argc, char **argv)
  {
    CMD_HELP("",
             "initiates a reset of both ADCs"
            );

    ad9637_reset();
    xfs_printf("\r\nResetting ADCs\r\n");
    return 0;
  }

/************************************************************/

  int module_adc_help(int argc, char **argv)
  {
    CMD_HELP("","ADC interface functions");

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type adc_ad9637_cmd_table[] =
  {
    {0, "adc", module_adc_help},
    {0, "adccalchk", adc_check_calibration},
    {0, "adcrecal", adc_recalibrate},
    {0, "adcsettm", adc_set_tm},
    {0, "adcrsttm", adc_rst_tm},
    {0, "adcgettxt", adc_get},
    {0, "adcget", adc_get_via_eth},
    /* {"adcstopeth", adc_stop_eth_tx}, */
    {0, "adccrd", adc_read_cfg},
    {0, "adcsync", adc_sync},
    {0, "adcrst", adc_reset},
    {0, NULL, NULL}
  };

/************************************************************/

#endif /* WD2_REV_F */

/************************************************************/
