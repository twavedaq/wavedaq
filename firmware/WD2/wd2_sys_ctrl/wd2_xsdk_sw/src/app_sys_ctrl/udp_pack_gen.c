
/***************************** Include Files *********************************/

#include <stdio.h>

#include "xfs_printf.h"
#include "xparameters.h"
#include "ether_com.h"
#include "utilities.h"
#include "udp_pack_gen.h"
#include "dbg.h"

#define TESTGEN_HEADER_LEN_BYTES  8

/************************** Variable Definitions *****************************/



/*****************************************************************************/
/* EMAC functions                                                            */
/*****************************************************************************/

void udp_pack_gen_construct(udp_pack_gen_type *self, unsigned int plb_pram_address_i)
{
  self->plb_pram_address = plb_pram_address_i;
}


/*****************************************************************************/

void udpgen_init_default_header(udp_pack_gen_type *self, network_if_type *nw_if, unsigned int src_port, network_target_type *target)
{
  unsigned int header_num = 0;
  udp_gen_header_entry_type udp_gen_entry;
  char buff[40];
  
  xfs_local_printf("Src MAC Address     : %s\r\n", sprint_mac(buff, nw_if->mac_addr));
  xfs_local_printf("Src IP  Address     : %s\r\n", sprint_ip(buff, nw_if->ip_addr));
  xfs_local_printf("Src Port            : %d\r\n", src_port);

  xfs_local_printf("Dst MAC Address     : %s\r\n", sprint_mac(buff, target->dst_mac_addr));
  xfs_local_printf("Dst IP  Address     : %s\r\n", sprint_ip(buff, target->dst_ip_addr));
  xfs_local_printf("Dst Port            : %d\r\n", target->dst_port);


/*  for (header_num=0; header_num < 64; header_num++) */
/*  { */
    udpgen_create_header_entry(self, &udp_gen_entry,
                                      nw_if->mac_addr,      nw_if->ip_addr,      src_port,
                                      target->dst_mac_addr, target->dst_ip_addr, target->dst_port);

    udpgen_header_to_plb_bram(self, &udp_gen_entry, header_num);
/*  } */
}

/*****************************************************************************/

void udpgen_adjust_header_entry(udp_pack_gen_type *self, udp_gen_header_entry_type *ip)
{
  ip->total_length[0] = 0;
  ip->total_length[1] = 28; /* IP + UDP Header Length */

  adjust_ip_checksum((udp_header_type*)ip);

  /* pre-calculated checksum is not inverted as ip-checksum */
  /* so invert back again */
  ip->ip_header_checksum[0] = ~ip->ip_header_checksum[0];
  ip->ip_header_checksum[1] = ~ip->ip_header_checksum[1];
}

/******************************************************************************/

void udpgen_create_header_entry(udp_pack_gen_type *self,
                                udp_gen_header_entry_type *udp_gen_entry,
                                const unsigned char *src_mac,
                                const unsigned char *src_ip,
                                unsigned int  src_port,
                                const unsigned char *dst_mac,
                                const unsigned char *dst_ip,
                                unsigned int  dst_port)
{
  memset(udp_gen_entry, 0, sizeof(udp_gen_header_entry_type));

  ncpy(udp_gen_entry->src_mac, src_mac, 6);
  ncpy(udp_gen_entry->dst_mac, dst_mac, 6);

  udp_gen_entry->len_type[0]       = 0x08;
/*  udp_gen_entry->len_type[1]       = 0x00; */
  udp_gen_entry->ver_headerlen[0]  = 0x45;
  udp_gen_entry->service_type[0]   = 0x00;
/*  udp_gen_entry->total_length[0]   = 0; */
  udp_gen_entry->total_length[1]   = 28; /* IP + UDP Header Length */

/*  udp_gen_entry->identification[0] = 0x00; */
/*  udp_gen_entry->identification[1] = 0x00; */
  udp_gen_entry->flags[0]          = 0x40;
/*  udp_gen_entry->frag_offset[0]    = 0x00; */
  udp_gen_entry->time_to_live[0]   = 0x40;
  udp_gen_entry->protocol[0] = 0x11; /* UDP protocol */
/*  udp_gen_entry->ip_header_checksum[0] */ /* adjusted later */
/*  udp_gen_entry->ip_header_checksum[1] */ /* adjusted later */
  ncpy(udp_gen_entry->src_ip, src_ip, 4);
  ncpy(udp_gen_entry->dst_ip, dst_ip, 4);

/* udp header */
  udp_gen_entry->src_port[0] = (src_port >> 8) & 0xff;
  udp_gen_entry->src_port[1] =  src_port & 0xff;
  udp_gen_entry->dst_port[0] = (dst_port >> 8) & 0xff;
  udp_gen_entry->dst_port[1] =  dst_port & 0xff;

  udpgen_adjust_header_entry(self, udp_gen_entry);
}

/******************************************************************************/

void udpgen_header_to_plb_bram(udp_pack_gen_type *self, udp_gen_header_entry_type *udp_gen_entry, unsigned int header_num)
{
  volatile unsigned int  *plb_bram_ptr = (volatile unsigned int *)self->plb_pram_address;
  unsigned char *cptr  = (unsigned char *) udp_gen_entry;
  unsigned int k, num_words;

  num_words = sizeof(udp_gen_header_entry_type)/4;

  for (k=0; k<num_words; k++)
  {
    plb_bram_ptr[(header_num * 16) + k] = (cptr[4*k]<<24) | (cptr[4*k+1]<<16) | (cptr[4*k+2]<<8) | cptr[4*k+3];
  }
}

/*****************************************************************************/
