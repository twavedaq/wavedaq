/*
 *
 * Xilinx, Inc.
 * XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS" AS A
 * COURTESY TO YOU.  BY PROVIDING THIS DESIGN, CODE, OR INFORMATION AS
 * ONE POSSIBLE   IMPLEMENTATION OF THIS FEATURE, APPLICATION OR
 * STANDARD, XILINX IS MAKING NO REPRESENTATION THAT THIS IMPLEMENTATION
 * IS FREE FROM ANY CLAIMS OF INFRINGEMENT, AND YOU ARE RESPONSIBLE
 * FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE FOR YOUR IMPLEMENTATION
 * XILINX EXPRESSLY DISCLAIMS ANY WARRANTY WHATSOEVER WITH RESPECT TO
 * THE ADEQUACY OF THE IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO
 * ANY WARRANTIES OR REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE
 * FROM CLAIMS OF INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.
 */

/*
 * Xilinx EDK 11.3 EDK_LS3.57
 *
 * This file is a sample test application
 *
 * This application is intended to test and/or illustrate some
 * functionality of your system.  The contents of this file may
 * vary depending on the IP in your system and may use existing
 * IP driver functions.  These drivers will be generated in your
 * XPS project when you run the "Generate Libraries" menu item
 * in XPS.
 *
 * Your XPS project directory is at:
 *    /afs/psi.ch/project/genie1414/work/PilatusXFS/firmware/backend_board/edk11.3_hello_world/
 */


/* Located in: ppc440_0/include/xparameters.h */

#include "xparameters.h"
#include "stdio.h"
#include "xfs_printf.h"
#include "xtime.h"

#if 0
  #define xfs_printf(...)
  #define print(...)
#endif


/*****************************************************************************/

int memtest_8bit (unsigned int do_print, unsigned int start_adr, unsigned size)
{
  unsigned int count;
  unsigned int error;
  unsigned int i;
  volatile unsigned char* ptr;
  ptr = (unsigned char*)start_adr;

  count = size;
  error = 0;

  for(i=0;i<count;i++)
  {
    ptr[i] = ~i;
  }

  for(i=0;i<count;i++)
  {
    if (ptr[i] != ((~i)&0xff))
    {
      if (do_print) xfs_printf("Error: [0x%08x] = 0x%02x\r\n",&ptr[i],ptr[i]);
      error++;
    }
  }

  if (do_print) xfs_printf("memtest_8bit  done: errors = %d\r\n",error);

  return error;
}

/*****************************************************************************/


int memtest_16bit (unsigned int do_print, unsigned int start_adr, unsigned size)
{
  unsigned int count;
  unsigned int error;
  unsigned int i;
  volatile unsigned short* ptr;

  ptr = (unsigned short*)start_adr;

  count = size / 2;
  error = 0;
  for(i=0;i<count;i++)
  {
    ptr[i] = ~i;
  }

  for(i=0;i<count;i++)
  {
    if (ptr[i] != ((~i)&0xffff))
    {
      if (do_print) xfs_printf("Error: [0x%08x] = 0x%04x\r\n",&ptr[i],ptr[i]);
      error++;
    }
  }

  if (do_print) xfs_printf("memtest_16bit done: errors = %d\r\n",error);

  return error;
}

/*****************************************************************************/

int memtest_32bit (unsigned int do_print, unsigned int start_adr, unsigned size)
{
  unsigned int count;
  unsigned int error;
  unsigned int i;
  volatile unsigned int* ptr;
  volatile unsigned int read_val;
  volatile unsigned int cmp_val;

  ptr = (unsigned int*)start_adr;
  count = size / 4;
  error = 0;
  for(i=0;i<count;i++)
  {
    ptr[i] = ~i;
  }

  for(i=0;i<count;i++)
  {
    read_val = ptr[i];
    cmp_val = (~i);
    if (read_val != cmp_val)
    {
      if (do_print) xfs_printf("Error: [0x%08x] = 0x%08x (0x%08x expected) fail mask: 0x%08x\r\n",&ptr[i],read_val,cmp_val,(read_val^cmp_val));
      error++;
    }
  }

  if (do_print) xfs_printf("memtest_32bit done: errors = %d\r\n",error);

  return error;
}

#if 1
int memtest (unsigned int do_print, unsigned int start_adr, unsigned size)
{
  static int error_p = 0;
  static int error_t = 0;

/*  xfs_printf("\r\n\r\n************************\r\n");
  xfs_printf(" memtest\r\n startaddr :  0x%08x\r\n size     :  0x%08x\r\n %s %s\r\n", start_adr, size, __DATE__,__TIME__);
  xfs_printf("************************\r\n");
*/

   /*
    * Disable Cache
    */
   /*XCache_DisableDCache(); */
   /*XCache_DisableICache(); */

  xfs_printf("\r\n");

  xfs_printf("Testing RAM with 32bit access...\r\n");
  error_p = memtest_32bit(do_print, start_adr, size);
  error_t += error_p;
  xfs_printf("Testing RAM with 16bit access...\r\n");
  error_p = memtest_16bit(do_print, start_adr, size);
  error_t += error_p;
  xfs_printf("Testing RAM with 8bit access...\r\n");
  error_p = memtest_8bit(do_print, start_adr, size);
  error_t += error_p;

    if (do_print)
    {
      /*xfs_printf("Done pass %4d: errors = %d  total errors = %d\r\n",pass,error_p,error_t); */
      xfs_printf("Memtest Done :   total errors = %d\r\n\r\n",error_t);
    }

  /*
   * Enable Cache
   */
  /*XCache_EnableICache(0x00000001);     */
  /*XCache_EnableDCache(0x00000001);     */
  /*XCache_EnableICache(I_CACHE_REGION); */
  /*XCache_EnableDCache(D_CACHE_REGION); */

  return error_t;
}
#endif

#if 0
int memtest (unsigned int do_print, unsigned int start_adr, unsigned size)
{
  unsigned int i;
  volatile unsigned char* c_ptr;
  volatile unsigned short* s_ptr;
  volatile unsigned int* i_ptr;

/*  static int error_p = 0; */
static int error_t = 0;

  xfs_printf("\r\n\r\n************************\r\n");
  xfs_printf(" memtest\r\n startaddr :  0x%08x\r\n size     :  0x%08x\r\n %s %s\r\n", start_adr, size, __DATE__,__TIME__);
  xfs_printf("************************\r\n");

  c_ptr = (unsigned char*)XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR;
  s_ptr = (unsigned short*)XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR;
  i_ptr = (unsigned int*)XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR;

   /*
    * Disable Cache
    */
  /*XCache_DisableDCache();  */
  /*XCache_DisableICache();  */

  xfs_printf("\r\n");

    xfs_printf("\r\nWriting\r\n");
    for(i=0;i<64;i++)
    {
      i_ptr[i] = ~i;
    }
    xfs_printf("\r\nReading:\r\n");
    for(i=0;i<16;i++)
    {
      xfs_printf("c_ptr[%d] (0x%08X) = 0x%02X\r\n",i,&c_ptr[i],c_ptr[i]);
    }
    xfs_printf("\r\n");
    for(i=0;i<128;i++)
    {
      xfs_printf("s_ptr[%d] (0x%08X) = 0x%04X\r\n",i,&s_ptr[i],s_ptr[i]);
    }
    xfs_printf("\r\n");
    for(i=0;i<64;i++)
    {
      xfs_printf("i_ptr[%d] (0x%08X) = 0x%08X\r\n",i,&i_ptr[i],i_ptr[i]);
    }
    xfs_printf("\r\n");

#if 0
    error_p = memtest_32bit(do_print, start_adr, size);
    error_t += error_p;
    error_p = memtest_16bit(do_print, start_adr, size);
    error_t += error_p;
    error_p = memtest_8bit(do_print, start_adr, size);
    error_t += error_p;

    if (do_print)
    {
/*      xfs_printf("Done pass %4d: errors = %d  total errors = %d\r\n",pass,error_p,error_t); */
      xfs_printf("Memtest Done :   total errors = %d\r\n",error_t);
    }

#endif

   /*
    * Enable Cache
    */
/*  XCache_EnableICache(0x00000001);     */
/*  XCache_EnableDCache(0x00000001);     */
/*  XCache_EnableICache(I_CACHE_REGION); */
/*  XCache_EnableDCache(D_CACHE_REGION); */

  return error_t;
}
#endif
