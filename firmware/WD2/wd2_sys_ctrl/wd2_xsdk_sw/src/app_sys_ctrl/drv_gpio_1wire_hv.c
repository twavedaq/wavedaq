/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  27.10.2015 13:44:40
 *
 *  Description :  GPIO software interface to control 1Wire interface on HV board.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xparameters.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "xfs_printf.h"
#include "drv_gpio_1wire_hv.h"

/******************************************************************************/

unsigned int hv_ds28ea00_get_input()
{
  if(HV_1WIRE_IO & plb_gpio_get(SYSPTR(gpio_sys), GPIO_HV_BOARD))
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void hv_ds28ea00_set_output(unsigned char value)
{
  if(value)
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_IO);
  }
  else
  {
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_IO);
  }
}

/******************************************************************************/

void hv_ds28ea00_set_tristate(unsigned char value)
{
  if(value)
  {
    plb_gpio_tristate_set(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_IO);
    plb_gpio_clr(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_DIR_O); /* output disable (if Buffer EN pin) */
    /*plb_gpio_set(GPIO_HV_BOARD, HV_1WIRE_DIR_O);  */ /* output disable (if Buffer EN_N pin) */
  }
  else
  {
    plb_gpio_set(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_DIR_O); /* output enable (if Buffer EN pin) */
    /*plb_gpio_clr(GPIO_HV_BOARD, HV_1WIRE_DIR_O); */ /* output enable (if Buffer EN_N pin) */
    plb_gpio_tristate_clr(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_IO);
  }
}

/******************************************************************************/

void hv_ds28ea00_init()
{
  plb_gpio_tristate_clr(SYSPTR(gpio_sys), GPIO_HV_BOARD, HV_1WIRE_DIR_O);
  hv_ds28ea00_set_tristate(1); /* configure as input */
  hv_ds28ea00_set_output(0);   /* set low */
}

/******************************************************************************/
/******************************************************************************/
