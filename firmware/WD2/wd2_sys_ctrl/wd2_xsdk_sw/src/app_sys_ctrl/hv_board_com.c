/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  29.10.2014 11:18:30
 *
 *  Description :  High level functions for communication with
 *                 HV board devices.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "hv_board_com.h"
#include "drv_hard_spi_hv.h"
#include "drv_gpio_1wire_hv.h"
#include "register_map_wd2.h"
#include "system.h"
#include "stdio.h"
#include "xtime.h"
#include "dbg.h"

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/
unsigned int switch_states = 0x00000000; /* 0 = off, 1 = on */

/******************************************************************************/

void hv_init()
{
  hv_ds28ea00_init();
  hv_ltc2656_init();
  hv_ltc2494_init();
  hv_ltc2601_init();
  hv_mcp23s17_init();
}

/******************************************************************************/

void hv_channel_switch(unsigned char channel, unsigned char flag)
{
  volatile unsigned char olatb;

  if(flag)
  {
    switch_states |= (0x01<<channel);
  }
  else
  {
    switch_states &= ~(0x01<<channel);
  }

  /* read olatb */
  olatb =  hv_mcp23s17_read(MCP23S17_OLATB_WD2_REG_ADR);

  /* modify olatb (set IN1 and IN2) */
  olatb &= ~(MCP23S17_GPB_IN1_O | MCP23S17_GPB_IN2_O);
  olatb |= ( 0x03 & ( switch_states>>(channel&(~0x00000001)) ) );

  /* write olatb */
  hv_mcp23s17_write(MCP23S17_OLATB_WD2_REG_ADR, olatb);

  /* unlatch CPC channel */
  hv_mcp23s17_write(MCP23S17_OLATA_WD2_REG_ADR, ~(0x01<<(channel/2)));

  /* latch CPC channel */
  hv_mcp23s17_write(MCP23S17_OLATA_WD2_REG_ADR, 0xFF);
}

/******************************************************************************/

int hv_dac_set(unsigned char channel, float voltage)
{
  if(channel > 15)
  {
    set_error_flag(ERR_WD2_HV);
    return 0;
  }
  else if(channel > 7)
  {
    hv_ltc2656_dac2_set(channel-8, LTC2656_CMD_WRN_PUN, voltage);
    return 1;
  }
  else
  {
    hv_ltc2656_dac1_set(channel, LTC2656_CMD_WRN_PUN, voltage);
    return 1;
  }
}

/******************************************************************************/

int gain[8] = { 1, 4, 8, 16, 32, 64, 128, 256 };

int hv_c_voltage_read(unsigned char *channel, float *voltage)
{
   static int gain_index[16] = { 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0 };
   static int current_channel1 = 0, current_channel2 = 0, adc_index = 0;

   int voltage_value, invalid, overflow;
   unsigned int adc_value, c;
   unsigned char cmd[2];

   if (adc_index == 0) {
      /* if EOC, address next channel to be started */
      c = (current_channel1 + 1) % 8;
      cmd[0] = LTC2494_CMD_PREAMBLE | LTC2494_CMD_EN | LTC2494_CMD_ODD | (c & 0x07);
      cmd[1] = LTC2494_CMD_EN2 | LTC2494_CMD_RJ50HZ | gain_index[c];
      adc_value = hv_ltc2494_adc1_transceive(cmd);
      invalid = overflow = 0;

      if ((adc_value & LTC2494_STAT_EOC_N) == 0) {
         voltage_value = (int) ((adc_value >> 4) & 0x0001FFFF);

         /* expand negative two's complement */
         if ((adc_value & LTC2494_STAT_SIG) == 0)
            voltage_value |= 0xFFFE0000;

         /* overflow bit -> reduce gain */
         if (adc_value & LTC2494_STAT_MSB && voltage_value > 0) {
            invalid = 1;
            if (gain_index[current_channel1] > 0) {
               gain_index[current_channel1]--;
               /*if (current_channel1 == 0) */
               /*   printf("Set gain CH%02d to %3d\r\n", current_channel1, gain[gain_index[current_channel1]]); */
            } else {
               overflow = 1;
            }
         }

         /* small value -> increase gain */
         if (voltage_value < 0x3000 && voltage_value > -0x3000) {
            if (gain_index[current_channel1] < 7) {
               gain_index[current_channel1]++;
               /*if (current_channel1 == 0) */
               /*   printf("Set gain CH%02d to %3d\r\n", current_channel1, gain[gain_index[current_channel1]]); */
               invalid = 1;
            }
         }

         *voltage = (((float)voltage_value * LTC2494_RES_V) + LTC2494_VCOM_V) /
               (float)gain[gain_index[current_channel1]];
         *channel = current_channel1;


         /*if (current_channel1 == 0)
            printf("CH%02d ADC_VALUE=0x%08X VOLTAGE_VALUE=%d(%04X) invalid=%d U=%lf\r\n",
                   *channel, adc_value, voltage_value, voltage_value, invalid, *voltage);
         */
         
         /* switch to next channel */
         current_channel1 = (current_channel1 + 1) % 8;

         /* switch to next ADC */
         adc_index = (adc_index + 1) % 2;

         if (overflow) /* signal valid overflow value of 125 uA */
            return 1;

         if (invalid)
            return 0;

         return 1;
      }
   } else {
      /* if EOC, address next channel to be started */
      c = (current_channel2 + 1) % 8;
      cmd[0] = LTC2494_CMD_PREAMBLE | LTC2494_CMD_EN | LTC2494_CMD_ODD | (c & 0x07);
      cmd[1] = LTC2494_CMD_EN2 | LTC2494_CMD_RJ50HZ | gain_index[c+8];
      adc_value = hv_ltc2494_adc2_transceive(cmd);
      invalid = overflow = 0;

      if ((adc_value & LTC2494_STAT_EOC_N) == 0) {
         voltage_value = (int) ((adc_value >> 4) & 0x0001FFFF);

         /* expand negative two's complement */
         if ((adc_value & LTC2494_STAT_SIG) == 0)
            voltage_value |= 0xFFFE0000;

         /* overflow bit -> reduce gain */
         if (adc_value & LTC2494_STAT_MSB && voltage_value > 0) {
            invalid = 1;
            if (gain_index[current_channel2+8] > 0) {
               gain_index[current_channel2+8]--;
               /*printf("Set gain CH%02d to %3d\r\n", current_channel2+8, gain[gain_index[current_channel2+8]]); */
            } else
               overflow = 1;
         }

         /* small value -> increase gain */
         if (voltage_value < 0x3000 && voltage_value > -0x3000) {
            if (gain_index[current_channel2+8] < 7) {
               gain_index[current_channel2+8]++;
               /*printf("Set gain CH%02d to %3d\r\n", current_channel2+8, gain[gain_index[current_channel2+8]]); */
               invalid = 1;
            }
         }

         *voltage = (((float)voltage_value * LTC2494_RES_V) + LTC2494_VCOM_V) /
               (float)gain[gain_index[current_channel2+8]];
         *channel = current_channel2+8;

         /* switch to next channel */
         current_channel2 = (current_channel2 + 1) % 8;

         /* switch to next ADC */
         adc_index = (adc_index + 1) % 2;

         if (overflow) /* signal valid overflow value of 125 uA */
            return 1;

         if (invalid)
            return 0;

         return 1;
      }
   }

   /* switch to next ADC */
   adc_index = (adc_index + 1) % 2;

   return 0;
}

/******************************************************************************/

int hv_test_c_voltage_read(unsigned char channel, float *voltage, int gain_index)
{
   static int voltage_value;

   unsigned int adc_value;
   unsigned char cmd[2];

   /* if EOC, address next channel to be started */
   cmd[0] = LTC2494_CMD_PREAMBLE | LTC2494_CMD_EN | LTC2494_CMD_ODD | (channel & 0x07);
   cmd[1] = LTC2494_CMD_EN2 | LTC2494_CMD_RJ50HZ | gain_index;

   if(channel >= 8)
      adc_value = hv_ltc2494_adc2_transceive(cmd);
   else
      adc_value = hv_ltc2494_adc1_transceive(cmd);

   if ((adc_value & LTC2494_STAT_EOC_N) == 0) {
      voltage_value = (int) ((adc_value >> 4) & 0x0001FFFF);

      /* expand negative two's complement */
      if ((adc_value & LTC2494_STAT_SIG) == 0)
         voltage_value |= 0xFFFE0000;

      /* return successfully read value */
      *voltage = (((float)voltage_value * LTC2494_RES_V) + LTC2494_VCOM_V) / (float) gain[gain_index];
      return 1;
   }

   return 0;
}

/******************************************************************************/

int hv_current_read(unsigned char *channel, float *current)
{
   int status;

   status = hv_c_voltage_read(channel, current);
   if (status) {
      *current = *current / LTC2494_R_SHUNT;
      return 1;
   }

   return 0;
}

/******************************************************************************/

int hv_test_current_read(unsigned char channel, float *current, int gain_index)
{
   int status;

   status = hv_test_c_voltage_read(channel, current, gain_index);
   if (status) {
      *current = *current / LTC2494_R_SHUNT;
      return 1;
   }

  return 0;
}

/******************************************************************************/

float hv_current_get(unsigned char *channel)
{
  float current;

  while (!hv_current_read(channel, &current))
     usleep(10000);

  return current;
}

/******************************************************************************/

float hv_test_current_get(unsigned char channel, int gain_index)
{
  float current;

  while (!hv_test_current_read(channel, &current, gain_index))
     usleep(10000);

  return current;
}

/******************************************************************************/

void hv_power_on(unsigned char flag)
{
/*  unsigned char olatb; */

  /* read olatb */
/*  olatb =  hv_mcp23s17_transceive(MCP23S17_CMD_READ, MCP23S17_OLATB_WD2_REG_ADR, 0); */

  /* modify olatb (set or clear POWER_ON output) */
  if(flag)
  {
/*    olatb |= MCP23S17_GPB_PWR_ON_O; */
    hv_mcp23s17_set(MCP23S17_OLATB_WD2_REG_ADR, MCP23S17_GPB_PWR_ON_O);
  }
  else
  {
/*    olatb &= ~MCP23S17_GPB_PWR_ON_O; */
    hv_mcp23s17_clr(MCP23S17_OLATB_WD2_REG_ADR, MCP23S17_GPB_PWR_ON_O);
  }

  /* write olatb */
/*  hv_mcp23s17_transceive(MCP23S17_CMD_WRITE, MCP23S17_OLATB_WD2_REG_ADR, olatb); */
}

/******************************************************************************/

unsigned char hv_power_version()
{
  /* 0x00 = V1 */
  /* 0x01 = V2 */
  /* 0x02 = V3 */
  /* 0x03 = V4 */
  unsigned char gpiob;

  /* read gpiob */
  gpiob = hv_mcp23s17_read(MCP23S17_GPIOB_REG_ADR);

  gpiob = (~gpiob & (MCP23S17_GPB_BOARD_A1_I | MCP23S17_GPB_BOARD_A0_I) ) >> 4;

  return gpiob;
}

/******************************************************************************/

unsigned char hv_power_is_plugged()
{
  unsigned char gpiob;

  /* read gpiob */
  gpiob =  hv_mcp23s17_read(MCP23S17_GPIOB_REG_ADR);

  if( gpiob & MCP23S17_GPB_PLUGIN_I )
  {
    /* plugged in */
    return 1;
  }
  else
  {
    /* not plugged */
    return 0;
  }
}

/******************************************************************************/

unsigned char hv_is_plugged()
{
  unsigned int reg_val;
  
  reg_bank_read(WD2_REG_STATUS, &reg_val, 1);
  
  if(reg_val&WD2_HV_BOARD_PLUGGED_MASK) return 1;
  else return 0;
}

/******************************************************************************/

int hv_base_set(float voltage)
{
  voltage = voltage / LTC2601_V_SCL_FCTR;

  hv_ltc2601_set(LTC2601_CMD_WR_PU, voltage);

  return 1;
}

/******************************************************************************/

int hv_base_get(float *voltage)
{
  unsigned int adc_value;
  float v;

  adc_value = hv_ltc2484_transceive(LTC2484_CMD_EN | LTC2484_CMD_FOB);

  if( adc_value & LTC2484_STAT_EOC_N )
  {
    /* EOC not reached */
    return 0;
  }

  if( adc_value & LTC2484_STAT_SIG )
  {
    if( adc_value & LTC2484_STAT_MSB )
    {
      /* Overrange condition */
      set_error_flag(ERR_WD2_HV);
      return 0;
    }
    else
    {
      v = (adc_value & 0x0FFFFFFF) * LTC2484_RES_V;
    }
  }
  else
  {
    if( adc_value & LTC2484_STAT_MSB )
    {
      v = (adc_value | 0xF0000000) * LTC2484_RES_V;
    }
    else
    {
      /* Underrange condition */
      set_error_flag(ERR_WD2_HV);
      return 0;
    }
  }

  *voltage = v * LTC2484_V_SCL_FCTR;
  return 1;
}

/******************************************************************************/

void hv_led(unsigned char flag)
{
/*  unsigned char olatb; */

  /* read olatb */
/*  olatb =  hv_mcp23s17_transceive(MCP23S17_CMD_READ, MCP23S17_OLATB_WD2_REG_ADR, 0); */

  /* modify olatb (set or clear HV_ON output for LED) */
  if(flag)
  {
/*    olatb |= MCP23S17_GPB_HV_ON_O; */
    hv_mcp23s17_set(MCP23S17_OLATB_WD2_REG_ADR, MCP23S17_GPB_HV_ON_O);
  }
  else
  {
/*    olatb &= ~MCP23S17_GPB_HV_ON_O; */
    hv_mcp23s17_clr(MCP23S17_OLATB_WD2_REG_ADR, MCP23S17_GPB_HV_ON_O);
  }

  /* write olatb */
/*  hv_mcp23s17_transceive(MCP23S17_CMD_WRITE, MCP23S17_OLATB_WD2_REG_ADR, olatb); */
}


/******************************************************************************/

unsigned char hv_eeprom_read_status_register()
{
  return hv_eeprom_status_get();
}

/******************************************************************************/

void hv_eeprom_write_status_register(unsigned char send_data)
{
  hv_eeprom_transceive(HV_EEPROM_WRSR, send_data, NULL, NULL, 2);
}

/******************************************************************************/

void hv_eeprom_read(unsigned int addr, unsigned char *recv_data, int len)
{
  hv_eeprom_transceive(HV_EEPROM_READ, addr,  NULL, recv_data, len+2);
}

/******************************************************************************/

void hv_eeprom_write(unsigned int addr, unsigned char *send_data, int len)
{
  unsigned int block_addr = addr;
  unsigned char *buf_ptr;
  int wr_bytes;
  int bytes_remaining = len;
  /*unsigned char status; */

  buf_ptr = send_data;

  /* limit to one full memory write */
  if( len > HV_EEPROM_BYTES )
  {
    len = HV_EEPROM_BYTES;
  }

  while( bytes_remaining > 0 )
  {
    while( hv_eeprom_read_status_register()&HV_EEPROM_STAT_BIT_WIP )
    {
      usleep(10);
    }
    wr_bytes = HV_EEPROM_BYTES_PER_PAGE - (block_addr%HV_EEPROM_BYTES_PER_PAGE);
    if( wr_bytes > bytes_remaining )
    {
      wr_bytes = bytes_remaining;
    }
    /* set write enable latch */
    hv_eeprom_transceive(HV_EEPROM_WREN, 0,  NULL, NULL, 1);
    /* write data */
    hv_eeprom_transceive(HV_EEPROM_WRITE, (unsigned char)(block_addr), buf_ptr, NULL, wr_bytes+2);
    /* updated position */
    bytes_remaining -= wr_bytes;
    buf_ptr += wr_bytes;
    block_addr = block_addr + wr_bytes;
    wr_bytes = 0;
  }
}

/******************************************************************************/

void hv_digi_therm_set(unsigned char value)
{
  hv_ds28ea00_set_output(value);
}

/******************************************************************************/

void hv_digi_therm_setio(unsigned char value)
{
  /* 0 = output, 1 = input (tristate) */
  hv_ds28ea00_set_tristate(value);
}


/******************************************************************************/

unsigned char hv_digi_therm_get()
{
  return hv_ds28ea00_get_input();
}

/******************************************************************************/
/******************************************************************************/
