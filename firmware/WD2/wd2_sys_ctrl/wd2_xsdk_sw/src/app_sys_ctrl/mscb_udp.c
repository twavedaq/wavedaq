//-------------------------------------------------------------------------------------
//  Paul Scherrer Institute
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDREAM2
//
//  Author  :  rs32
//  Created :  11.09.2014
//
//  Description :  UDP communication for MSCB
//  test with e.g.  nc -u -p 1177 wd2.psi.ch 1177
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>

#include "mscb.h"
#include "dbg.h"
#include "xtime.h"

#define SUBM_VERSION 5 // UDP protocol version number

typedef struct {
  unsigned short size;
  unsigned short seq_num;
  unsigned char  flags;
  unsigned char  version;
} MUDP_HEADER;

/*------------------------------------------------------------------*/

unsigned int execute(unsigned char *buf, unsigned char *rb)
{
  if (buf[0] == MCMD_ECHO) {
    rb[0] = MCMD_ACK;
    rb[1] = SUBM_VERSION;
    rb[2] = 0;
    rb[3] = 0;

    unsigned int uptime = time_get_sec();
    rb[7] = uptime & 0xFF;
    uptime >>= 8;
    rb[6] = uptime & 0xFF;
    uptime >>= 8;
    rb[5] = uptime & 0xFF;
    uptime >>= 8;
    rb[4] = uptime & 0xFF;
    uptime >>= 8;

    return 8;
  }

  if (buf[0] == MCMD_TOKEN) {
    // later add password checking here like
    // if (password[0] == 0 || strcmp(buf, password) == 0)
    //    buf[0] = MCMD_ACK;
    // else
    //    buf[0] = 0xFF;

    rb[0] = MCMD_ACK;
    return 1;
  }

  return 0;
}

/*------------------------------------------------------------------*/

unsigned int mscb_handler(network_if_type *nw_if, unsigned char *frame, int frame_len)
{
  int data_len, send_len, src_port;

  udp_header_type *rec_ip_fp = (udp_header_type *)frame;
  MUDP_HEADER *pmudp;
  char *data_ptr;
  unsigned char *buf;
  unsigned int mscb_len;
  int i, cmd, bsize;
  unsigned char retbuf[1024+10];


  data_len = (rec_ip_fp->udp_message_len[0]<<8) + (rec_ip_fp->udp_message_len[1]) - 8;
  data_ptr = (char *)(&frame[UDP_HEADER_LEN]);
  src_port = (rec_ip_fp->src_port[0]<<8) + (rec_ip_fp->src_port[1]);

  pmudp = (MUDP_HEADER *)data_ptr;
  if (DBG_INFO) {
    xil_printf("dbg: %d\r\n", global_dbg_level);
    xil_printf("\r\nMSCB: received frame_len = %d data_len = %d src_port = %d\r\n", frame_len, data_len, src_port);
    xil_printf("size:     %d\r\n", pmudp->size);
    xil_printf("seq:      %d\r\n", pmudp->seq_num);
    xil_printf("flags:    %d\r\n", pmudp->flags);
    xil_printf("version:  %d\r\n", pmudp->version);
  }

  // don't continue if incorrect size
  if (data_len != pmudp->size + (int)sizeof(MUDP_HEADER))
    return 1;

  // don't continue if incorrect version
  if (pmudp->version != SUBM_VERSION && *((unsigned char *)(pmudp+1)) != MCMD_ECHO)
    return 1;

  buf =  (unsigned char *)(pmudp+1);
  cmd =  (pmudp->flags & RS485_FLAG_CMD) > 0;
  bsize = pmudp->size;

  for (i=0 ; i<bsize ; i+=mscb_len) {

    mscb_len = buf[i] & 0x07;
    if (mscb_len == 0x07) {
      if (buf[i+1] & 0x80)
        mscb_len = (((buf[i+1] & 0x7F) << 8) | buf[i+2]) + 4;
      else
        mscb_len = (buf[i+1] & 0x7F) + 3; // add command, length and CRC
    } else
      mscb_len += 2; // add command and CRC

    if (cmd)
      data_len = execute(buf, retbuf);
    else
      data_len = mscb_interpret(1, buf+i, retbuf);

    if (data_len) {
      pmudp->size    = data_len;
      pmudp->seq_num = pmudp->seq_num;
      pmudp->flags   = 0;
      pmudp->version = SUBM_VERSION;

      data_len += sizeof(MUDP_HEADER);
      memcpy(pmudp+1, retbuf, data_len);

      // prepare answer frame
      ncpy(rec_ip_fp->dst_mac, rec_ip_fp->src_mac, 6);
      ncpy(rec_ip_fp->src_mac, nw_if->mac_addr,     6);

      ncpy(rec_ip_fp->dst_ip,  rec_ip_fp->src_ip, 4);
      ncpy(rec_ip_fp->src_ip,  nw_if->ip_addr,     4);

      rec_ip_fp->src_port[0] = (MSCB_SEND_SRC_PORT >> 8) & 0xff ;
      rec_ip_fp->src_port[1] = MSCB_SEND_SRC_PORT & 0xff;

      rec_ip_fp->dst_port[0] = (src_port >> 8) & 0xff ;
      rec_ip_fp->dst_port[1] = src_port & 0xff;

      send_len = UDP_HEADER_LEN + data_len;

      adjust_header(rec_ip_fp, send_len);

      nw_if->ll_fifo->send(nw_if->ll_fifo, (unsigned char*)frame, send_len);

      if (cmd)
        break;
    }
  }

  return 1;
}

/*****************************************************************************/
