/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  23.09.2014 15:06:42
 *
 *  Description :  Module for ADC control.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef WD2_REV_F

#include "drv_drs4_control.h"
#include "xfs_printf.h"
#include "update_config.h"
#include "dbg.h"
#include "register_map_wd2.h"
#include "adc_serdes_calibration.h"
#include "system.h"
#include "drv_soft_spi_adc_ltm9009.h"
#include "plb_serdes_cal_if.h"
#include "drv_plb_wd2_reg_bank.h"
#include <stdlib.h>
#include "cmd_processor.h"

/************************************************************/

  int adc_check_calibration(int argc, char **argv)
  {
    /* unsigned int data; */
    adc_eye_cal_type adc_cal;

    CMD_HELP("",
             "check if common local data clock phase is valid",
             "check the calibration of the pll phase delay of the common internal data clock"
             );

    /* Start the calibration */
    if(DBG_INFO) xfs_printf("Checking calibration...\r\n");
    ads_timing_cal(&adc_cal, ADC_CAL_MODE_CHECK, 1);

    return 0;
  }

/************************************************************/

  int adc_recalibrate(int argc, char **argv)
  {
    adc_eye_cal_type adc_cal;

//      xfs_printf("adcrecal:   recalibrate common local data clock phase\r\n");


    CMD_HELP("",
             "recalibrate common local data clock phase",
             "calibrate the pll phase delay of the common internal data clock"
            );

    /* Start the calibration */
    if(DBG_INFO) xfs_printf("Starting calibration...\r\n");
    ads_timing_cal(&adc_cal, ADC_CAL_MODE_CALIBRATE, 1);

    return 0;
  }

/************************************************************/

  int adc_set_tm(int argc, char **argv)
  {
    unsigned int pattern;
    unsigned int mask;
    unsigned int data;

    CMD_HELP("[pattern]",
             "set adc test mode for all adcs/channels",
             "  [pattern] : 12bit hex adc test pattern - optional (see datasheet)\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 1)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    if(argc > 1)
    {
      pattern = (unsigned int)(hatoi((const char*)argv[1]));
      pattern = ( (pattern<<WD2_ADC_0_1458_TP_OFS) & WD2_ADC_0_1458_TP_MASK) | WD2_ADC_0_1458_OUTTEST_MASK; /* 0x8000 to enable testmode */
      mask    = WD2_ADC_0_2367_TP_MASK | WD2_ADC_0_2367_OUTTEST_MASK;
    }
    else
    {
      pattern = WD2_ADC_0_1458_OUTTEST_MASK; /* 0x8000 to enable testmode */
    }


    reg_bank_read(WD2_REG_ADC_0_CFG_1458, &data, 1);
    data = ( data & ~mask );
    data = data | pattern;
    reg_bank_write(WD2_REG_ADC_0_CFG_1458, &data, 1);

    reg_bank_read(WD2_REG_ADC_0_CFG_2367, &data, 1);
    data = ( data & ~mask );
    data = data | pattern;
    reg_bank_write(WD2_REG_ADC_0_CFG_2367, &data, 1);

    reg_bank_read(WD2_REG_ADC_1_CFG_1458, &data, 1);
    data = ( data & ~mask );
    data = data | pattern;
    reg_bank_write(WD2_REG_ADC_1_CFG_1458, &data, 1);

    reg_bank_read(WD2_REG_ADC_1_CFG_2367, &data, 1);
    data = ( data & ~mask );
    data = data | pattern;
    reg_bank_write(WD2_REG_ADC_1_CFG_2367, &data, 1);

    return 0;
  }

/************************************************************/

  int adc_rst_tm(int argc, char **argv)
  {
    unsigned int  data;

    CMD_HELP("",
             "reset adc test patterns on all adcs/channels",
            );

    reg_bank_read(WD2_REG_ADC_0_CFG_1458, &data, 1);
    data = ( data & ~WD2_ADC_0_1458_OUTTEST_MASK );
    reg_bank_write(WD2_REG_ADC_0_CFG_1458, &data, 1);

    reg_bank_read(WD2_REG_ADC_0_CFG_2367, &data, 1);
    data = ( data & ~WD2_ADC_0_2367_OUTTEST_MASK );
    reg_bank_write(WD2_REG_ADC_0_CFG_2367, &data, 1);

    reg_bank_read(WD2_REG_ADC_1_CFG_1458, &data, 1);
    data = ( data & ~WD2_ADC_1_1458_OUTTEST_MASK );
    reg_bank_write(WD2_REG_ADC_1_CFG_1458, &data, 1);

    reg_bank_read(WD2_REG_ADC_1_CFG_2367, &data, 1);
    data = ( data & ~WD2_ADC_1_2367_OUTTEST_MASK );
    reg_bank_write(WD2_REG_ADC_1_CFG_2367, &data, 1);

    return 0;
  }

/************************************************************/

  int adc_get(int argc, char **argv)
  {
    unsigned int no_words;
    unsigned int timeout;
    int adc_data;

    CMD_HELP("<nr_of_words> <adc_pattern>",
             "show a specified number of sampled data words",
             "reads the specified number of words from the ADC.\r\n"
             "Use adcsettm to configure for test outputs.\r\n"
             "    <nr_of_words> : number of data words to read (1..15)\r\n"
             "    <adc_pattern> : adc test pattern code (optional, see datasheet)\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    no_words = (unsigned int)(xfs_atoi(argv[1]));

    /* Start the calibration */
    if(DBG_INFO) xfs_printf("Reading input data from ADC...\r\n");
    plb_serdes_cal_trigger_read(SYSPTR(serdes_cal_ctrl[0]), no_words);
    plb_serdes_cal_trigger_read(SYSPTR(serdes_cal_ctrl[1]), no_words);
    timeout = 1000000;
    while( !plb_serdes_cal_data_is_available(SYSPTR(serdes_cal_ctrl[1])) && timeout)
    {
      timeout--;
    }
    if(!timeout)
    {
      return 0;
    }
    for(unsigned int i=0;i<no_words;i++)
    {
      if (DBG_INFO) xfs_printf("\r\n");
      for(int adc=0; adc<2; adc++)
      {
        if (DBG_INFO) xfs_printf("ADC %d access number %d\r\n", adc, i);
        for(int adc_ch=0; adc_ch<8; adc_ch++)
        {
          adc_data = plb_serdes_cal_get(SYSPTR(serdes_cal_ctrl[adc]), adc_ch);
          if (DBG_INFO) xfs_printf("adc %d   channel %d   0x%04X\r\n", adc, adc_ch, adc_data);
        }
        adc_data = plb_serdes_cal_get(SYSPTR(serdes_cal_ctrl[adc]), 8);
        if (DBG_INFO) xfs_printf("adc %d   frame(0x%01X)  0x%04X\r\n", adc, (adc_data>>12)&0x0F, adc_data&0x0FFF);
        if( !plb_serdes_cal_update(SYSPTR(serdes_cal_ctrl[adc])) )
        {
          return 0;
        }
      }
    }

    return 0;
  }


/************************************************************/

  int adc_get_via_eth(int argc, char **argv)
  {
    CMD_HELP("",
             "transmit a data frame via ethernet",
             "reads one frame from the ADC and\r\n"
             "ransmits the corresponding data via ethernet.\r\n"
            );

    if(DBG_INFO) xfs_printf("\r\nSending one ADC data frame via ethernet.\r\n");
    drs4_start_daq();
    /* delay to the domino wave stabilize and    */
    /* make sure the drs fsm is ready to trigger */
    usleep(500);
    /* t >= 200us !!! */
    drs4_soft_trigger();

    return 0;
  }

/************************************************************/

/*  int adc_stop_eth_tx(int argc, char **argv)
 *  {
 *     CMD_HELP("",
 *              "stop drs data transmission via ethernet",
 *              "stops a running transmission of adc data via ethernet."
 *             );
 *
 *    SYSPPTR(udp_pack_gen_gmac)->stop();
 *    xfs_printf("\r\nEthernet transmission stopped\r\n");
 *
 *    return 0;
 *  }
 */

/************************************************************/

  int adc_reset(int argc, char **argv)
  {
    CMD_HELP("",
             "initiates a reset of both ADCs"
            );

    ltm9009_reset();
    xfs_printf("\r\nResetting ADCs\r\n");
    return 0;
  }

/************************************************************/

  int module_adc_help(int argc, char **argv)
  {
    CMD_HELP("","ADC interface functions");

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type adc_ltm9009_cmd_table[] =
  {
    {0, "adc", module_adc_help},
    {0, "adccalchk", adc_check_calibration},
    {0, "adcrecal", adc_recalibrate},
    {0, "adcsettm", adc_set_tm},
    {0, "adcrsttm", adc_rst_tm},
    {0, "adcgettxt", adc_get},
    {0, "adcget", adc_get_via_eth},
    /* {"adcstopeth", adc_stop_eth_tx}, */
    {0, "adcrst", adc_reset},
    {0, NULL, NULL}
  };

/************************************************************/

#endif /* not WD2_REV_F */

/************************************************************/
