/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  16.04.2015 11:36:43
 *
 *  Description :  Test frame generator for  ADC-SERDES-WD2_PACKAGER-ETHERNET chain.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "plb_serdes_via_eth_testgen.h"
#include "utilities.h"
#include "xfs_printf.h"
#include "dbg.h"

/******************************************************************************/

void plb_serdes_via_eth_testgen_init(serdes_eth_testgen_type *self, unsigned int base_addr)
{
  self->base_addr = base_addr;
  serdes_eth_testgen_reset(self);
};

/******************************************************************************/

void serdes_eth_testgen_reset(serdes_eth_testgen_type *self)
{
  xfs_out32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_CTRL, SRDS_ETH_TESTGEN_CTRL_RESET );
}

/******************************************************************************/

void serdes_eth_testgen_stop(serdes_eth_testgen_type *self)
{
  serdes_eth_testgen_reset(self);
}

/******************************************************************************/

int serdes_eth_testgen_start(serdes_eth_testgen_type *self, unsigned int header, unsigned int total_frames)
{
  if (total_frames < 1) return -1;

  /* configure */
  xfs_out32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_HEADER, header );
  xfs_out32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_NR_OF_SAMPLES, 0x07FF );
  xfs_out32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_NR_OF_FRAMES, total_frames );

  /* start */
  xfs_out32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_CTRL, SRDS_ETH_TESTGEN_CTRL_START );
  if(DBG_INFO) xfs_printf("\r\nStart [0x%08X] = 0x%08X\r\n",  self->base_addr + 4*SRDS_ETH_TESTGEN_REG_CTRL,          xfs_in32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_CTRL) );
  if(DBG_INFO) xfs_printf("Start [0x%08X] = 0x%08X\r\n",      self->base_addr + 4*SRDS_ETH_TESTGEN_REG_NR_OF_SAMPLES, xfs_in32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_NR_OF_SAMPLES) );
  if(DBG_INFO) xfs_printf("Start [0x%08X] = 0x%08X\r\n",      self->base_addr + 4*SRDS_ETH_TESTGEN_REG_NR_OF_FRAMES,  xfs_in32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_NR_OF_FRAMES) );
  if(DBG_INFO) xfs_printf("Start [0x%08X] = 0x%08X\r\n",      self->base_addr + 4*SRDS_ETH_TESTGEN_REG_CURRENT_FRAME, xfs_in32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_CURRENT_FRAME) );
  if(DBG_INFO) xfs_printf("Start [0x%08X] = 0x%08X\r\n",      self->base_addr + 4*SRDS_ETH_TESTGEN_REG_HEADER,        xfs_in32(self->base_addr + 4*SRDS_ETH_TESTGEN_REG_HEADER) );
  return 0;
}

/******************************************************************************/
/******************************************************************************/


