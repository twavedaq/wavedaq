/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  29.10.2014 11:18:15
 *
 *  Description :  High level functions for communication with
 *                 HV board devices.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __HV_BOARD_COM_H__
#define __HV_BOARD_COM_H__

/************************************************************/

void hv_init();
void hv_channel_switch(unsigned char channel, unsigned char flag);
int hv_dac_set(unsigned char channel, float voltage);
int hv_c_voltage_read(unsigned char *channel, float *voltage);
int hv_test_c_voltage_read(unsigned char channel, float *voltage, int gain_index);
int hv_current_read(unsigned char *channel, float *current);
int hv_test_current_read(unsigned char channel, float *current, int gain_index);
float hv_current_get(unsigned char channel);
float hv_test_current_get(unsigned char channel, int gain_index);
void hv_power_on(unsigned char flag);
unsigned char hv_power_version();
unsigned char hv_is_plugged();
unsigned char hv_power_is_plugged();
int hv_base_set(float voltage);
int hv_base_get(float *voltage);
void hv_led(unsigned char flag);
unsigned char hv_eeprom_read_status_register();
void hv_eeprom_write_status_register(unsigned char send_data);
void hv_eeprom_read(unsigned int addr, unsigned char *recv_data, int len);
void hv_eeprom_write(unsigned int addr, unsigned char *send_data, int len);
void hv_digi_therm_set(unsigned char value);
void hv_digi_therm_setio(unsigned char value);
unsigned char hv_digi_therm_get();

#endif /* __HV_BOARD_COM_H__ */
