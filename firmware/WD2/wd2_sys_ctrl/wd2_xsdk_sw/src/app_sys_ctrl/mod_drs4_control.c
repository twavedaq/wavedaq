/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  12.05.2014 12:52:42
 *
 *  Description :  Module for configuring the DRS4 chips.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "drv_drs4_control.h"
#include "dbg.h"
#include "cmd_processor.h"
#include "register_map_wd2.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "drv_plb_wd2_reg_bank.h"
#include <stdlib.h>


/************************************************************/

  int mod_drs4_init_rsr(int argc, char **argv)
  {
    CMD_HELP("",
             "initialize the read shift register of both DRS4 chips",
             "Initializes the read shift register (RSR) with the default value."  
            );


    if(DBG_INFO) xfs_printf("\r\nInitializing read shift register (A=0xB)\r\n\r\n");
    drs4_init_rsr();

    return 0;
  }

/************************************************************/

  int mod_drs4_set_register(int argc, char **argv)
  {
    unsigned int value;

    CMD_HELP("<register> <value>",
             "write the registers of both DRS4 chips",
             "Writes the corresponding register with the specified value.\r\n"
             "Note: for the rdsr the value represents the position of the\r\n"
             "      single '1' written to that register.\r\n\r\n"
             "  <register> : register name.\r\n"
             "               wsrloop = Configuration Register WSRLOOP bit\r\n"
             "               pllen =   Configuration Register PLLEN bit\r\n"
             "               dmode =   Configuration Register DMODE bit\r\n"
             "               wsr = Write Shift Register\r\n"
             "               wcr = Write Config Register\r\n"
             "  <value>    : hex register value to write.\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    value = hatoi((const char*)argv[2]);

    if(fstrcmp(argv[1],"wsrloop"))
    {
      if(value)
      {
        if(DBG_INFO) xfs_printf("\r\nSetting Config Register (A=0xC) WSRLOOP\r\n\r\n");
        drs4_set_cfr(DRS4_CFG_WSRLOOP);
      }
      else
      {
        if(DBG_INFO) xfs_printf("\r\nClearing Config Register (A=0xC) WSRLOOP\r\n\r\n");
        drs4_clr_cfr(DRS4_CFG_WSRLOOP);
      }
    }
    else if(fstrcmp(argv[1],"pllen"))
    {
      if(value)
      {
        if(DBG_INFO) xfs_printf("\r\nSetting Config Register (A=0xC) PLLEN\r\n\r\n");
        drs4_set_cfr(DRS4_CFG_PLLEN);
      }
      else
      {
        if(DBG_INFO) xfs_printf("\r\nClearing Config Register (A=0xC) PLLEN\r\n\r\n");
        drs4_clr_cfr(DRS4_CFG_PLLEN);
      }
    }
    else if(fstrcmp(argv[1],"dmode"))
    {
      if(value)
      {
        if(DBG_INFO) xfs_printf("\r\nSetting Config Register (A=0xC) DMODE\r\n\r\n");
        drs4_set_cfr(DRS4_CFG_DMODE);
      }
      else
      {
        if(DBG_INFO) xfs_printf("\r\nClearing Config Register (A=0xC) DMODE\r\n\r\n");
        drs4_clr_cfr(DRS4_CFG_DMODE);
      }
    }
    else if(fstrcmp(argv[1],"wsr"))
    {
      if(DBG_INFO) xfs_printf("\r\nSetting Write Shift Register (A=0xD) to 0x%02X\r\n\r\n",value);
      drs4_set_wsr(value);
    }
    else if(fstrcmp(argv[1],"wcr"))
    {
      if(DBG_INFO) xfs_printf("\r\nSetting Write Config Register not implemented yet!!!\r\n\r\n",value);
      drs4_set_wcr(value);
    }
    else
    {
      xfs_printf("E%02X: Invalid arguments. Type \"drsset help\" for help.\r\n", ERR_INVALID_ARGS);
      return 0;
    }

    return 0;
  }

/************************************************************/

  int mod_drs4_set_valid_delay(int argc, char **argv)
  {
    unsigned char delay = 0;

    CMD_HELP("<value>",
             "set valid delay of DRS4 control FSM",
             "Sets the delay of the data valid signal from\r\n"  
             "the FSM to trigger and packager units.\r\n"
             "  <value> : decimal value of delay.\r\n"
            );


    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    delay = (unsigned char)(xfs_atoi(argv[1]) & 0x00FF);

    if(DBG_INFO) xfs_printf("\r\nSetting valid delay of ADCs to %d\r\n\r\n",delay);
    drs4_set_valid_delay(delay);

    return 0;
  }

/************************************************************/

  int mod_drs4_get_pll_lock(int argc, char **argv)
  {
    CMD_HELP("",
             "read the pll lock state of both DRS4 chips",
             "reads the LOCK outputs of the DRS4 chips"  
            );

    xfs_printf("DRS4 Channel 0 (A) PLL ");
    if(!drs4_get_pll_lock(DRS4_DEVICE_A))
    {
      xfs_printf(" NOT  ");
    }
    xfs_printf("locked\r\n");

    xfs_printf("DRS4 Channel 1 (B) PLL ");
    if(!drs4_get_pll_lock(DRS4_DEVICE_B))
    {
      xfs_printf(" NOT  ");
    }
    xfs_printf("locked\r\n");

    xfs_printf("\r\n");

    return 0;
  }

/************************************************************/

  int mod_drs4_start_daq(int argc, char **argv)
  {
    CMD_HELP("",
             "start DAQ of DRS4 chips (set DWRITE and DENABLE 1)"
            );

    drs4_start_daq();

    return 0;
  }

/************************************************************/

  int mod_drs4_soft_trigger(int argc, char **argv)
  {
    CMD_HELP("",
             "issue soft trigger to read DRS4 chips",
             ""  
            );

    drs4_soft_trigger();

    return 0;
  }

/************************************************************/

  int mod_drs4_get(int argc, char **argv)
  {
    CMD_HELP("",
             "start DAQ and issue soft trigger imediately",
             "start DAQ of DRS4 chips and issue soft trigger\r\n"
             "imediately to read DRS4 chips\r\n"
            );


    drs4_start_daq();
    /* delay to the domino wave stabilize and    */
    /* make sure the drs fsm is ready to trigger */
    usleep(500);
    /* t >= 200us !!! */
    drs4_soft_trigger();

    return 0;
  }

/************************************************************/

  int mod_drs4_fsm_re_init(int argc, char **argv)
  {
    CMD_HELP("",
             "re-initialization of the drs control FSM",
             "resets the drs control FSM."  
            );

    drs4_fsm_reset();

    return 0;
  }

/************************************************************/

  int module_drs4_help(int argc, char **argv)
  {
    CMD_HELP("",
             "DRS4 Control Module",
             "Can be used to program the shift registers configuring the DRS4 chips"  
             "as well as setting the mode of them"
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type drs4_control_cmd_table[] =
  {
    {0, "drs", module_drs4_help},
    {0, "drsinitrsr", mod_drs4_init_rsr},
    {0, "drsset", mod_drs4_set_register},
    {0, "drssetdly", mod_drs4_set_valid_delay},
    {0, "drsstart", mod_drs4_start_daq},
    {0, "drstrig", mod_drs4_soft_trigger},
    {0, "drsget", mod_drs4_get},
    {0, "drsgetlock", mod_drs4_get_pll_lock},
    {0, "drsreinit", mod_drs4_fsm_re_init},
    {0, NULL, NULL}
  };

/************************************************************/
