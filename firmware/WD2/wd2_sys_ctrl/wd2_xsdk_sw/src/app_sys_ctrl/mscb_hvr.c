//-------------------------------------------------------------------------------------
//  Paul Scherrer Institute
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDREAM2
//
//  Author  :  rs32
//  Created :  11.09.2014
//
//  Description :  High Voltage Regulator via MSCB
//
//-------------------------------------------------------------------------------------

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>

#include "system.h"
#include "mscb.h"
#include "dbg.h"
#include "xtime.h"
#include "hv_board_com.h"
#include "digi_therm.h"
#include "drv_hard_spi_hv.h"
#include "drv_plb_wd2_reg_bank.h"
#include "register_map_wd2.h"

/*------------------------------------------------------------------*/

/* number of HV channels */
#define N_HV_CHN 16

char node_name[] = "HVR-800";

/* maximum current in micro Ampere */
#define MAX_CURRENT 100

/* individual channel DAC range 5 Volts */
#define DAC_RANGE 5

/* high voltage divider from drv_hard_spi_hv.h */
#define DIVIDER HV_VOLTAGE_DIVIDER

/* default current resistor 10kOhm */
#define DEFAULT_RCURR 10E3

/* protection resistor 1.2kOhm */
#define RPROT 1200

/* high voltage switch resistance 14.5Ohm */
#define RSWITCH 14.5

/* current multiplier */
#define CUR_MULT (5+80E3/10E3)    // (g=5+80k/10k) at AD8227

/*---- Define variable parameters returned to CMD_GET_INFO command ----*/

char hv_vers[12];

typedef struct {
   float u_demand[N_HV_CHN];
   float i_meas[N_HV_CHN];

   float ub_demand;
   float ub_meas;
   float ub_act;

   float temp[4];

   float u_dac[N_HV_CHN];

   float r_current;

   float adc_gain;
   float adc_offset;
} USER_DATA;

USER_DATA user_data;

MSCB_INFO_VAR vars[] = {

   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U0",      &user_data.u_demand[0]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U1",      &user_data.u_demand[1]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U2",      &user_data.u_demand[2]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U3",      &user_data.u_demand[3]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U4",      &user_data.u_demand[4]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U5",      &user_data.u_demand[5]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U6",      &user_data.u_demand[6]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U7",      &user_data.u_demand[7]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U8",      &user_data.u_demand[8]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U9",      &user_data.u_demand[9]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U10",     &user_data.u_demand[10] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U11",     &user_data.u_demand[11] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U12",     &user_data.u_demand[12] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U13",     &user_data.u_demand[13] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U14",     &user_data.u_demand[14] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "U15",     &user_data.u_demand[15] },

   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I0",      &user_data.i_meas[0]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I1",      &user_data.i_meas[1]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I2",      &user_data.i_meas[2]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I3",      &user_data.i_meas[3]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I4",      &user_data.i_meas[4]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I5",      &user_data.i_meas[5]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I6",      &user_data.i_meas[6]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I7",      &user_data.i_meas[7]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I8",      &user_data.i_meas[8]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I9",      &user_data.i_meas[9]    },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I10",     &user_data.i_meas[10]   },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I11",     &user_data.i_meas[11]   },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I12",     &user_data.i_meas[12]   },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I13",     &user_data.i_meas[13]   },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I14",     &user_data.i_meas[14]   },
   { 4, UNIT_AMPERE, PRFX_MICRO, 0, MSCBF_FLOAT, "I15",     &user_data.i_meas[15]   },

   /* base voltage */
   {10, UNIT_STRING,          0, 0,           0, "HV Vers", &hv_vers                },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "UB",      &user_data.ub_demand    },               // 32
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT, "UBmeas",  &user_data.ub_meas      },               // 33
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UBact",   &user_data.ub_act     },  // 34

   { 4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp0",    &user_data.temp[0]       }, // 35
   { 4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp1",    &user_data.temp[1]       }, // 35
   { 4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp2",    &user_data.temp[2]       }, // 35
   { 4, UNIT_CELSIUS,         0, 0, MSCBF_FLOAT, "Temp3",    &user_data.temp[3]       }, // 35

   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC0",   &user_data.u_dac[0]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC1",   &user_data.u_dac[1]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC2",   &user_data.u_dac[2]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC3",   &user_data.u_dac[3]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC4",   &user_data.u_dac[4]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC5",   &user_data.u_dac[5]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC6",   &user_data.u_dac[6]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC7",   &user_data.u_dac[7]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC8",   &user_data.u_dac[8]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC9",   &user_data.u_dac[9]  },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC10",  &user_data.u_dac[10] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC11",  &user_data.u_dac[11] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC12",  &user_data.u_dac[12] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC13",  &user_data.u_dac[13] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC14",  &user_data.u_dac[14] },
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "UDAC15",  &user_data.u_dac[15] },

   /* current sens resistor */
   { 4, UNIT_OHM,             0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "RCurr",   &user_data.r_current  },  // 50

   /* calibration constants */
   { 4, UNIT_FACTOR,          0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "ADCgain", &user_data.adc_gain   },  // 51
   { 4, UNIT_VOLT,            0, 0, MSCBF_FLOAT | MSCBF_HIDDEN, "ADCofs",  &user_data.adc_offset },  // 52

   { 0 }
};

MSCB_INFO_VAR *variables = vars;

#define REG_ADR_BASE           XPAR_PLB_WD2_REG_BANK_0_MEM0_BASEADDR
#define REG_ADR_HV_VER         (REG_ADR_BASE+REG_HV_VER)
#define REG_ADR_HV_I_MEAS_0    (REG_ADR_BASE+WD2_REG_HV_I_MEAS_0)
#define REG_ADR_HV_U_BASE_MEAS (REG_ADR_BASE+WD2_REG_HV_U_BASE_MEAS)
#define REG_ADR_HV_TEMP_0      (REG_ADR_BASE+WD2_REG_HV_TEMP_0)
#define REG_ADR_HV_U_TARGET_0  (REG_ADR_BASE+WD2_REG_HV_U_TARGET_0)
#define REG_ADR_HV_R_SHUNT     (REG_ADR_BASE+WD2_REG_HV_R_SHUNT)

/********************************************************************\

  Application specific init and inout/output routines

\********************************************************************/

void set_hv(unsigned char index);
int read_base_hv();

static unsigned int last_set = 0; // time marker set when new hv is set

/*------------------------------------------------------------------*/

void mscb_vars(unsigned char *n_sub_addr, unsigned char *var_size)
{
  // declare number of sub-addresses and data size to framework
  *n_sub_addr = 1;
  *var_size = (unsigned char)sizeof(USER_DATA);
}

/*---- User init function ------------------------------------------*/

extern SYS_INFO sys_info;

#include "xfs_printf.h"
void mscb_hv_init(unsigned char init)
{
   int i;
   //xfs_local_printf("user_init\r\n");

   /* initial nonzero EEPROM values */
   if (init) {
      memset(&user_data, 0, sizeof(user_data));
      user_data.adc_gain = 1;
      user_data.adc_offset = 0;
      user_data.r_current = DEFAULT_RCURR;
   }
   if (user_data.r_current < 1000 || user_data.r_current > 100000)
      user_data.r_current = DEFAULT_RCURR;

   /* set default group address */
   if (sys_info.group_addr == 0xFFFF)
      sys_info.group_addr = 1600;

   /* set-up DAC & ADC */
   hv_init();

   /* set HV from user_data (might be nonzero) */
   mscb_write_param(32, 0);
   for (i=0 ; i<16 ; i++)
      mscb_write_param(i, 0);

   if (hv_power_is_plugged()) {
      hv_power_on(1);
      unsigned int d;
      switch(hv_power_version()) {
         case 0: strcpy(hv_vers, "80V/2mA");
                 d = (80 << 16) | 2;
                 break;
         case 1: strcpy(hv_vers, "210V/1mA");
                 d = (210 << 16) | 1;
                 break;
         case 2: strcpy(hv_vers, "80V/50mA");
                 d = (80 << 16) | 50;
                 break;
         case 3: strcpy(hv_vers, "210V/20mA");
                 d = (210 << 16) | 20;
                 break;
         default: strcpy(hv_vers, "????");
                 d = 0xFFFFFFFF;
                 break;
      }
      reg_bank_write(WD2_REG_HV_VER, &d, 1);
   } else
      strcpy(hv_vers, "None");

   /* scan 1-wire bus */
   digi_therm_read(NULL);
}

/*---- User write function -----------------------------------------*/

void mscb_write_param(unsigned char index, unsigned char flag)
{
   if (index < 16 && flag) {
      // get target value from register
	  float f;
      reg_bank_read(WD2_REG_HV_U_TARGET_0+index*4, (unsigned int *)&f, 1);
      user_data.u_demand[index] = f;
   }
   if (index < 16 && !flag) {
      // write target value to register
	  float f;
	  f = user_data.u_demand[index];
	  reg_bank_write(WD2_REG_HV_U_TARGET_0+index*4, (unsigned int *)&f, 1);
   }
   if (index >= 0 && index < 16)
     set_hv(index);
   if (index == 32) {
      if (hv_power_is_plugged()) {
         if (user_data.ub_demand > 0) {
            hv_base_set(user_data.ub_demand-1); // approach from below
            user_data.ub_act = user_data.ub_demand-1;
            last_set = time();
         } else {
            hv_base_set(0);
            user_data.ub_act = 0;
            last_set = time();
         }
      }
   }
}

/*---- User read function ------------------------------------------*/

unsigned char mscb_read_param(unsigned char index)
{
   if (index);
   return 0;
}

/*---- User function called vid CMD_USER command -------------------*/

unsigned char user_func(unsigned char *data_in, unsigned char *data_out)
{
   if (data_in || data_out);
   return 0;
}

/*------------------------------------------------------------------*/

void set_hv(unsigned char index)
{
   int i;
   float min, ub_old;

   ub_old = user_data.ub_demand;

   if (user_data.u_demand[index] == 0) {
      /* turn channel off */
      hv_channel_switch(index, 0);

      /* turn off base voltage if all channels off */
      for (i=0 ; i<16 ; i++)
         if (user_data.u_demand[i] > 0)
            break;
      if (i == 16) {
         user_data.ub_demand = 0;
         user_data.ub_act = 0;
         hv_base_set(user_data.ub_demand);
      }

   } else {
      /* turn channel on */
      hv_channel_switch(index, 1);

      /* set base voltage to channel with lowest voltage */
      min = 1000;
      for (i=0 ; i<16 ; i++)
         if (user_data.u_demand[i] > 0 && user_data.u_demand[i] < min)
            min = user_data.u_demand[i];

      if (min > 1)
         user_data.ub_demand = min - 1; // get out of the rail, leave room for 1V voltage drop (100 uA @ 10k)
      else
    	 user_data.ub_demand = 0;

      last_set = time();

      /* initial setting of base HV */
      if (ub_old != user_data.ub_demand) {
         hv_base_set(user_data.ub_demand);
         user_data.ub_act = user_data.ub_demand;
      }

      /* Use following lines to set voltage directly without drop correction
      for (i=0 ; i<16 ; i++)
         if (user_data.u_demand[i] > 0) {
            diff = user_data.u_demand[i] - user_data.ub_demand;
            hv_dac_set(i, diff);
            }
       */
   }
}

/*------------------------------------------------------------------*/

int read_base_hv()
{
   float hv;

   /* read voltage channel */
   if (!hv_base_get(&hv))
      return 0;

   /* apply calibration */
   hv = hv * user_data.adc_gain + user_data.adc_offset;

   /* 0.001 resolution */
   hv = floor(hv * 1000 + 0.5) / 1000.0;

   /* TBD: Turn on LED if HV on */
   if (hv > 5)
      hv_led(1);
   else
      hv_led(0);

   user_data.ub_meas = hv;
   reg_bank_write(WD2_REG_HV_U_BASE_MEAS, (unsigned int *)&hv, 1);

   return 1;
}

/*------------------------------------------------------------------*/

void correct_base_hv()
{
   float diff;

   diff = user_data.ub_demand - user_data.ub_meas;

   /* only correct if deviation is more than 2 mV */
   if (user_data.ub_demand > 0 && fabs(diff) > 0.002) {
      diff *= 0.5;
      if (fabs(diff) >= 0.001) {
         user_data.ub_act += diff;
         if (user_data.ub_act < 0)
            user_data.ub_act = 0;
         if (user_data.ub_act > user_data.ub_demand+10)
            user_data.ub_act = user_data.ub_demand+10;

         hv_base_set(user_data.ub_act);

         // printf("Demand: %1.3lf, Meas: %1.3lf, Act: %1.3lf\r\n", user_data.ub_demand, user_data.ub_meas, user_data.ub_act);
      }
   }
}

/*------------------------------------------------------------------*/

void read_current()
{
  float value, du, diff;
  unsigned char c;

  if (!hv_c_voltage_read(&c, &value))
     return;

  // convert voltage to current
  value = value / user_data.r_current;

  // if HV off, current must be zero
  if (user_data.u_demand[c] == 0)
     value = 0;

  // convert to uA
  value *= 1E6;

  // 3 significant digits
  user_data.i_meas[c] = floor(value * 1000 + 0.5) / 1000.0;
  reg_bank_write(WD2_REG_HV_I_MEAS_0+c*4,
                 (unsigned int *)&user_data.i_meas[c], 1);

  // correct DAC voltage
  diff = user_data.u_demand[c] - user_data.ub_demand;

  // correct voltage drop at shunt, protection resistor and switch
  du = user_data.i_meas[c] / 1E6 * (user_data.r_current+RPROT+RSWITCH);
  diff += du;

  // limit to DAC_RANGE
  if (diff > DAC_RANGE)
     diff = DAC_RANGE;
  if (diff < 0)
     diff = 0;
  user_data.u_dac[c] = diff;
  hv_dac_set(c, diff);
}

/*---- User loop function ------------------------------------------*/

void mscb_hv_loop(void)
{
   static unsigned int last_base = 0;
   double diff;
   float temp[DIGI_TERM_MAX_N_DEVICES];
   int i, n;

   read_base_hv();
   read_current();

   diff = time_diff(time(), last_set);
   if (diff > 2) { // let base HV settle a while
      diff = time_diff(time(), last_base);
      if (diff > 0.3) {
         if (read_base_hv()) {
            correct_base_hv();
            last_base = time();
         }
      }
   }

   /* cap DAC values one second after value(s) have been set */
   diff = time_diff(time(), last_set);
   if (diff > 1) {
      for (i=0 ; i<16 ; i++)
         if (user_data.u_demand[i] > user_data.ub_demand + DAC_RANGE) {
            user_data.u_demand[i] = user_data.ub_demand + DAC_RANGE;
      	    float f = user_data.u_demand[i];
      	    reg_bank_write(WD2_REG_HV_U_TARGET_0+i*4, (unsigned int *)&f, 1);
         }
   }

   /* read maximal 4 temperatures */
   n = digi_therm_read(temp);
   if (n != -1) {
      for (i=0 ; i<n && i<4; i++)
         user_data.temp[i] = ((int)(temp[i]*10.0+0.5)) / 10.0;
      for ( ; i<4 ; i++)
         user_data.temp[i] = -999; // indicate invalid data

      for (i=0 ; i<4 ; i++)
         reg_bank_write(WD2_REG_HV_TEMP_0+i*4,
 	                   (unsigned int *)&user_data.temp[i], 1);
   }
}
