/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  GTP config
 *
 *  Author  :  gromov_A
 *  Created :  10.05.2016 
 *
 *  Description : read and write comand for drp block in virtex 5
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "utilities.h"
#include "stdio.h"
#include "xfs_types.h"
#include "dbg.h"
#include "xtime.h"

#include "drv_drp_pll.h"

/******************************************************************************/

#define DRP_PARAM_PHASE_MUX                9
#define DRP_PARAM_HIGH_TIME               10
#define DRP_PARAM_LOW_TIME                11
#define DRP_PARAM_DELAY_TIME              12
#define DRP_PARAM_NO_COUNT                13
#define DRP_PARAM_EDGE                    14
#define DRP_PARAM_FILTER_TABLE_RES        15
#define DRP_PARAM_FILTER_TABLE_LFHF       16
#define DRP_PARAM_FILTER_TABLE_CP         17
#define DRP_PARAM_LKTABLE_LOCK_REF_DLY    18
#define DRP_PARAM_LKTABLE_LOCK_FB_DLY     19
#define DRP_PARAM_LKTABLE_LOCK_CNT        20
#define DRP_PARAM_LKTABLE_LOCK_SAT_HIGH   21
#define DRP_PARAM_LKTABLE_UNLOCK_CNT      22

#define EN_POS                            31
#define WR_POS                            30
#define APPLY_RST_POS                     29
#define CLR_RST_POS                       28
#define ADR_POS                           16
#define DATA_POS                           0
#define MAX_ADR                            5
#define NR_OF_REG_BITS                    16

/******************************************************************************/

int drp_r(drp_pll_type* self, xfs_u32 address)
{
  int cmd=0;
  int value=0;
  int busy=1;
  
  cmd = (1 << EN_POS) | (0 << WR_POS) | (address << ADR_POS);
  xfs_out32(self->base_address, cmd);
  /* xil_printf("\r\nDRP interface read cmd = 0x%08X",cmd); */

  do{
    busy = xfs_in32(self->base_address) & 0x80000000;
  }while(busy != 0);  
  
  value = xfs_in32(self->base_address);
  return value;
}

/******************************************************************************/

int drp_w(drp_pll_type* self, xfs_u32 address,xfs_u32 value)
{
  int cmd=0;
  int busy=1;
  int timer;

  /* Changes can only be applied, when PLL is in reset */
  /* (see XAPP879 (v1.1 page 6)  */
  drp_pll_reset_pll(self, 1);

  cmd = (1 << EN_POS) | (1 << WR_POS) | (address << ADR_POS) | (value << DATA_POS);
  xfs_out32(self->base_address, cmd);
  /* xil_printf("\r\nDRP interface write cmd = 0x%08X\r\n",cmd); */
  
  do{
    busy = xfs_in32(self->base_address) & 0x80000000;
  }while(busy != 0); 

  /* Remove reset */
  drp_pll_reset_pll(self, 0);

  /* Wait until PLL has locked */
  timer = 1000; /* timeout after 1s */
  while(!drp_pll_is_locked(self) && timer)
  {
    usleep(1000); /* wait 1ms */
    timer--;
  }
  if(!timer)
  {
    /*xil_printf("TIMEOUT: PLL did not lock\r\n"); */
  }

  return 0;
}

/******************************************************************************/

unsigned int read_drp(drp_pll_type* self, drp_description_type *ddp)
{
  int value=0;
  unsigned int adr_cnt=0;
  int reg_val;
  unsigned char address_to_read[MAX_ADR];
  bool old_adr=false;
  int bit = -1;

  /* xil_printf("\r\n---DRP READ---"); */
  /* find address to read */
  /* xil_printf("\r\nRegister adresses to read: "); */
  while (ddp[++bit].pos <NR_OF_REG_BITS)
  {    
    old_adr=false;
    for(unsigned int i=0;i<adr_cnt;i++)
    {
      if(address_to_read[i]==ddp[bit].addr)
      {
        old_adr = true;
      }
    }
    if(!old_adr)
    {
      /* xil_printf("0x%02X   ",ddp[bit].addr); */
      address_to_read[adr_cnt++]= ddp[bit].addr;
    }
  }
  
  if(adr_cnt > MAX_ADR)
  {
    /*xil_printf("\r\n !!! MAX_ADR exceeded. Increase MAX_ADR in drp_cmd.h"); */
    return 0;
  }
  
  /* read values */
  for(unsigned int read_adr=0;read_adr<adr_cnt;read_adr++)
  {
    reg_val=drp_r(self, address_to_read[read_adr]);
    /*xil_printf("\r\nregister content[0x%02X] = 0x%08X",address_to_read[read_adr],reg_val); */
    bit=-1;
    /*write each bit value */
    while (ddp[++bit].pos <NR_OF_REG_BITS)
    {
      /* if address is correct and the bit is set */
      if((ddp[bit].addr==address_to_read[read_adr]) && (reg_val & (1<<ddp[bit].pos)))
      {
        value = value | (1 << bit);  
      }
    }
  }
  /* //Not needed for pll drp:
  //shift if value doesn't start at bit 0
  if(ddp[++bit].pos==0xfe)
  {
    value = value << 1;
    //xil_printf("\r\nshifted for read");
  }
  */
  
  /*xil_printf("\r\nDRP parameter setting = 0x%02X",value); */
  /*xil_printf("\r\n---DRP READ FINISH---\r\n"); */
  return value;
}

/******************************************************************************/

unsigned int write_drp(drp_pll_type* self, drp_description_type *ddp,xfs_u32 value)
{
  unsigned int adr_cnt=0;
  int reg_val;
  int address_to_write[MAX_ADR];
  bool old_adr=false;
  int bit = -1;

  /*xil_printf("\r\n---DRP WRITE---"); */
  /*find address to read */
  while (ddp[++bit].pos <NR_OF_REG_BITS)
  {    
    old_adr=false;
    for(unsigned int i=0;i<adr_cnt;i++)
    {
      if(address_to_write[i]==ddp[bit].addr)
      {
        old_adr = true;
      }
    }
    if(!old_adr)
    {
      /*xil_printf("0x%02X   ",ddp[bit].addr); */
      address_to_write[adr_cnt++]= ddp[bit].addr;
    }
  }
  
  if(adr_cnt > MAX_ADR){
    if(DBG_WARN) xil_printf("\r\n !!! MAX_ADR exceeded. Increase MAX_ADR in drp_cmd.h");
    return 0;
  }
  
  /* //Not needed for pll drp:
  //if value doesn't start at bit 0
  if(ddp[++bit].pos==0xfe)
  {
    value = value >> 1;
    xil_printf("\r\nshifted for write");
  }
  */

  for(unsigned int write_adr=0; write_adr<adr_cnt; write_adr++)
  {
    reg_val = drp_r(self, address_to_write[write_adr]);
    /*xil_printf("\r\nReading register[0x%02X] = 0x%04X",address_to_write[write_adr],reg_val); */
    bit=-1;
    /* write each bit value */
    while (ddp[++bit].pos <16)
    {
      if(ddp[bit].addr==address_to_write[write_adr])
      {
        if( (value >> bit) & 0x01 )
        {
          reg_val = reg_val | (0x0001 << ddp[bit].pos);
        }
        else
        {
          reg_val = reg_val & ~(0x0001 << ddp[bit].pos);
        }
      }
    }
    
    /*xil_printf("\r\nWriting register[0x%02X] = 0x%04X",address_to_write[write_adr],reg_val); */

    drp_w(self, address_to_write[write_adr],reg_val);    
  }
  /*xil_printf("\r\n---DRP WRITE FINISH---\r\n"); */
  
  return 1;
}

/******************************************************************************/

unsigned int drp_pll_update_vco_frequency(drp_pll_type* self)
{
  unsigned int divclk_divider;
  unsigned int clkfb_divider;
  unsigned int clk0_divider;

  if(read_drp(self, CLKFB_No_Count)) clkfb_divider = 1;
  else                               clkfb_divider = read_drp(self, CLKFB_High_Time) + read_drp(self, CLKFB_Low_Time);

  if(read_drp(self, DIVCLK_No_Count)) divclk_divider = 1;
  else                                divclk_divider = read_drp(self, DIVCLK_High_Time) + read_drp(self, DIVCLK_Low_Time);

  if(self->clkfb_source == CLKFB_SRC_CLKFBOUT)
  {
    self->vco_frequency = self->clkin_frequency * clkfb_divider / divclk_divider;
  }
  else
  {
    if(read_drp(self, CLK0_No_Count)) clk0_divider = 1;
    else                              clk0_divider = read_drp(self, CLK0_High_Time) + read_drp(self, CLK0_Low_Time);

    self->vco_frequency = self->clkin_frequency * clkfb_divider * clk0_divider / divclk_divider;
  }
  
  return self->vco_frequency;
}

/******************************************************************************/

drp_description_type* drp_pll_get_param_ptr(xfs_u32 channel, xfs_u32 parameter)
{
  switch(channel)
  {
    case DRP_CHANNEL_CLK0:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLK0_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLK0_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLK0_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLK0_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLK0_No_Count;
        case DRP_PARAM_EDGE       : return CLK0_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_CLK1:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLK1_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLK1_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLK1_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLK1_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLK1_No_Count;
        case DRP_PARAM_EDGE       : return CLK1_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_CLK2:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLK2_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLK2_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLK2_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLK2_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLK2_No_Count;
        case DRP_PARAM_EDGE       : return CLK2_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_CLK3:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLK3_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLK3_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLK3_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLK3_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLK3_No_Count;
        case DRP_PARAM_EDGE       : return CLK3_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_CLK4:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLK4_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLK4_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLK4_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLK4_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLK4_No_Count;
        case DRP_PARAM_EDGE       : return CLK4_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_CLK5:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLK5_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLK5_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLK5_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLK5_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLK5_No_Count;
        case DRP_PARAM_EDGE       : return CLK5_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_CLKFB:
    {
      switch(parameter)
      {
        case DRP_PARAM_PHASE_MUX  : return CLKFB_Phase_MUX;
        case DRP_PARAM_HIGH_TIME  : return CLKFB_High_Time;
        case DRP_PARAM_LOW_TIME   : return CLKFB_Low_Time;
        case DRP_PARAM_DELAY_TIME : return CLKFB_Delay_Time;
        case DRP_PARAM_NO_COUNT   : return CLKFB_No_Count;
        case DRP_PARAM_EDGE       : return CLKFB_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_DIVCLK:
    {
      switch(parameter)
      {
        case DRP_PARAM_HIGH_TIME  : return DIVCLK_High_Time;
        case DRP_PARAM_LOW_TIME   : return DIVCLK_Low_Time;
        case DRP_PARAM_NO_COUNT   : return DIVCLK_No_Count;
        case DRP_PARAM_EDGE       : return DIVCLK_Edge;
        default : return NULL;
      }
    }
    case DRP_CHANNEL_NONE:
    {
      switch(parameter)
      {
        case DRP_PARAM_FILTER_TABLE_RES      : return FILTER_TABLE_RES;
        case DRP_PARAM_FILTER_TABLE_LFHF     : return FILTER_TABLE_LFHF;
        case DRP_PARAM_FILTER_TABLE_CP       : return FILTER_TABLE_CP;
        case DRP_PARAM_LKTABLE_LOCK_REF_DLY  : return LKTABLE_LOCK_REF_DLY;
        case DRP_PARAM_LKTABLE_LOCK_FB_DLY   : return LKTABLE_LOCK_FB_DLY;
        case DRP_PARAM_LKTABLE_LOCK_CNT      : return LKTABLE_LOCK_CNT;
        case DRP_PARAM_LKTABLE_LOCK_SAT_HIGH : return LKTABLE_LOCK_SAT_HIGH;
        case DRP_PARAM_LKTABLE_UNLOCK_CNT    : return LKTABLE_UNLOCK_CNT;
        default : return NULL;
      }
    }
    default : return NULL;
  }
}

/******************************************************************************/

unsigned int drp_pll_update_filter_and_lock_config(drp_pll_type* self)
{
  unsigned int mult;
  unsigned int dutycycle;

  drp_pll_get_divider_and_dutycycle(self, DRP_CHANNEL_CLKFB, mult, dutycycle);

  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_FILTER_TABLE_RES),  spartan6_filter_table[mult][self->pll_bandwidth].res);
  /* Do not set (no bit position defined in XAPP879): */
  /* write_drp(drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_FILTER_TABLE_LFHF), spartan6_filter_table[mult][pll_bandwidth].lfhf); */
  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_FILTER_TABLE_CP),   spartan6_filter_table[mult][self->pll_bandwidth].cp);

  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_LKTABLE_LOCK_REF_DLY),  spartan6_lock_table[mult].lock_ref_dly);
  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_LKTABLE_LOCK_FB_DLY),   spartan6_lock_table[mult].lock_fb_dly);
  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_LKTABLE_LOCK_CNT),      spartan6_lock_table[mult].lock_cnt);
  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_LKTABLE_LOCK_SAT_HIGH), spartan6_lock_table[mult].lock_sat_high);
  write_drp(self, drp_pll_get_param_ptr(DRP_CHANNEL_NONE, DRP_PARAM_LKTABLE_UNLOCK_CNT),    spartan6_lock_table[mult].unlock_cnt);
  
  return 1;
}

/******************************************************************************/

void drp_pll_init(drp_pll_type* self, xfs_u32 b_address, xfs_u32 f_clkin_mhz, xfs_u32 clkfb_src, xfs_u8 pll_bw)
{
  self->base_address    = io_remap(b_address);
  self->clkin_frequency = f_clkin_mhz;
  self->clkfb_source    = clkfb_src;
  self->pll_bandwidth   = pll_bw;

  drp_pll_update_vco_frequency(self);
}

/******************************************************************************/

unsigned int drp_pll_set_phase_step(drp_pll_type* self, unsigned int channel, unsigned int phase_step)
{
  write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_PHASE_MUX),  phase_step % 8);
  write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_DELAY_TIME), phase_step / 8);
  
  return 1;
}

/******************************************************************************/

unsigned int drp_pll_get_phase_step(drp_pll_type* self, unsigned int channel, unsigned int &phase_step)
{
  phase_step  = read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_DELAY_TIME)) * 8;
  phase_step += read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_PHASE_MUX));

  return 1;
}

/******************************************************************************/

unsigned int drp_pll_set_phase(drp_pll_type* self, unsigned int channel, unsigned int phase)
{
  unsigned int max_phase_steps;
  unsigned int phase_step;
  
  max_phase_steps = drp_pll_get_phase_steps(self, channel);
  
  phase_step = (((phase%360) * max_phase_steps) + 180) / 360 ; /* rounded phase_step = ( phase / (360/max_phase_steps) ) + 0.5 */

  return drp_pll_set_phase_step(self, channel, phase_step);
}

/******************************************************************************/

unsigned int drp_pll_get_phase(drp_pll_type* self, unsigned int channel, unsigned int &phase)
{
  unsigned int max_phase_steps;
  unsigned int phase_step;
  
  phase_step  = read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_DELAY_TIME)) * 8;
  phase_step += read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_PHASE_MUX));

  max_phase_steps = drp_pll_get_phase_steps(self, channel);
  
  phase = (phase_step * 360) / max_phase_steps;
  
  return 1;
}

/******************************************************************************/

unsigned int drp_pll_get_phase_steps(drp_pll_type* self, unsigned int channel)
{
  unsigned int channel_divider;
  
  channel_divider = read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_HIGH_TIME)) + read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_LOW_TIME));
  
  return channel_divider * 8; /* There are 8 phases per VCO cycle */
}

/******************************************************************************/

unsigned int drp_pll_set_divider_and_dutycycle(drp_pll_type* self, unsigned int channel, unsigned int divider, unsigned int dutycycle_percent)
{
  unsigned int high_time;
  unsigned int low_time;
  unsigned int edge;

  if(divider == 1)
  {
    write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_NO_COUNT), 1);
  }
  else if(divider > 1)
  {
    high_time = (divider * dutycycle_percent) / 100;
    if(high_time < 1)
    {
      high_time = 1;
    }
    edge = (unsigned int)(((divider * dutycycle_percent) + 50) / 100) - high_time; /* rounded high_time - floor high_time */
    low_time = divider - high_time;
    write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_NO_COUNT),  0);
    write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_HIGH_TIME), high_time);
    write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_LOW_TIME),  low_time);
    write_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_EDGE),      edge);

    if(channel == DRP_CHANNEL_CLKFB)
    {
      /* configure filter and lock with lookup tables */
      drp_pll_update_filter_and_lock_config(self);
    }
  }

  if( (channel == DRP_CHANNEL_CLK0) || (channel == DRP_CHANNEL_CLKFB) || (channel == DRP_CHANNEL_DIVCLK) )
  {
    drp_pll_update_vco_frequency(self);
  }
  
  return 1;
}

/******************************************************************************/

unsigned int drp_pll_get_divider_and_dutycycle(drp_pll_type* self, unsigned int channel, unsigned int& divider, unsigned int& dutycycle_percent)
{
  unsigned int high_time;
  unsigned int low_time;
  unsigned int edge;

  if(read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_NO_COUNT)))
  {
    divider = 1;
    dutycycle_percent = 50;
  }
  else
  {
    high_time = read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_HIGH_TIME));
    low_time  = read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_LOW_TIME));
    edge      = read_drp(self, drp_pll_get_param_ptr(channel, DRP_PARAM_EDGE));
    divider = high_time + low_time;
    if(edge)
    {
      dutycycle_percent = ((high_time * 100) + 50) / divider;
    }
    else
    {
      dutycycle_percent = (high_time * 100) / divider;
    }
  }

  return 1;
}

/******************************************************************************/

int drp_pll_change_fin(drp_pll_type* self, unsigned int fin_new_mhz)
{
  unsigned int divider;
  unsigned int dutycycle;
  unsigned int f_pfd;
  
  drp_pll_get_divider_and_dutycycle(self, DRP_CHANNEL_DIVCLK, divider, dutycycle);
  
  f_pfd = self->clkin_frequency / divider;

  if( (self->clkin_frequency%divider) || (fin_new_mhz%f_pfd) )
  {
    /* error: so far only integer values are allowed by this software */
    return 0;
  }
  else
  {
    self->clkin_frequency = fin_new_mhz;
    divider = fin_new_mhz / f_pfd;
    drp_pll_set_divider_and_dutycycle(self, DRP_CHANNEL_DIVCLK, divider, 50);
    drp_pll_reset_pll(self, 1);
    usleep(10);
    drp_pll_reset_pll(self, 0);
    return 1;
  }
}

/******************************************************************************/

int drp_pll_reset_pll(drp_pll_type* self, xfs_u32 value)
{
  if(value)
  {
    xfs_out32(self->base_address, (1 << APPLY_RST_POS));
  }
  else
  {
    xfs_out32(self->base_address, (1 << CLR_RST_POS));
  }

  return 0;
}

/******************************************************************************/

unsigned int drp_pll_is_locked(drp_pll_type* self)
{
  if(xfs_in32(self->base_address) & 0x20000000)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

unsigned int drp_pll_set_bandwidth(drp_pll_type* self, unsigned char bandwidth)
{
  self->pll_bandwidth = bandwidth;
  drp_pll_update_filter_and_lock_config(self);
  return 1;
}

/******************************************************************************/
/******************************************************************************/

