/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e
 *  Created :  21.05.2014 08:56:26
 *
 *  Description :  Module for controling and testing the external SRAM.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "memtest.h"
#include "xfs_printf.h"
#include "utilities.h"
#include <stdlib.h>
#include "xparameters.h"
#include "system.h"
#include "cmd_processor.h"
#include "dbg.h"

/************************************************************/

  int ext_mem_test(int argc, char **argv)
  {
    CMD_HELP("[<mode>]",
             "tests the external RAM (write->read->compare)",
             "Tests the external SRAM in a full write, read and compare sequence."  
             "  <mode> : defines the bit width of the memory access.\r\n"
             "           Available modes are 8, 16 and 32 bits.\r\n"
             "           If no mode is specified, all modes are tested sequentually.\r\n"
            );

    /* Check for minimum number of arguments */
    if(argc < 2)
    {
      memtest(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
    }
    else
    {
      if(fstrcmp(argv[1],"8"))
      {
        xfs_printf("Testing RAM with 8bit access...\r\n");
        memtest_8bit(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
      }
      else if(fstrcmp(argv[1],"16"))
      {
        xfs_printf("Testing RAM with 16bit access...\r\n");
        memtest_16bit(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
      }
      else if(fstrcmp(argv[1],"32"))
      {
        xfs_printf("Testing RAM with 32bit access...\r\n");
        memtest_32bit(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
      }
      else
      {
        xfs_printf("E%02X: Invalid arguments. Type \"extramtest help\" for help.\r\n", ERR_INVALID_ARGS);
        return 0;
      }
    }

    return 0;
  }

/************************************************************/

  int module_extram_help(int argc, char **argv)
  {
    CMD_HELP("",
             "External RAM Test and Control Module",
             "Can be used to control and test the external SRAM."  
            );

    return 0;
  }

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type ext_mem_cmd_table[] =
  {
    {0, "extram", module_extram_help},
    {0, "extramtest", ext_mem_test},
    {0, NULL, NULL}
  };

/************************************************************/
