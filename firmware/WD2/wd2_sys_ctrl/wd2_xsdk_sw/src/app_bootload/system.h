/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  12.02.2018 13:24:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */
 
#ifndef __SYSTEM_H__
#define __SYSTEM_H__


/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "drv_plb_gpio.h"
#include "xparameters.h"
#include "xfs_types.h"
#include "drv_spi_flash_w25q64.h"
#include "plb_spi_master_v2_00_a.h"
#include "plb_ll_fifo.h"
#include "network_if.h"
#include "dhcp.h"
#include "tftp_server.h"
#include "drv_plb_wd2_reg_bank.h"
#include "gmac_lite.h"
#include "fw_env.h"

/******************************************************************************/
/* macro definitions                                                          */
/******************************************************************************/


/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

#define SW_BOOTLOADER "SW_BL"

#define MAX_HOSTNAME_LENGTH  62

#define GMAC_COM_BUFF_LEN    1600


/******************************************************************************/
/* System Environment                                                         */
/******************************************************************************/

/* environment type */
typedef struct
{
  unsigned int   serial_no;
  char           hostname[MAX_HOSTNAME_LENGTH];
  /*unsigned char  eth_mac_addr[6]; */
  /*unsigned char  eth_ip_addr[4];  */
  /*unsigned int   eth_dhcp;        */
} sys_cfg_type;

/******************************************************************************/
/* class definition                                                           */
/******************************************************************************/

typedef struct 
{
    plb_gpio_type           gpio_sys;

    plb_spi_master          cfg_spi_master;
    plb_spi_salve_settings  cfg_spi_fpga_settings;
    spi_flash_w25q64        spi_flash;

    plb_ll_fifo_type        plb_fifo_gmac_0;
    network_if_type         nw_if_gmac_0;
    gmac_lite_type          gmac_lite_0;
    dhcp_type               dhcp_gmac_0;
    tftp_server_type        tftp_gmac_0;
    
    plb_register_bank       reg_bank;

    fw_env_type             env;      
    sys_cfg_type            cfg;
} system_type;

/******************************************************************************/

system_type system_hw;

#define SYSPTR(x) (&(system_hw.x))

/******************************************************************************/

xfs_u32 system_init(void);
xfs_u32 system_env_init(void);

void init_settings(int snr);
void print_sys_info(void);


#endif /* __SYSTEM_H__ */
