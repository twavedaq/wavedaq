/*
 *
 * Copyright (c) 2009 Xilinx, Inc. All Rights Reserved.
 *
 * You may copy and modify these files for your own internal use solely with
 * Xilinx programmable logic devices and  Xilinx EDK system or create IP
 * modules solely for Xilinx programmable logic devices and Xilinx EDK system.
 * No rights are granted to distribute any files unless they are distributed in
 * Xilinx programmable logic devices.
 *
 */

/*
 *      Simple SREC Bootloader
 *      This simple bootloader is provided with Xilinx EDK for you to easily re-use in your
 *      own software project. It is capable of booting an SREC format image file
 *      (Mototorola S-record format), given the location of the image in memory.
 *      In particular, this bootloader is designed for images stored in non-volatile flash
 *      memory that is addressable from the processor.
 *
 *      Please modify the define "SPI_FLASH_SOFTWARE_ADDR" in the wd2_flash_memory_map.h header file
 *      to point to the memory location from which the bootloader has to pick up the
 *      flash image from.
 *
 *      You can include these sources in your software application project in XPS and
 *      build the project for the processor for which you want the bootload to happen.
 *      You can also subsequently modify these sources to adapt the bootloader for any
 *      specific scenario that you might require it for.
 *
 */


#include "blconfig.h"
#include "system.h"
#include "gpio_slow_control.h"
#include "wd2_flash_memory_map.h"
#include "xfs_types.h"
#include "errors.h"
#include "srec.h"
#include "xilinx_cfg.h"
#include "drv_spi_flash_w25q64.h"
#include "drv_plb_wd2_reg_bank.h"
#include "xfs_printf.h"
#include "utilities.h"
#include "dbg.h"
#include "xparameters.h"
#include "tftp_config.h"
#include "revision.h"

#include "xuartlite_l.h"

#include "register_map_wd2.h"


#include "fw_env.h"

/* Defines */
#define CR             13
#define PR_BAR_ITEMS   25

/* Comment the following line, if you want a smaller and faster bootloader which will be silent */
#define VERBOSE

/* Declarations */
/*static void display_progress (xfs_u32 lines);*/
static void display_progress(xfs_u32 percent);
static int load_header(void);
static xfs_u8 load_exec(void);
static xfs_u8 flash_get_srec_line(void);

extern int srec_line;

#ifdef __cplusplus
extern "C" {
#endif

#ifdef __cplusplus
}
#endif

/* Data structures */
static srec_info_t srinfo;
static sw_file_info_type sw_header_info;
static xfs_u8 sr_buf[SREC_MAX_BYTES];
static xfs_u8 sr_data_buf[SREC_DATA_MAX_BYTES];

static xfs_u32 sr_buf_i;
static xfs_u32 rec_i;
static xfs_u32 flash_offset;


#ifdef VERBOSE
static const char *bl_errors[] = {
    "",
    "Error while copying executable image into RAM",
    "Error while reading an SREC line from flash",
    "SREC line is corrupted",
    "SREC has invalid checksum",
    "SREC incompatible with firmware",
    "Aborted by user"
};
#endif

/* We don't use interrupts/exceptions.
   Dummy definitions to reduce code size on MicroBlaze */
#ifdef __MICROBLAZE__
void _interrupt_handler () {}
void _exception_handler () {}
void _hw_exception_handler () {}
#endif

/******************************************************************************/

void nw_if_handler(network_if_type *nw_if, tftp_server_type *tftp_server)
{
  int k;
  unsigned int udp_port;

  if (nw_if->do_dhcp)
  {
    dhcp_request(nw_if->dhcp);
  }

  tftp_check_timeout(tftp_server);


  k = nw_if->ll_fifo->receive(nw_if->ll_fifo, nw_if->com_buff, nw_if->com_buff_len) ;

  if (k > 4)
  {
    k -= 4; /* subtract FCS length */

    if (dhcp_handle_req(nw_if->dhcp, nw_if->com_buff, k))
    {
      /*  xfs_local_printf("handle_dhcp\r\n\r\n"); */
      /*  nothing to do here                 */
    }
    else if (nw_if->ip_address_valid)
    {
      if (handle_arp(nw_if , nw_if->com_buff, k))
      {
        /*  xfs_local_printf("ARP_REQUEST\r\n\r\n"); */
        /*  nothing to do here                 */
      }
      else if (handle_ping(nw_if , nw_if->com_buff, k))
      {
        /*  xfs_local_printf("handle_ping\r\n\r\n"); */
        /*  nothing to do here                 */
      }
      else if ((udp_port = check_udp(nw_if , nw_if->com_buff, k)) > 0 )
      {
/*       if ( udp_port == FC_UDP_TERMINAL_CMD_PORT)                 */
/*       {                                                          */
/*         udp_terminal_handle_request(nw_if ,com_buff, k);         */
/*       }                                                          */
/*       else if ( udp_port == FC_UDP_WD2_TERMINAL_CMD_PORT)        */
/*       {                                                          */
/*         udp_wd2_terminal_handle_request(nw_if , com_buff, k);    */
/*       }                                                          */
/*       else if (handle_tftp(tftp_server , com_buff, k, udp_port)) */
       if (handle_tftp(tftp_server , nw_if->com_buff, k, udp_port))
       {
         /* xfs_local_printf("handle_tftp\r\n\r\n"); */
         /* nothing to do here                 */
       }
      }
    }
  }
}

/******************************************************************************/

char key_pressed(void)
{
  char c;

  if (!XUartLite_IsReceiveEmpty(STDIN_BASEADDRESS))
  {
    c = XUartLite_ReadReg(STDIN_BASEADDRESS, XUL_RX_FIFO_OFFSET);
    return c;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

int chk_abort(void)
{
  char c;

  c = key_pressed();

  if ((c==27) || (c==32))
  {
    return 1;
  }
  return 0;

}

/******************************************************************************/
#define MAX_CMD_LEN 32

typedef struct
{
    int esc;
    unsigned int bptr;
    char buffer[MAX_CMD_LEN+1];
} term_input_lite_type;


term_input_lite_type string_input;


/******************************************************************************/

int term_input_lite(term_input_lite_type* self)
{
  int len = -2;
  char c;

  if (!XUartLite_IsReceiveEmpty(STDIN_BASEADDRESS))
  {
    c = (xfs_u8)XUartLite_ReadReg(STDIN_BASEADDRESS, XUL_RX_FIFO_OFFSET);
    /* xfs_printf("c =   %c    (%d)   self->bptr=%d \r\n",c,c,self->bptr); */
    if (c==0x01b)  /* escape */
    {
      self->esc = 100; /* escape timeout value*/
    }
    else if (self->esc)   /* small workaround for escape sequences */
    {
      if (( (c >= 'A') && (c <='H')) ||(c=='~') || ( (c >= 'Q') && (c <='S'))  )
      {
        self->esc=0;
      }
    }
    else if ((c==0x08) || (c==0x7f))  /* backspace */
    {
      if (self->bptr)
      {
        self->bptr--;
        xfs_printf("\x08 \x08");
      }
    }
    else if ((c==0x0a) || (c==0x0d))
    {
      xfs_terminal_printf("\r\n");
      self->buffer[self->bptr]=0;
      len = self->bptr;
      self->bptr = 0;
    }
    else if  ((c >= 32) && (c <= 126))
    {
      if (self->bptr < MAX_CMD_LEN)
      {
        xfs_terminal_printf("%c",c);
        self->buffer[self->bptr++]=c;
      }
    }
    else if  ((c == 3) || (c == 4))
    {
      return -1 /* abort */;
    }
  }
  else if (self->esc)
  {
    self->esc--;
    if (!self->esc) return -1 /* abort */;
    usleep(100);
  }

  return len;
}

/******************************************************************************/

int get_string(term_input_lite_type* self)
{
  int len = -2;
  self->esc=0;
  self->bptr=0;

  while (len < -1) len = term_input_lite(self);

  return len;
}

/******************************************************************************/

int get_string_prompt(term_input_lite_type* self, char* prompt)
{
  int len;
  xfs_local_printf("\r\n" "%s > ", prompt);

  len = get_string(&string_input);
  return len;
}

/******************************************************************************/

void boot_from_spi(void)
{
  xfs_u8 ret;

#ifdef VERBOSE
    xfs_local_printf("Loading SREC image from flash @ address: 0x%08X\r\n\r\n", SPI_FLASH_SOFTWARE_ADDR);
#endif
  /* read header information */
  if (load_header() != XFS_OK)
  {
    xfs_local_printf("No valid software header found\r\n");
    return;
  }


  /* read and copy software */
  srec_line = 0;
  sr_buf_i = 0;
  flash_offset = SPI_FLASH_SOFTWARE_ADDR;

#ifdef VERBOSE
    xfs_local_printf("Loading: (Press ESC to abort)\r\n");
#endif

  /* returns only in case of error or user abort (ESC key pressed) */
  ret = load_exec();

  if (ret == LD_SREC_LINE_ERROR)
  {
    // tbd: line number ???
    xfs_local_printf("\r\n\r\nERROR: %s in SREC line %d\r\n", bl_errors[ret], srec_line);
  }
  else if (ret == BL_ABORTED)
  {
    xfs_local_printf("\r\n\r\nAborted...\r\n");
  }
  else if (ret < (sizeof(bl_errors) / sizeof(char*)) )
  {
    xfs_local_printf("\r\n\r\nERROR: %s \r\n", bl_errors[ret]);
  }

}

/******************************************************************************/

void boot_loader_help(void)
{
  xfs_local_printf("\r\n" "Bootloader keys:" "\r\n" "\r\n");
  xfs_local_printf(" b : boot from spi\r\n");
  xfs_local_printf(" c : clear software header\r\n");
  xfs_local_printf(" d : do dhcp\r\n");
  xfs_local_printf(" h : enter hostname\r\n");
  xfs_local_printf(" i : enter ip address\r\n");
  xfs_local_printf(" m : enter mac address\r\n");
  xfs_local_printf(" n : network information\r\n");
  xfs_local_printf(" p : printenv\r\n");
  xfs_local_printf(" r : reconfigure FPGA\r\n");
  xfs_local_printf(" s : setup and save initial environment\r\n");

  xfs_local_printf("\r\n" " any other key shows this help" "\r\n" "\r\n");

}

/******************************************************************************/

void key_handler(char c)
{
  unsigned int data = WD2_RECONFIGURE_FPGA_MASK;
  unsigned char mac_addr[6];
  unsigned char ip_addr[4];
  char *name;
  int status;
  int snr;

  switch (c)
  {
      case 'b':   boot_from_spi();
                  break;

      case 'h':   xfs_local_printf("Just press 'Enter' to use Hostname from environment\r\n");
                  status = get_string_prompt(&string_input, "Enter Hostname");
                  if (status == 0)
                  {
                    name = fw_getenv(SYSPTR(env), "hostname");
                    if (name) ncpy(SYSPTR(cfg)->hostname, name, MAX_HOSTNAME_LENGTH);
                  }
                  else if (status > 0)
                  {
                    ncpy(SYSPTR(cfg)->hostname, string_input.buffer, MAX_HOSTNAME_LENGTH);
                  }
                  else
                  {
                    xfs_local_printf("\r\n" "\r\n" "aborted..." "\r\n" "\r\n");
                    break;
                  }

                  SYSPTR(cfg)->hostname[MAX_HOSTNAME_LENGTH-1]=0;
                  xfs_local_printf("\r\nHostname:    %s\r\n", SYSPTR(cfg)->hostname);
                  break;

      case 'i':   xfs_local_printf("Just press 'Enter' to use IP Address from environment\r\n");
                  status = get_string_prompt(&string_input, "Enter IP Address");
                  if (status == 0)
                  {
                    status = parse_ip(fw_getenv(SYSPTR(env), "ipaddr"), ip_addr);
                  }
                  else if (status > 0)
                  {
                    status = parse_ip(string_input.buffer, ip_addr);
                  }
                  else
                  {
                    xfs_local_printf("\r\n" "\r\n" "aborted..." "\r\n" "\r\n");
                    break;
                  }


                  if (status == XFS_OK)
                  {
                    nw_if_set_ip_address(SYSPTR(nw_if_gmac_0), ip_addr);
                    nw_if_print_status(SYSPTR(nw_if_gmac_0));
                  }
                  else
                  {
                    xfs_local_printf("Error parsing IP Address\r\n");
                  }
                  break;


      case 'm':   xfs_local_printf("Just press 'Enter' to use MAC Address from environment\r\n");
                  status = get_string_prompt(&string_input, "Enter MAC Address");
                  if (status == 0)
                  {
                    status = parse_mac(fw_getenv(SYSPTR(env), "ethaddr"), mac_addr);
                  }
                  else if (status > 0)
                  {
                    status = parse_mac(string_input.buffer, mac_addr);
                  }
                  else
                  {
                    xfs_local_printf("\r\n" "\r\n" "aborted..." "\r\n" "\r\n");
                    break;
                  }

                  if (status == XFS_OK)
                  {
                    nw_if_set_mac_address(SYSPTR(nw_if_gmac_0), mac_addr);
                    nw_if_print_status(SYSPTR(nw_if_gmac_0));
                  }
                  else
                  {
                    xfs_local_printf("Error parsing MAC Address\r\n");
                  }
                  break;

      case 's':   xfs_local_printf("Press just 'Enter' or 'ESC' to abort\r\n");
                  status = get_string_prompt(&string_input, "Serial Number (digits only)");
                  if (status > 0)
                  {
                    snr = xfs_atoi(string_input.buffer);
                    xfs_local_printf("Are you sure to initialize the environment for serial number %d ?\r\n", snr);
                    status = get_string_prompt(&string_input, "Type 'yes' to continue");
                    if ((status > 0) && (fstrcmp(string_input.buffer, "yes")))
                    {
                      init_settings(snr);
                      break;
                    }
                  }

                  xfs_local_printf("\r\n" "\r\n" "aborted..." "\r\n" "\r\n");
                  break;

      case 'r':   reg_bank_set(WD2_REG_RST, &data, 1);
                  break;

      case 'd':   xfs_local_printf("enable dhcp \r\n");
                  nw_if_set_dhcp(SYSPTR(nw_if_gmac_0), 1);
                  break;

      case 'c':   xfs_local_printf("clear software header\r\n");
                  spi_flash_sector_erase(SYSPTR(spi_flash), SPI_FLASH_SOFTWARE_ADDR + SPI_FLASH_SW_HEADER_OFFS);
                  break;

      case 'n':   xfs_local_printf("\r\nHostname:    %s\r\n", SYSPTR(cfg)->hostname);
                  nw_if_print_status(SYSPTR(nw_if_gmac_0));
                  break;

      case 'p':   xfs_local_printf("printenv\r\n\r\n");
                  fw_printenv(SYSPTR(env), 1, 0);
                  break;

      default:    boot_loader_help();
                  break;

  }
}


/******************************************************************************/

int main(void)
{
  char c;

  system_init();
  gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_BL_LOAD);

  mspi_init(SYSPTR(cfg_spi_master), XPAR_PLB_SPI_MASTER_CONFIG_MEMORY_BASEADDR, 1);
  mspi_slv_init(SYSPTR(cfg_spi_master), SYSPTR(cfg_spi_fpga_settings), 0xFFFFFFFE, W25Q64_CPOL, W25Q64_CPHA, W25Q64_LSB_FIRST);
  spi_flash_init(SYSPTR(spi_flash), SYSPTR(cfg_spi_fpga_settings));

#ifdef VERBOSE
    xfs_local_printf("\r\n" "SREC Bootloader" "\r\n");
#endif



    boot_from_spi();

    /* If we reach here, we are in error */
    gpio_sc_set_sw_status(GPIO_LED_SW_STATUS_BL_FAIL);

#ifdef VERBOSE

    xfs_local_printf("\r\n" "\r\n" "Bootloader Failure" "\r\n" "Starting fallback operation" "\r\n");

    xfs_local_printf("\r\n----------------------------------------------------------\r\n");
    xfs_local_printf("-- Bootloader Version Information:\r\n");
    xfs_local_printf("----------------------------------------------------------\r\n\r\n");
    print_sys_info();
    xfs_local_printf("\r\n----------------------------------------------------------\r\n\r\n");
#endif


  dhcp_construct(SYSPTR(dhcp_gmac_0), SYSPTR(nw_if_gmac_0));
  gmac_lite_init(SYSPTR(gmac_lite_0));

  system_env_init();

  tftp_config_init();
  tftp_server_construct(SYSPTR(tftp_gmac_0), SYSPTR(nw_if_gmac_0), tftp_targets);


#ifdef VERBOSE
  {
    unsigned char buff[6];
    xfs_local_printf("\r\nHostname:    %s\r\n", SYSPTR(cfg)->hostname);
    nw_if_get_ip_address(SYSPTR(nw_if_gmac_0), buff);
    xfs_local_printf("IP Address:  %d.%d.%d.%d\r\n", buff[0], buff[1], buff[2], buff[3] );
    nw_if_get_mac_address(SYSPTR(nw_if_gmac_0), buff);
    xfs_local_printf("MAC Address: %02X:%02X:%02X:%02X:%02X:%02X\r\n", buff[0], buff[1], buff[2], buff[3], buff[4], buff[5] );
  }
#endif
    boot_loader_help();

    /* start fallback operation main loop */
    while(1)
    {
      /* chk_rst_rx_link(); */
      time_check_seconds();

      nw_if_handler(SYSPTR(nw_if_gmac_0), SYSPTR(tftp_gmac_0));

      c = key_pressed();
      if (c > 0) key_handler(c);
    }

    return 0;
}


/******************************************************************************/

#ifdef VERBOSE
/*static void display_progress (xfs_u32 count)*/
/*{                                            */
      /* Send carriage return */
/*    outbyte (CR);                            */
/*    print  ("Bootloader: Processed (0x)");   */
/*    putnum (count);                          */
/*    print (" S-records");                    */
/*}                                            */
static void display_progress (xfs_u32 percent)
{
  int i;
  /* Send carriage return */
  xfs_local_printf("%c", CR);
  xfs_local_printf("[");
  for(i=0;i<PR_BAR_ITEMS;i++)
  {
    if( (i*(100/PR_BAR_ITEMS)) <= percent ) xfs_local_printf("=");
    else                                    xfs_local_printf(" ");
  }
  xfs_local_printf("] %d%%", percent);
}
#endif

/******************************************************************************/

static int load_header(void)
{
  xfs_u32 flash_addr;
  xfs_u32 flash_len;
  xfs_u8* flash_buff;


  flash_addr = SPI_FLASH_SOFTWARE_ADDR + SPI_FLASH_SW_HEADER_OFFS;
  flash_len  = sizeof(sw_file_info_type);
  flash_buff = (unsigned char*) &sw_header_info;

  spi_flash_read(SYSPTR(spi_flash), flash_buff, flash_addr, flash_len);

// tbd: header ID
// if (sw_header_info.info.head_id  != SOFTWARE_HEADER_ID) return XFS_ERROR;
  if (sw_header_info.info.checksum != sw_file_info_checksum(&sw_header_info) ) return XFS_ERROR;

  return XFS_OK; /* OK */
}


/******************************************************************************/

static xfs_u8 load_exec(void)
{
    xfs_u8 ret;
    void (*laddr)();
    xfs_i8 done = 0;
    xfs_u32 processed = 0;
    xfs_u32 displayed = 0;
    unsigned int sw_rcl, sw_fcl;
    unsigned int fw_rcl, fw_fcl;

    srinfo.sr_data = sr_data_buf;
    srinfo.sr_hdr_data = sr_data_buf;

    while (!done) {
        if (chk_abort()) return BL_ABORTED;


        if ((ret = flash_get_srec_line()) != 0)
            return ret;

        if ((ret = decode_srec_line (&sr_buf[rec_i], &srinfo)) != 0)
            return ret;


#ifdef VERBOSE

        processed = (((flash_offset-SPI_FLASH_SOFTWARE_ADDR)+sr_buf_i)*100) / sw_header_info.info.data_len;
        if( (processed > displayed+(100/PR_BAR_ITEMS)) || (processed == 100) )
        {
          display_progress (processed);
          displayed = processed;
        }
#endif
        switch (srinfo.type) {
            case SREC_TYPE_0:
                sw_rcl = get_sw_rcl((char*)(srinfo.sr_hdr_data));
                sw_fcl = get_sw_fcl((char*)(srinfo.sr_hdr_data));
                fw_rcl = get_fw_rcl();
                fw_fcl = get_fw_fcl();
                /* xfs_local_printf("Header: %s\r\n", srinfo.sr_hdr_data); */
                if( fw_fcl!=sw_fcl || fw_rcl!=sw_rcl )
                {
                  xfs_local_printf("\r\nFW-SW Compatibility Level Missmatch!!!\r\n");
                  xfs_local_printf("FW Comp. Level Bitfile:  0x%02X\r\n", fw_fcl);
                  xfs_local_printf("FW Comp. Level Software: 0x%02X\r\n", sw_fcl);
                  xfs_local_printf("Register Layout Comp. Level Bitfile:  0x%04X\r\n", fw_rcl);
                  xfs_local_printf("Register Layout Comp. Level Software: 0x%04X\r\n\r\n", sw_rcl);
                  return SREC_FW_COMP_ERROR;
                }
                break;
            case SREC_TYPE_1:
            case SREC_TYPE_2:
            case SREC_TYPE_3:
                ncpy ((void*)srinfo.addr, (void*)srinfo.sr_data, srinfo.dlen);
                break;
            case SREC_TYPE_5:
                break;
            case SREC_TYPE_7:
            case SREC_TYPE_8:
            case SREC_TYPE_9:
                laddr = (void (*)())srinfo.addr;
                done = 1;
                ret = 0;
                break;
        }
    }

#ifdef VERBOSE
    xfs_local_printf("\r\n\r\nProcessed %d ", srec_line);
    xfs_local_printf(" S-records\r\nExecuting program starting at address: 0x%08X\r\n", (xfs_u32)laddr);
    xfs_local_printf("\r\n\r\n\r\n\r\n");
#endif

    (*laddr)();

    /* We will be dead at this point */
    return 0;
}

/******************************************************************************/

static xfs_u8 flash_get_srec_line(void)
{
  if( (flash_offset == SPI_FLASH_SOFTWARE_ADDR) && (sr_buf_i == 0) )
  {
    /* if this is the first call, read data first */
    spi_flash_read(SYSPTR(spi_flash), sr_buf, flash_offset, SREC_MAX_BYTES);
  }

  rec_i = sr_buf_i;

  while(1)
  {
    if(sr_buf_i >= SREC_MAX_BYTES-1)
    {
      if(rec_i == 0) return LD_SREC_LINE_ERROR;
      flash_offset += rec_i;
      spi_flash_read(SYSPTR(spi_flash), sr_buf, flash_offset, SREC_MAX_BYTES);
      rec_i = 0;
      sr_buf_i = 0;
    }

    /* Eat up line ends: \r\n (Windows) or \n (Unix) */
    if(sr_buf[sr_buf_i] == 0xD) /* Eat up the 0xD (\r) */
    {
      sr_buf_i++;
    }
    if(sr_buf[sr_buf_i] == 0xA) /* Eat up the 0xA (\n) */
    {
      sr_buf_i++;
      return 0;
    }

    sr_buf_i++;
  }
}

/******************************************************************************/

#ifdef __PPC__

#include <unistd.h>

/* Save some code and data space on PowerPC
   by defining a minimal exit */
void exit (int ret)
{
    _exit (ret);
}
#endif
