#!/bin/sh

meld wd2_xps_hw/system_template.bmm   wd2_xps_hw/implementation/system_bd.bmm
#meld wd2_xps_hw/system_template.bmm   wd2_xps_hw/implementation/system.bmm
#meld wd2_xps_hw/system_template.bmm   wd2_xps_hw/implementation/system_stub.bmm

cp wd2_xps_hw/implementation/system_bd.bmm wd2_xps_hw/SDK/SDK_Export/hw/system_bd.bmm
cp wd2_xps_hw/implementation/system_bd.bmm wd2_xsdk_workspace/wd2_xps_hw_platform/system_bd.bmm

