/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_master_v1_00_a/src/plb_spi_master.h
* Version:           1.00.a
* Description:       plb_spi_master Driver Header File
* Date:              Mon Jul 21 12:53:12 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef __PLB_SPI_MASTER_H__
#define __PLB_SPI_MASTER_H__

/***************************** Include Files *******************************/

#include "xfs_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/

/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 * -- SLV_REG2 : user logic slave module register 2
 * -- SLV_REG3 : user logic slave module register 3
 * -- SLV_REG4 : user logic slave module register 4
 * -- SLV_REG5 : user logic slave module register 5
 */

#define PLB_SPI_MASTER_CONFIG_REG_OFFSET     0x00000000
#define PLB_SPI_MASTER_NUMDBITS_REG_OFFSET   0x00000004
#define PLB_SPI_MASTER_STATUS_REG_OFFSET     0x00000008
#define PLB_SPI_MASTER_TXD_REG_OFFSET        0x0000000C
#define PLB_SPI_MASTER_RXD_REG_OFFSET        0x00000010
#define PLB_SPI_MASTER_SLVSEL_REG_OFFSET     0x00000014

#define PLB_SPI_MASTER_CONFIG_LOOP           0x00000001
#define PLB_SPI_MASTER_CONFIG_ENABLE         0x00000002
#define PLB_SPI_MASTER_CONFIG_CPOL           0x00000004
#define PLB_SPI_MASTER_CONFIG_CPHA           0x00000008
#define PLB_SPI_MASTER_CONFIG_TXFIFO_RST     0x00000010
#define PLB_SPI_MASTER_CONFIG_RXFIFO_RST     0x00000020
#define PLB_SPI_MASTER_CONFIG_MANUAL_SLV_SEL 0x00000040
#define PLB_SPI_MASTER_CONFIG_LSB_FIRST      0x00000080
#define PLB_SPI_MASTER_CONFIG_SW_RESET       0x00000100

#define PLB_SPI_MASTER_STATUS_RXFIFO_EMPTY   0x00000001
#define PLB_SPI_MASTER_STATUS_RXFIFO_AEMPTY  0x00000002
#define PLB_SPI_MASTER_STATUS_TXFIFO_FULL    0x00000004
#define PLB_SPI_MASTER_STATUS_TXFIFO_AFULL   0x00000008

#define PLB_SPI_MASTER_SLVSEL_NONE           0xFFFFFFFF

#define PLB_SPI_MASTER

#define TRUE   1
#define FALSE  0

/**************************** Type Definitions *****************************/

/**
 * This typedef contains configuration information for one slave device.
 */
typedef struct {
  unsigned int   slave_select;      /**< Pattern for slave select register */
  unsigned char  slave_data_width;  /**< Data transfer Width */
  unsigned char  slave_cpol;        /**< CPOL (SCK polarity) */
  unsigned char  slave_cpha;        /**< CPHA (SCK phase) */
  unsigned char  slave_lsb_first;   /**< CPHA (SCK phase) */
} plb_spi_salve_settings;

/**
 * The XSpi driver instance data. The user is required to allocate a
 * variable of this type for every SPI device in the system. A pointer
 * to a variable of this type is then passed to the driver API functions.
 */
typedef struct {
  unsigned int  base_address;         /**< Base address of device (IPIF) */
  int           is_started;           /**< Device has been started */
  unsigned char num_slaves;           /**< Number of slave selects for this device */
  unsigned char data_width;           /**< Data Transfer Width 8 or 16 or 24 or 32 */
  unsigned int  slave_select_mask;    /**< Mask that matches the number of SS bits */
  unsigned int  slave_select_reg_ptr; /**< Slave select register */
  unsigned int  *send_buffer_ptr;     /**< Buffer to send  */
  unsigned int  *recv_buffer_ptr;     /**< Buffer to receive */
} plb_spi_master;

/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_SPI_MASTER register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_MASTER device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 *   void mspi_mWriteReg(xfs_u32 BaseAddress, unsigned RegOffset, xfs_u32 Data)
 *
 */
#define mspi_mWriteReg(BaseAddress, RegOffset, Data) \
   Xil_Out32((BaseAddress) + (RegOffset), (xfs_u32)(Data))

/**
 *
 * Read a value from a PLB_SPI_MASTER register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_MASTER device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 *   xfs_u32 mspi_mReadReg(xfs_u32 BaseAddress, unsigned RegOffset)
 *
 */
#define mspi_mReadReg(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_SPI_MASTER user logic slave registers.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_MASTER device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 *   void mspi_mWriteSlaveRegn(xfs_u32 BaseAddress, unsigned RegOffset, xfs_u32 Value)
 *   xfs_u32 mspi_mReadSlaveRegn(xfs_u32 BaseAddress, unsigned RegOffset)
 *
 */
#define mspi_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_MASTER_CONFIG_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define mspi_mWriteSlaveReg1(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_MASTER_NUMDBITS_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define mspi_mWriteSlaveReg2(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_MASTER_STATUS_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define mspi_mWriteSlaveReg3(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_MASTER_TXD_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define mspi_mWriteSlaveReg4(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_MASTER_RXD_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define mspi_mWriteSlaveReg5(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_MASTER_SLVSEL_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))

#define mspi_mReadSlaveReg0(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_MASTER_CONFIG_REG_OFFSET) + (RegOffset))
#define mspi_mReadSlaveReg1(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_MASTER_NUMDBITS_REG_OFFSET) + (RegOffset))
#define mspi_mReadSlaveReg2(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_MASTER_STATUS_REG_OFFSET) + (RegOffset))
#define mspi_mReadSlaveReg3(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_MASTER_TXD_REG_OFFSET) + (RegOffset))
#define mspi_mReadSlaveReg4(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_MASTER_RXD_REG_OFFSET) + (RegOffset))
#define mspi_mReadSlaveReg5(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_MASTER_SLVSEL_REG_OFFSET) + (RegOffset))

/************************** Function Prototypes ****************************/
int  mspi_init(plb_spi_master *instance_ptr, unsigned int base_address, unsigned char num_slaves);
int mspi_slv_init(plb_spi_salve_settings *slave_setting_ptr, unsigned int slave_select, unsigned char slave_data_width, unsigned char slave_cpol, unsigned char slave_cpha, unsigned char slave_lsb_first);
int  mspi_send(plb_spi_master *instance_ptr, plb_spi_salve_settings *slave_setting_ptr, unsigned int *tx_buffer_ptr, unsigned int num_words);
void mspi_transceive(plb_spi_master *instance_ptr, plb_spi_salve_settings *slave_setting_ptr, unsigned int *tx_buffer_ptr, unsigned int *rx_buffer_ptr, unsigned int num_words);
void mspi_configure_for_slave(plb_spi_master *instance_ptr, plb_spi_salve_settings *slave_setting_ptr);
int  mspi_tx_fifo_is_full(plb_spi_master *instance_ptr);
int  mspi_tx_fifo_is_almost_full(plb_spi_master *instance_ptr);
int  mspi_rx_fifo_is_empty(plb_spi_master *instance_ptr);
int  mspi_rx_fifo_is_almost_empty(plb_spi_master *instance_ptr);
int mspi_get_status(plb_spi_master *instance_ptr);
void mspi_set_loopback(plb_spi_master *instance_ptr, int loopback_enable);
void mspi_reset(plb_spi_master *instance_ptr);
void mspi_reset_rxfifo(plb_spi_master *instance_ptr);
void mspi_reset_txfifo(plb_spi_master *instance_ptr);

#endif /** __PLB_SPI_MASTER_H__ */
