//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  21.05.2014 08:56:26
//
//  Description :  Module for controling and testing the external SRAM.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_ext_memory.h"
#include "memtest.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>
#include "xparameters.h"
#include "system.h"

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry ext_mem_cmd_table[] =
  {
    {"extram", ext_mem_help},
    {"extramtest", ext_mem_test},
    {0, general_ext_mem_help_func}
  };

/************************************************************/

  int ext_mem_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rExternal memory test and control commands:\n\r");
      xil_printf("extramtest:    tests the external RAM (write->read->compare)\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int ext_mem_test(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: extramtest [mode]\n\r\n\r");
      xil_printf("       mode:   defines the bit width of the memory access.\n\r");
      xil_printf("               Available modes are 8, 16 and 32 bits.\n\r");
      xil_printf("               If no mode is specified, all modes are tested sequentually.\n\r");
      xil_printf("\n\rDescription: Tests the external SRAM in a full write, read and compare sequence.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        memtest(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
      }
      else
      {
        if(fstrcmp(argv[1],"8"))
        {
          xil_printf("Testing RAM with 8bit access...\n\r");
          memtest_8bit(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
        }
        else if(fstrcmp(argv[1],"16"))
        {
          xil_printf("Testing RAM with 16bit access...\n\r");
          memtest_16bit(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
        }
        else if(fstrcmp(argv[1],"32"))
        {
          xil_printf("Testing RAM with 32bit access...\n\r");
          memtest_32bit(1, XPAR_EXTERNAL_MEMORY_MEM0_BASEADDR, MEM_SIZE_SRAM);
        }
        else
        {
          xil_printf("Invalid arguments. Type \"extramtest help\" for help.\n\r");
          return 0;
        }
      }
    }

    return 0;
  }

/************************************************************/

  int general_ext_mem_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* External RAM Test and Control Module:\n\rCan be used to control and test the external SRAM.\n\r");
    xil_printf("Type \"extram help\" for help on external SRAM commands.\n\r");
    return 0;
  }

/************************************************************/
