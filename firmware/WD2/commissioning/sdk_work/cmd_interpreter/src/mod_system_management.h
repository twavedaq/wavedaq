//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  22.05.2014 13:31:06
//
//  Description :  Module for configuring doing system management and housekeeping.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_SYSTEM_MANAGEMENT_H__
#define __MOD_SYSTEM_MANAGEMENT_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

  extern cmd_table_entry system_management_cmd_table[];

  int sm_help(int arc, char **argv);
  int sm_reset(int arc, char **argv);
  int sm_set_cal_buf(int arc, char **argv);
  int sm_set_cal_clk(int arc, char **argv);
  int sm_set_cal_osc(int arc, char **argv);
  int sm_set_pwr_cmp(int arc, char **argv);
  int sm_set_clk_ext(int arc, char **argv);
  int sm_get_version(int arc, char **argv);
  int general_sm_help_func(int arc, char **argv);

#endif // __MOD_SYSTEM_MANAGEMENT_H__
