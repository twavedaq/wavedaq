//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  02.05.2014 13:24:35
//
//  Description :  Code to use with a simple 32 bit timer running continuously.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xtime.h"

/******************************************************************************/

void usleep(unsigned int usec)
{
  unsigned int tic;
  unsigned int tic_to_wait;

  tic = time_tic_read();

  tic_to_wait = (TIME_FREQUENCY/1000000) * usec;
  while ( (time_tic_read()-tic) < tic_to_wait);
}

/******************************************************************************/

void msleep(unsigned int msec)
{
  unsigned int i;
  for (i=0; i < msec; i++)
  {
    usleep(1000);
  }
}

/******************************************************************************/

//void sleep(unsigned int sec)
//{
//  int i;
//  for (i=0; i < 2*sec; i++)
//  {
//    msleep(500);
//    time_check_seconds();
//  }
//}

/******************************************************************************/
