//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  12.05.2014 12:53:42
//
//  Description :  Module for configuring the DRS4 chips.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_DRS4_CONTROL_H__
#define __MOD_DRS4_CONTROL_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

  extern cmd_table_entry drs4_control_cmd_table[];

  int drs4_help(int arc, char **argv);
  int drs4_set_mode(int arc, char **argv);
  int drs4_set_register(int arc, char **argv);
  int drs4_get_pll_lock(int arc, char **argv);
  int general_drs4_control_help_func(int arc, char **argv);

#endif // __MOD_DRS4_CONTROL_H__
