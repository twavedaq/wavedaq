//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Processing the commands entered via the terminal command line
//                 using the modular command implementation.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include <new>

#include "cmd_processor.h"
//#include "gf_cmd_request.h"

//#include "stdio.h"
//#include "xtime.h"
//#include "xil_printf.h"
#include "xil_printf.h"

#include <stdlib.h>
#include "utilities.h"
//#include "system.h"

//#include "cpld_sr_ctrl.h"
//#include "dimax_head_ctrl.h"
//#include "spi_cfg_fctrl.h"
//#include "gf_cmd.h"
//#include "gf_dbg.h"
//#include "xtime.h"

//#include "memtest.h"
//#include "udp_terminal.h"
//#include "fw_env.h"

#include "wd2_config.h"

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/


/******************************************************************************/
/* function definitions                                                       */
/******************************************************************************/

int cmd_process(char *buffer_i,unsigned int len)
{
  char *argv[MAX_ARG_COUNT];
  unsigned int argc, buffer_ptr;
  char none = 0;
  int sqoute = 0;
  int dqoute = 0;
  int last_char_was_whitespace = 1;
  int cmd_table_index;
  int cmd_list_index;
  cmd_table_entry *cmd_table_ptr;

  // initialize cmd
  for (unsigned int i = 0; i < MAX_ARG_COUNT; i++)
    argv[i] = &none;

  for (buffer_ptr = 0; buffer_ptr < len; buffer_ptr++)
  {
    if ((buffer_i[buffer_ptr] == 0xa) || (buffer_i[buffer_ptr] == 0xd))
      buffer_i[buffer_ptr] = 0;
  }

  argc = 0;
  buffer_ptr = 0;

  while( buffer_i[buffer_ptr] && buffer_ptr<len)
  {
//     xil_printf("buffer_i[%d]=%c\n\r",buffer_ptr,buffer_i[buffer_ptr]);
    if       (buffer_i[buffer_ptr]=='\'') sqoute = ~sqoute;
    else if  (buffer_i[buffer_ptr]=='"')  dqoute = ~dqoute;

    if ( ((buffer_i[buffer_ptr]==0x09) || (buffer_i[buffer_ptr]==0x20)) && (!sqoute && !dqoute) )
    {  // unquoted whitespace
      buffer_i[buffer_ptr] = 0x00;
      last_char_was_whitespace = 1;
    }
    else // no whitespace or within quotes
    {
      if(last_char_was_whitespace)
      {
        if(argc < MAX_ARG_COUNT)
        {
          argv[argc++] = &buffer_i[buffer_ptr];
      }
        last_char_was_whitespace = 0;
      }
    }
    buffer_ptr++;
  }

  cmd_list_index = 0;
  while(cmd_list[cmd_list_index] != NULL)
  {
    cmd_table_ptr = cmd_list[cmd_list_index];
    cmd_table_index = 0;
    while(cmd_table_ptr[cmd_table_index].cmd_name != NULL)
    {
      if( fstrcmp(argv[0], cmd_table_ptr[cmd_table_index].cmd_name) )
      {
        cmd_table_ptr[cmd_table_index].cmd_func_ptr(argc, argv);
        return 0;
      }
      cmd_table_index++;
    }
    cmd_list_index++;
  }
  xil_printf("Unknown Command !!!\n\r");

  return 0;
}

/******************************************************************************/
/******************************************************************************/
