//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  24.04.2014 11:50:06
//
//  Description :  Example module for a command line interpreter test application
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MODULE1_H__
#define __MODULE1_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

#define INCLUDE_MOD1_FUNC_3   0

  extern cmd_table_entry module_1_cmd_table[];

  int mod1func1(int arc, char **argv);
  int mod1func2(int arc, char **argv);

#if INCLUDE_MOD1_FUNC_3
  int mod1func3(int arc, char **argv);
#endif

  int general_module1_help_func(int arc, char **argv);

#endif // __MODULE1_H__
