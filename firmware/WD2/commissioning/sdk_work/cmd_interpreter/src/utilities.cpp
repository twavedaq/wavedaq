//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  02.05.2014 13:24:35
//
//  Description :  Utilities such as string operation functions.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "utilities.h"
#include "xil_printf.h"

/******************************************************************************/

void ncpy_c(char *cp1, const char *cp2, int len)
{
 int i;
 for (i=0; i<len; i++)
 {
   cp1[i] = cp2[i];
 }
}

/******************************************************************************/

int ncmp(const char *cp1, const char *cp2, int len)
{
 int i;

 for (i=0; i<len; i++)
 {
   if (cp1[i] != cp2[i]) return 0;
 }

 return 1;
}

/******************************************************************************/

int fstrcmp(const char *cp1, const char *cp2)
{
  int i=0;

  while(1)
  {
     if(cp1[i] != cp2[i]) return 0;
     if(cp1[i] == 0) return 1;
     i++;
  }
}

/******************************************************************************/

void print_frame(unsigned char* fbuff, int len)
{
  int i;

  xil_printf("\n\r----Frame of len : %d Byte\n\r",len );
  for(i=0;i<len;i++)
  {
    xil_printf("0x%02x ",fbuff[i] );
    if ((i&0xf) == 0x7) xil_printf(" ");
    if ((i&0xf) == 0xf) xil_printf("\n\r");
  }
  xil_printf("\n\r");
}

/******************************************************************************/

int hex_val(char c)
{
  if (c >= '0' &&  c <= '9')
  {
    return (c - '0');
  }
  else if (c >= 'a' &&  c <= 'f')
  {
    return (c - 'a' + 10);
  }
  else if (c >= 'A' &&  c <= 'F')
  {
    return (c - 'A' + 10);
  }
  else
  {
    return -1;
  }
}

/******************************************************************************/

int hatoi(char *str)
{
  int val = 0;
  int digit;
  
  while(*str)
  {
    digit = hex_val(*str++);
    if (digit<0) return 0;
    val = val*16 + digit;
  }
  return val;
}

/******************************************************************************/
