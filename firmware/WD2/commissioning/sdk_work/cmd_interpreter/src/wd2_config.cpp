//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  24.04.2014 11:53:05
//
//  Description :  Structure of table to list names of implemented commands that can be
//                 interpreted plus a pointer to the function to be called by this
//                 command.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "wd2_config.h"

#if CFG_INCLUDE_HELP
#include "mod_help.h"
#endif
#if CFG_INCLUDE_SPI_REG_LOOP
#include "mod_spi_reg_loop.h"
#endif
#if CFG_INCLUDE_TEMP
#include "mod_temp.h"
#endif
#if CFG_INCLUDE_LMK
#include "mod_lmk.h"
#endif
#if CFG_INCLUDE_DAC
#include "mod_dac.h"
#endif
#if CFG_INCLUDE_FRONTEND_SREG
#include "mod_frontend_shiftreg.h"
#endif
#if CFG_INCLUDE_DRS4_CONTROL
#include "mod_drs4_control.h"
#endif
#if CFG_INCLUDE_LED_CONTROL
#include "mod_led.h"
#endif
#if CFG_INCLUDE_EXT_MEMORY
#include "mod_ext_memory.h"
#endif
#if CFG_INCLUDE_SYSTEM_MANAGEMENT
#include "mod_system_management.h"
#endif
#if CFG_INCLUDE_SERDES
#include "mod_serdes.h"
#endif
#if CFG_INCLUDE_HV
#include "mod_hv.h"
#endif
#if CFG_INCLUDE_MODULE_1
#include "module1.h"
#endif
#if CFG_INCLUDE_MODULE_2
#include "module2.h"
#endif
#if CFG_INCLUDE_CREDITS
#include "mod_credits.h"
#endif

  // Main command table collecting sub command
  // tables of modules
  cmd_table_entry *cmd_list[] =
  {
#if CFG_INCLUDE_HELP
    help_cmd_table,
#endif
#if CFG_INCLUDE_SPI_REG_LOOP
    spi_reg_loop_cmd_table,
#endif
#if CFG_INCLUDE_TEMP
    temp_cmd_table,
#endif
#if CFG_INCLUDE_DAC
    dac_cmd_table,
#endif
#if CFG_INCLUDE_LMK
    lmk_cmd_table,
#endif
#if CFG_INCLUDE_FRONTEND_SREG
    frontend_shiftreg_cmd_table,
#endif
#if CFG_INCLUDE_DRS4_CONTROL
    drs4_control_cmd_table,
#endif
#if CFG_INCLUDE_LED_CONTROL
    led_cmd_table,
#endif
#if CFG_INCLUDE_EXT_MEMORY
    ext_mem_cmd_table,
#endif
#if CFG_INCLUDE_SYSTEM_MANAGEMENT
    system_management_cmd_table,
#endif
#if CFG_INCLUDE_SERDES
    serdes_cmd_table,
#endif
#if CFG_INCLUDE_HV
    hv_cmd_table,
#endif
#if CFG_INCLUDE_MODULE_1
    module_1_cmd_table,
#endif
#if CFG_INCLUDE_MODULE_2
    module_2_cmd_table,
#endif
#if CFG_INCLUDE_CREDITS
    credits_cmd_table,
#endif
    0
  };

