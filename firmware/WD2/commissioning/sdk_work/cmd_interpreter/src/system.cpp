//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Central control for hardware access.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "system.h"
#include "xparameters.h"
#include "utilities.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "gpio_shift_register.h"
#include "gpio_drs4_control.h"
#include "gpio_slow_control.h"
#include "gpio_looptest.h"

#include "xil_printf.h"
//#include "gf_api.h"
//#include "gf_dbg.h"
//#include <stdlib.h>
 
//#ifndef LINUX_COMPILE
//#include "xtemac.h"
//#include "config.h"
//#include "fw_env.h"
//#endif

/******************************************************************************/
/* global vars                                                                */
/******************************************************************************/

// global pointer to hardware system
system_class *system_ptr;

//#ifndef PWR_CFG_ONLY  
//gf_status_type gf_status;
//gf_eye_cal_type gf_eye_cal[4];


//Buffer in SRAM
unsigned char *STATIC_READ_BUFF  = (unsigned char *)STATIC_RAM_ADDR_READ_BUFFER;
unsigned char *STATIC_WRITE_BUFF = (unsigned char *)STATIC_RAM_ADDR_WRITE_BUFFER;

//#endif /* PWR_CFG_ONLY */



//#ifndef LINUX_COMPILE
//// environment settings
//env_data_type env_data;
//#endif


/******************************************************************************/
/* class methods                                                              */
/******************************************************************************/

system_class::system_class(void)
{

  gpio_sys = new plb_gpio_class(XPAR_PLB_GPIO_0_BASEADDR);
//  cpld_sr_ctrl       = new cpld_sr_ctrl_class(XPAR_CPLD_SHIFT_REG_PPC440_BASEADDR);
//  dimax_head         = new dimax_head_ctrl_class(XPAR_PLB_DIMAX_HEAD_CTRL_INST_BASEADDR);
//
//
//#ifndef PWR_CFG_ONLY  
//  spi_flash_available = 0;
//  nw_if_ready = 0;
//
//  xps_spi_controller = new xps_spi_ctrl_class(XPAR_XPS_SPI_CFG_BASEADDR, 8);
//  spi_flash          = new spi_flash_class(xps_spi_controller);
//  spi_mac_eeprom     = new spi_mac_eeprom_class(xps_spi_controller);
//  system_monitor     = new xsysmon_class(XPAR_XPS_SYSMON_ADC_PPC440_BASEADDR, XPAR_XPS_SYSMON_ADC_PPC440_INCLUDE_INTR);
//  gf_com_master      = new gf_com_master_class(XPAR_PLB_GF_COM_MASTER_CTRL_BASEADDR);
//#endif /* PWR_CFG_ONLY */
//
//
//#ifndef LINUX_COMPILE
//  xil_ll_fifo_temac_ctrl_sfp = new xil_ll_fifo_class(XPAR_XPS_LL_FIFO_TEMAC_CTRL_SFP_BASEADDR);
//  xil_ll_fifo_temac_sync_sfp = new xil_ll_fifo_class(XPAR_XPS_LL_FIFO_TEMAC_SYNC_SFP_BASEADDR);
//
//
//  fctrl_ctrl_sfp = new sfp_ctrl_class(gpio_sys, GPIO_CTRL_SFP_REG,
//                                    GPIO_CTRL_SFP_IIC_SDA,  GPIO_CTRL_SFP_IIC_SCL,  GPIO_CTRL_SFP_TX_DISABLE,
//                                    GPIO_CTRL_SFP_RATE_SEL, GPIO_CTRL_SFP_LOS,      GPIO_CTRL_SFP_TX_FAULT,
//                                    GPIO_CTRL_SFP_MOD_DEF0, GPIO_CTRL_SFP_LINK_UP,  GPIO_CTRL_SFP_ERROR);
//
//  fctrl_sync_sfp = new sfp_ctrl_class(gpio_sys, GPIO_SYNC_SFP_REG,
//                                    GPIO_SYNC_SFP_IIC_SDA,  GPIO_SYNC_SFP_IIC_SCL,  GPIO_SYNC_SFP_TX_DISABLE,
//                                    GPIO_SYNC_SFP_RATE_SEL, GPIO_SYNC_SFP_LOS,      GPIO_SYNC_SFP_TX_FAULT,
//                                    GPIO_SYNC_SFP_MOD_DEF0, GPIO_SYNC_SFP_LINK_UP,  GPIO_SYNC_SFP_ERROR);
//
//  xtemac_ctrl = new xtemac_class(XPAR_TEMAC_CTRL_SFP_CHAN_0_DEVICE_ID, XPAR_TEMAC_CTRL_SFP_CHAN_0_BASEADDR, TEMAC_MDIO_ADDR_CTRL_SFP, fctrl_ctrl_sfp);
//  xtemac_sync = new xtemac_class(XPAR_TEMAC_SYNC_SFP_CHAN_0_DEVICE_ID, XPAR_TEMAC_SYNC_SFP_CHAN_0_BASEADDR, TEMAC_MDIO_ADDR_SYNC_SFP, fctrl_sync_sfp);
//
//  nw_if_ctrl  = new network_if_class(xil_ll_fifo_temac_ctrl_sfp, xtemac_ctrl, fctrl_ctrl_sfp);
//  nw_if_sync  = new network_if_class(xil_ll_fifo_temac_sync_sfp, xtemac_sync, fctrl_sync_sfp);
//#endif /* LINUX_COMPILE */
//
//
//#ifndef PWR_CFG_ONLY  
//  iic_temp      = new plb_iic_class(XPAR_PLB_I2C_CTRL_TEMP_BASEADDR, 400000, 1); // (addr, freq_hz, check_acknowledge)
//  tmp112_front  = new tmp112_class(TMP112_DEVICE_ADDRESS_0, iic_temp);
//  tmp112_sodimm = new tmp112_class(TMP112_DEVICE_ADDRESS_1, iic_temp);
//
//  max6650_fan_ctrl = new max6650_class(MAX6650_DEVICE_ADDRESS, iic_temp);
//#endif /* PWR_CFG_ONLY */

}

/******************************************************************************/

xfs_u32 system_class::init(void)
{
//  xfs_u32 status;
  xfs_u32 init_status = XFS_SUCCESS;
  soft_spi_temp_dac_lmk_init();
  gpio_sreg_init();
  gpio_drs4_cfg_init();
  gpio_sc_init();
  gpio_looptest_init();
  
  // init spi master
  spi_loop_master_ptr         = &spi_loop_master;
  spi_loop_slave_settings_ptr = &spi_loop_slave_settings;
  mspi_init(spi_loop_master_ptr, XPAR_PLB_SPI_MASTER_0_BASEADDR, 1);
  mspi_slv_init(spi_loop_slave_settings_ptr, 0xFFFFFFFE, 32, 0, 0, 0);
  // init spi slave
  spi_loop_slave_ptr         = &spi_loop_slave;
  sspi_init(spi_loop_slave_ptr, XPAR_PLB_SPI_SLAVE_0_BASEADDR);

//  // LED yellow during initialization
//  gpio_sys->set(GPIO_CTRL_BOARD_REG, GPIO_PWR_CTRL_KILL_O );
//  gpio_sys->clr(GPIO_CTRL_BOARD_REG, (GPIO_RUN_5_0V_O | GPIO_LED_BP_G_N_O | GPIO_LED_BP_R_N_O));
//  gpio_sys->tristate_write(GPIO_CTRL_BOARD_REG, ~(GPIO_PWR_CTRL_KILL_O | GPIO_RUN_5_0V_O | GPIO_LED_BP_G_N_O | GPIO_LED_BP_R_N_O));
//
//#ifndef PWR_CFG_ONLY  
//  status = system_monitor->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in system_monitor->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }
//    
//  status = iic_temp->init();  
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in iic_temp->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  status = max6650_fan_ctrl->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in max6650_fan_ctrl->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//
//  status = gf_com_master->init(DEFAULT_BIT_FREQ_CC);
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in gf_com_master->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  // default off
//  gpio_sys->clr(GPIO_PORT_GIGA_FROST_EAST, (GPIO_GIGA_PWR_EN_O));
//  gpio_sys->clr(GPIO_PORT_GIGA_FROST_WEST, (GPIO_GIGA_PWR_EN_O));
//  gpio_sys->set(GPIO_PORT_GIGA_FROST_EAST, (GPIO_GIGA_CFG_RST_O));
//  gpio_sys->set(GPIO_PORT_GIGA_FROST_WEST, (GPIO_GIGA_CFG_RST_O));
//  SYSTEM->gf_com_master->disable(GF_COM_TARGET_SW);
//  SYSTEM->gf_com_master->disable(GF_COM_TARGET_SE);
//  SYSTEM->gf_com_master->disable(GF_COM_TARGET_NW);
//  SYSTEM->gf_com_master->disable(GF_COM_TARGET_NE);
//
//  gpio_sys->tristate_write(GPIO_PORT_GIGA_FROST_EAST, ~(GPIO_GIGA_PWR_EN_O | GPIO_GIGA_CFG_RST_O));
//  gpio_sys->tristate_write(GPIO_PORT_GIGA_FROST_WEST, ~(GPIO_GIGA_PWR_EN_O | GPIO_GIGA_CFG_RST_O));
//
//
//  xps_spi_controller->init();
//  xps_spi_controller->store_select(SPI_SELECT_CTRL_FLASH);
//  spi_flash_available = spi_flash->spi_flash_available();
//#endif /* PWR_CFG_ONLY */
//
//#ifndef LINUX_COMPILE
//
//  status = xil_ll_fifo_temac_ctrl_sfp->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in xil_ll_fifo_temac_ctrl_sfp->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//    
//  status = xil_ll_fifo_temac_sync_sfp->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in xil_ll_fifo_temac_sync_sfp->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  status = fctrl_ctrl_sfp->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in fctrl_ctrl_sfp->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  status = fctrl_sync_sfp->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in fctrl_sync_sfp->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  status = xtemac_ctrl->init(env_data.eth0_mac_addr);
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in xtemac_ctrl->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  status = xtemac_sync->init(env_data.eth1_mac_addr);
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in xtemac_sync->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  nw_if_ctrl->set_name(env_data.hostname);
//  status = nw_if_ctrl->init(env_data.eth0_mac_addr, env_data.eth0_ip_addr, env_data.eth0_dhcp);
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in nw_if_ctrl->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//
//  nw_if_sync->set_name(env_data.eth1name);  
//  status = nw_if_sync->init(env_data.eth1_mac_addr, env_data.eth1_ip_addr, env_data.eth1_dhcp);
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in nw_if_sync->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//  
//  nw_if_ready = 1;  
//#endif
//
//#ifndef PWR_CFG_ONLY  
//  status = tmp112_front->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in tmp112_front->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//    
//  status = tmp112_sodimm->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in tmp112_sodimm->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//#endif /* PWR_CFG_ONLY */
//  
//  status = dimax_head->init();
//  if (status != XFS_SUCCESS)
//  {
//    if (DBG_ERR) xil_printf("Error in dimax_head->init()\n\r");
//    init_status = XFS_FAILURE;  
//  }  
//    
// 
  // if error:
  if (init_status != XFS_SUCCESS)
  {
//    // LED red
//    gpio_sys->set(GPIO_CTRL_BOARD_REG,  GPIO_LED_BP_G_N_O );
//    gpio_sys->clr(GPIO_CTRL_BOARD_REG,  GPIO_LED_BP_R_N_O );
//
//    if (DBG_ERR) xil_printf("Error in system_class::init\n\r");
    return XFS_FAILURE;
  }
  else
  {
//    // LED green 
//    gpio_sys->set(GPIO_CTRL_BOARD_REG,  GPIO_LED_BP_R_N_O );
//    gpio_sys->clr(GPIO_CTRL_BOARD_REG,  GPIO_LED_BP_G_N_O );
//    
    return XFS_SUCCESS;    
  }
  
    
}

/******************************************************************************/

//void system_class::bp_led_blink_yellow_usleep(unsigned int time)
//{
//    // Backpanel LED off
//    SYSTEM->gpio_sys->set(GPIO_CTRL_BOARD_REG, ( GPIO_LED_BP_G_N_O | GPIO_LED_BP_R_N_O));
//    usleep(time/2);
//    // Backpanel LED Yellow
//    SYSTEM->gpio_sys->clr(GPIO_CTRL_BOARD_REG, ( GPIO_LED_BP_G_N_O | GPIO_LED_BP_R_N_O));
//    usleep(time/2);  
//}
//
///******************************************************************************/
//#ifndef LINUX_COMPILE
//
///******************************************************************************/
//
//void system_class::default_env(void)
//{
//  bzero(&env_data, sizeof(env_data_type));
//
//  env_data.serial_no = 0;
//
//  ncpy(env_data.hostname, "frost-ctrl", 11);
//  ncpy(env_data.eth1name, "frost-sync", 11);
//
//  ncpy(env_data.eth0_mac_addr, eth0_default_mac_addr, 6);
//  ncpy(env_data.eth0_ip_addr,  eth0_default_ip_addr,  4);
//  env_data.eth0_dhcp = 0;
//
//  ncpy(env_data.eth1_mac_addr, eth1_default_mac_addr, 6);
//  ncpy(env_data.eth1_ip_addr,  eth1_default_ip_addr,  4);
//  env_data.eth1_dhcp = 0;
//  
//  
///******************************************************************************/
//  
////      env_data.eth0_ip_addr[2]      = env_data.serial_no;
////      env_data.eth0_mac_addr[5]     = (0x00 + (2*env_data.serial_no));
////      env_data.eth1_ip_addr[2]  = env_data.serial_no;
////      env_data.eth1_mac_addr[5] = (0x01 + (2*env_data.serial_no));
//}
//
///******************************************************************************/
//
//void system_class::fw_parse_env(void)
//{  
//  char *cp;
//  
//  unsigned char  prev_eth0_mac_addr[6];
//  unsigned char  prev_eth0_ip_addr[4];
//  unsigned int   prev_eth0_dhcp=0;
// 
//  unsigned char  prev_eth1_mac_addr[6];
//  unsigned char  prev_eth1_ip_addr[4];
//  unsigned int   prev_eth1_dhcp=0;
//
//  if (nw_if_ready)    
//  {
//    ncpy(prev_eth0_mac_addr, env_data.eth0_mac_addr, 6);
//    ncpy(prev_eth0_ip_addr,  env_data.eth0_ip_addr, 4);
//    prev_eth0_dhcp = env_data.eth0_dhcp;
//    
//    
//    ncpy(prev_eth1_mac_addr, env_data.eth1_mac_addr, 6);
//    ncpy(prev_eth1_ip_addr,  env_data.eth1_ip_addr, 4);
//    prev_eth1_dhcp = env_data.eth1_dhcp;
//  }
//  
//  
//  if ((cp = fw_getenv("sn")))
//  {
//    env_data.serial_no = atoi(cp);
//  }
//
//  if ((cp = fw_getenv("hostname")))
//  {
//    strncpy(env_data.hostname, cp, MAX_HOSTNAME_LENGTH);
//    env_data.hostname[MAX_HOSTNAME_LENGTH-1]=0;
//  }
//  
//  if ((cp = fw_getenv("ipaddr")))
//  {
//    parse_ip(cp, env_data.eth0_ip_addr);
//  }
//
//  if ((cp = fw_getenv("ethaddr")))
//  {
//    parse_mac(cp, env_data.eth0_mac_addr);
//  }
//
//  if ((cp = fw_getenv("ethdhcp")))
//  {
//    env_data.eth0_dhcp = atoi(cp);
//  }
//
//  if ((cp = fw_getenv("eth1name")))
//  {
//    strncpy(env_data.eth1name, cp, MAX_HOSTNAME_LENGTH);
//    env_data.eth1name[MAX_HOSTNAME_LENGTH-1]=0;
//  }
//  
//  if ((cp = fw_getenv("eth1addr")))
//  {
//    parse_mac(cp, env_data.eth1_mac_addr);
//  }
//
//  if ((cp = fw_getenv("ip1addr")))
//  {
//    parse_ip(cp, env_data.eth1_ip_addr);
//  }
//
//  if ((cp = fw_getenv("eth1dhcp")))
//  {
//    env_data.eth1_dhcp = atoi(cp);
//  }
//
//  if ((cp = fw_getenv("gf_bootdelay")))
//  {
//    env_data.auto_boot = atoi(cp);
//  }
//
//  if ((cp = fw_getenv("gf_pwronboot")))
//  {
//    env_data.pwr_on_boot = atoi(cp);
//  }
//
////  if ((cp = fw_getenv("auto_boot_part")))
////  {
////    env_data.auto_boot_part = atoi(cp);
////  }
//
//
//  if (nw_if_ready)    
//  {    
//    // set names
//    nw_if_ctrl->set_name(env_data.hostname);
//    nw_if_sync->set_name(env_data.eth1name);
//    
//    // check if network settings changed...
//
//    if ( (prev_eth0_dhcp != env_data.eth0_dhcp)                 ||
//         (!ncmp((char*)prev_eth0_mac_addr, (char*)env_data.eth0_mac_addr, 6)) || 
//         (!ncmp((char*)prev_eth0_ip_addr,  (char*)env_data.eth0_ip_addr, 4))  )
//    {
//      nw_if_ctrl->update_addr(env_data.eth0_mac_addr, env_data.eth0_ip_addr, env_data.eth0_dhcp);
//    }
//    
//    if ( (prev_eth1_dhcp != env_data.eth1_dhcp)                 ||
//         (!ncmp((char*)prev_eth1_mac_addr, (char*)env_data.eth1_mac_addr, 6)) || 
//         (!ncmp((char*)prev_eth1_ip_addr,  (char*)env_data.eth1_ip_addr, 4))  )
//    {
//      nw_if_sync->update_addr(env_data.eth1_mac_addr, env_data.eth1_ip_addr, env_data.eth1_dhcp);
//    }
//  }
//
//}
//
///******************************************************************************/
//
//xfs_u32 system_class::init_env(void)
//{
//// do initializations do
//  xps_spi_controller->init();
//  xps_spi_controller->store_select(SPI_SELECT_CTRL_FLASH);
//
//  spi_flash_available = spi_flash->spi_flash_available();
//
//  default_env();
//  fw_parse_env();
//  
//  return XFS_SUCCESS;
//}
//
//
///******************************************************************************/
//
//#endif  /* LINUX_COMPILE */
//
///******************************************************************************/


system_class::~system_class(void)
{
  delete gpio_sys;
//  delete cpld_sr_ctrl;
//
//
//#ifndef PWR_CFG_ONLY  
//  delete system_monitor;
//  delete spi_flash;
//
//
//  delete gf_com_master;
//
//
//  delete tmp112_front;
//  delete tmp112_sodimm;
//  delete iic_temp;
//#endif
//
//#ifndef LINUX_COMPILE
//  delete nw_if_ctrl;
//  delete nw_if_sync;
//
//  delete xtemac_ctrl;
//  delete xtemac_sync;
//
//  delete fctrl_ctrl_sfp;
//  delete fctrl_sync_sfp;
//
//  delete xil_ll_fifo_temac_ctrl_sfp;
//  delete xil_ll_fifo_temac_sync_sfp;
//#endif

}

/******************************************************************************/

