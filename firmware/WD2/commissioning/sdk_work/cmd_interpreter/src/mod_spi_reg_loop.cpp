//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  30.07.2014 13:32:36
//
//  Description :  Module for SPI access to LTC2600 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_spi_reg_loop.h"
#include "plb_spi_master.h"
#include "plb_spi_slave.h"
#include "xil_printf.h"
#include "system.h"
#include "utilities.h"
#include <stdlib.h>
#include "wd2_dbg.h"

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry spi_reg_loop_cmd_table[] =
  {
    {"slp", spi_reg_loop_help},
    {"slpstatus", spi_reg_get_status},
    {"slptrx", spi_reg_transceive},
    {"slploopback", spi_reg_loop_cfg},
    {"slptestm", spi_reg_loop_master_test},
    {"slptestms", spi_reg_loop_master_slave_test},
    {0, general_spi_reg_loop_help_func}
  };

/************************************************************/

  int spi_reg_loop_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rSPI register loop commands:\n\r");
      xil_printf("slpstatus:    print status register content\n\r");
      xil_printf("slptrx:       transmit/receive binary commands\n\r");
      xil_printf("slploopback:  configure internal spi loopback\n\r");
      xil_printf("slptestm:     loopback test at master only\n\r");
      xil_printf("slptestms:    master transmission test\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int spi_reg_get_status(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: slpstatus\n\r\n\r");
      xil_printf("\n\rDescription: prints the contet of the status register (hex).\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Master status Register: 0x%08X\n\r", mspi_get_status(system_ptr->spi_loop_master_ptr));
      xil_printf("Slave  status Register: 0x%08X\n\r", sspi_get_status(system_ptr->spi_loop_slave_ptr));
    }
    return 0;
  }

/************************************************************/

  int spi_reg_transceive(int arc, char **argv)
  {
    unsigned int tx_word, rx_word;
    char *bin_data_ptr;
    unsigned int  count;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: slptrx <payload>\n\r\n\r");
      xil_printf("       payload:   data to transmit as hex value (max. xxx bytes).\n\r");
      xil_printf("\n\rDescription: sends the specified payload as binary data via SPI.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      bin_data_ptr = argv[1];
      while(*bin_data_ptr != '\0')
      {
        count = 0;
        tx_word = 0;
        while( (*bin_data_ptr != '\0') && (count < 8) )
        {
          tx_word = tx_word*16 + (unsigned int)(hex_val(*bin_data_ptr));
          bin_data_ptr++;
          count++;
        }
        while(count < 8)
        {
          tx_word = tx_word*16;
          count++;
        }
        //xil_printf("%08X ", tx_word);
        mspi_transceive(system_ptr->spi_loop_master_ptr, system_ptr->spi_loop_slave_settings_ptr, &tx_word, &rx_word, 1);
      }
      //xil_printf("\n\rend of binary data string\n\r\n\r");
    }
    return 1;
  }

/************************************************************/

  int spi_reg_loop_cfg(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: slploopback <loopback enable>\n\r\n\r");
      xil_printf("       loopback enable:   enable or disable loopback.\n\r");
      xil_printf("\n\rDescription: controls the firmware loopback of the spi master device.\n\r");
      xil_printf("             The loopback can be used to test the spi master.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"enable"))
      {
        mspi_set_loopback(system_ptr->spi_loop_master_ptr, 1);
      }
      else if(fstrcmp(argv[1],"disable"))
      {
        mspi_set_loopback(system_ptr->spi_loop_master_ptr, 0);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"slploopback help\" for help.\n\r");
        return 0;
      }
    }
    return 1;
  }

/************************************************************/

  int spi_reg_loop_master_test(int arc, char **argv)
  {
    int i, j;
    unsigned int tx_value, tx_word, rx_word;
    unsigned int errors = 0;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: slptestm\n\r\n\r");
      xil_printf("\n\rDescription: sends test patterns to spi link, reads the recieved data,\n\r");
      xil_printf("             compares it to the patterns and reports if they match.\n\r");
      xil_printf("             The loopback is implemented from master tx to master rx shift register.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("\n\rSPI Master Loopback Test:\n\r");
      xil_printf("\n\rConfiguring Loopback\n\r");
      mspi_set_loopback(system_ptr->spi_loop_master_ptr, 1);
      xil_printf("Performing Test\n\r");
      for(i=0;i<2;i++)
      {
        tx_value = 0x00000001;
        for(j=0;j<32;j++)
        {
          if(i)
          {
            tx_word = ~tx_value;
          }
          else
          {
            tx_word = tx_value;
          }
          mspi_transceive(system_ptr->spi_loop_master_ptr, system_ptr->spi_loop_slave_settings_ptr, &tx_word, &rx_word, 1);
          if(rx_word != tx_word)
          {
            errors++;
            if(DBG_ERR)  xil_printf("Error in loop %d! Sent: 0x%08X   Received: 0x%08X\n\r", (i*32)+j, tx_word, rx_word);
          }
          tx_value = tx_value << 1;
        }
      }
      xil_printf("Resetting Loopback\n\r");
      mspi_set_loopback(system_ptr->spi_loop_master_ptr, 0);

      if(errors)
      {
        xil_printf("\n\rSPI Register Looptest failed (%d errors).\n\r\n\r", errors);
      }
      else
      {
        xil_printf("\n\rSPI Register Looptest successful.\n\r\n\r");
        return 0;
      }
    }

    return 1;
  }

/************************************************************/

  int spi_reg_loop_master_slave_test(int arc, char **argv)
  {
    int i, j;
    unsigned int tx_value, tx_word, rx_word;
    unsigned int errors = 0;
    int rx_report = 0;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: slptestms <debug level>\n\r\n\r");
      xil_printf("       debug level:   level of detail of the text output (range = 0,1).\n\r");
      xil_printf("\n\rDescription: sends test patterns to the master spi link, reads the recieved data\n\r");
      xil_printf("             at the slave spi link, compares it to the patterns and reports if they match.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("\n\rMaster Slave SPI Communication Test:\n\r");
      xil_printf("Performing Test...\n\r");
      for(i=0;i<2;i++)
      {
        tx_value = 0x00000001;
        for(j=0;j<32;j++)
        {
          if(i)
          {
            tx_word = ~tx_value;
          }
          else
          {
            tx_word = tx_value;
          }
          mspi_send(system_ptr->spi_loop_master_ptr, system_ptr->spi_loop_slave_settings_ptr, &tx_word, 1);
          rx_report = sspi_receive(system_ptr->spi_loop_slave_ptr, &rx_word, 1, 1000000);
          if(rx_report < 1)
          {
            errors++;
            if(DBG_ERR)  xil_printf("Error in transmission %d! Slave receiver timeout\n\r", (i*32)+j);
          }
          else if(rx_word != tx_word)
          {
            errors++;
            if(DBG_ERR)  xil_printf("Error in transmission %d! Sent: 0x%08X   Received: 0x%08X\n\r", (i*32)+j, tx_word, rx_word);
          }
          tx_value = tx_value << 1;
        }
      }

      if(errors)
      {
        xil_printf("\n\rSPI Register Master Slave Test failed (%d errors).\n\r\n\r", errors);
      }
      else
      {
        xil_printf("\n\rSPI Register Master Slave Test successful.\n\r\n\r");
        return 0;
      }
    }

    return 1;
  }

/************************************************************/

  int general_spi_reg_loop_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* SPI Register Loop Module:\n\r");
    xil_printf("Allows to use an SPI loop for transmitting and receiving\n\r");
    xil_printf("binary commands that are used on the SPI channel between\n\r");
    xil_printf("DCB and WaveDream2 in the final system.\n\r");
    xil_printf("Type \"slp help\" for help on SPI register loop commands.\n\r");
    return 0;
  }

/************************************************************/
