//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 12:23:47
//
//  Description :  Software controlled shift register.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xparameters.h"
#include "system.h"
#include "xil_printf.h"
#include "xtime.h"
#include "gpio_shift_register.h"

/******************************************************************************/

/************************************************************/
//Shift register variables
/************************************************************/
//Default frontend settings for all 16 channels
//   Bit layout:
//   D7    D6    D5    D4    D3    D2    D1    D0
//   ACDC  COMP2 OP2   COMP1 OP1   RSRVD CAL1  CAL0
//   ^MSB                                      ^LSB
unsigned char  FRONTEND_SETTINGS[16] = {0x02,0x02,0x02,0x02,
                                        0x02,0x02,0x02,0x02,
                                        0x02,0x02,0x02,0x02,
                                        0x02,0x02,0x02,0x02};

/************************************************************/

void gpio_sreg_init(void)
{
  SYSTEM->gpio_sys->clr(GPIO_SOFT_SHIFT_REGISTER, GPIO_SREG_DI25_O | GPIO_SREG_ST25_O | GPIO_SREG_CLK25_O);
  SYSTEM->gpio_sys->clr(GPIO_SOFT_SHIFT_REGISTER, GPIO_SREG_DI33_O | GPIO_SREG_ST33_O | GPIO_SREG_CLK33_O);

  //Load default value to shift registers
  frontend_setting_apply(); 
}

/******************************************************************************/

void gpio_sreg_clk(unsigned int reg_sel, unsigned int val)
{
  unsigned char sregclk;

  if (reg_sel == SREG25)
  {
    sregclk = GPIO_SREG_CLK25_O;
  }
  else
  {
    sregclk = GPIO_SREG_CLK33_O;
  }

  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SHIFT_REGISTER, sregclk );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SHIFT_REGISTER, sregclk );
  }
}

/******************************************************************************/

void gpio_sreg_store(unsigned int reg_sel, unsigned int val)
{
  unsigned char sreg_st;

  if (reg_sel == SREG25)
  {
    sreg_st = GPIO_SREG_ST25_O;
  }
  else
  {
    sreg_st = GPIO_SREG_ST33_O;
  }

  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SHIFT_REGISTER, sreg_st );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SHIFT_REGISTER, sreg_st );
  }
}

/******************************************************************************/

void gpio_sreg_di(unsigned int reg_sel, unsigned int val)
{
  unsigned char sreg_di;

  if (reg_sel == SREG25)
  {
    sreg_di = GPIO_SREG_DI25_O;
  }
  else
  {
    sreg_di = GPIO_SREG_DI33_O;
  }

  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SHIFT_REGISTER, sreg_di );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SHIFT_REGISTER, sreg_di );
  }
}

/******************************************************************************/

void gpio_sreg_wait()
{
  usleep(10);
}

/******************************************************************************/

void gpio_sreg_transmit(unsigned int reg_sel, unsigned char* tx_buffer, int nr_of_bytes, unsigned int first_bit, int nr_of_bits)
{
  unsigned int value;
  int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  gpio_sreg_store(reg_sel, 0);

  for (i=nr_of_bytes-1; i>=0; i--)
  {
    value = tx_buffer[i];
    value = (value << (7-first_bit));

    for (j=0; j<nr_of_bits; j++)
    {
      gpio_sreg_clk(reg_sel, 0);
      gpio_sreg_di(reg_sel, value & 0x80);
      gpio_sreg_wait();
      gpio_sreg_clk(reg_sel, 1);
      gpio_sreg_wait();
      value <<= 1;
    }
  }
  gpio_sreg_clk(reg_sel, 0);
  gpio_sreg_wait();
  gpio_sreg_store(reg_sel, 1);
  gpio_sreg_wait();
  gpio_sreg_store(reg_sel, 0);
}

/******************************************************************************/

void frontend_setting_apply()
{
  gpio_sreg_transmit(SREG25, FRONTEND_SETTINGS, 16, 1, 2);
  gpio_sreg_transmit(SREG33, FRONTEND_SETTINGS, 16, 7, 5);
}

/******************************************************************************/

void frontend_setting_clr(unsigned int channel, unsigned int value)
{
  FRONTEND_SETTINGS[channel] &= ~value;
}

/******************************************************************************/

void frontend_setting_set(unsigned int channel, unsigned int value)
{
  FRONTEND_SETTINGS[channel] |= value;
}

/******************************************************************************/

void frontend_setting_write(unsigned int channel, unsigned int value)
{
  FRONTEND_SETTINGS[channel] = value;
}

/******************************************************************************/
/******************************************************************************/
