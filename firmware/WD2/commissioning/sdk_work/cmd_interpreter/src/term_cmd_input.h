//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Capturing commands from terminal.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __TERM_CMD_INPUT_H__
#define __TERM_CMD_INPUT_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

//#include "system.h"
#include "utilities.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

//#define fstrcmp(cp1,cp2) ncmp(cp1,cp2,(sizeof(cp2)-1))

//const unsigned int NR_OF_INT_SYNC_LINES = 12;
//const unsigned int NR_OF_LR_IF_LINES    = 40;
//const unsigned int POSITIVE = 1;
//const unsigned int NEGATIVE = 0;


const unsigned int RECV_BUFF_LEN = 255;

//Maximum number of entries in the command history
const unsigned int MAX_HISTORY_SIZE = 20;

/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/

//void program_flash_data(void);

/******************************************************************************/
/* class definition                                                           */
/******************************************************************************/

class term_cmd_input_class
{
  public:
    term_cmd_input_class(void);
    ~term_cmd_input_class(void);
    int get_cmd(void);
    void  chk_cmd(void);

  private:

    int esc;
    int prompt;
    unsigned int bptr;
    char buffer[RECV_BUFF_LEN+1];

};

/******************************************************************************/

class cmd_history_item_class
{
  public:
    cmd_history_item_class(const char *cmd, xfs_u32 len);
    ~cmd_history_item_class();

    cmd_history_item_class* younger; // points to the younger item
    cmd_history_item_class* older;   // points to the older item
    char* command;
    xfs_u32 length;
};

/******************************************************************************/

class cmd_history_class
{
  public:
    cmd_history_class(xfs_u32 max_items);
    ~cmd_history_class();

    xfs_u32 is_empty();
    xfs_u32 add_new_cmd(const char *cmd, xfs_u32 len);
    xfs_u32 move_to_youngest();
    xfs_u32 move_to_younger();
    xfs_u32 move_to_older();
    const char* get_current_cmd();
    xfs_u32 get_current_length();

  private:
    cmd_history_item_class* youngest_cmd_ptr;
    cmd_history_item_class* current_cmd_ptr;

    xfs_u32 max_nr_of_items;
    xfs_u32 nr_of_items;

    void delete_oldest_cmd();
};

/******************************************************************************/

#endif // __TERM_CMD_INPUT_H__
