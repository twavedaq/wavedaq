//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  14.05.2014 13:22:20
//
//  Description :  Software interface for gpio slow control signal inputs and outputs.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xparameters.h"
#include "system.h"
#include "xil_printf.h"
//#include "xtime.h"
#include "gpio_slow_control.h"

/******************************************************************************/

void gpio_sc_init(void)
{
  SYSTEM->gpio_sys->write(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_LED_R |
                                                     GPIO_SC_OUT_LED_B |
                                                     GPIO_SC_OUT_RESET_FPGA);
}

/******************************************************************************/

void gpio_sc_set_led(unsigned int val)
{
  SYSTEM->gpio_sys->set(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_G | GPIO_SC_OUT_LED_B);
  SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, val&(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_G | GPIO_SC_OUT_LED_B) );
}

/******************************************************************************/

void gpio_sc_reset()
{
  SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_RESET_FPGA);
}

/******************************************************************************/

void gpio_sc_set_cal_buf(int val)
{
  if(val)
  {
    //buffers on
    SYSTEM->gpio_sys->set(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_BUFFER_CTRL);
  }
  else
  {
    //buffers off
    SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_BUFFER_CTRL);
  }
}

/******************************************************************************/

void gpio_sc_set_cal_clk(int source, int destination)
{
  if(source)
  {
    //tca is source
    SYSTEM->gpio_sys->set(GPIO_SLOW_CONTROL_OUTPUTS, destination);
  }
  else
  {
    //lmk is source
    SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, destination);
  }
}

/******************************************************************************/

void gpio_sc_set_cal_osc(int val)
{
  if(val)
  {
    //oscillator on
    SYSTEM->gpio_sys->set(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_TCA_CTRL);
  }
  else
  {
    //oscillator off
    SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_TCA_CTRL);
  }
}

/******************************************************************************/

void gpio_sc_set_pwr_cmp(int val)
{
  if(val)
  {
    //comparator power on
    SYSTEM->gpio_sys->set(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_PWR_CMP);
  }
  else
  {
    //comparator power off
    SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_PWR_CMP);
  }
}

/******************************************************************************/

void gpio_sc_clk_sel_ext(int val)
{
  if(val)
  {
    //enable TR_CLK
    SYSTEM->gpio_sys->set(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_CLK_SEL_EXT);
  }
  else
  {
    //disable TR_CLK
    SYSTEM->gpio_sys->clr(GPIO_SLOW_CONTROL_OUTPUTS, GPIO_SC_OUT_CLK_SEL_EXT);
  }
}

/******************************************************************************/

int gpio_sc_get_vers()
{
  unsigned int inputs;
  unsigned int mask = GPIO_SC_VERS_IND;

  inputs = SYSTEM->gpio_sys->get(GPIO_SLOWCONTROL_INPUTS);

  while( !(mask&0x01) )
  {
    mask   >>= 1;
    inputs >>= 1;
  }

  return (int)(inputs&mask);
}

/******************************************************************************/
/******************************************************************************/
