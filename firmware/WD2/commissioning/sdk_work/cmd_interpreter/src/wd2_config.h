//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  24.04.2014 11:53:05
//
//  Description :  Structure of table to list names of implemented commands that can be
//                 interpreted plus a pointer to the function to be called by this
//                 command.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __WD2_CONFIG_H__
#define __WD2_CONFIG_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/
#define CFG_INCLUDE_HELP                1
#define CFG_INCLUDE_SPI_REG_LOOP        1
#define CFG_INCLUDE_TEMP                1
#define CFG_INCLUDE_DAC                 1
#define CFG_INCLUDE_LMK                 1
#define CFG_INCLUDE_FRONTEND_SREG       1
#define CFG_INCLUDE_DRS4_CONTROL        1
#define CFG_INCLUDE_LED_CONTROL         1
#define CFG_INCLUDE_EXT_MEMORY          1
#define CFG_INCLUDE_SYSTEM_MANAGEMENT   1
#define CFG_INCLUDE_SERDES              1
#define CFG_INCLUDE_HV                  1
#define CFG_INCLUDE_MODULE_1            0
#define CFG_INCLUDE_MODULE_2            0
#define CFG_INCLUDE_CREDITS             1

  extern cmd_table_entry *cmd_list[];

#endif // __WD2_CONFIG_H__
