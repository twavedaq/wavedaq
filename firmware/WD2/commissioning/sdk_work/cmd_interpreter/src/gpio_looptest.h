//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  20.06.2014 12:23:35
//
//  Description :  IO test with external loopback cable or adapter.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __GPIO_LOOPTEST__
#define __GPIO_LOOPTEST__

/************************************************************/

#define ONE_HOT    0
#define ONE_COLD   1

// GPIO HV IOs - Test definitions (_IN,_OUT)
#define  GPIO_HV_MISO_IN         0x00000001
#define  GPIO_HV_MOSI_IN         0x00000002
#define  GPIO_HV_EN_IN           0x00000004
#define  GPIO_HV_RES_IN          0x00000008
#define  GPIO_HV_CS_OUT          0x00000010
#define  GPIO_HV_RX_OUT          0x00000020
#define  GPIO_HV_TX_OUT          0x00000040

// original definitions (_IN,_OUT)
//#define  GPIO_HV_MISO_IN         0x00000001
//#define  GPIO_HV_MOSI_OUT        0x00000002
//#define  GPIO_HV_EN_OUT          0x00000004
//#define  GPIO_HV_RES_OUT         0x00000008
//#define  GPIO_HV_CS_OUT          0x00000010
//#define  GPIO_HV_RX_OUT          0x00000020
//#define  GPIO_HV_TX_OUT          0x00000040


/************************************************************/

void gpio_looptest_init(void);

int serdes_looptest_oh_oc(int nr_of_bits, int type, int debug_level);
int hv_looptest_oh_oc(int type, int debug_level);

#endif // __GPIO_LOOPTEST__
