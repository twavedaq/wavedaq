//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  20.06.2014 12:23:39
//
//  Description :  IO test with external loopback cable or adapter.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xparameters.h"
#include "system.h"
#include "xil_printf.h"
#include "xtime.h"
#include "gpio_looptest.h"

/******************************************************************************/

void gpio_looptest_init(void)
{
  SYSTEM->gpio_sys->tristate_write(GPIO_HV, GPIO_HV_MISO_IN |
                                            GPIO_HV_MOSI_IN |
                                            GPIO_HV_EN_IN   |
                                            GPIO_HV_RES_IN);
}

/******************************************************************************/

int serdes_looptest_oh_oc(int nr_of_bits, int type, int debug_level)
{
  unsigned int value = 0x00000001;
  unsigned int output;
  unsigned int input;
  unsigned int mask = 0x000000001;
  unsigned int errors = 0;
  int i;

  for(i=0;i<nr_of_bits-1;i++)
  {
    mask <<= 0x01;
    mask  |= 0x01;
  }

  if(debug_level)
  {
    xil_printf("Mask: 0x%08X\n\r", mask);
  }

  // do test for each line
  for(i=0;i<nr_of_bits;i++)
  {
    // drive output
    if(type == ONE_COLD)
    {
      output = (~value)&mask;
    }
    else
    {
      output = value&mask;
    }
    SYSTEM->gpio_sys->write(GPIO_SERDES_DCB_OUTPUTS, output);
    usleep(10);

    // check input
    input = SYSTEM->gpio_sys->get(GPIO_SERDES_INPUTS);
    input &= mask;
    if(input != output)
    {
      errors++;
      if(debug_level)
      {
        xil_printf("Error: output 0x%08X - input 0x%08X\n\r", output, input);
      }
    }

    // update value
    value <<= 1;
  }

  // report result
  if(errors)
  {
    xil_printf("\n\rSerdes loop test failed with %d errors.\n\r\n\r", errors);
  }
  else
  {
    xil_printf("\n\rSerdes loop test sucessfully finished.\n\rSerdes connections ok.\n\r\n\r");
  }

  return errors;
}

/******************************************************************************/

int hv_looptest_oh_oc(int type, int debug_level)
{
  unsigned int value = 0x00000001;
  unsigned int output;
  unsigned int input;
  unsigned int mask = 0x000000001;
  unsigned int errors = 0;
  int nr_of_bits = 3;
  int i;

  for(i=0;i<nr_of_bits;i++) // mask 1 bit more due to GND<->HV_RES
  {
    mask <<= 0x01;
    mask  |= 0x01;
  }

  if(debug_level)
  {
    xil_printf("Mask: 0x%08X\n\r", mask);
  }

  // do test for each line
  for(i=0;i<nr_of_bits;i++)
  {
    // drive output
    if(type == ONE_COLD)
    {
      output = (~value)&(mask&(~GPIO_HV_RES_IN)); // HV_RES is connected to GND
    }
    else
    {
      output = value&mask;
    }
    SYSTEM->gpio_sys->write(GPIO_HV, (output<<4) );
    usleep(10);

    // check input
    input = SYSTEM->gpio_sys->get(GPIO_HV);
    input &= mask;
    if(input != output)
    {
      errors++;
      if(debug_level)
      {
        xil_printf("Error: output 0x%08X - input 0x%08X\n\r", output, input);
      }
    }

    // update value
    value <<= 1;
  }

  // report result
  if(errors)
  {
    xil_printf("\n\rHV interface loop test failed with %d errors.\n\r\n\r", errors);
  }
  else
  {
    xil_printf("\n\rHV interface loop test sucessfully finished.\n\rHV interface connections ok.\n\r\n\r");
  }

  return errors;
}

/******************************************************************************/
/******************************************************************************/
