//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 08:55:59
//
//  Description :  Module for SPI access to LTC2600 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_frontend_shiftreg.h"
#include "gpio_shift_register.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry frontend_shiftreg_cmd_table[] =
  {
    {"fe", frontend_help},
    {"feset", frontend_set},
    {0, general_frontend_help_func}
  };

/************************************************************/

  int frontend_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rFrontend configuration commands:\n\r");
      xil_printf("feset:  configure one or all channels of the frontend\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int frontend_set(int arc, char **argv)
  {
    unsigned int ch_nr;
    unsigned int reg_val;
    int i;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: feset <frontend channel> <settings value>\n\r\n\r");
      xil_printf("       frontend channel:   frontend channel to set (range = 1..16 or all).\n\r");
      xil_printf("       settings value:     hex value representing the settings.\n\r");
      xil_printf("                           Bit layout:\n\r");
      xil_printf("                           D7    D6    D5    D4    D3    D2    D1    D0   \n\r");
      xil_printf("                           ACDC  COMP2 OP2   COMP1 OP1   RSRVD CAL1  CAL0 \n\r");
      xil_printf("                           ^MSB                                      ^LSB\n\r");
      xil_printf("\n\rDescription: Configures the coresponding channel of the frontend.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      reg_val = hatoi(argv[2]);

      if(fstrcmp(argv[1],"all")) //write setting to all channels
      {
        for(i=0;i<16;i++)
        {
          frontend_setting_write(i, reg_val);
        }
      }
      else  //write setting to one channel
      {
        ch_nr = atoi(argv[1]) - 1;
        //Validate argument range
        if(ch_nr > 15)
        {
          xil_printf("Invalid arguments. Type \"feset help\" for help.\n\r");
          return 0;
        }
        frontend_setting_write(ch_nr, reg_val);
      }
      frontend_setting_apply();
    }
    return 0;
  }

/************************************************************/

  int general_frontend_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* Frontend Configuration Module:\n\rCan be used to program the shift registers configuring the frontend.\n\r");
    xil_printf("Type \"fe help\" for help on frontend commands.\n\r");
    return 0;
  }

/************************************************************/
