//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  24.04.2014 11:50:06
//
//  Description :  Example module for a command line interpreter test application
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "module1.h"
#include "xil_printf.h"

  cmd_table_entry module_1_cmd_table[] =
  {
    {"mod1f1", mod1func1},
    {"mod1f2", mod1func2},
#if INCLUDE_MOD1_FUNC_3
    {"mod1f3", mod1func3},
#endif
    {0, general_module1_help_func}
  };

  int mod1func1(int arc, char **argv)
  {
    xil_printf("I'm function mod1func1 of module1 called by %s with argument %s\n\r", argv[0], argv[1]);
    return 0;
  }

  int mod1func2(int arc, char **argv)
  {
    xil_printf("I'm function mod1func2 of module1 called by %s with arguments %s and %s\n\r", argv[0], argv[1], argv[2]);
    return 0;
  }

#if INCLUDE_MOD1_FUNC_3
  int mod1func3(int arc, char **argv)
  {
    xil_printf("I'm function mod1func3 of module1 called by %s with arguments %s, %s and %s\n\r", argv[0], argv[1], argv[2], argv[3]);
    return 0;
  }
#endif

  int general_module1_help_func(int arc, char **argv)
  {
    xil_printf("\n\rThis is the help text for module1\n\r");
    return 0;
  }
