//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  02.05.2014 13:24:35
//
//  Description :  Code to use with a simple 32 bit timer running continuously.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __XTIME_H__
#define __XTIME_H__

/******************************************************************************/

#include "xparameters.h"
#include "utilities.h"

//#define TIME_FREQUENCY 125000000
#define TIME_FREQUENCY XPAR_PROC_BUS_0_FREQ_HZ

// beware of rounding problems !!!
#define TIME_PICO_SEC_PER_TIC (1000000000 / (TIME_FREQUENCY/1000))

#define time_tic_read() (xfs_in32(XPAR_PLB_TIMEBASE_0_BASEADDR))

void usleep(unsigned int usec);
void msleep(unsigned int msec);
//void sleep(unsigned int sec);

/******************************************************************************/

#endif // __XTIME_H__
