//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 08:55:55
//
//  Description :  Module for SPI access to LTC2600 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_FRONTEND_SHIFTREG_H__
#define __MOD_FRONTEND_SHIFTREG_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

  extern cmd_table_entry frontend_shiftreg_cmd_table[];

  int frontend_help(int arc, char **argv);
  int frontend_set(int arc, char **argv);
  int general_frontend_help_func(int arc, char **argv);

#endif // __MOD_FRONTEND_SHIFTREG_H__
