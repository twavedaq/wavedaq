//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  05.05.2014 14:11:28
//
//  Description :  Module for SPI access to LMK03000 configurable pll clock
//                 distributor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_LMK_H__
#define __MOD_LMK_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

#define INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS   1

  extern cmd_table_entry lmk_cmd_table[];

  int lmk_help(int arc, char **argv);
  int lmk_reset(int arc, char **argv);
#if INCLUDE_MOD_SPI_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_channel(int arc, char **argv);
  int lmk_set_clkout_en(int arc, char **argv);
  int lmk_set_loop_filter(int arc, char **argv);
  int lmk_set_oscin_freq(int arc, char **argv);
  int lmk_set_pll(int arc, char **argv);
  int lmk_set(int arc, char **argv);
#endif
  int lmk_set_reg(int arc, char **argv);
  int lmk_upload(int arc, char **argv);
  int lmk_set_sync(int arc, char **argv);
  int lmk_get_lock(int arc, char **argv);
  int general_spi_lmk_help_func(int arc, char **argv);

#endif // __MOD_LMK_H__
