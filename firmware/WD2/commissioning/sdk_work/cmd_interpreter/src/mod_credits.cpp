//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  23.05.2014 13:14:46
//
//  Description :  Credits of the DRS4 board. Maybe incomplete. Just fun.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_credits.h"
#include "wd2_config.h"
#include "xil_printf.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry credits_cmd_table[] =
  {
    {"stefan", credits_stefan},
    {"ueli", credits_ueli},
    {"gerd", credits_gerd},
    {"elmar", credits_elmar},
    {0, 0}
  };

/************************************************************/

  int credits_stefan(int arc, char **argv)
  {
    xil_printf("\n\r...did a great job inventing me\n\r   and designing my chips together with roberto.\n\r\n\r");
    return 0;
  }

/************************************************************/

  int credits_ueli(int arc, char **argv)
  {
    xil_printf("\n\r...did a great job designing me...\n\r...even though suffering from the mentor tools.\n\r\n\r");
    return 0;
  }

/************************************************************/

  int credits_gerd(int arc, char **argv)
  {
    xil_printf("\n\r...hopefully does a great job\n\r   writing firmware for me.\n\r\n\r");
    return 0;
  }

/************************************************************/

  int credits_elmar(int arc, char **argv)
  {
    xil_printf("\n\r...hopefully does a great job\n\r   writing firmware for me.\n\r\n\r");
    return 0;
  }

/************************************************************/
/************************************************************/
