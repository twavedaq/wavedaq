//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Software controlled SPI controller for communication with
//                 temperature sensor MAX6662, DAC LTC2600 and LMK03000.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "xparameters.h"
#include "system.h"
#include "xil_printf.h"
#include "xtime.h"
#include "drv_soft_spi_temp_dac_lmk.h"

/******************************************************************************/

/************************************************************/
//LMK03000 variables
/************************************************************/
//Initialize LMK registers to their default values
unsigned char  LMK03000_REGISTERS[16][4]  = {{0x00,0x03,0xC8,0x00},  /* Register  0 */
                                             {0x00,0x03,0x02,0x01},  /* Register  1 */
                                             {0x00,0x03,0x02,0x02},  /* Register  2 */
                                             {0x00,0x03,0x02,0x03},  /* Register  3 */
                                             {0x00,0x00,0x01,0x04},  /* Register  4 */
                                             {0x00,0x00,0x01,0x05},  /* Register  5 */
                                             {0x00,0x00,0x01,0x06},  /* Register  6 */
                                             {0x00,0x03,0xC8,0x07},  /* Register  7 */
                                             {0x10,0x00,0x09,0x08},  /* Register  8 */
                                             {0xA0,0x02,0x2A,0x09},  /* Register  9 */
                                             {0x00,0x00,0x00,0x0A},  /* Register 10 */
                                             {0x00,0x82,0x00,0x0B},  /* Register 11 */
                                             {0x00,0x00,0x00,0x0C},  /* Register 12 */
                                             {0x02,0x99,0x00,0xAD},  /* Register 13 */
                                             {0x18,0x00,0x14,0x0E},  /* Register 14 */
                                             {0xCC,0x00,0x50,0x0F}}; /* Register 15 */
//unsigned char  LMK03000_REGISTERS[16][4]  = {{0x00,0x00,0x01,0x00},  /* Register  0 */
//                                             {0x00,0x00,0x01,0x01},  /* Register  1 */
//                                             {0x00,0x00,0x01,0x02},  /* Register  2 */
//                                             {0x00,0x00,0x01,0x03},  /* Register  3 */
//                                             {0x00,0x00,0x01,0x04},  /* Register  4 */
//                                             {0x00,0x00,0x01,0x05},  /* Register  5 */
//                                             {0x00,0x00,0x01,0x06},  /* Register  6 */
//                                             {0x00,0x00,0x01,0x07},  /* Register  7 */
//                                             {0x10,0x00,0x09,0x08},  /* Register  8 */
//                                             {0xA0,0x02,0x2A,0x09},  /* Register  9 */
//                                             {0x00,0x00,0x00,0x0A},  /* Register 10 */
//                                             {0x00,0x82,0x00,0x0B},  /* Register 11 */
//                                             {0x00,0x00,0x00,0x0C},  /* Register 12 */
//                                             {0x02,0x82,0x80,0x0D},  /* Register 13 */
//                                             {0x08,0x00,0x0A,0x0E},  /* Register 14 */
//                                             {0x08,0x02,0xF8,0x0F}}; /* Register 15 */
/************************************************************/

/************************************************************/
//LTC2600 variables
/************************************************************/
//Initial DAC voltages in nV
unsigned int LTC2600_INIT_NV[8] = {1600*1e6,  //VOUTA ROFS
                                   1250*1e6, //VOUTB OFS
                                   1250*1e6, //VOUTC CAL_DC
                                   800*1e6,  //VOUTD TLEVEL_1
                                   800*1e6,  //VOUTE TLEVEL_2
                                   800*1e6,  //VOUTF TLEVEL_3
                                   800*1e6,  //VOUTG TLEVEL_4
                                   700*1e6}; //VOUTH BIAS
/************************************************************/

void lmk03000_init();
void ltc2600_init();

void soft_spi_temp_dac_lmk_init(void)
{
//  SYSTEM->gpio_sys->write(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SCLK_O | SPI_TEMP_DAC_LMK_SDI_IO | 
//                        SPI_TEMP_CS | SPI_DAC_CS | SPI_LMK_CS);
  SYSTEM->gpio_sys->write(GPIO_SOFT_SPI_TEMP_DAC_LMK, GPIO_LMK_SYNC_OUT | SPI_TEMP_CS | SPI_DAC_CS | SPI_LMK_CS);
  SYSTEM->gpio_sys->tristate_clr(GPIO_SOFT_SPI_TEMP_DAC_LMK, GPIO_LMK_SYNC_OUT | SPI_TEMP_DAC_LMK_SCLK_O | SPI_TEMP_DAC_LMK_SDI_IO | 
                                 SPI_TEMP_CS | SPI_DAC_CS | SPI_LMK_CS);
  SYSTEM->gpio_sys->tristate_set(GPIO_SOFT_SPI_TEMP_DAC_LMK, GPIO_LMK_LD_IN);

  lmk03000_upload_configuration();
  ltc2600_init();
}

/******************************************************************************/

void soft_spi_sck(unsigned int val)
{
  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SCLK_O );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SCLK_O );
  }
}

/******************************************************************************/

void soft_spi_cs_n(unsigned char slave, unsigned int val)
{
  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SPI_TEMP_DAC_LMK, slave );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SPI_TEMP_DAC_LMK, slave );
  }
}

/******************************************************************************/

void soft_spi_sdo(unsigned int val)
{
  if (val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDI_IO );
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDI_IO );
  }
}

/******************************************************************************/

unsigned int soft_spi_sdi()
{
  if (SYSTEM->gpio_sys->get(GPIO_SOFT_SPI_TEMP_DAC_LMK) & SPI_TEMP_DAC_LMK_SDI_IO)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void soft_spi_wait(void)
{
  usleep(1);
}

/******************************************************************************/

void soft_spi_set_data_direction(unsigned int val)
{
  if(val == SPI_DATA_DIR_OUT)
  {
    SYSTEM->gpio_sys->tristate_clr(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDI_IO);
  }
  else
  {
    SYSTEM->gpio_sys->tristate_set(GPIO_SOFT_SPI_TEMP_DAC_LMK, SPI_TEMP_DAC_LMK_SDI_IO);
  }
}

/******************************************************************************/

void soft_spi_transmit(unsigned char slave, unsigned char* tx_buffer, unsigned int nr_of_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_set_data_direction(SPI_DATA_DIR_OUT);

  soft_spi_cs_n(slave, 0);
  usleep(1);

  for (i=0; i<nr_of_bytes; i++)
  {
    value = tx_buffer[i];
  
    for (j=0; j<8; j++)
    {
      soft_spi_sck(0);
      soft_spi_sdo(value & 0x80);
      soft_spi_wait();
      soft_spi_sck(1);
      soft_spi_wait();
      value <<= 1;
    }
  }
  soft_spi_sck(0);

  soft_spi_wait();
  soft_spi_cs_n(slave, 1);

}

/******************************************************************************/

void soft_spi_transceive(unsigned char slave, unsigned char* tx_buffer, unsigned char* rx_buffer, unsigned int nr_of_tx_bytes, unsigned int nr_of_rx_bytes)
{
  unsigned int value;
  unsigned int i,j;

/*
*  Format :
*           D7 D6 D5 D4 D3 D2 D1 D0
*           ^first               ^last
*/

  soft_spi_cs_n(slave, 0);
  usleep(1);

  //transmit
  soft_spi_set_data_direction(SPI_DATA_DIR_OUT);

  for (i=0; i<nr_of_tx_bytes; i++)
  {
    value = tx_buffer[i];

    for (j=0; j<8; j++)
    {
      soft_spi_sck(0);
      soft_spi_sdo(value & 0x80);
      soft_spi_wait();
      soft_spi_sck(1);
      soft_spi_wait();
      value <<= 1;
    }
  }

  //receive
  usleep(1);
  soft_spi_set_data_direction(SPI_DATA_DIR_IN);
  usleep(1);

  for (i=0; i<nr_of_rx_bytes; i++)
  {
    value = 0;
  
    for (j=0; j<8; j++)
    {
      value <<= 1;
      soft_spi_sck(0);
      soft_spi_wait();
      soft_spi_sck(1);
      value |= soft_spi_sdi();
      soft_spi_wait();
    }
    soft_spi_sck(0);
    soft_spi_wait();

    rx_buffer[i] = value;
  }

  soft_spi_wait();
  soft_spi_cs_n(slave, 1);
  //reset data direction after deselecting slave since slave only stops
  //driving the line when deselcted.
  soft_spi_wait();
  soft_spi_set_data_direction(SPI_DATA_DIR_OUT);
}



/******************************************************************************/
/******************************************************************************/

int max6662_get_temp()
{
  unsigned char tx_buffer[1] = {MAX6662_CMD_READ | MAX6662_REG_TEMP};
  unsigned char rx_buffer[2];
  int temp;

  soft_spi_transceive(SPI_TEMP_CS, tx_buffer, rx_buffer, 1, 2);

  temp = ((int)rx_buffer[0] << 5) | (((int)rx_buffer[1] >> 3) & 0x1F);

  if(temp & 0x00000400) //sign bit is set
  {
    temp |= 0xFFFFF800; //negative value: set msbs to 1
  }
  else
  {
    temp &= 0x000003FF; //positivie value: set msbs to 0
  }

  return temp;
}

/******************************************************************************/

void max6662_print_temp()
{
  int temp;
  unsigned int decimals;

  temp = max6662_get_temp();

  decimals = temp & 0x0F;
  if(temp < 0)
  {
    decimals = (~(decimals-1) & 0x0F);
  }
  decimals = decimals * 625;

  xil_printf("%d.%04d", temp/16 , decimals);
}

/******************************************************************************/
/******************************************************************************/

void lmk03000_upload_configuration()
{
  //Set all LMK registers to initial values
  int i;
  
  for(i=0;i<16;i++)
  {
    if( (i!=10) && (i!=12) ) // Registers 10 and 12 do are not writable on LMK
    {
      soft_spi_transmit(SPI_LMK_CS, &LMK03000_REGISTERS[i][0], 4);
    }
  }
}

/******************************************************************************/

void lmk03000_reset()
{
  LMK03000_REGISTERS[0][0] |= LMK03000_RESET;

  soft_spi_transmit(SPI_LMK_CS, &LMK03000_REGISTERS[0][0], 4);
}

/******************************************************************************/

void lmk03000_set_sync(int val)
{
  if(val)
  {
    SYSTEM->gpio_sys->set(GPIO_SOFT_SPI_TEMP_DAC_LMK, GPIO_LMK_SYNC_OUT);
  }
  else
  {
    SYSTEM->gpio_sys->clr(GPIO_SOFT_SPI_TEMP_DAC_LMK, GPIO_LMK_SYNC_OUT);
  }
}

/******************************************************************************/

int lmk03000_get_ld()
{
  if( SYSTEM->gpio_sys->get(GPIO_SOFT_SPI_TEMP_DAC_LMK)&GPIO_LMK_LD_IN )
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void lmk03000_set_channel(unsigned char ch_nr, unsigned char mux, unsigned char enable, unsigned char divider, unsigned char delay)
{
  LMK03000_REGISTERS[ch_nr][0] = 0;
  LMK03000_REGISTERS[ch_nr][1] = ((mux & 0x03) << 1) | (enable & 0x01);
  LMK03000_REGISTERS[ch_nr][2] = divider;
  LMK03000_REGISTERS[ch_nr][3] = (delay << 4)  | ch_nr;

  soft_spi_transmit(SPI_LMK_CS, &LMK03000_REGISTERS[ch_nr][0], 4);
}

/******************************************************************************/

void lmk03000_set_clkout_en(unsigned char ch_nr, unsigned char clkout_en)
{
  if(!clkout_en)
  {
    LMK03000_REGISTERS[ch_nr][1] &= ~LMK03000_CLKOUT_EN;
  }
  else
  {
    LMK03000_REGISTERS[ch_nr][1] |= LMK03000_CLKOUT_EN;
  }

  soft_spi_transmit(SPI_LMK_CS, &LMK03000_REGISTERS[ch_nr][0], 4);
}

/******************************************************************************/

void lmk03000_set_vboost(unsigned char vboost)
{
  if(!vboost)
  {
    LMK03000_REGISTERS[LMK03000_REG9][1] &= ~LMK03000_VBOOST;
  }
  else
  {
    LMK03000_REGISTERS[LMK03000_REG9][1] |= LMK03000_VBOOST;
  }

  soft_spi_transmit(SPI_LMK_CS, &LMK03000_REGISTERS[LMK03000_REG9][0], 4);
}

/******************************************************************************/

void lmk03000_set_div4(unsigned char div4)
{
  if(!div4)
  {
    LMK03000_REGISTERS[LMK03000_REG11][2] &= ~LMK03000_DIV4;
  }
  else
  {
    LMK03000_REGISTERS[LMK03000_REG11][2] |= LMK03000_DIV4;
  }

  soft_spi_transmit(SPI_LMK_CS, &LMK03000_REGISTERS[LMK03000_REG11][0], 4);
}

/******************************************************************************/

void lmk03000_set_loop_filter(unsigned char r4_lf, unsigned char r3_lf, unsigned char c3_c4_lf)
{
  LMK03000_REGISTERS[LMK03000_REG13][2] &= 0xC0;
  LMK03000_REGISTERS[LMK03000_REG13][2] |= ((r4_lf    & 0x07) << 3) | (r3_lf & 0x07);
  LMK03000_REGISTERS[LMK03000_REG13][3] =  ((c3_c4_lf & 0x0F) << 4) | LMK03000_REG13;

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_oscin_freq(unsigned char oscin_freq)
{
  LMK03000_REGISTERS[LMK03000_REG13][1] &= 0xC0;
  LMK03000_REGISTERS[LMK03000_REG13][1] |= (oscin_freq >> 2);
  LMK03000_REGISTERS[LMK03000_REG13][2] &= 0x3F;
  LMK03000_REGISTERS[LMK03000_REG13][2] |= (oscin_freq << 6);

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_pll_mux(unsigned char pll_mux)
{
  LMK03000_REGISTERS[LMK03000_REG14][1] &= 0x0F;
  LMK03000_REGISTERS[LMK03000_REG14][1] |= (pll_mux << 4);

  lmk03000_upload_configuration();
}

/******************************************************************************/

unsigned char lmk03000_get_pll_mux()
{
  unsigned char pll_mux;
  pll_mux = LMK03000_REGISTERS[LMK03000_REG14][1] & 0xF0;
  pll_mux >>= 4;

  return pll_mux;
}

/******************************************************************************/

void lmk03000_set_en_fout(unsigned char en_fout)
{
  if(!en_fout)
  {
    LMK03000_REGISTERS[LMK03000_REG14][0] &= ~LMK03000_EN_FOUT;
  }
  else
  {
    LMK03000_REGISTERS[LMK03000_REG14][0] |= LMK03000_EN_FOUT;
  }

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_en_clkout_global(unsigned char en_clkout_global)
{
  if(!en_clkout_global)
  {
    LMK03000_REGISTERS[LMK03000_REG14][0] &= ~LMK03000_EN_CLKOUT_GLOBAL;
  }
  else
  {
    LMK03000_REGISTERS[LMK03000_REG14][0] |= LMK03000_EN_CLKOUT_GLOBAL;
  }

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_powerdown(unsigned char powerdown)
{
  if(!powerdown)
  {
    LMK03000_REGISTERS[LMK03000_REG14][0] &= ~LMK03000_POWERDOWN;
  }
  else
  {
    LMK03000_REGISTERS[LMK03000_REG14][0] |= LMK03000_POWERDOWN;
  }

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_pll_r(unsigned int pll_r)
{
  LMK03000_REGISTERS[LMK03000_REG14][1] &= 0xF0;
  LMK03000_REGISTERS[LMK03000_REG14][1] |= (unsigned char)((pll_r >> 8) & 0x0F);
  LMK03000_REGISTERS[LMK03000_REG14][2] = (unsigned char)(pll_r & 0x000000FF);

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_pll_n(unsigned int pll_n)
{
  LMK03000_REGISTERS[LMK03000_REG15][0] &= 0xFC;
  LMK03000_REGISTERS[LMK03000_REG15][0] |= (unsigned char)((pll_n >> 16) & 0x03);
  LMK03000_REGISTERS[LMK03000_REG15][1] =  (unsigned char)((pll_n >> 8) & 0x000000FF);
  LMK03000_REGISTERS[LMK03000_REG15][2] =  (unsigned char)(pll_n & 0x000000FF);

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_pll_cp_gain(unsigned char pll_cp_gain)
{
  LMK03000_REGISTERS[LMK03000_REG15][0] &= 0x3F;
  LMK03000_REGISTERS[LMK03000_REG15][0] |= ((pll_cp_gain & 0x03) << 6);

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_vco_div(unsigned char vco_div)
{
  LMK03000_REGISTERS[LMK03000_REG15][0] &= 0xC3;
  LMK03000_REGISTERS[LMK03000_REG15][0] |= ((vco_div & 0x0F) << 2);

  lmk03000_upload_configuration();
}

/******************************************************************************/

void lmk03000_set_reg(unsigned int reg_val)
{
  int adr;
  int i;

  adr = (reg_val & 0x0F);

  for(i=3;i>=0;i--)
  {
    LMK03000_REGISTERS[adr][i] = (unsigned char)(reg_val);
    reg_val = reg_val>>8;
  }
}

/******************************************************************************/
/******************************************************************************/

void ltc2600_set(unsigned int channel, unsigned int command, unsigned int voltage_nv)
{
  unsigned int  value;
  unsigned char data[3];

  //calculate DAC value from voltage
  value = voltage_nv/LTC2600_RES_NV;

  data[0] = command | channel;
  data[1] = (unsigned char)((value >> 8) & 0xFF);
  data[2] = (unsigned char)(value & 0xFF);

  soft_spi_transmit(SPI_DAC_CS, data, 3);
}

/******************************************************************************/

void ltc2600_init()
{
  int i;

  //Set values for each channel
  for(i=0;i<8;i++)
  {
    ltc2600_set(i, LTC2600_CMD_WR_N, LTC2600_INIT_NV[i]);
  }

  //Power Up all channels
  ltc2600_set(LTC2600_DAC_ADR_ALL, LTC2600_CMD_PU_N, 0);
}

/******************************************************************************/
/******************************************************************************/
