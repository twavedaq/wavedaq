//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  30.07.2014 13:32:42
//
//  Description :  Module for SPI access to LTC2600 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_SPI_REG_LOOP_H__
#define __MOD_SPI_REG_LOOP_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

  extern cmd_table_entry spi_reg_loop_cmd_table[];

  int spi_reg_loop_help(int arc, char **argv);
  int spi_reg_get_status(int arc, char **argv);
  int spi_reg_transceive(int arc, char **argv);
  int spi_reg_loop_cfg(int arc, char **argv);
  int spi_reg_loop_master_test(int arc, char **argv);
  int spi_reg_loop_master_slave_test(int arc, char **argv);
  int general_spi_reg_loop_help_func(int arc, char **argv);

#endif // __MOD_SPI_REG_LOOP_H__
