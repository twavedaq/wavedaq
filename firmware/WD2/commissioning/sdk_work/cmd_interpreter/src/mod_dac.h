//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  07.05.2014 08:55:55
//
//  Description :  Module for SPI access to LTC2600 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __MOD_DAC_H__
#define __MOD_DAC_H__

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "cmd_table.h"

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

  extern cmd_table_entry dac_cmd_table[];

  int dac_help(int arc, char **argv);
  int dac_configure_and_enable(int arc, char **argv);
  int dac_configure(int arc, char **argv);
  int dac_enable(int arc, char **argv);
  int dac_disable(int arc, char **argv);
  int general_spi_dac_help_func(int arc, char **argv);

#endif // __MOD_DAC_H__
