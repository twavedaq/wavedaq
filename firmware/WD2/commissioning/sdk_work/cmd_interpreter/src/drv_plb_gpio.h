//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  02.05.2014 13:24:35
//
//  Description :  Driver vor custom pcore plb_gpio.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __DRV_PLB_GPIO_H__
#define __DRV_PLB_GPIO_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "utilities.h"

/******************************************************************************/
/* class definition                                                           */
/******************************************************************************/

class plb_gpio_class
{
  public:
    plb_gpio_class(xfs_u32 b_address);

    void set(xfs_u32 io_register, xfs_u32 value);
    void clr(xfs_u32 io_register, xfs_u32 value);
    void write(xfs_u32 io_register, xfs_u32 value);
    xfs_u32 rb_out_reg(xfs_u32 io_register);
    xfs_u32 get(xfs_u32 io_register);
    void tristate_set(xfs_u32 io_register, xfs_u32 value);
    void tristate_clr(xfs_u32 io_register, xfs_u32 value);
    void tristate_write(xfs_u32 io_register, xfs_u32 value);
    xfs_u32 tristate_get(xfs_u32 io_register);

  private:
    xfs_u32 base_address;

    static const xfs_u32 NUM_OF_REG_PER_SET = 8;

    static const xfs_u32 OFFSET_REG_IO = 0x00;
    static const xfs_u32 OFFSET_REG_T  = 0x10;
    static const xfs_u32 OFFSET_OP_WR  = 0x00;
    static const xfs_u32 OFFSET_OP_RD  = 0x04;
    static const xfs_u32 OFFSET_OP_SET = 0x08;
    static const xfs_u32 OFFSET_OP_CLR = 0x0C;
};

/******************************************************************************/

#endif // __DRV_PLB_GPIO_H__
