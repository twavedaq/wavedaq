//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  02.05.2014 13:24:53
//
//  Description :  Module for SPI access to MAX6662 temperature sensor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_temp.h"
#include "drv_soft_spi_temp_dac_lmk.h"
#include "xil_printf.h"
#include "utilities.h"

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry temp_cmd_table[] =
  {
    {"temp", max6662_temp},
    {0, general_spi_temp_help_func}
  };

/************************************************************/
  int max6662_temp(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rTemperature sensor commands:\n\rtemp: reads and prints the current temperature read from MAX6662\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("\n\rTcurrent = ");
      max6662_print_temp();
      xil_printf("%cC\n\r\n\r",248);
    }
    return 0;
  }

/************************************************************/
  int general_spi_temp_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* SPI TEMP Module:\n\rAllows access to temperature sensor via SPI.\n\r");
    xil_printf("Type \"temp help\" for help on temperature commands.\n\r");
    return 0;
  }

/************************************************************/
