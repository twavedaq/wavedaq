/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_slave_v1_00_a/src/plb_spi_slave.h
* Version:           1.00.a
* Description:       plb_spi_slave Driver Header File
* Date:              Mon Jul 21 12:53:12 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/

#ifndef __PLB_SPI_SLAVE_H__
#define __PLB_SPI_SLAVE_H__

/***************************** Include Files *******************************/

#include "xfs_types.h"
#include "xstatus.h"
#include "xil_io.h"

/************************** Constant Definitions ***************************/

/**
 * User Logic Slave Space Offsets
 * -- SLV_REG0 : user logic slave module register 0
 * -- SLV_REG1 : user logic slave module register 1
 * -- SLV_REG2 : user logic slave module register 2
 * -- SLV_REG3 : user logic slave module register 3
 * -- SLV_REG4 : user logic slave module register 4
 * -- SLV_REG5 : user logic slave module register 5
 */

#define PLB_SPI_SLAVE_CONFIG_REG_OFFSET     0x00000000
#define PLB_SPI_SLAVE_STATUS_REG_OFFSET     0x00000004
#define PLB_SPI_SLAVE_TXD_REG_OFFSET        0x00000008
#define PLB_SPI_SLAVE_RXD_REG_OFFSET        0x0000000C

#define PLB_SPI_SLAVE_CONFIG_ENABLE         0x00000001
#define PLB_SPI_SLAVE_CONFIG_TXFIFO_RST     0x00000002
#define PLB_SPI_SLAVE_CONFIG_RXFIFO_RST     0x00000004
#define PLB_SPI_SLAVE_CONFIG_SW_RESET       0x00000008

#define PLB_SPI_SLAVE_STATUS_RXFIFO_EMPTY   0x00000001
#define PLB_SPI_SLAVE_STATUS_RXFIFO_AEMPTY  0x00000002
#define PLB_SPI_SLAVE_STATUS_TXFIFO_FULL    0x00000004
#define PLB_SPI_SLAVE_STATUS_TXFIFO_AFULL   0x00000008
#define PLB_SPI_SLAVE_STATUS_SLV_MODE_SEL   0x00000010

#define PLB_SPI_SLAVE

#define TRUE   1
#define FALSE  0

/**************************** Type Definitions *****************************/

/**
 * The XSpi driver instance data. The user is required to allocate a
 * variable of this type for every SPI device in the system. A pointer
 * to a variable of this type is then passed to the driver API functions.
 */
typedef struct {
  unsigned int  base_address;         /**< Base address of device (IPIF) */
  int           is_started;           /**< Device has been started */
  unsigned int  *send_buffer_ptr;     /**< Buffer to send  */
  unsigned int  *recv_buffer_ptr;     /**< Buffer to receive */
} plb_spi_slave;

/***************** Macros (Inline Functions) Definitions *******************/

/**
 *
 * Write a value to a PLB_SPI_slave register. A 32 bit write is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is written.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_slave device.
 * @param   RegOffset is the register offset from the base to write to.
 * @param   Data is the data written to the register.
 *
 * @return  None.
 *
 * @note
 * C-style signature:
 *   void sspi_mWriteReg(xfs_u32 BaseAddress, unsigned RegOffset, xfs_u32 Data)
 *
 */
#define sspi_mWriteReg(BaseAddress, RegOffset, Data) \
   Xil_Out32((BaseAddress) + (RegOffset), (xfs_u32)(Data))

/**
 *
 * Read a value from a PLB_SPI_slave register. A 32 bit read is performed.
 * If the component is implemented in a smaller width, only the least
 * significant data is read from the register. The most significant data
 * will be read as 0.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_slave device.
 * @param   RegOffset is the register offset from the base to write to.
 *
 * @return  Data is the data from the register.
 *
 * @note
 * C-style signature:
 *   xfs_u32 sspi_mReadReg(xfs_u32 BaseAddress, unsigned RegOffset)
 *
 */
#define sspi_mReadReg(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (RegOffset))


/**
 *
 * Write/Read 32 bit value to/from PLB_SPI_slave user logic slave registers.
 *
 * @param   BaseAddress is the base address of the PLB_SPI_slave device.
 * @param   RegOffset is the offset from the slave register to write to or read from.
 * @param   Value is the data written to the register.
 *
 * @return  Data is the data from the user logic slave register.
 *
 * @note
 * C-style signature:
 *   void sspi_mWriteSlaveRegn(xfs_u32 BaseAddress, unsigned RegOffset, xfs_u32 Value)
 *   xfs_u32 sspi_mReadSlaveRegn(xfs_u32 BaseAddress, unsigned RegOffset)
 *
 */
#define sspi_mWriteSlaveReg0(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_slave_CONFIG_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define sspi_mWriteSlaveReg1(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_slave_NUMDBITS_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define sspi_mWriteSlaveReg2(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_slave_STATUS_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define sspi_mWriteSlaveReg3(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_slave_TXD_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define sspi_mWriteSlaveReg4(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_slave_RXD_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))
#define sspi_mWriteSlaveReg5(BaseAddress, RegOffset, Value) \
   Xil_Out32((BaseAddress) + (PLB_SPI_slave_SLVSEL_REG_OFFSET) + (RegOffset), (xfs_u32)(Value))

#define sspi_mReadSlaveReg0(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_slave_CONFIG_REG_OFFSET) + (RegOffset))
#define sspi_mReadSlaveReg1(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_slave_NUMDBITS_REG_OFFSET) + (RegOffset))
#define sspi_mReadSlaveReg2(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_slave_STATUS_REG_OFFSET) + (RegOffset))
#define sspi_mReadSlaveReg3(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_slave_TXD_REG_OFFSET) + (RegOffset))
#define sspi_mReadSlaveReg4(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_slave_RXD_REG_OFFSET) + (RegOffset))
#define sspi_mReadSlaveReg5(BaseAddress, RegOffset) \
   Xil_In32((BaseAddress) + (PLB_SPI_slave_SLVSEL_REG_OFFSET) + (RegOffset))

/************************** Function Prototypes ****************************/
int  sspi_init(plb_spi_slave *instance_ptr, unsigned int base_address);
int sspi_receive(plb_spi_slave *instance_ptr, unsigned int *rx_buffer_ptr, unsigned int num_words, unsigned int word_timeout);
int sspi_transceive(plb_spi_slave *instance_ptr, unsigned int *tx_buffer_ptr, unsigned int *rx_buffer_ptr, unsigned int num_words, unsigned int word_timeout);
int  sspi_tx_fifo_is_full(plb_spi_slave *instance_ptr);
int  sspi_tx_fifo_is_almost_full(plb_spi_slave *instance_ptr);
int  sspi_rx_fifo_is_empty(plb_spi_slave *instance_ptr);
int  sspi_rx_fifo_is_almost_empty(plb_spi_slave *instance_ptr);
int sspi_get_status(plb_spi_slave *instance_ptr);
//void sspi_set_loopback(plb_spi_slave *instance_ptr, int loopback_enable);
void sspi_reset(plb_spi_slave *instance_ptr);
void sspi_reset_rxfifo(plb_spi_slave *instance_ptr);
void sspi_reset_txfifo(plb_spi_slave *instance_ptr);

#endif /** __PLB_SPI_slave_H__ */
