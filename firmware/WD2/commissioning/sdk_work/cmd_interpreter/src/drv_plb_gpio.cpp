//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  02.05.2014 13:24:35
//
//  Description :  Driver vor custom pcore plb_gpio.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------


/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "drv_plb_gpio.h"
#include "utilities.h"

/******************************************************************************/
/* class methods                                                              */
/******************************************************************************/

plb_gpio_class::plb_gpio_class(xfs_u32 b_address)
{
  base_address = io_remap(b_address);
}

/******************************************************************************/

void plb_gpio_class::set(xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_SET, value);
}

/******************************************************************************/

void plb_gpio_class::clr(xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_CLR, value);
}

/******************************************************************************/

void plb_gpio_class::write(xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_WR, value);
}

/******************************************************************************/

xfs_u32 plb_gpio_class::rb_out_reg(xfs_u32 io_register)
{
  return xfs_in32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_WR);
}

/******************************************************************************/

xfs_u32 plb_gpio_class::get(xfs_u32 io_register)
{
  return xfs_in32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_IO+OFFSET_OP_RD);
}

/******************************************************************************/

void plb_gpio_class::tristate_set(xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_SET, value);
}

/******************************************************************************/

void plb_gpio_class::tristate_clr(xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_CLR, value);
}

/******************************************************************************/

void plb_gpio_class::tristate_write(xfs_u32 io_register, xfs_u32 value)
{
  xfs_out32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_WR, value);
}

/******************************************************************************/

xfs_u32 plb_gpio_class::tristate_get(xfs_u32 io_register)
{
  return xfs_in32(base_address+(NUM_OF_REG_PER_SET*io_register*4)+OFFSET_REG_T+OFFSET_OP_WR);
}

/******************************************************************************/
