//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  12.05.2014 12:52:42
//
//  Description :  Module for configuring the DRS4 chips.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_drs4_control.h"
#include "gpio_drs4_control.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry drs4_control_cmd_table[] =
  {
    {"drs", drs4_help},
    {"drsmode", drs4_set_mode},
    {"drssetreg", drs4_set_register},
    {"drsgetlock", drs4_get_pll_lock},
    {0, general_drs4_control_help_func}
  };

/************************************************************/

  int drs4_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rDRS4 control commands:\n\r");
      xil_printf("drsmode:    set A[3:0] of both DRS4 chips\n\r");
      xil_printf("drssetreg:  write the registers of both DRS4 chips\n\r");
      xil_printf("drsgetlock: read the pll lock state of both DRS4 chips\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int drs4_set_mode(int arc, char **argv)
  {
    unsigned int adr_val;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: drsmode <mode>\n\r\n\r");
      xil_printf("       mode:   hex value set at A[3:0] of both drs chips.\n\r");
      xil_printf("\n\rDescription: Sets the address bits of the DRS4 chips to the specified value.\n\r");
      xil_printf("             See DRS4 datasheet.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      adr_val = hatoi(argv[1]);
      xil_printf("\n\rSetting DRS4 Mode A=0x%01X\n\r\n\r",adr_val);
      gpio_drs4_cfg_a(adr_val);
    }

    return 0;
  }

/************************************************************/

  int drs4_set_register(int arc, char **argv)
  {
    int reg_val;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: drssetreg <register> <value>\n\r\n\r");
      xil_printf("       register:   register name.\n\r");
      xil_printf("                   rdsr  = Read Shift Register (value specifies position)\n\r");
      xil_printf("                   cfg   = Configuration Register\n\r");
      xil_printf("                   wrsr  = Write Shift Register\n\r");
      xil_printf("                   wrcfg = Write Config Register\n\r");
      xil_printf("       value:      hex register value to write.\n\r");
      xil_printf("\n\rDescription: Writes the corresponding register with the specified value.\n\r");
      xil_printf("             Note: for the rdsr the value represents the position of the\n\r");
      xil_printf("              single '1' written to that register.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      reg_val = hatoi(argv[2]);

      if(fstrcmp(argv[1],"rdsr"))
      {
        xil_printf("\n\rSetting bit %d in read shift register (A=0xB)\n\r\n\r",reg_val);
        gpio_drs4_cfg_set_read_sreg(reg_val);
      }
      else if(fstrcmp(argv[1],"cfg"))
      {
        reg_val |= 0xF8;
        xil_printf("\n\rSetting Config Register (A=0xC) to 0x%02X\n\r\n\r",reg_val);
        gpio_drs4_cfg_transmit(DRS4_ADR_EN_CFG_SREG, reg_val);
      }
      else if(fstrcmp(argv[1],"wrsr"))
      {
        xil_printf("\n\rSetting Write Shift Register (A=0xD) to 0x%02X\n\r\n\r",reg_val);
        gpio_drs4_cfg_transmit(DRS4_ADR_EN_WRITE_SREG, reg_val);
      }
      else if(fstrcmp(argv[1],"wrcfg"))
      {
        xil_printf("\n\rSetting Write Config Register (A=0xE) to 0x%02X\n\r\n\r",reg_val);
        gpio_drs4_cfg_transmit(DRS4_ADR_EN_WRITE_CFG_SREG, reg_val);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"drssetreg help\" for help.\n\r");
        return 0;
      }
    }

    return 0;
  }

/************************************************************/

  int drs4_get_pll_lock(int arc, char **argv)
  {
    int drs_a_lock;
    int drs_b_lock;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: drsgetlock\n\r");
      xil_printf("\n\rDescription: reads the LOCK outputs of the DRS4 chips.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      drs_a_lock  = gpio_drs4_read_pll_lock(GPIO_DRS4_PLLLCK_A_I);
      drs_b_lock  = gpio_drs4_read_pll_lock(GPIO_DRS4_PLLLCK_B_I);

      xil_printf("DRS4 Channel A PLL ");
      if(!drs_a_lock)
      {
        xil_printf("not ");
      }
      xil_printf("locked\n\r");

      xil_printf("DRS4 Channel B PLL ");
      if(!drs_b_lock)
      {
        xil_printf("not ");
      }
      xil_printf("locked\n\r");

      xil_printf("\n\r");
    }

    return 0;
  }

/************************************************************/

  int general_drs4_control_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* DRS4 Control Module:\n\rCan be used to program the shift registers configuring the DRS4 chips\n\r");
    xil_printf("as well as setting the mode of them.\n\r");
    xil_printf("Type \"drs help\" for help on frontend commands.\n\r");
    return 0;
  }

/************************************************************/
