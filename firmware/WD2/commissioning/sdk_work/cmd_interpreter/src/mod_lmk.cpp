//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  05.05.2014 14:11:28
//
//  Description :  Module for SPI access to LMK03000 configurable pll clock
//                 distributor.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_lmk.h"
#include "drv_soft_spi_temp_dac_lmk.h"
//#include "gpio_slow_control.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry lmk_cmd_table[] =
  {
    {"lmk", lmk_help},
    {"lmkrst", lmk_reset},
#if INCLUDE_MOD_SPI_LMK_HIGHLEVEL_FUNCTIONS
    {"lmksetch", lmk_set_channel},
    {"lmkchen", lmk_set_clkout_en},
    {"lmklf", lmk_set_loop_filter},
    {"lmkoif", lmk_set_oscin_freq},
    {"lmksetpll", lmk_set_pll},
    {"lmkset", lmk_set},
#endif
    {"lmksetsync", lmk_set_sync},
    {"lmkgetlock", lmk_get_lock},
    {"lmksetreg", lmk_set_reg},
    {"lmkupload", lmk_upload},
    {0, general_spi_lmk_help_func}
  };

/************************************************************/
  int lmk_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rLMK commands:\n\r");
      xil_printf("lmkrst:     resets the lmk register settings\n\r");
#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
      xil_printf("lmksetch:   apply settings for specific channel\n\r");
      xil_printf("lmkchen:    enables or disables one of the LMK channel outputs\n\r");
      xil_printf("lmklf:      sets the component values for the internal loop filter\n\r");
      xil_printf("lmkoif:     sets the oscillator input frequency of the LMK\n\r");
      xil_printf("lmksetpll:  sets the pll configuration parameters of the LMK\n\r");
      xil_printf("lmkset:     sets single bit register flags\n\r");
#endif
      xil_printf("lmksetreg:  tranmits the 32 bit hex value specified to the LMK\n\r");
      xil_printf("lmkupload:  tranmitts a complete configuration set to the  LMK\n\r");
      xil_printf("lmksetsync: sets the SYNC intput of the LMK\n\r");
      xil_printf("lmkgetlock: reads the LOCK output of the LMK\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }

    return 0;
  }

/************************************************************/

  int lmk_reset(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmkreset\n\r");
      xil_printf("\n\rDescription: Resets the LMK register content.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      lmk03000_reset();
    }

    return 0;
  }

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_channel(int arc, char **argv)
  {
    unsigned char ch_nr;
    unsigned char mux;
    unsigned char divider;
    unsigned char delay;
    unsigned char enable;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmksetch <channel number> <clockout mux> <clockout divider>\n\r");
      xil_printf("          <clockout delay> <channel enable>\n\r\n\r");
      xil_printf("       channel number:   selects channel (range = 0..7)\n\r");
      xil_printf("       clockout mux:     0 = bypassed, 1 = divided, 2 = delayed,\n\r");
      xil_printf("                         3 = divided and delayed\n\r");
      xil_printf("       clockout divider: channel divider value (range = 2..510)\n\r");
      xil_printf("                         (even values only)\n\r");
      xil_printf("       clockout delay:   channel delay value in ps (range = 0..2250)\n\r");
      xil_printf("                         (resolution 150ps)\n\r");
      xil_printf("       channel enable:   0 = disabled, 1 = enabled\n\r");
      xil_printf("\n\rDescription: Sets the configuration for one of the LMK channels.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 6)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      ch_nr   = (unsigned char)(atoi(argv[1]));
      mux     = (unsigned char)(atoi(argv[2]));
      divider = (unsigned char)(atoi(argv[3]) / 2);
      delay   = (unsigned char)(atoi(argv[4]) / 150);
      enable  = (unsigned char)(atoi(argv[5]));

      //Validate arguments range
      if( (ch_nr > 7) || (mux > 3) || (divider == 0) || (delay > 15) )
      {
        xil_printf("Invalid arguments. Type \"lmksetch help\" for help.\n\r");
        return 0;
      }

      //Setup the LMK channel
      lmk03000_set_channel(ch_nr, mux, enable, divider, delay);
    }

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_clkout_en(int arc, char **argv)
  {
    unsigned char ch_nr;
    unsigned char enable;

    ch_nr   = (unsigned char)(atoi(argv[1]));

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmkchen <channel number> <on/off>\n\r\n\r");
      xil_printf("       channel number:   selects channel (range = 0..7)\n\r");
      xil_printf("       on/off:           channel setting on or off\n\r");
      xil_printf("\n\rDescription: Enables or disables one of the LMK channel outputs.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[2],"on"))
      {
        enable = 1;
      }
      else if(fstrcmp(argv[2],"off"))
      {
        enable = 0;
      }
      else
      {
        enable = 2;
      }

      //Validate argument range
      if( (ch_nr > 7) || (enable == 2) )
      {
        xil_printf("Invalid arguments. Type \"lmkchen help\" for help.\n\r");
        return 0;
      }

      lmk03000_set_clkout_en(ch_nr, enable);
    }

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_SPI_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_loop_filter(int arc, char **argv)
  {
    unsigned char r4_lf;
    unsigned char r3_lf;
    unsigned char c3_c4_lf;

    r3_lf    = (unsigned char)(atoi(argv[1]));
    r4_lf    = (unsigned char)(atoi(argv[2]));
    c3_c4_lf = (unsigned char)(atoi(argv[3]));

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmklf <R3> <R4> <C3/C4>\n\r\n\r");
      xil_printf("       R3:      resistor value ID for R3 (range = 0..4)\n\r");
      xil_printf("       R4:      resistor value ID for R4 (range = 0..4)\n\r");
      xil_printf("       C3/C4:   capacitor value ID for C3 and C4 (range = 0..11)\n\r");
      xil_printf("\n\rDescription: Sets the component values for the internal loop filter.\n\r");
      xil_printf("             See LMK datasheet for details.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 4)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }
      //Validate argument range
      if( (r3_lf > 4) || (r4_lf > 4) || (c3_c4_lf > 11) )
      {
        xil_printf("Invalid arguments. Type \"lmkchen help\" for help.\n\r");
        return 0;
      }

      lmk03000_set_loop_filter(r4_lf, r3_lf, c3_c4_lf);
    }

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_oscin_freq(int arc, char **argv)
  {
    unsigned char oscin_freq;

    oscin_freq = (unsigned char)(atoi(argv[1]));

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmkoif <frequency>\n\r\n\r");
      xil_printf("       frequency:   oscillator frequency in multiples of 1 MHz\n\r");
      xil_printf("                    (range = 1..200MHz)\n\r");
      xil_printf("\n\rDescription: Sets the oscillator input frequency of the LMK.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }
      //Validate argument range
      if(oscin_freq > 200)
      {
        xil_printf("Invalid arguments. Type \"lmkoif help\" for help.\n\r");
        return 0;
      }

      lmk03000_set_oscin_freq(oscin_freq);
    }

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set_pll(int arc, char **argv)
  {
    unsigned int pll_r;
    unsigned int pll_n;
    unsigned char vco_div;
    unsigned char pll_mux;
    unsigned char pll_cp_gain;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmksetpll <r divider> <n divider> <vco divider>\n\r");
      xil_printf("          <pll mux> <pll charge pump>\n\r\n\r");
      xil_printf("       r divider:         R divider value (range = 1..4095)\n\r");
      xil_printf("       n divider:         N divider value (range = 1..262143)\n\r");
      xil_printf("       vco divider:       VCO divider value (range = 2..8)\n\r");
      xil_printf("       pll mux:           output mode of the LD pin (range = 0..11)\n\r");
      xil_printf("       pll charge pump:   charge pump gain (range = 0..3)\n\r");
      xil_printf("\n\rDescription: Sets the pll configuration parameters of the LMK.\n\r");
      xil_printf("             See LMK datasheet for details.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 6)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      pll_r       = atoi(argv[1]);
      pll_n       = atoi(argv[2]);
      vco_div     = (unsigned char)(atoi(argv[3]));
      pll_mux     = (unsigned char)(atoi(argv[4]));
      pll_cp_gain = (unsigned char)(atoi(argv[5]));

      //Validate arguments range
      if( (pll_r < 1)    || (pll_r > 4095)    ||
          (pll_n < 1)    || (pll_n > 262143)  ||
          (vco_div < 2)  || (vco_div > 8)     ||
          (pll_mux > 11) || (pll_cp_gain > 3) )
      {
        xil_printf("Invalid arguments. Type \"lmksetch help\" for help.\n\r");
        return 0;
      }

      //Setup the LMK pll
      lmk03000_set_pll_mux(pll_mux);
      lmk03000_set_pll_cp_gain(pll_cp_gain);
      lmk03000_set_vco_div(vco_div);
      lmk03000_set_pll_r(pll_r);
      lmk03000_set_pll_n(pll_n);
    }

    return 0;
  }
#endif

/************************************************************/

#if INCLUDE_MOD_LMK_HIGHLEVEL_FUNCTIONS
  int lmk_set(int arc, char **argv)
  {
    unsigned char enable;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmkset <identifier> <on/off>\n\r\n\r");
      xil_printf("       identifier:   configuration bit id (vboost, div4, fout, clkoutglobal, pwrdn)\n\r");
      xil_printf("       on/off:       configuration bit setting on or off\n\r");
      xil_printf("\n\rDescription: sets single bit register flags.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 3)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[2],"on"))
      {
        enable = 1;
      }
      else if(fstrcmp(argv[2],"off"))
      {
        enable = 0;
      }
      else
      {
        xil_printf("Invalid arguments. Type \"lmkset help\" for help.\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"vboost"))
      {
        lmk03000_set_vboost(enable);
      }
      else if(fstrcmp(argv[1],"div4"))
      {
        lmk03000_set_div4(enable);
      }
      else if(fstrcmp(argv[1],"fout"))
      {
        lmk03000_set_en_fout(enable);
      }
      else if(fstrcmp(argv[1],"clkoutglobal"))
      {
        lmk03000_set_en_clkout_global(enable);
      }
      else if(fstrcmp(argv[1],"pwrdn"))
      {
        lmk03000_set_powerdown(enable);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"lmkset help\" for help.\n\r");
        return 0;
      }
    }

    return 0;
  }
#endif

/************************************************************/
  int lmk_set_reg(int arc, char **argv)
  {
    unsigned int reg_val;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmksetreg <register value>\n\r");
      xil_printf("       register value:   hex representation of the register content\n\r");
      xil_printf("\n\rDescription: tranmits the 32 bit hex value specified to the LMK.\n\r");
      xil_printf("             To be used to write contents of a register in binary format.\n\r");
      xil_printf("             See LMK datasheet for details.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      reg_val = (hatoi(argv[1]));
      xil_printf("Writing 0x%08X to LMK\n\r", reg_val);
      lmk03000_set_reg(reg_val);
      lmk03000_upload_configuration();
    }
    return 0;
  }

/************************************************************/

  int lmk_upload(int arc, char **argv)
  {
    unsigned int reg_val;
    int i,j;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmkupload <lmk configuration file>\n\r");
      xil_printf("       lmk configuration file:   txt file exportet from LMK tool\n\r");
      xil_printf("                                 containing the configuration of all registers\n\r");
      xil_printf("\n\rDescription: tranmits the contents of the text file generated by the lmk tool \n\r");
      xil_printf("             to the LMK.\n\r");
      xil_printf("\n\rNote:        linebreaks have to be replaced by a blank space in the file.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      for(i=3;i<17;i++)
      {
        j = 0;
        while( (argv[i][j]!='x') && (argv[i][j]!='X') )
        {
          j++;
        }
        j++;
        reg_val = hatoi(&argv[i][j]);
        xil_printf("Setting Register 0x%08X\n\r", reg_val);
        lmk03000_set_reg(reg_val);
      }
      xil_printf("Uploading contents to LMK\n\r");
      lmk03000_upload_configuration();
      xil_printf("\n\r");
    }
    return 0;
  }

/************************************************************/

  int lmk_set_sync(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmksetsync <logic level>\n\r");
      xil_printf("       logic level:   value to set sync to (range = 0..1)\n\r");
      xil_printf("\n\rDescription: Sets the SYNC intput of the LMK.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }
      lmk03000_set_sync(atoi(argv[1]));
    }

    return 0;
  }

/************************************************************/

  int lmk_get_lock(int arc, char **argv)
  {
    unsigned char pll_mux;
    unsigned char ld_state;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: lmkgetlock\n\r");
      xil_printf("\n\rDescription: reads the LOCK output of the LMK.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      pll_mux  = lmk03000_get_pll_mux();
      ld_state = lmk03000_get_ld();

      if( ( (pll_mux == 3) && (ld_state == 1) ) ||
          ( (pll_mux == 4) && (ld_state == 0) ) )
      {
        xil_printf("LMK PLL Locked\n\r");
      }
      else if( ( (pll_mux == 3) && (ld_state == 0) ) ||
               ( (pll_mux == 4) && (ld_state == 1) ) )
      {
        xil_printf("LMK PLL  NOT  Locked\n\r");
      }
      else
      {
        xil_printf("LD pin of LMK not configured for\n\rDigital Lock Detect\n\r");
      }
    }

    return 0;
  }

/************************************************************/

  int general_spi_lmk_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* SPI LMK Module:\n\rAllows access to LMK pll clock distributor configuration via SPI.\n\r");
    xil_printf("Type \"lmk help\" for help on lmk commands.\n\r");
    return 0;
  }

/************************************************************/
