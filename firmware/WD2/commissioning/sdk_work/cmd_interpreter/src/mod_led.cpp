//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  14.05.2014 12:47:20
//
//  Description :  Module for controling the onboard led.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_led.h"
#include "gpio_slow_control.h"
#include "xil_printf.h"
#include "utilities.h"
#include "xtime.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry led_cmd_table[] =
  {
    {"led", led_help},
    {"ledset", led_set},
    {"ledtest", led_test},
    {0, general_led_help_func}
  };

/************************************************************/

  int led_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rLED control commands:\n\r");
      xil_printf("ledset:    set the state of the RGB LED\n\r");
      xil_printf("ledtest:   set RGB LED colors one after another\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }
    return 0;
  }

/************************************************************/

  int led_set(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: ledset <color>\n\r\n\r");
      xil_printf("       color:   led color r, g, b, y, m, c, w or off.\n\r");
      xil_printf("                for red, green, blue, yellow, cyan, magenta or white\n\r");
      xil_printf("                respectively.\n\r");
      xil_printf("\n\rDescription: Set all RGB LED colors one after another.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      //Check for minimum number of arguments
      if(arc < 2)
      {
        xil_printf("Too few arguments\n\r");
        return 0;
      }

      if(fstrcmp(argv[1],"off"))
      {
        gpio_sc_set_led(0);
      }
      else if(fstrcmp(argv[1],"r"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_R);
      }
      else if(fstrcmp(argv[1],"g"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_G);
      }
      else if(fstrcmp(argv[1],"b"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_B);
      }
      else if(fstrcmp(argv[1],"y"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_G);
      }
      else if(fstrcmp(argv[1],"m"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_B);
      }
      else if(fstrcmp(argv[1],"c"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_G | GPIO_SC_OUT_LED_B);
      }
      else if(fstrcmp(argv[1],"w"))
      {
        gpio_sc_set_led(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_G | GPIO_SC_OUT_LED_B);
      }
      else
      {
        xil_printf("Invalid arguments. Type \"ledset help\" for help.\n\r");
        return 0;
      }
    }

    return 0;
  }

/************************************************************/

  int led_test(int arc, char **argv)
  {
    unsigned int delay = 2e6;

    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: ledtest\n\r\n\r");
      xil_printf("\n\rDescription: Applies the specified color to the onboard LED.\n\r");
      xil_printf("\n\r");
    }
    else
    {
      gpio_sc_set_led(0);
      usleep(delay);
      xil_printf("LED red (R)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_R);
      usleep(delay);
      xil_printf("LED green (G)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_G);
      usleep(delay);
      xil_printf("LED blue (B)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_B);
      usleep(delay);
      xil_printf("LED yellow (RG)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_G);
      usleep(delay);
      xil_printf("LED magenta (RB)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_B);
      usleep(delay);
      xil_printf("LED cyan (BG)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_G | GPIO_SC_OUT_LED_B);
      usleep(delay);
      xil_printf("LED white (RBG)\n\r");
      gpio_sc_set_led(GPIO_SC_OUT_LED_R | GPIO_SC_OUT_LED_G | GPIO_SC_OUT_LED_B);
      usleep(delay);
      xil_printf("LED off\n\r\n\r");
      gpio_sc_set_led(0);
    }

    return 0;
  }

/************************************************************/

  int general_led_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* LED Control Module:\n\rCan be used to set the RGB LED.\n\r");
    xil_printf("Type \"led help\" for help on led commands.\n\r");
    return 0;
  }

/************************************************************/
