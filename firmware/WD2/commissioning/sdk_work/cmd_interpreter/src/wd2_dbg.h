/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WaveDream2
 *
 *  Author  :  schmid_e, theidel
 *  Created :  31.07.2014 08:14:22
 *
 *  Description :  Debug level output control.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __WD2_DBG_H__
#define __WD2_DBG_H__

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/

#define DBG_LEVEL_NONE     0
#define DBG_LEVEL_ERR      1
#define DBG_LEVEL_WARN     2
#define DBG_LEVEL_INIT     3
#define DBG_LEVEL_INFO     4
#define DBG_LEVEL_ALL      5

#define DBG_LEVEL_DEFAULT  DBG_LEVEL_ALL

#define DBG_ERR   (wd2_dbg_level >= DBG_LEVEL_ERR )
#define DBG_WARN  (wd2_dbg_level >= DBG_LEVEL_WARN)
#define DBG_INIT  (wd2_dbg_level >= DBG_LEVEL_INIT)
#define DBG_INFO  (wd2_dbg_level >= DBG_LEVEL_INFO)
#define DBG_ALL   (wd2_dbg_level >= DBG_LEVEL_ALL )


/* Error Flags: */
//#define ERR_WD2_COM       0x00000001
//#define ERR_WD2_SRAM      0x00000002
//#define ERR_WD2_   0x00000004

/******************************************************************************/
/* external global var                                                        */
/******************************************************************************/

extern unsigned int wd2_dbg_level;

/*
  -- declare global var as follows:
  unsigned int wd2_dbg_level = DBG_LEVEL_DEFAULT;

  -- use as follows
  if (DBG_ERR)  printf("Error in xxxx\n");
*/

/******************************************************************************/
/* function prototypes                                                        */
/******************************************************************************/

void set_error_flag(unsigned int error_flag);

/******************************************************************************/

#endif /* __WD2_DBG_H__ */

/******************************************************************************/

