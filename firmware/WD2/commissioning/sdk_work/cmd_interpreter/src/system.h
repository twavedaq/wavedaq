//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Central control for hardware access.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#ifndef __SYSTEM_H__
#define __SYSTEM_H__


/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include "xparameters.h"
#include "xfs_types.h"

#include "drv_soft_spi_temp_dac_lmk.h"
#include "drv_plb_gpio.h"
#include "plb_spi_master.h"
#include "plb_spi_slave.h"

//#include "cpld_sr_ctrl.h"
//#include "dimax_head_ctrl.h"

//#ifndef PWR_CFG_ONLY  
//#include "spi_flash.h"
//#include "spi_cfg_fctrl.h"
//#include "spi_mac_eeprom.h"
//#include "xsysmon_class.h"
//#include "tmp112.h"
//#include "max6650.h"
//#include "gf_com_master.h"
//#endif

//#ifndef LINUX_COMPILE
//#include "config.h"
//#include "sfp_ctrl.h"
//#include "xil_ll_fifo.h"
//#include "network_if.h"
//#endif

/******************************************************************************/
/* macro definitions                                                          */
/******************************************************************************/


/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/



#define SYSTEM system_ptr

#define FPGA_FC "FC"

#define FPGA_POSITION   FPGA_FC

#define XPAR_XPS_SYSMON_ADC_PPC440_INCLUDE_INTR 0

// static buffer in SODIMM RAM

#define STATIC_RAM_ADDR_ENV0          0x01000000
#define STATIC_RAM_ADDR_ENV1          0x01100000

#define STATIC_RAM_ADDR_READ_BUFFER   0x02000000
#define STATIC_RAM_ADDR_WRITE_BUFFER  0x04000000

#define STATIC_RAM_ADDR_TFTP_BUFFER   0x06000000

//#ifndef LINUX_COMPILE
//extern XFlash FlashInstance_0; /* XFlash Instance. */
//#endif

#define MEM_SIZE_SRAM              0x00400000

//Buffer in SRAM
extern unsigned char *STATIC_READ_BUFF  ;
extern unsigned char *STATIC_WRITE_BUFF ;


//const xfs_u32 GF_SPI_SW = 0;
//const xfs_u32 GF_SPI_SE = 1;
//const xfs_u32 GF_SPI_NW = 2;
//const xfs_u32 GF_SPI_NE = 3;

// Cache
// Enable Cache
// A value of 0x80000000 or 0x40000000 or 0xC0000000 enables the cache for the first 256 MB of memory (0 - 0x7FFFFFF).
// A value of 0x1 or 0x2 or 0x3 enables the cache for the last 256 MB of memory (0xF0000000 - 0xFFFFFFFF).
// if you are migrating software from a PowerPC 405 processor design, be aware that each bit enables 128 MB more of memory for caching.
// e.g. XCache_EnableICache(0x00000001);
// e.g. XCache_EnableDCache(0x00000001);

//const xfs_u32 I_CACHE_REGION = 0x00000001;
//const xfs_u32 D_CACHE_REGION = 0x00000001;


//const xfs_u32 PCB_INFO_HWID            = 0x000F;
//const xfs_u32 PCB_INFO_PCB_TYPE        = 0x00F0;
//const xfs_u32 PCB_INFO_PCB_TYPE_OFFSET = 4;
//const xfs_u32 PCB_INFO_TOP_BOTTOM      = 0x0100;
//
//const xfs_u32 GPIO_CTRL_SFP_ERROR         = 0x0100;
//const xfs_u32 GPIO_CTRL_SFP_LINK_UP       = 0x0080;
//const xfs_u32 GPIO_CTRL_SFP_TX_DISABLE    = 0x0040;
//const xfs_u32 GPIO_CTRL_SFP_RATE_SEL      = 0x0020;
//const xfs_u32 GPIO_CTRL_SFP_LOS           = 0x0010;
//const xfs_u32 GPIO_CTRL_SFP_TX_FAULT      = 0x0008;
//const xfs_u32 GPIO_CTRL_SFP_MOD_DEF0      = 0x0004;
//const xfs_u32 GPIO_CTRL_SFP_IIC_SCL       = 0x0002;  /* SFP_MOD_DEF1 */
//const xfs_u32 GPIO_CTRL_SFP_IIC_SDA       = 0x0001;  /* SFP_MOD_DEF2 */
//
//const xfs_u32 GPIO_SYNC_SFP_ERROR         = 0x0100;
//const xfs_u32 GPIO_SYNC_SFP_LINK_UP       = 0x0080;
//const xfs_u32 GPIO_SYNC_SFP_TX_DISABLE    = 0x0040;
//const xfs_u32 GPIO_SYNC_SFP_RATE_SEL      = 0x0020;
//const xfs_u32 GPIO_SYNC_SFP_LOS           = 0x0010;
//const xfs_u32 GPIO_SYNC_SFP_TX_FAULT      = 0x0008;
//const xfs_u32 GPIO_SYNC_SFP_MOD_DEF0      = 0x0004;
//const xfs_u32 GPIO_SYNC_SFP_IIC_SCL       = 0x0002;  /* SFP_MOD_DEF1 */
//const xfs_u32 GPIO_SYNC_SFP_IIC_SDA       = 0x0001;  /* SFP_MOD_DEF2 */
//
//
//const xfs_u32 TEMP_I2C_GPIO_REG        = 5;
//const xfs_u32 TEMP_I2C_GPIO_BIT_SDA    = 0x0400;
//const xfs_u32 TEMP_I2C_GPIO_BIT_SCL    = 0x0200;
//------------------------------------------------------------------------------
// Temac MDIO Addr
//------------------------------------------------------------------------------


//#define TEMAC_MDIO_ADDR_CTRL_SFP      0x0001
//#define TEMAC_MDIO_ADDR_SYNC_SFP      0x0001


//------------------------------------------------------------------------------
// System GPIO
//------------------------------------------------------------------------------

//// port 0  Soft SPI Temp/DAC/LMK GPIOs
const xfs_u32 GPIO_SOFT_SPI_TEMP_DAC_LMK = 0;
//// port 1  Soft Shift Register GPIOs
const xfs_u32 GPIO_SOFT_SHIFT_REGISTER   = 1;
//// port 2  DRS4 Configuration Interface
const xfs_u32 GPIO_DRS4_CFG_IF           = 2;
//// port 3  Inputs: SELECT_I, INIT_I, PLLLCK_A_I, PLLLCK_B_I, LD_I,
////                 TRG_BUSY_I, TRG_SYNC_I, TRIGGER_I, trg_fe
const xfs_u32 GPIO_SLOWCONTROL_INPUTS    = 3;
//// port 4  Outputs: mscb, SYNC_LMK_O, PWR_CMP_O, TCA_CTRL_O, CAL_CTRL_A_O, CAL_CTRL_B_O,
////                  BUFFER_CTRL_O, CLK_SEL_O, RESET_FPGA_O, LED_R_O, LED_G_O, LED_B_O
const xfs_u32 GPIO_SLOW_CONTROL_OUTPUTS  = 4;
//// port 5  SERDES Inputs
const xfs_u32 GPIO_SERDES_INPUTS         = 5;
//// port 6  SERDES DCB Outputs
const xfs_u32 GPIO_SERDES_DCB_OUTPUTS    = 6;
//// port 7  InOuts:  HV_TX_O, HV_RX_O, HV_CS_O, HV_RES_O, HV_EN_O, HV_MOSI_O, HV_MISO_I,
////                  EXT_TRIG_IN_I, EXT_TRIG_OUT_O
const xfs_u32 GPIO_HV                    = 7;

//// port 0 and 1  SFP GPIOs
//const xfs_u32 GPIO_CTRL_SFP_REG = 0;
//const xfs_u32 GPIO_SYNC_SFP_REG = 1;
//
//// port 2 and 3 :  Giga Frost East and West
//const xfs_u32 GPIO_PORT_GIGA_FROST_EAST    = 2;
//const xfs_u32 GPIO_PORT_GIGA_FROST_WEST    = 3;
//
//const xfs_u32 GPIO_GIGA_PWR_EN_O        = 0x00000020; // O
//const xfs_u32 GPIO_GIGA_CFG_RST_O       = 0x00000010; // O
//const xfs_u32 GPIO_GIGA_CFG_DONE_N_I    = 0x00000008; // I
//const xfs_u32 GPIO_GIGA_CFG_DONE_S_I    = 0x00000004; // I
//const xfs_u32 GPIO_GIGA_PWR_GOOD_I      = 0x00000002; // I
//const xfs_u32 GPIO_GIGA_PRESENT_N_I     = 0x00000001; // I
//
//// port 5 :  Frost CTRL Status
//const xfs_u32 GPIO_CTRL_BOARD_REG = 4;
//
//const xfs_u32 GPIO_PWR_CTRL_KILL_O       = 0x00000200; // O
//
//const xfs_u32 GPIO_RUN_5_0V_O            = 0x00000100; // O
//const xfs_u32 GPIO_PGOOD_5_0V_I          = 0x00000080; // I
//const xfs_u32 GPIO_PGOOD_0_9V_I          = 0x00000040; // I
//const xfs_u32 GPIO_PGOOD_MGT_AVCC_I      = 0x00000020; // I
//const xfs_u32 GPIO_PGOOD_MGT_AVCC_PLL_I  = 0x00000010; // I
//const xfs_u32 GPIO_PGOOD_MGT_AVTT_RX_I   = 0x00000008; // I
//const xfs_u32 GPIO_PGOOD_MGT_AVTT_TX_I   = 0x00000004; // I
//const xfs_u32 GPIO_LED_BP_G_N_O          = 0x00000002; // O
//const xfs_u32 GPIO_LED_BP_R_N_O          = 0x00000001; // O

/******************************************************************************/
/* environment settings                                                       */
/******************************************************************************/

//const unsigned char eth0_default_mac_addr[6] = { 0x00, 0x50, 0xc2, 0x46, 0xd9, 0x00 } ;
//const unsigned char eth0_default_ip_addr[4]  = { 10, 1, 0, 1 } ;
//
//const unsigned char eth1_default_mac_addr[6] = { 0x00, 0x50, 0xc2, 0x46, 0xd9, 0x01 } ;
//const unsigned char eth1_default_ip_addr[4]  = { 10, 0, 0, 1 } ;


/******************************************************************************/
/* class definition                                                           */
/******************************************************************************/

class system_class
{
  public:
    system_class(void);
    ~system_class(void);

    xfs_u32 init(void);

    plb_gpio_class   *gpio_sys;

    plb_spi_master           spi_loop_master;
    plb_spi_salve_settings   spi_loop_slave_settings;
    plb_spi_master           *spi_loop_master_ptr;
    plb_spi_salve_settings   *spi_loop_slave_settings_ptr;

    plb_spi_slave           spi_loop_slave;
    plb_spi_slave           *spi_loop_slave_ptr;

//    void bp_led_blink_yellow_usleep(unsigned int time);
//
//    cpld_sr_ctrl_class    *cpld_sr_ctrl;
//    dimax_head_ctrl_class *dimax_head;
//        
//#ifndef PWR_CFG_ONLY  
//    xps_spi_ctrl_class    *xps_spi_controller;
//    spi_flash_class       *spi_flash;
//    spi_mac_eeprom_class  *spi_mac_eeprom;
//    xsysmon_class         *system_monitor;
//    plb_iic_class         *iic_temp;
//    tmp112_class          *tmp112_front;
//    tmp112_class          *tmp112_sodimm;
//    max6650_class         *max6650_fan_ctrl;
//    gf_com_master_class   *gf_com_master;
//    unsigned int spi_flash_available;
//    unsigned int nw_if_ready;      
//#endif
//
//#ifndef LINUX_COMPILE
//    sfp_ctrl_class        *fctrl_ctrl_sfp;
//    sfp_ctrl_class        *fctrl_sync_sfp;
//    xtemac_class          *xtemac_ctrl;
//    xtemac_class          *xtemac_sync;
//    network_if_class      *nw_if_sync;
//    network_if_class      *nw_if_ctrl;
//    xil_ll_fifo_class     *xil_ll_fifo_temac_ctrl_sfp;
//    xil_ll_fifo_class     *xil_ll_fifo_temac_sync_sfp;
//
//    xfs_u32 init_env(void);
//    void default_env(void);
//    void fw_parse_env(void);
//    
//  
//#endif

};

extern system_class *system_ptr;


/******************************************************************************/
/* System Environment                                                         */
/******************************************************************************/

//#ifndef LINUX_COMPILE
//typedef struct
//{
//  unsigned int   serial_no;
//  char           hostname[MAX_HOSTNAME_LENGTH];
//  unsigned char  eth0_mac_addr[6];
//  unsigned char  eth0_ip_addr[4];
//  unsigned int   eth0_dhcp;
//
//  char           eth1name[MAX_HOSTNAME_LENGTH];
//  unsigned char  eth1_mac_addr[6];
//  unsigned char  eth1_ip_addr[4];
//  unsigned int   eth1_dhcp;
//
//  unsigned int   auto_boot;
//  unsigned int   pwr_on_boot;
////  unsigned int   auto_boot_part;
//} env_data_type;
//
//extern env_data_type env_data;
//#endif

/******************************************************************************/
/* System Status                                                              */
/******************************************************************************/


//#ifndef PWR_CFG_ONLY  
//
//typedef struct
//{
//  short tmp_core;
//  short tmp_core_min;
//  short tmp_core_max;
//
//  short vccint;
//  short vccint_min;
//  short vccint_max;
//
//  short vccaux;
//  short vccaux_min;
//  short vccaux_max;
//
//  short tmp112_0;
//  short tmp112_1;
//  short tmp112_2;
//  short tmp112_3;
//
//  unsigned short sys_status;
//  
//  unsigned char seu_cfg_count;
//  unsigned char seu_cfg_status; // bits tbd
//  unsigned char seu_ram_count;
//  unsigned char seu_ram_status; // bits tbd
// 
//  unsigned char status_valid;   // 1 bit
//
//
//} gf_quadrant_status_type;

/******************************************************************************/

//typedef struct
//{
//  unsigned char board_present;  // 1 bit
//  unsigned char power_enable;   // 1 bit
//  unsigned char power_good;     // 1 bit
//} gf_board_status_type;

/******************************************************************************/

//typedef struct
//{
//  short tmp_core;
//  short tmp_core_min;
//  short tmp_core_max;
//
//  short vccint;
//  short vccint_min;
//  short vccint_max;
//
//  short vccaux;
//  short vccaux_min;
//  short vccaux_max;
//
//  short tmp112_0;
//  short tmp112_1;
//
//  unsigned char seu_cfg_count;
//  unsigned char seu_cfg_status;
//// gpio
//// inputs
//  unsigned char pgood_0_9v;
//  unsigned char pgood_5_0v;
//  unsigned char pgood_mgt_avcc;
//  unsigned char pgood_mgt_avcc_pll;
//  unsigned char pgood_mgt_avtt_rx;
//  unsigned char pgood_mgt_avtt_tx;
//// outputs
//  unsigned char run_5_0v;
//  unsigned char atm_run_apwr;
//  unsigned char atm_run_v3_3_a;
//  unsigned char atm_run_v3_3_x;
//  unsigned char atm_run_vs_vpix_sf;
//
//  unsigned char fpga_cfg_done[4];  // 1 bit
//  
//} gf_frost_ctrl_status_type;

/******************************************************************************/

//typedef struct
//{
//  gf_frost_ctrl_status_type  frost_ctrl;
//  gf_head_status_type        head;
//  gf_board_status_type       gf_board[2];
//  gf_quadrant_status_type    quadrant[4];
//} gf_status_type;

/******************************************************************************/

/******************************************************************************/
/* EYE CAL strucy                                                             */
/******************************************************************************/

//typedef struct
//{
//  unsigned char eye_center[3];
//  unsigned char eye_width[3];
//} gf_eye_cal_type;


/******************************************************************************/

//extern gf_eye_cal_type gf_eye_cal[4];
//extern gf_status_type gf_status;
//
//#endif



/******************************************************************************/

#endif // __SYSTEM_H__
