//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e
//  Created :  26.06.2014 08:22:49
//
//  Description :  Module for testing HV interface connections.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

#include "mod_hv.h"
#include "gpio_looptest.h"
//#include "gpio_slow_control.h"
#include "xil_printf.h"
#include "utilities.h"
#include <stdlib.h>

/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/
  cmd_table_entry hv_cmd_table[] =
  {
    {"hv", hv_help},
    {"hvloop", hv_looptest},
    {0, general_hv_help_func}
  };

/************************************************************/
  int hv_help(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rHV commands:\n\r");
      xil_printf("hvloop: performs a loopback test at the hv interface connector\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("Unknown command\n\r");
    }

    return 0;
  }

/************************************************************/

  int hv_looptest(int arc, char **argv)
  {
    if(fstrcmp(argv[1],"help"))
    {
      xil_printf("\n\rUsage: hvloop <enable debugging>\n\r");
      xil_printf("       enable debugging: if set to \"dbg\", a detailed error report is shown.\n\r");
      xil_printf("                         If omitted, only a summary is shown at the end of the test.\n\r");
      xil_printf("\n\rDescription: Performs a looptest of the hv lines to the corresponding connector.\n\r");
      xil_printf("\n\r         The test drives a counter value to the one half of the hv lines while\n\r");
      xil_printf("\n\r         reading the other half and comparing the received values to the sent values.\n\r");
      xil_printf("\n\r         I.e. a loop adapter has to connected to the wavedream board.\n\r");
      xil_printf("\n\r         Loop connections (MSB to LSB):\n\r");
      xil_printf("\n\r         GMD->HV_RES, HV_TX->HV_EN, HV_RX->HV_MOSI, HV_CS->HV_MISO\n\r");
      xil_printf("\n\r");
    }
    else
    {
      xil_printf("\n\rONE HOT Loop Test:\n\r");
      hv_looptest_oh_oc(ONE_HOT, 1);
      xil_printf("\n\r\n\rONE COLD Loop Test:\n\r");
      hv_looptest_oh_oc(ONE_COLD, 1);
    }

    return 0;
  }

/************************************************************/

  int general_hv_help_func(int arc, char **argv)
  {
    xil_printf("\n\r* HV Module:\n\rControls tests of the IOs at the HV connector.\n\r");
    xil_printf("Type \"hv help\" for help on hv commands.\n\r");
    return 0;
  }

/************************************************************/
