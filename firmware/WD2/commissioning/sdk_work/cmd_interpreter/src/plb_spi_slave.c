/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_SPI_SLAVE_v1_00_a/src/plb_spi_slave.c
* Version:           1.00.a
* Description:       plb_spi_slave Driver Source File
* Date:              Mon Jul 21 12:53:12 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/


/***************************** Include Files *******************************/

#include "plb_spi_slave.h"
#include "xtime.h"
#include "xil_printf.h"
#include "wd2_dbg.h"

/************************** Function Definitions ***************************/

int sspi_init(plb_spi_slave *instance_ptr, unsigned int base_address)
{
  // Set some default values
  instance_ptr->base_address = base_address;
  instance_ptr->is_started   = FALSE;

  // to be changed (funktioniert nur bis max 31 slaves)
  sspi_reset(instance_ptr);
  sspi_reset_rxfifo(instance_ptr);
  sspi_reset_txfifo(instance_ptr);

  instance_ptr->is_started   = TRUE;

  return 1;
}

/***************************************************************************/

int sspi_receive(plb_spi_slave *instance_ptr, unsigned int *rx_buffer_ptr, unsigned int num_words, unsigned int word_timeout)
{
  unsigned int rx_index = 0;
  unsigned int time = 0;

  // check if initialization is done
  if(!instance_ptr->is_started)
  {
    return -1;
  }

  while( (rx_index < num_words) && (time < word_timeout) )
  {
    if(!sspi_rx_fifo_is_empty(instance_ptr))
    {
      rx_buffer_ptr[rx_index] = sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_RXD_REG_OFFSET);
      time = 0;
      rx_index++;
    }
    else
    {
      time++;
    }
  }

  return rx_index;
}

/***************************************************************************/

int sspi_transceive(plb_spi_slave *instance_ptr, unsigned int *tx_buffer_ptr, unsigned int *rx_buffer_ptr, unsigned int num_words, unsigned int word_timeout)
{
  unsigned int rx_index = 0;
  unsigned int tx_index = 0;
  unsigned int time = 0;

  // check if initialization is done
  if(!instance_ptr->is_started)
  {
    return -1;
  }

  while( (!sspi_tx_fifo_is_almost_full(instance_ptr)) && (tx_index < num_words) )
  {
    sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_TXD_REG_OFFSET, tx_buffer_ptr[tx_index]);
    tx_index++;
  }

  while( (rx_index < num_words) && (time < word_timeout) )
  {
    if(!sspi_rx_fifo_is_empty(instance_ptr))
    {
      rx_buffer_ptr[rx_index] = sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_RXD_REG_OFFSET);
      while( (!sspi_tx_fifo_is_almost_full(instance_ptr)) && (tx_index < num_words) )
      {
        sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_TXD_REG_OFFSET, tx_buffer_ptr[tx_index]);
        tx_index++;
      }
      time = 0;
      rx_index++;
    }
    else
    {
      time++;
    }
  }

  return rx_index;
}

/***************************************************************************/

int sspi_tx_fifo_is_full(plb_spi_slave *instance_ptr)
{
  if(sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_STATUS_REG_OFFSET) & PLB_SPI_SLAVE_STATUS_TXFIFO_FULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int sspi_tx_fifo_is_almost_full(plb_spi_slave *instance_ptr)
{
  if(sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_STATUS_REG_OFFSET) & PLB_SPI_SLAVE_STATUS_TXFIFO_AFULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int sspi_rx_fifo_is_empty(plb_spi_slave *instance_ptr)
{
  if(sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_STATUS_REG_OFFSET) & PLB_SPI_SLAVE_STATUS_RXFIFO_EMPTY)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int sspi_rx_fifo_is_almost_empty(plb_spi_slave *instance_ptr)
{
  if(sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_STATUS_REG_OFFSET) & PLB_SPI_SLAVE_STATUS_RXFIFO_AEMPTY)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int sspi_get_status(plb_spi_slave *instance_ptr)
{
  return sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_STATUS_REG_OFFSET);
}

/***************************************************************************/

//void sspi_set_loopback(plb_spi_slave *instance_ptr, int loopback_enable)
//{
//  unsigned int ctrl_reg_val;
//
//  // read control register
//  ctrl_reg_val = sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET);
//  if(loopback_enable)
//  {
//    ctrl_reg_val |=  PLB_SPI_SLAVE_CONFIG_LOOP;
//  }
//  else
//  {
//    ctrl_reg_val &= ~PLB_SPI_SLAVE_CONFIG_LOOP;
//  }
//  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, ctrl_reg_val);
//}
//
///***************************************************************************/

void sspi_reset(plb_spi_slave *instance_ptr)
{
  unsigned int reg_val;

  reg_val = sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET);
  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, reg_val | PLB_SPI_SLAVE_CONFIG_SW_RESET);
  usleep(100);
  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, reg_val & PLB_SPI_SLAVE_CONFIG_SW_RESET);
}

/***************************************************************************/

void sspi_reset_rxfifo(plb_spi_slave *instance_ptr)
{
  unsigned int reg_val;

  reg_val = sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET);
  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, reg_val | PLB_SPI_SLAVE_CONFIG_RXFIFO_RST);
  usleep(100);
  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, reg_val & PLB_SPI_SLAVE_CONFIG_RXFIFO_RST);
}

/***************************************************************************/

void sspi_reset_txfifo(plb_spi_slave *instance_ptr)
{
  unsigned int reg_val;

  reg_val = sspi_mReadReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET);
  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, reg_val | PLB_SPI_SLAVE_CONFIG_TXFIFO_RST);
  usleep(100);
  sspi_mWriteReg(instance_ptr->base_address, PLB_SPI_SLAVE_CONFIG_REG_OFFSET, reg_val & PLB_SPI_SLAVE_CONFIG_TXFIFO_RST);
}

/***************************************************************************/
/***************************************************************************/
