//-------------------------------------------------------------------------------------
//  Paul Scherrer Institut
//-------------------------------------------------------------------------------------
//
//  Project :  WaveDream2
//
//  Author  :  schmid_e, theidel
//  Created :  02.05.2014 13:24:35
//
//  Description :  Capturing commands from terminal.
//
//-------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------

/******************************************************************************/
/* include files                                                              */
/******************************************************************************/

#include <new>

#include "term_cmd_input.h"
#include "xil_printf.h"
#include "xparameters.h"

#include <stdlib.h>
#include "utilities.h"
#include "cmd_processor.h"

#include "xuartlite_l.h"


/******************************************************************************/
/* term_cmd_input_class                                                       */
/******************************************************************************/

term_cmd_input_class::term_cmd_input_class(void)
{
  esc = 0;
  prompt = 1;
  bptr = 0;
}

/******************************************************************************/

term_cmd_input_class::~term_cmd_input_class(void)
{
}

/******************************************************************************/

void term_cmd_input_class::chk_cmd(void)
{
  int len;

  len = get_cmd();
  if (len>0)
  {
     cmd_process(buffer, len);
  }
}

/******************************************************************************/

int term_cmd_input_class::get_cmd(void)
{
  int len = -1;
  static cmd_history_class history(MAX_HISTORY_SIZE);

  char c;

  if (prompt)
  {
    xil_printf("%s > ","WaveDream2");//env_data.name);
    //xfs_local_printf("%s > ",env_data.name);
    prompt=0;
  }


//  if (!XUartLite_mIsReceiveEmpty(STDIN_BASEADDRESS))
  if (!XUartLite_IsReceiveEmpty(STDIN_BASEADDRESS))
  {
//    c = (xfs_u8)XUartLite_mReadReg(STDIN_BASEADDRESS, XUL_RX_FIFO_OFFSET);
    c = (xfs_u8)XUartLite_ReadReg(STDIN_BASEADDRESS, XUL_RX_FIFO_OFFSET);
//      xil_printf("c =   %c    (%d)   bptr=%d \n\r",c,c,bptr);
    if (c==0x01b)  // escape
    {
      esc=1;
    }
    else if (esc)   // small workaround for escape sequences
    {
      if(c=='A') // Arrow Up
      {
        // if further history is available
        // update current command pointer
        // if the end of the history record is not reached
        // replace the cli command by the previous one
        if(history.move_to_older())
        {
          //delete the terminal output and flush the buffer
          while(bptr != 0)
          {
            bptr--;
            xil_printf("\x08 \x08");
          }
          //copy the history command to the buffer
          ncpy(buffer, history.get_current_cmd(), history.get_current_length());
          bptr = history.get_current_length();
          //print the buffer
          buffer[bptr]=0;
          xil_printf("%s",buffer);
        }
        esc=0;
      }
      else if(c=='B') // Arrow Down
      {
        //delete the terminal output and flush the buffer
        while(bptr != 0)
        {
          bptr--;
          xil_printf("\x08 \x08");
        }
        // if later history is available
        // update current command pointer
        if(history.move_to_younger())
        {
          //copy the history command to the buffer
          ncpy(buffer, history.get_current_cmd(), history.get_current_length());
          bptr = history.get_current_length();
          //print the buffer
          buffer[bptr]=0;
          xil_printf("%s",buffer);
        }
        esc=0;
      }
      else if (( (c >= 'C') && (c <='H')) ||(c=='~') || ( (c >= 'Q') && (c <='S'))  )
      {
        esc=0;
      }
    }
    else if (c==0x08)  // backspace
    {
      if(bptr) bptr--;
      xil_printf("\x08 \x08");
    }
    else if ((c==0x0a) || (c==0x0d))
    {
      xil_printf("\n\r");
      buffer[bptr]=0;
      len = bptr;
      bptr = 0;
      if(len > 0) // if there is a command, add it to the history
      {
        history.add_new_cmd(buffer, len);
      }
    }
    else if  ((c >= 32) && (c <= 126))
    {
      if (bptr < RECV_BUFF_LEN)
      {
        xil_printf("%c",c);
        buffer[bptr++]=c;
      }
    }
  }

  if (len >= 0) prompt=1;
  return len;
}

/******************************************************************************/
/* cmd_history_item_class                                                     */
/******************************************************************************/

cmd_history_item_class::cmd_history_item_class(const char *cmd, xfs_u32 len)
{
  length = len;
  command = new (std::nothrow) char[len+1];
  if (!command) return;
  ncpy(command, cmd, len);
  command[len] = '\0';

  younger = NULL;
  older = NULL;
}

/******************************************************************************/

cmd_history_item_class::~cmd_history_item_class()
{
  delete[] command;
}


/******************************************************************************/
/* cmd_history_class                                                          */
/******************************************************************************/

cmd_history_class::cmd_history_class(xfs_u32 max_items)
{
  youngest_cmd_ptr  = NULL;
  current_cmd_ptr = NULL;
  max_nr_of_items = max_items;
  nr_of_items = 0;
}

/******************************************************************************/

cmd_history_class::~cmd_history_class()
{
  cmd_history_item_class* delete_ptr;

  current_cmd_ptr  = youngest_cmd_ptr;
  youngest_cmd_ptr = NULL;
  while(current_cmd_ptr != NULL)
  {
    delete_ptr = current_cmd_ptr;
    current_cmd_ptr = current_cmd_ptr->older;
    delete delete_ptr;
  }
}

/******************************************************************************/

xfs_u32 cmd_history_class::is_empty()
{
  if(youngest_cmd_ptr == NULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

xfs_u32 cmd_history_class::add_new_cmd(const char *cmd, xfs_u32 len)
{
  xfs_u32 history_grows = 1;

  // add the new record in any case if the list is empty yet
  if(is_empty())
  {
    current_cmd_ptr = new (std::nothrow) cmd_history_item_class(cmd, len);
    if (!current_cmd_ptr || !current_cmd_ptr->command) return 0;
    youngest_cmd_ptr = current_cmd_ptr;
    nr_of_items++;
  }
  // only add the new record if it does not match the latest one
  // i.e. avoid double entries
  else if(!fstrcmp(youngest_cmd_ptr->command, cmd))
  {
    current_cmd_ptr = new (std::nothrow) cmd_history_item_class(cmd, len);
    if (!current_cmd_ptr || !current_cmd_ptr->command) return 0;
    current_cmd_ptr->older = youngest_cmd_ptr;
    youngest_cmd_ptr->younger = current_cmd_ptr;
    youngest_cmd_ptr = current_cmd_ptr;
    nr_of_items++;
  }

  // if number of history records reaches the maximum history size,
  // delete oldest history record
  if(nr_of_items >= max_nr_of_items)
  {
    delete_oldest_cmd();
    history_grows = 0;
  }

  current_cmd_ptr = NULL;

  return history_grows;
}

/******************************************************************************/

xfs_u32 cmd_history_class::move_to_youngest()
{
  current_cmd_ptr = youngest_cmd_ptr;
  return is_empty();
}

/******************************************************************************/

xfs_u32 cmd_history_class::move_to_younger()
{
  if(!is_empty())
  {
    if(current_cmd_ptr != NULL)
    {
      if(current_cmd_ptr->younger != NULL)
      {
        current_cmd_ptr = current_cmd_ptr->younger;
        return 1;
      }
      else
      {
        current_cmd_ptr = NULL;
        return 0;
      }
    }
    else
    {
      return 0;
    }
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

xfs_u32 cmd_history_class::move_to_older()
{
  if(!is_empty())
  {
    if(current_cmd_ptr == NULL)
    {
      current_cmd_ptr = youngest_cmd_ptr;
      return 1;
    }
    else if(current_cmd_ptr->older != NULL)
    {
      current_cmd_ptr = current_cmd_ptr->older;
      return 1;
    }
    else
    {
      return 0;
    }
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

const char* cmd_history_class::get_current_cmd()
{
  if(current_cmd_ptr != NULL)
  {
    return current_cmd_ptr->command;
  }
  else
  {
    return NULL;
  }
}

/******************************************************************************/

xfs_u32 cmd_history_class::get_current_length()
{
  if(current_cmd_ptr != NULL)
  {
    return current_cmd_ptr->length;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

void cmd_history_class::delete_oldest_cmd()
{
  current_cmd_ptr = youngest_cmd_ptr;
  while(current_cmd_ptr->older != NULL)
  {
    current_cmd_ptr = current_cmd_ptr->older;
  }
  current_cmd_ptr->younger->older = NULL;
  delete current_cmd_ptr;
  current_cmd_ptr = NULL;
  nr_of_items--;
}

/******************************************************************************/

