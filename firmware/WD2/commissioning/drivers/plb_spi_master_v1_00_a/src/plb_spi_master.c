/*****************************************************************************
* Filename:          /afs/psi.ch/project/genie1414/work/wavedream/firmware/commissioning/drivers/plb_spi_master_v1_00_a/src/plb_spi_master.c
* Version:           1.00.a
* Description:       plb_spi_master Driver Source File
* Date:              Mon Jul 21 12:53:12 2014 (by Create and Import Peripheral Wizard)
*****************************************************************************/


/***************************** Include Files *******************************/

#include "plb_spi_master.h"
#include "xtime.h"

/************************** Function Definitions ***************************/

int mspi_init(plb_spi_master *instance_ptr, unsigned short device_id, unsigned int base_address, unsigned char num_slaves)
{
  // Set some default values
  instance_ptr->base_address = base_address;
  instance_ptr->is_started   = FALSE;
  instance_ptr->num_slaves   = num_slaves;

  // to be changed (funktioniert nur bis max 31 slaves)
  instance_ptr->slave_select_mask     = (1 << instance_ptr->num_slaves) - 1;
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_SLVSEL_REG_OFFSET, instance_ptr->slave_select_mask);
  mspi_reset(instance_ptr);

  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_SLVSEL_REG_OFFSET, PLB_SPI_MASTER_CONFIG_ENABLE);

  instance_ptr->is_started   = TRUE;

  return 1;
}

/***************************************************************************/

int mspi_send(plb_spi_m *instance_ptr, unsigned int *tx_buffer_ptr, plb_spi_salve_settings *slave_setting, unsigned int num_words)
{
  // check if initialization is done
  if(!instance_ptr->is_started)
  {
    return 0;
  }

  // configure master with slave parameters
  mspi_configure_for_slave(instance_ptr, slave_setting);

  // write loop
  for(i=0;i<num_words;i++)
  {
    // wait if fifo is almost full
    while(mspi_tx_fifo_is_almost_full(instance_ptr));
    // write to tx fifo
    mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, tx_buffer_ptr[i]);
  }

  return 1;
}

/***************************************************************************/

void mspi_receive(plb_spi_m *instance_ptr, unsigned int *tx_buffer_ptr, unsigned int *rx_buffer_ptr, unsigned int num_words)
{
  // check if initialization is done
  if(!instance_ptr->is_started)
  {
    return 0;
  }

  // configure master with slave parameters
  mspi_configure_for_slave(instance_ptr, slave_setting);

  // flush rx fifo
  while(!mspi_rx_fifo_is_empty(instance_ptr))
  {
    rx_buffer_ptr[0] = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_RXD_REG_OFFSET);
  }

  // write loop
  for(i=0;i<num_words;i++)
  {
    // wait if fifo is almost full
    while(mspi_tx_fifo_is_almost_full(instance_ptr));
    // write to tx fifo
    mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_TXD_REG_OFFSET, tx_buffer_ptr[i]);
    // wait for rx data
    while(mspi_rx_fifo_is_empty(instance_ptr));
    // read from rx fifo
    rx_buffer_ptr[i] = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_RXD_REG_OFFSET);
  }

  return 1;
}

/***************************************************************************/

void mspi_configure_for_slave(plb_spi_m *instance_ptr, plb_spi_salve_settings *slave_setting)
{
  unsigned int ctrl_reg_val;

  // read control register
  ctrl_reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  // clear configuration bits and fifo resets
  ctrl_reg_val &= ~(PLB_SPI_MASTER_CONFIG_CPOL |
                    PLB_SPI_MASTER_CONFIG_CPHA |
                    PLB_SPI_MASTER_CONFIG_LSB_FIRST |
                    PLB_SPI_MASTER_CONFIG_TXFIFO_RST |
                    PLB_SPI_MASTER_CONFIG_RXFIFO_RST);

  // configure for slave
  if(slave_setting->slave_cpol)
  {
    ctrl_reg_val |= PLB_SPI_MASTER_CONFIG_CPOL;
  }
  if(slave_setting->slave_cpha)
  {
    ctrl_reg_val |= PLB_SPI_MASTER_CONFIG_CPHA;
  }
  if(slave_setting->slave_lsb_first)
  {
    ctrl_reg_val |= PLB_SPI_MASTER_CONFIG_LSB_FIRST;
  }
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, ctrl_reg_val);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_NUMDBITS_REG_OFFSET, slave_setting->slave_data_width);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_SLVSEL_REG_OFFSET, slave_setting->slave_select);
}

/***************************************************************************/

int mspi_tx_fifo_is_full(plb_spi_m *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET) & PLB_SPI_MASTER_STATUS_TXFIFO_FULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_tx_fifo_is_almost_full(plb_spi_m *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET) & PLB_SPI_MASTER_STATUS_TXFIFO_AFULL)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_rx_fifo_is_empty(plb_spi_m *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET) & PLB_SPI_MASTER_STATUS_RXFIFO_EMPTY)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

int mspi_rx_fifo_is_almost_empty(plb_spi_m *instance_ptr)
{
  if(mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET) & PLB_SPI_MASTER_STATUS_RXFIFO_AEMPTY)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/***************************************************************************/

void mspi_set_loopback(plb_spi_m *instance_ptr, int loopback_enable)
{
  // read control register
  ctrl_reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  if(loopback_enable)
  {
    ctrl_reg_val |=  PLB_SPI_MASTER_CONFIG_LOOP;
  }
  else
  {
    ctrl_reg_val &= ~PLB_SPI_MASTER_CONFIG_LOOP;
  }
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, ctrl_reg_val);
}

/***************************************************************************/

void mspi_reset(plb_spi_m *instance_ptr)
{
  unsigned int reg_val;

  reg_val = mspi_mReadReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val_original | PLB_SPI_MASTER_CONFIG_SW_RESET);
  usleep(100);
  mspi_mWriteReg(instance_ptr->base_address, PLB_SPI_MASTER_CONFIG_REG_OFFSET, reg_val_original & PLB_SPI_MASTER_CONFIG_SW_RESET);
}

/***************************************************************************/
/***************************************************************************/
