/********************************************************************\

  Name:         monitor.c
  Created by:   Stefan Ritt


  Contents:     Driver for MAX1253 power monitor

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include <math.h>
#include "mscbemb.h"
#include "cmb.h"

#define CMD_MAN_CONV            0x00
#define CMD_READ_ALARM_REG      0x01
#define CMD_READ_DATA_REG       0x02
#define CMD_READ_ALL_DATA_REG   0x03
#define CMD_READ_CONF_REG       0x04
#define CMD_READ_GLOB_CONF      0x05
#define CMD_RESET               0x07
#define CMD_CLEAR_ALARM         0x08
#define CMD_CLEAR_ALL_ALARM     0x09
#define CMD_WRITE_DATA_REG      0x0a
#define CMD_WRITE_ALL_DATA_REG  0x0b
#define CMD_WRITE_CONF_REG      0x0c
#define CMD_WRITE_GLOB_CONF     0x0d

/*------------------------------------------------------------------*/

/* read 16 bits from MAX1253 power monitor */
void monitor_read(unsigned char cmd, unsigned char addr, unsigned char *pd, unsigned char nbytes)
{
   unsigned char i;
   
   spi_adr(SPI_ADR_ADC, 0);
   spi_write_msb((cmd << 4) | (addr & 0x0F));
   for (i=0 ; i<nbytes ; i++)
      *(pd++) = spi_read_msb();
   spi_adr(0, 1);
}

/*------------------------------------------------------------------*/

/* address certain register in MAX1253 power monitor */
void monitor_address(unsigned char cmd, unsigned char addr)
{
   spi_adr(SPI_ADR_ADC, 0);
   spi_write_msb((cmd << 4) | (addr & 0x0F));
}

/*------------------------------------------------------------------*/

void monitor_deaddress()
{
   spi_adr(0, 1);
}

/*------------------------------------------------------------------*/

/* write single byte to MAX1253 power monitor */
void monitor_write(unsigned char d)
{
   spi_write_msb(d);
}

/*------------------------------------------------------------------*/

void monitor_init()
{
   unsigned char  i;
   unsigned short upper, lower;

   monitor_address(0x07, 0); // Reset

   for (i=0 ; i<10 ; i++) {
      
      if (i == MON_TEMP) {
         monitor_address(CMD_WRITE_DATA_REG, i); // Set data register to zero
         lower = 30/0.125;
         monitor_write(lower >> 4); // 0V
         monitor_write((lower & 0x0F) << 4);
         monitor_deaddress();
      }
      
      monitor_address(CMD_WRITE_CONF_REG, i); // Write channel configuration registers
      
      if (i == MON_TEMP) {
         lower = 0x800; // datasheet, p. 22
         upper = 0x7FF; 
      } else {
         upper = 0xFFF;
         lower = 0x000;
      }

      monitor_write(upper >> 4);
      monitor_write((upper & 0x0F) << 4);
      monitor_write(lower >> 4); 
      monitor_write((lower & 0x0F) << 4);
      monitor_write(0x3A);   // 4 faults, 1024 averaging
      monitor_deaddress();
   }

   monitor_address(CMD_WRITE_GLOB_CONF, 0); // Write global configuration registers
   monitor_write(0xFF); // Enable all channels
   monitor_write(0xFF);
   monitor_write(0x00); // Single ended input
   monitor_write(0x00);
   monitor_write(0x15); // INT push-pull active low, auto scan, internal reference
   monitor_deaddress();
   
   monitor_clear_alarms();

   // set default temperature alarm limit
   monitor_set_alarm_limits(MON_TEMP, 0, 30);
}

/*------------------------------------------------------------------*/

void monitor_set_alarm_limits(unsigned char channel, float lower, float upper)
{
   unsigned short l, u;
   
   monitor_address(CMD_WRITE_CONF_REG, channel); // Write channel configuration registers
      
   if (channel == MON_TEMP) {
      l = lower/0.125;
      u = upper/0.125; 
   } else if (channel == MON_I) {   
      l = 0x000;
      u = 0xFFF;
   } else if (channel == MON_24V) {
      l = (unsigned short) (lower* 2.0/(18+2.0) /2.5*4096.0);
      u = (unsigned short) (upper* 2.0/(18+2.0) /2.5*4096.0);
   } else if (channel >= MON_T1 && channel <= MON_T3) {
      l = lower*0.01 /2.5*4096.0;
      u = upper*0.01 /2.5*4096.0;
   } else {
      l = 0x000;
      u = 0xFFF;
   }

   monitor_write(u >> 4);
   monitor_write((u & 0x0F) << 4);
   monitor_write(l >> 4); 
   monitor_write((l & 0x0F) << 4);
   monitor_write(0x3A);   // 4 faults, 1024 averaging
   monitor_deaddress();
}

/*------------------------------------------------------------------*/

void monitor_clear_alarms()
{
   monitor_address(CMD_CLEAR_ALL_ALARM, 0); // Clear alarm for all channels
   monitor_deaddress();
}

/*------------------------------------------------------------------*/

float monitor_read_adc(unsigned char channel)
{
   unsigned short d;
   float v;
   
   monitor_read(CMD_READ_DATA_REG, channel, (char *)&d, 2); // Read data register
   
   v = (d >> 4)*2.5/4096.0; // convert to V with a 2.5 V reference
   
   switch (channel) {
      case MON_TEMP: 
         v *= 0.125/2.5*4096.0;
         break;
      case MON_VDD:  
         v *= 2.0;
         break;
      case MON_I:
         // v /= 10.0/(1.0+10.0);   // divider 1k / 10k
         v /= 20.0;            // MAX4372T has a gain of 20 V/V
         v = v / 0.005;        // 5 mOhm shunt resitor      
         break;
      case MON_24V:
         v /= 2.0/(18+2.0);   
         break;
      case MON_12V:
         v /= 0.6/(2.4+0.6);   
         break;
      case MON_5V:
         v /= 1.2/(1.3+1.2);
         break;
      case MON_T1:
      case MON_T2:
      case MON_T3:
         v /= 0.010;     // LM35 has 10mV / deg. C 
         break;
   }
   
   // round to two digits
   v = floor(v*100+0.5)/100.0;
   return v;
}

/*------------------------------------------------------------------*/

unsigned long monitor_read_alarm_reg()
{
   unsigned char d[3];
   
   monitor_read(CMD_READ_ALARM_REG, 0, d, 3);
   return ((unsigned long)d[0] << 16) | (d[1] << 8) | d[2];
}
