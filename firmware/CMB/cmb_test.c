/********************************************************************\

  Name:         cmb_test.c
  Created by:   Stefan Ritt

  Contents:     Firmware for WaveDREAM Crate Management Board using
                a SiLabs C8051F120 CPU

  $Id$

\********************************************************************/
      
#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "cmb.h"
#include "git-revision.h"

/*------------------------------------------------------------------*/

void cmb_setup(void)
{
   unsigned short i;            // software timer

   watchdog_disable();          // Disable watchdog timer

   SFRPAGE = CONFIG_PAGE;       // set SFR page

   XBR0 = 0x0C;                 // Enable UART0 and CEX0
   XBR1 = 0x00;
   XBR2 = 0x40;

   // all pins used by the external memory interface are in push-pull mode
   P0MDOUT = 0xFF;
   P1MDOUT = 0xF7;              // P1.3 (uC_DI) is input
   P2MDOUT = 0x7F;              // P2.7 (uC_INT) is input
   P3MDOUT = 0xFF;
   P5MDOUT = 0xFE;              // P5.0 (DI) is input
   P6MDOUT = 0xFF;
   
   
   P0 = 0xE0;                   // /WR, /RD, are high, RESET is high
   P1 = 0x08;                   // P1.3 is input
   P2 = 0x80;                   // P1, P2 contain the address lines, P2.7 is input
   P3 = 0xFF;                   // P3 contains the data lines
   P5 = 0xFF;
   P6 = 0xFF;

   OSCICN = 0x83;               // set internal oscillator to run
                                // at its maximum frequency

   CLKSEL = 0x00;               // Select the internal osc. as
                                // the SYSCLK source

   //Turn on the PLL and increase the system clock by a factor of M/N = 2
   PLL0CN = 0x00;               // Set internal osc. as PLL source
   SFRPAGE = LEGACY_PAGE;
   FLSCL = 0x10;                // Set FLASH read time for 50MHz clk or less
   SFRPAGE = CONFIG_PAGE;
   PLL0CN |= 0x01;              // Enable Power to PLL
   PLL0DIV = 0x01;              // Set Pre-divide value to N (N = 1)
   PLL0FLT = 0x01;              // Set the PLL filter register for
   // a reference clock from 19 - 30 MHz
   // and an output clock from 45 - 80 MHz
   PLL0MUL = 0x02;              // Multiply SYSCLK by M (M = 2)

   for (i = 0; i < 256; i++);   // Wait at least 5us
   PLL0CN |= 0x02;              // Enable the PLL
   // Wait until PLL frequency is locked
   for (i = 0 ; i<50000 && ((PLL0CN & 0x10) == 0) ; i++); 
   CLKSEL = 0x02;               // Select PLL as SYSCLK source

   SFRPAGE = LEGACY_PAGE;

   //EMI0CF = 0xD7;               // Split-Mode, non-multiplexed
                                // on P0-P3 (use 0xF7 for P4 - P7)

   //EMI0TC = 0xB7;               // This value may be modified
                                // according to SYSCLK to meet the
                                // timing requirements for the CS8900A
                                // For example, EMI0TC should be >= 0xB7
                                // for a 100 MHz SYSCLK.

   /* init memory */
   //RS485_ENABLE = 0;

   /* remove reset from CS8900A chip */
   delay_ms(10);
   //CS8900A_RESET = 0;

   /* initialize UART0 */
   uart_init(0, BD_115200);
   PS0 = 1;                     // serial interrupt high priority
                  
   /* initialize PCA0 */
   SFRPAGE   = PCA0_PAGE;
   PCA0CN    = 0x40;
   PCA0CPM0  = 0x42;
   PCA0CPH0  = 0x00; // 100% Duty Cycle
                  
   /* reset system clock */
   // sysclock_reset();
   sysclock_init();

   /* Blink LEDs */
   led_blink(0, 3, 150);
   led_blink(1, 3, 150);
}
 
/*------------------------------------------------------------------*/

struct {
   unsigned char power;
   unsigned char fans;
   unsigned char hv;
   float voltage[4];
   float current;
   float temp[4];
   unsigned char board_power[18];
   unsigned short mscb_addr;
} user_data;

/*------------------------------------------------------------------*/

void cmb_main(void)
{
   unsigned char i;
   
   // initialize the C8051F12x
   cmb_setup();
   spi_init();
   led_clear();
   gpio_init();
   fan_init();
   monitor_init();
   monitor_set_alarm_limits(MON_TEMP, 0, 50);

   memset(&user_data, 0, sizeof(user_data));
   user_data.fans = 10;
   fan_set_speed(user_data.fans);
   for (i=0 ; i<18 ; i++)
      user_data.board_power[i] = 1;
   gpio_board_power(user_data.board_power);
   gpio_main_power(user_data.power);
   hv_power(user_data.hv);
   
   do {
      
      menu();
      
   } while (1);
}

