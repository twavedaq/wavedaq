/********************************************************************\

  Name:         cmb.h
  Created by:   Stefan Ritt

  Contents:     Header file for cmb.c

\********************************************************************/

#ifndef CMB_H
#define CMB_H

typedef struct {  // application data
   unsigned char power;
   unsigned char fans;
   unsigned char hv_on;

   unsigned char error;

   float voltage[4];
   float current;

   float hv_demand;
   float hv_measured;
   float hv_current;

   float temp[4];
   float t_off;

   unsigned char board_power[18];

   unsigned char spi_slot;
} USER_DATA;

#define ERROR_FAN1      1
#define ERROR_FAN2      2
#define ERROR_FAN3      4
#define ERROR_T0        8
#define ERROR_T1       16
#define ERROR_T2       32
#define ERROR_T3       64

typedef struct {  // submaster confiuguration
   char           host_name[20];
   char           password[20];
   unsigned char  eth_mac_addr[6];
   unsigned short magic;
} SUBM_CFG;

typedef struct {  // client configuration
   unsigned int node_addr;
   unsigned int group_addr;
   char node_name[16];
   unsigned short magic;
} CLI_CFG;

/*---- cmb.c ----*/
void flash_write_data(void);

/*---- submaster.c ----*/
void submaster_init(void);
void submaster_yield(void);
void client_yield(void);

/*---- spi.c ----*/
#define SPI_ADR_ADC        0
#define SPI_ADR_SR         1
#define SPI_ADR_DISPLAY    2
#define SPI_ADR_MISOMOSI   3
#define SPI_ADR_HV_DAC     4
#define SPI_ADR_HV_ADC     5
#define SPI_ADR_UNUSED     6
#define SPI_ADR_RST_SR     7

#define WD_SPI_WRITE8      0x11
#define WD_SPI_WRITE16     0x12
#define WD_SPI_WRITE32     0x14
#define WD_SPI_READ8       0x21
#define WD_SPI_READ16      0x22
#define WD_SPI_READ32      0x24
#define WD_SPI_BOARD_ID    0x30
#define WD_SPI_CLEAR       0x40
#define WD_SPI_SYNC        0x50
#define WD_SPI_RESET       0xF0

void spi_init();
void spi_adr(unsigned char adr, unsigned char cs);
void spi_write_lsb(unsigned char d);
void spi_write_msb(unsigned char d);
void spi_write8_lsb(unsigned char adr, unsigned char d);
unsigned char spi_read_msb();
void backplane_spi_cs(unsigned char cs);
void backplane_spi_write_lsb(unsigned char d);
void backplane_spi_write_msb(unsigned char d);
unsigned char backplane_spi_read_msb();

/*---- led.c ----*/
void led_init();
void led_clear();
void led_puts(unsigned char *p);

/*---- gpio.c ----*/
#define GPIO_SW_PREV  (1<<3)
#define GPIO_SW_NEXT  (1<<2)
#define GPIO_SW_INC   (1<<1)
#define GPIO_SW_DEC   (1<<0)

void gpio_init();
void gpio_write(unsigned char device, unsigned char adr, unsigned char d);
unsigned char gpio_read(unsigned char device, unsigned char port);
void gpio_out(unsigned char device, unsigned char port, unsigned char d);
unsigned char gpio_in(unsigned char device, unsigned char port);
unsigned char gpio_read_switches();
unsigned char gpio_read_fan_fault();
unsigned long gpio_read_plugin();
void gpio_main_power(unsigned char flag);
void gpio_board_init(unsigned char flag);
void gpio_board_flash_select(unsigned char flag);
void gpio_board_power(unsigned char flag[18]);
void gpio_board_select(unsigned char b);

/*---- monitor.c ----*/
#define MON_TEMP  0
#define MON_VDD   1
#define MON_I     2
#define MON_24V   3
#define MON_12V   4
#define MON_5V    5
#define MON_T1    7
#define MON_T2    8
#define MON_T3    9

#define MON_AL_T0_HIGH   (1l << 23)
#define MON_AL_T0_LOW    (1l << 22)
#define MON_AL_VDD_HIGH  (1l << 21)
#define MON_AL_VDD_LOW   (1l << 20)

#define MON_AL_CURR_HIGH (1l << 19)
#define MON_AL_CURR_LOW  (1l << 18)
#define MON_AL_24V_HIGH  (1l << 17)
#define MON_AL_24V_LOW   (1l << 16)
#define MON_AL_12V_HIGH  (1l << 15)
#define MON_AL_12V_LOW   (1l << 14)
#define MON_AL_5V_HIGH   (1l << 13)
#define MON_AL_5V_LOW    (1l << 12)

#define MON_AL_T1_HIGH   (1l <<  9)
#define MON_AL_T1_LOW    (1l <<  8)
#define MON_AL_T2_HIGH   (1l <<  7)
#define MON_AL_T2_LOW    (1l <<  6)
#define MON_AL_T3_HIGH   (1l <<  5)
#define MON_AL_T4_LOW    (1l <<  4)

void monitor_init();
float monitor_read_adc(unsigned char channel);
unsigned long monitor_read_alarm_reg();
void monitor_clear_alarms();
void monitor_set_alarm_limits(unsigned char channel, float lower, float upper);

/*---- flash.c ----*/

void flash_block64_erase(unsigned char subadr, unsigned long adr);
void flash_write(unsigned char *buf, unsigned char subadr, unsigned long adr, unsigned short bufsize);
void flash_read(unsigned char *buf, unsigned char subadr, unsigned long adr, unsigned short bufsize);
void flash_read_jedec_id(unsigned char board, unsigned char *b0, unsigned char *b1, unsigned char *b2);

/*---- hv.c ----*/
void hv_init();
void hv_power(unsigned char flag);
float hv_read_voltage();
float hv_read_current();
void hv_set_voltage(float value);

/*---- fan.c ----*/
void fan_init();
void fan_set_speed(unsigned char value);

/*---- menu.c ----*/
void menu_init(void);
void menu_yield(void);

/*---- upgrade.c ----*/
void upgrade(void);

#endif // CMB_H
