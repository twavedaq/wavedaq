/********************************************************************\

  Name:         menu.c
  Created by:   Stefan Ritt

  Contents:     Menu handling with switches and LED display

\********************************************************************/
      
#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include "mscbemb.h"
#include "cmb.h"
#include "git-revision.h"

/*------------------------------------------------------------------*/

// states for state machine
#define ST_DEFAULT  0
#define ST_POWER    1
#define ST_FANS     2
#define ST_HV       3
#define ST_VOLT     4
#define ST_CURR     5
#define ST_TEMP     6
#define ST_BOARD    7
#define ST_TESTSPI  8
#define ST_MSCB     9
#define ST_VERSION  10

#define ST_LAST    ST_VERSION

static unsigned char state;

extern USER_DATA user_data;
extern char host_name[20]; 

extern unsigned long start_time;
extern MSCB_INFO_VAR *variables;

/*------------------------------------------------------------------*/

void menu_init(void)
{
   led_init();
   state = ST_DEFAULT;
}

/*------------------------------------------------------------------*/

void menu_yield(void)
{
   static unsigned char sw_old = 0;
   static unsigned char v_index = 0;
   static unsigned char t_index = 0;
   static unsigned char b_index = 0;
   static unsigned long last = 0;
   static unsigned long sw_time = 0;
   static unsigned long def_time = 0;
   static unsigned char block = 0;

   char str[10], *p;
   unsigned char sw, next, prev, inc, dec, progr;
   unsigned char *ppwr, b0, b1, b2;
   
   // only do ten times per second
   if (time() - last < 10)
      return;
   last = time();   
   
   // evaluate switches
   next = prev = inc = dec = 0;
   sw = gpio_read_switches();
   
   // next item on release OFF/NEXT
   if (!block) {
      if (!(sw & GPIO_SW_PREV) && (sw_old & GPIO_SW_PREV))
         prev = 1;   
      if (!(sw & GPIO_SW_NEXT) && (sw_old & GPIO_SW_NEXT))
         next = 1;   
      if (!(sw & GPIO_SW_INC) && (sw_old & GPIO_SW_INC))
         inc = 1;   
      if (!(sw & GPIO_SW_DEC) && (sw_old & GPIO_SW_DEC))
         dec = 1;  
   }   
   if (sw == 0)
      block = 0;
   
   // fall back to default display
   if (sw != 0)
      def_time = time();
   if (time() > def_time+60*100 && state != ST_TESTSPI)
      state = ST_DEFAULT;
   
   // on/off logic
   if (state == ST_BOARD)
      ppwr = &user_data.board_power[b_index];
   else if (state == ST_HV)
      ppwr = &user_data.hv_on;
   else
      ppwr = &user_data.power;
   
   if ((sw & GPIO_SW_INC) && (*ppwr & 0x01) == 0) {
      if (!(sw_old & GPIO_SW_INC))
         sw_time = time();
      progr = (time() - sw_time) / 10;
      // go in "hold" mode after 0.5 sec
      if (progr >= 5) {
         block = 1; // do not use for next/prev
         sprintf(str, "Hold %c", (progr-5) / 2);
         led_puts(str);
         // switch after 1.6 secs
         if (progr >= 5+15) {
            *ppwr = 1;
            if (state == ST_BOARD)
               gpio_board_power(user_data.board_power);
            else if (state == ST_HV)
               hv_power(1);
            else {
               monitor_clear_alarms();
               gpio_main_power(1);
               user_data.error = 0;
               start_time = time();
            }
         }
         sw_old = sw;
         return;
      }
   }
   if ((sw & GPIO_SW_DEC) && (*ppwr & 0x01) == 1) {
      if (!(sw_old & GPIO_SW_DEC))
         sw_time = time();
      progr = (time() - sw_time) / 10;
      if (progr >= 5) {
         block = 1;
         sprintf(str, "Hold %c", 7-(progr-5) / 2);
         led_puts(str);
         if (progr >= 5+15) {
            *ppwr = 0;
            if (state == ST_BOARD)
               gpio_board_power(user_data.board_power);
            else if (state == ST_HV)
               hv_power(0);
            else {
               monitor_clear_alarms();
               user_data.hv_on = 0;
               hv_power(0);
               gpio_main_power(0);
               user_data.error = 0;
            }
         } 
        sw_old = sw;
        return;
      }
   }
   sw_old = sw;
   
   // next/previous state   
   if (next)
      state = (state + 1) % (ST_LAST+1);
   if (prev)
      state = (state - 1 + (ST_LAST+1)) % (ST_LAST+1);
   
   switch (state) {
      case ST_DEFAULT:
         if (user_data.error == 0) {
            if (user_data.power > 0) {
               /*
               if (user_data.hv_on == 0)
                  led_puts("OK*HVOFF");
               else if (user_data.hv_on > 0)
                  led_puts("OK*HVON ");
               else
               */
                  led_puts(">>>OK<<<");
            } else
               led_puts("--OFF--");
         } else if (user_data.error & (ERROR_T0 | ERROR_T1 | ERROR_T2 | ERROR_T3)) {
            led_puts("OVERTEMP");
         } else if (user_data.error & (ERROR_FAN1 | ERROR_FAN2 | ERROR_FAN3)) {
            sprintf(str, "FANFAIL%bd", user_data.error & 0x07);
            led_puts(str);
         }
      break;
      
      case ST_POWER:
         if (user_data.power > 0)
            led_puts("PWR ON");
         else
            led_puts("PWR OFF");
         break;

      case ST_FANS:
         sprintf(str, "FANS%3bd%%", user_data.fans);
         led_puts(str);
         if (inc)
            user_data.fans = user_data.fans < 100 ? user_data.fans+10 : 100;
         if (dec)
            user_data.fans = user_data.fans > 10 ? user_data.fans-10 : 10;
         fan_set_speed(user_data.fans);
         break;
      
      case ST_HV:
         if (user_data.hv_on > 0)
            led_puts("HV ON");
         else
            led_puts("HV OFF");
         hv_power(user_data.hv_on);
         break;
         
      case ST_VOLT:
         sprintf(str, "U%bd:%4.1fV", v_index, user_data.voltage[v_index]);
         led_puts(str);
         if (inc)
            v_index = (v_index + 1) % 4;
         if (dec)
            v_index = (v_index - 1 + 4) % 4;
         break;
         
      case ST_CURR:
         sprintf(str, "I:%5.2fA", user_data.current);
         led_puts(str);
         break;

      case ST_TEMP:
         sprintf(str, "T%bd:%4.1f\x08", t_index, user_data.temp[t_index]);
         led_puts(str);
         if (inc)
            t_index = (t_index + 1) % 4;
         if (dec)
            t_index = (t_index - 1 + 4) % 4;
         break;
         
      case ST_BOARD:
         sprintf(str, "%s:%s", variables[b_index+17].name, 
                 (user_data.board_power[b_index] & 0x80) ? (user_data.board_power[b_index] & 0x01 ? "ON": "OFF") : "N/A");
         led_puts(str);
         if (inc)
            b_index = (b_index + 1) % 18;
         if (dec)
            b_index = (b_index - 1 + 18) % 18;
         break;
         
      case ST_TESTSPI:
         flash_read_jedec_id(b_index, &b0, &b1, &b2);
         if (b_index == 16)
            sprintf(str, "D:%02bX%02bX%02bX", b0, b1, b2);
         else if (b_index == 17)
            sprintf(str, "T:%02bX%02bX%02bX", b0, b1, b2);
         else
            sprintf(str, "%1bx:%02bX%02bX%02bX", b_index, b0, b1, b2);
         led_puts(str);
         if (inc)
            b_index = (b_index + 1) % 18;
         if (dec)
            b_index = (b_index - 1 + 18) % 18;
         break;         

      case ST_MSCB:
         led_puts(host_name);
         break;       

      case ST_VERSION:
         if (strrchr(GIT_REVISION, '-')) {
            p = strrchr(GIT_REVISION, '-')+2;
            str[0] = '#';
            strncpy(str+1, p, 7);
            led_puts(str);
         } else
            led_puts("--------");
         break;

   }
}

/*------------------------------------------------------------------*/
