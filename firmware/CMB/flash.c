/********************************************************************\

  Name:         flash.c
  Created by:   Stefan Ritt


  Contents:     Driver for W25Q64 SPI flash on WDB and TCB boards

\********************************************************************/

#include <stdio.h>
#include <string.h>
#include <intrins.h>
#include <stdlib.h>
#include <math.h>
#include "mscbemb.h"
#include "cmb.h"

// commands

#define  W25Q64_CMD_WEN               0x06
#define  W25Q64_CMD_VOL_SR_WEN        0x50
#define  W25Q64_CMD_WDE               0x04
#define  W25Q64_CMD_READ_SR1          0x05   // Status Register 1 (S7:S0)
#define  W25Q64_CMD_READ_SR2          0x35   // Status Register 2 (S15:S8)
#define  W25Q64_CMD_WRITE_SR          0x01
#define  W25Q64_CMD_PAGE_PROG         0x02
#define  W25Q64_CMD_SECTOR_ERASE      0x20
#define  W25Q64_CMD_BLOCK32_ERASE     0x52
#define  W25Q64_CMD_BLOCK64_ERASE     0xD8
#define  W25Q64_CMD_CHIP_ERASE        0xC7   // both commands have the same effect
#define  W25Q64_CMD_CHIP_ERASE_ALT    0x60   // both commands have the same effect
#define  W25Q64_CMD_ERASE_PROG_SUSP   0x75
#define  W25Q64_CMD_ERASE_PROG_RESU   0x7A
#define  W25Q64_CMD_POWER_DOWN        0xB9
#define  W25Q64_CMD_READ_DATA         0x03
#define  W25Q64_CMD_FAST_READ         0x0B
#define  W25Q64_CMD_REL_POWER_DOWN    0xAB
#define  W25Q64_CMD_MANUF_DEV_ID      0x90
#define  W25Q64_CMD_JEDEC_ID          0x9F
#define  W25Q64_CMD_UNIQUE_ID         0x4B
#define  W25Q64_CMD_READ_SFDP_REG     0x5A
#define  W25Q64_CMD_ERASE_SECU_REG    0x44
#define  W25Q64_CMD_PROG_SECU_REG     0x42
#define  W25Q64_CMD_READ_SECU_REG     0x48
#define  W25Q64_CMD_EN_QPI            0x38
#define  W25Q64_CMD_EN_RESET          0x66
#define  W25Q64_CMD_RESET             0x99

// status register #1
#define  W25Q64_SR1_BUSY              (1<<0)
#define  W25Q64_SR1_WEL               (1<<1)
#define  W25Q64_SR1_BP0               (1<<2)
#define  W25Q64_SR1_BP1               (1<<3)
#define  W25Q64_SR1_BP2               (1<<4)
#define  W25Q64_SR1_TB                (1<<5)
#define  W25Q64_SR1_SEC               (1<<6)
#define  W25Q64_SR1_SRP0              (1<<7)

// status register #2
#define  W25Q64_SR2_SRP1              (1<<0)
#define  W25Q64_SR2_QE                (1<<1)
#define  W25Q64_SR2_R                 (1<<2)
#define  W25Q64_SR2_LB1               (1<<3)
#define  W25Q64_SR2_LB2               (1<<4)
#define  W25Q64_SR2_LB3               (1<<5)
#define  W25Q64_SR2_CMP               (1<<6)
#define  W25Q64_SR2_SUS               (1<<7)

/*------------------------------------------------------------------*/

void flash_block64_erase(unsigned char subadr, unsigned long adr)
{
   unsigned char board, status;
   
   board = subadr;
   backplane_spi_cs(0);
   gpio_board_flash_select(1);
   gpio_board_select(board);

   // write enable   
   backplane_spi_write_msb(W25Q64_CMD_WEN);
   gpio_board_select(0xFF);

   gpio_board_select(board);
   backplane_spi_write_msb(W25Q64_CMD_BLOCK64_ERASE);
   backplane_spi_write_msb((adr >> 16) & 0xFF); // 23 - 16
   backplane_spi_write_msb((adr >>  8) & 0xFF); // 15 -  8
   backplane_spi_write_msb((adr)       & 0xFF); //  8 -  0
   gpio_board_select(0xFF);

   gpio_board_select(board);
   backplane_spi_write_msb(W25Q64_CMD_READ_SR1);
   do {
      status = backplane_spi_read_msb();
   } while (status & W25Q64_SR1_BUSY);
   gpio_board_select(0xFF);

   gpio_board_flash_select(0);
   backplane_spi_cs(1);
}

/*------------------------------------------------------------------*/

void flash_write(unsigned char *buf, unsigned char subadr, unsigned long adr, unsigned short bufsize)
{
   unsigned char board, status;
   unsigned short i;
   
   board = subadr;
   backplane_spi_cs(0);
   gpio_board_flash_select(1);
   gpio_board_select(board);

   // write enable   
   backplane_spi_write_msb(W25Q64_CMD_WEN);
   gpio_board_select(0xFF);
   
   gpio_board_select(board);
   backplane_spi_write_msb(W25Q64_CMD_PAGE_PROG);
   backplane_spi_write_msb((adr >> 16) & 0xFF); // 23 - 16
   backplane_spi_write_msb((adr >>  8) & 0xFF); // 15 -  8
   backplane_spi_write_msb((adr)       & 0xFF); //  8 -  0
   
   for (i=0 ; i<bufsize ; i++)
      backplane_spi_write_msb(buf[i]);
   
   gpio_board_select(0xFF);

   gpio_board_select(board);
   backplane_spi_write_msb(W25Q64_CMD_READ_SR1);
   do {
      status = backplane_spi_read_msb();
   } while (status & W25Q64_SR1_BUSY);
   gpio_board_select(0xFF);

   gpio_board_flash_select(0);
   backplane_spi_cs(1);
}

/*------------------------------------------------------------------*/

void flash_read(unsigned char *buf, unsigned char subadr, unsigned long adr, unsigned short bufsize)
{
   unsigned char board;
   unsigned short i;
   
   board = subadr;
   backplane_spi_cs(0);
   gpio_board_flash_select(1);
   gpio_board_select(board);
   
   backplane_spi_write_msb(W25Q64_CMD_READ_DATA);
   backplane_spi_write_msb((adr >> 16) & 0xFF); // 23 - 16
   backplane_spi_write_msb((adr >>  8) & 0xFF); // 15 -  8
   backplane_spi_write_msb((adr)       & 0xFF); //  8 -  0
   
   for (i=0 ; i<bufsize ; i++)
      buf[i] = backplane_spi_read_msb();
   
   gpio_board_select(0xFF);
   gpio_board_flash_select(0);
   backplane_spi_cs(1);
}

/*------------------------------------------------------------------*/

void flash_read_jedec_id(unsigned char board, unsigned char *b0, unsigned char *b1, unsigned char *b2)
{
   backplane_spi_cs(0);
   gpio_board_flash_select(1);
   gpio_board_select(board);
   
   backplane_spi_write_msb(W25Q64_CMD_JEDEC_ID);
   *b0 = backplane_spi_read_msb();
   *b1 = backplane_spi_read_msb();
   *b2 = backplane_spi_read_msb();
   
   gpio_board_select(0xFF);
   gpio_board_flash_select(0);
   backplane_spi_cs(1);
}
