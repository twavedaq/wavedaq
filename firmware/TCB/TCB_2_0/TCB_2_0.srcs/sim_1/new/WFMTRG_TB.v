`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/05/2016 10:01:28 AM
// Design Name: 
// Module Name: WFMTRG_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WFMTRG_TB();
    reg CLK_t;
    reg [255:0] WDDATA_t;
    reg [23:0] QHTHR_t;
    wire ISTRG_t;
    wire [23:0] SUM_t;
    wire [3:0] MAX_t;
    wire [20:0] MAXWFM_t;


    WFMTRG WFMTRGSIM(
        .CLK(CLK_t),
        .FCDATA(WDDATA_t),
        .SUM(SUM_t),
        .MAX(MAX_t),
        .MAXWFM(MAXWFM_t)
        ) ;


    initial begin
           CLK_t = 0;
    WDDATA_t[1023:0] = 0;
    end
    
    always 
        #5 CLK_t = !CLK_t;

    initial begin
    #20
    WDDATA_t <= 0;
    
    end
    

endmodule
