`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.04.2017 18:19:57
// Design Name: 
// Module Name: PEAKFINDER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PEAKFINDER(
    input CLK,
    input [WFMSIZE-1:0] MAXIN,
    input [(WFMSIZE*OTHERSSIZE)-1:0] OTHERS,
    output reg ISMAX
    );
    
    parameter OTHERSSIZE=3;
    parameter WFMSIZE=28;
    
    wire [OTHERSSIZE-1:0] OTHERCMP;
    
    genvar iCMP;
    for (iCMP=0; iCMP<OTHERSSIZE; iCMP = iCMP+1) begin
        assign OTHERCMP[iCMP] = ($signed(MAXIN) > $signed(OTHERS[WFMSIZE*iCMP+:WFMSIZE]));
    end
    
    always @(posedge CLK) begin
        ISMAX <= &OTHERCMP;
    end
    
endmodule
