`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08.03.2017 11:19:19
// Design Name: 
// Module Name: TCTrackFinder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TCTrackFinder2(
    input CLK,
    input [nTracks-1:0] TRACKHITINPUT,
    input [8*nTracks-1:0] TRACKTILEIDINPUT,
    input [5*nTracks-1:0] TRACKTILETIMEINPUT,
    input [31:0] TCOMPH,
    input [31:0] TCOMPL,
    output reg [1:0] TRACKHIT,
    output reg [8*2-1:0] TRACKTILEID,
    output reg [5*2-1:0] TRACKTILETIME
    );
    parameter nTracks = 4;

    // REGISTER THE INFO FOR THE SECOND COMPARISON
    reg [nTracks-1:0] TRACKHITREG;
    reg [5*nTracks-1:0] TRACKTILETIMEREG;
    reg [8*nTracks-1:0] TRACKTILEIDREG;
    
    always @(posedge CLK) begin
        TRACKHITREG <= TRACKHITINPUT;
        TRACKTILEIDREG <= TRACKTILEIDINPUT;
        TRACKTILETIMEREG <= TRACKTILETIMEINPUT;
    end

    //OUTPUT OF FIRST HIT SEARCH 
    reg [7:0] TILEIDTMP;
    reg [4:0] TILETIMETMP;
    reg HITTMP;
    
    //COMPUTE TIME DIFFERENCES
    integer iTrack;
    reg OLDHIT;
    reg [5:0] OLDTIME;
    reg [6*nTracks-1:0] TRACKTIMEDIFF;
    always @(*) begin
       for(iTrack=nTracks-1; iTrack>=0; iTrack=iTrack-1) begin
          TRACKTIMEDIFF[6*iTrack+:6] = OLDTIME - {1'b0,TRACKTILETIMEINPUT[5*iTrack+:5]};
       end
    end

    //FIRST SEARCH THE FIRST HIT
    always @(posedge CLK) begin
        HITTMP <= 1'b0;
        TILEIDTMP <= 0;
        TILETIMETMP <= 0;
        OLDHIT <= 1'b0;
        OLDTIME <= 0;
    
       for(iTrack=nTracks-1; iTrack>=0; iTrack=iTrack-1) begin
            if (TRACKHITINPUT[iTrack] & (~OLDHIT | ($signed(TRACKTIMEDIFF[6*iTrack+:6])>=$signed(TCOMPH[5:0])))) begin
                TILEIDTMP <= TRACKTILEIDINPUT[iTrack*8+:8];
                TILETIMETMP <= TRACKTILETIMEINPUT[iTrack*5+:5];
                HITTMP <= 1'b1; 
                OLDTIME <= {1'b0, TRACKTILETIMEINPUT[iTrack*5+:5]} + 6'b10000;
                OLDHIT <= 1'b1;
            end
        end
    end

    //COMPUTE TIME DIFFERENCES
    reg [6*nTracks-1:0] TILETIMEDIFF1;
    always @(*) begin
       for(iTrack=nTracks-1; iTrack>=0; iTrack=iTrack-1) begin
          TILETIMEDIFF1[6*iTrack+:6] = {1'b0,TILETIMETMP} - {1'b0,TRACKTILETIMEREG[5*iTrack+:5]};
       end
    end

    //THEN SEARCH FOR A SECOND HIT LOOKING AT THE TIME VALUES
    always @(posedge CLK) begin
       // VALIDATE FIRST HIT   
       TRACKTILEID[7:0] <= TILEIDTMP;
       TRACKTILETIME[4:0] <= TILETIMETMP;
       TRACKHIT[0] <= HITTMP;

       // LOOK FOR A SECOND TRACK
       TRACKTILEID[15:8] <= 0;
       TRACKTILETIME[9:5] <= 0;
       TRACKHIT[1] <= 1'b0;
       for(iTrack=nTracks-1; iTrack>=0; iTrack=iTrack-1) begin
          if (HITTMP & TRACKHITREG[iTrack] & ($signed(TILETIMEDIFF1[6*iTrack+:6])>=$signed(TCOMPL[5:0]))) begin
             TRACKTILEID[15:8] <= TRACKTILEIDREG[iTrack*8+:8];
             TRACKTILETIME[9:5] <= TRACKTILETIMEREG[iTrack*5+:5];
             TRACKHIT[1] <= 1'b1; 
          end
       end
    end
    
endmodule


