`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 19.04.2017 18:01:25
// Design Name: 
// Module Name: ALFATRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ALFATRG(
    input CLK,
    input ALGCLK,
    input [WFMSIZE-1:0] PMTSUM,
    input [31:0] THR,
    input [31:0] SCALE,
    output [WFMSIZE+3-1:0] CHARGE,
    output reg [WFMSIZE-1:0] PEAK,
    output [47:0] QOVERA,
    output [3:0] ISMAXBITS,
    output TRG
    );
    
    parameter SHIFTREGSIZE = 8;
    parameter PRESAMPLES = 3;
    parameter WFMSIZE = 30;
    parameter MAXPOS = 6;
    
    reg [WFMSIZE*PRESAMPLES-1:0] PRESAMPLE_SHIFT;
    reg [WFMSIZE*SHIFTREGSIZE-1:0] PMTSUM_SHIFT;
    reg [WFMSIZE*SHIFTREGSIZE-1:0] PMTSUM_LATCH;
    reg [WFMSIZE*(SHIFTREGSIZE+PRESAMPLES)-1:0] PMTSUM_COMPARE;
    wire TRG_PROMPT;
    wire ISMAX;
    
   // SHIFT REGISTER FOR PMTSUM AND LATCH
   always @(posedge CLK) begin
        PMTSUM_SHIFT <= {PMTSUM_SHIFT[WFMSIZE*(SHIFTREGSIZE-1)-1:0], PMTSUM};
        PRESAMPLE_SHIFT <= {PRESAMPLE_SHIFT[WFMSIZE*(PRESAMPLES-1)-1:0], PMTSUM_SHIFT[WFMSIZE*(SHIFTREGSIZE-1)+:28]};
        if(ISMAX==1'b1) begin
            PMTSUM_LATCH <= PMTSUM_SHIFT;
        end
    end
    
    // BUILD LIST OF VALUES TO COMPARE WITH
    always @(*) begin
        PMTSUM_COMPARE  = {PRESAMPLE_SHIFT, PMTSUM_SHIFT};
        PMTSUM_COMPARE[WFMSIZE*MAXPOS+:WFMSIZE] = THR[WFMSIZE-1:0];
    end
    
    // LOOK FOR MAX
    PEAKFINDER #(
        .OTHERSSIZE(SHIFTREGSIZE+PRESAMPLES),
        .WFMSIZE(WFMSIZE)
    ) PEAKFINDERBLOCK(
        .CLK(ALGCLK),
        .MAXIN(PMTSUM_SHIFT[WFMSIZE*MAXPOS+:WFMSIZE]),
        .OTHERS(PMTSUM_COMPARE),
        .ISMAX(ISMAX)
    );
   
   //SUM ALFAS
    ALFASUM #(
        .WFMSIZE(WFMSIZE)
    )
    ALFASUMBLOCK(
        .CLK(ALGCLK),
        .WFM(PMTSUM_LATCH),
        .SUM(CHARGE)
    );
       
       
    reg [2:0] ISMAXDLY;
    //DELAY TO BE IN TIME WITH CHARGE COMPUTATION
    always @(posedge ALGCLK) begin
        PEAK <= PMTSUM_LATCH[WFMSIZE*MAXPOS+:WFMSIZE];
        ISMAXDLY[2:0] <= {ISMAXDLY[1:0],ISMAX};
    end

   assign ISMAXBITS = {ISMAXDLY[2:0],ISMAX};
    
    ALFATRGGEN #(
        .WFMSIZE(WFMSIZE)
    ) ALFATRGGENBLOCK(
        .CLK(ALGCLK),
        .PEAK(PEAK),
        .CHARGE(CHARGE),
        .ISMAX(ISMAXDLY[2]),
        .SCALE(SCALE),
        .QOVERA(QOVERA),
        .TRG(TRG_PROMPT)
   );

    SHAPER TRG_SH(
        .DIN(TRG_PROMPT),
        .DOUT(TRG),
        .CLK(ALGCLK)
    );
    
endmodule
