`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.10.2016 10:20:50
// Design Name: 
// Module Name: SERDES_SLAVE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module SERDES_SLAVE(
    //SERDES CONNECTIONS
    input SERDES_CLK,
    input CLK,
    input SYNC,
    input [7:0] TCB_RTX_D_P,
    input [7:0] TCB_RTX_D_N,
    input [7:0] TCB_RTX1_D_P,
    input [7:0] TCB_RTX1_D_N,
    input [7:0] TCB_RTX2_D_P,
    input [7:0] TCB_RTX2_D_N,
    input [7:0] TCB_RTX3_D_P,
    input [7:0] TCB_RTX3_D_N,
    output [255:0] FCDATA,
    output [7:0] WDB_TX_D_P,
    output [7:0] WDB_TX_D_N,
    input [63:0] BPDATA,
    output [1:0] DCB_TX_D_P,
    output [1:0] DCB_TX_D_N,
    input  [1:0] DCB_RX_D_P,
    input  [1:0] DCB_RX_D_N,
    output [15:0] DCBDATA,
    //READOUT OF SERDES STATUS and MASKING
    output [`NSERDESFP*8*5-1:0] FCCURRENTDLY,
    output [`NSERDESFP*8*3-1:0] FCCURRENTBISTLIP,
    output [2*5-1:0] DCBCURRENTDLY,
    output [2*3-1:0] DCBCURRENTBISTLIP,
    input [`NSERDESFP-1:0] FCSERDESMASK,
    //SOFTWARE SERDES CONFIGURATION
    input RELOADDLY,
    input CHECKGLBRESET,
    input CHECKGLBENABLE,
    input [8*5*`NSERDESFP-1:0] FCDLY,
    input [8*`NSERDESFP-1:0] FCBITSLIP,
    input [63:0] CHECKVALUE,
    output [8*`NSERDESFP-1:0] CHECKERROR,
    output [32*8*`NSERDESFP-1:0] CHECKCOUNTERS,
    output [31:0] CHECKTIMER,
    input [`NSERDESFP*8-1:0] FCSERDESRESET,
    input [7:0] BPSERDESRESET,
    input [3:0] DCBSERDESRESET,
    input [9:0] DCBDLY,
    input [1:0] DCBBITSLIP,
    output [1:0] DCBCHECKERROR,
    output [63:0] DCBCHECKCOUNTERS,
    //AUTOMATIC SERDES CALIBRATION
    input CALIBSTART,
    input RESET_CALIBFSM,
    input PATTERNSERDES,
    input CALIBMASK,
    input FCSERDESMASKENABLE,
    output [`NSERDESFP-1:0] FCCALIBBUSY,
    output [`NSERDESFP-1:0] FCCALIBFAIL,
    output FCALIGNBUSY,
    output FCALIGNFAIL,
    output [`NSERDESFP*4-1:0] FCALIGNOFFSET,
    output [4:0] FCSERDESMIN,
    output [`NSERDESFP-1:0] FCALIGNDLY,
    output DCBCALIBBUSY,
    output DCBCALIBFAIL,
    output [(`NSERDESFP)*32-1:0] FCDLY_TESTED,
    output [(`NSERDESFP)*32-1:0] FCDLY_STATE,
    output [31:0] DCBDLY_TESTED,
    output [31:0] DCBDLY_STATE
    );
    
    wire [8*`NSERDESFP-1:0] INPUT_P;
    wire [8*`NSERDESFP-1:0] INPUT_N;
    
    assign INPUT_P = {TCB_RTX3_D_P, TCB_RTX2_D_P, TCB_RTX1_D_P, TCB_RTX_D_P};
    assign INPUT_N = {TCB_RTX3_D_N, TCB_RTX2_D_N, TCB_RTX1_D_N, TCB_RTX_D_N};
    
    wire [`NSERDESFP-1:0] FCSERDESMASKLOCAL;
      
    //INPUT FROM Front Panel Connector
    genvar iSerdes;
    for(iSerdes=0; iSerdes<`NSERDESFP; iSerdes=iSerdes+1) begin
        INPUTSERDES #(
            .nLink(8),
            .negPolarity(1)//account for negative polarity on FCI Cable
        ) IN(
            .DATA(FCDATA[64*iSerdes+:64]),
            .CLK(SERDES_CLK),
            .CLKDIV(CLK),
            .IN_P(INPUT_P[8*iSerdes+:8]),
            .IN_N(INPUT_N[8*iSerdes+:8]),
            .DATAMASK(FCSERDESMASKLOCAL[iSerdes]|FCSERDESMASK[iSerdes]),
            .CALIBSTART(CALIBSTART),
            .RESET_FSM(RESET_CALIBFSM),
            .FORCE_RESET(FCSERDESRESET[iSerdes*8+:8]),
            .FORCE_BITSLIP(FCBITSLIP[8*iSerdes+:8]),
            .FORCE_DLY(FCDLY[40*iSerdes+:40]),
            .FORCE_LOADDLY(RELOADDLY),
            .BUSY(FCCALIBBUSY[iSerdes]),
            .FAIL(FCCALIBFAIL[iSerdes]),
            .CHECKVALUE(CHECKVALUE),
            .CHECKGLBRESET(CHECKGLBRESET),
            .CHECKGLBENABLE(CHECKGLBENABLE),
            .CHECKERROR(CHECKERROR[8*iSerdes+:8]),
            .CHECKCOUNTERS(CHECKCOUNTERS[32*8*iSerdes+:32*8]),
            .REGENABLE(FCALIGNDLY[iSerdes]),
            .DLY_APPLIED(FCCURRENTDLY[5*8*iSerdes+:5*8]),
            .BISTLIP_APPLIED(FCCURRENTBISTLIP[3*8*iSerdes+:3*8]),
            .DLY_TESTED(FCDLY_TESTED[32*iSerdes+:32]),
            .DLY_STATE(FCDLY_STATE[32*iSerdes+:32]),
            .CALIBMASK(CALIBMASK)
        );
    end
    
    //FSM to align serdes
    SERDESALIGNFSM #(
        .nSerdes(`NSERDESFP)
    ) FSM(
        .SERDESDATA(FCDATA),
        .SERDESBUSY(FCCALIBBUSY),
        .SERDESFAIL(FCCALIBFAIL),
        .CLK(CLK),
        .START(CALIBSTART),
        .RESET_FSM(RESET_CALIBFSM),
        .MASKENABLE(FCSERDESMASKENABLE),
        .SERDESMASK(FCSERDESMASKLOCAL),
        .SERDESDLY(FCALIGNDLY),
        .SERDESOFFSET(FCALIGNOFFSET),
        .SERDESMIN(FCSERDESMIN),
        .BUSY(FCALIGNBUSY),
        .FAIL(FCALIGNFAIL)
        );
      
    //OUTPUT TO BACKPLANE
    OUTPUTSERDES #(
        .nLink(8)
    ) OUT(
        .OUT_P(WDB_TX_D_P),
        .OUT_N(WDB_TX_D_N),
        .CLK(SERDES_CLK),
        .CLKDIV(CLK),
        .DATA(BPDATA),
        .RESET(BPSERDESRESET),
        .SYNC(SYNC),
        .PATTERN_ENA(PATTERNSERDES)
    );
    
        //OUTPUT TO DCB 
    OUTPUTSERDES #(
        .nLink(2)
    ) TO_DCB(
        .OUT_P(DCB_TX_D_P),
        .OUT_N(DCB_TX_D_N),
        .CLK(SERDES_CLK),
        .CLKDIV(CLK),
        .DATA(CHECKVALUE[15:0]),
        .RESET(DCBSERDESRESET[2+:2]),
        .SYNC(SYNC),
        .PATTERN_ENA(PATTERNSERDES)
    );

    //INPUT FROM DCB
    INPUTSERDES #(
        .nLink(2)
    ) FROM_DCB (
        .DATA(DCBDATA),
        .CLK(SERDES_CLK),
        .CLKDIV(CLK),
        .IN_P(DCB_RX_D_P),
        .IN_N(DCB_RX_D_N),
        .DATAMASK(16'b0),
        .CALIBSTART(CALIBSTART),
        .RESET_FSM(RESET_CALIBFSM),
        .FORCE_RESET(DCBSERDESRESET[1:0]),
        .FORCE_BITSLIP(DCBBITSLIP),
        .FORCE_DLY(DCBDLY),
        .FORCE_LOADDLY(RELOADDLY),
        .BUSY(DCBCALIBBUSY),
        .FAIL(DCBCALIBFAIL),
        .CHECKVALUE(CHECKVALUE[15:0]),
        .CHECKGLBRESET(CHECKGLBRESET),
        .CHECKGLBENABLE(CHECKGLBENABLE),
        .CHECKERROR(DCBCHECKERROR),
        .CHECKCOUNTERS(DCBCHECKCOUNTERS),
        .REGENABLE(16'b0),
        .DLY_APPLIED(DCBCURRENTDLY),
        .BISTLIP_APPLIED(DCBCURRENTBISTLIP),
        .DLY_TESTED(DCBDLY_TESTED),
        .DLY_STATE(DCBDLY_STATE),
        .CALIBMASK(CALIBMASK)
    );
    
    reg [31:0] CHECKCOUREG;
    // CHECK TIME COUNTER
    always @(posedge CLK) begin
        if(CHECKGLBRESET) begin
           CHECKCOUREG <= 32'b0; 
        end else if(CHECKGLBENABLE) begin
            CHECKCOUREG <=  CHECKCOUREG +1;
        end
    end
    
    assign CHECKTIMER = CHECKCOUREG[31:0];
    
endmodule
