`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 05.11.2015 16:45:08
// Design Name: 
// Module Name: SHAPESELECT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SHAPESELECT(
    input CLK,
    input SELECT,
    output reg LDTACK
    );
//HERE SOME LOGIC TO EXTEND THE SELECT SIGNAL FOR THE DTACK LED
reg [23:0] COUSEL;
reg ENDSEL;
always @(posedge CLK) begin
    if(SELECT) begin
        LDTACK <=1 ;
    end    
    if(LDTACK) begin
        COUSEL <= COUSEL+1;
    end
    if(&COUSEL==1) begin
        LDTACK <=0;
        COUSEL <=0;
    end
end    
endmodule
