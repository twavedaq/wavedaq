`timescale 1ns / 1ps
// here all the MMCM definitions
// MMCM connections example

module ALLMMCM(
    clk_in_p, clk_in_n,                //bus of clk inputs
    clk, pedclk, slowclk, //bus of clk output
    reset, locked,fastclk,algclk,resetdlyctrl,        // 
    dlyctrlres
    );
    
    input [3:0] clk_in_p, clk_in_n; 
    output [1:0] clk, fastclk, algclk;
    output [3:0] locked;
    input reset; 
    output pedclk;
    wire refclk;
    output slowclk;
    input resetdlyctrl;
    output dlyctrlres;
    
//3 MMCM instantiation, by now all with the same configuration 
MMCM_TX  PLL0
(// Clock in ports
   .clk_in1_p(clk_in_p[0]),
   .clk_in1_n(clk_in_n[0]),
  // Clock out ports  
   .clk(clk[0]),         
   .fastclk(fastclk[0]),  
   .algclk(algclk[0]),  
  // Status and control signals                
   .reset(reset),
   .locked(locked[0])            
 );
 
 MMCM_TX  PLL1
 (// Clock in ports
    .clk_in1_p(clk_in_p[1]),
    .clk_in1_n(clk_in_n[1]),
   // Clock out ports  
    .clk(clk[1]),        
    .fastclk(fastclk[1]),  
    .algclk(algclk[1]), 
   // Status and control signals                
    .reset(1'b0),
    .locked(locked[1])            
  );
  
  IDLY_PLL  IDLYY_PLL_INST
  (// Clock in ports
     .clk_in1_p(clk_in_p[2]),
     .clk_in1_n(clk_in_n[2]),
    // Clock out ports  
     .refclk(refclk),      
    // Status and control signals                
     .reset(reset),
     .locked(locked[2])            
   );
   
   fast_mmcm  fastPLL
     (// Clock in ports
        .clk_in1_p(clk_in_p[3]),
        .clk_in1_n(clk_in_n[3]),
       // Clock out ports  
        .pedclk(pedclk), 
        .slowclk(slowclk),    
       // Status and control signals                
        .reset(reset),
        .locked(locked[3])            
      );

reg dlyctrlres;
reg [9:0] locked_old;
always @(posedge clk[1]) begin
    locked_old <= {locked_old[8:0],&locked};
    if( &locked & ~locked_old[9]) 
        dlyctrlres <=1;
     else
        dlyctrlres <= 0;
end
        
    (* IODELAY_GROUP = "delay_group" *)
        IDELAYCTRL
        delayctrl (
         .REFCLK (refclk),
         .RST    (resetdlyctrl | dlyctrlres),
         .RDY    ());
    
 endmodule
