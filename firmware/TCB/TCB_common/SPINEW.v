`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.09.2016 16:12:02
// Design Name: 
// Module Name: SPINEW
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SPINEW(
input CLK,
input SELECT,
input SPICLK,
input MOSI,
output reg MISO,
output reg MISO_EN,
output reg [7:0] CMD,
output reg [31:0] ADDR,
input tri [31:0] RDATA,
output reg [31:0] WDATA,
output reg RCLK,
output reg WENA
    );
    
// register input signals and remove glitches
reg SPICLK_REG, SELECT_REG, MOSI_REG;
reg [1:0]SPICLK_TMP, SPIMOSI_TMP, SELECT_TMP;
reg PREV_SPICLK_REG;
always @(posedge CLK) begin
    SPICLK_TMP <= {SPICLK_TMP[0],SPICLK};
    SPICLK_REG <= &SPICLK_TMP;
    PREV_SPICLK_REG <= SPICLK_REG;
    SPIMOSI_TMP <= {SPIMOSI_TMP[0],MOSI};
    MOSI_REG <= &SPIMOSI_TMP;
    SELECT_TMP <= {SELECT_TMP[0], SELECT};
    SELECT_REG <= &SELECT_TMP;
end

//=============Internal Constants======================
parameter SIZE = 4;
parameter CMD_STATE  = 4'b0001,
        ADDR_STATE = 4'b0010,
        ERROR_STATE = 4'b0011,
        WRITE_STATE = 4'b0100,
        WAIT_DUMMY_STATE = 4'b0101,
        OUT_STATE = 4'b0110,
        RCLK_WRITE_STATE = 4'b0111,
        WAIT_WRITE_STATE = 4'b1000,
        IDLE_STATE  = 4'b0000,
        OUT_SETUP_STATE  = 4'b1001,
        ERROR_SETUP_STATE = 4'b1010;
//=============Internal Variables======================
reg   [SIZE-1:0]          STATE        ;// Seq part of the FSM
//=============Internal shift registers================
reg [7:0] CMD_SHIFTREG;
reg [31:0] ADDR_SHIFTREG;
reg [31:0] DATA_SHIFTREG;
reg [31:0] OUT_SHIFTREG;
reg [7:0] ERROR_SHIFTREG;
//=============Internal counter       ================
reg [7:0] COUNT;
//==========Code startes Here==========================
always @ (posedge CLK)
begin : FSM
    //keep in reset while SELECT is HIGH
    if (SELECT_REG == 1'b0) begin
        STATE <= CMD_STATE;
        MISO <= 0;
        MISO_EN <= 1'b1;
        ADDR <= 0;
        CMD <= 0;
        WDATA <= 0;
        RCLK <= 0;
        WENA <= 0;
        CMD_SHIFTREG <=0;
        ADDR_SHIFTREG <=0;
        OUT_SHIFTREG <=0;
        DATA_SHIFTREG <=0;
        ERROR_SHIFTREG <=0;
        COUNT <=8'd8;
    end else
    // data incoming!!!
        MISO_EN <= 1'b0;
        case(STATE)
        CMD_STATE : begin
            //receive CMD
            if(PREV_SPICLK_REG ==1'b0 && SPICLK_REG == 1'b1) begin
                CMD_SHIFTREG <= {CMD_SHIFTREG[6:0], MOSI_REG};
                COUNT <= COUNT-1;
            end
            
            if(COUNT == 0) begin //counted 8 bits
                CMD <= CMD_SHIFTREG;
                case(CMD_SHIFTREG)
                8'h11: begin //Write8
                    STATE <= ADDR_STATE;
                    ADDR_SHIFTREG <=0;
                    COUNT <=8;
                end
                8'h21: begin //Read8
                    STATE <= ADDR_STATE;
                    ADDR_SHIFTREG <=0;
                    COUNT <=8;
                end
                8'h12: begin //Write16
                    STATE <= ADDR_STATE;
                    ADDR_SHIFTREG <=0;
                    COUNT <=16;
                end
                8'h22: begin //Read16
                    STATE <= ADDR_STATE;
                    ADDR_SHIFTREG <=0;
                    COUNT <=16;
                end
                8'h14: begin //Write32
                    STATE <= ADDR_STATE;
                    ADDR_SHIFTREG <=0;
                    COUNT <=32;
                end
                8'h24: begin //Read32
                    STATE <= ADDR_STATE;
                    ADDR_SHIFTREG <=0;
                    COUNT <=32;
                end
                default: begin //unknow command
                    STATE <= ERROR_SETUP_STATE;
                    ERROR_SHIFTREG<=8'h01;
                    COUNT <=8;
                end
                endcase
            end
        end
        ADDR_STATE : begin
            //receive ADDR
            if(PREV_SPICLK_REG ==1'b0 && SPICLK_REG == 1'b1) begin
                ADDR_SHIFTREG <= {ADDR_SHIFTREG[30:0], MOSI_REG};
                COUNT <= COUNT-1;
            end
            
            if (COUNT == 0) begin// end ADDR
                ADDR <= ADDR_SHIFTREG;
                case(CMD_SHIFTREG)
                8'h11: begin //Write8
                    STATE <= WRITE_STATE;
                    DATA_SHIFTREG <=0;
                    COUNT <=32;
                end
                8'h12: begin //Write16
                    STATE <= WRITE_STATE;
                    DATA_SHIFTREG <=0;
                    COUNT <=32;
                end
                8'h14: begin //Write32
                    STATE <= WRITE_STATE;
                    DATA_SHIFTREG <=0;
                    COUNT <=32;
                end
                8'h21: begin //Read8
                    STATE <= WAIT_DUMMY_STATE;
                    COUNT <=8;
                end
                8'h22: begin //Read16
                    STATE <= WAIT_DUMMY_STATE;
                    COUNT <=8;
                end
                8'h24: begin //Read32
                    STATE <= WAIT_DUMMY_STATE;
                    COUNT <=8;
                end
                
                default: begin
                    STATE <= ERROR_SETUP_STATE;//theoretically is should never be here
                end
                endcase
            end
        end
            
        WRITE_STATE : begin
            //Setup WENA
            WENA <=1'b1;
            //receive DATA
            if(PREV_SPICLK_REG ==1'b0 && SPICLK_REG == 1'b1) begin
                DATA_SHIFTREG <= {DATA_SHIFTREG[30:0], MOSI_REG};
                COUNT <= COUNT-1;
            end
            if (COUNT==0)begin //write 32 bit to address and keep writing
                WDATA <= DATA_SHIFTREG;
                STATE <= RCLK_WRITE_STATE;
            end
        end
        
        RCLK_WRITE_STATE : begin //delay RCLK by 1 CLK ( to ensure WDATA is stable)
            RCLK <=1'b1;
            STATE <= WAIT_WRITE_STATE;
        end
        
        WAIT_WRITE_STATE: begin //1 clock delay to reset and start writing again
            RCLK <=1'b0;
            ADDR <= ADDR+1; // select next address
            COUNT <=32; //reload COUNT
            STATE <= WRITE_STATE;
        end
        
        WAIT_DUMMY_STATE: begin
        
            RCLK <=1'b0;
            if(COUNT==8) RCLK <=1'b1;
            
            //receive count clocks
            if(PREV_SPICLK_REG ==1'b0 && SPICLK_REG == 1'b1) begin
                COUNT <= COUNT-1;
            end
            
            if (COUNT == 0) begin// end ADDR
                OUT_SHIFTREG <=RDATA;
                COUNT <=32;
                STATE <= OUT_SETUP_STATE;
            end
        end
        
        OUT_SETUP_STATE: begin
            OUT_SHIFTREG <= {OUT_SHIFTREG[30:0], 1'b0};
            MISO <= OUT_SHIFTREG[31];
            STATE <= OUT_STATE;
            //COUNT <= COUNT-1;
            //Block Transfert: increment ADDR
            ADDR <= ADDR+1;
        end
        
        OUT_STATE : begin 
            RCLK <=1'b0;
            
            //shift out
            if(PREV_SPICLK_REG ==1'b0 && SPICLK_REG == 1'b1) begin
                OUT_SHIFTREG <= {OUT_SHIFTREG[30:0], 1'b0};
                MISO <= OUT_SHIFTREG[31];
                COUNT <= COUNT-1;
                
                if(COUNT==30)begin //Block Transfert: RCLK
                    RCLK <=1'b1;
                end
                
            end
            
            if(COUNT == 0) begin //end of word transfer
                //Block Tranfer: Latch 
                STATE <= OUT_SETUP_STATE;
                OUT_SHIFTREG <=RDATA;
                COUNT <=32;
            end
            
        end
        
        ERROR_SETUP_STATE:begin
            ERROR_SHIFTREG <= {ERROR_SHIFTREG[6:0], 1'b0};
            MISO <= ERROR_SHIFTREG[7];
            COUNT <= COUNT-1;
            STATE <=ERROR_STATE;
        end
        
        ERROR_STATE : begin   
            //shift out
            if(PREV_SPICLK_REG ==1'b0 && SPICLK_REG == 1'b1) begin
                ERROR_SHIFTREG <= {ERROR_SHIFTREG[6:0], 1'b0};
                MISO <= ERROR_SHIFTREG[7];
                COUNT <= COUNT-1;
            end
            if(COUNT == 0) begin
                STATE <=IDLE_STATE;
            end
        end 
        default: STATE <= IDLE_STATE;
        IDLE_STATE : begin
            MISO <= 0;
            ADDR <= 0;
            CMD <= 0;
            WDATA <= 0;
            RCLK <= 0;
            WENA <= 0;
            CMD_SHIFTREG <=0;
            ADDR_SHIFTREG <=0;
            OUT_SHIFTREG <=0;
            ERROR_SHIFTREG <=0;
            DATA_SHIFTREG <=0;
            COUNT <=8'd8;
        end
        
    endcase
end
    
endmodule
