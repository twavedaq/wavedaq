`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.09.2015 12:29:28
// Design Name: 
// Module Name: PACKETIZER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module PACKETIZER(
    input CLK,
    output reg [31:0] RADDR,
    output reg [31:0] WADDR,
    input tri [31:0] RDATA,
    output [31:0] WDATA,
    output reg RCLK,
    output reg WENA,
    input START,
    input SWSTART,
    input ENABLE,
    input ABORT,
    output reg RUNNING,
    output [9:0] INSTRUCTION_ADDR,
    input [31:0] INSTRUCTION_CMD,
    input [31:0] INSTRUCTION_DATA0,
    input [31:0] INSTRUCTION_DATA1
    );
    
    
    //PACKETIZER UTILITY REGS
    wire [31:0] WADDR_FROM_REG;
    wire [31:0] RADDR_FROM_REG;
    
    PACKREGS #(
        .BASEADDR(`RPACKREG_ADDR)
    ) ALU(
        .CLK(CLK),
        .RCLK(RCLK),
        .WENA(WENA),
        .WADDR(WADDR),
        .RADDR(RADDR),
        .WDATA(WDATA),
        .RDATA(RDATA),
        .WADDR_FROM_REG(WADDR_FROM_REG),
        .RADDR_FROM_REG(RADDR_FROM_REG)
    );

    reg WDATA_SOURCE;
    reg [31:0] LOCAL_WDATA;
    assign WDATA=(WDATA_SOURCE)?LOCAL_WDATA:RDATA;
    
    wire [31:0] LOCAL_RADDR;
    wire [31:0] LOCAL_WADDR;
    assign LOCAL_RADDR=(INSTRUCTION_DATA0[31])?RADDR_FROM_REG:INSTRUCTION_DATA0;
    assign LOCAL_WADDR=(INSTRUCTION_DATA1[31])?WADDR_FROM_REG:INSTRUCTION_DATA1;
    
    //wire LOCALSTART;
    //assign LOCALSTART = (START & ENABLE) | SWSTART;
    reg START_OLD;
    reg SWSTART_OLD;
    reg [9:0] INSTRUCTION_COUNTER;
    reg [15:0] COUNTER;
    
    parameter WAIT_STATE  = 3'b000,
              SETADDR_STATE = 3'b001,
              SETRCLK_STATE = 3'b010,
              SETWENA_STATE = 3'b011,
              NEXT_STATE = 3'b100,
              CHECKBITWAIT_STATE = 3'b101,
              CHECKBIT_STATE = 3'b110;
    reg [2:0] STATE;
    
    assign INSTRUCTION_ADDR = INSTRUCTION_COUNTER;
    
    always @(posedge CLK) begin
        START_OLD <= START;
        SWSTART_OLD <= SWSTART;
        if (ABORT) begin
            STATE <= WAIT_STATE;
        end else begin
            case(STATE)
                WAIT_STATE: begin
                    if((ENABLE & START & ~START_OLD) | (SWSTART & ~SWSTART_OLD)) begin
                        STATE <= SETADDR_STATE;
                        RUNNING <= 1'b1;
                    end else begin
                        RUNNING <= 1'b0;
                    end
                    COUNTER <= 0;
                    INSTRUCTION_COUNTER <= 0;
                    RCLK <= 1'b0;
                    WENA <= 1'b0;
                    WDATA_SOURCE <= 1'b0;
                    LOCAL_WDATA<=0;
                end
                SETADDR_STATE: begin
                    case(INSTRUCTION_CMD[31:28])
                        4'h0: begin //stop
                            STATE <= WAIT_STATE;
                        end
                        4'h1: begin //copy
                            RADDR <= LOCAL_RADDR;
                            WADDR <= LOCAL_WADDR;
                            INSTRUCTION_COUNTER <= INSTRUCTION_COUNTER+1;
                            RCLK <= 1'b1;
                            WENA <= 1'b0;
                            STATE <= SETRCLK_STATE;
                            WDATA_SOURCE <= 1'b0;
                        end
                        4'h2: begin //block copy
                            WDATA_SOURCE <= 1'b0;
                            if(COUNTER < INSTRUCTION_CMD[15:0]) begin
                                //next address
                                RADDR <= LOCAL_RADDR+COUNTER;
                                WADDR <= LOCAL_WADDR+COUNTER;
                                COUNTER <= COUNTER+1;
                                RCLK<=1'b1;
                                WENA<=1'b0;
                                STATE <= SETRCLK_STATE;
                            end else begin
                                //block end
                                INSTRUCTION_COUNTER <= INSTRUCTION_COUNTER+1;
                                COUNTER <= 16'b0;
                                STATE <= NEXT_STATE;
                            end
                        end
                        4'h3: begin //direct write
                            WDATA_SOURCE <= 1'b1;
                            RADDR <= 32'hFFFFFFFF;//DUMMY ADDRESS
                            WADDR <= LOCAL_WADDR;
                            RCLK <= 1'b1;
                            WENA <= 1'b1;
                            INSTRUCTION_COUNTER <= INSTRUCTION_COUNTER+1;
                            LOCAL_WDATA <= INSTRUCTION_DATA0;
                            STATE <= NEXT_STATE;
                        end
                        4'h4: begin //jump
                            WDATA_SOURCE <= 1'b0;
                            INSTRUCTION_COUNTER <= INSTRUCTION_CMD[9:0];
                            STATE <= NEXT_STATE;
                        end
                        4'h5: begin //jmp if
                            RADDR <= LOCAL_RADDR;
                            WADDR <= 32'hFFFFFFFF;//DUMMY ADDRESS
                            RCLK <= 1'b1;
                            WENA <= 1'b0;
                            STATE <= CHECKBITWAIT_STATE;
                            WDATA_SOURCE <= 1'b0;
                        end
                        default: begin //unknow command, wait
                            STATE <= WAIT_STATE;
                        end
                    endcase
                end
                CHECKBITWAIT_STATE: begin // delay to let data from BRAM to arrive
                    RCLK <= 1'b0;
                    WENA <= 1'b0;
                    STATE <= CHECKBIT_STATE;
                end
                CHECKBIT_STATE: begin
                    if(|(RDATA & INSTRUCTION_DATA1)) begin
                        INSTRUCTION_COUNTER <= INSTRUCTION_CMD[9:0]; // jump
                    end else begin
                        INSTRUCTION_COUNTER <= INSTRUCTION_COUNTER+1; //increment
                    end
                    STATE <= NEXT_STATE;
                end
                SETRCLK_STATE: begin
                    RCLK <= 1'b0;
                    WENA <= 1'b1;
                    STATE <= SETWENA_STATE;
                end
                SETWENA_STATE: begin
                    RCLK <= 1'b1;
                    WENA <= 1'b1;
                    STATE <= NEXT_STATE;
                end
                NEXT_STATE: begin
                    RCLK <= 1'b0;
                    WENA <= 1'b0;
                    if (INSTRUCTION_COUNTER == 10'h3FF) begin
                        STATE <= WAIT_STATE;
                    end else begin
                        STATE <= SETADDR_STATE;
                    end
                end
                default: begin
                    STATE <= WAIT_STATE;
                end
            endcase
        end
    end
    
    
endmodule