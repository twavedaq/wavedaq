`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: OUTPUTSERDES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OUTPUTSERDES(
    output [nLink-1:0] OUT_P,
    output [nLink-1:0] OUT_N,
    input CLK,
    input CLKDIV,
    input [8*nLink-1:0] DATA,
    input [nLink-1:0] RESET,
    input SYNC,
    input PATTERN_ENA
    );
    
    parameter nLink = 8;
    
    //pattern generator
    wire [7:0] PATTERN;
    PATTERNGENERATOR #(
        .PATTERN_INIT(8'ha0)
    ) GENERATOR(
        .SYNC(SYNC),
        .CLK(CLKDIV),
        .PATTERN(PATTERN)
    );
    
    // link instantiation
    genvar iLink;
    for(iLink = 0; iLink < nLink; iLink = iLink+1) begin
    
        wire [7:0] PATTERNMUX;
        
        assign PATTERNMUX = (PATTERN_ENA)? PATTERN : DATA[8*iLink+:8];
    
        OUTPUTLINK LINK(
            .OUT_P(OUT_P[iLink]),
            .OUT_N(OUT_N[iLink]),
            .CLK(CLK),
            .CLKDIV(CLKDIV),
            .DATA(PATTERNMUX),
            .RESET(RESET[iLink])
        );
    end
endmodule
