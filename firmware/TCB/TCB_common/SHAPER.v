`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 09.09.2015 14:34:57
// Design Name: 
// Module Name: SHAPER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: each bit shaped to be 40 ns long, each bit is valid only
// when it is fired for at least two clk cycle (to be protected from glitches) 
//////////////////////////////////////////////////////////////////////////////////


module SHAPER(
    input DIN,
    output DOUT,
    input CLK
    );
parameter SRLENGTH = 4;
// THE INPUT SIGNAL IS SHAPED TO BE 40 NS LONG
reg DOUTREG;
reg DIN_REG;
wire CLR;
//REGISTER THE DATA IN SIGNAL
always @(posedge CLK) begin
    DIN_REG <= DIN;
end
// DIN TRANSFERED TO DOUT WITH PRESET/CLEAR FLIP-FLOP
// 21/06/2016: now everything is registered
always @(posedge CLK)
    begin
        if (CLR)
            DOUTREG <= 1'b0;
        else if(DIN & ~DIN_REG)
            DOUTREG <= 1'b1;
    end
//HERE THE SHIFT REGISTER THAT GENERATE THE CLEAR AFTER SRLENGTH CLK TICKS
reg [SRLENGTH-1:0] TMP;      
always @(posedge CLK) 
       begin 
         TMP <= TMP << 1; 
         TMP[0] <= DOUTREG; 
       end 
assign CLR  = TMP[SRLENGTH-1]; 
assign DOUT = DOUTREG;
endmodule
