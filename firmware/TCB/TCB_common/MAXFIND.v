`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.08.2017 13:56:04
// Design Name: 
// Module Name: MAXFIND
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module MAXFIND(
    input CLK,
    input [MAXWFMSIZE*ENTRIES-1:0] MAXWFMIN,
    input [MAXSIZE*ENTRIES-1:0] MAXIN,
    input [TIMESIZE*ENTRIES-1:0] TIMEIN,
    output reg [MAXWFMSIZE-1:0] MAXWFM,
    output reg [MAXSIZE+CMP_LAYER-1:0] MAX,
    output reg [TIMESIZE-1:0] TIME
    );
    
    parameter CMP_LAYER = 2; // number of comparison layers in tree implementation
    parameter ENTRIES = 2**CMP_LAYER;
    
    parameter MAXWFMSIZE = 22;
    parameter MAXSIZE = 2;
    parameter TIMESIZE = 10;
    parameter INHERIT_MAX = 1;
    parameter SIGNED_COMPARE = 1;
    
    wire [ENTRIES*ENTRIES-1:0] COMP;
    
    //WE COMPUTE ALL THE COMPARISON BETWEEN INPUTS AND PUT IN A MATRIX
    //WE THEN REDUCE THE BITS WITH AN "AND" PORT FOR ANY ROW, THE MAX IS THE ONLY HAVING A "1" AS A RESULT OF THE REDUCE "AND"
    
    genvar iComp;
    genvar jComp;
    for(iComp = 0; iComp < ENTRIES; iComp = iComp + 1) begin
        for(jComp = iComp + 1; jComp < ENTRIES; jComp = jComp + 1) begin
            if (SIGNED_COMPARE == 1) begin
               assign COMP[jComp+ENTRIES*iComp] = $signed(MAXWFMIN[MAXWFMSIZE*iComp+:MAXWFMSIZE]) >= $signed(MAXWFMIN[MAXWFMSIZE*jComp+:MAXWFMSIZE]);
            end else begin
               assign COMP[jComp+ENTRIES*iComp] = MAXWFMIN[MAXWFMSIZE*iComp+:MAXWFMSIZE] >= MAXWFMIN[MAXWFMSIZE*jComp+:MAXWFMSIZE];

            end
            assign COMP[iComp+ENTRIES*jComp] = ~COMP[jComp+ENTRIES*iComp];
        end
        assign COMP[iComp+ENTRIES*iComp]=1;
    end
    
    integer iRow;
    always @(posedge CLK) begin
        MAXWFM <= 0;
        MAX <= 0;
        TIME <= 0;
        for(iRow = 0; iRow < ENTRIES; iRow = iRow+1) begin
            if(&COMP[ENTRIES*iRow+:ENTRIES]) begin
                MAXWFM <= MAXWFMIN[MAXWFMSIZE*iRow+:MAXWFMSIZE];
                if (INHERIT_MAX) MAX <= {iRow[CMP_LAYER-1:0], MAXIN[MAXSIZE*iRow+:MAXSIZE]};
                else  MAX <= iRow[CMP_LAYER-1:0];
                TIME <= TIMEIN[TIMESIZE*iRow+:TIMESIZE];
            end
        end
    end
    
endmodule
