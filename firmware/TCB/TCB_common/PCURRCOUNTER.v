`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/13/2017 04:33:31 PM
// Design Name: 
// Module Name: PCURRCOUNTER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PCURRCOUNTER(
    input PCURR,
    input CLK,
    input SYNC,
    output [31:0] PCCOUNT,
    input RUNMODE
    );
    
reg [1:0] PCURRREG;
reg [31:0] COUNTER;
reg SYNC_OLD;

always @(posedge CLK) begin
    
    SYNC_OLD <= SYNC;
    PCURRREG[1:0] <= {PCURRREG[0],PCURR};
    if(SYNC & ~SYNC_OLD) COUNTER <= 32'b0;   

else if(PCURRREG[0] & ~PCURRREG[1]) COUNTER<=COUNTER+1;

end

//LATCH THE COUNTER AFTER EACH EVENT
reg RUNMODE_OLD;
reg [31:0] COUNTER_LATCH;
always @(posedge CLK) begin
    RUNMODE_OLD <= RUNMODE;
    // SYNCHRONOUS LATCH AT NEGEDGE RUNMODE
    if(RUNMODE_OLD & ~RUNMODE)
        COUNTER_LATCH <= COUNTER;
end
assign PCCOUNT = COUNTER_LATCH;
 
endmodule
