`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: OUTPUTLINK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module OUTPUTLINK(
    output OUT_P,
    output OUT_N,
    input CLK,
    input CLKDIV,
    input [7:0] DATA,
    input RESET
    );
    
    wire OUT;
    
    OSERDESE2 #(
        .DATA_RATE_OQ("SDR"),
        .DATA_RATE_TQ("SDR"),
        .DATA_WIDTH(8),
        .TRISTATE_WIDTH(1),
        .SERDES_MODE("MASTER")
    ) oserdese2_master (
        .D1             (DATA[7]),
        .D2             (DATA[6]),
        .D3             (DATA[5]),
        .D4             (DATA[4]),
        .D5             (DATA[3]),
        .D6             (DATA[2]),
        .D7             (DATA[1]),
        .D8             (DATA[0]),
        .T1             (1'b0),
        .T2             (1'b0),
        .T3             (1'b0),
        .T4             (1'b0),
        .SHIFTIN1       (1'b0),
        .SHIFTIN2       (1'b0),
        .SHIFTOUT1      (),
        .SHIFTOUT2      (),
        .OCE            (1'b1),
        .CLK            (CLK),
        .CLKDIV         (CLKDIV),
        .OQ             (OUT),
        .TQ             (),
        .OFB            (),
        .TFB            (),
        .TBYTEIN        (1'b0),
        .TBYTEOUT       (),
        .TCE            (1'b0),
        .RST            (RESET)
    );
    
    OBUFDS #(
        .IOSTANDARD ("LVDS")
    ) BUFFER(
        .O(OUT_P),
        .OB(OUT_N),
        .I(OUT)
    );
    
endmodule
