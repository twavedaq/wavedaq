`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 10.09.2015 09:55:33
// Design Name: 
// Module Name: TIMEEVE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments: time and event counters
// 
//////////////////////////////////////////////////////////////////////////////////


module TIMEEVE(
    input RUNMODE,
    input SYNC,
    input CLK,
    input SLOWCLK,
    output [31:0] EVECOU,
    output [31:0] LIVETIME,
    output [31:0] TOTALTIME
    );
//DIVIDE THE SLOWCLK BY 10 TO HAVE 1MHZ CLK FOR AN INTEGRATION TIME FO 70MINS...
wire RESET;
reg CLKTIME;
reg [3:0] COUTIME;
always @(posedge SLOWCLK) begin
    if(RESET) begin
        COUTIME <= 0;
        CLKTIME <=1'b0;
    end else begin
        COUTIME <= COUTIME+1;
        if(COUTIME == 4'b1000) CLKTIME <=1'b1;
    end
end
assign RESET = (COUTIME == 4'b1001) | SYNC; //COUNTER RESET WHEN COUNTER AT 10 OR SYNC

//TOTAL TIME COUNTER, ALWAYS ENABLED
reg OLD_CLKTIME;
reg [31:0] TOTALTIME_REG;
reg [31:0] TOTALTIME_LATCH;
reg SYNC_OLD;
always @(posedge CLK) begin
    SYNC_OLD <= SYNC;
    if(SYNC & ~SYNC_OLD)
        TOTALTIME_REG <= 0;
    else begin
        OLD_CLKTIME <=CLKTIME;
        if (CLKTIME & ~OLD_CLKTIME) //handmade edge-triggering
            TOTALTIME_REG <= TOTALTIME_REG+1;
    end
end 
//LATCH THE TOTAL TIME AFTER EACH EVENT
reg RUNMODE_OLD;
always @(posedge CLK) begin
    RUNMODE_OLD<= RUNMODE;
    // SYNCHRONOUS LATCH AT NEGEDGE RUNMODE
    if(RUNMODE_OLD & ~RUNMODE)
        TOTALTIME_LATCH <= TOTALTIME_REG;
end
assign TOTALTIME = TOTALTIME_LATCH;
//LIVE TIME COUNTER, ENABLED BY RUNMODE
reg [31:0] LIVETIME_REG;

always @(posedge CLK) begin
    if(SYNC & ~SYNC_OLD) //posedge SYNC
        LIVETIME_REG <= 0;
    else if(RUNMODE & CLKTIME & ~OLD_CLKTIME)
        LIVETIME_REG <= LIVETIME_REG+1;
end
assign LIVETIME = LIVETIME_REG;
//EVENT COUNTER COUNTER
reg [31:0] EVECOU_REG;
always @(posedge CLK) begin
    if(SYNC & ~SYNC_OLD) //posedge SYNC
        EVECOU_REG <= 0;
    else
    if(~RUNMODE_OLD & RUNMODE) //posedge RUNMODE
        EVECOU_REG <= EVECOU_REG+1;
end
assign EVECOU = EVECOU_REG;

endmodule
