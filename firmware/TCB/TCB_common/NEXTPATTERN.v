`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: NEXTPATTERN
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NEXTPATTERN(
    input [7:0] CURRENT,
    output [7:0] NEXT
    );
    
    //logic for pattern generation
    //keep high part
    assign NEXT[7:4] = CURRENT[7:4];
    //increment low part
    assign NEXT[3:0] = CURRENT[3:0] + 1;
endmodule
