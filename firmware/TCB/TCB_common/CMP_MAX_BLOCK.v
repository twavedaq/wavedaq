`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.04.2017 15:23:40
// Design Name: 
// Module Name: CMP_MAX_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CMP_MAX_BLOCK(
    input [WFMWIDTH-1:0] WFMA,
    input [WFMWIDTH-1:0] WFMB,
    input [MAXWIDTH-1:0] MAXA,
    input [MAXWIDTH-1:0] MAXB,
    input CLK,
    output reg [WFMWIDTH-1:0] WFMO,
    output reg [MAXWIDTH-1:0] MAXO
    );

parameter WFMWIDTH = 22;
parameter MAXWIDTH = 1;

wire CMP;

assign CMP = $signed(WFMA) >= $signed(WFMB);

always @(posedge CLK) begin
    if(CMP) begin 
        WFMO <= WFMA;
        MAXO <= MAXA;
     end
     else begin
        WFMO <= WFMB;
        MAXO <= MAXB;
     end
end

endmodule
