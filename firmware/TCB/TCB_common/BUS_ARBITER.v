`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.06.2017 14:38:36
// Design Name: 
// Module Name: BUS_ARBITER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module BUS_ARBITER(
    input CLK,
    input [7:0]CMD_SPI,
    input [31:0] ADDR_SPI,
    output tri [31:0] RDATA_SPI,
    input [31:0] WDATA_SPI,
    output reg [31:0] RADDR_SPI,
    output reg [31:0] WADDR_SPI,
    input RCLK_SPI,
    input WENA_SPI,
    input [31:0] RADDR_PACKET,
    input [31:0] WADDR_PACKET,
    output tri [31:0] RDATA_PACKET,
    input [31:0] WDATA_PACKET,
    input RCLK_PACKET,
    input WENA_PACKET,
    input tri [31:0] RDATA,
    output [31:0] WDATA,
    output RCLK,
    output WENA,
    output reg [31:0] RADDR,
    output reg [31:0] WADDR,
    output ENABLE_PACKET,
    output SWSTART_PACKET,
    output ABORT_PACKET,
    output SELFSTART_PACKET
    );
    
    wire [31:0]ARBITER_CONF;
    
    //BUS MASTERING LOGIC
    wire SOURCE;
    assign SOURCE = ARBITER_CONF[0];

        //packetizer commands
    assign ENABLE_PACKET = ARBITER_CONF[1];
    assign SWSTART_PACKET = ARBITER_CONF[2];
    assign ABORT_PACKET = ARBITER_CONF[3];
    assign SELFSTART_PACKET = ARBITER_CONF[4];
    
    
    //config reg (SPI only access)
    SINGLEREG_RW_BLOCK 
    #(
    .REG_ADDR(`RARBITER_ADDR)
    ) RARBITER_REG (
    .CLK(CLK),
    .RCLK(RCLK_SPI),
    .RADDR(RADDR_SPI),
    .WADDR(WADDR_SPI),
    .WENA(WENA_SPI),
    .DATAIN(WDATA_SPI),
    .DATAOUT(RDATA_SPI),
    .VALUE(ARBITER_CONF),
    .RESET(1'b0)
    );
    
   SINGLEREG_R_BLOCK 
    #(
    .REG_ADDR(`RARBITER_ADDR)
    ) RARBITER2_REG (
    .CLK(CLK),
    .RCLK(RCLK_PACKET),
    .RADDR(RADDR_PACKET),
    .DATAOUT(RDATA_PACKET),
    .VALUE(ARBITER_CONF)
    );

    
    
    always @(*) begin
        if(SOURCE==1'b0) begin //SPI
            casex (CMD_SPI)
            8'h1x: begin //Write
                RADDR = 32'hFFFFFFFF;
                WADDR = ADDR_SPI;
            end
            8'h2x: begin //Read
                RADDR = ADDR_SPI;
                WADDR = 32'hFFFFFFFF;
            end
            default: begin //OTHER
                RADDR = 32'hFFFFFFFF;
                WADDR = 32'hFFFFFFFF;
            end
            endcase
        end else begin //Packetizer
            RADDR = RADDR_PACKET;
            WADDR = WADDR_PACKET;
        end
           
    end
    
    always @(*) begin
        casex (CMD_SPI)
            8'h1x: begin //Write
                RADDR_SPI = 32'hFFFFFFFF;
                WADDR_SPI = ADDR_SPI;
            end
            8'h2x: begin //Read
                RADDR_SPI = ADDR_SPI;
                WADDR_SPI = 32'hFFFFFFFF;
            end
            default: begin //OTHER
                RADDR_SPI = 32'hFFFFFFFF;
                WADDR_SPI = 32'hFFFFFFFF;
            end
        endcase
     end
  
    
    assign WDATA = (SOURCE)?WDATA_PACKET:WDATA_SPI;
    assign RCLK = (SOURCE)?RCLK_PACKET:RCLK_SPI;
    assign WENA = (SOURCE)?WENA_PACKET:WENA_SPI;
    assign RDATA_PACKET = (SOURCE)?RDATA:32'hZZZZZZZZ;
    assign RDATA_SPI = (SOURCE)?32'hZZZZZZZZ:RDATA;

    
endmodule
