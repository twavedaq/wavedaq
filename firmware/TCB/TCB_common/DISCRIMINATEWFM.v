`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.08.2017 17:30:12
// Design Name: 
// Module Name: DISCRIMINATEWFM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DISCRIMINATEWFM(
    input [WFMSIZE-1:0] INPUT,
    input [WFMSIZE-1:0] THR,
    input CLK,
    output reg TRIG
    );
    parameter LENGTH = 4;
    parameter WFMSIZE = 32;    
    
    reg FIRE;
    reg FIRE_OLD;
    reg [10:0] COUNTER;
    
    //compare threshold
    always @(posedge CLK) begin
        FIRE_OLD <= FIRE;
    
        if($signed(THR)!=0) 
            FIRE <= ($signed(INPUT) > $signed(THR));
        else
            FIRE <= 1'b0;
    end

   reg STATE;
    // SHAPER PART
    always @(posedge CLK)
        begin
            if(STATE == 0) begin
               //WAIT FOR DISCR FALLING EDGE
               if(FIRE) begin
                  TRIG <= 1'b1;
               end else if(FIRE_OLD & ~FIRE) begin
                  TRIG <= 1'b1;
                  STATE <= 1'b1;
                  COUNTER <= 0;
               end else begin
                  TRIG <= 1'b0;
               end
            end else begin
               //WAIT SOME TIME
               COUNTER <= COUNTER+1;

               if (COUNTER >= LENGTH) begin
                  STATE<=1'b0;
                  COUNTER <= 0;
                  TRIG <= 1'b0;
               end else begin
                  TRIG <= 1'b1;
               end
            end

            /*if (FIRE & ~FIRE_OLD) begin
                TRIG <= 1'b1;
                COUNTER <= 0;
            end else if(COUNTER >= LENGTH) begin
                TRIG<=1'b0;
                COUNTER <=0;
            end else if(TRIG) begin
                COUNTER <= COUNTER+1;
            end*/
                
        end

endmodule
