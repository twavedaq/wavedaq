`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 03.05.2017 11:37:00
// Design Name: 
// Module Name: DATACHECK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DATACHECK(
    input CLK,
    input [7:0] DATA,
    input [7:0] VALUE,
    input RESET,
    input ENABLE,
    output reg ERROR,
    output reg [31:0] COUNTER
    );
    
    reg [7:0] DATAREG;
    
    always @(posedge CLK) begin
        DATAREG <= DATA;
    end
    
    wire [7:0] COMPARE;
    assign COMPARE = (DATAREG ^ VALUE);
    
    always @(posedge CLK) begin
        if(RESET) begin
            ERROR <= 1'b0;
            COUNTER <= 32'b0;
        end else if (ENABLE) begin
            if ( |COMPARE ) begin
                ERROR <= 1'b1;
                COUNTER  <= COUNTER+1;
            end
        end
    
    end
    
    
endmodule
