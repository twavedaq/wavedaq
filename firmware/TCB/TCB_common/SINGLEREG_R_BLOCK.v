`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 21.06.2017 14:19:58
// Design Name: 
// Module Name: SINGLEREG_R_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SINGLEREG_R_BLOCK(
    input CLK,
    input [31:0]RADDR,
    input RCLK,

    output tri [31:0] DATAOUT,
    input [31:0] VALUE
    );
    
    parameter [31:0]REG_ADDR = 32'b0;

    
//SIMPLE REGISTER
reg RCLK_OLD;
reg OUT;
always @(posedge CLK) begin
    RCLK_OLD <= RCLK;
    if(RCLK & ~RCLK_OLD) begin
        if(RADDR==REG_ADDR)
            OUT <= 1'b1;
        else OUT <= 1'b0;
    end
end

assign DATAOUT = (OUT)?VALUE:32'hzzzzzzzz;


endmodule
