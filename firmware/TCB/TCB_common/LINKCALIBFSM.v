`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: LINKCALIBFSM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module LINKCALIBFSM(
    input [8*nLink-1:0] LINKDATA,
    input CLK,
    input START,
    input RESET_FSM,
    output reg BITSLIP,
    output reg [4:0] DLY,
    output reg LOADDLY,
    output reg RESET,
    output BUSY,
    output reg FAIL,
    output reg USE_DLY,
    output reg [31:0] DLY_TESTED,
    output reg [31:0] DLY_STATE,
    input CALIBMASK
    );
    
    wire MATCH;
    wire MATCH_EXACT;
    
    COMPARELINK #(
        .nLink(nLink)
    ) CMP(
        .CLK(CLK),
        .LINKDATA(LINKDATA),
        .MATCH(MATCH),
        .MATCH_EXACT(MATCH_EXACT)
    );
    
    //block to compare LINKDATA and NEXTDATA

    
    
    //=============Internal Constants======================
    parameter SIZE = 4;
    parameter WAIT_STATE  = 4'b0001,
            RESET_STATE = 4'b0010,
            COUNT_STATE = 4'b0011,
            CHECK_STATE = 4'b0100,
            DLY_STATE_LOOP = 4'b0101,
            COUNT_STATE_LOOP = 4'b0110,
            CHECK_STATE_LOOP = 4'b0111,
            DLY_FOUND = 4'b1000,
            COUNT_STATE_WAIT = 4'b1001,
            COUNT_STATE_LOOP_WAIT = 4'b1010,
            BITSLIP_SEARCH_WAIT = 4'b1011,
            BITSLIP_SEARCH = 4'b1100,
            BITSLIP_CHECK = 4'b1101;
            
    parameter TIME_PER_POINT = 24'h80000;
    parameter THR = 5'h6;//measured bit size = 14 (account for 1 to find edge)
    parameter nLink = 8;
    //=============Internal Variables======================
    reg   [SIZE-1:0]          STATE        ;// Seq part of the FSM
    reg   [24:0]              COUNTER      ;
    reg   [24:0]              TIMER        ;
    reg                       PREVDLY      ;
    reg   [2:0]               BITCOUNT     ;
    
    assign BUSY = (STATE !=WAIT_STATE);
    
    always @ (posedge CLK)
    begin : FSM
        //reset
        if (RESET_FSM == 1'b1) begin
            STATE <= WAIT_STATE;
        end else
        case(STATE)
            WAIT_STATE : begin
                BITSLIP <= 1'b0;
                LOADDLY <= 1'b0;
                if(START) begin
                    STATE <= RESET_STATE;
                    RESET <= 1'b1;
                    DLY <=5'b0;
                    DLY_TESTED <= 32'b0;
                    DLY_STATE <= 32'b0;
                    USE_DLY <=1'b1;
                 end else begin
                    RESET <= 1'b0;
                    USE_DLY <=1'b0;
                 end
            end
            RESET_STATE : begin
                RESET <= 1'b0;
                FAIL<=1'b0;
                LOADDLY <= 1'b1;
                COUNTER <= 0;
                TIMER <= 0;
                STATE <= COUNT_STATE_WAIT;
            end
            COUNT_STATE_WAIT: begin
                LOADDLY <= 1'b0;
                if(TIMER<10) TIMER <= TIMER+1;
                else begin
                    STATE<=COUNT_STATE;
                    TIMER <= 0;
                end
            end
            COUNT_STATE : begin
                LOADDLY <= 1'b0;
                if(TIMER < TIME_PER_POINT) begin
                    TIMER <= TIMER+1;
                    if(MATCH) COUNTER <= COUNTER +1;
                end else begin
                    STATE <= CHECK_STATE;
                end
            end
            CHECK_STATE : begin
                if(COUNTER == TIME_PER_POINT) begin
                    //good point
                    PREVDLY <= 1'b1;
                    //DLY_STATE <= DLY_STATE | (32'b1<<DLY);
                    DLY_STATE[DLY] <= 1'b1;
                end else begin
                    //bad point
                    PREVDLY <= 1'b0;
                    //DLY_STATE <= DLY_STATE & ~(32'b1<<DLY);
                end
                //DLY_TESTED <= DLY_TESTED | (32'b1<<DLY);
                DLY_TESTED[DLY] <= 1'b1;
                DLY <= DLY +1;
                LOADDLY <= 1'b0;
                STATE <= DLY_STATE_LOOP;
            end
            DLY_STATE_LOOP : begin
                if(DLY == 5'b0) begin
                    //no edge found
                    FAIL<=1'b1;
                    STATE <= WAIT_STATE;
                end else begin
                    LOADDLY <= 1'b1;
                    STATE <= COUNT_STATE_LOOP_WAIT;
                end
                COUNTER <= 0;
                TIMER <= 0;
            end
            COUNT_STATE_LOOP_WAIT: begin
                LOADDLY <= 1'b0;
                if(TIMER<10) TIMER <= TIMER+1;
                else begin
                    STATE<=COUNT_STATE_LOOP;
                    TIMER <= 0;
                end
            end
            COUNT_STATE_LOOP : begin
                if(TIMER < TIME_PER_POINT) begin
                    TIMER <= TIMER+1;
                    if(MATCH) COUNTER <= COUNTER +1;
                end else begin
                    STATE <= CHECK_STATE_LOOP;
                end
                LOADDLY <= 1'b0;
            end
            CHECK_STATE_LOOP : begin
                DLY <= DLY +1;
                //DLY_TESTED <= DLY_TESTED | (32'b1<<DLY);
                DLY_TESTED[DLY] <= 1'b1;
                STATE <= DLY_STATE_LOOP;
                if(COUNTER == TIME_PER_POINT) begin
                    //good point
                    //DLY_STATE <= DLY_STATE | (32'b1<<DLY);
                    DLY_STATE[DLY] <= 1'b1;
                    if(PREVDLY==1'b0) begin
                        //rising edge
                        if((DLY < (32-THR))& ~CALIBMASK) begin
                            //found
                            DLY <= DLY + THR;
                            STATE <= DLY_FOUND;
                         end
                    end
                    
                    PREVDLY <= 1'b1;
                end else begin
                    //bad point
                    //DLY_STATE <= DLY_STATE & ~(32'b1<<DLY);
                    if(PREVDLY==1'b1) begin
                        //falling edge
                         if((DLY>=THR) & ~CALIBMASK) begin
                             //found
                             DLY <= DLY - THR;
                             STATE <= DLY_FOUND;
                         end
                    end
                    PREVDLY <= 1'b0;
                end
                
            end
            DLY_FOUND: begin
                LOADDLY <=1'b1;
                STATE <= BITSLIP_SEARCH_WAIT;
                BITCOUNT <= 3'b000;
                TIMER <= 0;
            end
            BITSLIP_SEARCH_WAIT: begin
                LOADDLY <=1'b0;
                BITSLIP <= 1'b0;
                if(TIMER<10) TIMER <= TIMER+1;
                else begin
                    STATE<=BITSLIP_SEARCH;
                    TIMER <= 0;
                    COUNTER <= 0;
                end
            end
            BITSLIP_SEARCH: begin
                if(TIMER < TIME_PER_POINT) begin
                    TIMER <= TIMER+1;
                    if(MATCH_EXACT) COUNTER <= COUNTER +1;
                end else begin
                    STATE <= BITSLIP_CHECK;
                end
            end
            BITSLIP_CHECK: begin
                TIMER <= 0;
                if(COUNTER == TIME_PER_POINT) begin
                    //bitslip ok
                    FAIL<=1'b0;
                    STATE <= WAIT_STATE;
                end else begin
                    //next bitslip
                    if(BITCOUNT!=3'b111) begin
                        BITSLIP <= 1'b1;
                        STATE <= BITSLIP_SEARCH_WAIT;
                        BITCOUNT <= BITCOUNT+1;
                    end else begin
                        //end of possible bitslip
                        FAIL<=1'b1;
                        STATE <= WAIT_STATE;
                    end
                end
            end
            default : begin
                STATE <= WAIT_STATE;
            end
        endcase
    end

    

    
endmodule
