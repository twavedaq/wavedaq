`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.10.2016 10:23:21
// Design Name: 
// Module Name: TRGBUS_TX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

module TRGBUS_TX(
    input CLK,
    input SYNC,
    input [15:0] TRGBUSDLY,
    input TRGBUS_NEGEDGE,
    output TRG_OUT_P,
    output TRG_OUT_N,
    input TRG_OUT,
    output SYNC_OUT_P,
    output SYNC_OUT_N,
    input SYNC_OUT,
    output SPARE_OUT_P,
    output SPARE_OUT_N,
    input [31:0] EVECOU,
    input [31:0] FSTIME,
    input [`TRITYPDIM-1:0] TRITYPE,
    input [`TRGFAMNUM-1:0] ROENA,
    output ENGETRI,
    input ENADC
    );

    parameter USE_ODELAY = 1;
    
    // ------------- SYNC -------------
    wire SYNC_DIRECT;
    assign SYNC_DIRECT = (USE_ODELAY==1)?~SYNC_OUT:SYNC_OUT;//this accounts for SYNC polarity swap by Ancillary
    //ODDR to select output edge
    reg RSYNC;
    wire SYNC_TODLY;
    always @(posedge CLK) begin
        RSYNC <= SYNC_DIRECT;
    end
    ODDR #(
      .DDR_CLK_EDGE("SAME_EDGE"),
      .INIT(0),
      .SRTYPE("SYNC")
    ) SYNC_ODDR(
      .D1((TRGBUS_NEGEDGE)?RSYNC:SYNC_DIRECT),
      .D2(SYNC_DIRECT),
      .C(CLK),
      .CE(1'b1),
      .Q(SYNC_TODLY),
      .S(1'b0),
      .R(1'b0)
    );

    //ODELAY
    wire SYNC_OUTDLY;
    generate
        if(USE_ODELAY==1) begin 
            (* IODELAY_GROUP = "delay_group" *)
            ODELAYE2 # (
               .CINVCTRL_SEL           ("FALSE"),      // TRUE, FALSE
               .DELAY_SRC              ("ODATAIN"),    // ODATAIN, CLKIN
               .HIGH_PERFORMANCE_MODE  ("TRUE"),       // TRUE, FALSE
               .ODELAY_TYPE            ("VAR_LOAD"),   // FIXED, VARIABLE, or VAR_LOADABLE
               .ODELAY_VALUE           (0),            // 0 to 31
               .REFCLK_FREQUENCY       (200.0),
               .PIPE_SEL               ("FALSE"),
               .SIGNAL_PATTERN         ("DATA")        // CLOCK, DATA
            ) SYNC_DELAY (
               .DATAOUT                (SYNC_OUTDLY),
               .CLKIN                  (),
               .C                      (CLK),
               .CE                     (1'b0),
               .INC                    (1'b0),
               .ODATAIN                (SYNC_TODLY), // Driven by OLOGIC/OSERDES
               .LD                     (TRGBUSDLY[15]),
               .REGRST                 (1'b0),
               .LDPIPEEN               (1'b1),
               .CNTVALUEIN             (TRGBUSDLY[4:0]),
               .CNTVALUEOUT            (),
               .CINVCTRL               (1'b0)
            );
        end
    endgenerate

    //output buffer
    wire SYNC_TOBUFFER;
    assign SYNC_TOBUFFER = (USE_ODELAY==1)?SYNC_OUTDLY:SYNC_TODLY;
    OBUFDS #(
       .IOSTANDARD("LVDS") // Specify the output I/O standard
    ) OBUFDS_SYNC (
       .O(SYNC_OUT_P),    // Diff_p output (connect directly to top-level port)
       .OB(SYNC_OUT_N),   // Diff_n output (connect directly to top-level port)
       .I(SYNC_TOBUFFER)  // Buffer input 
    );

    // ------------- TRG -------------
    wire TRG_DIRECT;
    reg [2:0] TRG_SHAPE;
    assign TRG_DIRECT = (|TRG_SHAPE)|TRG_OUT;//this is to shape the trigger
    always @(posedge CLK) begin
        TRG_SHAPE <= {TRG_SHAPE[1:0],TRG_OUT};
    end
    //ODDR to select output edge
    reg RTRG;
    wire TRG_TODLY;
    always @(posedge CLK) begin
        RTRG <= TRG_DIRECT;
    end
    ODDR #(
      .DDR_CLK_EDGE("SAME_EDGE"),
      .INIT(0),
      .SRTYPE("SYNC")
    ) TRG_ODDR(
      .D1((TRGBUS_NEGEDGE)?RTRG:TRG_DIRECT),
      .D2(TRG_DIRECT),
      .C(CLK),
      .CE(1'b1),
      .Q(TRG_TODLY),
      .S(1'b0),
      .R(1'b0)
    );

    //ODELAY
    wire TRG_OUTDLY;
    generate
        if(USE_ODELAY==1) begin 
            (* IODELAY_GROUP = "delay_group" *)
            ODELAYE2 # (
               .CINVCTRL_SEL           ("FALSE"),      // TRUE, FALSE
               .DELAY_SRC              ("ODATAIN"),    // ODATAIN, CLKIN
               .HIGH_PERFORMANCE_MODE  ("TRUE"),       // TRUE, FALSE
               .ODELAY_TYPE            ("VAR_LOAD"),   // FIXED, VARIABLE, or VAR_LOADABLE
               .ODELAY_VALUE           (0),            // 0 to 31
               .REFCLK_FREQUENCY       (200.0),
               .PIPE_SEL               ("FALSE"),
               .SIGNAL_PATTERN         ("DATA")        // CLOCK, DATA
            ) TRG_DELAY (
               .DATAOUT                (TRG_OUTDLY),
               .CLKIN                  (),
               .C                      (CLK),
               .CE                     (1'b0),
               .INC                    (1'b0),
               .ODATAIN                (TRG_TODLY), // Driven by OLOGIC/OSERDES
               .LD                     (TRGBUSDLY[15]),
               .REGRST                 (1'b0),
               .LDPIPEEN               (1'b1),
               .CNTVALUEIN             (TRGBUSDLY[9:5]),
               .CNTVALUEOUT            (),
               .CINVCTRL               (1'b0)
            );
        end
    endgenerate
    

    //output buffer
    wire TRG_TOBUFFER;
    assign TRG_TOBUFFER = (USE_ODELAY==1)?TRG_OUTDLY:TRG_TODLY;
    OBUFDS #(
       .IOSTANDARD("LVDS") // Specify the output I/O standard
    ) OBUFDS_TRG (
       .O(TRG_OUT_P),    // Diff_p output (connect directly to top-level port)
       .OB(TRG_OUT_N),   // Diff_n output (connect directly to top-level port)
       .I(TRG_TOBUFFER)  // Buffer input 
    );

    // ------------- SPARE -------------
    wire SPARE_DIRECT;
    wire [15-`TRITYPDIM-`TRGFAMNUM-1:0] BLANK;  //-1 IS FRO ENADC 
    assign BLANK = 0;

    //Serialization FSM
    TRGLINK_TX #(
    .datasize(16+32),
    .invert(0) //SPARE no longer inverted
    ) TRGLINK_TX_SPARE_BLOCK(
    .TRGIN(TRG_OUT),
    .ENABLE(1'b1),
    .CLK(CLK),
    .SYNC(SYNC),
    .TRGOUT(SPARE_DIRECT),
    .DATA({ENADC,ROENA,BLANK,TRITYPE,FSTIME[15:0],EVECOU[15:0]}),
    .ENGETRI(ENGETRI)
    );

    //ODDR to select output edge
    reg RSPARE;
    wire SPARE_TODLY;
    always @(posedge CLK) begin
        RSPARE <= SPARE_DIRECT;
    end
    ODDR #(
      .DDR_CLK_EDGE("SAME_EDGE"),
      .INIT(0),
      .SRTYPE("SYNC")
    ) SPARE_ODDR(
      .D1((TRGBUS_NEGEDGE)?RSPARE:SPARE_DIRECT),
      .D2(SPARE_DIRECT),
      .C(CLK),
      .CE(1'b1),
      .Q(SPARE_TODLY),
      .S(1'b0),
      .R(1'b0)
    );

    //ODELAY
    wire SPARE_OUTDLY;
    generate
        if(USE_ODELAY==1) begin 
            (* IODELAY_GROUP = "delay_group" *)
            ODELAYE2 # (
               .CINVCTRL_SEL           ("FALSE"),      // TRUE, FALSE
               .DELAY_SRC              ("ODATAIN"),    // ODATAIN, CLKIN
               .HIGH_PERFORMANCE_MODE  ("TRUE"),       // TRUE, FALSE
               .ODELAY_TYPE            ("VAR_LOAD"),   // FIXED, VARIABLE, or VAR_LOADABLE
               .ODELAY_VALUE           (0),            // 0 to 31
               .REFCLK_FREQUENCY       (200.0),
               .PIPE_SEL               ("FALSE"),
               .SIGNAL_PATTERN         ("DATA")        // CLOCK, DATA
            ) SPARE_DELAY (
               .DATAOUT                (SPARE_OUTDLY),
               .CLKIN                  (),
               .C                      (CLK),
               .CE                     (1'b0),
               .INC                    (1'b0),
               .ODATAIN                (SPARE_TODLY), // Driven by OLOGIC/OSERDES
               .LD                     (TRGBUSDLY[15]),
               .REGRST                 (1'b0),
               .LDPIPEEN               (1'b1),
               .CNTVALUEIN             (TRGBUSDLY[14:10]),
               .CNTVALUEOUT            (),
               .CINVCTRL               (1'b0)
            );
        end
    endgenerate

    //output buffer
    wire SPARE_TOBUFFER;
    assign SPARE_TOBUFFER = (USE_ODELAY==1)?SPARE_OUTDLY:SPARE_TODLY;
    OBUFDS #(
       .IOSTANDARD("LVDS") // Specify the output I/O standard
    ) OBUFDS_SPARE (
       .O(SPARE_OUT_P),    // Diff_p output (connect directly to top-level port)
       .OB(SPARE_OUT_N),   // Diff_n output (connect directly to top-level port)
       .I(SPARE_TOBUFFER)  // Buffer input 
    );

endmodule
