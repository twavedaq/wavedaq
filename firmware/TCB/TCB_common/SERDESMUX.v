`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 17:56:00
// Design Name: 
// Module Name: SERDESMUX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"
module SERDESMUX(
    input [31:0] ALGSEL,
    input [64*NALG-1:0] IN,
    output reg [63:0] OUT
    );
    
    parameter NALG =3;
    // THIS IS A MUX
    integer iTRG;
    always @(*) begin
        OUT =0;
    
        for(iTRG=NALG-1; iTRG>=0; iTRG=iTRG-1) begin
            if (ALGSEL == iTRG) OUT = IN[64*iTRG+:64];
        end
    
    end
endmodule
