`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.11.2016 14:19:36
// Design Name: 
// Module Name: TRGLINK_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TRGLINK_RX(
    CLK,
    SYNC,
    ENABLE,
    TRGIN,
    TRGOUT,
    DATA,
    VALID,
    ERROR
    );
    
    parameter datasize = 32; //serialized word size
    
    //in out ports
    input TRGIN;
    input ENABLE;
    input CLK;
    input SYNC;
    output TRGOUT;
    output reg [datasize-1:0] DATA; 
    output reg VALID;
    output reg ERROR;
    
    //registers
    reg [1:0] STATE = 0;
    reg [datasize-1:0] SHIFTREG;
    reg [logb2(datasize)-1:0] COUNTER;
    reg TRGOUT_REG;
    reg PARITY;
    reg [2:0] WAITREG;
    
    //internal states
    parameter WAIT_TRG = 2'b00;
    parameter WAIT_ZERO = 2'b01;
    parameter DESERIALIZE = 2'b10;
    parameter OUT = 2'b11;
    
    always @(posedge CLK) begin
        //SERIAL FSM
    	if(SYNC) begin
    		STATE <= WAIT_TRG;
    		TRGOUT_REG<=1'b0;
    		VALID<=1'b0;
    		ERROR<=1'b0;
    		PARITY<=1'b0;
    		COUNTER <= datasize;
    		DATA<=0;
    		WAITREG <= 3'b101;
    	end else begin
    		case(STATE) 
    			WAIT_TRG: if(TRGIN) begin
    				STATE<=WAIT_ZERO;
    				VALID <=1'b0;
    				SHIFTREG <= 0;
    				ERROR<=1'b0;
    				PARITY<=1'b0;
    				WAITREG <= 3'b101;
    			end else TRGOUT_REG <= 1'b0;
    			WAIT_ZERO: 
    			if(WAITREG == 0) begin
    			    if(~TRGIN) begin
    			         STATE<=DESERIALIZE;
                         TRGOUT_REG <= 1'b1;
                         VALID <=1'b0;
                         ERROR <= 1'b0;
                         PARITY<=1'b0;
				         COUNTER<=datasize;
                    end else STATE<=WAIT_TRG; // Glitch removal
                    WAITREG <= 3'b011;
                end else begin
                    WAITREG<= WAITREG -1;
                end 
                DESERIALIZE: begin
                TRGOUT_REG<=1'b0;
                if(WAITREG == 0) begin
                    if (COUNTER ==0) begin
                        DATA <=SHIFTREG;
                        VALID <=1'b0;
                        //check parity
                        if(TRGIN != PARITY) ERROR <=1'b1;
                        else ERROR<=1'b0;
                    
                        STATE<=OUT;
                    end else begin
                        ERROR<=1'b0;
                        SHIFTREG[0] <= TRGIN;
                        SHIFTREG[datasize-1:1] <= SHIFTREG[datasize-2:0];
                        if(TRGIN==1'b1) PARITY <= ~PARITY;
                        else PARITY <= PARITY;
                        COUNTER = COUNTER -1;
                    end
                    WAITREG <= 3'b011;
                end else begin
                    WAITREG<=WAITREG-1;
                end
                end
                OUT: begin
                    VALID <=1'b1;
                    STATE <= WAIT_TRG;
                    COUNTER <= datasize;
                    TRGOUT_REG <= 1'b0;
                end
    		endcase
    	end
    	
    	//STOP & RESETFSM
    	if(~ENABLE) begin
    	    STATE <= WAIT_TRG;
    	    VALID <=1'b0;
    	    ERROR <=1'b0;
    	    COUNTER <= datasize;
    	    TRGOUT_REG <= 1'b0;
    	    WAITREG <= 3'b100;
    	end
    end
    //TRGIN pass-trough
    assign TRGOUT = (ENABLE)?TRGOUT_REG:TRGIN;
    
    //log2 function to calculate the COUNTER size
    function integer logb2;
    	input integer n;
    	integer i;
    	begin
    		i=1;
    		for(logb2=0; i<=n; logb2=logb2+1) i = i <<1;
    
    	end
    endfunction
endmodule
