`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.08.2017 17:30:12
// Design Name: 
// Module Name: DISCRCVETO
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DISCRVETO(
    input [WFMSIZE-1:0] INPUT,
    input [WFMSIZE-1:0] THR,
    input [WFMSIZE-1:0] THR_TOVETO,
    input DISCR_TOVETO,
    input CLK,
    output reg TRIG
    );
    parameter WFMSIZE = 32;    
    
    reg FIRE;
    reg FIRE_OLD;
    reg DISCR_TOVETO_OLD;
    
    //compare threshold
    always @(posedge CLK) begin
        FIRE_OLD <= FIRE;
        DISCR_TOVETO_OLD <= DISCR_TOVETO;
    
        if($signed(THR)!=0 && $signed(THR_TOVETO)<$signed(THR)) 
            FIRE <= ($signed(INPUT) > $signed(THR));
        else
            FIRE <= 1'b0;
    end

    // shaped until falling edge of discriminator to be vetoed
    always @(posedge CLK)
        begin
           if(FIRE & ~FIRE_OLD) begin
                  TRIG <= 1'b1;
           end else if (DISCR_TOVETO_OLD & ~DISCR_TOVETO) begin
                  TRIG <= 1'b0;
           end
        end

endmodule
