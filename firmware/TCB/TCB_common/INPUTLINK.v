`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.01.2018 11:55:22
// Design Name: 
// Module Name: INPUTLINK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module INPUTLINK(
    output [7:0] DATA,
    input RESET,
    input BITSLIP,
    input [4:0] DLY,
    output [4:0] CURRENT_DLY,
    output reg [2:0] CURRENT_BISLIP,
    input LOADDLY,
    input CLK,
    input CLKDIV,
    input IN_P,
    input IN_N
    );
    
    wire IN;
    
    IBUFDS #(
        .DIFF_TERM("FALSE"),
        .IOSTANDARD("LVDS_25")
    )ibufds_inst(
        .I(IN_P),
        .IB(IN_N),
        .O(IN)
    );
    
    wire IN_DLY;
    (* IODELAY_GROUP = "delay_group" *)
    IDELAYE2 # (
        .CINVCTRL_SEL("FALSE"),
        .DELAY_SRC("IDATAIN"),
        .HIGH_PERFORMANCE_MODE("TRUE"),
        .IDELAY_TYPE("VAR_LOAD"),
        .IDELAY_VALUE(1),
        .REFCLK_FREQUENCY(200.0),
        .PIPE_SEL("FALSE"),
        .SIGNAL_PATTERN("DATA")
    ) idelaye2_bus (
        .DATAOUT(IN_DLY),
        .DATAIN(1'b0),
        .C(CLKDIV),
        .CE(1'b0), //(in_delay_data_ce),
        .INC(1'b0), //in_delay_data_inc),
        .IDATAIN(IN), // Driven by IOB
        .LD(LOADDLY),
        .REGRST(RESET),
        .LDPIPEEN(1'b0),
        .CNTVALUEIN(DLY), //in_delay_tap_in),
        .CNTVALUEOUT(CURRENT_DLY), //in_delay_tap_out),
        .CINVCTRL(1'b0)
    );
    
    wire CLKB;
    assign CLKB = ~CLK;
    
    // declare the iserdes
    ISERDESE2
    #(
        .DATA_RATE("SDR"),
        .DATA_WIDTH(8),
        .INTERFACE_TYPE    ("NETWORKING"), 
        .DYN_CLKDIV_INV_EN ("FALSE"),
        .DYN_CLK_INV_EN    ("FALSE"),
        .NUM_CE            (2),
        .OFB_USED          ("FALSE"),
        .IOBDELAY          ("IFD"),                                // Use input at DDLY to output the data on Q
        .SERDES_MODE       ("MASTER")
    ) iserdese2_master (
        .Q1(DATA[0]),
        .Q2(DATA[1]),
        .Q3(DATA[2]),
        .Q4(DATA[3]),
        .Q5(DATA[4]),
        .Q6(DATA[5]),
        .Q7(DATA[6]),
        .Q8(DATA[7]),
        .SHIFTOUT1(),
        .SHIFTOUT2(),
        .BITSLIP(BITSLIP),
        .CE1(1'b1),
        .CE2(1'b1),
        .CLK(CLK),
        .CLKB(CLKB),
        .CLKDIV(CLKDIV),
        .CLKDIVP(1'b0),
        .D(1'b0),
        .DDLY(IN_DLY),
        .RST(RESET),
        .SHIFTIN1(1'b0),
        .SHIFTIN2(1'b0),
        // unused connections
        .DYNCLKDIVSEL(1'b0),
        .DYNCLKSEL(1'b0),
        .OFB(1'b0),
        .OCLK(1'b0),
        .OCLKB(1'b0),
        .O()
    );
    
    always @(posedge CLKDIV) begin
        if(RESET) begin
            CURRENT_BISLIP <= 3'b0;
        end else if (BITSLIP) begin
            CURRENT_BISLIP <= CURRENT_BISLIP+1;
        end
    end
    
endmodule
