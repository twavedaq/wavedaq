`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.11.2015 17:25:20
// Design Name: 
// Module Name: RMODE_BUSY_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RMODE_BUSY_BLOCK(
    input ENADDR,
    input [31:0] WDATA,
    input WENA,
    input RCLK,
    input STOP,
    output RMODE,
    output RMODEMEM,
    output INBUSY,
    input BUFFERBUSY,
    input RUNNING_PACKET,
    input SELFSTART_PACKET,
    input CLK,
    input EXBUSY,
    output reg SWSYNCOUT,
    output reg SWSTOPOUT
    );

//catch negedge packetizer
reg RUNNING_PACKET_OLD;
wire END_PACKET;

always @(posedge CLK) begin
    RUNNING_PACKET_OLD <= RUNNING_PACKET;
end
assign END_PACKET = ~RUNNING_PACKET & RUNNING_PACKET_OLD;

parameter BUSY_STATE  = 3'b001,
          READY_STATE = 3'b010,
          RUN_STATE = 3'b011,
          STOPPING_STATE = 3'b000,
          PACK_STATE = 3'b100,
          STOP_STATE = 3'b101;
reg [2:0] STATE;
reg [31:0] COUNTER; 
reg SWSTART;

always @ (posedge CLK)
begin : FSM
   if (SWSTOPOUT == 1'b1) begin
      STATE <= STOP_STATE;
   end else begin
      case(STATE)
         STOP_STATE : begin
            if(SWSTART)
               STATE <= READY_STATE;
         end
         BUSY_STATE : begin
            if(SWSTART)
               STATE <= READY_STATE;
            if(END_PACKET & SELFSTART_PACKET)
                STATE <= PACK_STATE;
         end
         PACK_STATE : begin
            if(~BUFFERBUSY)
                STATE <= READY_STATE;
         end
         READY_STATE : begin
            if(~EXBUSY)
               STATE <= RUN_STATE;
         end
         RUN_STATE : begin
            if(STOP) begin
               STATE <= STOPPING_STATE;
               COUNTER <= 32'h0;
               end
         end
         STOPPING_STATE : begin
            COUNTER <= COUNTER +1;
            if(COUNTER > 32'h8)
                STATE <= BUSY_STATE;
         end
         default: begin
            STATE <= STOP_STATE;
         end
      endcase
   end
end


//THE SWSYNC IS FIRED BY ACCESSING WDATA[2] IN RCMD REGISTER AND AUTOMATICALLY
//RELASED AFTER 4 CKL TICKS BY SHAPER
reg SYNC_REG;
reg [1:0] COUSYNC;
wire RCMD;
assign RCMD = ENADDR & WENA;
always @(posedge CLK) begin
    if(RCLK & RCMD & WDATA[2]) begin
        SWSYNCOUT <= 1;
        COUSYNC <= 0;
    end
    else begin
        if(SWSYNCOUT) begin
            COUSYNC <= COUSYNC + 1;
        end
        if (COUSYNC == 2'b11) begin
            SWSYNCOUT <= 0;
        end
    end
end

//THE SWSTOP IS FIRED BY ACCESSING WDATA[1] IN RCMD REGISTER AND AUTOMATICALLY
//RELASED AFTER 4 CKL TICKS BY SHAPER
reg STOP_REG;
reg [1:0] COUSTOP;
always @(posedge CLK) begin
    if(RCLK & RCMD & WDATA[1]) begin
        SWSTOPOUT <= 1;
        COUSTOP <= 0;
    end
    else begin
        if(SWSTOPOUT) begin
            COUSTOP <= COUSTOP + 1;
        end
        if (COUSTOP == 2'b11) begin
             SWSTOPOUT <= 0;
        end
    end
end

//THE SWSTART IS FIRED BY ACCESSING WDATA[0] IN RRUN REGISTER AND AUTOMATICALLY
//RELASED AFTER 1 CKL TICKS BY SHAPER
reg RCLK_OLD;
always @(posedge CLK) begin
   RCLK_OLD <= RCLK;
    if(RCLK & ~RCLK_OLD) begin
        if(RCMD & WDATA[0]) begin
            SWSTART <= 1;
        end
    end
   else SWSTART <= 0;
end

//assign RMODE and INBUSY according to internal state
assign RMODE = (STATE==RUN_STATE);
assign RMODEMEM = (STATE==RUN_STATE) | (STATE==STOPPING_STATE);
assign INBUSY = (STATE==BUSY_STATE) | (STATE==PACK_STATE);

endmodule
