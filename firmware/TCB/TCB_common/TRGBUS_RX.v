`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.10.2016 10:23:21
// Design Name: 
// Module Name: TRGBUS_RX
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module TRGBUS_RX(
    input CLK,
    input FASTCLK,
    input SERDES_RESET,
    input SYNC,
    input [15:0] TRGBUSDLY,
    input TRG_IN_P,
    input TRG_IN_N,
    output reg TRG_IN,
    input SYNC_IN_P,
    input SYNC_IN_N,
    output reg SYNC_IN,
    output reg [7:0] SYNC_RISING_WFM,
    output reg [7:0] SYNC_FALLING_WFM,
    input SPARE_IN_P,
    input SPARE_IN_N,
    output [31:0] SYS_EVECOU,
    output [`TRITYPDIM-1:0] SYS_TRITYPE,
    output [`TRGFAMNUM-1:0] SYS_ROENA,
    output SYS_VALID,
    output SYS_ERROR,
    output SYS_ENADC
    );
        
    // ------------- SYNC -------------
    // input buffer
    wire WSYNC;
    IBUFDS #(
       .DIFF_TERM("TRUE"), // Differential Termination
       .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest perforrmance="FALSE"
       .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_SYNC (
       .O(WSYNC), // Buffer output
       .I(SYNC_IN_P), // Diff_p buffer input (connect directly to top-level port)
       .IB(SYNC_IN_N) // Diff_n buffer input (connect directly to top-level port)
    );

    // IDLY    
    wire SYNCTOSERDES;
    (* IODELAY_GROUP = "delay_group" *) // Specifies group name for associated IDELAYs/ODELAYs and IDELAYCTRL
        
    IDELAYE2 #(
       .CINVCTRL_SEL("FALSE"),          // Enable dynamic clock inversion (FALSE, TRUE)
       .DELAY_SRC("IDATAIN"),           // Delay input (IDATAIN, DATAIN)
       .HIGH_PERFORMANCE_MODE("TRUE"), // Reduced jitter ("TRUE"), Reduced power ("FALSE")
       .IDELAY_TYPE("VAR_LOAD"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
       .IDELAY_VALUE(0),                // Input delay tap setting (0-31)
       .PIPE_SEL("FALSE"),              // Select pipelined mode, FALSE, TRUE
       .REFCLK_FREQUENCY(200.0),        // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
       .SIGNAL_PATTERN("DATA")          // DATA, CLOCK input signal
    )
    IDELAYE2_SYNC (
       .CNTVALUEOUT(),              // 5-bit output: Counter value output
       .DATAOUT(SYNCTOSERDES),          // 1-bit output: Delayed data output
       .C(CLK),                     // 1-bit input: Clock input
       .CE(1'b0),                    // 1-bit input: Active high enable increment/decrement input
       .CINVCTRL(1'b0),                 // 1-bit input: Dynamic clock inversion input
       .CNTVALUEIN(TRGBUSDLY[4:0]),    // 5-bit input: Counter value input
       .DATAIN(),                   // 1-bit input: Internal delay data input
       .IDATAIN(WSYNC),              // 1-bit input: Data input from the I/O
       .INC(1'b0),                  // 1-bit input: Increment / Decrement tap delay input
       .LD(TRGBUSDLY[15]),             // 1-bit input: Load IDELAY_VALUE input
       .LDPIPEEN(1'b1),             // 1-bit input: Enable PIPELINE register to load data input
       .REGRST(1'b0)                // 1-bit input: Active-high reset tap-delay input
    );

    //iserdes
    wire [7:0] SYNC_DATA;
    ISERDESE2
    #(
       .DATA_RATE("SDR"),
       .DATA_WIDTH(8),
       .INTERFACE_TYPE    ("NETWORKING"), 
       .DYN_CLKDIV_INV_EN ("FALSE"),
       .DYN_CLK_INV_EN    ("FALSE"),
       .NUM_CE            (2),
       .OFB_USED          ("FALSE"),
       .IOBDELAY          ("IFD"),                                // Use input at DDLY to output the data on Q
       .SERDES_MODE       ("MASTER")
    ) SYNC_SERDES (
       .Q1(SYNC_DATA[0]),
       .Q2(SYNC_DATA[1]),
       .Q3(SYNC_DATA[2]),
       .Q4(SYNC_DATA[3]),
       .Q5(SYNC_DATA[4]),
       .Q6(SYNC_DATA[5]),
       .Q7(SYNC_DATA[6]),
       .Q8(SYNC_DATA[7]),
       .SHIFTOUT1(),
       .SHIFTOUT2(),
       .BITSLIP(1'b0),
       .CE1(1'b1),
       .CE2(1'b1),
       .CLK(FASTCLK),
       .CLKB(~FASTCLK),
       .CLKDIV(CLK),
       .CLKDIVP(1'b0),
       .D(1'b0),
       .DDLY(SYNCTOSERDES),
       .RST(SERDES_RESET),
       .SHIFTIN1(1'b0),
       .SHIFTIN2(1'b0),
       // unused connections
       .DYNCLKDIVSEL(1'b0),
       .DYNCLKSEL(1'b0),
       .OFB(1'b0),
       .OCLK(1'b0),
       .OCLKB(1'b0),
       .O()
    );

    //latch logic for SYNC edge
    reg [7:0] SYNC_DATA_REG;
    reg [7:0] SYNC_DATA_REG_REG;
    reg SYNC_IN_REG;
    always @(posedge CLK) begin
        SYNC_IN <= |SYNC_DATA;
        SYNC_IN_REG <= SYNC_IN;
        SYNC_DATA_REG <= SYNC_DATA;
        SYNC_DATA_REG_REG <= SYNC_DATA_REG;
    end
    
    always @(posedge CLK) begin
        if(SERDES_RESET) begin
            SYNC_RISING_WFM <= 8'b0;
            SYNC_FALLING_WFM <= 8'b0;
        end else begin
            if(SYNC_IN & ~SYNC_IN_REG) begin
                SYNC_RISING_WFM <= SYNC_DATA_REG;
            end
            if(~ SYNC_IN & SYNC_IN_REG) begin
                SYNC_FALLING_WFM <= SYNC_DATA_REG_REG;
            end
        end
    end
    
    // ------------- TRIGGER -------------
    // input buffer
    wire WTRG;
    IBUFDS #(
       .DIFF_TERM("TRUE"), // Differential Termination
       .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest perforrmance="FALSE"
       .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_TRG (
       .O(WTRG), // Buffer output
       .I(TRG_IN_P), // Diff_p buffer input (connect directly to top-level port)
       .IB(TRG_IN_N) // Diff_n buffer input (connect directly to top-level port)
    );

    // IDLY    
    wire TRGTOREG;
    (* IODELAY_GROUP = "delay_group" *) // Specifies group name for associated IDELAYs/ODELAYs and IDELAYCTRL
        
    IDELAYE2 #(
       .CINVCTRL_SEL("FALSE"),          // Enable dynamic clock inversion (FALSE, TRUE)
       .DELAY_SRC("IDATAIN"),           // Delay input (IDATAIN, DATAIN)
       .HIGH_PERFORMANCE_MODE("TRUE"), // Reduced jitter ("TRUE"), Reduced power ("FALSE")
       .IDELAY_TYPE("VAR_LOAD"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
       .IDELAY_VALUE(0),                // Input delay tap setting (0-31)
       .PIPE_SEL("FALSE"),              // Select pipelined mode, FALSE, TRUE
       .REFCLK_FREQUENCY(200.0),        // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
       .SIGNAL_PATTERN("DATA")          // DATA, CLOCK input signal
    )
    IDELAYE2_TRG (
       .CNTVALUEOUT(),              // 5-bit output: Counter value output
       .DATAOUT(TRGTOREG),          // 1-bit output: Delayed data output
       .C(CLK),                     // 1-bit input: Clock input
       .CE(1'b0),                    // 1-bit input: Active high enable increment/decrement input
       .CINVCTRL(1'b0),                 // 1-bit input: Dynamic clock inversion input
       .CNTVALUEIN(TRGBUSDLY[9:5]),    // 5-bit input: Counter value input
       .DATAIN(),                   // 1-bit input: Internal delay data input
       .IDATAIN(WTRG),              // 1-bit input: Data input from the I/O
       .INC(1'b0),                  // 1-bit input: Increment / Decrement tap delay input
       .LD(TRGBUSDLY[15]),             // 1-bit input: Load IDELAY_VALUE input
       .LDPIPEEN(1'b1),             // 1-bit input: Enable PIPELINE register to load data input
       .REGRST(1'b0)                // 1-bit input: Active-high reset tap-delay input
    );

    //IOB register for TRG
    (* IOB = "TRUE"*)
    reg RTRG_IOB; 
    always @(posedge CLK) begin
        RTRG_IOB <= TRGTOREG;
        TRG_IN <= RTRG_IOB;
    end
    
    // ------------- SPARE -------------
    // input buffer
    wire WSPARE;
    IBUFDS #(
       .DIFF_TERM("TRUE"), // Differential Termination
       .IBUF_LOW_PWR("FALSE"), // Low power="TRUE", Highest perforrmance="FALSE"
       .IOSTANDARD("LVDS_25") // Specify the input I/O standard
    ) IBUFDS_SPARE (
       .O(WSPARE), // Buffer output
       .I(SPARE_IN_P), // Diff_p buffer input (connect directly to top-level port)
       .IB(SPARE_IN_N) // Diff_n buffer input (connect directly to top-level port)
    );
    // IDLY    
    wire SPARETOREG;
    (* IODELAY_GROUP = "delay_group" *) // Specifies group name for associated IDELAYs/ODELAYs and IDELAYCTRL
        
    IDELAYE2 #(
      .CINVCTRL_SEL("FALSE"),          // Enable dynamic clock inversion (FALSE, TRUE)
      .DELAY_SRC("IDATAIN"),           // Delay input (IDATAIN, DATAIN)
      .HIGH_PERFORMANCE_MODE("TRUE"), // Reduced jitter ("TRUE"), Reduced power ("FALSE")
      .IDELAY_TYPE("VAR_LOAD"),           // FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
      .IDELAY_VALUE(0),                // Input delay tap setting (0-31)
      .PIPE_SEL("FALSE"),              // Select pipelined mode, FALSE, TRUE
      .REFCLK_FREQUENCY(200.0),        // IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
      .SIGNAL_PATTERN("DATA")          // DATA, CLOCK input signal
    )
    IDELAYE2_SPARE (
      .CNTVALUEOUT(),              // 5-bit output: Counter value output
      .DATAOUT(SPARETOREG),          // 1-bit output: Delayed data output
      .C(CLK),                     // 1-bit input: Clock input
      .CE(1'b0),                    // 1-bit input: Active high enable increment/decrement input
      .CINVCTRL(1'b0),                 // 1-bit input: Dynamic clock inversion input
      .CNTVALUEIN(TRGBUSDLY[14:10]),    // 5-bit input: Counter value input
      .DATAIN(),                   // 1-bit input: Internal delay data input
      .IDATAIN(WSPARE),              // 1-bit input: Data input from the I/O
      .INC(1'b0),                  // 1-bit input: Increment / Decrement tap delay input
      .LD(TRGBUSDLY[15]),             // 1-bit input: Load IDELAY_VALUE input
      .LDPIPEEN(1'b1),             // 1-bit input: Enable PIPELINE register to load data input
      .REGRST(1'b0)                // 1-bit input: Active-high reset tap-delay input
    );
   
    //IOB register for SPARE
    (* IOB = "TRUE"*)
    reg RSPARE_IOB; 
    always @(posedge CLK) begin
        RSPARE_IOB <= SPARETOREG;
    end

    //SERIAL INFO fsm
    wire [15-`TRITYPDIM-`TRGFAMNUM-1:0] BLANK; // -1 IS FOR SYS_ENADC
    TRGLINK_RX #(.datasize(16+32)) TRGLINK_RX_SPARE_BLOCK(
        .CLK(CLK),
        .SYNC(SYNC),
        .ENABLE(1'b1),
        .TRGIN(RSPARE_IOB),
        .TRGOUT(),
        .DATA({SYS_ENADC,SYS_ROENA,BLANK,SYS_TRITYPE,SYS_EVECOU}),
        .VALID(SYS_VALID),
        .ERROR(SYS_ERROR)
    );

    

endmodule
