//TRIGGER bus TRG output driver signal with serialization

module TRGLINK_TX(
TRGIN, //Trigger generation signal
ENABLE, //if 0 just register TRGIN else serialize DATA
CLK,   //100MHz CLK
SYNC,  //Signal used as reset
TRGOUT,//Trigger bus signal output
DATA,  //Data to be serialized
ENGETRI);  //TRIGGER ENBALE (DISABLED DURING SERIALISATION)

parameter datasize = 32; //serialized word size
parameter invert = 0; // if spare line used need to be inverted for ancillary issues
parameter parity_enable = 1; // enable parity bit

//in out ports
input TRGIN;
input ENABLE;
input CLK;
input SYNC;
output reg TRGOUT;
input [datasize-1:0] DATA; 
output ENGETRI;

//registers
reg [1:0] STATE = 0;
reg [datasize-1:0] SHIFTREG;
reg [logb2(datasize)-1:0] COUNTER;
reg PARITY;
reg [1:0] WAITREG;

//internal states
parameter WAIT = 2'b00;
parameter TRIG = 2'b01;
parameter SERIALIZE = 2'b10;
parameter RESET = 2'b11;

assign ENGETRI = (STATE == WAIT); 

always @(posedge CLK) begin
    //SERIAL FSM
	if(SYNC) begin
		STATE <= WAIT;
		TRGOUT<=(invert==0)?1'b0:1'b1;
		SHIFTREG <= 0;
		COUNTER <= datasize;
		PARITY<= 1'b0;
		WAITREG<= 2'b11;
	end else begin
		case(STATE) 
			WAIT: if(TRGIN) begin
				STATE<=TRIG;
				TRGOUT<=(invert==0)?1'b1:1'b0;
				PARITY<= 1'b0;
				WAITREG<= 2'b11;
			end else begin
			    TRGOUT<=(invert==0)?1'b0:1'b1;
			    PARITY<= 1'b0;
			end
			TRIG:
			if(WAITREG == 0) begin
		        TRGOUT<=(invert==0)?1'b0:1'b1;
				COUNTER<=datasize;
				STATE<=SERIALIZE;
				PARITY<= 1'b0;
				WAITREG<= 2'b11;
		    end else begin
		        if (WAITREG == 2'b11) SHIFTREG <=DATA;
				WAITREG <= WAITREG -1;
		    end
			SERIALIZE:
			if(WAITREG == 0) begin
			    if(COUNTER ==0) begin
			        if(parity_enable==1) TRGOUT<=(invert==0)?PARITY:~PARITY;
			        else TRGOUT<=(invert==0)?1'b0:1'b1;
			        PARITY<= 1'b0;
				    STATE<=RESET;
			    end else begin
			        if(invert==0) begin
			    	    if(SHIFTREG[datasize-1]) PARITY <= ~PARITY;
			    	    else PARITY <= PARITY;
			        end else begin
			            if(~SHIFTREG[datasize-1]) PARITY <= ~PARITY;
                        else PARITY <= PARITY;
			        end
			        TRGOUT<=(invert==0)?SHIFTREG[datasize-1]:~SHIFTREG[datasize-1];
				    SHIFTREG[datasize-1:1]<=SHIFTREG[datasize-2:0];
				    SHIFTREG[0]<=1'b0;
				    COUNTER = COUNTER -1;
				end
				WAITREG<= 2'b11;
			end else begin
			    WAITREG <= WAITREG -1;
			end
			RESET:
			if(WAITREG == 0) begin
				TRGOUT<=(invert==0)?1'b0:1'b1;
				SHIFTREG <= 0;
				COUNTER <= datasize;
				PARITY<= 1'b0;
				STATE <= WAIT;
				WAITREG<= 2'b11;
            end else begin
                WAITREG <= WAITREG -1;
            end
		endcase
	end
	
	//TRGIN pass-trough register
	if(~ENABLE) TRGOUT <= TRGIN;
end

//log2 function to calculate the COUNTER size
function integer logb2;
	input integer n;
	integer i;
	begin
		i=1;
		for(logb2=0; i<=n; logb2=logb2+1) i = i <<1;

	end
endfunction

endmodule
