`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 20.06.2017 18:40:26
// Design Name: 
// Module Name: NEWRAM
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module NEWRAM(
    input CLK,
    input CLKDATA,
    input ENA,
    input RCLK,
    input RECORD,
    input WENA,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [SIZE-1:0] INADDR,
    input [31:0] WDATA,
    output tri [31:0] RDATA,
    input [31:0] DIN,
    output [31:0] DOUT
    );

    parameter [31:0] BASEADDR = 'h1000;
    parameter SIZE = 10;
    parameter USECLKDATA = 0;
    
    (* ram_style="block" *) reg [31:0]ram[2**SIZE-1:0];
    reg [31:0] RDATA_REG;
    reg [31:0] DOUT_REG;
    assign DOUT = DOUT_REG;
    reg OUT;

    reg RCLK_OLD;
    wire ENB;
    always @(posedge CLK) begin
        RCLK_OLD<=RCLK;
    end
    assign ENB = RCLK & ~RCLK_OLD;
    //assign ENB = RCLK;//may speed up
    //optionally select another clock for the data input
    wire CLKD;
    assign CLKD = (USECLKDATA)?CLKDATA:CLK;
    
    wire READ;
    wire WRITE;
    assign READ = (BASEADDR[31:SIZE]==RADDR[31:SIZE]);
    assign WRITE = (BASEADDR[31:SIZE]==WADDR[31:SIZE]) & WENA;
    wire [SIZE-1:0]ADDR;
    assign ADDR = (READ)?RADDR[SIZE-1:0]:WADDR[SIZE-1:0];

    always@(posedge CLKD)begin
        if(ENA)
        begin
            if(RECORD)
                ram[INADDR]<=DIN;
            DOUT_REG<=ram[INADDR];
        end
    end
    always@(posedge CLK)begin
        if(ENB & (READ | WRITE))
        begin
            if(WRITE) begin
                ram[ADDR]<=WDATA;
            end
            RDATA_REG<=ram[ADDR];
        end
    end
    
    //assign DOUT=(ENA)?ram[INADDR]:32'b0;
    
    always @(posedge CLK) begin
        if(ENB)
        begin
            if(READ) begin
               OUT<=1'b1;
            end else OUT<=1'b0;
        end
    end
    
    assign RDATA=(OUT)?RDATA_REG:32'hZZZZZZZZ;

    
endmodule
