`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2019 11:04:29
// Design Name: 
// Module Name: XECTDCSTRETCH
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module stretches XEC TDC hits by 2 to allow coincidence
// with TC
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module XECTDCSTRETCH(
    input CLK,
    //input quantities
    input HITIN,
    input [4:0] NTDCIN,
    input [7:0] TIMEIN,
    //parameters
    input [7:0] ADDVAL,
    //outputs
    output HITOUT,
    output [4:0] NTDCOUT,
    output [8:0] TIMEOUT //need one more bit here
    );

    reg OLDHIT;
    reg [7:0] TIMESUM;
    reg [4:0] NTDCOLD;
    always @(posedge CLK) begin
       OLDHIT <= HITIN;
       TIMESUM <= TIMEIN + ADDVAL;
       NTDCOLD <= NTDCIN;
    end

    assign HITOUT = HITIN | OLDHIT;
    assign NTDCOUT = (OLDHIT==1'b1)? NTDCOLD : (HITIN)? NTDCIN : 5'b0;
    assign TIMEOUT = (OLDHIT==1'b1)? TIMESUM : (HITIN)? TIMEIN : 9'b0;
    
endmodule
