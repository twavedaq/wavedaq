`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 13.02.2017 15:39:43
// Design Name: 
// Module Name: ALLTRGCOU
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module ALLTRGCOU(
    input [`NTRG-1:0] ISTRG,
    output [32*`NTRG-1:0] TRGCOUNTER,
    input SYNC,
    input RUNMODE,
    input CLK,
    input [`NTRG-1:0] TRGCOUMODE //TRGCOUMODE IS ASSIGNED GENT AND IS RUNMODE IF DRS USED OR 1 IF ONLY DISCRIMINATOR TRG
    );
    
    TRGCOUNTERS_BLOCK TRGCOUNTERS_INST[`NTRG-1:0](
        .ISTRG(ISTRG), // it was trimod when shaper used
        .SYNC(SYNC),
        .RUNMODE(RUNMODE),
        .TRGCOUNTER(TRGCOUNTER),
        .CLK(CLK),
        .TRGCOUMODE(TRGCOUMODE)
    );
endmodule
