`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 01.12.2016 11:10:30
// Design Name: 
// Module Name: SIMPLETCTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module SIMPLETCTRG(
    input [1023:0] DATA,
    input CLK,
    output reg OR,
    output reg [3:0] TRACKHIT,
    output reg [5*4-1:0] TRACKTILETIME,
    output reg [9*4-1:0] TRACKTILEID,
    output reg [6:0] TCNHIT
    );

   wire [8*2-1:0] TRACKTILEIDUSFROMTCB;
   wire [5*2-1:0] TRACKTILETIMEUSFROMTCB;
   wire [2-1:0] TRACKHITUSFROMTCB;
   wire [8*2-1:0] TRACKTILEIDDSFROMTCB;
   wire [5*2-1:0] TRACKTILETIMEDSFROMTCB;
   wire [2-1:0] TRACKHITDSFROMTCB;
   wire ORFROMTCB;
   wire [6:0] TCNHITFROMTCB;

   SERDESDECODE #(
      .SLOT(`TCSLOT),
      .NSLOT(16)
   ) DECODE (
      .DATA(DATA),
      .WFMSUM(),
      .MAX(),
      .TDCSUM(),
      .TDCNUM(),
      .TCOR(ORFROMTCB),
      .TCHITUS(TRACKHITUSFROMTCB),
      .TCTILEIDUS(TRACKTILEIDUSFROMTCB),
      .TCTILETIMEUS(TRACKTILETIMEUSFROMTCB),
      .TCHITDS(TRACKHITDSFROMTCB),
      .TCTILEIDDS(TRACKTILEIDDSFROMTCB),
      .TCTILETIMEDS(TRACKTILETIMEDSFROMTCB),
      .TCNHIT(TCNHITFROMTCB),
      .CRCPAIRTRG(),
      .CRCSINGLETRG(),
      .BGOPRESHCOUNTER(),
      .NGENTRG(),
      //.RDCPLASTICLYSOSINGLE(),
      //.BGOVETOFIRE(),
      .ALFATRG(),
      .MONITORDCTRG(),
      .PROTONCURRENTTRG(),
      .RFACCELTRG(),
      .XECLEDMPPCTRG(),
      .XECLEDPMTTRG(),
      .TCDIODETRG(),
      .TCLASERTRG(),
      .BGOTHRFIRE(),
      .BGOCOSM(),
      .BGOTRG(),
      .RDCPLASTICSINGLE(),
      .RDCLYSOOR(),
      .RDCLYSOTHRFIRE(),
      .RDCTRG()
   );
    
    always @(posedge CLK) begin
        OR <= ORFROMTCB;
        TRACKHIT <= {TRACKHITDSFROMTCB,TRACKHITUSFROMTCB};
        TRACKTILETIME <= {TRACKTILETIMEDSFROMTCB,TRACKTILETIMEUSFROMTCB};
        //extend tile id
        TRACKTILEID[9*0+:9] <= {1'b0,TRACKTILEIDUSFROMTCB[8*0+:8]};
        TRACKTILEID[9*1+:9] <= {1'b0,TRACKTILEIDUSFROMTCB[8*1+:8]};
        TRACKTILEID[9*2+:9] <= {1'b1,TRACKTILEIDDSFROMTCB[8*0+:8]};
        TRACKTILEID[9*3+:9] <= {1'b1,TRACKTILEIDDSFROMTCB[8*1+:8]};
        TCNHIT <= DATA[64*`TCSLOT+57 +:7];
    end
    
endmodule
