`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 16.02.2017 14:10:34
// Design Name: 
// Module Name: TRI_PROCESS
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module TRI_PROCESS(
    input CLK,
    input ALGCLK,
    input PEDCLK,
    input RUNMODE,
    input SYNC,
    //input data
    input [1023:0] DATA,
    //parameters
    input signed [31:0] QHTHR,
    input signed [31:0] QLTHR,
    input signed [31:0] QCTHR,
    input signed [31:0] QNGENTHR_LOW,
    input signed [31:0] QNGENTHR_HIGH,
    input [31:0] QSUMSEL,
    input [31:0] RLXe_PATCH,
    input [31:0] RTIMN,
    input [31:0] RTIMW,
    input [31:0] TCHITTHR,
    //LXe quantities
    output [34:0] LXeQSUM,
    output [7:0] LXeMAX,
    output [4:0] LXeTDCNUM,
    output [7:0] LXeTDCSUM,
    output LXe_QH,
    output LXe_QL,
    output LXe_QH_VETO,
    output LXe_QL_VETO,
    output LXe_QCOSM,
    output LXe_QNGEN,
    output LXe_PATCH,
    //TC quantities
    output TC_SINGLE,
    output TC_MULTIPLICITY,
    //Combined quantities
    output [4-1:0] DM_NARROW,
    output [4-1:0] DM_WIDE,
    output [4-1:0] TIMEN,
    output [4-1:0] TIMEW,
    //others
    output T_PED,
    output T_ALPHA,
    output RDCTRG,
    output RDCPLASTICSINGLE,
    output RDCLYSOOR,
    output RDCLYSOTHRFIRE,
    output BGOPRESHCOUNTER,
    output BGOTRG,
    output BGOCOSM,
    output BGOTHRFIRE,
    output TCLASERTRG,
    output TCDIODETRG,
    output XECLEDPMTTRG,
    output XECLEDMPPCTRG,
    output RFACCELTRG,
    output PROTONCURRENTTRG,
    output MONITORDCTRG,
    output NGENTRG,
    output CRCSINGLETRG,
    output CRCPAIRTRG,
    //bus for memories
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA,
    //single crate trigger
    input [4:0] SINGLECRATE_SHAPER_VAL,
    input [4:0] SINGLECRATE_SHAPER_VAL_VETO,
    input [255:0] SINGLECRATE_ISVETO,
    input [255:0] SINGLECRATE_MASK,
    input [127:0] SINGLECRATE_TWOCHANNEL_SEL,
    input SINGLECRATE_LOGIC_SEL,
    output SINGLECRATETRG,
    //FOOT patameters
    input [31:0] MAJTRIG,
    input [63:0] MASKTOFX,
    input [63:0] MASKTOFY,
    input [31:0] INTERSPILLDELAY,
    //FOOT triggers
    output MARGARITATRG,
    output MARGARITASINGLE,
    output TOFTRG,
    output [1:0] FOOTTRG,
    output INTERSPILLTRG,
    output TRIGBACKTRG,
    //SCIFI scaler data
    output [(32*(`SCIFIFIBERS+1))-1:0] SCIFIDATA,
    output SCIFITRG
    );

//LXe PROCESSING    
WFMTRG WFMTRGBLOCK(
    .DATA(DATA),
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    .SUM(LXeQSUM),
    .MAX(LXeMAX),
    .TDCNUM(LXeTDCNUM),
    .TDCSUM(LXeTDCSUM),
    .QSUMSEL(QSUMSEL)
);
// AT THE END DISCRIMINATION WITH QHTHR
wire LXe_QH_PROMPT;
DISCRIMINATEWFM #(
    .LENGTH(10)
) QHDISCR(
    .INPUT(LXeQSUM[34:3]),
    .THR(QHTHR),
    .CLK(ALGCLK),
    .TRIG(LXe_QH_PROMPT)
);
reg [3:0] LXe_QH_DLY;
always @(posedge ALGCLK) begin
    LXe_QH_DLY <= {LXe_QH_DLY[2:0], LXe_QH_PROMPT};
end
assign LXe_QH = LXe_QH_DLY[3];

DISCRVETO QHVETO(
    .INPUT(LXeQSUM[34:3]),
    .THR(QCTHR),
    .THR_TOVETO(QHTHR),
    .DISCR_TOVETO(LXe_QH),
    .CLK(ALGCLK),
    .TRIG(LXe_QH_VETO)
);

// AT THE END DISCRIMINATION WITH QLTHR
wire LXe_QL_PROMPT;
DISCRIMINATEWFM #(
    .LENGTH(10)
) QLDISCR(
    .INPUT(LXeQSUM[34:3]),
    .THR(QLTHR),
    .CLK(ALGCLK),
    .TRIG(LXe_QL_PROMPT)
);

reg [3:0] LXe_QL_DLY;
always @(posedge ALGCLK) begin
    LXe_QL_DLY <= {LXe_QL_DLY[2:0], LXe_QL_PROMPT};
end
assign LXe_QL = LXe_QL_DLY[3];

DISCRVETO QLVETO(
    .INPUT(LXeQSUM[34:3]),
    .THR(QCTHR),
    .THR_TOVETO(QLTHR),
    .DISCR_TOVETO(LXe_QL),
    .CLK(ALGCLK),
    .TRIG(LXe_QL_VETO)
);

// AT THE END DISCRIMINATION WITH QCTHR
DISCRIMINATEWFM #(
    .LENGTH(20)
) QCDISCR (
    .INPUT(LXeQSUM[34:3]),
    .THR(QCTHR),
    .CLK(ALGCLK),
    .TRIG(LXe_QCOSM)
);

// LXe MAX Id COMPARISON WITH PATCHES
// Register MAX when QSUM above threshold
reg [7:0] LXeMAX_REG;
reg LXeMAX_VALID;
always @(posedge CLK) begin
   if(~LXe_QH) begin
      LXeMAX_REG <= LXeMAX;
      LXeMAX_VALID <= 1'b1;
   end else begin
      LXeMAX_VALID <= 1'b0;
   end
end

wire LXe_PATCH_PROMPT;
ONELUT  #(.BASEADDR(`LXePatchAddr))
    LXePatch_inst (
    .DATAIN({RLXe_PATCH[6:0], LXeMAX_REG}),
    .CLK(CLK),
    .DATAOUT(LXe_PATCH_PROMPT),
    .RCLK(RCLK),
    .WENA(WENA),
    .RADDR(RADDR),
    .WADDR(WADDR),
    .WDATA(WDATA),
    .RDATA(RDATA)
    );

//DLY to align patch algorithm with SUM WFM
//To be changed with WD2G
reg [13:0] LXe_PATCH_DLY;
assign LXe_PATCH = LXe_PATCH_DLY[13];
always @(posedge CLK) begin
   LXe_PATCH_DLY <= {LXe_PATCH_DLY[12:0], LXe_PATCH_PROMPT};
end

// DISCRIMINATION FOR NEUTRON GENERATOR
wire LXe_QNGEN_PROMPT;
DISCRIMINATEWFM #(
    .LENGTH(10)
) QNGENDISCR(
    .INPUT(LXeQSUM[34:3]),
    .THR(QNGENTHR_LOW),
    .CLK(ALGCLK),
    .TRIG(LXe_QNGEN_PROMPT)
);

reg [3:0] LXe_QNGEN_DLY;
always @(posedge ALGCLK) begin
    LXe_QNGEN_DLY <= {LXe_QNGEN_DLY[2:0], LXe_QNGEN_PROMPT};
end

wire LXe_QNGEN_VETO;
DISCRVETO QNGENVETO(
    .INPUT(LXeQSUM[34:3]),
    .THR(QNGENTHR_HIGH),
    .THR_TOVETO(QNGENTHR_LOW),
    .DISCR_TOVETO(LXe_QNGEN_DLY[3]),
    .CLK(ALGCLK),
    .TRIG(LXe_QNGEN_VETO)
);

assign LXe_QNGEN = LXe_QNGEN_DLY[3] & ~(LXe_QNGEN_VETO);

//TC PROCESSING
wire TC_SINGLE_PROMPT;
wire [6:0] TC_NHIT;
wire [3:0] TC_TRACKHIT;
wire [5*4-1:0] TC_TRACKTILETIME;
wire [9*4-1:0] TC_TRACKTILEID;

SIMPLETCTRG SIMPLETCTRGBLOCK(
     .DATA(DATA),
     .CLK(CLK),
     .OR(TC_SINGLE_PROMPT),
     .TRACKHIT(TC_TRACKHIT),
     .TRACKTILETIME(TC_TRACKTILETIME),
     .TRACKTILEID(TC_TRACKTILEID),
     .TCNHIT(TC_NHIT)
);

// TC SINGLE SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
TCSINGLESHAPER(
  .DIN(TC_SINGLE_PROMPT),
  .DOUT(TC_SINGLE),
  .CLK(ALGCLK)  
);

//TC MULTIPLICITY THRESHOLD
reg TC_MULTIPLICITY_PROMPT;
always @(posedge CLK) begin
    if (TC_NHIT >= TCHITTHR && TCHITTHR != 0) begin
       TC_MULTIPLICITY_PROMPT <= 1'b1;
    end else begin
       TC_MULTIPLICITY_PROMPT <= 1'b0;
    end
end
// TC MULTIPLICITY SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
TCMULTSHAPER(
  .DIN(TC_MULTIPLICITY_PROMPT),
  .DOUT(TC_MULTIPLICITY),
  .CLK(ALGCLK)  
);

// AUX DECODING
wire T_ALPHA_PROMPT;
wire TCLASERTRG_PROMPT;
wire TCDIODETRG_PROMPT;
AUXTRG AUXTRGBLOCK(
    .DATA(DATA),
    .CLK(CLK),
    .BGOCOSM(BGOCOSM),
    .BGOTHRFIRE(BGOTHRFIRE),
    .BGOPRESHCOUNTER(BGOPRESHCOUNTER),
    .BGOTRG(BGOTRG),
    .RDCPLASTICSINGLE(RDCPLASTICSINGLE),
    .RDCLYSOOR(RDCLYSOOR),
    .RDCLYSOTHRFIRE(RDCLYSOTHRFIRE),
    .RDCTRG(RDCTRG),
    .TCLASERTRG(TCLASERTRG_PROMPT),
    .TCDIODETRG(TCDIODETRG_PROMPT),
    .XECLEDPMTTRG(XECLEDPMTTRG),
    .XECLEDMPPCTRG(XECLEDMPPCTRG),
    .RFACCELTRG(RFACCELTRG),
    .PROTONCURRENTTRG(PROTONCURRENTTRG),
    .MONITORDCTRG(MONITORDCTRG),
    .ALFATRG(T_ALPHA_PROMPT),
    .NGENTRG(NGENTRG),
    .CRCSINGLETRG(CRCSINGLETRG),
    .CRCPAIRTRG(CRCPAIRTRG)
);

// ALPHA SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
ALPHASHAPER(
  .DIN(T_ALPHA_PROMPT),
  .DOUT(T_ALPHA),
  .CLK(ALGCLK)  
);

// TC LASER SYNC FROM AUX SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
TCLASERTRGSHAPER(
  .DIN(TCLASERTRG_PROMPT),
  .DOUT(TCLASERTRG),
  .CLK(ALGCLK)  
);
// TC LASER DIODE FROM AUX SHAPER
SHAPER #(.SRLENGTH(`SRSIZE))
TCDIODETRGSHAPER(
  .DIN(TCDIODETRG_PROMPT),
  .DOUT(TCDIODETRG),
  .CLK(ALGCLK)  
);

/////////////////////////
// SINGLE CRATE LOGIC
SINGLECRATE_TRG SINGLECRATE(
    .DATA(DATA),
    .SHAPER_VAL(SINGLECRATE_SHAPER_VAL),
    .SHAPER_VAL_VETO(SINGLECRATE_SHAPER_VAL_VETO),
    .ISVETO(SINGLECRATE_ISVETO),
    .MASK(SINGLECRATE_MASK),
    .CLK(CLK),
    .TWOCHANNEL_SEL(SINGLECRATE_TWOCHANNEL_SEL),
    .LOGIC_SEL(SINGLECRATE_LOGIC_SEL),
    .TRG(SINGLECRATETRG)
    );

/////////////////////////
// MARGARITA TRIGGER LOGIC
MARGARITA_BLOCK MARGARITA(
    .DATA(DATA),
    .SHAPER_VAL(SINGLECRATE_SHAPER_VAL),
    .SHAPER_VAL_VETO(SINGLECRATE_SHAPER_VAL_VETO),
    .CLK(CLK),
    .MAJTRIG(MAJTRIG),
    .MASKTOFX(MASKTOFX),
    .MASKTOFY(MASKTOFY),
    .MARGTRGOR(MARGARITASINGLE),
    .MARGTRG(MARGARITATRG),
    .TOFTRG(TOFTRG),
    .FOOTTRG(FOOTTRG),
    .INTERSPILLDELAY(INTERSPILLDELAY),
    .INTERSPILLTRG(INTERSPILLTRG),
    .TRIGBACKTRG(TRIGBACKTRG)
);

/////////////////////////
// SCIFI LOGIC WITH FIBER HIT COUNTER
SCIFI_BLOCK SCIFI(
    .DATA(DATA),
    .SHAPER_VAL(SINGLECRATE_SHAPER_VAL),
    .CLK(CLK),
    .RUNMODE(RUNMODE),
    .SYNC(SYNC),
    .SCIFITRG(SCIFITRG),
    .SCIFIDATA(SCIFIDATA)
);

// LUT for direction match narrow
wire [3:0] DM_NARROW_PROMPT;
wire [3:0] DM_WIDE_PROMPT;
reg  [3:0] DM_VALID;

genvar itrack;
for(itrack = 0; itrack<4; itrack = itrack+1) begin
   always @(posedge CLK) begin
      DM_VALID[itrack] <= TC_TRACKHIT[itrack] & LXeMAX_VALID;
   end

   // LUT DM NARROW INSTANTIATION
   DMLUT #(.BASEADDR(`DMNarrowAddr+itrack*32'h1000))
        DirMatchNLUT (
        .LXeID(LXeMAX_REG),
        .TCID(TC_TRACKTILEID[9*itrack+:9]),
        .ISDM(DM_NARROW_PROMPT[itrack]),
        .CLK(CLK),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .WDATA(WDATA),
        .RDATA(RDATA)
       );
   // DM NARROW SHAPER
   SHAPER #(.SRLENGTH(`SRSIZE))
   DMNSHAPER(
     .DIN(DM_NARROW_PROMPT[itrack] & DM_VALID[itrack]),
     .DOUT(DM_NARROW[itrack]),
     .CLK(ALGCLK)  
   );

   // LUT DMWIDE INSTANTIATION
   DMLUT #(.BASEADDR(`DMWideAddr+itrack*32'h1000))
        DirMatchWLUT (
        .LXeID(LXeMAX_REG),
        .TCID(TC_TRACKTILEID[9*itrack+:9]),
        .ISDM(DM_WIDE_PROMPT[itrack]),
        .CLK(CLK),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .WDATA(WDATA),
        .RDATA(RDATA)
       );
   // DM WIDE SHAPER
   SHAPER #(.SRLENGTH(`SRSIZE))
   DMWSHAPER(
     .DIN(DM_WIDE_PROMPT[itrack]&DM_VALID[itrack]),
     .DOUT(DM_WIDE[itrack]),
     .CLK(ALGCLK)  
   );
end

// LXe - TC TIME CONICIDENCES
wire [4-1:0] TIMEN_PROMPT;
wire [4-1:0] TIMEW_PROMPT;

//stretch XEC time for coincidence
wire [8:0] LXeTDCSUMStretched;
wire [4:0] LXeTDCNUMStretched;
wire LXeHasHit;
XECTDCSTRETCH XECSTRETCH(
   .CLK(CLK),
   .HITIN(LXeTDCNUM!=0),
   .NTDCIN(LXeTDCNUM),
   .TIMEIN(LXeTDCSUM),
   .ADDVAL({LXeTDCNUM, 3'b000}),
   .HITOUT(LXeHasHit),
   .NTDCOUT(LXeTDCNUMStretched),
   .TIMEOUT(LXeTDCSUMStretched)
);

genvar itime;
for(itime = 0; itime<4; itime = itime+1) begin
    //stretch each TC time
    wire track_hit;
    wire [5:0] track_time;
    TCTDCSTRETCH TCSTRETCH(
       .CLK(CLK),
       .HITIN(TC_TRACKHIT[itime]),
       .TIMEIN(TC_TRACKTILETIME[5*itime+:5]),
       .ADDVAL(5'b10000),
       .HITOUT(track_hit),
       .TIMEOUT(track_time)
    );

    //compare against thresholds
    TIMECOMPARE TIMECOMPAREN_BLOCK(
       .CLK(CLK),
       .LXETIME(LXeTDCSUMStretched),
       .LXENUM(LXeTDCNUMStretched),
       .LXEHIT(LXeHasHit),
       .TCTIME(track_time),
       .TCHIT(track_hit),
       .THR(RTIMN[5:0]),
       .ISTIME(TIMEN_PROMPT[itime])
    );
    TIMECOMPARE TIMECOMPAREW_BLOCK(
       .CLK(CLK),
       .LXETIME(LXeTDCSUMStretched),
       .LXENUM(LXeTDCNUMStretched),
       .LXEHIT(LXeHasHit),
       .TCTIME(track_time),
       .TCHIT(track_hit),
       .THR(RTIMW[5:0]),
       .ISTIME(TIMEW_PROMPT[itime])
    );

   // TIME NARROW SHAPER
   SHAPER #(.SRLENGTH(`SRSIZE)
   ) TMNSHAPER(
     .DIN(TIMEN_PROMPT[itime]),
     .DOUT(TIMEN[itime]),
     .CLK(ALGCLK)  
   );
  // TIME WIDE SHAPER
  SHAPER #(.SRLENGTH(`SRSIZE))
  TMWSHAPER(
    .DIN(TIMEW_PROMPT[itime]),
    .DOUT(TIMEW[itime]),
    .CLK(ALGCLK)  
  );    
end

//RANDOM TRIGGER
PEDBLOCK PEDBLOCK_INST(
     .PEDCLK(PEDCLK),
     .CLK(CLK),
     .ISTRG(T_PED)
); 
  
endmodule
