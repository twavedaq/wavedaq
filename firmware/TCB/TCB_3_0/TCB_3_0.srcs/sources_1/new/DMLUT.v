`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10.03.2017 15:22:50
// Design Name: 
// Module Name: DMLUT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module DMLUT(
    input CLK,
    input [7:0] LXeID,
    input [8:0] TCID,
    output reg ISDM,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input RCLK,
    input [31:0] WDATA,
    output [31:0] RDATA,
    input WENA
    );

    parameter [31:0] BASEADDR = 0;    
    wire [3:0] DM_PREMUX;
    genvar iLUT;
    for(iLUT = 0; iLUT<4; iLUT = iLUT + 1) begin
    // DM NARROW LUT INSTANTIATION
    ONELUT #(.BASEADDR(BASEADDR+iLUT*32'h400))
         DirMatchLUT (
        .DATAIN({TCID[6:0], LXeID[7:0]}),
        .CLK(CLK),
        .DATAOUT(DM_PREMUX[iLUT]),
        .RCLK(RCLK),
        .WENA(WENA),
        .RADDR(RADDR),
        .WADDR(WADDR),
        .WDATA(WDATA),
        .RDATA(RDATA)
        );
end        

always @(*) begin
    case(TCID[8:7])
        2'b00 : ISDM <= DM_PREMUX[0];
        2'b01 : ISDM <= DM_PREMUX[1];
        2'b10 : ISDM <= DM_PREMUX[2];
        2'b11 : ISDM <= DM_PREMUX[3];
    endcase
end
        
endmodule
