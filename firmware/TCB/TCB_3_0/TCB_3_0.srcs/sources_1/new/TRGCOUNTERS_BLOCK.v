`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.09.2015 11:00:39
// Design Name: 
// Module Name: TRGCOUNTERS_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TRGCOUNTERS_BLOCK(
    input ISTRG,
    input SYNC,
    input RUNMODE,
    output [31:0] TRGCOUNTER,
    input CLK,
    input TRGCOUMODE
    );
reg [31:0] TRGCOUNTER_TEMP;
reg [31:0] TRGCOUNTER_REG;
reg ISTRG_OLD;
reg SYNC_OLD;
reg RUNMODE_OLD;
//COUNTER INCREASED BY TRIMOD
always @(posedge CLK) begin
    SYNC_OLD <= SYNC;
    ISTRG_OLD <= ISTRG;
    if(SYNC & ~SYNC_OLD)        //posedge SYNC
        TRGCOUNTER_TEMP <= 0;
    else if(ISTRG & ~ISTRG_OLD & TRGCOUMODE) //posedge ISTRG AND RGCOUMODE (FOR EXPLANATION SEE ALLTRGCOU PAGE)
        TRGCOUNTER_TEMP = TRGCOUNTER_TEMP + 1;
end
//NOW LATCH THE TRGCOUNTER 
always @(posedge CLK) begin
    RUNMODE_OLD <= RUNMODE;
    if(RUNMODE_OLD & ~RUNMODE) //negedge RUNMNODE
        TRGCOUNTER_REG <= TRGCOUNTER_TEMP;
end
assign TRGCOUNTER = TRGCOUNTER_REG;
endmodule
