`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07/26/2018 09:09:42 PM
// Design Name: 
// Module Name: PROG_SHAPER
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module PROG_SHAPER(
    input IN,
    output reg OUT,
    input CLK,
    input [VAL_SIZE-1:0] VAL
    );
    
    parameter VAL_SIZE = 5;
    
    reg IN_OLD;
    always @(posedge CLK) begin
        IN_OLD <= IN;
    end
    
    reg [VAL_SIZE-1:0] COUNT;
    wire END;
    
    assign END = (COUNT == 0);
    
    always @(posedge CLK) begin
        if(IN & ~(IN_OLD)) begin
            COUNT <= VAL;
        end else if(!END) begin
            COUNT <= COUNT - 1;
        end
    end
    
    always @(posedge CLK) begin
        if(VAL==0) begin
            OUT <= IN;
        end else begin
            if(IN & ~(IN_OLD)) begin
                OUT <=1'b1;
            end else if(END) begin
                OUT <=1'b0;
            end

        end
    end
    
    
endmodule
