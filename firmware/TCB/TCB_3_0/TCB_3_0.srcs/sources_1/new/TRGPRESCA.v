`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 10.09.2015 12:00:50
// Design Name: 
// Module Name: TRGPRESCA
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module TRGPRESCA(
    input CLK,
    input ALGCLK,
    input [`NTRG-1:0] TRIMOD,
    input [`NTRG-1:0] TRGFORCE,
    input [(32*`NTRG)-1:0] PRESCA,
    input RUNMODE,
    input [`NTRG-1:0] TRGENA,
    input SYNC,
    output GETRI,
    output [`NTRG-1:0] TRIPATTERN,
    output [`TRITYPDIM-1:0] TRITYPE,
    output reg [`TRGFAMNUM-1:0] ROENA,
    input ENGETRI,
    input [(6*`NTRG)-1:0] TRGDLY,
    input[31:0] PRESCAADC,
    output reg ENADC
    );

reg [`NTRG-1:0] TRIPATTERN_REG;
wire [`NTRG-1:0] TRIFIRE;
reg [`DLYSIZE-1:0] DLYGETRI;
reg [`NTRG*`SRSIZE-1:0] TRIMODREG; // this is a shift register to collect the trigger types in a event
//HERE WE INSTATIATE THE ONEPRESCA BLOCKS
ONEPRESCALER ONEPRESCA_BLOCK [`NTRG-1:0](
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    .TRGFORCE(TRGFORCE),
    .TRIMOD(TRIMOD),
    .PRESCA(PRESCA),
    .TRIPATTERN(TRIFIRE),
    .SYNC(SYNC),
    .MASK(TRGENA),
    .TRGDLY(TRGDLY)
);
// DLY BY 1 CLK TICK THE ENGETRI
reg ENGETRI_REG;
always @(posedge CLK) begin
    ENGETRI_REG <= ENGETRI;
end
// TRIGGER SIGNAL GENERATION
assign GETRI = |TRIFIRE & RUNMODE & ENGETRI_REG;

//PRESCALER FOR ADC DATA ENABLE BIT ON TRIGEGR BUS
wire ENADC_PROMPT;
ONEPRESCALER ADCPRESCA_BLOCK (
    .ALGCLK(ALGCLK),
    .CLK(CLK),
    .TRGFORCE(1'b0),
    .TRIMOD(GETRI), //counts on any valid getri
    .PRESCA(PRESCAADC),
    .TRIPATTERN(ENADC_PROMPT),
    .SYNC(SYNC),
    .MASK(1'b1),
    .TRGDLY(5'b0)
);

reg [`TRITYPDIM-1:0] TRITYPE_REG;
//TRIGGER TYPE ASSOCIATION WITH PRIORITY
integer iTRG;
reg GETRI_OLD;
always @(posedge CLK)
	begin
	GETRI_OLD <= GETRI;
	if(GETRI & ~GETRI_OLD) begin
        TRITYPE_REG <= 0;   
        for(iTRG=`NTRG-1; iTRG>=0; iTRG=iTRG-1) begin
            if(TRIFIRE[iTRG]) TRITYPE_REG <= iTRG;
        end
    end
end
// THIS IS TO COMMUNICATE WHICH TRIGGER FAMILY WAS GENERATED TO INSTRUCT TCB PACKETIZERS ACCORDINGLY
// bit meaning: AUX, DCH, RDC, TC, LXe, ALL
always @(TRITYPE_REG) begin
    case (TRITYPE_REG)
         0: ROENA <= 6'b000001; //MEG
         1: ROENA <= 6'b000001; //MEG LowQ
         2: ROENA <= 6'b000001; //MEG WideAngle
         3: ROENA <= 6'b000001; //MEG WideTime
         4: ROENA <= 6'b000001; //Radiative Narrow
         5: ROENA <= 6'b000001; //Radiative Wide
         6: ROENA <= 6'b001100; //Michel
        10: ROENA <= 6'b000010; //QH
        11: ROENA <= 6'b000010; //QL
        12: ROENA <= 6'b000010; //Alpha
        13: ROENA <= 6'b100010; //LED
        14: ROENA <= 6'b100010; //LED
        15: ROENA <= 6'b000010; //Cosmic
        20: ROENA <= 6'b000100; //Track
        21: ROENA <= 6'b000100; //Cosmic
        22: ROENA <= 6'b000100; //Single
        23: ROENA <= 6'b100100; //Laser
        30: ROENA <= 6'b001010; //RDC-LXe
        31: ROENA <= 6'b001000; //RDC
        32: ROENA <= 6'b001000; //RDC Plastic
        33: ROENA <= 6'b001000; //RDCLyso QSUM
        34: ROENA <= 6'b001000; //RDCLyso OR
        40: ROENA <= 6'b010000; //Track
        41: ROENA <= 6'b110000; //CRC-DCH
        42: ROENA <= 6'b010000; //Cosmics
        43: ROENA <= 6'b110000; //CRC pair
        44: ROENA <= 6'b100000; //CRC Single
        45: ROENA <= 6'b010000; //Single
        46: ROENA <= 6'b100000; //Monitor DCH
        47: ROENA <= 6'b100000; //Monitor DCH Counters
        50: ROENA <= 6'b100010; //Pi0
        51: ROENA <= 6'b100010; //Pi0 NoPresh
        52: ROENA <= 6'b100000; //BGO
        53: ROENA <= 6'b100000; //BGO Sum
        54: ROENA <= 6'b100000; //BGO Cosmics
        55: ROENA <= 6'b100000; //PreSh Single
        56: ROENA <= 6'b000110; //CW Boron
        57: ROENA <= 6'b100000; //SciFi
        58: ROENA <= 6'b100010; //Neutron
        59: ROENA <= 6'b110000; //RF
        63: ROENA <= 6'b111110; //Pedestal
        default: ROENA <= 6'b000000;
    endcase
    ENADC <= ENADC_PROMPT;
end
assign TRITYPE = TRITYPE_REG;
// here delay the getri by 50ns waiting for other algorithms to be executed
always @(posedge CLK) begin 
    DLYGETRI <= {DLYGETRI[`DLYSIZE-2:0],GETRI};
end
// then register the trimod when the dlygetri fires
integer itrg;
always @(posedge DLYGETRI[`DLYSIZE-1]) begin
    for(itrg = 0; itrg<`NTRG; itrg = itrg+1) begin
    	TRIPATTERN_REG[itrg] <= |TRIMODREG[itrg*`SRSIZE +: `SRSIZE];
	end
end
// NOW PLAY WITH THE TRIMOD SHIFT REGISTER
// THE SHIFT REGISTER IS ORGANISED IN BLOCKS OF 8 BITS
integer ishift;
always @(posedge CLK) begin
    for(ishift = 0; ishift<`NTRG; ishift = ishift+1) begin
    	TRIMODREG[ishift*`SRSIZE +: `SRSIZE] <= {TRIMOD[ishift]|TRGFORCE[ishift],TRIMODREG[(ishift)*`SRSIZE + 1 +: `SRSIZE-1]};
    end
end
assign TRIPATTERN = TRIPATTERN_REG;
endmodule
