`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 09.10.2018 09:49:43
// Design Name: 
// Module Name: FInterface
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module FInterface(
    input [3:0] TCB_RX1_D_P,
    input [3:0] TCB_RX1_D_N,
    output [3:0] TCB_TX1_D_P,
    output [3:0] TCB_TX1_D_N,
    input CLK,
    output FTRGOUT,
    output FBUSYOUT,
    output [31:0] FSTIME,
    input MASKFTRG,
    input MASKFBUSY,
    input TRGOUT,
    input SYSBUSY,
    input MARGTRG
);
    
wire FTRG, FBUSY, FRESCOU_IN, FSCLK_IN;
wire FRESCOU, FSCLK;

// RXTX1 RESERVED AS INTERFACE FOR OTHER DAQ    
// SIGNALS FROM [0:3] ARE INPUTS, THE OTHERS AS OUTPUT
//INPUTS
IBUFDS #(
    .DIFF_TERM("FALSE"),       // Differential Termination
    .IBUF_LOW_PWR("FALSE"),     // Low power="TRUE", Highest performance="FALSE" 
    .IOSTANDARD("LVDS")     // Specify the input I/O standard
) IBUFDSRTX1_inst[3:0] (
    .O({FTRG, FBUSY, FRESCOU_IN, FSCLK_IN}),  // Buffer output
    .I(TCB_RX1_D_P[3:0]),  // Diff_p buffer input (connect directly to top-level port)
    .IB(TCB_RX1_D_N[3:0]) // Diff_n buffer input (connect directly to top-level port)
);
// DEBOUNCER FOR FTRG SIGNAL    
Debouncer DTRG(
    .CLK(CLK),
    .IN(FTRG&MASKFTRG),
    .OUT(FTRGOUT)
);
// DEBOUNCER FOR FSCLK SIGNAL    
Debouncer DFSCLK(
    .CLK(CLK),
    .IN(FSCLK_IN),
    .OUT(FSCLK)
);
// DEBOUNCER FOR FTRG SIGNAL    
Debouncer DFRESCOU(
    .CLK(CLK),
    .IN(FRESCOU_IN),
    .OUT(FRESCOU)
);
// DEBOUNCER FOR FSCLK SIGNAL    
Debouncer DFBUSY(
    .CLK(CLK),
    .IN(FBUSY&MASKFBUSY),
    .OUT(FBUSYOUT)
);

//SYNCHRONISATION TIME COUNTER
reg [31:0] FSTIME_REG, FSTIME_LATCH;
reg FRESCOU_OLD, FSCLK_OLD;
reg TRGOUT_OLD;
always @(posedge CLK) begin
    FRESCOU_OLD <= FRESCOU;
    FSCLK_OLD <= FSCLK;
    if(FRESCOU & ~FRESCOU_OLD) //posedge FRESCOU
        FSTIME_REG <= 0;
    else
    if(~FSCLK_OLD & FSCLK) //posedge FSCLK
        FSTIME_REG <= FSTIME_REG+1;
end
//LATCH THE SYNCHRONISATIO TIME AFTER EACH EVENT
always @(posedge CLK) begin
    TRGOUT_OLD<= TRGOUT;
    // SYNCHRONOUS LATCH AT POSEDGE TRGOUT
    if(~TRGOUT_OLD & TRGOUT)
        FSTIME_LATCH <= FSTIME_REG;
end
assign FSTIME = FSTIME_LATCH;

//SHAPE TRGOUT TO BE 200NS BEFORE SENDING OUT
wire TRGOUTREG;
PROG_SHAPER #(
   .VAL_SIZE(5)
) TRGTOEXT(
   .IN(TRGOUT),
   .OUT(TRGOUTREG),
   .CLK(CLK),
   .VAL(5'b10000)
);

//SHAPE TRGOUT TO BE 200NS BEFORE SENDING OUT
wire MARGTRGREG;
PROG_SHAPER #(
   .VAL_SIZE(5)
) MERGTRGTOEXT(
   .IN(MARGTRG),
   .OUT(MARGTRGREG),
   .CLK(CLK),
   .VAL(5'b01000)
);

//OUTPUTS
OBUFDS #(
    .IOSTANDARD("LVDS"), // Specify the output I/O standard
    .SLEW("SLOW")           // Specify the output slew rate
) OBUFDS_RXTX1_D_P[3:0] (
    .O(TCB_TX1_D_P[3:0]),     // Diff_p output (connect directly to top-level port)
    .OB(TCB_TX1_D_N[3:0]),   // Diff_n output (connect directly to top-level port)
    .I({1'b0, MARGTRGREG, TRGOUTREG, SYSBUSY})      // Buffer input 
);
    
endmodule
