`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 17.02.2017 13:57:28
// Design Name: 
// Module Name: ONELUT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ONELUT(
    input [14:0] DATAIN,
    input CLK,
    output  DATAOUT,
    input [31:0] RADDR,
    input [31:0] WADDR,
    input [31:0] WDATA,
    input RCLK,
    output [31:0] RDATA,
    input WENA
    );
    
    parameter [31:0] BASEADDR = 32'h20000;

  wire PORTB_ENABLE;
 
  wire [31:0] RWADDR;
  assign RWADDR = (WENA)?WADDR:RADDR;
  assign PORTB_ENABLE = (RWADDR[31:10] == BASEADDR[31:10]);
  wire [31:0] RDATA_WIRE;
  assign RDATA = (PORTB_ENABLE)?RDATA_WIRE:32'bzzzzzzzz;

  
    // BRAM_SINGLE_MACRO : In order to incorporate this function into the design,
    //   Verilog   : the following instance declaration needs to be placed
    //  instance   : in the body of the design code.  The instance name
    // declaration : (BRAM_SINGLE_MACRO_inst) and/or the port declarations within the
    //    code     : parenthesis may be changed to properly reference and
    //             : connect this function to the design.  All inputs
    //             : and outputs must be connected.
    
    //  <-----Cut code below this line---->
    
       // BRAM_SINGLE_MACRO: Single Port RAM
       //                    Kintex-7
       // Xilinx HDL Language Template, version 2013.2
       
       /////////////////////////////////////////////////////////////////////
       //  READ_WIDTH | BRAM_SIZE | READ Depth  | ADDR Width |            //
       // WRITE_WIDTH |           | WRITE Depth |            |  WE Width  //
       // ============|===========|=============|============|============//
       //    37-72    |  "36Kb"   |      512    |    9-bit   |    8-bit   //
       //    19-36    |  "36Kb"   |     1024    |   10-bit   |    4-bit   //
       //    19-36    |  "18Kb"   |      512    |    9-bit   |    4-bit   //
       //    10-18    |  "36Kb"   |     2048    |   11-bit   |    2-bit   //
       //    10-18    |  "18Kb"   |     1024    |   10-bit   |    2-bit   //
       //     5-9     |  "36Kb"   |     4096    |   12-bit   |    1-bit   //
       //     5-9     |  "18Kb"   |     2048    |   11-bit   |    1-bit   //
       //     3-4     |  "36Kb"   |     8192    |   13-bit   |    1-bit   //
       //     3-4     |  "18Kb"   |     4096    |   12-bit   |    1-bit   //
       //       2     |  "36Kb"   |    16384    |   14-bit   |    1-bit   //
       //       2     |  "18Kb"   |     8192    |   13-bit   |    1-bit   //
       //       1     |  "36Kb"   |    32768    |   15-bit   |    1-bit   //
       //       1     |  "18Kb"   |    16384    |   14-bit   |    1-bit   //
       /////////////////////////////////////////////////////////////////////
    

    
       BRAM_TDP_MACRO #(
          .BRAM_SIZE("36Kb"), // Target BRAM: "18Kb" or "36Kb" 
          .DEVICE("7SERIES"), // Target device: "7SERIES" 
          .DOA_REG(0),        // Optional port A output register (0 or 1)
          .DOB_REG(0),        // Optional port B output register (0 or 1)
          .INIT_A(36'h0000000),  // Initial values on port A output port
          .INIT_B(36'h00000000), // Initial values on port B output port
          .INIT_FILE ("NONE"),
          .READ_WIDTH_A (1),   // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
          .READ_WIDTH_B (32),   // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
          .SIM_COLLISION_CHECK ("ALL"), // Collision check enable "ALL", "WARNING_ONLY", 
                                        //   "GENERATE_X_ONLY" or "NONE" 
          .SRVAL_A(36'h00000000), // Set/Reset value for port A output
          .SRVAL_B(36'h00000000), // Set/Reset value for port B output
          .WRITE_MODE_A("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE" 
          .WRITE_MODE_B("WRITE_FIRST"), // "WRITE_FIRST", "READ_FIRST", or "NO_CHANGE" 
          .WRITE_WIDTH_A(1), // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
          .WRITE_WIDTH_B(32) // Valid values are 1-36 (19-36 only valid when BRAM_SIZE="36Kb")
       ) LXePatch_inst (
          .DOA(DATAOUT),       // Output port-A data, width defined by READ_WIDTH_A parameter
          .DOB(RDATA_WIRE),       // Output port-B data, width defined by READ_WIDTH_B parameter
          .ADDRA(DATAIN),   // Input port-A address, width defined by Port A depth
          .ADDRB(RWADDR[9:0]),   // Input port-B address, width defined by Port B depth
          .CLKA(CLK),     // 1-bit input port-A clock
          .CLKB(RCLK),     // 1-bit input port-B clock
          .DIA(1'b0),       // Input port-A data, width defined by WRITE_WIDTH_A parameter
          .DIB(WDATA),       // Input port-B data, width defined by WRITE_WIDTH_B parameter
          .ENA(1'b1),       // 1-bit input port-A enable
          .ENB(1'b1),       // 1-bit input port-B enable
          .REGCEA(1), // 1-bit input port-A output register enable
          .REGCEB(1), // 1-bit input port-B output register enable
          .RSTA(0),     // 1-bit input port-A reset
          .RSTB(0),     // 1-bit input port-B reset
          .WEA(4'b0),       // Input port-A write enable, width defined by Port A depth
          .WEB({4{PORTB_ENABLE&WENA}})//,PORTB_ENABLE&WENABLE,PORTB_ENABLE&WENABLE,PORTB_ENABLE&WENABLE})        // Input port-B write enable, width defined by Port B depth
       );
    				
endmodule
