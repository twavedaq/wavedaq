`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 31.05.2019 11:04:29
// Design Name: 
// Module Name: TCTDCSTRETCH
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module stretches TC TDC hits by 2 to allow coincidence
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TCTDCSTRETCH(
    input CLK,
    //input quantities
    input HITIN,
    input [4:0] TIMEIN,
    //parameters
    input [4:0] ADDVAL,
    //outputs
    output HITOUT,
    output [5:0] TIMEOUT //need one more bit here
    );

    reg OLDHIT;
    reg [5:0] TIMESUM;

    always @(posedge CLK) begin
       OLDHIT <= HITIN;
       TIMESUM <= TIMEIN + ADDVAL;
    end
    
    assign HITOUT = HITIN | OLDHIT;
    assign TIMEOUT = (OLDHIT==1'b1)? TIMESUM : (HITIN)? {1'b0,TIMEIN} : 5'b0;
endmodule
