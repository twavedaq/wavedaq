`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 03.01.2019 11:16:52
// Design Name: 
// Module Name: SCIFI_BLOCK
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////

`include "global_parameters.vh"

module SCIFI_BLOCK(
    input [1023:0] DATA,
    input [4:0] SHAPER_VAL,
    input CLK,
    input RUNMODE,
    input SYNC,
    output SCIFITRG,
    output [(32*(`SCIFIFIBERS+1))-1:0] SCIFIDATA
    );
    
// EXTRACTS SCIFI HITS FROM THE DATA STREAM
wire [`SCIFICHANNELS - 1:0] SCIFIHIT;
genvar iSlot;
for(iSlot = `SCIFIFIRSTSLOT; iSlot<(`SCIFILASTSLOT+1); iSlot = iSlot+1) begin
    assign SCIFIHIT[16*(iSlot-`SCIFIFIRSTSLOT)+:16] = DATA[(64*iSlot)+48 +:16];
end

// HIT SHAPERS
//wire [`SCIFICHANNELS -1 :0] SCIFIHIT_SH;
//genvar iHit;
//for(iHit =0; iHit<`SCIFICHANNELS; iHit= iHit+1) begin
    //PROG_SHAPER #(
       //.VAL_SIZE(5)
   //) CHANNEL_SHAPER_SCIFI(
       //.IN(SCIFIHIT[iHit]),
       //.OUT(SCIFIHIT_SH[iHit]),
       //.CLK(CLK),
       //.VAL(SHAPER_VAL)
   //);
//end

// CREATE THE FIBER HITS
reg [`SCIFIFIBERS-1:0] FIBHIT;
reg FIBOR;
genvar fibHit;
for(fibHit = 0; fibHit<`SCIFIFIBERS; fibHit= fibHit+1) begin
    always @(posedge CLK) begin
        //FIBHIT[fibHit] <= SCIFIHIT_SH[fibHit*2] & SCIFIHIT_SH[fibHit*2+1];
        FIBHIT[fibHit] <= SCIFIHIT[fibHit*2] & SCIFIHIT[fibHit*2+1];
    end//always
end// for

always @(posedge CLK) begin
   FIBOR = |FIBHIT;
end//always
assign SCIFITRG = FIBOR;


// CREATE THE COUNTERS
genvar fibCou;
for(fibCou = 0; fibCou<`SCIFIFIBERS; fibCou= fibCou+1) begin
     TRGCOUNTERS_BLOCK SCIFICOUNTERS_INST(
        .ISTRG(FIBHIT[fibCou]), 
        .SYNC(SYNC),
        .RUNMODE(RUNMODE),
        .TRGCOUNTER(SCIFIDATA[fibCou*32+:32]),
        .CLK(CLK),
        .TRGCOUMODE(1'b1)
    );
end

// EXTRACT PROTON CURRENT
wire PROTONCURRENT;
assign PROTONCURRENT = DATA[(64*`SCIFILASTSLOT)+63];

// PROTONCURRENT SHAPER
//wire PROTONCURRENT_SH;
//PROG_SHAPER #(
   //.VAL_SIZE(5)
//) CHANNEL_PCURRSH(
   //.IN(PROTONCURRENT),
   //.OUT(PROTONCURRENT_SH),
   //.CLK(CLK),
   //.VAL(SHAPER_VAL)
//);

// PROTONCURRENT COUNTER
TRGCOUNTERS_BLOCK PCURRCOUNTERS_INST(
   //.ISTRG(PROTONCURRENT_SH), 
   .ISTRG(PROTONCURRENT), 
   .SYNC(SYNC),
   .RUNMODE(RUNMODE),
   .TRGCOUNTER(SCIFIDATA[`SCIFIFIBERS*32+:32]),
   .CLK(CLK),
   .TRGCOUMODE(1'b1)
);



endmodule
