`timescale 100ps / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 10/05/2016 10:01:28 AM
// Design Name: 
// Module Name: WFMTRG_TB
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module WFMTRG_TB();
    reg CLK_t;
    reg ALGCLK_t;
    wire [1023:0] WDDATA_t;
    wire [28:0] SUM_t;
    wire [3:0] MAX_t;
    wire [7:0] TDCSUM_t;
    wire [4:0] TDCNUM_t;
    
    reg [25-1:0] WDBSUM[15:0];
    reg [8-1:0] WDBTDCSUM[15:0];
    reg [5-1:0] WDBTDCNUM[15:0];
    reg WDBANYTDC[15:0];

//    input [1023:0] WDDATA,
//    input          CLK,
//    input          ALGCLK,
//    output  [28:0] SUM,
//    //output  [21:0] MAXWFM,
//    output   [3:0] MAX,
//    output   [7:0] TDCSUM,
//    output   [4:0] TDCNUM

    WFMTRG WFMTRGSIM(
        .CLK(CLK_t),
        .ALGCLK(ALGCLK_t),
        .WDDATA(WDDATA_t),
        .SUM(SUM_t),
        .MAX(MAX_t),
        .TDCSUM(TDCSUM_t),
        .TDCNUM(TDCNUM_t)
        ) ;

    genvar i;
    for (i=0; i<16; i=i+1) begin
        assign WDDATA_t[64*i+:64] = {WDBANYTDC[i],2'b0,WDBTDCNUM[i],WDBTDCSUM[i],16'b0,7'b0,WDBSUM[i]};
    end

    integer iSlot;
    initial begin
           CLK_t = 0;
           ALGCLK_t = 0;
           for (iSlot=0; iSlot<16;iSlot=iSlot+1) begin
                WDBANYTDC[iSlot]=0;
                WDBTDCNUM[iSlot]=0;
                WDBTDCSUM[iSlot]=0;
                WDBSUM[iSlot]=0;
           end
    end
    
    always 
        #50 CLK_t = !CLK_t;
        
    always
        #25 ALGCLK_t = !ALGCLK_t;

    initial begin
    #100
        WDBTDCNUM[0]=5'h1;
        WDBTDCSUM[0]=8'h2;
    #100
        WDBTDCNUM[1]=5'h1F;
        WDBTDCSUM[1]=8'hF;
    #100
        WDBTDCNUM[0]=0;
        WDBTDCSUM[0]=0;
        WDBTDCNUM[1]=0;
        WDBTDCSUM[1]=0;
    #1000
        $finish();
    end

endmodule
