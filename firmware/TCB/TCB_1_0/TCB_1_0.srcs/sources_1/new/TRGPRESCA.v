`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Luca Galli
// 
// Create Date: 10.09.2015 12:00:50
// Design Name: 
// Module Name: TRGPRESCA
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module TRGPRESCA(
    input CLK,
    input ALGCLK,
    input [`NTRG-1:0] TRIMOD,
    input [(32*`NTRG)-1:0] PRESCA,
    input RUNMODE,
    input [`NTRG-1:0] TRGENA,
    input SYNC,
    output GETRI,
    output [`NTRG-1:0] TRIPATTERN,
    output [`TRITYPDIM-1:0] TRITYPE
    );

reg [`NTRG-1:0] TRIPATTERN_REG;
wire [`NTRG-1:0] TRIFIRE;
reg [`DLYSIZE-1:0] DLYGETRI;
reg [`NTRG*`SRSIZE-1:0] TRIMODREG; // this is a shift register to collect the trigger types in a event
//HERE WE INSTATIATE THE ONEPRESCA BLOCKS
ONEPRESCA ONEPRESCA_BLOCK [`NTRG-1:0](
    .CLK(ALGCLK),
    .TRIMOD(TRIMOD&TRGENA),
    .PRESCA(PRESCA),
    .TRIPATTERN(TRIFIRE),
    .SYNC(SYNC),
    .MASK(TRGENA)
);
// TRIGGER SIGNAL GENERATION
assign GETRI = |TRIFIRE & RUNMODE;
reg [`TRITYPDIM-1:0] TRITYPE_REG;
//TRIGGER TYPE ASSOCIATION (COPIED FROM MEG2-0)
   always @(posedge GETRI)
	begin
        casez (TRIFIRE)
            32'b???????????????????????????????1 : TRITYPE_REG <= 4'b0000;
            32'b??????????????????????????????10 : TRITYPE_REG <= 4'b0001;
//            32'b?????????????????????????????100 : TRITYPE_REG <= 4'b0010;
//           32'b????????????????????????????1000 : TRITYPE_REG <= 4'b0011;
//            32'b???????????????????????????10000 : TRITYPE_REG <= 4'b0100;
//            32'b??????????????????????????100000 : TRITYPE_REG <= 4'b0101;
//            32'b?????????????????????????1000000 : TRITYPE_REG <= 4'b0110;
//            32'b????????????????????????10000000 : TRITYPE_REG <= 4'b0111;
//            32'b???????????????????????100000000 : TRITYPE_REG <= 4'b1000;
            default: begin
				TRITYPE_REG <= 4'b0000;
			   end
         endcase
       end
assign TRITYPE = TRITYPE_REG;
// here delay the getri by 40ns waiting for other algorithms to be executed
always @(posedge CLK) begin 
    DLYGETRI <= {DLYGETRI[`DLYSIZE-2:0],GETRI};
end
// then register the trimod when the dlygetri fires
integer itrg;
always @(posedge DLYGETRI[`DLYSIZE-1]) begin
    for(itrg = 0; itrg<`NTRG; itrg = itrg+1) begin
    	TRIPATTERN_REG[itrg] <= |TRIMODREG[itrg*`SRSIZE +: `SRSIZE];
	end
end
// NOW PLAY WUTH THE TRIMOD SHIFT REGISTER
// THE SHIFT REGISTER IS ORGANISED IN BLOCKS OF 8 BITS
integer ishift;
always @(posedge CLK) begin
    for(ishift = 0; ishift<`NTRG; ishift = ishift+1) begin
    	TRIMODREG[ishift*`SRSIZE +: `SRSIZE] <= {TRIMOD[ishift],TRIMODREG[(ishift)*`SRSIZE + 1 +: `SRSIZE-1]};
    end
end
assign TRIPATTERN = TRIPATTERN_REG;
endmodule
