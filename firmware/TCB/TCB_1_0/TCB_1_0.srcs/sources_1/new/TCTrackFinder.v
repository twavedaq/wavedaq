`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 07.03.2017 11:21:47
// Design Name: 
// Module Name: TCTrackFinder
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module TCTrackFinder(
    input [127:0] TILEHIT,
    input CLK,
    input [5*128-1:0] TILETIME,
    output reg HIT0,
    output reg HIT1,
    output reg [6:0] TILEID0,
    output reg [6:0] TILEID1,
    output reg [4:0] TILETIME0,
    output reg [4:0] TILETIME1,
    input [31:0] TCOMPH,
    input [31:0] TCOMPL
    );

    parameter nTile = 128;
    
    // REGISTER THE INFO FOR THE SECOND COMPARISON
    reg [127:0] TILEHITREG;
    reg [5*128-1:0] TILETIMEREG;
    
    always @(posedge CLK) begin
        TILEHITREG <= TILEHIT;
        TILETIMEREG <= TILETIME;
    end

    //OUTPUT OF FIRST HIT SEARCH 
    reg [6:0] TILEIDTMP;
    reg [4:0] TILETIMETMP;
    reg HITTMP;
    
    //COMPUTE TIME DIFFERENCES
    integer iTile;
    reg OLDHIT;
    reg [5:0] OLDTIME;
    reg [6*128-1:0] TILETIMEDIFF;
    always @(*) begin
       for(iTile=nTile-1; iTile>=0; iTile=iTile-1) begin
          TILETIMEDIFF[6*iTile+:6] = OLDTIME - {1'b0,TILETIME[5*iTile+:5]};
       end
    end

    //FIRST SEARCH THE FIRST HIT
    always @(posedge CLK) begin
        HITTMP <= 1'b0;
        TILEIDTMP <= 0;
        TILETIMETMP <= 0;
        OLDHIT <= 1'b0;
        OLDTIME <= 0;
    
    for(iTile=nTile-1; iTile>=0; iTile=iTile-1) begin
            if (TILEHIT[iTile] & (~OLDHIT | ($signed(TILETIMEDIFF[6*iTile+:6])>=$signed(TCOMPH[5:0])))) begin
                TILEIDTMP <= iTile[6:0];
                TILETIMETMP <= TILETIME[iTile*5+:5];
                HITTMP <= 1'b1; 
                OLDTIME <= {1'b0, TILETIME[iTile*5+:5]} + 6'b10000;
                OLDHIT <= 1'b1;
            end
        end
    end

   
    //COMPUTE TIME DIFFERENCES
    reg [6*128-1:0] TILETIMEDIFF1;
    always @(*) begin
       for(iTile=nTile-1; iTile>=0; iTile=iTile-1) begin
          TILETIMEDIFF1[6*iTile+:6] = {1'b0,TILETIMETMP} - {1'b0,TILETIMEREG[5*iTile+:5]};
       end
    end

    //THEN SEARCH FOR A SECOND HIT LOOKING AT THE TIME VALUES
    always @(posedge CLK) begin
       // VALIDATE FIRST HIT   
       TILEID0 <= TILEIDTMP;
       TILETIME0 <= TILETIMETMP;
       HIT0 <= HITTMP;

       // LOOK FOR A SECOND TRACK
       TILEID1 <= 0;
       TILETIME1 <= 0;
       HIT1 <= 1'b0;
       for(iTile=nTile-1; iTile>=0; iTile=iTile-1) begin
          if (HITTMP & TILEHITREG[iTile] & ($signed(TILETIMEDIFF1[6*iTile+:6])>=$signed(TCOMPL[5:0]))) begin
             TILEID1 <= iTile[6:0];
             TILETIME1 <= TILETIMEREG[iTile*5+:5];
             HIT1 <= 1'b1; 
          end
       end
    end
    

endmodule
