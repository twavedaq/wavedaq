`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 16:58:18
// Design Name: 
// Module Name: PATTERNTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module PATTERNTRG(
    input [255:0] HIT,
    input CLK,
    input [256*`PATTERNNUM-1:0] PATTERNS,
    input [(`PATTERNNUM/32+1)*32-1:0] PATTERNENA,
    output reg [`PATTERNNUM-1:0] OUT,
    output reg GLOBALOR
    );
    
//    wire [255:0] HIT;
    
//    SERDESCHANNELHIT SERDESCHANNELHIT_inst(
//        .WDDATA(WDDATA),
//        .CLK(CLK),
//        .HIT(HIT)
//    );
    
    genvar iPatt;
    for(iPatt=0; iPatt<`PATTERNNUM; iPatt= iPatt+1) begin
        reg [255:0] FOUND;
        genvar iCh;
        for(iCh=0; iCh<256; iCh=iCh+1) begin
            always @(posedge CLK) begin
                FOUND[iCh] <= ~PATTERNS[iPatt*256+iCh] | HIT[iCh];
            end
        end
        
        always @(posedge CLK) begin
            OUT[iPatt] <= &FOUND;
        end
    end
    
    always @(posedge CLK) begin
        GLOBALOR <= |(OUT & PATTERNENA[`PATTERNNUM-1:0]);
    end
    
endmodule
