`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 04.02.2016 16:42:27
// Design Name: 
// Module Name: ALLSERDES
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////
`include "global_parameters.vh"

module SERDES_MASTERWD(
    //SERDES CONNECTIONS
    input SERDES_CLK,
    input CLK,
    input SYNC,
    input [7:0] WDB_RX0_D_P,
    input [7:0] WDB_RX0_D_N,
    input [7:0] WDB_RX1_D_P,
    input [7:0] WDB_RX1_D_N,
    input [7:0] WDB_RX2_D_P,
    input [7:0] WDB_RX2_D_N,
    input [7:0] WDB_RX3_D_P,
    input [7:0] WDB_RX3_D_N,
    input [7:0] WDB_RX4_D_P,
    input [7:0] WDB_RX4_D_N,
    input [7:0] WDB_RX5_D_P,
    input [7:0] WDB_RX5_D_N,
    input [7:0] WDB_RX6_D_P,
    input [7:0] WDB_RX6_D_N,
    input [7:0] WDB_RX7_D_P,
    input [7:0] WDB_RX7_D_N,
    input [7:0] WDB_RX8_D_P,
    input [7:0] WDB_RX8_D_N,
    input [7:0] WDB_RX9_D_P,
    input [7:0] WDB_RX9_D_N,
    input [7:0] WDB_RX10_D_P,
    input [7:0] WDB_RX10_D_N,
    input [7:0] WDB_RX11_D_P,
    input [7:0] WDB_RX11_D_N,
    input [7:0] WDB_RX12_D_P,
    input [7:0] WDB_RX12_D_N,
    input [7:0] WDB_RX13_D_P,
    input [7:0] WDB_RX13_D_N,
    input [7:0] WDB_RX14_D_P,
    input [7:0] WDB_RX14_D_N,
    input [7:0] WDB_RX15_D_P,
    input [7:0] WDB_RX15_D_N,
    output [1023:0] BPDATA,
    output [7:0] TCB_RTX_D_P,
    output [7:0] TCB_RTX_D_N,
    output [7:0] TCB_RTX1_D_P,
    output [7:0] TCB_RTX1_D_N,
    output [7:0] TCB_RTX2_D_P,
    output [7:0] TCB_RTX2_D_N,
    output [7:0] TCB_RTX3_D_P,
    output [7:0] TCB_RTX3_D_N,
    input [63:0] FCDATA,
    output [1:0] DCB_TX_D_P,
    output [1:0] DCB_TX_D_N,
    input  [1:0] DCB_RX_D_P,
    input  [1:0] DCB_RX_D_N,
    output [15:0] DCBDATA,
    //READOUT OF SERDES STATUS and MASKING
    output [`NSERDESBP*8*5-1:0] BPCURRENTDLY,
    output [`NSERDESBP*8*3-1:0] BPCURRENTBISTLIP,
    output [2*5-1:0] DCBCURRENTDLY,
    output [2*3-1:0] DCBCURRENTBISTLIP,
    input [`NSERDESBP-1:0] BPSERDESMASK,
    //SOFTWARE SERDES CONFIGURATION
    input RELOADDLY,
    input CHECKGLBRESET,
    input CHECKGLBENABLE,
    input [8*5*`NSERDESBP-1:0] BPDLY,
    input [8*`NSERDESBP-1:0] BPBITSLIP,
    input [63:0] CHECKVALUE,
    output [8*`NSERDESBP-1:0] CHECKERROR,
    output [32*(8*`NSERDESBP)-1:0] CHECKCOUNTERS,
    output [31:0] CHECKTIMER,
    input [`NSERDESBP*8-1:0] BPSERDESRESET,
    input [7:0] FCSERDESRESET,
    input [3:0] DCBSERDESRESET,
    input [9:0] DCBDLY,
    input [1:0] DCBBITSLIP,
    output [1:0] DCBCHECKERROR,
    output [63:0] DCBCHECKCOUNTERS,
    //AUTOMATIC SERDES CALIBRATION
    input CALIBSTART,
    input RESET_CALIBFSM,
    input PATTERNSERDES,
    input CALIBMASK,
    input BPSERDESMASKENABLE,
    output [`NSERDESBP-1:0] BPCALIBBUSY,
    output [`NSERDESBP-1:0] BPCALIBFAIL,
    output BPALIGNBUSY,
    output BPALIGNFAIL,
    output [`NSERDESBP*4-1:0] BPALIGNOFFSET,
    output [4:0] BPSERDESMIN,
    output [`NSERDESBP-1:0] BPALIGNDLY,
    output DCBCALIBBUSY,
    output DCBCALIBFAIL,
    output [(`NSERDESBP)*32-1:0] BPDLY_TESTED,
    output [(`NSERDESBP)*32-1:0] BPDLY_STATE,
    output [31:0] DCBDLY_TESTED,
    output [31:0] DCBDLY_STATE
    );
    
    wire [8*`NSERDESBP-1:0] INPUT_P;
    wire [8*`NSERDESBP-1:0] INPUT_N;
    assign INPUT_P = {WDB_RX15_D_P, WDB_RX14_D_P, WDB_RX13_D_P, WDB_RX12_D_P, WDB_RX11_D_P, WDB_RX10_D_P, WDB_RX9_D_P, WDB_RX8_D_P, WDB_RX7_D_P, WDB_RX6_D_P, WDB_RX5_D_P, WDB_RX4_D_P, WDB_RX3_D_P, WDB_RX2_D_P, WDB_RX1_D_P, WDB_RX0_D_P};
    assign INPUT_N = {WDB_RX15_D_N, WDB_RX14_D_N, WDB_RX13_D_N, WDB_RX12_D_N, WDB_RX11_D_N, WDB_RX10_D_N, WDB_RX9_D_N, WDB_RX8_D_N, WDB_RX7_D_N, WDB_RX6_D_N, WDB_RX5_D_N, WDB_RX4_D_N, WDB_RX3_D_N, WDB_RX2_D_N, WDB_RX1_D_N, WDB_RX0_D_N};

    wire [8*`NSERDESFP-1:0] OUTPUT_P;
    wire [8*`NSERDESFP-1:0] OUTPUT_N;
    assign {TCB_RTX3_D_P, TCB_RTX2_D_P, TCB_RTX1_D_P, TCB_RTX_D_P} = OUTPUT_P;
    assign {TCB_RTX3_D_N, TCB_RTX2_D_N, TCB_RTX1_D_N, TCB_RTX_D_N} = OUTPUT_N;
    
    wire [`NSERDESBP-1:0] BPSERDESMASKLOCAL;
    
    //INPUT FROM WDB
    genvar iSerdes;
    for(iSerdes=0; iSerdes<`NSERDESBP; iSerdes=iSerdes+1) begin
        INPUTSERDES #(
            .nLink(8)
        ) IN(
            .DATA(BPDATA[64*iSerdes+:64]),
            .CLK(SERDES_CLK),
            .CLKDIV(CLK),
            .IN_P(INPUT_P[8*iSerdes+:8]),
            .IN_N(INPUT_N[8*iSerdes+:8]),
            .DATAMASK(BPSERDESMASKLOCAL[iSerdes]|BPSERDESMASK[iSerdes]),
            .CALIBSTART(CALIBSTART),
            .RESET_FSM(RESET_CALIBFSM),
            .FORCE_RESET(BPSERDESRESET[iSerdes*8+:8]),
            .FORCE_BITSLIP(BPBITSLIP[8*iSerdes+:8]),
            .FORCE_DLY(BPDLY[40*iSerdes+:40]),
            .FORCE_LOADDLY(RELOADDLY),
            .BUSY(BPCALIBBUSY[iSerdes]),
            .FAIL(BPCALIBFAIL[iSerdes]),
            .CHECKVALUE(CHECKVALUE),
            .CHECKGLBRESET(CHECKGLBRESET),
            .CHECKGLBENABLE(CHECKGLBENABLE),
            .CHECKERROR(CHECKERROR[8*iSerdes+:8]),
            .CHECKCOUNTERS(CHECKCOUNTERS[32*8*iSerdes+:32*8]),
            .REGENABLE(BPALIGNDLY[iSerdes]),
            .DLY_APPLIED(BPCURRENTDLY[5*8*iSerdes+:5*8]),
            .BISTLIP_APPLIED(BPCURRENTBISTLIP[3*8*iSerdes+:3*8]),
            .DLY_TESTED(BPDLY_TESTED[32*iSerdes+:32]),
            .DLY_STATE(BPDLY_STATE[32*iSerdes+:32]),
            .CALIBMASK(CALIBMASK)
        );
    end
    
    //FSM to align serdes
    SERDESALIGNFSM #(
        .nSerdes(`NSERDESBP)
    ) FSM(
        .SERDESDATA(BPDATA),
        .SERDESBUSY(BPCALIBBUSY),
        .SERDESFAIL(BPCALIBFAIL),
        .CLK(CLK),
        .START(CALIBSTART),
        .RESET_FSM(RESET_CALIBFSM),
        .MASKENABLE(BPSERDESMASKENABLE),
        .SERDESMASK(BPSERDESMASKLOCAL),
        .SERDESDLY(BPALIGNDLY),
        .SERDESOFFSET(BPALIGNOFFSET),
        .SERDESMIN(BPSERDESMIN),
        .BUSY(BPALIGNBUSY),
        .FAIL(BPALIGNFAIL)
        );
    
    //OUTPUT TO FRONTPANEL
    for(iSerdes=0; iSerdes<`NSERDESFP; iSerdes=iSerdes+1) begin
        OUTPUTSERDES #(
            .nLink(8)
        ) OUT(
            .OUT_P(OUTPUT_P[8*iSerdes+:8]),
            .OUT_N(OUTPUT_N[8*iSerdes+:8]),
            .CLK(SERDES_CLK),
            .CLKDIV(CLK),
            .DATA(FCDATA),
            .RESET(FCSERDESRESET),
            .SYNC(SYNC),
            .PATTERN_ENA(PATTERNSERDES)
        );
    end

    //OUTPUT TO DCB 
    OUTPUTSERDES #(
        .nLink(2)
    ) OUT(
        .OUT_P(DCB_TX_D_P),
        .OUT_N(DCB_TX_D_N),
        .CLK(SERDES_CLK),
        .CLKDIV(CLK),
        .DATA(CHECKVALUE[15:0]),
        .RESET(DCBSERDESRESET[2+:2]),
        .SYNC(SYNC),
        .PATTERN_ENA(PATTERNSERDES)
    );

    //INPUT FROM DCB
    INPUTSERDES #(
        .nLink(2)
    ) FROM_DCB (
        .DATA(DCBDATA),
        .CLK(SERDES_CLK),
        .CLKDIV(CLK),
        .IN_P(DCB_RX_D_P),
        .IN_N(DCB_RX_D_N),
        .DATAMASK(16'b0),
        .CALIBSTART(CALIBSTART),
        .RESET_FSM(RESET_CALIBFSM),
        .FORCE_RESET(DCBSERDESRESET[1:0]),
        .FORCE_BITSLIP(DCBBITSLIP),
        .FORCE_DLY(DCBDLY),
        .FORCE_LOADDLY(RELOADDLY),
        .BUSY(DCBCALIBBUSY),
        .FAIL(DCBCALIBFAIL),
        .CHECKVALUE(CHECKVALUE[15:0]),
        .CHECKGLBRESET(CHECKGLBRESET),
        .CHECKGLBENABLE(CHECKGLBENABLE),
        .CHECKERROR(DCBCHECKERROR),
        .CHECKCOUNTERS(DCBCHECKCOUNTERS),
        .REGENABLE(16'b0),
        .DLY_APPLIED(DCBCURRENTDLY),
        .BISTLIP_APPLIED(DCBCURRENTBISTLIP),
        .DLY_TESTED(DCBDLY_TESTED),
        .DLY_STATE(DCBDLY_STATE),
        .CALIBMASK(CALIBMASK)
    );
    
    reg [31:0] CHECKCOUREG;
    // CHECK TIME COUNTER
    always @(posedge CLK) begin
        if(CHECKGLBRESET) begin
           CHECKCOUREG <= 32'b0;
        end else if(CHECKGLBENABLE) begin
            CHECKCOUREG <=  CHECKCOUREG +1;
        end
    end

    assign CHECKTIMER = CHECKCOUREG;

endmodule
