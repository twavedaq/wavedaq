`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 12.04.2017 10:22:48
// Design Name: 
// Module Name: DETECTORBUILD
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module DETECTORBUILD(
    input CLK,
    input [SIZE-1:0] IN,
    input [SIZE-1:0] MASK,
    output OUT
    );
    
    parameter SIZE = 4;
    
    // MASK INPUT
    wire [SIZE-1:0] MASKED;
    genvar iInput;
    for (iInput=0; iInput<SIZE; iInput = iInput +1) begin
        assign MASKED[iInput] = (MASK[iInput])?IN[iInput]:1'b1;
    end
    
    assign OUT = &MASKED;
    
endmodule
