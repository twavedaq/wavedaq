`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 17:03:29
// Design Name: 
// Module Name: SERDESCHANNELHIT
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERDESCHANNELHIT(
    input [1023:0] WDDATA,
    output [255:0] HIT,
    output [767:0] HITTIME
    );
    
    genvar iSlot;
    genvar iCha;    
    for(iSlot=0; iSlot<16; iSlot= iSlot+1) begin
        assign HIT[16*iSlot+:16] = WDDATA[64*iSlot+:16];
        for(iCha = 0; iCha<16; iCha = iCha+1) begin
            assign HITTIME[(iSlot*16*3 + iCha*3)+:3] = WDDATA[(64*iSlot +16 +iCha*3)+:3]; 
        end
    end
    
endmodule
