`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 29.11.2016 17:03:29
// Design Name: 
// Module Name: SERDESDECODE
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: This module decodes serdes data from WDBs
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module SERDESDECODE(
    input [64*NSLOT-1:0] DATA,
    //XEC-like connection
    output [24:0] WFMSUM,
    output [7:0] TDCSUM,
    output [4:0] TDCNUM,
    output ANYTDC,
    //TC-like connection
    output [15:0] TDCHIT,
    output [3*16-1:0] TDCVAL,
    //AUX-like connection
    output [24:0] AUXWFMSUM,
    output [3:0] AUXWFMMAX,
    output [20:0] AUXWFMMAXVAL,
    output [15:0] DISCRSTATE
    );

    parameter SLOT=0;
    parameter NSLOT=1;
    
    //decode XEC
    // CONNECTION SCHEME:
    // WDi :BIT 25-0 FOR SUM OF WAVEFORMs
    //     :BIT 55-48 FOR SUM OF TDCs
    //     :BIT 60-56 FOR NUM OF TDCs
    //     :BIT 63 FOR ANY TDC OVER THRESHOLD
    assign WFMSUM = DATA[64*SLOT+:25];
    assign TDCSUM = DATA[64*SLOT+48+:8];    
    assign TDCNUM = DATA[64*SLOT+56+:5];    
    assign ANYTDC = DATA[64*SLOT+63];

    //decode TC
    assign TDCHIT = DATA[64*SLOT+:16];    
    assign TDCVAL = DATA[64*SLOT+16+:3*8];

    //decode AUX
    assign AUXWFMSUM  = DATA[64*SLOT+:25]; 
    assign AUXWFMMAXVAL  = DATA[64*SLOT+25+:18]; 
    assign AUXWFMMAX  = DATA[64*SLOT+25+19+:4]; 
    assign DISCRSTATE = DATA[64*SLOT+25+19+4+:16]; 
    
endmodule
