`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11/14/2018 11:16:41 AM
// Design Name: 
// Module Name: CRCTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module CRCTRG(
    input CLK,
    input [15:0] HIT,
    input [7:0] HITMASK,
    input [15:0] PAIRENABLE,
    output reg SINGLETRG,
    output reg PAIRTRG
    );
    
    //compute bar coincidence with mask
    reg [3:0] BARTOP;
    reg [3:0] BARBOTTOM;
    always @(posedge CLK) begin
        BARTOP[0] <= (HIT[0] & HIT[1]) & HITMASK[0];
        BARTOP[1] <= (HIT[2] & HIT[3]) & HITMASK[1];
        BARTOP[2] <= (HIT[4] & HIT[5]) & HITMASK[2];
        BARTOP[3] <= (HIT[6] & HIT[7]) & HITMASK[3];
        BARBOTTOM[0] <= (HIT[8] & HIT[9]) & HITMASK[4];
        BARBOTTOM[1] <= (HIT[10] & HIT[11]) & HITMASK[5];
        BARBOTTOM[2] <= (HIT[12] & HIT[13]) & HITMASK[6];
        BARBOTTOM[3] <= (HIT[14] & HIT[15]) & HITMASK[7];
    end
    
    //single bar trigger, hit in any bar
    always @(posedge CLK) begin
        SINGLETRG <= (|BARTOP) | (|BARBOTTOM);
    end
    
    //compute pair coincidences
    reg [15:0] PAIRHIT;
    always @(posedge CLK) begin
        PAIRHIT[0] <= (BARTOP[0] & BARBOTTOM[0]) & PAIRENABLE[0];
        PAIRHIT[1] <= (BARTOP[0] & BARBOTTOM[1]) & PAIRENABLE[1];
        PAIRHIT[2] <= (BARTOP[0] & BARBOTTOM[2]) & PAIRENABLE[2];
        PAIRHIT[3] <= (BARTOP[0] & BARBOTTOM[3]) & PAIRENABLE[3];
        PAIRHIT[4] <= (BARTOP[1] & BARBOTTOM[0]) & PAIRENABLE[4];
        PAIRHIT[5] <= (BARTOP[1] & BARBOTTOM[1]) & PAIRENABLE[5];
        PAIRHIT[6] <= (BARTOP[1] & BARBOTTOM[2]) & PAIRENABLE[6];
        PAIRHIT[7] <= (BARTOP[1] & BARBOTTOM[3]) & PAIRENABLE[7];
        PAIRHIT[8] <= (BARTOP[2] & BARBOTTOM[0]) & PAIRENABLE[8];
        PAIRHIT[9] <= (BARTOP[2] & BARBOTTOM[1]) & PAIRENABLE[9];
        PAIRHIT[10] <= (BARTOP[2] & BARBOTTOM[2]) & PAIRENABLE[10];
        PAIRHIT[11] <= (BARTOP[2] & BARBOTTOM[3]) & PAIRENABLE[11];
        PAIRHIT[12] <= (BARTOP[3] & BARBOTTOM[0]) & PAIRENABLE[12];
        PAIRHIT[13] <= (BARTOP[3] & BARBOTTOM[1]) & PAIRENABLE[13];
        PAIRHIT[14] <= (BARTOP[3] & BARBOTTOM[2]) & PAIRENABLE[14];
        PAIRHIT[15] <= (BARTOP[3] & BARBOTTOM[3]) & PAIRENABLE[15];
    end
    
    //final OR
    always @(posedge CLK) begin
        PAIRTRG <= |PAIRHIT;
    end
    
    
    
    
endmodule
