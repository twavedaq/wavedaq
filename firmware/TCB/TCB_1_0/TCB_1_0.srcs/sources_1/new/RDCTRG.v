`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2017 10:19:39
// Design Name: 
// Module Name: RDCTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module RDCTRG(
    input CLK,
    input [23:0] PLASTICHIT,
    input [75:0] LYSOHIT,
    input [5*25-1:0] LYSOINSUM,
    input [5*21-1:0] LYSOINMAXWFM,
    input [5*4-1:0] LYSOINMAX,
    input [31:0] LYSOTHR,
    input [31:0] MASK,
    output reg [27:0] LYSOSUM,
    output [6:0] LYSOMAX,
    output [20:0] LYSOMAXWFM,
    output reg [11:0] PLASTICCOINC,
    output reg PLASTICSINGLE,
    output reg LYSOOR,
    output reg LYSOTHRFIRE,
    output reg [11:0]PLASTICLYSOCOINC,
    output reg PLASTICLYSOSINGLE,
    output TRG
    );
    
    //Plastic coincidencies
    //reg [10:0] PLASTICCOINC;
    //reg PLASTICSINGLE;
    integer iPlastic;
    always @(posedge CLK) begin
        for(iPlastic=0; iPlastic<12; iPlastic= iPlastic+1) begin
            PLASTICCOINC[iPlastic] = PLASTICHIT[2*iPlastic] & PLASTICHIT[2*iPlastic+1];
        end
        PLASTICSINGLE <= |PLASTICCOINC;
    end
    
    //LYSO OR
    //reg LYSOOR;
    always @(posedge CLK) begin
        LYSOOR <= |LYSOHIT;
    end
    
    //PLASTIC-LYSO SPATIAL COINCIDENCE
    integer iPSLYSO; 
    always @(posedge CLK) begin
       for(iPSLYSO=0; iPSLYSO<12; iPSLYSO = iPSLYSO+1) begin
       case(iPSLYSO)
          0  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[0] | LYSOHIT[1] | LYSOHIT[2] | LYSOHIT[3]);
          1  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[4] | LYSOHIT[5] | LYSOHIT[6] | LYSOHIT[7] | LYSOHIT[8] | LYSOHIT[9]);
          2  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[10] | LYSOHIT[11] | LYSOHIT[12] | LYSOHIT[13] | LYSOHIT[14] | LYSOHIT[15] | LYSOHIT[16] | LYSOHIT[17]);
          3  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[18] | LYSOHIT[19] | LYSOHIT[20] | LYSOHIT[21] | LYSOHIT[22] | LYSOHIT[23] | LYSOHIT[24] | LYSOHIT[25] | LYSOHIT[26] | LYSOHIT[27]);
          4  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[18] | LYSOHIT[19] | LYSOHIT[20] | LYSOHIT[21] | LYSOHIT[22] | LYSOHIT[23] | LYSOHIT[24] | LYSOHIT[25] | LYSOHIT[26] | LYSOHIT[27] |
                                                                    LYSOHIT[28] | LYSOHIT[29] | LYSOHIT[30] | LYSOHIT[31] | LYSOHIT[32] | LYSOHIT[33] | LYSOHIT[34] | LYSOHIT[35] | LYSOHIT[36] | LYSOHIT[37]);
          5  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[28] | LYSOHIT[29] | LYSOHIT[30] | LYSOHIT[31] | LYSOHIT[32] | LYSOHIT[33] | LYSOHIT[34] | LYSOHIT[35] | LYSOHIT[36] | LYSOHIT[37]);
          6  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[38] | LYSOHIT[39] | LYSOHIT[40] | LYSOHIT[41] | LYSOHIT[42] | LYSOHIT[43] | LYSOHIT[44] | LYSOHIT[45] | LYSOHIT[46] | LYSOHIT[47]);
          7  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[38] | LYSOHIT[39] | LYSOHIT[40] | LYSOHIT[41] | LYSOHIT[42] | LYSOHIT[43] | LYSOHIT[44] | LYSOHIT[45] | LYSOHIT[46] | LYSOHIT[47]| 
                                                                    LYSOHIT[48] | LYSOHIT[49] | LYSOHIT[50] | LYSOHIT[51] | LYSOHIT[52] | LYSOHIT[53] | LYSOHIT[54] | LYSOHIT[55] | LYSOHIT[56] | LYSOHIT[57]);
          8  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[48] | LYSOHIT[49] | LYSOHIT[50] | LYSOHIT[51] | LYSOHIT[52] | LYSOHIT[53] | LYSOHIT[54] | LYSOHIT[55] | LYSOHIT[56] | LYSOHIT[57]);
          9  : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[58] | LYSOHIT[59] | LYSOHIT[60] | LYSOHIT[61] | LYSOHIT[62] | LYSOHIT[63] | LYSOHIT[64] | LYSOHIT[65]);
          10 : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[66] | LYSOHIT[67] | LYSOHIT[68] | LYSOHIT[69] | LYSOHIT[70] | LYSOHIT[71]);
          11 : PLASTICLYSOCOINC[iPSLYSO] = PLASTICCOINC[iPSLYSO] & (LYSOHIT[72] | LYSOHIT[73] | LYSOHIT[74] | LYSOHIT[75]);
          endcase;     
       end
       PLASTICLYSOSINGLE <= |PLASTICLYSOCOINC;
    end
    
    //LYSO SUM THREE
    reg [3*26-1:0] FIRSTOUT;
    reg [2*27-1:0] SECONDOUT;
    integer isumzero;
    always @(posedge CLK) begin
        for(isumzero = 0; isumzero < 2; isumzero = isumzero + 1) begin  
            FIRSTOUT[isumzero*26+:26] <= $signed(LYSOINSUM[25*isumzero*2+:25]) + $signed(LYSOINSUM[25*(isumzero*2+1)+:25]);
        end
        //sign-extend last input
        FIRSTOUT[2*26+:26] <= {LYSOINSUM[25*5-1], LYSOINSUM[25*4+:25]};
    end
    always @(posedge CLK) begin
        SECONDOUT[0+:27] <= $signed(FIRSTOUT[0+:26]) + $signed(FIRSTOUT[26*1+:26]);
        SECONDOUT[27+:27] <= {FIRSTOUT[3*26-1], FIRSTOUT[2*26+:26]};
    end
    always @(posedge CLK) begin
        LYSOSUM <= $signed(SECONDOUT[0+:27]) + $signed(SECONDOUT[27*1+:27]);
    end
    always @(posedge CLK) begin
        if($signed(LYSOSUM)>$signed(LYSOTHR[26:0])) begin
            LYSOTHRFIRE <= 1'b1;
        end else begin
            LYSOTHRFIRE <= 1'b0;
        end
    end

    //LYSO MAX THREE
    genvar icmpzero;
    wire [3*5-1:0] FIRSTMAX;
    wire signed [21*3-1:0] OUTFIRSTCMP;
    wire [2*6-1:0] SECONDMAX;
    wire signed [21*2-1:0] OUTSECONDCMP;
    for(icmpzero = 0; icmpzero < 2; icmpzero = icmpzero + 1) begin
        CMP_MAX_BLOCK #(
        .MAXWIDTH(5),
        .WFMWIDTH(21)
        ) CMP_MAX_INST(
        .WFMA(LYSOINMAXWFM[21*icmpzero*2+:21]),
        .WFMB(LYSOINMAXWFM[21*(icmpzero*2+1)+:21]),
        .MAXA({1'b0, LYSOINMAX[4*icmpzero*2+:4]}),
        .MAXB({1'b1, LYSOINMAX[4*(icmpzero*2+1)+:4]}),
        .CLK(CLK),
        .WFMO(OUTFIRSTCMP[21*icmpzero+:21]),
        .MAXO(FIRSTMAX[5*icmpzero+:5])
        );
    end
    reg [3:0] DLYFIRSTMAX;
    reg [20:0] DLYFIRSTMAXWFM;
    always @(posedge CLK) begin
        DLYFIRSTMAX <= LYSOINMAX[4*4+:4];
        DLYFIRSTMAXWFM <= LYSOINMAXWFM[21*4+:21];
        
    end
    assign OUTFIRSTCMP[21*2+:21] = DLYFIRSTMAXWFM;
    assign FIRSTMAX[5*2+:5] = {1'b0, DLYFIRSTMAX};
    
    CMP_MAX_BLOCK #(
    .MAXWIDTH(6),
    .WFMWIDTH(21)
    ) SECOND_CMP_MAX_INST(
    .WFMA(OUTFIRSTCMP[0+:21]),
    .WFMB(OUTFIRSTCMP[21+:21]),
    .MAXA({1'b0, FIRSTMAX[0+:5]}),
    .MAXB({1'b1, FIRSTMAX[5+:5]}),
    .CLK(CLK),
    .WFMO(OUTSECONDCMP[0+:21]),
    .MAXO(SECONDMAX[0+:6])
    );
    reg [4:0] DLYSECONDMAX;
    reg [20:0] DLYSECONDMAXWFM;
    always @(posedge CLK) begin
        DLYSECONDMAX <= FIRSTMAX[5*2+:5];
        DLYSECONDMAXWFM <= OUTFIRSTCMP[21*2+:21];
        
    end
    assign OUTSECONDCMP[21+:21] = DLYSECONDMAXWFM;
    assign SECONDMAX[6+:6] = {1'b0, DLYSECONDMAX};
    
    CMP_MAX_BLOCK #(
    .MAXWIDTH(7),
    .WFMWIDTH(21)
    ) THIRD_CMP_MAX_INST(
    .WFMA(OUTSECONDCMP[0+:21]),
    .WFMB(OUTSECONDCMP[21+:21]),
    .MAXA({1'b0, SECONDMAX[0+:6]}),
    .MAXB({1'b1, SECONDMAX[6+:6]}),
    .CLK(CLK),
    .WFMO(LYSOMAXWFM),
    .MAXO(LYSOMAX)
    );
    
    //COMBINE LOGIC TOGETHER
    DETECTORBUILD #(
    .SIZE(4)
    ) RDCBUILD(
    .CLK(CLK),
    .IN({PLASTICSINGLE, LYSOOR, LYSOTHRFIRE, PLASTICLYSOSINGLE}),
    .MASK(MASK[3:0]),
    .OUT(TRG)
    );
        
endmodule
