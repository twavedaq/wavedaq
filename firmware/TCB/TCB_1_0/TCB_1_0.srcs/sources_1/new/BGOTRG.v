`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 11.04.2017 17:52:31
// Design Name: 
// Module Name: BGOTRG
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module BGOTRG(
    input CLK,
    input [24:0] SUM,
    input [3:0] MAX,
    input [20:0] MAXWFM,
    input [15:0] HIT,
    input [15:0] PRESH_HIT,
    input [31:0] THR,
    input [31:0] VETOTHR,
    input [31:0] MASK,
    input [31:0] HITDLY,
    output THRFIRE,
    output VETOFIRE,
    output reg HITOR,
    output reg ISMAX,
    output PRESHCOUNTER,
    output TRG,
    output reg TRGCOSM
    );
    
    //PRESHOWER LOGIC
    // OR 4 channels together
    reg [1:0] PRESHCOUNTER_0;
    reg [1:0] PRESHCOUNTER_1;
    always @(posedge CLK) begin
        PRESHCOUNTER_0[0] <= |PRESH_HIT[0+:4];
        PRESHCOUNTER_0[1] <= |PRESH_HIT[4+:4];
        PRESHCOUNTER_1[0] <= |PRESH_HIT[8+:4];
        PRESHCOUNTER_1[1] <= |PRESH_HIT[12+:4];
    end
    //AND TWO edge
    reg [1:0] PRESHCOUNTERHIT;
    always @(posedge CLK) begin
        PRESHCOUNTERHIT[0] <= &PRESHCOUNTER_0;
        PRESHCOUNTERHIT[1] <= &PRESHCOUNTER_1;
    end
    //OR of two counters with pipeline for delay
    reg[31:0] PRESHCOUNTEROR_PIPELINE;
    reg[31:0] PRESHCOUNTERAND_PIPELINE;
    always @(posedge CLK) begin
        PRESHCOUNTEROR_PIPELINE[0] <= |PRESHCOUNTERHIT;
        PRESHCOUNTERAND_PIPELINE[0] <= &PRESHCOUNTERHIT;
    end
    always @(posedge CLK) begin
         PRESHCOUNTEROR_PIPELINE[31:1] <=  PRESHCOUNTEROR_PIPELINE[30:0];
         PRESHCOUNTERAND_PIPELINE[31:1] <=  PRESHCOUNTERAND_PIPELINE[30:0];
    end
    //shapers for councidence and veto
    wire PRESHCOUNTEROR;
    wire PRESHCOUNTERAND;
    wire PRESHCOUNTEROR_TOVETO;
    wire PRESHCOUNTERAND_TOVETO;

    SHAPER #(
      .SRLENGTH(6)
    ) PRESHAND (
      .DIN(PRESHCOUNTERAND_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(PRESHCOUNTERAND)
   );
    SHAPER #(
      .SRLENGTH(6)
    ) PRESHOR (
      .DIN(PRESHCOUNTEROR_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(PRESHCOUNTEROR)
   );
    SHAPER #(
      .SRLENGTH(64)
    ) PRESHAND_VETO (
      .DIN(PRESHCOUNTERAND_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(PRESHCOUNTERAND_TOVETO)
   );
    SHAPER #(
      .SRLENGTH(64)
    ) PRESHOR_VETO (
      .DIN(PRESHCOUNTEROR_PIPELINE[HITDLY[4:0]]),
      .CLK(CLK),
      .DOUT(PRESHCOUNTEROR_TOVETO)
   );

    //BGO CHANNEL OR, with pipeline for delay+3CLK
   reg[2:0] HITOR_DLY;
   always @(posedge CLK) begin
      HITOR_DLY <= {HITOR_DLY[1:0], |HIT};
   end 
    
    reg[31:0] HITOR_PIPELINE;
    always @(posedge CLK) begin
        HITOR_PIPELINE[0] <= HITOR_DLY[2];
    end
    always @(posedge CLK) begin
        HITOR_PIPELINE[31:1] <= HITOR_PIPELINE[30:0];
    end
    always @(posedge CLK) begin
        HITOR <= HITOR_PIPELINE[HITDLY[4:0]];
    end
   
 
    //COMPARE THRESHOLD
    wire BGO_QH_PROMPT;
    DISCRIMINATEWFM #(
       .LENGTH(48),
       .WFMSIZE(25)
    ) BGO_DISCR (
       .INPUT(SUM),
       .THR(THR[24:0]),
       .CLK(CLK),
       .TRIG(BGO_QH_PROMPT)
    );
    reg [3:0] BGO_QH_DLY;
    always @(posedge CLK) begin
       BGO_QH_DLY <= {BGO_QH_DLY[2:0], BGO_QH_PROMPT};
    end
    assign THRFIRE = BGO_QH_DLY[3];

    DISCRVETO #(
       .WFMSIZE(25)
    ) BGO_VETO (
       .INPUT(SUM),
       .THR(VETOTHR[24:0]),
       .THR_TOVETO(THR[24:0]),
       .DISCR_TOVETO(THRFIRE),
       .CLK(CLK),
       .TRIG(VETOFIRE)
    );

    
    //THE CRYSTAL COLLECTING THE MAXIMUM ENERGY MUST BE ONE OF THE CENTRAL..
    reg THRFIRE_OLD;
    always @(posedge CLK) begin
        THRFIRE_OLD <= THRFIRE;
        if(THRFIRE & ~THRFIRE_OLD) begin
           case(MAX)
               4'h0: ISMAX <= 1'b0;
               4'h1: ISMAX <= 1'b0;
               4'h2: ISMAX <= 1'b0;
               4'h3: ISMAX <= 1'b0;
               4'h4: ISMAX <= 1'b0;
               4'h5: ISMAX <= 1'b1;
               4'h6: ISMAX <= 1'b1;
               4'h7: ISMAX <= 1'b0;
               4'h8: ISMAX <= 1'b0;
               4'h9: ISMAX <= 1'b1;
               4'hA: ISMAX <= 1'b1;
               4'hB: ISMAX <= 1'b0;
               4'hC: ISMAX <= 1'b0;
               4'hD: ISMAX <= 1'b0;
               4'hE: ISMAX <= 1'b0;
               4'hF: ISMAX <= 1'b0;
               default: ISMAX <= 1'b0;
           endcase
       end else if(~THRFIRE) begin
         ISMAX <= 1'b0;
       end
    end
    
    //COMBINE LOGIC TOGETHER
    DETECTORBUILD #(
    .SIZE(6)
    ) BGOBUILD(
    .CLK(CLK),
    .IN({~PRESHCOUNTERAND_TOVETO, ~PRESHCOUNTEROR_TOVETO, ISMAX, HITOR, ~VETOFIRE, THRFIRE}),
    .MASK(MASK[5:0]),
    .OUT(TRG)
    );

    //COMBINE LOGIC TOGETHER INVERTING PRESHOWER
    DETECTORBUILD #(
    .SIZE(6)
    ) BGOPRESHBUILD(
    .CLK(CLK),
    .IN({PRESHCOUNTERAND, PRESHCOUNTEROR, ISMAX, HITOR, ~VETOFIRE, THRFIRE}),
    .MASK(MASK[5:0]),
    .OUT(PRESHCOUNTER)
    );
    
    //BGO TRIGGER FOR COSMICS 
    //COINCIDENCE OF A TOP CRYSTAL WITH A BOTTOM ONE (CHECK CONNECTION!)
    reg TOPCOINC, BOTTOMCOINC;
    always @(posedge CLK) begin
        TOPCOINC <= HIT[0] | HIT[4] | HIT[8] | HIT[12];
        BOTTOMCOINC <= HIT[3] | HIT[7] | HIT[11] | HIT[15];
        TRGCOSM <= TOPCOINC & BOTTOMCOINC; 
    end

    
endmodule
