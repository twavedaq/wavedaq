/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  MEG - DCB
 *
 *  Author  :  schmid_e
 *  Created :  21.09.2018 09:29:35
 *
 *  Description :  Central control for hardware access.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __SYSTEM_H__
#define __SYSTEM_H__

#include "xgpiops.h"

/******************************************************************************/

#define STDOUT_IS_PS7_UART
#define UART_DEVICE_ID 0

#define SYSPTR(x) (&(system_hw.x))

/******************************************************************************/

typedef struct
{
  XGpioPs   gpio_mio;   /* GPIO for MIO pins */
} system_type;

extern system_type system_hw;

int init_system();

#endif /* __SYSTEM_H__ */
