/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  MEG DCB
 *
 *  Author  :  schmid_e
 *  Created :  21.05.2019 11:24:17
 *
 *  Description :  Software status definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#ifndef __SW_STATUS_H__
#define __SW_STATUS_H__

/* GPIO Slow Control Outputs */
#define  SW_STATUS_WDB_ACCESS    0x0001
#define  SW_STATUS_SW_UPDATE     0x0002
#define  SW_STATUS_FW_UPDATE     0x0004
#define  SW_STATUS_BL_LOAD       0x0008
#define  SW_STATUS_BL_FAIL       0x0010
#define  SW_STATUS_DHCP_REQ      0x0020
#define  SW_STATUS_MARKER        0x0040
#define  SW_STATUS_ERROR         0x0080
#define  SW_STATUS_ALL           0x00FF

/************************************************************/

#endif /* __SW_STATUS_H__ */
