
/***************************** Include Files *********************************/

/************************** Constant Definitions *****************************/

/*
 * The following constants define the commands which may be sent to the Flash
 * device.
 */
/* Read Device Identification */
#define  QSFL_CMD_REMS          0x90   /* Read Electronics Manufacturer ID */
#define  QSFL_CMD_RDID          0x9F   /* Read JEDEC Manufaturer ID and JEDEC CFI */
#define  QSFL_CMD_RES           0xAB   /* Read Electronic Signature (max. 50MHz) */
/* Register Access */
#define  QSFL_CMD_RSR1          0x05   /* Read Status Register 1 (S7:S0)  */
#define  QSFL_CMD_RSR2          0x07   /* Read Status Register 2 (S15:S8) */
#define  QSFL_CMD_RDCR          0x35   /* Read Configuration Register */
#define  QSFL_CMD_WRR           0x01   /* Write Register (Status-1, Configuration-1) */
#define  QSFL_CMD_WRDI          0x04   /* Write Disable */
#define  QSFL_CMD_WREN          0x06   /* Write Enable */
#define  QSFL_CMD_CLSR          0x30   /* Clear Status Register-1 - Erase/Prog. Fail Reset */
#define  QSFL_CMD_ABRD          0x14   /* Autoboot Register Read (max. 104MHz if QUAD=1) */
#define  QSFL_CMD_ABWR          0x15   /* Autoboot Register Write */
#define  QSFL_CMD_BRRD          0x16   /* Bank Register Read */
#define  QSFL_CMD_BRWR          0x17   /* Bank Register Write */
#define  QSFL_CMD_BRAC          0xB9   /* Bank Register Access (Legacy command formerly used for Deep Power Down) */
#define  QSFL_CMD_DLPRD         0x41   /* Data Learning Pattern Read */
#define  QSFL_CMD_PNVDLR        0x43   /* Program NV Data Learning Pattern */
#define  QSFL_CMD_WVDLR         0x4A   /* Write Volatile Data Learning Register */
/* Read Flash Array */
#define  QSFL_CMD_READ          0x03   /* Read (3- or 4-byte address) (max. 50MHz) */
#define  QSFL_CMD_4READ         0x13   /* Read (4-byte address) (max. 50MHz) */
#define  QSFL_CMD_FAST_READ     0x0B   /* Read (3- or 4-byte address) */
#define  QSFL_CMD_4FAST_READ    0x0C   /* Read (4-byte address) */
#define  QSFL_CMD_DDRFR         0x0D   /* DDR Fast Read (3- or 4-byte address) (max. 66MHz) */
#define  QSFL_CMD_4DDRFR        0x0E   /* DDR Fast Read (4-byte address) (max. 66MHz) */
#define  QSFL_CMD_DOR           0x3B   /* Read Dual Out (3- or 4-byte address) (max. 104MHz) */
#define  QSFL_CMD_4DOR          0x3C   /* Read Dual Out (4-byte address) (max. 104MHz) */
#define  QSFL_CMD_QOR           0x6B   /* Read Quad Out (3- or 4-byte address) (max. 104MHz) */
#define  QSFL_CMD_4QOR          0x6C   /* Read Quad Out (4-byte address) (max. 104MHz) */
#define  QSFL_CMD_DIOR          0xBB   /* Dual I/O Read (3- or 4-byte address) (max. 104MHz) */
#define  QSFL_CMD_4DIOR         0xBC   /* Dual I/O Read (4-byte address) (max. 104MHz) */
#define  QSFL_CMD_DDRDIOR       0xBD   /* DDR Dual I/O Read (3- or 4-byte address) (max. 66MHz) */
#define  QSFL_CMD_DDR4DIOR      0xBE   /* DDR Dual I/O Read (4-byte address) (max. 66MHz) */
#define  QSFL_CMD_QIOR          0xEB   /* Quad I/O Read (3- or 4-byte address) (max. 104MHz) */
#define  QSFL_CMD_4QIOR         0xEC   /* Quad I/O Read (4-byte address) (max. 104MHz) */
#define  QSFL_CMD_DDRQIOR       0xED   /* Quad I/O Read (3- or 4-byte address) (max. 66MHz) */
#define  QSFL_CMD_DDR4QIOR      0xEE   /* Quad I/O Read (4-byte address) (max. 66MHz) */
/* Program Flash Array */
#define  QSFL_CMD_PP            0x02   /* Page Program (3- or 4-byte address) (max. 80MHz) */
#define  QSFL_CMD_4PP           0x12   /* Page Program (4-byte address) (max. 80MHz) */
#define  QSFL_CMD_QPP           0x32   /* Quad Page Program (3- or 4-byte address) (max. 80MHz) (Alternate Instr. 0x38) */
#define  QSFL_CMD_4QPP          0x34   /* Quad Page Program (4-byte address) (max. 80MHz) */
#define  QSFL_CMD_PGSP          0x85   /* Program Suspend */
#define  QSFL_CMD_PGRS          0x8A   /* Program Resume */
/* Erase Flash */
#define  QSFL_CMD_P4E           0x20   /* Parameter 4kB, sector Erase (3- or 4-byte address) */
#define  QSFL_CMD_4P4E          0x21   /* Parameter 4kB, sector Erase (4-byte address) */
#define  QSFL_CMD_BE            0xC7   /* Bulk Erase (alternate instr. 0x60) */
#define  QSFL_CMD_SE            0xD8   /* Erase 64kB or 256kB (3- or 4-byte address) */
#define  QSFL_CMD_4SE           0xDC   /* Erase 64kB or 256kB (4-byte address) */
#define  QSFL_CMD_ERSP          0x75   /* Erase Suspend */
#define  QSFL_CMD_ERRS          0x7A   /* Erase Resume */
/* One Time Program Array */
#define  QSFL_CMD_OTPP          0x42   /* OTP Program */
#define  QSFL_CMD_OTPR          0x4B   /* OTP Read */
/* Advanced sector Protection */
#define  QSFL_CMD_DYBRD         0xE0   /* DYB Read */
#define  QSFL_CMD_DYBWR         0xE1   /* DYB Write */
#define  QSFL_CMD_PPBRD         0xE2   /* PPB Read */
#define  QSFL_CMD_PPBP          0xE3   /* PPB Program */
#define  QSFL_CMD_PPBE          0xE4   /* PPB Erase */
#define  QSFL_CMD_ASPRD         0x2B   /* ASP Read */
#define  QSFL_CMD_ASPP          0x2F   /* ASP Program */
#define  QSFL_CMD_PLBRD         0xA7   /* PPB Lock Bit Read */
#define  QSFL_CMD_PLBWR         0xA6   /* PPB Lock Bit Write */
#define  QSFL_CMD_PASSRD        0xE7   /* Password Read */
#define  QSFL_CMD_PASSP         0xE8   /* Password Program */
#define  QSFL_CMD_PASSU         0xE9   /* Password Unlock */
/* Reset */
#define  QSFL_CMD_RESET         0xF0   /* Software Reset */
#define  QSFL_CMD_MBR           0xFF   /* Mode Bit Reset */
/* Reserved for future use */
/* #define  QSFL_CMD_MPM           0xA3 */   /* Reserved for Multi-I/O-High Perf Mode (MPM) */
/* #define  QSFL_CMD_RFU18         0x18 */   /* Reserved for future use */
/* #define  QSFL_CMD_RFUE5         0xE5 */   /* Reserved for future use */
/* #define  QSFL_CMD_RFUE6         0xE6 */   /* Reserved for future use */

/*****************************************************************************/
/*****************************************************************************/
