/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  WDAQ - DCB
 *
 *  Author  :  schmid_e
 *  Created :  21.12.2014 14:25:20
 *
 *  Description :  Module for controling the MIO GPIOs.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "xfs_printf.h"
#include "dbg.h"
#include "cmd_processor.h"
#include "system.h"
#include "xgpiops.h"

/************************************************************/

  int cmd_gpio_mio_set(int argc, char **argv)
  {
    unsigned int mio_pin;
    unsigned int state;

    CMD_HELP("<mio_pin> <state>",
             "set MIO GPIO pin",
             "Set the state of the spedified MIO GPIO Pin.\r\n"  
             "  <mio_pin> : MIO number\r\n"
             "  <state>   : state of the output (0 or 1)\r\n"
            );

    if(argc < 3)
    {
      xfs_printf("E%02X: Too few arguments\r\n", ERR_TOO_FEW_ARGS);
      return 0;
    }

    mio_pin = (unsigned int)strtol(argv[1], NULL, 0);
    state   = (unsigned int)strtol(argv[2], NULL, 0);

    if(DBG_INF0) xfs_printf("Pin: %d   State: %d\r\n", mio_pin, state);
    XGpioPs_SetDirectionPin(SYSPTR(gpio_mio), mio_pin, 1);
    XGpioPs_SetOutputEnablePin(SYSPTR(gpio_mio), mio_pin, 1);
		XGpioPs_WritePin(SYSPTR(gpio_mio), mio_pin, state);
    
    return 0;
  }

/************************************************************/

int module_gpio_mio_help(int argc, char **argv)
  {
    CMD_HELP("",
             "MIO GPIO Control Module",
             "Can be used to set the state of an MIO GPIO output"
            );

    return 0;
  }


/************************************************************/
/* COMMAND TABLE                                            */
/************************************************************/

  cmd_table_entry_type gpio_mio_cmd_table[] =
  {
    {0, "mio", module_gpio_mio_help},
    {0, "mioset", cmd_gpio_mio_set},
    {0, NULL, NULL}
  };

/************************************************************/
