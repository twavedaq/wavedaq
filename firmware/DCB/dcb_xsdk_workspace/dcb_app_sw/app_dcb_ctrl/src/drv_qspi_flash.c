
/***************************** Include Files *********************************/

#include "drv_qspi_flash.h"
#include "xparameters.h"  /* SDK generated parameters */
#include "xqspips.h"      /* QSPI device driver */
#include "dbg.h"
#include "xfs_printf.h"
#include "sleep.h"

/************************** Constant Definitions *****************************/

/*
 * The following constants define the offsets within a FlashBuffer data
 * type for each kind of data.  Note that the read data offset is not the
 * same as the write data because the QSPI driver is designed to allow full
 * duplex transfers such that the number of bytes received is the number
 * sent and received.
 */
#define COMMAND_OFFSET    0 /* Flash instruction */
#define ADDRESS_1_OFFSET  1 /* MSB byte of address to read or write */
#define ADDRESS_2_OFFSET  2 /* Middle byte of address to read or write */
#define ADDRESS_3_OFFSET  3 /* LSB byte of address to read or write */
#define DATA_OFFSET       4 /* Start of Data for Read/Write */
#define DUMMY_OFFSET      4 /* Dummy byte offset for fast, dual and quad reads */
#define DUMMY_SIZE        1 /* Number of dummy bytes for fast, dual and quad reads */
#define RD_ID_SIZE        4 /* Read ID command + 3 bytes ID response */
#define RD_REG_SIZE       2 /* Read Register command + 1 byte response */
#define BANK_SEL_SIZE     2 /* BRWR or EARWR command + 1 byte bank value */
#define RD_CFG_SIZE       2 /* 1 byte Configuration register + RD CFG command*/
#define WR_CFG_SIZE       3 /* WRR command + 1 byte each Status and Config Reg*/

/* Base address of Flash1 */
#define FLASH1BASE   0x0000000

/* Sixteen MB */
#define SIXTEENMB    0x1000000


/* Mask for quad enable bit in Flash configuration register */
#define FLASH_QUAD_EN_MASK   0x02
#define FLASH_SRWD_MASK      0x80

/* Bank mask */
#define BANKMASK        0xF000000

/*
 * Identification of Flash
 * Micron:
 * Byte 0 is Manufacturer ID;
 * Byte 1 is first byte of Device ID - 0xBB or 0xBA
 * Byte 2 is second byte of Device ID describes flash size:
 * 128Mbit : 0x18; 256Mbit : 0x19; 512Mbit : 0x20
 * Spansion:
 * Byte 0 is Manufacturer ID;
 * Byte 1 is Device ID - Memory Interface type - 0x20 or 0x02
 * Byte 2 is second byte of Device ID describes flash size:
 * 128Mbit : 0x18; 256Mbit : 0x19; 512Mbit : 0x20
 */
#define MICRON_ID_BYTE0        0x20
#define MICRON_ID_BYTE2_128    0x18
#define MICRON_ID_BYTE2_256    0x19
#define MICRON_ID_BYTE2_512    0x20
#define MICRON_ID_BYTE2_1G     0x21

#define SPANSION_ID_BYTE0      0x01
#define SPANSION_ID_BYTE2_128  0x18
#define SPANSION_ID_BYTE2_256  0x19
#define SPANSION_ID_BYTE2_512  0x20

#define WINBOND_ID_BYTE0       0xEF
#define WINBOND_ID_BYTE2_128   0x18

#define MACRONIX_ID_BYTE0      0xC2
#define MACRONIX_ID_BYTE2_256  0x19
#define MACRONIX_ID_BYTE2_512  0x1A
#define MACRONIX_ID_BYTE2_1G   0x1B

#define ISSI_ID_BYTE0          0x9D
#define ISSI_ID_BYTE2_256      0x19

/*
 * The index for Flash config table
 */
/* Spansion*/
#define SPANSION_INDEX_START              0
#define FLASH_CFG_TBL_SINGLE_128_SP       SPANSION_INDEX_START
#define FLASH_CFG_TBL_STACKED_128_SP     (SPANSION_INDEX_START + 1)
#define FLASH_CFG_TBL_PARALLEL_128_SP    (SPANSION_INDEX_START + 2)
#define FLASH_CFG_TBL_SINGLE_256_SP      (SPANSION_INDEX_START + 3)
#define FLASH_CFG_TBL_STACKED_256_SP     (SPANSION_INDEX_START + 4)
#define FLASH_CFG_TBL_PARALLEL_256_SP    (SPANSION_INDEX_START + 5)
#define FLASH_CFG_TBL_SINGLE_512_SP      (SPANSION_INDEX_START + 6)
#define FLASH_CFG_TBL_STACKED_512_SP     (SPANSION_INDEX_START + 7)
#define FLASH_CFG_TBL_PARALLEL_512_SP    (SPANSION_INDEX_START + 8)

/* Micron */
#define MICRON_INDEX_START               (FLASH_CFG_TBL_PARALLEL_512_SP + 1)
#define FLASH_CFG_TBL_SINGLE_128_MC       MICRON_INDEX_START
#define FLASH_CFG_TBL_STACKED_128_MC     (MICRON_INDEX_START + 1)
#define FLASH_CFG_TBL_PARALLEL_128_MC    (MICRON_INDEX_START + 2)
#define FLASH_CFG_TBL_SINGLE_256_MC      (MICRON_INDEX_START + 3)
#define FLASH_CFG_TBL_STACKED_256_MC     (MICRON_INDEX_START + 4)
#define FLASH_CFG_TBL_PARALLEL_256_MC    (MICRON_INDEX_START + 5)
#define FLASH_CFG_TBL_SINGLE_512_MC      (MICRON_INDEX_START + 6)
#define FLASH_CFG_TBL_STACKED_512_MC     (MICRON_INDEX_START + 7)
#define FLASH_CFG_TBL_PARALLEL_512_MC    (MICRON_INDEX_START + 8)
#define FLASH_CFG_TBL_SINGLE_1GB_MC      (MICRON_INDEX_START + 9)
#define FLASH_CFG_TBL_STACKED_1GB_MC     (MICRON_INDEX_START + 10)
#define FLASH_CFG_TBL_PARALLEL_1GB_MC    (MICRON_INDEX_START + 11)

/* Winbond */
#define WINBOND_INDEX_START              (FLASH_CFG_TBL_PARALLEL_1GB_MC + 1)
#define FLASH_CFG_TBL_SINGLE_128_WB       WINBOND_INDEX_START
#define FLASH_CFG_TBL_STACKED_128_WB     (WINBOND_INDEX_START + 1)
#define FLASH_CFG_TBL_PARALLEL_128_WB    (WINBOND_INDEX_START + 2)

/* Macronix */
#define MACRONIX_INDEX_START             (FLASH_CFG_TBL_PARALLEL_128_WB + 1 - 3)
#define FLASH_CFG_TBL_SINGLE_256_MX       MACRONIX_INDEX_START
#define FLASH_CFG_TBL_STACKED_256_MX     (MACRONIX_INDEX_START + 1)
#define FLASH_CFG_TBL_PARALLEL_256_MX    (MACRONIX_INDEX_START + 2)
#define FLASH_CFG_TBL_SINGLE_512_MX      (MACRONIX_INDEX_START + 3)
#define FLASH_CFG_TBL_STACKED_512_MX     (MACRONIX_INDEX_START + 4)
#define FLASH_CFG_TBL_PARALLEL_512_MX    (MACRONIX_INDEX_START + 5)
#define FLASH_CFG_TBL_SINGLE_1G_MX       (MACRONIX_INDEX_START + 6)
#define FLASH_CFG_TBL_STACKED_1G_MX      (MACRONIX_INDEX_START + 7)
#define FLASH_CFG_TBL_PARALLEL_1G_MX     (MACRONIX_INDEX_START + 8)
/* ISSI */
#define ISSI_INDEX_START                 (FLASH_CFG_TBL_PARALLEL_1G_MX + 1)
#define FLASH_CFG_TBL_SINGLE_256_ISSI     ISSI_INDEX_START
#define FLASH_CFG_TBL_STACKED_256_ISSI   (ISSI_INDEX_START + 1)
#define FLASH_CFG_TBL_PARALLEL_256_ISSI  (ISSI_INDEX_START + 2)

/*
 * The following defines are for dual flash stacked mode interface.
 */
#define LQSPI_CR_FAST_QUAD_READ    0x0000006B /* Fast Quad Read output */
#define LQSPI_CR_1_DUMMY_BYTE      0x00000100 /* 1 Dummy Byte between address and return data */

#define DUAL_STACK_CONFIG_WRITE    (XQSPIPS_LQSPI_CR_TWO_MEM_MASK | \
           LQSPI_CR_1_DUMMY_BYTE | \
           LQSPI_CR_FAST_QUAD_READ)

#define DUAL_QSPI_CONFIG_WRITE    (XQSPIPS_LQSPI_CR_TWO_MEM_MASK | \
           XQSPIPS_LQSPI_CR_SEP_BUS_MASK | \
           LQSPI_CR_1_DUMMY_BYTE | \
           LQSPI_CR_FAST_QUAD_READ)

/*
 * Number of flash pages to be written.
 */
#define PAGE_COUNT            32

/*
 * Max page size to initialize write and read buffer
 */
#define MAX_PAGE_SIZE       1024

/**************************** Type Definitions *******************************/

typedef struct{
  u32 sector_size;          /* Individual sector size or combined sector size in case of parallel config*/
  u32 nr_of_sectors;      /* Total no. of sectors in one/two flash devices */
  u32 page_size;          /* Individual page size or combined page size in case of parallel config*/
  u32 nr_of_pages;        /* Total no. of pages in one/two flash devices */
  u32 device_size;        /* This is the size of one flash device NOT the combination of both devices, if present */
  u8  manufacturer_id;    /* Manufacturer ID - used to identify make */
  u8  device_id_mem_size; /* Byte of device ID indicating the memory size */
  u32 sector_mask;        /* Mask to get sector start address */
  u8  nr_of_dies;         /* No. of die forming a single flash */
}FlashInfo;

/***************** Macros (Inline Functions) Definitions *********************/

/************************** Function Prototypes ******************************/

/************************** Variable Definitions *****************************/

FlashInfo Flash_Config_Table[34] = {
  /* Spansion */
  {0x10000,  0x100,  256,  0x10000, 0x1000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_128, 0xFFFF0000, 1},
  {0x10000,  0x200,  256,  0x20000, 0x1000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_128, 0xFFFF0000, 1},
  {0x20000,  0x100,  512,  0x10000, 0x1000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_128, 0xFFFE0000, 1},
  {0x10000,  0x200,  256,  0x20000, 0x2000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_256, 0xFFFF0000, 1},
  {0x10000,  0x400,  256,  0x40000, 0x2000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_256, 0xFFFF0000, 1},
  {0x20000,  0x200,  512,  0x20000, 0x2000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_256, 0xFFFE0000, 1},
  {0x40000,  0x100,  512,  0x20000, 0x4000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_512, 0xFFFC0000, 1},
  {0x40000,  0x200,  512,  0x40000, 0x4000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_512, 0xFFFC0000, 1},
  {0x80000,  0x100, 1024,  0x20000, 0x4000000, SPANSION_ID_BYTE0, SPANSION_ID_BYTE2_512, 0xFFF80000, 1},
  /* Spansion 1Gbit is handled as 512Mbit stacked */
  /* Micron */
  {0x10000,  0x100,  256,  0x10000, 0x1000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_128,   0xFFFF0000, 1},
  {0x10000,  0x200,  256,  0x20000, 0x1000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_128,   0xFFFF0000, 1},
  {0x20000,  0x100,  512,  0x10000, 0x1000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_128,   0xFFFE0000, 1},
  {0x10000,  0x200,  256,  0x20000, 0x2000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_256,   0xFFFF0000, 1},
  {0x10000,  0x400,  256,  0x40000, 0x2000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_256,   0xFFFF0000, 1},
  {0x20000,  0x200,  512,  0x20000, 0x2000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_256,   0xFFFE0000, 1},
  {0x10000,  0x400,  256,  0x40000, 0x4000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_512,   0xFFFF0000, 2},
  {0x10000,  0x800,  256,  0x80000, 0x4000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_512,   0xFFFF0000, 2},
  {0x20000,  0x400,  512,  0x40000, 0x4000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_512,   0xFFFE0000, 2},
  {0x10000,  0x800,  256,  0x80000, 0x8000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_1G,    0xFFFF0000, 4},
  {0x10000, 0x1000,  256, 0x100000, 0x8000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_1G,    0xFFFF0000, 4},
  {0x20000,  0x800,  512,  0x80000, 0x8000000, MICRON_ID_BYTE0,   MICRON_ID_BYTE2_1G,    0xFFFE0000, 4},
  /* Winbond */
  {0x10000,  0x100,  256,  0x10000, 0x1000000, WINBOND_ID_BYTE0,  WINBOND_ID_BYTE2_128,  0xFFFF0000, 1},
  {0x10000,  0x200,  256,  0x20000, 0x1000000, WINBOND_ID_BYTE0,  WINBOND_ID_BYTE2_128,  0xFFFF0000, 1},
  {0x20000,  0x100,  512,  0x10000, 0x1000000, WINBOND_ID_BYTE0,  WINBOND_ID_BYTE2_128,  0xFFFE0000, 1},
  /* Macronix */
  {0x10000,  0x200,  256,  0x20000, 0x2000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_256, 0xFFFF0000, 1},
  {0x10000,  0x400,  256,  0x40000, 0x2000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_256, 0xFFFF0000, 1},
  {0x20000,  0x200,  512,  0x20000, 0x2000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_256, 0xFFFE0000, 1},
  {0x10000,  0x400,  256,  0x40000, 0x4000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_512, 0xFFFF0000, 1},
  {0x10000,  0x800,  256,  0x80000, 0x4000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_512, 0xFFFF0000, 1},
  {0x20000,  0x400,  512,  0x40000, 0x4000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_512, 0xFFFE0000, 1},
  { 0x2000, 0x4000,  256,  0x80000, 0x8000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_1G,  0xFFFF0000, 1},
  { 0x2000, 0x8000,  256, 0x100000, 0x8000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_1G,  0xFFFF0000, 1},
  { 0x4000, 0x4000,  512,  0x80000, 0x8000000, MACRONIX_ID_BYTE0, MACRONIX_ID_BYTE2_1G,  0xFFFE0000, 1},
  /* ISSI */
  {0x10000,  0x200,  256,  0x20000, 0x2000000, ISSI_ID_BYTE0,     ISSI_ID_BYTE2_256,     0xFFFF0000, 1}
};        /**< Flash Config Table */

unsigned int flash_manufacturer;
unsigned int flash_cfg_table_index;

/*****************************************************************************/

int qspi_flash_init(XQspiPs *qspi_instance_ptr, unsigned int qspi_device_id)
{
  unsigned char read_buffer[4];
  int status;

  XQspiPs_Config *qspi_config;

  /* Initialize the QSPI driver so that it's ready to use */
  qspi_config = XQspiPs_LookupConfig(qspi_device_id);
  if (qspi_config == NULL) return XST_FAILURE;

  status = XQspiPs_CfgInitialize(qspi_instance_ptr, qspi_config, qspi_config->BaseAddress);
  if (status != XST_SUCCESS) return XST_FAILURE;

  /* Perform a self-test to check hardware build */
  status = XQspiPs_SelfTest(qspi_instance_ptr);
  if (status != XST_SUCCESS) return XST_FAILURE;

  /* Set the pre-scaler for QSPI clock */
  XQspiPs_SetClkPrescaler(qspi_instance_ptr, XQSPIPS_CLK_PRESCALE_8);

  /* Set Manual Start and Manual Chip select options and drive the HOLD_B high. */
  XQspiPs_SetOptions(qspi_instance_ptr, XQSPIPS_FORCE_SSELECT_OPTION |
                                        XQSPIPS_MANUAL_START_OPTION |
                                        XQSPIPS_HOLD_B_DRIVE_OPTION);
  if(qspi_config->ConnectionMode == XQSPIPS_CONNECTION_MODE_STACKED)
  {
    /* Enable two flash memories, Shared bus (NOT separate bus), L_PAGE selected by default */
    XQspiPs_SetLqspiConfigReg(qspi_instance_ptr, DUAL_STACK_CONFIG_WRITE);
  }

  if(qspi_config->ConnectionMode == XQSPIPS_CONNECTION_MODE_PARALLEL)
  {
    /* Enable two flash memories on separate buses */
    XQspiPs_SetLqspiConfigReg(qspi_instance_ptr, DUAL_QSPI_CONFIG_WRITE);
  }

  /* Assert the Flash chip select. */
  XQspiPs_SetSlaveSelect(qspi_instance_ptr);

  /*
   * Read flash ID and obtain all flash related information
   * It is important to call the read id function before
   * performing proceeding to any operation, including
   * preparing the write_buffer
   */
  qspi_flash_read_id(qspi_instance_ptr, read_buffer);

  return XST_SUCCESS;
}

/*****************************************************************************/

unsigned char qspi_flash_read_reg(XQspiPs *qspi_instance_ptr, unsigned char read_cmd)
{
  unsigned char rd_buffer[RD_REG_SIZE];
  unsigned char cmd_buffer[2];

  /* Read ID in Auto mode. */
  cmd_buffer[COMMAND_OFFSET]   = read_cmd;
  cmd_buffer[ADDRESS_1_OFFSET] = 0x00;    /* dummy byte */

  XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, rd_buffer, RD_REG_SIZE);

  return rd_buffer[RD_REG_SIZE-1];
}

/*****************************************************************************/

unsigned char qspi_flash_read_stat1(XQspiPs *qspi_instance_ptr)
{
  return qspi_flash_read_reg(qspi_instance_ptr, QSFL_READ_STATUS1_CMD);
}

/*****************************************************************************/

unsigned char qspi_flash_read_stat2(XQspiPs *qspi_instance_ptr)
{
  return qspi_flash_read_reg(qspi_instance_ptr, QSFL_READ_STATUS2_CMD);
}

/*****************************************************************************/

unsigned char qspi_flash_read_cfg(XQspiPs *qspi_instance_ptr)
{
  return qspi_flash_read_reg(qspi_instance_ptr, QSFL_READ_CONFIG_CMD);
}

/*****************************************************************************/

int qspi_flash_read_id(XQspiPs *qspi_instance_ptr, unsigned char *rd_buffer_ptr)
{
  unsigned char cmd_buffer[ADDRESS_3_OFFSET+1];
  int status;
  int start_index;

  /* Read ID in Auto mode. */
  cmd_buffer[COMMAND_OFFSET]   = QSFL_READ_ID_CMD;
  cmd_buffer[ADDRESS_1_OFFSET] = 0xC0;    /* 3 dummy bytes */
  cmd_buffer[ADDRESS_2_OFFSET] = 0xDE;
  cmd_buffer[ADDRESS_3_OFFSET] = 0xAD;

  status = XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, rd_buffer_ptr, RD_ID_SIZE);
  if (status != XST_SUCCESS) {
    return XST_FAILURE;
  }

  /* Deduce flash make */
  if(rd_buffer_ptr[1] == MICRON_ID_BYTE0)
  {
    flash_manufacturer = MICRON_ID_BYTE0;
    start_index        = MICRON_INDEX_START;
  }
  else if(rd_buffer_ptr[1] == SPANSION_ID_BYTE0)
  {
    flash_manufacturer = SPANSION_ID_BYTE0;
    start_index        = SPANSION_INDEX_START;
  }
  else if(rd_buffer_ptr[1] == WINBOND_ID_BYTE0)
  {
    flash_manufacturer = WINBOND_ID_BYTE0;
    start_index        = WINBOND_INDEX_START;
  }
  else if(rd_buffer_ptr[1] == MACRONIX_ID_BYTE0)
  {
    flash_manufacturer = MACRONIX_ID_BYTE0;
    start_index        = MACRONIX_INDEX_START;
  }
  else if(rd_buffer_ptr[0] == ISSI_ID_BYTE0)
  {
    flash_manufacturer = ISSI_ID_BYTE0;
    start_index        = ISSI_INDEX_START;
  }


  /* If valid flash ID, then check connection mode & size and
   * assign corresponding index in the Flash configuration table
   */
  if(((flash_manufacturer == MICRON_ID_BYTE0) || (flash_manufacturer == SPANSION_ID_BYTE0)||
      (flash_manufacturer == WINBOND_ID_BYTE0)) &&
      (rd_buffer_ptr[3] == MICRON_ID_BYTE2_128))
  {
    switch(qspi_instance_ptr->Config.ConnectionMode)
    {
      case XQSPIPS_CONNECTION_MODE_SINGLE:
        flash_cfg_table_index = FLASH_CFG_TBL_SINGLE_128_SP + start_index;
        break;
      case XQSPIPS_CONNECTION_MODE_PARALLEL:
        flash_cfg_table_index = FLASH_CFG_TBL_PARALLEL_128_SP + start_index;
        break;
      case XQSPIPS_CONNECTION_MODE_STACKED:
        flash_cfg_table_index = FLASH_CFG_TBL_STACKED_128_SP + start_index;
        break;
      default:
        flash_cfg_table_index = 0;
        break;
    }
  }
  /* 256 and 512Mbit supported only for Micron and Spansion, not Winbond */
  if( ( (flash_manufacturer == MICRON_ID_BYTE0) || (flash_manufacturer == SPANSION_ID_BYTE0) || (flash_manufacturer == MACRONIX_ID_BYTE0) ) &&
        (rd_buffer_ptr[3] == MICRON_ID_BYTE2_256) )
  {
    switch(qspi_instance_ptr->Config.ConnectionMode)
    {
      case XQSPIPS_CONNECTION_MODE_SINGLE:
        flash_cfg_table_index = FLASH_CFG_TBL_SINGLE_256_SP + start_index;
        break;
      case XQSPIPS_CONNECTION_MODE_PARALLEL:
        flash_cfg_table_index = FLASH_CFG_TBL_PARALLEL_256_SP + start_index;
        break;
      case XQSPIPS_CONNECTION_MODE_STACKED:
        flash_cfg_table_index = FLASH_CFG_TBL_STACKED_256_SP + start_index;
        break;
      default:
        flash_cfg_table_index = 0;
        break;
    }
  }
  if( (flash_manufacturer == ISSI_ID_BYTE0) && (rd_buffer_ptr[2] == MICRON_ID_BYTE2_256) )
  {
    switch(qspi_instance_ptr->Config.ConnectionMode)
    {
      case XQSPIPS_CONNECTION_MODE_SINGLE:
        flash_cfg_table_index = FLASH_CFG_TBL_SINGLE_256_ISSI;
        break;
      case XQSPIPS_CONNECTION_MODE_PARALLEL:
        flash_cfg_table_index = FLASH_CFG_TBL_PARALLEL_256_ISSI;
        break;
      case XQSPIPS_CONNECTION_MODE_STACKED:
        flash_cfg_table_index = FLASH_CFG_TBL_STACKED_256_ISSI;
        break;
      default:
        flash_cfg_table_index = 0;
        break;
    }
  }
  if ( ( ( (flash_manufacturer == MICRON_ID_BYTE0) || (flash_manufacturer == SPANSION_ID_BYTE0) ) && (rd_buffer_ptr[3] == MICRON_ID_BYTE2_512) ) ||
       (   (flash_manufacturer == MACRONIX_ID_BYTE0) && (rd_buffer_ptr[3] == MACRONIX_ID_BYTE2_512) ) )
  {

    switch(qspi_instance_ptr->Config.ConnectionMode)
    {
      case XQSPIPS_CONNECTION_MODE_SINGLE:
        flash_cfg_table_index = FLASH_CFG_TBL_SINGLE_512_SP + start_index;
        break;
      case XQSPIPS_CONNECTION_MODE_PARALLEL:
        flash_cfg_table_index = FLASH_CFG_TBL_PARALLEL_512_SP + start_index;
        break;
      case XQSPIPS_CONNECTION_MODE_STACKED:
        flash_cfg_table_index = FLASH_CFG_TBL_STACKED_512_SP + start_index;
        break;
      default:
        flash_cfg_table_index = 0;
        break;
    }
  }
  /*
   * 1Gbit Single connection supported for Spansion.
   * The ConnectionMode will indicate stacked as this part has 2 SS
   * The device ID will indicate 512Mbit.
   * This configuration is handled as the above 512Mbit stacked configuration
   */
  /* 1Gbit single, parallel and stacked supported for Micron */
  if( ( (flash_manufacturer == MICRON_ID_BYTE0)   && (rd_buffer_ptr[3] == MICRON_ID_BYTE2_1G)   ) ||
      ( (flash_manufacturer == MACRONIX_ID_BYTE0) && (rd_buffer_ptr[3] == MACRONIX_ID_BYTE2_1G) ) )
  {
    switch(qspi_instance_ptr->Config.ConnectionMode)
    {
      case XQSPIPS_CONNECTION_MODE_SINGLE:
        flash_cfg_table_index = FLASH_CFG_TBL_SINGLE_1GB_MC;
        break;
      case XQSPIPS_CONNECTION_MODE_PARALLEL:
        flash_cfg_table_index = FLASH_CFG_TBL_PARALLEL_1GB_MC;
        break;
      case XQSPIPS_CONNECTION_MODE_STACKED:
        flash_cfg_table_index = FLASH_CFG_TBL_STACKED_1GB_MC;
        break;
      default:
        flash_cfg_table_index = 0;
        break;
    }
  }

  if( DBG_ALL ) xfs_printf("QSPI flash ID = 0x%02X 0x%02X 0x%02X\n\r", rd_buffer_ptr[1], rd_buffer_ptr[2], rd_buffer_ptr[3]);

  return XST_SUCCESS;
}

/*****************************************************************************/

unsigned int qspi_flash_write_in_progress(XQspiPs *qspi_instance_ptr)
{
	unsigned char rd_status_cmd_seq[] = { QSFL_READ_STATUS_CMD, 0 };  /* Must send 2 bytes */
  unsigned char flash_status[2];

  XQspiPs_PolledTransfer(qspi_instance_ptr, rd_status_cmd_seq, flash_status, sizeof(rd_status_cmd_seq));
  if (flash_status[1] & 0x01)
  {
    return 1;
  }
  else
  {
    return 0;
  }
}

/******************************************************************************/

int qspi_flash_wait_write_done(XQspiPs *qspi_instance_ptr, unsigned int timeout_us)
{
  unsigned int timer = timeout_us;

  /* Move checks to the beginning of the functions (calling this routine) */
  while( timer && qspi_flash_write_in_progress(qspi_instance_ptr) )
  {
    usleep(1);
    timer--;
  }

  if(!timer)
  {
    if (DBG_ERR) xfs_local_printf("SPI flash timeout\r\n");
    return -1;
  }

  return 1;
}

/******************************************************************************/

unsigned int get_real_addr(XQspiPs *qspi_instance_ptr, unsigned int address)
{
  unsigned int lqspi_cfg_reg;
  unsigned int real_addr;

  switch(qspi_instance_ptr->Config.ConnectionMode)
  {
    case XQSPIPS_CONNECTION_MODE_SINGLE:
      real_addr = address;
      break;

    case XQSPIPS_CONNECTION_MODE_STACKED:
      /* Get the current LQSPI Config reg value */
      lqspi_cfg_reg = XQspiPs_GetLqspiConfigReg(qspi_instance_ptr);

      /* Select lower or upper Flash based on sector address */
      if( address & Flash_Config_Table[flash_cfg_table_index].device_size )
      {
        /* Set selection to U_PAGE */
        XQspiPs_SetLqspiConfigReg(qspi_instance_ptr, lqspi_cfg_reg | XQSPIPS_LQSPI_CR_U_PAGE_MASK);

        /* Subtract first flash size when accessing second flash */
        real_addr = address & (~Flash_Config_Table[flash_cfg_table_index].device_size);
      }
      else
      {
        /* Set selection to L_PAGE */
        XQspiPs_SetLqspiConfigReg(qspi_instance_ptr, lqspi_cfg_reg & (~XQSPIPS_LQSPI_CR_U_PAGE_MASK));
        real_addr = address;
      }
      /* Assert the Flash chip select */
      XQspiPs_SetSlaveSelect(qspi_instance_ptr);
      break;

    case XQSPIPS_CONNECTION_MODE_PARALLEL:
      /* The effective address in each flash is the actual address/2  */
      real_addr = address / 2;
      break;

    default:
      /* real_addr wont be assigned in this case; */
      break;
  }

  return(real_addr);
}

/*****************************************************************************/

int send_bank_sel(XQspiPs *qspi_instance_ptr, unsigned int bank_sel)
{
  unsigned char cmd_buffer[ADDRESS_1_OFFSET+1];
  unsigned char wr_enable_cmd_seq = { QSFL_WRITE_ENABLE_CMD };

  /* Bank select commands for Micron and Spansion are different */
  if(flash_manufacturer == MICRON_ID_BYTE0)
  {
    /* For Micron command WREN should be sent first except for some specific feature set */
    XQspiPs_PolledTransfer(qspi_instance_ptr, &wr_enable_cmd_seq, NULL, sizeof(wr_enable_cmd_seq));

    cmd_buffer[COMMAND_OFFSET]   = QSFL_EXTADD_REG_WR_CMD;
    cmd_buffer[ADDRESS_1_OFFSET] = bank_sel;

    /* Send the Extended address register write command written, no receive buffer required */
    XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, NULL, BANK_SEL_SIZE);
  }
  if(flash_manufacturer == SPANSION_ID_BYTE0)
  {
    cmd_buffer[COMMAND_OFFSET]   = QSFL_BANK_REG_WR_CMD;
    cmd_buffer[ADDRESS_1_OFFSET] = bank_sel;

    /* Send the Extended address register write command written, no receive buffer required */
    XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, NULL, BANK_SEL_SIZE);
  }

  /* Winbond can be added here */

  return XST_SUCCESS;
}

/*****************************************************************************/

void qspi_flash_write(XQspiPs *qspi_instance_ptr, unsigned int address, unsigned int byte_count, unsigned char command, unsigned char *wr_buffer_ptr)
{
  unsigned char wr_enable_cmd_seq = { QSFL_WRITE_ENABLE_CMD };
  unsigned char cmd_buffer_ptr[QSFL_WR_OVERHEAD_SIZE];
  unsigned int  real_addr;
  unsigned int  bank_sel;
  unsigned char rd_flag_sr_cmd_seq[] = {QSFL_READ_FLAG_STATUS_CMD, 0};
  unsigned char flag_status[2];
  unsigned int  bytes_to_send = 0;

  while(byte_count)
  {
    if(byte_count > QSLF_WR_MAX_DATA_SIZE) bytes_to_send = QSLF_WR_MAX_DATA_SIZE;
    else                                   bytes_to_send = byte_count;

    /* Translate address */
    real_addr = get_real_addr(qspi_instance_ptr, address);
    /* Bank Select */
    if(Flash_Config_Table[flash_cfg_table_index].device_size > SIXTEENMB)
    {
      /* Calculate bank */
      bank_sel = real_addr/SIXTEENMB;
      /* Select bank */
      send_bank_sel(qspi_instance_ptr, bank_sel);
    }

    /* Send write enable */
    XQspiPs_PolledTransfer(qspi_instance_ptr, &wr_enable_cmd_seq, NULL, sizeof(wr_enable_cmd_seq));

    /* Setup the write command with the specified address and data for the
     * Flash.
     * This will ensure a 3B address is transferred even when address
     * is greater than 128Mb.
     */
    cmd_buffer_ptr[COMMAND_OFFSET]   = command;
    cmd_buffer_ptr[ADDRESS_1_OFFSET] = (unsigned char)((real_addr & 0xFF0000) >> 16);
    cmd_buffer_ptr[ADDRESS_2_OFFSET] = (unsigned char)((real_addr &   0xFF00) >>  8);
    cmd_buffer_ptr[ADDRESS_3_OFFSET] = (unsigned char)( real_addr &     0xFF);

    /* Send write command, address, and data (do not receive anything) */
    XQspiPs_PolledSplitTransfer(qspi_instance_ptr, cmd_buffer_ptr, QSFL_WR_OVERHEAD_SIZE, wr_buffer_ptr, NULL, bytes_to_send);

    if( (Flash_Config_Table[flash_cfg_table_index].nr_of_dies > 1) && (flash_manufacturer == MICRON_ID_BYTE0) )
    {
      XQspiPs_PolledTransfer(qspi_instance_ptr, rd_flag_sr_cmd_seq, flag_status, sizeof(rd_flag_sr_cmd_seq));
    }

    /* Wait for the write command to the Flash to be completed, it takes some time for the data to be written */
    qspi_flash_wait_write_done(qspi_instance_ptr, 10000);

    if( (Flash_Config_Table[flash_cfg_table_index].nr_of_dies > 1) && (flash_manufacturer == MICRON_ID_BYTE0) )
    {
      XQspiPs_PolledTransfer(qspi_instance_ptr, rd_flag_sr_cmd_seq, flag_status, sizeof(rd_flag_sr_cmd_seq));
    }
    byte_count -= bytes_to_send;
    wr_buffer_ptr = wr_buffer_ptr + bytes_to_send;
    address += bytes_to_send;
  }
}

/*****************************************************************************/

void qspi_flash_read(XQspiPs *qspi_instance_ptr, unsigned int address, unsigned int byte_count, unsigned char command, unsigned char *rd_buffer_ptr)
{
  unsigned char wr_buffer[byte_count+QSFL_WR_OVERHEAD_SIZE+DUMMY_SIZE];
//  unsigned char cmd_buffer[QSFL_WR_OVERHEAD_SIZE];
//  unsigned char wr_buffer[byte_count+DUMMY_SIZE];
  unsigned int real_addr;
  unsigned int real_byte_count;
  unsigned int bank_sel;
  unsigned int buffer_index;
  unsigned int total_byte_count;
  unsigned char shift_size;

  /* Retain the actual byte count */
  total_byte_count = byte_count;

  while( ((signed long)(byte_count)) > 0 )
  {

    /* Translate address based on type of connection. If stacked assert the slave select based on address */
    real_addr = get_real_addr(qspi_instance_ptr, address);

    /* Select bank */
    if(Flash_Config_Table[flash_cfg_table_index].device_size > SIXTEENMB)
    {
      bank_sel = real_addr/SIXTEENMB;
      send_bank_sel(qspi_instance_ptr, bank_sel);
    }

    /* Handle read across bank boundaries */
    if( (address & BANKMASK) != ((address+byte_count) & BANKMASK) )
    {
      real_byte_count = (address & BANKMASK) + SIXTEENMB - address;
    }
    else
    {
      real_byte_count = byte_count;
    }


    /* Setup the write command with the specified address and data for the flash */
    wr_buffer[COMMAND_OFFSET]   = command;
    wr_buffer[ADDRESS_1_OFFSET] = (unsigned char)((real_addr & 0xFF0000) >> 16);
    wr_buffer[ADDRESS_2_OFFSET] = (unsigned char)((real_addr &   0xFF00) >>  8);
    wr_buffer[ADDRESS_3_OFFSET] = (unsigned char)( real_addr &     0xFF);
//    cmd_buffer[COMMAND_OFFSET]   = command;
//    cmd_buffer[ADDRESS_1_OFFSET] = (unsigned char)((real_addr & 0xFF0000) >> 16);
//    cmd_buffer[ADDRESS_2_OFFSET] = (unsigned char)((real_addr &   0xFF00) >>  8);
//    cmd_buffer[ADDRESS_3_OFFSET] = (unsigned char)( real_addr &     0xFF);

    if ( (command == QSFL_FAST_READ_CMD) || (command == QSFL_DUAL_READ_CMD) || (command == QSFL_QUAD_READ_CMD) )
    {
      real_byte_count += DUMMY_SIZE;
    }
    /* Send the read command */
    XQspiPs_PolledTransfer(qspi_instance_ptr, wr_buffer, &(rd_buffer_ptr[total_byte_count - byte_count]), real_byte_count + QSFL_WR_OVERHEAD_SIZE);
//    XQspiPs_PolledSplitTransfer(qspi_instance_ptr, cmd_buffer, QSFL_WR_OVERHEAD_SIZE, wr_buffer, &(rd_buffer_ptr[total_byte_count - byte_count]), real_byte_count);

    /* Discard the first 5 dummy bytes, shift the data in read buffer */
    if( (command == QSFL_FAST_READ_CMD) || (command == QSFL_DUAL_READ_CMD) || (command == QSFL_QUAD_READ_CMD) )
    {
      shift_size = QSFL_WR_OVERHEAD_SIZE + DUMMY_SIZE;
    }
    else
    {
      shift_size =  QSFL_WR_OVERHEAD_SIZE;
    }

    for( buffer_index = (total_byte_count - byte_count);
         buffer_index < (total_byte_count - byte_count) + real_byte_count;
         buffer_index++ )
    {
      rd_buffer_ptr[buffer_index] = rd_buffer_ptr[buffer_index + shift_size];
    }

    /* Increase address to next bank */
    address = (address & BANKMASK) + SIXTEENMB;
    /* Decrease byte count by bytes already read */
    if ( (command == QSFL_FAST_READ_CMD) || (command == QSFL_DUAL_READ_CMD) || (command == QSFL_QUAD_READ_CMD) )
    {
      byte_count = byte_count - (real_byte_count - DUMMY_SIZE);
    }
    else
    {
      byte_count = byte_count - real_byte_count;
    }
  }
}

/*****************************************************************************/

void qspi_flash_erase(XQspiPs *qspi_instance_ptr, unsigned int address, unsigned int byte_count)
{
  unsigned char cmd_buffer[ADDRESS_3_OFFSET+1];
  unsigned char wr_enable_cmd_seq = { QSFL_WRITE_ENABLE_CMD };
  int sector;
  unsigned int real_addr;
  unsigned int lqspi_cfg_reg;
  unsigned int nr_of_sectors;
  unsigned int bank_sel;
  unsigned char bank_init_flag = 1;
  unsigned char rd_flag_sr_cmd_seq[] = { QSFL_READ_FLAG_STATUS_CMD, 0 };
  unsigned char flag_status[2];

  if (byte_count >= ((Flash_Config_Table[flash_cfg_table_index]).nr_of_sectors * (Flash_Config_Table[flash_cfg_table_index]).sector_size) )
  {
    if(qspi_instance_ptr->Config.ConnectionMode == XQSPIPS_CONNECTION_MODE_STACKED)
    {
      lqspi_cfg_reg = XQspiPs_GetLqspiConfigReg(qspi_instance_ptr);
      /* Set selection to L_PAGE */
      XQspiPs_SetLqspiConfigReg(qspi_instance_ptr, lqspi_cfg_reg & (~XQSPIPS_LQSPI_CR_U_PAGE_MASK));
      XQspiPs_SetSlaveSelect(qspi_instance_ptr);
    }

    if(Flash_Config_Table[flash_cfg_table_index].nr_of_dies == 1)
    {
      qspi_flash_bulk_erase(qspi_instance_ptr);
    }

    if(Flash_Config_Table[flash_cfg_table_index].nr_of_dies > 1)
    {
      qspi_flash_die_erase(qspi_instance_ptr);
    }

    if(qspi_instance_ptr->Config.ConnectionMode == XQSPIPS_CONNECTION_MODE_STACKED)
    {
      lqspi_cfg_reg = XQspiPs_GetLqspiConfigReg(qspi_instance_ptr);
      /* Set selection to U_PAGE */
      XQspiPs_SetLqspiConfigReg(qspi_instance_ptr, lqspi_cfg_reg | XQSPIPS_LQSPI_CR_U_PAGE_MASK);
      XQspiPs_SetSlaveSelect(qspi_instance_ptr);

      if(Flash_Config_Table[flash_cfg_table_index].nr_of_dies == 1)
      {
        qspi_flash_bulk_erase(qspi_instance_ptr);
      }

      if(Flash_Config_Table[flash_cfg_table_index].nr_of_dies > 1)
      {
        qspi_flash_die_erase(qspi_instance_ptr);
      }
    }
    return;
  }

  nr_of_sectors = byte_count/(Flash_Config_Table[flash_cfg_table_index].sector_size) + 1;

  /* If byte_count to k sectors,
   * but the address range spans from N to N+k+1 sectors, then
   * increment no. of sectors to be erased
   */
  if( ((address + byte_count) & Flash_Config_Table[flash_cfg_table_index].sector_mask) ==
      ((address + (nr_of_sectors * Flash_Config_Table[flash_cfg_table_index].sector_size)) &
      Flash_Config_Table[flash_cfg_table_index].sector_mask) )
  {
    nr_of_sectors++;
  }

  for (sector = 0; sector < nr_of_sectors; sector++)
  {
    real_addr = get_real_addr(qspi_instance_ptr, address);

    if( Flash_Config_Table[flash_cfg_table_index].device_size > SIXTEENMB )
    {
      if( bank_init_flag ) bank_init_flag = 0;
      bank_sel = real_addr/SIXTEENMB;
      send_bank_sel(qspi_instance_ptr, bank_sel);
    }

    XQspiPs_PolledTransfer(qspi_instance_ptr, &wr_enable_cmd_seq, NULL, sizeof(wr_enable_cmd_seq));

    cmd_buffer[COMMAND_OFFSET]   = QSFL_SEC_ERASE_CMD;
    cmd_buffer[ADDRESS_1_OFFSET] = (unsigned char)(real_addr >> 16);
    cmd_buffer[ADDRESS_2_OFFSET] = (unsigned char)(real_addr >> 8);
    cmd_buffer[ADDRESS_3_OFFSET] = (unsigned char)(real_addr & 0xFF);

    XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, NULL, sizeof(cmd_buffer));

    if( (Flash_Config_Table[flash_cfg_table_index].nr_of_dies > 1) && (flash_manufacturer == MICRON_ID_BYTE0) )
    {
      XQspiPs_PolledTransfer(qspi_instance_ptr, rd_flag_sr_cmd_seq, flag_status, sizeof(rd_flag_sr_cmd_seq));
    }

    qspi_flash_wait_write_done(qspi_instance_ptr, 10000000);

    if( (Flash_Config_Table[flash_cfg_table_index].nr_of_dies > 1) && (flash_manufacturer == MICRON_ID_BYTE0) )
    {
      XQspiPs_PolledTransfer(qspi_instance_ptr, rd_flag_sr_cmd_seq, flag_status, sizeof(rd_flag_sr_cmd_seq));
    }

    address += Flash_Config_Table[flash_cfg_table_index].sector_size;
  }
}

/*****************************************************************************/

void qspi_flash_parameter_erase(XQspiPs *qspi_instance_ptr, unsigned int address)
{
  unsigned char cmd_buffer[ADDRESS_3_OFFSET+1];
  unsigned char wr_enable_cmd_seq = { QSFL_WRITE_ENABLE_CMD };
  unsigned int real_addr;
  unsigned int bank_sel;

  real_addr = get_real_addr(qspi_instance_ptr, address);

  cmd_buffer[COMMAND_OFFSET]   = QSFL_PARAM_ERASE_CMD;
  cmd_buffer[ADDRESS_1_OFFSET] = (unsigned char)(real_addr >> 16);
  cmd_buffer[ADDRESS_2_OFFSET] = (unsigned char)(real_addr >> 8);
  cmd_buffer[ADDRESS_3_OFFSET] = (unsigned char)(real_addr & 0xFF);

  if( Flash_Config_Table[flash_cfg_table_index].device_size > SIXTEENMB )
  {
    bank_sel = real_addr/SIXTEENMB;
    send_bank_sel(qspi_instance_ptr, bank_sel);
  }

  XQspiPs_PolledTransfer(qspi_instance_ptr, &wr_enable_cmd_seq, NULL, sizeof(wr_enable_cmd_seq));

  XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, NULL, sizeof(cmd_buffer));

  qspi_flash_wait_write_done(qspi_instance_ptr, 10000000);
}

/*****************************************************************************/

void qspi_flash_bulk_erase(XQspiPs *qspi_instance_ptr)
{
  unsigned char cmd_buffer[COMMAND_OFFSET+1];
  unsigned char wr_enable_cmd_seq = { QSFL_WRITE_ENABLE_CMD };

  XQspiPs_PolledTransfer(qspi_instance_ptr, &wr_enable_cmd_seq, NULL, sizeof(wr_enable_cmd_seq));

  cmd_buffer[COMMAND_OFFSET] = QSFL_BULK_ERASE_CMD;

  XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, NULL, sizeof(cmd_buffer));

  qspi_flash_wait_write_done(qspi_instance_ptr, 10000);
}

/*****************************************************************************/

void qspi_flash_die_erase(XQspiPs *qspi_instance_ptr)
{
  unsigned char cmd_buffer[ADDRESS_3_OFFSET+1];
  unsigned char wr_enable_cmd_seq = { QSFL_WRITE_ENABLE_CMD };
  unsigned char die_count;

  for(die_count = 0; die_count < Flash_Config_Table[flash_cfg_table_index].nr_of_dies; die_count++)
  {
    /* Specific to Micron flash */
    send_bank_sel(qspi_instance_ptr, die_count*2);

    XQspiPs_PolledTransfer(qspi_instance_ptr, &wr_enable_cmd_seq, NULL, sizeof(wr_enable_cmd_seq));

    /* The address is the start address of die - MSB bits will be
     * derived from bank select by the flash
     */
    cmd_buffer[COMMAND_OFFSET]   = QSFL_DIE_ERASE_CMD;
    cmd_buffer[ADDRESS_1_OFFSET] = 0x00;
    cmd_buffer[ADDRESS_2_OFFSET] = 0x00;
    cmd_buffer[ADDRESS_3_OFFSET] = 0x00;

    XQspiPs_PolledTransfer(qspi_instance_ptr, cmd_buffer, NULL, sizeof(cmd_buffer));

    qspi_flash_wait_write_done(qspi_instance_ptr, 10000);
  }
}

/*****************************************************************************/
/*****************************************************************************/
