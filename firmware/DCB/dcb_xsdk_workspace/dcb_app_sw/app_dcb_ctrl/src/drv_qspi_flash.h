
/***************************** Include Files *********************************/

#include "xparameters.h"  /* SDK generated parameters */
#include "xqspips.h"      /* QSPI device driver */

/************************** Constant Definitions *****************************/

/*
 * The following constants define the commands which may be sent to the FLASH
 * device.
 */
#define QSFL_WRITE_STATUS_CMD       0x01   /* QSFL_CMD_WRR       */   
#define QSFL_WRITE_CMD              0x02   /* QSFL_CMD_PP        */
#define QSFL_QUAD_WRITE_CMD         0x32   /* QSFL_CMD_QPP       */
#define QSFL_READ_CMD               0x03   /* QSFL_CMD_READ      */
#define QSFL_WRITE_DISABLE_CMD      0x04   /* QSFL_CMD_WRDI      */
#define QSFL_READ_STATUS_CMD        0x05   /* QSFL_CMD_RSR1      */
#define QSFL_WRITE_ENABLE_CMD       0x06   /* QSFL_CMD_WREN      */
#define QSFL_FAST_READ_CMD          0x0B   /* QSFL_CMD_FAST_READ */
#define QSFL_DUAL_READ_CMD          0x3B   /* QSFL_CMD_DOR       */
#define QSFL_QUAD_READ_CMD          0x6B   /* QSFL_CMD_QOR       */
#define QSFL_BULK_ERASE_CMD         0xC7   /* QSFL_CMD_BE        */
#define QSFL_SEC_ERASE_CMD          0xD8   /* QSFL_CMD_SE        */
#define QSFL_PARAM_ERASE_CMD        0x20   /* QSFL_CMD_P4E       */
#define QSFL_READ_ID_CMD            0x9F   /* QSFL_CMD_RDID      */
#define QSFL_READ_CONFIG_CMD        0x35   /* QSFL_CMD_RDCR      */
#define QSFL_READ_STATUS1_CMD       0x05   /* QSFL_CMD_RSR1      */
#define QSFL_READ_STATUS2_CMD       0x07   /* QSFL_CMD_RSR2      */
#define QSFL_WRITE_CONFIG_CMD       0x01   /* QSFL_CMD_WRR       */
#define QSFL_BANK_REG_RD_CMD        0x16   /* QSFL_CMD_BRRD      */
#define QSFL_BANK_REG_WR_CMD        0x17   /* QSFL_CMD_BRWR      */
/* Bank register is called Extended Address Register in Micron */
#define QSFL_EXTADD_REG_RD_CMD      0xC8   /* --> Micron only <-- */
#define QSFL_EXTADD_REG_WR_CMD      0xC5   /* --> Micron only <-- */
#define QSFL_DIE_ERASE_CMD          0xC4   /* --> Micron only <-- */
#define QSFL_READ_FLAG_STATUS_CMD   0x70   /* --> Micron only <-- */

/* Command and address size for write instructions */
#define QSFL_WR_OVERHEAD_SIZE          4

#define QSLF_WR_MAX_DATA_SIZE        256   /* Spansion flash buffer size 256Bytes*/

/**************************** Type Definitions *******************************/

/***************** Macros (Inline Functions) Definitions *********************/

/************************** Function Prototypes ******************************/

int qspi_flash_init(XQspiPs *qspi_instance_ptr, unsigned int qspi_device_id);
unsigned char qspi_flash_read_stat1(XQspiPs *qspi_instance_ptr);
unsigned char qspi_flash_read_stat2(XQspiPs *qspi_instance_ptr);
unsigned char qspi_flash_read_cfg(XQspiPs *qspi_instance_ptr);
int qspi_flash_read_id(XQspiPs *qspi_instance_ptr, unsigned char *rd_buffer_ptr);
void qspi_flash_write(XQspiPs *qspi_instance_ptr, unsigned int address, unsigned int byte_count, unsigned char command, unsigned char *wr_buffer_ptr);
void qspi_flash_read(XQspiPs *qspi_instance_ptr, unsigned int address, unsigned int byte_count, unsigned char command, unsigned char *rd_buffer_ptr);
void qspi_flash_erase(XQspiPs *qspi_instance_ptr, unsigned int address, unsigned int byte_count);
void qspi_flash_parameter_erase(XQspiPs *qspi_instance_ptr, unsigned int address);
void qspi_flash_bulk_erase(XQspiPs *qspi_instance_ptr);
void qspi_flash_die_erase(XQspiPs *qspi_instance_ptr);

/************************** Variable Definitions *****************************/
