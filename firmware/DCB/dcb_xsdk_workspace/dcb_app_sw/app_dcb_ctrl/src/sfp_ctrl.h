/******************************************************************************/
/*                                                                            */
/*  file: sfp_ctrl.h                                                          */
/*                                                                            */
/*  (c) 2010 PSI tg32/se32                                                    */
/*                                                                            */
/******************************************************************************/

#ifndef __SFP_CTRL_H__
#define __SFP_CTRL_H__

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/

#include "xiicps.h"
#include "xgpiops.h"
#include "utilities.h"
#include "xtime_l.h"

/******************************************************************************/

#define IIC_ADDRESS(addr)   (addr>>1)

/******************************************************************************/

typedef struct
{
  XIicPs  *iic_dev_ptr;
  XGpioPs *emio_dev_ptr;

  xfs_u32 sfp_tx_disable_pin;
  xfs_u32 sfp_rate_sel0_pin;
  xfs_u32 sfp_rate_sel1_pin;
  xfs_u32 sfp_los_pin;
  xfs_u32 sfp_tx_fault_pin;
  xfs_u32 sfp_mod_def0_pin;

  xfs_u8  sfp_inserted;
  xfs_u8  marvell_phy;
  XTime   sfp_insert_delay;
} sfp_ctrl_type;


/******************************************************************************/

static const xfs_u32 SFP_MARVELL_PHY_ADR = 0xAC;
static const xfs_u32 SFP_EEPROM_ADDR     = 0xA0;
static const xfs_u32 SFP_EEPROM_ADDR_2   = 0xA2;

xfs_u32 sfp_ctrl_init(sfp_ctrl_type *self, XIicPs *init_iic_dev_ptr, XGpioPs *init_gpio_dev_ptr,
                      xfs_u32 init_rate_sel1_pin,
                      xfs_u32 init_rate_sel0_pin,
                      xfs_u32 init_tx_disable_pin,
                      xfs_u32 init_tx_fault_pin,
                      xfs_u32 init_mod_abs_pin,
                      xfs_u32 init_los_pin);

void sfp_marvell_phy_init(sfp_ctrl_type *self);
xfs_u32 sfp_module_present(sfp_ctrl_type *self);
xfs_u16 sfp_marvell_reg_read(sfp_ctrl_type *self, unsigned int reg_num);
void sfp_marvell_reg_write(sfp_ctrl_type *self, unsigned int reg_num, xfs_u16 data);
void marvell_reg_dump(sfp_ctrl_type *self);
void sfp_module_info(sfp_ctrl_type *self);
void gpio_sfp_read(sfp_ctrl_type *self);
int is_marvell_phy(sfp_ctrl_type *self);
void sfp_check_connection(sfp_ctrl_type *self);
/*
 * void gpio_link_up(sfp_ctrl_type *self, unsigned int state);
 * void gpio_sfp_error(sfp_ctrl_type *self, unsigned int state);
 */
#endif // __SFP_CTRL_H__
