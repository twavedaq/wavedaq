#include "system.h"
#include "xfs_printf.h"
#include "sc_io.h"
#include "sw_state.h"
#include "term_cmd_input.h"
#include "update_config.h"
#include "dbg.h"

/*void nw_if_handler(network_if_class *nw_if, tftp_class *tftp_server)*/
void nw_if_handler()
{
  sfp_check_connection(SYSPTR(sfp_ctrl[0]));
  sfp_check_connection(SYSPTR(sfp_ctrl[1]));
}

int main()
{
  /* Set default debug level */
  set_dbg_level(DBG_LEVEL_SPAM);

  init_system();

  if (DBG_INIT)
  {
    xfs_printf("\r\n");
    xfs_printf("---------------------------------------------------------\r\n");
    xfs_printf("-- WDAQ DCB Firmware                                   --\r\n");
    xfs_printf("---------------------------------------------------------\r\n\r\n");
    xfs_printf("---------------------------------------------------------\r\n");
    xfs_printf("-- Version Information:\r\n");
    print_sys_info();
    xfs_printf("---------------------------------------------------------\r\n\r\n");
  }

  term_cmd_input_init(SYSPTR(term_stdin), STDIN_BASEADDRESS);

  /* Initialization done */
  emio_reset_sw_state(SYSPTR(gpio_mio));

  while(1)
  {

    nw_if_handler();
    /*set_cmd_sender(CMD_IF_UDP);
     *nw_if_handler(SYSPTR(nw_if_gmac_0));
     *set_cmd_sender(CMD_IF_RS232);
     */
    term_cmd_input_chk_cmd(SYSPTR(term_stdin));
    /*set_cmd_sender(CMD_IF_SPI);
     *spi_cmd_input_chk_cmd(&spi_cmd_input);
     */
    auto_update_configurations();
    trigger_update_configurations();
  }

  return 0;
}
