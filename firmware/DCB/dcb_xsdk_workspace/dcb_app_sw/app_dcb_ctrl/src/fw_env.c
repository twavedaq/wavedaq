/******************************************************************************/
/*                                                                            */
/* (C) Copyright 2000-2010                                                    */
/* Wolfgang Denk, DENX Software Engineering, wd@denx.de.                      */
/*                                                                            */
/* (C) Copyright 2008                                                         */
/* Guennadi Liakhovetski, DENX Software Engineering, lg@denx.de.              */
/*                                                                            */
/* heavily modified for use in FPGA firmware at PSI                           */
/* but still compatible with original layout for U-Boot                       */
/*                                                                            */
/* See file CREDITS for list of people who contributed to this                */
/* project.                                                                   */
/*                                                                            */
/* This program is free software; you can redistribute it and/or              */
/* modify it under the terms of the GNU General Public License as             */
/* published by the Free Software Foundation; either version 2 of             */
/* the License, or (at your option) any later version.                        */
/*                                                                            */
/* This program is distributed in the hope that it will be useful,            */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of             */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the              */
/* GNU General Public License for more details.                               */
/*                                                                            */
/* You should have received a copy of the GNU General Public License          */
/* along with this program; if not, write to the Free Software                */
/* Foundation, Inc., 59 Temple Place, Suite 330, Boston,                      */
/* MA 02111-1307 USA                                                          */
/*                                                                            */
/******************************************************************************/


#include <stdio.h>
#include <string.h>

#include "xfs_printf.h"
#include "fw_env.h"
#include "dbg.h"

/******************************************************************************/
/*                                                                            */
/* very simple (but not very fast) implementation for crc32                   */
/*                                                                            */
/******************************************************************************/

uint32_t fw_env_crc32(uint32_t crc, const unsigned char *data, int len)
{
  int i;
  int bit;

  crc ^= 0xffffffff;

  for (i = 0; i < len; i++)
  {
    crc ^= data[i];
    for (bit = 0; bit < 8; bit++)
    {
      crc = (crc >> 1) ^ ((crc & 1) * FW_ENV_CRC32_POLYNOMIAL);
    }
  }

  return (crc ^ 0xffffffff);
}


/******************************************************************************/
/*                                                                            */
/* s1 is either a simple 'name', or a 'name=value' pair.                      */
/* s2 is a 'name=value' pair.                                                 */
/* If the names match, return the value of s2, else NULL.                     */
/*                                                                            */
/******************************************************************************/

static char *fw_env_match (const char *s1, char *s2)
{
  while (*s1 == *s2++)
  {
    if (*s1++ == '=') return s2;
  }

  if (*s1 == '\0' && *(s2 - 1) == '=') return s2;

  return NULL;
}

/******************************************************************************/

int fw_env_is_active(fw_env_type *self)
{
  if (self->stg_entries == 1) return 1;
  if (self->flags && (*self->flags == FW_ENV_FLAG_ACTIVE)) return 1;
  return 0;
}


/******************************************************************************/
/* check one environment entry for trigger function */

void fw_env_trigger_var(fw_env_type *self, char *env_entry)
{
  fw_env_trigger_entry_type *trigger_entry;
  char *val;
  
  if (!self->trigger_list) return;
  
  for (trigger_entry=self->trigger_list; trigger_entry->var_name ;trigger_entry++)
  {
    if ((val=fw_env_match (trigger_entry->var_name, env_entry)) != NULL)
    {
      if (trigger_entry->trigger_func) trigger_entry->trigger_func(trigger_entry->var_name, val);
      return;
    }
  }
  
};

/******************************************************************************/
/* check whole environment for trigger function */

void fw_env_trigger_all(fw_env_type *self)
{
  char *env, *nxt;
  
  if (!self->trigger_list) return;
  
  for (env = self->data; *env; env = nxt + 1)
  {
    for (nxt = env; *nxt; ++nxt) 
    {
      if (nxt >= &self->data[ self->data_size_max])
      {
        if (DBG_ERR) xfs_printf( "## Error: environment not terminated\r\n");
        return ;
      }
    }
    fw_env_trigger_var(self, env);

  }
};

/******************************************************************************/

ulong fw_env_init(fw_env_type *self, char *mem, unsigned int size, char *default_env, unsigned int default_env_size, fw_env_storage_desc_type *env_stg, unsigned int stg_entries, fw_env_trigger_entry_type *trigger_list)
{
  struct env_image_single    *single;
  struct env_image_redundant *redundant;

  self->env_mem          = mem;
  self->env_size         = size;
  self->default_env      = default_env;
  self->default_env_size = default_env_size;
  self->env_stg          = env_stg;
  self->stg_entries      = stg_entries;
  self->env_sel          = 0;
  self->data_size_max    = size - sizeof (unsigned int);
  self->trigger_list     = trigger_list;

  if (stg_entries > 1)
  {
    /* we have redundant envs */
    redundant = (struct env_image_redundant*)mem;

    self->data_size_max -= sizeof (char);
    self->crc   = &redundant->crc;
    self->flags = &redundant->flags;
    self->data  = redundant->data;
  }
  else
  {
    /* single env */
    single = (struct env_image_single*)mem;

    self->crc   = &single->crc;
    self->flags = NULL;
    self->data  = single->data;
  }

  return 0;
}


/******************************************************************************/

void fw_env_clean(fw_env_type *self)
{
  if (self->env_size < 8) return;

  /* zero first bytes */
  memset(self->env_mem, 0x00, 8);

  /* fill rest with clean pattern, could be skipped for performance reasons */
  memset(self->env_mem+8, FW_ENV_CLEAN_BYTE, self->env_size-8);
}


/******************************************************************************/

void fw_env_default(fw_env_type *self)
{

  fw_env_clean(self);

  if (self->default_env)
  {
    if (self->default_env_size <= self->data_size_max)
    {
      memcpy(self->data, self->default_env, self->default_env_size);
    }
    else
    {
      if (DBG_WARN) xfs_printf("Warning: Default Env too long! Not used.\r\n");
    }
  }
  fw_env_trigger_all(self);
}


/******************************************************************************/

int fw_env_load(fw_env_type *self)
{
  unsigned int crc_ok;
  int result;
  int ignore_active_flag;


  for (ignore_active_flag = ((self->stg_entries > 1) ? 0 : 1);  ignore_active_flag <= 1; ignore_active_flag++)
  {
    for (self->env_sel=0; self->env_sel < self->stg_entries; self->env_sel++)
    {

      result = self->env_stg[self->env_sel].storage_handler(&self->env_stg[self->env_sel], FW_ENV_STORAGE_READ, self->env_mem, self->env_size);

      if (result == FW_ENV_STORAGE_OK)
      {
        crc_ok = (*self->crc == fw_env_crc32 (0, (uint8_t *) self->data, self->data_size_max));
        if (crc_ok && ( ignore_active_flag || fw_env_is_active(self)) )
        {
          /* use current env  */
          fw_env_trigger_all(self);
          return 1;
        }
      }
    }
  }

  /* only reached if no valid env was found */
  self->env_sel=0;
  if (DBG_WARN) xfs_printf("Warning: No valid env found, using default environment\r\n");
  fw_env_default(self);

  return 0;
}


/******************************************************************************/

int fw_env_save(fw_env_type *self)
{
  int error = 0;
  int result;
  int env_sel;

  /* Update CRC */
  *self->crc = fw_env_crc32(0, (uint8_t *) self->data,  self->data_size_max);

  /* write env to all storage locations with location 0 as active  */
  for (env_sel=0; env_sel < (int)self->stg_entries; env_sel++)
  {
    if (self->stg_entries > 1)
    {
       /* active flag only in redundant envs */
       *self->flags = (env_sel == 0) ? FW_ENV_FLAG_ACTIVE : FW_ENV_FLAG_OBSOLETE;
    }

    result = self->env_stg[env_sel].storage_handler(&self->env_stg[env_sel], FW_ENV_STORAGE_WRITE, self->env_mem, self->env_size);
    if (result != FW_ENV_STORAGE_OK)
    {
      if(DBG_ERR) xfs_printf("Error: can't write environment to location %d\r\n",env_sel);
      error = 1;
    }
  }

  return error;
}


/******************************************************************************/
/*                                                                            */
/* Search the environment for a variable.                                     */
/* Return the value, if found, or NULL, if not found.                         */
/*                                                                            */
/******************************************************************************/

char *fw_getenv (fw_env_type *self, const char *name)
{
  char *env, *nxt;

  for (env = self->data; *env; env = nxt + 1)
  {
    char *val;

    for (nxt = env; *nxt; ++nxt)
    {
      if (nxt >= &self->data[ self->data_size_max])
      {
        if (DBG_ERR) xfs_printf( "## Error: environment not terminated\r\n");
        return NULL;
      }
    }
    val = fw_env_match (name, env);
    if (!val) continue;
    return val;
  }
  return NULL;
}


/******************************************************************************/
/*                                                                            */
/* Print the current definition of one, or more, or all                       */
/* environment variables                                                      */
/*                                                                            */
/******************************************************************************/

/* TBD: use buffer and only one xfs_printf per line */

int fw_printenv(fw_env_type *self, int argc, char *argv[])
{
  char *env, *nxt;
  int rc = -1;
  int opt_q=0;
  int opt_c=0;
  int opt_n=0;
  char* cp;

  while (argc > 1)
  {
    if (strcmp (argv[1], "-q") == 0)
    {
      opt_q = 1;
      ++argv;
      --argc;
    }
    else if (strcmp (argv[1], "-c") == 0)
    {
      opt_c = 1;
      opt_q = 1;
     
      ++argv;
      --argc;
    }
    else if (strcmp (argv[1], "-n") == 0)
    {
      opt_n = 1;
      ++argv;
      --argc;
    }
    else break;
  }

  if ((opt_n) && (argc != 2))
  {
    xfs_printf( "## Error: `-n' option requires exactly one argument\r\n");
    return -1;
  }


  while (argc--)
  {   
    for (env = self->data; *env; env = nxt + 1)
    {
      for (nxt = env; *nxt; ++nxt) 
      {
        if (nxt >= &self->data[ self->data_size_max])
        {
          if (DBG_ERR) xfs_printf( "## Error: environment not terminated\r\n");
          return -1;
        }
      }

      if (!argc || fw_env_match (argv[1], env))
      {
        if (opt_c) xfs_printf("setenv ");

        cp = env;

        if (opt_n)
        {
          /* skip var name */
          while (*cp != '=') cp++;
        }
        else
        {
          /* print var name */
          while (*cp != '=') xfs_printf("%c", *cp++);
        }
        cp++;
        if (opt_c) xfs_printf (" ");
        else if (!opt_n) xfs_printf("=");

        if (opt_q) xfs_printf ("'");
        while (*cp)
        {
          if ((opt_q) && (*cp == '\'')) xfs_printf("'\"%c\"'", *cp++);
          else             xfs_printf("%c", *cp++);
        }
        if (opt_q) xfs_printf ("'");
        xfs_printf ("\r\n");
        rc = 0;
      }
    }
    if (argc <= 1) break;
    argv++;
  }

  return rc;
}


/******************************************************************************/
/*                                                                            */
/* Set/Clear a single variable in the environment.                            */
/* This is called in sequence to update the environment                       */
/* in RAM without updating the copy in flash after each set                   */
/*                                                                            */
/******************************************************************************/

int fw_setenv(fw_env_type *self, int argc, char *argv[])
{
  int len;
  char *env, *nxt;
  char *val = NULL;
  int i;

  /* name  : argv[1]             */
  /* value : argv[2] ... argv[n] */

  if (argc < 2) return -1;

  /* search if variable with this name already exists */
  for (nxt = env = self->data; *env; env = nxt + 1)
  {
    for (nxt = env; *nxt; ++nxt)
    {
      if (nxt >= &self->data[ self->data_size_max])
      {
        if (DBG_ERR) xfs_printf( "## Error: environment not terminated\r\n");
        return -1;
      }
    }
    if ((val = fw_env_match (argv[1], env)) != NULL)  break;
  }

  /* Delete any existing definition */
  if (val)
  {
    if (*++nxt == '\0')
    {
      *env = '\0';
    }
    else
    {
      for (;;)
      {
        *env = *nxt++;
        if ((*env == '\0') && (*nxt == '\0')) break;
        ++env;
      }
    }
    *++env = '\0';
  }

  for (len = 0, i = 2; i < argc; ++i) len += strlen(argv[i]) ;

  /* Delete only ? */
  if ((argc <= 2) || (len == 0) ) return 0;

  /* add name + '=' for first arg, ' ' for all others + \0 \0 */
  len += strlen (argv[1]) + argc;

  /* Append new definition at the end */
  for (env = self->data; *env || *(env + 1); ++env);

  if (env > self->data) ++env;
  /* Overflow when: * "name" + "=" + "val" +"\0\0"  > self->env_size - (env-environment) */

  if (len > (&self->data[ self->data_size_max] - env))
  {
    if (DBG_ERR) xfs_printf("Error: environment overflow, \"%s\" deleted\r\n", argv[1]);
    return -1;
  }

  val = argv[1];
  nxt = env;
  while ((*env = *val++) != '\0') ++env;
  *env = '=';
  
  for (i = 2; i < argc; ++i)
  {
    char *val = argv[i];
    if (i != 2) *++env = ' ';
    while (*val != '\0') *++env = *val++;
  }

  /* end is marked with double '\0' */
  *++env = '\0';
  *++env = '\0';
  
  fw_env_trigger_var(self, nxt);

  return 0;
}

/******************************************************************************/
/*                                                                            */
/* Set/Clear a single variable in the environment.                            */
/* This is called in sequence to update the environment                       */
/* in RAM without updating the copy in flash after each set                   */
/*                                                                            */
/******************************************************************************/

int fw_env_write(fw_env_type *self, char *name, char *value)
{
  int argc = value ? 3 : 2;
  char *argv[3] = {(char*)"", name, value};

  return fw_setenv(self, argc, argv);
}


