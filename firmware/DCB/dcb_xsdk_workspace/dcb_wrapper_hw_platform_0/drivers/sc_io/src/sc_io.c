/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  MEGII - DCB
 *
 *  Author  :  schmid_e (Author of generation script)
 *  Created :  03.05.2019 15:45:07
 *
 *  Description :  Slow Control IO definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */

#include "sc_io.h"

#define NR_OF_MIOS    54
#define NR_OF_EMIOS   64

void emio_init(XGpioPs* self)
{
  /* Configure all EMIO Banks 2 GPIOs as outputs */
  XGpioPs_SetDirection(self, 2, 0xFFFFFFFF);
  /* Configure all EMIO Banks 3 GPIOs as inputs */
  XGpioPs_SetDirection(self, 3, 0x00000000);
}

unsigned int emio_get_pin(XGpioPs* self, unsigned int pin_number)
{
  pin_number = pin_number % NR_OF_EMIOS;
  return XGpioPs_ReadPin(self, NR_OF_MIOS+pin_number);
}

void emio_set_pin(XGpioPs* self, unsigned int pin_number, unsigned int data)
{
  pin_number = pin_number % NR_OF_EMIOS;
  return XGpioPs_WritePin(self, NR_OF_MIOS+pin_number, data);
}

unsigned int emio_mask_read(XGpioPs* self, unsigned char bank, unsigned int mask)
{
  return XGpioPs_Read(self, bank) & mask;
}

void emio_mask_write(XGpioPs* self, unsigned int mask, unsigned int data)
{
  unsigned int reg;

  reg = XGpioPs_Read(self, EMIO_BANK_CTRL);
  reg &= ~mask;
  reg |= (data & mask);
  XGpioPs_Write(self, EMIO_BANK_CTRL, reg);
}

void emio_set_sw_state(XGpioPs* self, unsigned int state)
{
  emio_mask_write(self, (state<<BIT_IDX_EMIO_CTRL_SW_STATE_OFFSET), (state<<BIT_IDX_EMIO_CTRL_SW_STATE_OFFSET) );
}

void emio_clr_sw_state(XGpioPs* self, unsigned int state)
{
  emio_mask_write(self, (state<<BIT_IDX_EMIO_CTRL_SW_STATE_OFFSET), 0 );
}

void emio_flash_sw_state(XGpioPs* self, unsigned int state)
{
  emio_mask_write(self, (state<<BIT_IDX_EMIO_CTRL_SW_STATE_OFFSET), (state<<BIT_IDX_EMIO_CTRL_SW_STATE_OFFSET) );
  emio_mask_write(self, (state<<BIT_IDX_EMIO_CTRL_SW_STATE_OFFSET), 0 );
}

void emio_reset_sw_state(XGpioPs* self)
{
  emio_mask_write(self, BIT_IDX_EMIO_CTRL_SW_STATE_MASK, 0 );
}
