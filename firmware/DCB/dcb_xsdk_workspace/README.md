# General Information

## Maintainer
Elmar Schmid [elmar.schmid@psi.ch]

## Authors
Elmar Schmid [elmar.schmid@psi.ch]

<!--
## Changelog
 See [Changelog](Changelog.md)
-->

## Description
This project contains the software code and XSDK projects for the Data Concentrator Board (DCB) of the MEG project.

<!--
Detailed Documentation can be found here:
[Doc/README.md](Doc/README.md)
-->

<!--
Before checking out, please refer to the [HowTo ...](Doc/HowTo.md) Section of the documentation.
-->

# Dependencies

<!--
## Libraries

All dependencies are included as submodules
-->

## Tools

* SDK 2017.4

# How To ...

## ... Get a working-copy of the project
1. Clone the repository including all submodules to your PC ```git clone git@bitbucket.org:twavedaq/wavedaq.git```
2. Start XSDK
3. Select the workspace:
   Choose [workingCopy]/firmware/DCB/dcb_xsdk_workspace when asked for the XSDK workspace
4. Import the projects:
   Click "Import Project" and browse to the [workingCopy]/firmware/DCB/dcb_xsdk_workspace folder, click OK and Finish (Note: four projects should be imported - app_dcb_ctrl, app_fsbl, dcb_wrapper_bsp and dcp_wrapper_hw_platform_0)
5. Add the custom software repository:
   1. In the menu, select Xilix -> Repositories
   2. At the local repositories, click New...
   3. Select the folder [workingCopy]/firmware/DCB/dcb_xsdk_workspace/dcb_lib and click OK
   4. Click OK
6. Regenerate BSP sources
   Right-click the dcb_wrapper_bsp project and select Re-generate BSP Sources
7. Build the projects app_fsbl and app_dcb_ctrl with right-clicking (on the project) -> Build Project

## ... import the hardware from the Vivado project
1. In the project-explorer, Right-click the dcb_wrapper_hw_platform_0 project and select "Change Hardware Platform Specification"
2. Click Yes
3. Browse to the [workingCopy]/firmware/DCB/dcb_vivado_hw/sdk_export/dcb_wrapper.hdf file
4. Click OK

## ... update the fsbl with the latest hardware changes
1. Build a new FSBL:
   1. In the menu, select File -> New -> Application Project
   2. Project name: fsbl_new
   3. Check the Hardware Platform: dcb_wrapper_hw_platform_0
   4. Board Support Package: dcb_wrapper_bsp
   5. Click Next>
   6. Choose Zynq FSBL from the Available Templates
   7. Click Finish
   8. Use a diff tool (e.g. WinMerge) to merge the hardware changes in the new FSBL to the existing FSBL

## ... create a boot image
1. In the menu, select Xilinx -> Create Boot Image
2. Select Import from existing BIF file
3. In the platform project folder, select the .bif file according to your needs. One of them is referencing the .bit in the Vivado project the other one the .bit in the XSDK platform project
4. Adapt the paths to your workspace for each file ( unfortunately this cannot be done with relative paths :-( )
5. Click Create Image to finish

## ... program the flash
1. In the menu, select Xilinx -> Program Flash
2. Set the appropriate settings for the hardware platform and the connection
3. For the Image File, select BOOT.bin in the platform project folder
4. Click Program
