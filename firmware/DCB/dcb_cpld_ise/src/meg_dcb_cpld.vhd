-- ################################################################################
--  FPGA CPLD CONFIG FILE
-- ################################################################################
--  Author    : Elmar Schmid
--              (translation of schematic design from Ueli Hartmann (HU32) to vhdl)
--  Created   : March 2019
-- ################################################################################
-- ############################################
--  Define Device, Package, And Speed Grade ###
-- ############################################
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity meg_dcb_cpld is
    Port (
      -- Control from Backplane
      BOARD_SELECT_N_I      : in  std_logic;
      FLASH_SELECT_INIT_N_I : in  std_logic; -- origianlly INIT_N_I
      EXT_SPI_CS_N_I        : in  std_logic; -- originally FLASH_SELECT_N_I

      -- Resets
      RESET_FPGA_N_I        : in  std_logic;
      RESET_RC_FB_O         : out std_logic;
      RESET_RC_FB_I         : in  std_logic;
      RESET_B_N_O           : out std_logic;
      PS_RST_N_O            : out std_logic;

      EXT_SPI_CLK_I             : in  std_logic;

      --SPI Flash interface
      FLASH_SCK_O           : out std_logic;
      FLASH_CS_N_O          : out std_logic;

      -- SPI FPGA IO
      LOC_SPI_MISO_I            : in  std_logic;
      LOC_SPI_MISO_EN_N_I           : in  std_logic;

      -- SPI FPGA MIO
      FLASH_MISO_I          : in  std_logic; -- also to Flash
      LOC_SPI_CLK_I          : in  std_logic;
      LOC_SPI_CS_N_I        : in  std_logic;

      -- SPI Backplane
      EXT_SPI_MISO_O        : out std_logic;
      SPI_MISO_DRV_EN_O     : out std_logic;

      -- Flash Select
      EXT_FLASH_ACCESS_N_O  : out std_logic;

      -- Configuration Mode Select
      MODE_SELECT_I         : in  std_logic_vector(1 downto 0);
      MODE_O                : out std_logic_vector(4 downto 0);

      -- status if between CPLD and FPGA
      FPGA_CPLD_CLK_I       : in  std_logic;
      FPGA_CPLD_CE_I        : in  std_logic;
      CPLD_FPGA_DATA_O      : out std_logic;

      -- FTDI spares
      ADBUS4_I              : in  std_logic;
      ACBUS4_I              : in  std_logic;
      ACBUS5_I              : in  std_logic
    );
end meg_dcb_cpld;



architecture Behavioral of meg_dcb_cpld is

  constant C_CFG_JTAG   : std_logic_vector(4 downto 0) := "00000";
  constant C_CFG_QSPI   : std_logic_vector(4 downto 0) := "01000";
  constant C_CFG_SD     : std_logic_vector(4 downto 0) := "01100";

  signal ext_flash_access_n : std_logic;
  signal ext_flash_cs_n     : std_logic;
  signal local_reset        : std_logic;
  signal spi_miso_drv_en    : std_logic;

begin

  -- TBD: status if between CPLD and FPGA
  CPLD_FPGA_DATA_O     <= '0';

  ext_flash_access_n   <= BOARD_SELECT_N_I or  FLASH_SELECT_INIT_N_I;
  ext_flash_cs_n       <= BOARD_SELECT_N_I or  EXT_SPI_CS_N_I;
  spi_miso_drv_en      <= BOARD_SELECT_N_I nor LOC_SPI_MISO_EN_N_I;

  EXT_FLASH_ACCESS_N_O <= ext_flash_access_n;
  FLASH_CS_N_O         <= ext_flash_cs_n when ext_flash_access_n = '0' else LOC_SPI_CS_N_I;
  FLASH_SCK_O          <= EXT_SPI_CLK_I  when ext_flash_access_n = '0' else LOC_SPI_CLK_I;
  EXT_SPI_MISO_O       <= FLASH_MISO_I   when ext_flash_access_n = '0' else LOC_SPI_MISO_I;
  SPI_MISO_DRV_EN_O    <= '1'            when ext_flash_access_n = '0' else spi_miso_drv_en;

  PS_RST_N_O    <= not local_reset;
  RESET_RC_FB_O <= ext_flash_access_n and local_reset;
  RESET_B_N_O   <= not local_reset;

  process(RESET_FPGA_N_I, RESET_RC_FB_I)
  begin
    if RESET_RC_FB_I = '1' then
      local_reset <= '0';
    elsif falling_edge(RESET_FPGA_N_I) then
      local_reset <= '1';
    end if;
  end process;

  process(MODE_SELECT_I)
  begin
    case MODE_SELECT_I is
      when "00"   => MODE_O <= C_CFG_JTAG;
      when "10"   => MODE_O <= C_CFG_SD;
      when others => MODE_O <= C_CFG_QSPI;
    end case;
  end process;

end Behavioral;
