#
# Vivado pre-synthesis script
#
puts ">>> Executing pre-synthesis steps:"
add_files -norecurse ./../../dcb.srcs/sources_1/bd/dcb/dcb.bd
open_bd_design ./../../dcb.srcs/sources_1/bd/dcb/dcb.bd
write_bd_tcl -force -no_ip_version ./../../../bd_new.tcl
close_bd_design [get_bd_designs dcb]
write_project_tcl -force ./../../../project_new.tcl
puts "End of pre-synthesis steps <<<\n\n"
