---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Two Shot with clock domain crossing
--
--  Project :  MEG DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ XC7Z030-1FBG676C
--
--  Tool Version :  Vivado 2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  19.12.2018 16:38:00
--
--  Description :  Two shot generator with clock domain crossing. The output pulse
--                 width is a minimum of two cycles of the output clock.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity util_double_shot_cc is
  generic (
    CGN_NR_OF_BITS  : integer := 1
  );
  port (
    IN_I      : in  std_logic_vector(CGN_NR_OF_BITS-1 downto 0);
    OUT_O     : out std_logic_vector(CGN_NR_OF_BITS-1 downto 0);
    CLK_OUT_I : in  std_logic
  );
end util_double_shot_cc;

architecture behavioral of util_double_shot_cc is

	signal sync_reg0    : std_logic_vector(CGN_NR_OF_BITS-1 downto 0)	:= (others => '0');
	signal sync_reg1    : std_logic_vector(CGN_NR_OF_BITS-1 downto 0)	:= (others => '0');
	signal edge_reg     : std_logic_vector(CGN_NR_OF_BITS-1 downto 0)	:= (others => '0');
	signal two_shot_reg : std_logic_vector(CGN_NR_OF_BITS-1 downto 0)	:= (others => '0');
	signal out_reg      : std_logic_vector(CGN_NR_OF_BITS-1 downto 0)	:= (others => '0');
	
	attribute syn_srlstyle : string;
    attribute syn_srlstyle of sync_reg0 	: signal is "registers";
    attribute syn_srlstyle of sync_reg1 	: signal is "registers";
	
	attribute shreg_extract : string;
    attribute shreg_extract of sync_reg0 	: signal is "no";
    attribute shreg_extract of sync_reg1 	: signal is "no";
	
	attribute ASYNC_REG : string;
    attribute ASYNC_REG of sync_reg0 		: signal is "TRUE";
    attribute ASYNC_REG of sync_reg1 		: signal is "TRUE";

	attribute IOB : string;
    attribute IOB of out_reg : signal is "TRUE"; 
  
begin

	process(CLK_OUT_I)
	begin
		if rising_edge(CLK_OUT_I) then
			sync_reg0    <= IN_I;
			sync_reg1    <= sync_reg0;
      edge_reg     <= sync_reg1;
      two_shot_reg <= sync_reg1 and not edge_reg;
      out_reg      <= two_shot_reg or (sync_reg1 and not edge_reg);
		end if;
	end process;
	OUT_O <= out_reg;

end architecture behavioral;
