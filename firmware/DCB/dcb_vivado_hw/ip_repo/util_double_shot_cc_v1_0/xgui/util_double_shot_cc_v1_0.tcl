# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "CGN_NR_OF_BITS" -parent ${Page_0}


}

proc update_PARAM_VALUE.CGN_NR_OF_BITS { PARAM_VALUE.CGN_NR_OF_BITS } {
	# Procedure called to update CGN_NR_OF_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_NR_OF_BITS { PARAM_VALUE.CGN_NR_OF_BITS } {
	# Procedure called to validate CGN_NR_OF_BITS
	return true
}


proc update_MODELPARAM_VALUE.CGN_NR_OF_BITS { MODELPARAM_VALUE.CGN_NR_OF_BITS PARAM_VALUE.CGN_NR_OF_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_NR_OF_BITS}] ${MODELPARAM_VALUE.CGN_NR_OF_BITS}
}

