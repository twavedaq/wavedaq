---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Trigger Manager
--
--  Project :  MEG DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ XC7Z030-1FBG676C
--
--  Tool Version :  Vivado 2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  11.01.2019 12:30:00
--
--  Description :  Receives trigger pulse inputs from cable and registers and drives
--                 them to the backplane.
--                 Receives serial trigger information and drives it to the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity trigger_manager is
  generic (
    CGN_NR_OF_BITS         : integer := 48;
    CGN_OVERSAMPLNG_FACTOR : integer := 4;
    CGN_PARITY             : integer := 1;  -- 0=No Parity, 1=Even Parity, 2=Odd Parity
    CGN_PERR_COUNT_WIDTH   : integer := 16; --Parity error counter width [bits]"
    CGN_DIRECTION          : integer := 1   -- Specifies data direction 0="LSB first" or 1="MSB first"
  );
  port (
    -- Front Panel MCX
    TRIGGER_MCX_I     : in  std_logic;
    TRIGGER_MCX_O     : out std_logic;
    -- Front Panel Cable
    TRIGGER_FCI_P_I   : in  std_logic;
    TRIGGER_FCI_N_I   : in  std_logic;
    TRIGGER_SDATA_P_I : in  std_logic;
    TRIGGER_SDATA_N_I : in  std_logic;
    -- Backplane
    TRIGGER_BPL_O     : out std_logic;
    TRIGGER_SDATA_O   : out std_logic;
    -- PS signals
    TRIGGER_PS_I      : in  std_logic;
    -- Internal
    TRIGGER_DCB_O     : out std_logic;
    TRIGGER_PDATA_O   : out std_logic_vector(CGN_NR_OF_BITS-1 downto 0);
    VALID_O           : out std_logic;
    VALID_RST_I       : in  std_logic;
    PERR_O            : out std_logic;
    PERR_COUNT_O      : out std_logic_vector(CGN_PERR_COUNT_WIDTH-1 downto 0);
    PERR_COUNT_RST_I  : in  std_logic;
    RST_I             : in  std_logic;
    WDB_CLK_I         : in  std_logic;
    PS_CLK_I          : in  std_logic
  );
end trigger_manager;

architecture behavioral of trigger_manager is

  signal trig_ps_sync0    : std_logic := '0';
  signal trig_ps_sync1    : std_logic := '0';
  signal trig_ps_edge     : std_logic := '0';
  signal trig_ps_two_shot : std_logic := '0';
  signal trig_ps_out_reg  : std_logic := '0';

  signal trigger_fci      : std_logic := '0';
  signal trig_ext         : std_logic := '0';
  signal trig_dcb_sync0   : std_logic := '0';
  signal trig_dcb_sync1   : std_logic := '0';
  signal trigger          : std_logic := '0';

  signal trigger_sdata    : std_logic := '0';
  signal trig_sdata_sync0 : std_logic := '0';
  signal trig_sdata_sync1 : std_logic := '0';
  signal trig_sdata_out   : std_logic := '0';

  attribute syn_srlstyle : string;
    attribute syn_srlstyle of trig_ps_sync0     : signal is "registers";
    attribute syn_srlstyle of trig_ps_sync1     : signal is "registers";
    attribute syn_srlstyle of trig_dcb_sync0    : signal is "registers";
    attribute syn_srlstyle of trig_dcb_sync1    : signal is "registers";
    attribute syn_srlstyle of trig_sdata_sync0  : signal is "registers";
    attribute syn_srlstyle of trig_sdata_sync1  : signal is "registers";

  attribute shreg_extract : string;
    attribute shreg_extract of trig_ps_sync0    : signal is "no";
    attribute shreg_extract of trig_ps_sync1    : signal is "no";
    attribute shreg_extract of trig_dcb_sync0   : signal is "no";
    attribute shreg_extract of trig_dcb_sync1   : signal is "no";
    attribute shreg_extract of trig_sdata_sync0 : signal is "no";
    attribute shreg_extract of trig_sdata_sync1 : signal is "no";

  attribute ASYNC_REG : string;
    attribute ASYNC_REG of trig_ps_sync0        : signal is "TRUE";
    attribute ASYNC_REG of trig_ps_sync1        : signal is "TRUE";
    attribute ASYNC_REG of trig_dcb_sync0       : signal is "TRUE";
    attribute ASYNC_REG of trig_dcb_sync1       : signal is "TRUE";
    attribute ASYNC_REG of trig_sdata_sync0     : signal is "TRUE";
    attribute ASYNC_REG of trig_sdata_sync1     : signal is "TRUE";

	attribute IOB : string;
    attribute IOB of trig_sdata_out : signal is "TRUE";

begin

  IBUFDS_TRIGGER_inst : IBUFDS
  generic map (
    DIFF_TERM    => FALSE, -- Differential Termination
    IBUF_LOW_PWR => TRUE,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
    IOSTANDARD   => "DEFAULT")
  port map (
    O  => trigger_fci,   -- Buffer output
    I  => TRIGGER_FCI_P_I, -- Diff_p buffer input (connect directly to top-level port)
    IB => TRIGGER_FCI_N_I  -- Diff_n buffer input (connect directly to top-level port)
  );

  IBUFDS_SDATA_inst : IBUFDS
  generic map (
    DIFF_TERM    => FALSE, -- Differential Termination
    IBUF_LOW_PWR => TRUE,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
    IOSTANDARD   => "DEFAULT")
  port map (
    O  => trigger_sdata,   -- Buffer output
    I  => TRIGGER_SDATA_P_I, -- Diff_p buffer input (connect directly to top-level port)
    IB => TRIGGER_SDATA_N_I  -- Diff_n buffer input (connect directly to top-level port)
  );

  -- trigger routing
  trig_ext      <= trigger_fci or TRIGGER_MCX_I;
  trigger       <= trig_ext or trig_ps_out_reg;
  TRIGGER_BPL_O <= trigger;
  TRIGGER_MCX_O <= trigger;
  TRIGGER_DCB_O <= trig_dcb_sync1;

  -- serial trigger info routing
  TRIGGER_SDATA_O <= trig_sdata_out;
  TRIGGER_PDATA_O <= (others=>'0');
  VALID_O         <= '0';
  PERR_O          <= '0';
  PERR_COUNT_O    <= (others=>'0');

  process(WDB_CLK_I)
  begin
    if rising_edge(WDB_CLK_I) then
      trig_ps_sync0    <= TRIGGER_PS_I;
      trig_ps_sync1    <= trig_ps_sync0;
      trig_ps_edge     <= trig_ps_sync1;
      trig_ps_two_shot <= trig_ps_sync1 and not trig_ps_edge;
      trig_ps_out_reg  <= trig_ps_two_shot or (trig_ps_sync1 and not trig_ps_edge);
    end if;
  end process;

  process(WDB_CLK_I)
  begin
    if rising_edge(WDB_CLK_I) then
      trig_dcb_sync0 <= trigger;
      trig_dcb_sync1 <= trig_dcb_sync0;
    end if;
  end process;

  process(WDB_CLK_I)
  begin
    if rising_edge(WDB_CLK_I) then
      trig_sdata_sync0 <= trigger_sdata;
      trig_sdata_sync1 <= trig_sdata_sync0;
      trig_sdata_out   <= trig_sdata_sync1;
    end if;
  end process;

end architecture behavioral;
