# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "CGN_DIRECTION" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_NR_OF_BITS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_OVERSAMPLNG_FACTOR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_PARITY" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_PERR_COUNT_WIDTH" -parent ${Page_0}


}

proc update_PARAM_VALUE.CGN_DIRECTION { PARAM_VALUE.CGN_DIRECTION } {
	# Procedure called to update CGN_DIRECTION when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_DIRECTION { PARAM_VALUE.CGN_DIRECTION } {
	# Procedure called to validate CGN_DIRECTION
	return true
}

proc update_PARAM_VALUE.CGN_NR_OF_BITS { PARAM_VALUE.CGN_NR_OF_BITS } {
	# Procedure called to update CGN_NR_OF_BITS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_NR_OF_BITS { PARAM_VALUE.CGN_NR_OF_BITS } {
	# Procedure called to validate CGN_NR_OF_BITS
	return true
}

proc update_PARAM_VALUE.CGN_OVERSAMPLNG_FACTOR { PARAM_VALUE.CGN_OVERSAMPLNG_FACTOR } {
	# Procedure called to update CGN_OVERSAMPLNG_FACTOR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_OVERSAMPLNG_FACTOR { PARAM_VALUE.CGN_OVERSAMPLNG_FACTOR } {
	# Procedure called to validate CGN_OVERSAMPLNG_FACTOR
	return true
}

proc update_PARAM_VALUE.CGN_PARITY { PARAM_VALUE.CGN_PARITY } {
	# Procedure called to update CGN_PARITY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_PARITY { PARAM_VALUE.CGN_PARITY } {
	# Procedure called to validate CGN_PARITY
	return true
}

proc update_PARAM_VALUE.CGN_PERR_COUNT_WIDTH { PARAM_VALUE.CGN_PERR_COUNT_WIDTH } {
	# Procedure called to update CGN_PERR_COUNT_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_PERR_COUNT_WIDTH { PARAM_VALUE.CGN_PERR_COUNT_WIDTH } {
	# Procedure called to validate CGN_PERR_COUNT_WIDTH
	return true
}


proc update_MODELPARAM_VALUE.CGN_NR_OF_BITS { MODELPARAM_VALUE.CGN_NR_OF_BITS PARAM_VALUE.CGN_NR_OF_BITS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_NR_OF_BITS}] ${MODELPARAM_VALUE.CGN_NR_OF_BITS}
}

proc update_MODELPARAM_VALUE.CGN_OVERSAMPLNG_FACTOR { MODELPARAM_VALUE.CGN_OVERSAMPLNG_FACTOR PARAM_VALUE.CGN_OVERSAMPLNG_FACTOR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_OVERSAMPLNG_FACTOR}] ${MODELPARAM_VALUE.CGN_OVERSAMPLNG_FACTOR}
}

proc update_MODELPARAM_VALUE.CGN_PARITY { MODELPARAM_VALUE.CGN_PARITY PARAM_VALUE.CGN_PARITY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_PARITY}] ${MODELPARAM_VALUE.CGN_PARITY}
}

proc update_MODELPARAM_VALUE.CGN_PERR_COUNT_WIDTH { MODELPARAM_VALUE.CGN_PERR_COUNT_WIDTH PARAM_VALUE.CGN_PERR_COUNT_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_PERR_COUNT_WIDTH}] ${MODELPARAM_VALUE.CGN_PERR_COUNT_WIDTH}
}

proc update_MODELPARAM_VALUE.CGN_DIRECTION { MODELPARAM_VALUE.CGN_DIRECTION PARAM_VALUE.CGN_DIRECTION } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_DIRECTION}] ${MODELPARAM_VALUE.CGN_DIRECTION}
}

