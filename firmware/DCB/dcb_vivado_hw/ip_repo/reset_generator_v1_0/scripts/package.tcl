###############################################################
# Include PSI packaging commands
###############################################################
source ../../../EE_Projects/psi_fpga_all/TCL/PsiIpPackage/PsiIpPackage.tcl
#namespace import psi::ip_package::2017_4_1::*
namespace import -force psi::ip_package::latest::*

###############################################################
# General Information
###############################################################
set IP_NAME reset_generator
set IP_VERSION 1.0
set IP_REVISION "auto"
set IP_LIBRARY PSI
set IP_DESCIRPTION "Generate resets for all necessary clock domains in the MEG WAVEDAQ DCB project"

init $IP_NAME $IP_VERSION $IP_REVISION $IP_LIBRARY
set_description $IP_DESCIRPTION
set_logo_relative "../doc/psi_logo_150.gif"
set_datasheet_relative "../doc/$IP_NAME.pdf"

###############################################################
# Add Source Files
###############################################################

#Relative Source Files
add_sources_relative { \
  ../hdl/reset_generator.vhd \
}

#Relative Library Files
#add_lib_relative \
#  "../../.."	\
#  { \
#    VHDL/psi_common/hdl/psi_common_math_pkg.vhd \
#  }

###############################################################
# Driver Files
###############################################################

#add_drivers_relative ../drivers/clock_measure { \
#  src/reset_generator.c \
#  src/reset_generator.h \
#}


###############################################################
# GUI Parameters
###############################################################

#This component has a standard AXI slave port
has_std_axi_if true

#User Parameters
gui_add_page "Configuration"

gui_create_parameter "CGN_INPUT_RESET_POLARITY" "Input reset polarity (0 = active low, 1 = active high)"
gui_parameter_set_range 0 1
gui_add_parameter

gui_create_parameter "CGN_INPUT_CLK_PULSE_WIDTH" "Minimum pulse width of the asynchronous input clock"
gui_add_parameter

gui_create_parameter "CGN_NR_OF_BUS_RST" "Width of the ACTIVE HIGH bus reset output"
gui_add_parameter

gui_create_parameter "CGN_NR_OF_BUS_RST_N" "Width of the ACTIVE LOW bus reset output"
gui_add_parameter

gui_create_parameter "CGN_NR_OF_PERIPHERAL_RST" "Width of the ACTIVE HIGH peripheral reset output"
gui_add_parameter

gui_create_parameter "CGN_NR_OF_PERIPHERAL_RST_N" "Width of the ACTIVE LOW peripheral reset output"
gui_add_parameter

###############################################################
# Optional Ports
###############################################################

#None

###############################################################
# Package Core
###############################################################
set TargetDir ".."
#											Edit  	Synth
package_ip $TargetDir 						false 	true
