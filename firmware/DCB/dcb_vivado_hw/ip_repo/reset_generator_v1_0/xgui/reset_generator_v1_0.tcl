# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "CGN_INPUT_CLK_PULSE_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_INPUT_RESET_POLARITY" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_NR_OF_BUS_RST" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_NR_OF_BUS_RST_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_NR_OF_PERIPHERAL_RST" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_NR_OF_PERIPHERAL_RST_N" -parent ${Page_0}


}

proc update_PARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH { PARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH } {
	# Procedure called to update CGN_INPUT_CLK_PULSE_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH { PARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH } {
	# Procedure called to validate CGN_INPUT_CLK_PULSE_WIDTH
	return true
}

proc update_PARAM_VALUE.CGN_INPUT_RESET_POLARITY { PARAM_VALUE.CGN_INPUT_RESET_POLARITY } {
	# Procedure called to update CGN_INPUT_RESET_POLARITY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_INPUT_RESET_POLARITY { PARAM_VALUE.CGN_INPUT_RESET_POLARITY } {
	# Procedure called to validate CGN_INPUT_RESET_POLARITY
	return true
}

proc update_PARAM_VALUE.CGN_NR_OF_BUS_RST { PARAM_VALUE.CGN_NR_OF_BUS_RST } {
	# Procedure called to update CGN_NR_OF_BUS_RST when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_NR_OF_BUS_RST { PARAM_VALUE.CGN_NR_OF_BUS_RST } {
	# Procedure called to validate CGN_NR_OF_BUS_RST
	return true
}

proc update_PARAM_VALUE.CGN_NR_OF_BUS_RST_N { PARAM_VALUE.CGN_NR_OF_BUS_RST_N } {
	# Procedure called to update CGN_NR_OF_BUS_RST_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_NR_OF_BUS_RST_N { PARAM_VALUE.CGN_NR_OF_BUS_RST_N } {
	# Procedure called to validate CGN_NR_OF_BUS_RST_N
	return true
}

proc update_PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST { PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST } {
	# Procedure called to update CGN_NR_OF_PERIPHERAL_RST when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST { PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST } {
	# Procedure called to validate CGN_NR_OF_PERIPHERAL_RST
	return true
}

proc update_PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N { PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N } {
	# Procedure called to update CGN_NR_OF_PERIPHERAL_RST_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N { PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N } {
	# Procedure called to validate CGN_NR_OF_PERIPHERAL_RST_N
	return true
}


proc update_MODELPARAM_VALUE.CGN_INPUT_RESET_POLARITY { MODELPARAM_VALUE.CGN_INPUT_RESET_POLARITY PARAM_VALUE.CGN_INPUT_RESET_POLARITY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_INPUT_RESET_POLARITY}] ${MODELPARAM_VALUE.CGN_INPUT_RESET_POLARITY}
}

proc update_MODELPARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH { MODELPARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH PARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH}] ${MODELPARAM_VALUE.CGN_INPUT_CLK_PULSE_WIDTH}
}

proc update_MODELPARAM_VALUE.CGN_NR_OF_BUS_RST { MODELPARAM_VALUE.CGN_NR_OF_BUS_RST PARAM_VALUE.CGN_NR_OF_BUS_RST } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_NR_OF_BUS_RST}] ${MODELPARAM_VALUE.CGN_NR_OF_BUS_RST}
}

proc update_MODELPARAM_VALUE.CGN_NR_OF_BUS_RST_N { MODELPARAM_VALUE.CGN_NR_OF_BUS_RST_N PARAM_VALUE.CGN_NR_OF_BUS_RST_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_NR_OF_BUS_RST_N}] ${MODELPARAM_VALUE.CGN_NR_OF_BUS_RST_N}
}

proc update_MODELPARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST { MODELPARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST}] ${MODELPARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST}
}

proc update_MODELPARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N { MODELPARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N}] ${MODELPARAM_VALUE.CGN_NR_OF_PERIPHERAL_RST_N}
}

