---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Reset generator
--
--  Project :  MEG
--
--  PCB  :  Data Concentrator Board
--  Part :  Xilinx Zynq XC7Z030-1FBG676C
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  12.04.2019 09:07:56
--
--  Description :  Generates resets triggered by an asynchronous reset input. The
--                 output resets are synchronized to the corresponding clock domain.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

Library UNISIM;
use UNISIM.vcomponents.all;

--- vvv Start of Testbench Generator Configuration vvv ---
--- See TbGenerator documentation for info on global
--- settings like processes, testcases, dutlib and tbpkg
--- ^^^  End of Testbench Generator Configuration  ^^^ ---

entity reset_generator is
  generic (
    CGN_INPUT_RESET_POLARITY   : integer := 1; -- 0 = active low, 1 = active high $$ constant=1 $$
    CGN_INPUT_CLK_PULSE_WIDTH  : integer := 4; -- $$ constant=4 $$
    CGN_NR_OF_BUS_RST          : integer := 1; -- $$ constant=1 $$
    CGN_NR_OF_BUS_RST_N        : integer := 1; -- $$ constant=1 $$
    CGN_NR_OF_PERIPHERAL_RST   : integer := 1; -- $$ constant=1 $$
    CGN_NR_OF_PERIPHERAL_RST_N : integer := 1  -- $$ constant=1 $$
  );
  port (
    RST_I                  : in  std_logic;
    REF_CLK_I              : in  std_logic; -- $$ type=clk; freq=50e6 $$
    BUS_RST_O              : out std_logic_vector(CGN_NR_OF_BUS_RST-1 downto 0);
    BUS_RST_CLK_I          : in  std_logic_vector(CGN_NR_OF_BUS_RST-1 downto 0);           -- $$ type=clk; freq=133e6 $$
    BUS_RST_N_O            : out std_logic_vector(CGN_NR_OF_BUS_RST_N-1 downto 0);         -- $$ lowactive=true $$
    BUS_RST_N_CLK_I        : in  std_logic_vector(CGN_NR_OF_BUS_RST_N-1 downto 0);         -- $$ type=clk; freq=133e6 $$
    PERIPHERAL_RST_O       : out std_logic_vector(CGN_NR_OF_PERIPHERAL_RST-1 downto 0);
    PERIPHERAL_RST_CLK_I   : in  std_logic_vector(CGN_NR_OF_PERIPHERAL_RST-1 downto 0);    -- $$ type=clk; freq=150e6 $$
    PERIPHERAL_RST_N_O     : out std_logic_vector(CGN_NR_OF_PERIPHERAL_RST_N-1 downto 0);  -- $$ lowactive=true $$
    PERIPHERAL_RST_N_CLK_I : in  std_logic_vector(CGN_NR_OF_PERIPHERAL_RST_N-1 downto 0)   -- $$ type=clk; freq=150e6 $$
  );
end reset_generator;

architecture behavioral of reset_generator is

  constant C_IN_WIDTH_COUNT_SIZE  : integer := 2;
  constant C_OUT_WIDTH_COUNT_SIZE : integer := 4;

  signal input_rst       : std_logic;
  signal rst             : std_logic := '0';
  signal bus_rst         : std_logic := '0';
  signal peripheral_rst  : std_logic := '0';
  signal in_width_count  : std_logic_vector(C_IN_WIDTH_COUNT_SIZE-1  downto 0) := (others=>'0');
  signal out_width_count : std_logic_vector(C_OUT_WIDTH_COUNT_SIZE-1 downto 0) := (others=>'0');

  subtype sync_type is std_logic_vector(1 downto 0);
  type sync_array_type is array (integer range <>) of sync_type;
  signal in_rst_sync : sync_type := (others=>'0');
  signal bus_rst_sync          : sync_array_type(CGN_NR_OF_BUS_RST-1 downto 0)          := (others=>(others=>'0'));
  signal bus_rst_n_sync        : sync_array_type(CGN_NR_OF_BUS_RST_N-1 downto 0)        := (others=>(others=>'1'));
  signal peripheral_rst_sync   : sync_array_type(CGN_NR_OF_PERIPHERAL_RST-1 downto 0)   := (others=>(others=>'0'));
  signal peripheral_rst_n_sync : sync_array_type(CGN_NR_OF_PERIPHERAL_RST_N-1 downto 0) := (others=>(others=>'1'));

begin

  active_high_input : if CGN_INPUT_RESET_POLARITY = 1 generate
    input_rst <= RST_I;
  end generate;
  active_low_input : if CGN_INPUT_RESET_POLARITY = 0 generate
    input_rst <= not RST_I;
  end generate;

  -- Capture asynchronous input reset
  process(input_rst, REF_CLK_I)
  begin
    if input_rst = '1' then
      in_width_count <= (others=>'1');
      rst <= '1';
    elsif rising_edge(REF_CLK_I) then
      if in_width_count = 0 then
        rst <= '0';
      else
        rst <= '1';
        in_width_count <= in_width_count - 1;
      end if;
    end if;
  end process;

  -- Generate resets
  process(REF_CLK_I)
  begin
    if rising_edge(REF_CLK_I) then
      if rst = '1' then
        out_width_count <= (others=>'1');
        bus_rst         <= '1';
        peripheral_rst  <= '1';
      elsif out_width_count = 0 then
        if bus_rst = '1' then
          out_width_count <= (others=>'1');
          bus_rst         <= '0';
          peripheral_rst  <= '1';
        else
          bus_rst         <= '0';
          peripheral_rst  <= '0';
        end if;
      else
        out_width_count <= out_width_count - 1;
      end if;
    end if;
  end process;

  -- Synchronize resets to clock domains
  bus_rst_output_synchronization : for i in CGN_NR_OF_BUS_RST-1 downto 0 generate
    process(BUS_RST_CLK_I(i))
    begin
      if rising_edge(BUS_RST_CLK_I(i)) then
        bus_rst_sync(i) <= bus_rst_sync(i)(0) & bus_rst;
      end if;
    end process;
    BUS_RST_O(i) <= bus_rst_sync(i)(1);
  end generate bus_rst_output_synchronization;

  bus_rst_n_output_synchronization : for i in CGN_NR_OF_BUS_RST_N-1 downto 0 generate
    process(BUS_RST_N_CLK_I(i))
    begin
      if rising_edge(BUS_RST_N_CLK_I(i)) then
        bus_rst_n_sync(i) <= bus_rst_n_sync(i)(0) & not(bus_rst);
      end if;
    end process;
    BUS_RST_N_O(i) <= bus_rst_n_sync(i)(1);
  end generate bus_rst_n_output_synchronization;

  peripheral_rst_output_synchronization : for i in CGN_NR_OF_PERIPHERAL_RST-1 downto 0 generate
    process(PERIPHERAL_RST_CLK_I(i))
    begin
      if rising_edge(PERIPHERAL_RST_CLK_I(i)) then
        peripheral_rst_sync(i) <= peripheral_rst_sync(i)(0) & peripheral_rst;
      end if;
    end process;
    PERIPHERAL_RST_O(i) <= peripheral_rst_sync(i)(1);
  end generate peripheral_rst_output_synchronization;

  peripheral_rst_n_output_synchronization : for i in CGN_NR_OF_PERIPHERAL_RST_N-1 downto 0 generate
    process(PERIPHERAL_RST_N_CLK_I(i))
    begin
      if rising_edge(PERIPHERAL_RST_N_CLK_I(i)) then
        peripheral_rst_n_sync(i) <= peripheral_rst_n_sync(i)(0) & not(peripheral_rst);
      end if;
    end process;
    PERIPHERAL_RST_N_O(i) <= peripheral_rst_n_sync(i)(1);
  end generate peripheral_rst_n_output_synchronization;

end architecture behavioral;