#Constants
#set LibPath "../../.."
set LibPath "~/EE_Projects/psi_fpga_all"

namespace import psi::sim::*

#Set library
psi::sim::add_library reset_generator

#suppress messages
psi::sim::compile_suppress 135,1236
psi::sim::run_suppress 8684,3479,3813,8009,3812

# # libraries
# psi::sim::add_sources "$LibPath/VHDL/psi_common/hdl" {
#     psi_common_array_pkg.vhd \
# 	psi_common_math_pkg.vhd \
# } -tag lib
#
# # axi_slave_ipif
# psi::sim::add_sources "$LibPath/VivadoIp/axi_slave_ipif_package/hdl" {
# 	axi_slave_ipif_package.vhd \
# } -tag lib
#
# # psi_tb_v1_0
# psi::sim::add_sources "$LibPath/VHDL/psi_tb/hdl" {
# 	psi_tb_txt_util.vhd \
# 	psi_tb_axi_pkg.vhd \
# } -tag lib

# project sources
psi::sim::add_sources "../hdl" {
	reset_generator.vhd \
} -tag src

# testbenches
psi::sim::add_sources "../tb" {
	reset_generator_tb.vhd \
} -tag tb

#TB Runs
psi::sim::create_tb_run "reset_generator_tb"
psi::sim::add_tb_run

#psi::sim::create_tb_run "top_tb"
#psi::sim::add_tb_run
