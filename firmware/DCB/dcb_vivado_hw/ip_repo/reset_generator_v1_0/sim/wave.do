onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /reset_generator_tb/AllProcessesDone_c
add wave -noupdate /reset_generator_tb/ProcessDone
add wave -noupdate /reset_generator_tb/REF_CLK_I
add wave -noupdate /reset_generator_tb/RST_I
add wave -noupdate /reset_generator_tb/BUS_RST_CLK_I(0)
add wave -noupdate /reset_generator_tb/BUS_RST_O
add wave -noupdate /reset_generator_tb/BUS_RST_N_CLK_I(0)
add wave -noupdate /reset_generator_tb/BUS_RST_N_O
add wave -noupdate /reset_generator_tb/PERIPHERAL_RST_CLK_I(0)
add wave -noupdate /reset_generator_tb/PERIPHERAL_RST_O
add wave -noupdate /reset_generator_tb/PERIPHERAL_RST_N_CLK_I(0)
add wave -noupdate /reset_generator_tb/PERIPHERAL_RST_N_O
add wave -noupdate -divider Internal
add wave -noupdate /reset_generator_tb/REF_CLK_I
add wave -noupdate /reset_generator_tb/i_dut/input_rst
add wave -noupdate /reset_generator_tb/i_dut/in_width_count
add wave -noupdate -radix unsigned /reset_generator_tb/i_dut/out_width_count
add wave -noupdate /reset_generator_tb/i_dut/rst
add wave -noupdate /reset_generator_tb/i_dut/bus_rst
add wave -noupdate /reset_generator_tb/i_dut/peripheral_rst
add wave -noupdate -divider <NULL>
add wave -noupdate /reset_generator_tb/i_dut/bus_rst
add wave -noupdate /reset_generator_tb/i_dut/BUS_RST_CLK_I(0)
add wave -noupdate /reset_generator_tb/i_dut/bus_rst_sync
add wave -noupdate /reset_generator_tb/i_dut/BUS_RST_O(0)
add wave -noupdate -divider <NULL>
add wave -noupdate /reset_generator_tb/i_dut/bus_rst
add wave -noupdate /reset_generator_tb/i_dut/BUS_RST_N_CLK_I(0)
add wave -noupdate /reset_generator_tb/i_dut/bus_rst_n_sync
add wave -noupdate /reset_generator_tb/i_dut/BUS_RST_N_O(0)
add wave -noupdate -divider <NULL>
add wave -noupdate /reset_generator_tb/i_dut/peripheral_rst
add wave -noupdate /reset_generator_tb/i_dut/PERIPHERAL_RST_CLK_I(0)
add wave -noupdate /reset_generator_tb/i_dut/peripheral_rst_sync
add wave -noupdate /reset_generator_tb/i_dut/PERIPHERAL_RST_O(0)
add wave -noupdate -divider <NULL>
add wave -noupdate /reset_generator_tb/i_dut/peripheral_rst
add wave -noupdate /reset_generator_tb/i_dut/PERIPHERAL_RST_N_CLK_I(0)
add wave -noupdate /reset_generator_tb/i_dut/peripheral_rst_n_sync
add wave -noupdate /reset_generator_tb/i_dut/PERIPHERAL_RST_N_O(0)
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {320 ns} 0} {{Cursor 2} {1050 ns} 0}
quietly wave cursor active 2
configure wave -namecolwidth 284
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 1
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 6250
configure wave -gridperiod 12500
configure wave -griddelta 40
configure wave -timeline 1
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1155 ns}
