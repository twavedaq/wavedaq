---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Slow Control GPIO
--
--  Project :  WaveDAQ DCB
--
--  PCB  :  DCB
--  Part :  Xilinx ZYNQ-7000
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  03.05.2019 12:04:00
--
--  Description :  General Purpose Control Interface for WDAQ DCB.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

------------------------------------------------------------------------------
------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------
library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;

------------------------------------------------------------------------------
-- Package Header
------------------------------------------------------------------------------
package io_definitions_pkg is

  constant C_SW_STATE_SIZE                         : natural :=  8;
  constant C_CTRL_SIZE                             : natural := 52;
  constant C_STAT_SIZE                             : natural := 12;

  -- Bit indices
  -- -- AXI GPIO
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 0;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 1;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 2;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 3;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 4;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 5;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 6;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 7;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 8;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 9;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 10;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 11;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 12;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 13;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 14;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 15;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 16;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 17;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 18;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 19;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 20;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 21;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 22;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 23;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 24;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 25;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 26;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 27;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 28;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 29;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 30;
--  constant C_BIT_IDX_AXI_CTRL_RESERVED             : natural := 31;

--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 0;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 1;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 2;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 3;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 4;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 5;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 6;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 7;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 8;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 9;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 10;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 11;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 12;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 13;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 14;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 15;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 16;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 17;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 18;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 19;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 20;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 21;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 22;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 23;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 24;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 25;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 26;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 27;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 28;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 29;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 30;
--  constant C_BIT_IDX_AXI_STAT_RESERVED             : natural := 31;

  -- -- EMIO GPIO
  constant C_BIT_IDX_EMIO_CTRL_INIT                   : natural := 0;
  constant C_BIT_IDX_EMIO_CTRL_FLASH_SEL              : natural := 1;
  constant C_BIT_IDX_EMIO_CTRL_BPL_SPI_SCHEME         : natural := 2;
  constant C_BIT_IDX_EMIO_CTRL_BPL_MASTER_SPI_DE      : natural := 3;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 4;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 5;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 6;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 7;
  constant C_BIT_IDX_EMIO_CTRL_SFP1_RS0               : natural := 8;
  constant C_BIT_IDX_EMIO_CTRL_SFP1_RS1               : natural := 9;
  constant C_BIT_IDX_EMIO_CTRL_SFP1_DISABLE           : natural := 10;
  constant C_BIT_IDX_EMIO_CTRL_ETH0_RST               : natural := 11;
  constant C_BIT_IDX_EMIO_CTRL_SFP2_RS0               : natural := 12;
  constant C_BIT_IDX_EMIO_CTRL_SFP2_RS1               : natural := 13;
  constant C_BIT_IDX_EMIO_CTRL_SFP2_DISABLE           : natural := 14;
  constant C_BIT_IDX_EMIO_CTRL_ETH1_RST               : natural := 15;
  constant C_BIT_IDX_EMIO_CTRL_SW_STATE_LO            : natural := 16;
  constant C_BIT_IDX_EMIO_CTRL_SW_STATE_HI            : natural := C_BIT_IDX_EMIO_CTRL_SW_STATE_LO+C_SW_STATE_SIZE-1;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 24;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 25;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 26;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 27;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 28;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 29;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 30;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 31;
  constant C_BIT_IDX_EMIO_CTRL_BOARD_SEL_LO           : natural := 32;
  constant C_BIT_IDX_EMIO_CTRL_BOARD_SEL_HI           : natural := 48;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 49;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 50;
--  constant C_BIT_IDX_EMIO_CTRL_RESERVED               : natural := 51;

  constant C_BIT_IDX_EMIO_STAT_FS_INIT                : natural := 52;
  constant C_BIT_IDX_EMIO_STAT_SPI_CS                 : natural := 53;
  constant C_BIT_IDX_EMIO_STAT_BOARD_SEL              : natural := 54;
--  constant C_BIT_IDX_EMIO_STAT_RESERVED               : natural := 55;
  constant C_BIT_IDX_EMIO_STAT_SFP1_TX_FAULT          : natural := 56;
  constant C_BIT_IDX_EMIO_STAT_SFP1_MOD               : natural := 57;
  constant C_BIT_IDX_EMIO_STAT_SFP1_LOS               : natural := 58;
--  constant C_BIT_IDX_EMIO_STAT_RESERVED               : natural := 59;
  constant C_BIT_IDX_EMIO_STAT_SFP2_TX_FAULT          : natural := 60;
  constant C_BIT_IDX_EMIO_STAT_SFP2_MOD               : natural := 61;
  constant C_BIT_IDX_EMIO_STAT_SFP2_LOS               : natural := 62;
--  constant C_BIT_IDX_EMIO_STAT_RESERVED               : natural := 63;




end package;
