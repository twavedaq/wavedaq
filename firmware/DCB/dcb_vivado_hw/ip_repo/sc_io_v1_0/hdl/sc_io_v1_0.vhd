---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Slow Control IO
--
--  Project :  WaveDAQ DCB
--
--  PCB  :  DCB
--  Part :  Xilinx ZYNQ-7000
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  03.05.2019 12:04:00
--
--  Description :  Slow control IO signal formatting for WDAQ DCB.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

------------------------------------------------------------------------------
-- Libraries
------------------------------------------------------------------------------

library ieee;
  use ieee.std_logic_1164.all;
  use ieee.numeric_std.all;
  use ieee.std_logic_misc.all;

library work;
  use work.io_definitions_pkg.all;

------------------------------------------------------------------------------
-- Entity
------------------------------------------------------------------------------
entity sc_io_v1_0 is
--  generic
--  (
--  );
  port
  (
    -- GPIO Buses
    GPIO_EMIO_I           : in  std_logic_vector(63 downto 0);
    GPIO_EMIO_O           : out std_logic_vector(63 downto 0);
--    GPIO_AXI_I            : in  std_logic_vector(31 downto 0);
--    GPIO_AXI_O            : out std_logic_vector(31 downto 0);
    -- Global select and init
    BPL_FS_INIT_N_I       : in  std_logic;
    BPL_FS_INIT_N_O       : out std_logic;
    BPL_SPI_CS_FPGA_N_I   : in  std_logic;
    BPL_BOARD_SELECT_N_I  : in  std_logic;
    BPL_BOARD_SELECT_N_O  : out std_logic_vector(16 downto 0);
    FPGA_RESET_I          : in  std_logic;
    FPGA_RESET_N_O        : out std_logic;
    -- BPL SPI
    SPI_CS_N_I            : in  std_logic_vector(2 downto 0);
    BPL_SPI_CS_N_O        : out std_logic;
    BPL_ANY_CS_N_O        : out std_logic;
    BPL_MASTER_SPI_DE_N_O : out std_logic;
    -- SFP Signals
    SFP_1_RS_O            : out std_logic_vector(1 downto 0);
    SFP_1_TX_DISABLE_O    : out std_logic;
    SFP_1_TX_FAULT_I      : in  std_logic;
    SFP_1_MOD_I           : in  std_logic;
    SFP_1_LOS_I           : in  std_logic;
    SFP_1_LOS_N_O         : out std_logic;
    SFP_2_RS_O            : out std_logic_vector(1 downto 0);
    SFP_2_TX_DISABLE_O    : out std_logic;
    SFP_2_TX_FAULT_I      : in  std_logic;
    SFP_2_MOD_I           : in  std_logic;
    SFP_2_LOS_I           : in  std_logic;
    SFP_2_LOS_N_O         : out std_logic;
    -- Ethernet IP resets
    ETH0_RESET_O          : out std_logic;
    ETH1_RESET_O          : out std_logic;
    -- Software Status
    SW_STATE_O            : out std_logic_vector(C_SW_STATE_SIZE-1 downto 0);
    -- Clock for IO registers
    CLK_I                 : in  std_logic
  );

end entity sc_io_v1_0;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture rtl of sc_io_v1_0 is

  signal bpl_any_cs_n   : std_logic := '1';
  signal bs_mux_sel     : std_logic := '0';
  signal fs_init        : std_logic := '0';

begin

  bpl_any_cs_n   <= AND_REDUCE(SPI_CS_N_I);
  BPL_ANY_CS_N_O <= bpl_any_cs_n;
  bs_mux_sel     <= bpl_any_cs_n and not(GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_BPL_SPI_SCHEME));
  fs_init        <= GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_INIT) or GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_FLASH_SEL);

  -- Feedback control signals to allow for read operations
  GPIO_EMIO_O(C_CTRL_SIZE-1 downto 0) <= GPIO_EMIO_I(C_CTRL_SIZE-1 downto 0);

  -- EMIO outputs
  BPL_MASTER_SPI_DE_N_O <= not GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_BPL_MASTER_SPI_DE);
  BPL_FS_INIT_N_O       <= not GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_INIT)      when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_BPL_SPI_SCHEME) = '0' else not fs_init;
  BPL_SPI_CS_N_O        <= not GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_FLASH_SEL) when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_BPL_SPI_SCHEME) = '0' else bpl_any_cs_n;
  BPL_BOARD_SELECT_N_O  <= not GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_BOARD_SEL_HI downto C_BIT_IDX_EMIO_CTRL_BOARD_SEL_LO) when bs_mux_sel = '0' else (others=>'1');
  SFP_1_RS_O(0)         <= 'Z' when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SFP1_RS0)     = '1' else '0';
  SFP_1_RS_O(1)         <= 'Z' when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SFP1_RS1)     = '1' else '0';
  SFP_1_TX_DISABLE_O    <= 'Z' when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SFP1_DISABLE) = '1' else '0';
  ETH0_RESET_O          <= GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_ETH0_RST);
  SFP_2_RS_O(0)         <= 'Z' when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SFP2_RS0)     = '1' else '0';
  SFP_2_RS_O(1)         <= 'Z' when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SFP2_RS1)     = '1' else '0';
  SFP_2_TX_DISABLE_O    <= 'Z' when GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SFP2_DISABLE) = '1' else '0';
  ETH1_RESET_O          <= GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_ETH1_RST);
  SW_STATE_O            <= GPIO_EMIO_I(C_BIT_IDX_EMIO_CTRL_SW_STATE_HI downto C_BIT_IDX_EMIO_CTRL_SW_STATE_LO);

  -- EMIO inputs
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_FS_INIT)       <= not BPL_FS_INIT_N_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SPI_CS)        <= not BPL_SPI_CS_FPGA_N_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_BOARD_SEL)     <= not BPL_BOARD_SELECT_N_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SFP1_TX_FAULT) <= SFP_1_TX_FAULT_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SFP1_MOD)      <= SFP_1_MOD_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SFP1_LOS)      <= SFP_1_LOS_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SFP2_TX_FAULT) <= SFP_2_TX_FAULT_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SFP2_MOD)      <= SFP_2_MOD_I;
  GPIO_EMIO_O(C_BIT_IDX_EMIO_STAT_SFP2_LOS)      <= SFP_2_LOS_I;

  -- Miscelleneous
  SFP_1_LOS_N_O  <= not SFP_1_LOS_I;
  SFP_2_LOS_N_O  <= not SFP_2_LOS_I;

  -- non-register IOs
  FPGA_RESET_N_O <= not FPGA_RESET_I;

  --  -- IO FFs
--  process( CLK_I )
--  begin
--    if rising_edge(CLK_I) then
--    end if;
--  end process;

end rtl;
