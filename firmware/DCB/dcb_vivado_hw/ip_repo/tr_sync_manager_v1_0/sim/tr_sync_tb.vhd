--------------------------------------------------------------------------------
-- Company:
-- Engineer:
--
-- Create Date:   08:20:30 03/20/2017
-- Design Name:
-- Module Name:   /afs/psi.ch/project/genie1414/work/wavedream/firmware/pulse_shaper_test/tr_sync_tb.vhd
-- Project Name:  pulse_shaper_test
-- Target Device:
-- Tool versions:
-- Description:
--
-- VHDL Test Bench Created by ISE for module: pulse_shaper
--
-- Dependencies:
--
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes:
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;

library tr_sync_manager_v1_0;
use tr_sync_manager_v1_0.tr_sync_manager;

ENTITY tr_sync_tb IS
END tr_sync_tb;

ARCHITECTURE behavior OF tr_sync_tb IS

  -- Clock period definitions
  constant DLY_CLK_I_period   : time := 12.5 ns / 3;
  constant WDB_CLK_I_period   : time := 12.5 ns;

  signal TR_SYNC_FCI_P_I : std_logic := '0';
  signal TR_SYNC_FCI_N_I : std_logic := '0';
  signal TR_SYNC_BPL_O   : std_logic := '0';
  signal TR_SYNC_PS_I    : std_logic := '0';
  signal TR_SYNC_DCB_O   : std_logic := '0';
  signal DELAY_I         : std_logic_vector(4 downto 0) := (others=>'0');
  --signal DELAY_I         : std_logic_vector(8 downto 0) := (others=>'0');
  --signal LD_I            : std_logic := '0';
  --signal RST_I           : std_logic := '0';
  --signal CLK_I           : std_logic := '0';
  signal DLY_CLK_I       : std_logic := '0';
  signal WDB_CLK_I       : std_logic := '0';

BEGIN

  uut : entity tr_sync_manager_v1_0.tr_sync_manager
  port map
  (
    TR_SYNC_FCI_P_I  => TR_SYNC_FCI_P_I,
    TR_SYNC_FCI_N_I  => TR_SYNC_FCI_N_I,
    TR_SYNC_BPL_O    => TR_SYNC_BPL_O,
    TR_SYNC_PS_I     => TR_SYNC_PS_I,
    TR_SYNC_DCB_O    => TR_SYNC_DCB_O,
    DELAY_I          => DELAY_I,
    --LD_I             => ,
    --RST_I            => ,
    --CLK_I            => ,
    DLY_CLK_I        => DLY_CLK_I,
    WDB_CLK_I        => WDB_CLK_I
  );

  -- Clock process definitions
  DLY_CLK_I_process :process
  begin
    DLY_CLK_I <= '0';
    wait for DLY_CLK_I_period/2;
    DLY_CLK_I <= '1';
    wait for DLY_CLK_I_period/2;
  end process;

  WDB_CLK_I_process :process
  begin
    WDB_CLK_I <= '0';
    wait for WDB_CLK_I_period/2;
    WDB_CLK_I <= '1';
    wait for WDB_CLK_I_period/2;
  end process;

  -- Stimulus process
  stim_proc: process
  begin
    TR_SYNC_FCI_P_I <= '0';
    TR_SYNC_FCI_N_I <= '1';
    TR_SYNC_PS_I    <= '0';
    wait until WDB_CLK_I = '1';
    wait until WDB_CLK_I = '1';
    TR_SYNC_PS_I    <= '1';
    wait until WDB_CLK_I = '1';
    wait until WDB_CLK_I = '1';
    TR_SYNC_PS_I    <= '0';
    wait for 100 ns;
    wait until WDB_CLK_I = '1';
    DELAY_I <= DELAY_I + 1;
  end process;

END;
