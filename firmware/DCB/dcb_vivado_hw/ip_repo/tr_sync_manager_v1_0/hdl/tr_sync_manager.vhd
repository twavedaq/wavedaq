---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Trigger Sync Signal Manager
--
--  Project :  MEG DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ XC7Z030-1FBG676C
--
--  Tool Version :  Vivado 2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  14.01.2019 09:32:00
--
--  Description :  Receives tr_sync sync pulse inputs from cable and registers and drives
--                 them to the backplane.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;
Library tr_sync_manager_v1_0;
use tr_sync_manager_v1_0.tr_sync_delay;

entity tr_sync_manager is
  port (
    -- Front Panel Cable
    TR_SYNC_FCI_P_I  : in  std_logic;
    TR_SYNC_FCI_N_I  : in  std_logic;
    -- Backplane
    TR_SYNC_BPL_O    : out std_logic;
    -- Internal
    TR_SYNC_PS_I     : in  std_logic;
    TR_SYNC_DCB_O    : out std_logic;
    DELAY_I          : in  std_logic_vector(4 downto 0);
    --DELAY_I   : in  std_logic_vector(8 downto 0);
    --LD_I      : in  std_logic;
    --RST_I     : in  std_logic;
    --CLK_I     : in  std_logic;
    DLY_CLK_I        : in  std_logic;
    WDB_CLK_I        : in  std_logic
  );
end tr_sync_manager;

architecture behavioral of tr_sync_manager is

  signal tr_sync_ps_sync0    : std_logic := '0';
  signal tr_sync_ps_sync1    : std_logic := '0';
  signal tr_sync_ps_edge     : std_logic := '0';
  signal tr_sync_ps_two_shot : std_logic := '0';
  signal tr_sync_ps          : std_logic := '0';

  signal tr_sync_out_bpl     : std_logic := '0';
  signal tr_sync_fci         : std_logic := '0';
  signal tr_sync_fci_sync0   : std_logic := '0';
  signal tr_sync_fci_sync1   : std_logic := '0';
  signal tr_sync_dcb         : std_logic := '0';

  signal tr_sync             : std_logic := '0';

  attribute syn_srlstyle : string;
    attribute syn_srlstyle of tr_sync_ps_sync0     : signal is "registers";
    attribute syn_srlstyle of tr_sync_ps_sync1     : signal is "registers";
    attribute syn_srlstyle of tr_sync_fci_sync0    : signal is "registers";
    attribute syn_srlstyle of tr_sync_fci_sync1    : signal is "registers";

  attribute shreg_extract : string;
    attribute shreg_extract of tr_sync_ps_sync0    : signal is "no";
    attribute shreg_extract of tr_sync_ps_sync1    : signal is "no";
    attribute shreg_extract of tr_sync_fci_sync0   : signal is "no";
    attribute shreg_extract of tr_sync_fci_sync1   : signal is "no";

  attribute ASYNC_REG : string;
    attribute ASYNC_REG of tr_sync_ps_sync0        : signal is "TRUE";
    attribute ASYNC_REG of tr_sync_ps_sync1        : signal is "TRUE";
    attribute ASYNC_REG of tr_sync_fci_sync0       : signal is "TRUE";
    attribute ASYNC_REG of tr_sync_fci_sync1       : signal is "TRUE";

	attribute IOB : string;
    attribute IOB of tr_sync_fci_sync0 : signal is "TRUE";

begin

  IBUFDS_inst : IBUFDS
  generic map (
    DIFF_TERM    => FALSE, -- Differential Termination
    IBUF_LOW_PWR => TRUE,  -- Low power (TRUE) vs. performance (FALSE) setting for referenced I/O standards
    IOSTANDARD   => "DEFAULT")
  port map (
    O  => tr_sync_fci,   -- Buffer output
    I  => TR_SYNC_FCI_P_I, -- Diff_p buffer input (connect directly to top-level port)
    IB => TR_SYNC_FCI_N_I  -- Diff_n buffer input (connect directly to top-level port)
  );

  -- tr_sync routing
  tr_sync       <= tr_sync_fci_sync1 or tr_sync_ps;
  TR_SYNC_BPL_O <= tr_sync_out_bpl; -- insert output delay
  TR_SYNC_DCB_O <= tr_sync_dcb;

  process(WDB_CLK_I)
  begin
    if rising_edge(WDB_CLK_I) then
      tr_sync_ps_sync0    <= TR_SYNC_PS_I;
      tr_sync_ps_sync1    <= tr_sync_ps_sync0;
      tr_sync_ps_edge     <= tr_sync_ps_sync1;
      tr_sync_ps_two_shot <= tr_sync_ps_sync1 and not tr_sync_ps_edge;
      tr_sync_ps  <= tr_sync_ps_two_shot or (tr_sync_ps_sync1 and not tr_sync_ps_edge);
    end if;
  end process;

  process(WDB_CLK_I)
  begin
    if rising_edge(WDB_CLK_I) then
      -- input and sync
      tr_sync_fci_sync0 <= tr_sync_fci;
      tr_sync_fci_sync1 <= tr_sync_fci_sync0;
      -- output
      tr_sync_dcb       <= tr_sync; -- delay or route back from io pin?
    end if;
  end process;

  TR_SYNC_DELAY_INST : entity tr_sync_manager_v1_0.tr_sync_delay
  port map(
    DATA_I    => tr_sync,
    DATA_O    => tr_sync_out_bpl,
    DELAY_I   => DELAY_I,
    --LD_I      => LD_I,
    --RST_I     => RST_I,
    --CLK_I     => CLK_I,
    DLY_CLK_I => DLY_CLK_I
  );

end architecture behavioral;
