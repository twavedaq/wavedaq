---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Trigger Sync Delay
--
--  Project :  MEG DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ XC7Z030-1FBG676C
--
--  Tool Version :  Vivado 2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  17.01.2019 14:32:00
--
--  Description :  Delays the trigger sync output
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity tr_sync_delay is
  port (
    DATA_I    : in  std_logic;
    DATA_O    : out std_logic;
    DELAY_I   : in  std_logic_vector(4 downto 0);
    --DELAY_I   : in  std_logic_vector(9 downto 0);
    --LD_I      : in  std_logic;
    --RST_I     : in  std_logic;
    --CLK_I     : in  std_logic;
    DLY_CLK_I : in  std_logic
  );
end tr_sync_delay;

architecture behavioral of tr_sync_delay is

  --signal odelay       : std_logic_vector(4 downto 0) := (others=>'0');
  signal edge_sel     : std_logic := '0';
  signal sreg_sel     : std_logic_vector(3 downto 0) := (others=>'0');

  signal sreg_out     : std_logic := '0';
  signal sreg_out_dly : std_logic := '0';

  signal oddr_d_p     : std_logic := '0';
  signal oddr_d_n     : std_logic := '0';

  --signal odelay_in    : std_logic := '0';

  --attribute IODELAY_GROUP : STRING;
  --attribute IODELAY_GROUP of <label_name>: label is "<iodelay_group_name>";

begin

  edge_sel <= DELAY_I(0);
  sreg_sel <= DELAY_I(4 downto 1);
  --odelay   <= DELAY_I(4 downto 0);
  --edge_sel <= DELAY_I(5);
  --sreg_sel <= DELAY_I(9 downto 6);

   SRL16E_inst : SRL16E
   generic map (
      INIT => X"0001")
   port map (
      Q   => sreg_out,
      A0  => sreg_sel(0),
      A1  => sreg_sel(1),
      A2  => sreg_sel(2),
      A3  => sreg_sel(3),
      CE  => '1',
      CLK => DLY_CLK_I,
      D   => DATA_I
   );

  process(DLY_CLK_I)
  begin
    if rising_edge(DLY_CLK_I) then
      sreg_out_dly <= sreg_out;
    end if;
  end process;

  oddr_d_p <= sreg_out_dly when edge_sel = '1' else sreg_out;
  oddr_d_n <= sreg_out;

  ODDR_inst : ODDR
  generic map(
    DDR_CLK_EDGE => "SAME_EDGE", -- "OPPOSITE_EDGE" or "SAME_EDGE"
    INIT         => '0',
    SRTYPE       => "SYNC"
  )
  port map (
    Q  => DATA_O,
    C  => DLY_CLK_I,
    CE => '1',
    D1 => oddr_d_p,
    D2 => oddr_d_n,
    R  => '0',
    S  => '0'
  );

--  odelay_in <= DATA_O;
--
--   ODELAYE2_inst : ODELAYE2
--   generic map (
--      CINVCTRL_SEL          => "FALSE",    -- Enable dynamic clock inversion (FALSE, TRUE)
--      DELAY_SRC             => "ODATAIN",  -- Delay input (ODATAIN, CLKIN)
--      HIGH_PERFORMANCE_MODE => "FALSE",    -- Reduced jitter ("TRUE"), Reduced power ("FALSE")
--      ODELAY_TYPE           => "VAR_LOAD", -- FIXED, VARIABLE, VAR_LOAD, VAR_LOAD_PIPE
--      ODELAY_VALUE          => 0,          -- Output delay tap setting (0-31)
--      PIPE_SEL              => "FALSE",    -- Select pipelined mode, FALSE, TRUE
--      REFCLK_FREQUENCY      => 200.0,      -- IDELAYCTRL clock input frequency in MHz (190.0-210.0, 290.0-310.0).
--      SIGNAL_PATTERN        => "DATA"      -- DATA, CLOCK input signal
--   )
--   port map (
--      CNTVALUEOUT => open,     -- 5-bit output: Counter value output
--      DATAOUT     => DATA_O,   -- 1-bit output: Delayed data/clock output
--      C           => CLK_I,    -- 1-bit input: Clock input
--      CE          => '0',      -- 1-bit input: Active high enable increment/decrement input
--      CINVCTRL    => '0',      -- 1-bit input: Dynamic clock inversion input
--      CLKIN       => '0',      -- 1-bit input: Clock delay input
--      CNTVALUEIN  => odelay,   -- 5-bit input: Counter value input
--      INC         => '0',      -- 1-bit input: Increment / Decrement tap delay input
--      LD          => LD_I,     -- 1-bit input: Loads ODELAY_VALUE tap delay in VARIABLE mode, in VAR_LOAD or
--                               -- VAR_LOAD_PIPE mode, loads the value of CNTVALUEIN
--      LDPIPEEN    => '0',      -- 1-bit input: Enables the pipeline register to load data
--      ODATAIN     => sreg_out, -- 1-bit input: Output delay data input
--      REGRST      => RST_I     -- 1-bit input: Active-high reset tap-delay input
--   );

end architecture behavioral;
