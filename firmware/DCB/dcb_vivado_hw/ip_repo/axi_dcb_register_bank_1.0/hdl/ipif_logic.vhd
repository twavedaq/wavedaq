------------------------------------------------------------------------------
-- ipif_logic.vhd - entity/architecture pair
------------------------------------------------------------------------------
--
-- ***************************************************************************
-- ** Copyright (c) 1995-2010 Xilinx, Inc.  All rights reserved.            **
-- **                                                                       **
-- ** Xilinx, Inc.                                                          **
-- ** XILINX IS PROVIDING THIS DESIGN, CODE, OR INFORMATION "AS IS"         **
-- ** AS A COURTESY TO YOU, SOLELY FOR USE IN DEVELOPING PROGRAMS AND       **
-- ** SOLUTIONS FOR XILINX DEVICES.  BY PROVIDING THIS DESIGN, CODE,        **
-- ** OR INFORMATION AS ONE POSSIBLE IMPLEMENTATION OF THIS FEATURE,        **
-- ** APPLICATION OR STANDARD, XILINX IS MAKING NO REPRESENTATION           **
-- ** THAT THIS IMPLEMENTATION IS FREE FROM ANY CLAIMS OF INFRINGEMENT,     **
-- ** AND YOU ARE RESPONSIBLE FOR OBTAINING ANY RIGHTS YOU MAY REQUIRE      **
-- ** FOR YOUR IMPLEMENTATION.  XILINX EXPRESSLY DISCLAIMS ANY              **
-- ** WARRANTY WHATSOEVER WITH RESPECT TO THE ADEQUACY OF THE               **
-- ** IMPLEMENTATION, INCLUDING BUT NOT LIMITED TO ANY WARRANTIES OR        **
-- ** REPRESENTATIONS THAT THIS IMPLEMENTATION IS FREE FROM CLAIMS OF       **
-- ** INFRINGEMENT, IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS       **
-- ** FOR A PARTICULAR PURPOSE.                                             **
-- **                                                                       **
-- ***************************************************************************
--
------------------------------------------------------------------------------
-- Filename:          ipif_logic.vhd
-- Version:           1.00.a
-- Description:       User logic.
-- Date:              Mon Jan 29 12:27:58 2018 (by Create and Import Peripheral Wizard)
-- VHDL Standard:     VHDL'93
------------------------------------------------------------------------------
-- Naming Conventions:
--   active low signals:                    "*_n"
--   clock signals:                         "clk", "clk_div#", "clk_#x"
--   reset signals:                         "rst", "rst_n"
--   generics:                              "C_*"
--   user defined types:                    "*_TYPE"
--   state machine next state:              "*_ns"
--   state machine current state:           "*_cs"
--   combinatorial signals:                 "*_com"
--   pipelined or register delay signals:   "*_d#"
--   counter signals:                       "*cnt*"
--   clock enable signals:                  "*_ce"
--   internal version of output port:       "*_i"
--   device pins:                           "*_pin"
--   ports:                                 "- Names begin with Uppercase"
--   processes:                             "*_PROCESS"
--   component instantiations:              "<ENTITY_>I_<#|FUNC>"
------------------------------------------------------------------------------

-- DO NOT EDIT BELOW THIS LINE --------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library proc_common_v3_00_a;
use proc_common_v3_00_a.proc_common_pkg.all;

-- DO NOT EDIT ABOVE THIS LINE --------------------

--USER libraries added here


library axi_dcb_reg_bank_v1_0;
use axi_dcb_reg_bank_v1_0.ipif_user_cfg.all;


------------------------------------------------------------------------------
-- Entity section
------------------------------------------------------------------------------
--
-- Definition of Ports:
--   Bus2IP_Clk                   -- Bus to IP clock
--   Bus2IP_Reset                 -- Bus to IP reset
--   Bus2IP_Addr                  -- Bus to IP address bus
--   Bus2IP_CS                    -- Bus to IP chip select for user logic memory selection
--   Bus2IP_RNW                   -- Bus to IP read/not write
--   Bus2IP_Data                  -- Bus to IP data bus
--   Bus2IP_BE                    -- Bus to IP byte enables
--   Bus2IP_RdCE                  -- Bus to IP read chip enable
--   Bus2IP_WrCE                  -- Bus to IP write chip enable
--   IP2Bus_Data                  -- IP to Bus data bus
--   IP2Bus_RdAck                 -- IP to Bus read transfer acknowledgement
--   IP2Bus_WrAck                 -- IP to Bus write transfer acknowledgement
--   IP2Bus_Error                 -- IP to Bus error response
------------------------------------------------------------------------------

entity ipif_logic is
--  generic
--  (
--    CGN_USE_MEM0_SWAP         : boolean  := false;    --  true;  --  false; --
--    CGN_USE_MEM1_SWAP         : boolean  := false     --  true   --  false  --
--  );
  port
  (
    -- ADD USER PORTS BELOW THIS LINE ------------------
    --USER ports added here
    -- ADD USER PORTS ABOVE THIS LINE ------------------
    USER_TO_IPIF_I                 : in  user_to_ipif_type;
    IPIF_TO_USER_O                 : out ipif_to_user_type;

    -- DO NOT EDIT BELOW THIS LINE ---------------------
    -- Bus protocol ports, do not add to or delete
    Bus2IP_Clk                     : in  std_logic;
    Bus2IP_Reset                   : in  std_logic;
    Bus2IP_Addr                    : in  std_logic_vector(0 to C_SLV_AWIDTH-1);
    Bus2IP_CS                      : in  std_logic_vector(0 to C_NUM_CS-1);
    Bus2IP_RNW                     : in  std_logic;
    Bus2IP_Data                    : in  std_logic_vector(0 to C_SLV_DWIDTH-1);
    Bus2IP_BE                      : in  std_logic_vector(0 to C_SLV_DWIDTH/8-1);
    Bus2IP_RdCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    Bus2IP_WrCE                    : in  std_logic_vector(0 to C_NUM_REG-1);
    IP2Bus_Data                    : out std_logic_vector(0 to C_SLV_DWIDTH-1);
    IP2Bus_RdAck                   : out std_logic;
    IP2Bus_WrAck                   : out std_logic;
    IP2Bus_Error                   : out std_logic
    -- DO NOT EDIT ABOVE THIS LINE ---------------------
  );

  attribute SIGIS : string;
  attribute SIGIS of Bus2IP_Clk    : signal is "CLK";
  attribute SIGIS of Bus2IP_Reset  : signal is "RST";

end entity ipif_logic;

------------------------------------------------------------------------------
-- Architecture section
------------------------------------------------------------------------------

architecture IMP of ipif_logic is

  --USER signal declarations added here, as needed for user logic

  signal  reg_ip2bus_data       :  std_logic_vector(C_SLV_DWIDTH-1   downto 0);
  signal  reg_ip2bus_rdack      :  std_logic;
  signal  reg_ip2bus_wrack      :  std_logic;
  signal  reg_ip2bus_error      :  std_logic;

--  signal  mem_ip2bus_data       :  std_logic_vector(C_SLV_DWIDTH-1   downto 0);
--  signal  mem_ip2bus_rdack      :  std_logic;
--  signal  mem_ip2bus_wrack      :  std_logic;
--  signal  mem_ip2bus_error      :  std_logic;

begin


  ipif_register_inst : entity axi_dcb_reg_bank_v1_0.ipif_register
  port map
  (
    -- MAP USER PORTS BELOW THIS LINE ------------------
    USER_TO_REG_I  => USER_TO_IPIF_I.reg,
    REG_TO_USER_O  => IPIF_TO_USER_O.reg,
    -- MAP USER PORTS ABOVE THIS LINE ------------------

    Bus2IP_Clk     => Bus2IP_Clk,
    Bus2IP_Reset   => Bus2IP_Reset,
    Bus2IP_Data    => Bus2IP_Data,
    Bus2IP_BE      => Bus2IP_BE,
    Bus2IP_RdCE    => Bus2IP_RdCE,
    Bus2IP_WrCE    => Bus2IP_WrCE,

    IP2Bus_Data    => reg_ip2bus_data,
    IP2Bus_RdAck   => reg_ip2bus_rdack,
    IP2Bus_WrAck   => reg_ip2bus_wrack,
    IP2Bus_Error   => reg_ip2bus_error
  );

  IPIF_TO_USER_O.rst <= Bus2IP_Reset;

  IP2Bus_Data  <= reg_ip2bus_data when Bus2IP_CS(0) = '1' else
                  (others => '0');

  IP2Bus_WrAck <= reg_ip2bus_wrack;
  IP2Bus_RdAck <= reg_ip2bus_rdack;
  IP2Bus_Error <= reg_ip2bus_error;

--  ipif_memory_inst : entity axi_dcb_reg_bank_v1_0.ipif_memory
--  generic map
--  (
--    CGN_USE_MEM0_SWAP   => CGN_USE_MEM0_SWAP,
--    CGN_USE_MEM1_SWAP   => CGN_USE_MEM1_SWAP
--  )
--  port map
--  (
--    -- MAP USER PORTS BELOW THIS LINE ------------------
--    USER_TO_MEM_I  => USER_TO_IPIF_I.mem,
--    MEM_TO_USER_O  => IPIF_TO_USER_O.mem,
--
--    -- MAP USER PORTS ABOVE THIS LINE ------------------
--    Bus2IP_Clk     => Bus2IP_Clk,
--    Bus2IP_Reset   => Bus2IP_Reset,
--    Bus2IP_Data    => Bus2IP_Data,
--    Bus2IP_BE      => Bus2IP_BE,
--    Bus2IP_Addr    => Bus2IP_Addr,
--    Bus2IP_CS      => Bus2IP_CS(1 to 2),
--    Bus2IP_RNW     => Bus2IP_RNW,
--
--    IP2Bus_Data    => mem_ip2bus_data,
--    IP2Bus_RdAck   => mem_ip2bus_rdack,
--    IP2Bus_WrAck   => mem_ip2bus_wrack,
--    IP2Bus_Error   => mem_ip2bus_error
--  );
--
--  IPIF_TO_USER_O.rst <= Bus2IP_Reset;
--
--  IP2Bus_Data  <= reg_ip2bus_data when Bus2IP_CS(0) = '1' else
--                  mem_ip2bus_data when Bus2IP_CS(1) = '1' else
--                  mem_ip2bus_data when Bus2IP_CS(2) = '1' else
--                  (others => '0');
--
--  IP2Bus_WrAck <= reg_ip2bus_wrack or mem_ip2bus_wrack;
--  IP2Bus_RdAck <= reg_ip2bus_rdack or mem_ip2bus_rdack;
--  IP2Bus_Error <= reg_ip2bus_error or mem_ip2bus_error;

end IMP;
