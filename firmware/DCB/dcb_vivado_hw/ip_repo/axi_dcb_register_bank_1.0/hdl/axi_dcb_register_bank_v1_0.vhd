---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  axi_dcb_register_bank_v1_0.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)
--  Created :  15.01.2020 11:16:30
--
--  Description :  Toplevel of the MEGII DCB register bank.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi_dcb_reg_bank_v1_0;
use axi_dcb_reg_bank_v1_0.ipif_user_cfg.all;
use axi_dcb_reg_bank_v1_0.ipif_axi;

entity axi_dcb_register_bank_v1_0 is
  generic (
    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH  : integer           := 32;
    C_S00_AXI_ADDR_WIDTH  : integer           := 32;
    C_S00_AXI_BASEADDR    : std_logic_vector := X"FFFFFFFF";
    C_S00_AXI_HIGHADDR    : std_logic_vector := X"00000000";
    C_S00_AXI_MIN_SIZE    : std_logic_vector := X"FFFFFFFF";
    -- Users parameters
    C_USE_WSTRB           : integer          := 0;
    C_DPHASE_TIMEOUT      : integer          := 8;
    C_FAMILY              : string           := "virtex6"
    --C_MEM0_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM0_HIGHADDR       : std_logic_vector := X"00000000";
    --C_MEM1_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM1_HIGHADDR       : std_logic_vector := X"00000000"
  );
  port (
    -- -------------------------------------------
    -- User Ports                               --
    -- -------------------------------------------
    -- Register 0 [0x0000]: HW_VER
    HW_VER_REG_RD_STROBE_O              : out std_logic;
    HW_VER_REG_WR_STROBE_O              : out std_logic;

    -- Register 1 [0x0004]: REG_LAYOUT_VER
    REG_LAYOUT_VER_REG_RD_STROBE_O      : out std_logic;
    REG_LAYOUT_VER_REG_WR_STROBE_O      : out std_logic;

    -- Register 2 [0x0008]: FW_BUILD_DATE
    FW_BUILD_DATE_REG_RD_STROBE_O       : out std_logic;
    FW_BUILD_DATE_REG_WR_STROBE_O       : out std_logic;
    FW_BUILD_YEAR_I                     : in  std_logic_vector(15 downto 0);    
    FW_BUILD_MONTH_I                    : in  std_logic_vector(7 downto 0);     
    FW_BUILD_DAY_I                      : in  std_logic_vector(7 downto 0);     

    -- Register 3 [0x000C]: FW_BUILD_TIME
    FW_BUILD_TIME_REG_RD_STROBE_O       : out std_logic;
    FW_BUILD_TIME_REG_WR_STROBE_O       : out std_logic;
    FW_BUILD_HOUR_I                     : in  std_logic_vector(7 downto 0);     
    FW_BUILD_MINUTE_I                   : in  std_logic_vector(7 downto 0);     
    FW_BUILD_SECOND_I                   : in  std_logic_vector(7 downto 0);     

    -- Register 4 [0x0010]: SW_BUILD_DATE
    SW_BUILD_DATE_REG_RD_STROBE_O       : out std_logic;
    SW_BUILD_DATE_REG_WR_STROBE_O       : out std_logic;
    SW_BUILD_YEAR_O                     : out std_logic_vector(15 downto 0);    
    SW_BUILD_MONTH_O                    : out std_logic_vector(7 downto 0);     
    SW_BUILD_DAY_O                      : out std_logic_vector(7 downto 0);     

    -- Register 5 [0x0014]: SW_BUILD_TIME
    SW_BUILD_TIME_REG_RD_STROBE_O       : out std_logic;
    SW_BUILD_TIME_REG_WR_STROBE_O       : out std_logic;
    SW_BUILD_HOUR_O                     : out std_logic_vector(7 downto 0);     
    SW_BUILD_MINUTE_O                   : out std_logic_vector(7 downto 0);     
    SW_BUILD_SECOND_O                   : out std_logic_vector(7 downto 0);     

    -- Register 6 [0x0018]: FW_GIT_HASH_TAG
    FW_GIT_HASH_TAG_REG_RD_STROBE_O     : out std_logic;
    FW_GIT_HASH_TAG_REG_WR_STROBE_O     : out std_logic;
    FW_GIT_HASH_TAG_I                   : in  std_logic_vector(31 downto 0);    

    -- Register 7 [0x001C]: SW_GIT_HASH_TAG
    SW_GIT_HASH_TAG_REG_RD_STROBE_O     : out std_logic;
    SW_GIT_HASH_TAG_REG_WR_STROBE_O     : out std_logic;
    SW_GIT_HASH_TAG_O                   : out std_logic_vector(31 downto 0);    

    -- Register 8 [0x0020]: PROT_VER
    PROT_VER_REG_RD_STROBE_O            : out std_logic;
    PROT_VER_REG_WR_STROBE_O            : out std_logic;
    PROTOCOL_VERSION_O                  : out std_logic_vector(7 downto 0);     

    -- Register 9 [0x0024]: SN
    SN_REG_RD_STROBE_O                  : out std_logic;
    SN_REG_WR_STROBE_O                  : out std_logic;
    SERIAL_NUMBER_O                     : out std_logic_vector(15 downto 0);    

    -- Register 10 [0x0028]: STATUS
    STATUS_REG_RD_STROBE_O              : out std_logic;
    STATUS_REG_WR_STROBE_O              : out std_logic;
    FLASH_SEL_I                         : in  std_logic;                        
    BOARD_SEL_I                         : in  std_logic;                        
    SERIAL_BUSY_I                       : in  std_logic;                        
    DCB_BUSY_I                          : in  std_logic;                        
    SYS_BUSY_I                          : in  std_logic;                        

    -- Register 11 [0x002C]: TEMP
    TEMP_REG_RD_STROBE_O                : out std_logic;
    TEMP_REG_WR_STROBE_O                : out std_logic;
    TEMPERATURE_O                       : out std_logic_vector(15 downto 0);    

    -- Register 12 [0x0030]: PLL_LOCK
    PLL_LOCK_REG_RD_STROBE_O            : out std_logic;
    PLL_LOCK_REG_WR_STROBE_O            : out std_logic;
    WDB_CLK_MGR_LOCK_I                  : in  std_logic;                        
    SYS_DCM_LOCK_I                      : in  std_logic;                        
    LMK_PLL_LOCK_I                      : in  std_logic;                        

    -- Register 13 [0x0034]: DCB_LOC
    DCB_LOC_REG_RD_STROBE_O             : out std_logic;
    DCB_LOC_REG_WR_STROBE_O             : out std_logic;
    CRATE_ID_O                          : out std_logic_vector(7 downto 0);     
    SLOT_ID_O                           : out std_logic_vector(7 downto 0);     

    -- Register 14 [0x0038]: CTRL
    CTRL_REG_RD_STROBE_O                : out std_logic;
    CTRL_REG_WR_STROBE_O                : out std_logic;
    SYNC_DELAY_O                        : out std_logic_vector(4 downto 0);     
    DAQ_SOFT_TRIGGER_O                  : out std_logic;                        

    -- Register 15 [0x003C]: SET_CTRL
    SET_CTRL_REG_RD_STROBE_O            : out std_logic;
    SET_CTRL_REG_WR_STROBE_O            : out std_logic;
    SET_BIT_CTRL_O                      : out std_logic_vector(31 downto 0);    

    -- Register 16 [0x0040]: CLR_CTRL
    CLR_CTRL_REG_RD_STROBE_O            : out std_logic;
    CLR_CTRL_REG_WR_STROBE_O            : out std_logic;
    CLR_BIT_CTRL_O                      : out std_logic_vector(31 downto 0);    

    -- Register 17 [0x0044]: CLK_CTRL
    CLK_CTRL_REG_RD_STROBE_O            : out std_logic;
    CLK_CTRL_REG_WR_STROBE_O            : out std_logic;
    DISTRIBUTOR_CLK_OUT_EN_O            : out std_logic_vector(19 downto 0);    
    DISTRIBUTOR_CLK_SRC_SEL_O           : out std_logic;                        
    BUS_CLK_SRC_SEL_O                   : out std_logic;                        
    LMK_CLK_SRC_SEL_O                   : out std_logic;                        
    EXT_CLK_IN_SEL_O                    : out std_logic;                        

    -- Register 18 [0x0048]: SET_CLK_CTRL
    SET_CLK_CTRL_REG_RD_STROBE_O        : out std_logic;
    SET_CLK_CTRL_REG_WR_STROBE_O        : out std_logic;
    SET_BIT_CLK_CTRL_O                  : out std_logic_vector(31 downto 0);    

    -- Register 19 [0x004C]: CLR_CLK_CTRL
    CLR_CLK_CTRL_REG_RD_STROBE_O        : out std_logic;
    CLR_CLK_CTRL_REG_WR_STROBE_O        : out std_logic;
    CLR_BIT_CLK_CTRL_O                  : out std_logic_vector(31 downto 0);    

    -- Register 20 [0x0050]: COM_CTRL
    COM_CTRL_REG_RD_STROBE_O            : out std_logic;
    COM_CTRL_REG_WR_STROBE_O            : out std_logic;
    INTER_PKG_DELAY_O                   : out std_logic_vector(23 downto 0);    

    -- Register 21 [0x0054]: RST
    RST_REG_RD_STROBE_O                 : out std_logic;
    RST_REG_WR_STROBE_O                 : out std_logic;
    WDB_CLK_MGR_RST_O                   : out std_logic;                        
    TRIGGER_MGR_RST_O                   : out std_logic;                        
    TR_SYNC_BPL_O                       : out std_logic;                        
    LMK_SYNC_DCB_O                      : out std_logic;                        
    DATA_LINK_IF_RST_O                  : out std_logic;                        
    RECONFIGURE_FPGA_O                  : out std_logic;                        

    -- Register 22 [0x0058]: APLY_CFG
    APLY_CFG_REG_RD_STROBE_O            : out std_logic;
    APLY_CFG_REG_WR_STROBE_O            : out std_logic;
    APPLY_SETTINGS_LMK_O                : out std_logic;                        

    -- Register 23 [0x005C]: LMK_0
    LMK_0_REG_RD_STROBE_O               : out std_logic;
    LMK_0_REG_WR_STROBE_O               : out std_logic;
    LMK0_RESET_O                        : out std_logic;                        
    LMK0_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK0_CLKOUT_EN_O                    : out std_logic;                        
    LMK0_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK0_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 24 [0x0060]: LMK_1
    LMK_1_REG_RD_STROBE_O               : out std_logic;
    LMK_1_REG_WR_STROBE_O               : out std_logic;
    LMK1_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK1_CLKOUT_EN_O                    : out std_logic;                        
    LMK1_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK1_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 25 [0x0064]: LMK_2
    LMK_2_REG_RD_STROBE_O               : out std_logic;
    LMK_2_REG_WR_STROBE_O               : out std_logic;
    LMK2_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK2_CLKOUT_EN_O                    : out std_logic;                        
    LMK2_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK2_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 26 [0x0068]: LMK_3
    LMK_3_REG_RD_STROBE_O               : out std_logic;
    LMK_3_REG_WR_STROBE_O               : out std_logic;
    LMK3_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK3_CLKOUT_EN_O                    : out std_logic;                        
    LMK3_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK3_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 27 [0x006C]: LMK_4
    LMK_4_REG_RD_STROBE_O               : out std_logic;
    LMK_4_REG_WR_STROBE_O               : out std_logic;
    LMK4_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK4_CLKOUT_EN_O                    : out std_logic;                        
    LMK4_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK4_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 28 [0x0070]: LMK_5
    LMK_5_REG_RD_STROBE_O               : out std_logic;
    LMK_5_REG_WR_STROBE_O               : out std_logic;
    LMK5_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK5_CLKOUT_EN_O                    : out std_logic;                        
    LMK5_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK5_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 29 [0x0074]: LMK_6
    LMK_6_REG_RD_STROBE_O               : out std_logic;
    LMK_6_REG_WR_STROBE_O               : out std_logic;
    LMK6_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK6_CLKOUT_EN_O                    : out std_logic;                        
    LMK6_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK6_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 30 [0x0078]: LMK_7
    LMK_7_REG_RD_STROBE_O               : out std_logic;
    LMK_7_REG_WR_STROBE_O               : out std_logic;
    LMK7_CLKOUT_MUX_O                   : out std_logic_vector(1 downto 0);     
    LMK7_CLKOUT_EN_O                    : out std_logic;                        
    LMK7_CLKOUT_DIV_O                   : out std_logic_vector(7 downto 0);     
    LMK7_CLKOUT_DLY_O                   : out std_logic_vector(3 downto 0);     

    -- Register 31 [0x007C]: LMK_8
    LMK_8_REG_RD_STROBE_O               : out std_logic;
    LMK_8_REG_WR_STROBE_O               : out std_logic;
    LMK8_PHASE_NOISE_OPT_O              : out std_logic_vector(27 downto 0);    

    -- Register 32 [0x0080]: LMK_9
    LMK_9_REG_RD_STROBE_O               : out std_logic;
    LMK_9_REG_WR_STROBE_O               : out std_logic;
    LMK9_VBOOST_O                       : out std_logic;                        

    -- Register 33 [0x0084]: LMK_11
    LMK_11_REG_RD_STROBE_O              : out std_logic;
    LMK_11_REG_WR_STROBE_O              : out std_logic;
    LMK11_DIV4_O                        : out std_logic;                        

    -- Register 34 [0x0088]: LMK_13
    LMK_13_REG_RD_STROBE_O              : out std_logic;
    LMK_13_REG_WR_STROBE_O              : out std_logic;
    LMK13_OSCIN_FREQ_O                  : out std_logic_vector(7 downto 0);     
    LMK13_VCO_R4_LF_O                   : out std_logic_vector(2 downto 0);     
    LMK13_VCO_R3_LF_O                   : out std_logic_vector(2 downto 0);     
    LMK13_VCO_C3_C4_LF_O                : out std_logic_vector(3 downto 0);     

    -- Register 35 [0x008C]: LMK_14
    LMK_14_REG_RD_STROBE_O              : out std_logic;
    LMK_14_REG_WR_STROBE_O              : out std_logic;
    LMK14_EN_FOUT_O                     : out std_logic;                        
    LMK14_EN_CLKOUT_GLOBAL_O            : out std_logic;                        
    LMK14_POWERDOWN_O                   : out std_logic;                        
    LMK14_PLL_MUX_O                     : out std_logic_vector(3 downto 0);     
    LMK14_PLL_R_O                       : out std_logic_vector(11 downto 0);    

    -- Register 36 [0x0090]: LMK_15
    LMK_15_REG_RD_STROBE_O              : out std_logic;
    LMK_15_REG_WR_STROBE_O              : out std_logic;
    LMK15_PLL_CP_GAIN_O                 : out std_logic_vector(1 downto 0);     
    LMK15_VCO_DIV_O                     : out std_logic_vector(3 downto 0);     
    LMK15_PLL_N_O                       : out std_logic_vector(17 downto 0);    

    -- Register 37 [0x0094]: TIME_LSB
    TIME_LSB_REG_RD_STROBE_O            : out std_logic;
    TIME_LSB_REG_WR_STROBE_O            : out std_logic;
    TIME_LSB_I                          : in  std_logic_vector(31 downto 0);    

    -- Register 38 [0x0098]: TIME_MSB
    TIME_MSB_REG_RD_STROBE_O            : out std_logic;
    TIME_MSB_REG_WR_STROBE_O            : out std_logic;
    TIME_MSB_I                          : in  std_logic_vector(31 downto 0);    

    -- Register 39 [0x009C]: TIME_LSB_SET
    TIME_LSB_SET_REG_RD_STROBE_O        : out std_logic;
    TIME_LSB_SET_REG_WR_STROBE_O        : out std_logic;
    TIME_LSB_SET_O                      : out std_logic_vector(31 downto 0);    

    -- Register 40 [0x00A0]: TIME_MSB_SET
    TIME_MSB_SET_REG_RD_STROBE_O        : out std_logic;
    TIME_MSB_SET_REG_WR_STROBE_O        : out std_logic;
    TIME_MSB_SET_O                      : out std_logic_vector(31 downto 0);    

    -- Register 41 [0x00A4]: EVENT_TX_RATE
    EVENT_TX_RATE_REG_RD_STROBE_O       : out std_logic;
    EVENT_TX_RATE_REG_WR_STROBE_O       : out std_logic;
    EVENT_TX_RATE_I                     : in  std_logic_vector(31 downto 0);    

    -- Register 42 [0x00A8]: EVENT_NR
    EVENT_NR_REG_RD_STROBE_O            : out std_logic;
    EVENT_NR_REG_WR_STROBE_O            : out std_logic;
    EVENT_NUMBER_I                      : in  std_logic_vector(31 downto 0);    

    -- Register 43 [0x00AC]: TRG_CFG
    TRG_CFG_REG_RD_STROBE_O             : out std_logic;
    TRG_CFG_REG_WR_STROBE_O             : out std_logic;
    EXT_TRIGGER_OUT_ENABLE_O            : out std_logic;                        
    ENABLE_AUTO_TRIGGER_O               : out std_logic;                        
    MANUAL_TRIGGER_O                    : out std_logic;                        

    -- Register 44 [0x00B0]: SET_TRG_CFG
    SET_TRG_CFG_REG_RD_STROBE_O         : out std_logic;
    SET_TRG_CFG_REG_WR_STROBE_O         : out std_logic;
    SET_BIT_TRG_CFG_O                   : out std_logic_vector(31 downto 0);    

    -- Register 45 [0x00B4]: CLR_TRG_CFG
    CLR_TRG_CFG_REG_RD_STROBE_O         : out std_logic;
    CLR_TRG_CFG_REG_WR_STROBE_O         : out std_logic;
    CLR_BIT_TRG_CFG_O                   : out std_logic_vector(31 downto 0);    

    -- Register 46 [0x00B8]: TRG_AUTO_PERIOD
    TRG_AUTO_PERIOD_REG_RD_STROBE_O     : out std_logic;
    TRG_AUTO_PERIOD_REG_WR_STROBE_O     : out std_logic;
    AUTO_TRIGGER_PERIOD_O               : out std_logic_vector(31 downto 0);    

    -- Register 47 [0x00BC]: TRB_INFO_STAT
    TRB_INFO_STAT_REG_RD_STROBE_O       : out std_logic;
    TRB_INFO_STAT_REG_WR_STROBE_O       : out std_logic;
    TRB_FLAG_NEW_I                      : in  std_logic;                        
    TRB_FLAG_PARITY_ERROR_I             : in  std_logic;                        
    TRB_PARITY_ERROR_COUNT_I            : in  std_logic_vector(15 downto 0);    

    -- Register 48 [0x00C0]: TRB_INFO_LSB
    TRB_INFO_LSB_REG_RD_STROBE_O        : out std_logic;
    TRB_INFO_LSB_REG_WR_STROBE_O        : out std_logic;
    TRB_INFO_LSB_I                      : in  std_logic_vector(31 downto 0);    

    -- Register 49 [0x00C4]: TRB_INFO_MSB
    TRB_INFO_MSB_REG_RD_STROBE_O        : out std_logic;
    TRB_INFO_MSB_REG_WR_STROBE_O        : out std_logic;
    TRB_INFO_MSB_I                      : in  std_logic_vector(15 downto 0);    

    -- Register 50 [0x00C8]: LMK_MOD_FLAG
    LMK_MOD_FLAG_REG_RD_STROBE_O        : out std_logic;
    LMK_MOD_FLAG_REG_WR_STROBE_O        : out std_logic;
    LMK_7_MOD_I                         : in  std_logic;                        
    LMK_6_MOD_I                         : in  std_logic;                        
    LMK_5_MOD_I                         : in  std_logic;                        
    LMK_4_MOD_I                         : in  std_logic;                        
    LMK_3_MOD_I                         : in  std_logic;                        
    LMK_2_MOD_I                         : in  std_logic;                        
    LMK_1_MOD_I                         : in  std_logic;                        
    LMK_0_MOD_I                         : in  std_logic;                        

    -- Register 51 [0x00CC]: CRC32_REG_BANK
    CRC32_REG_BANK_REG_RD_STROBE_O      : out std_logic;
    CRC32_REG_BANK_REG_WR_STROBE_O      : out std_logic;
    CRC32_REG_BANK_O                    : out std_logic_vector(31 downto 0);    

    -- -------------------------------------------
    -- Ports of Axi Slave Bus Interface S00_AXI --
    -- -------------------------------------------
    s00_axi_aclk     : in std_logic;
    s00_axi_aresetn  : in std_logic;
    s00_axi_awaddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot   : in std_logic_vector(2 downto 0);
    s00_axi_awvalid  : in std_logic;
    s00_axi_awready  : out std_logic;
    s00_axi_wdata    : in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb    : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid   : in std_logic;
    s00_axi_wready   : out std_logic;
    s00_axi_bresp    : out std_logic_vector(1 downto 0);
    s00_axi_bvalid   : out std_logic;
    s00_axi_bready   : in std_logic;
    s00_axi_araddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot   : in std_logic_vector(2 downto 0);
    s00_axi_arvalid  : in std_logic;
    s00_axi_arready  : out std_logic;
    s00_axi_rdata    : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp    : out std_logic_vector(1 downto 0);
    s00_axi_rvalid   : out std_logic;
    s00_axi_rready   : in std_logic
  );
end axi_dcb_register_bank_v1_0;

architecture arch_imp of axi_dcb_register_bank_v1_0 is

  ---------------------------------------------------------------------------
  -- interface signal definitions
  ---------------------------------------------------------------------------
  signal user_to_ipif     : user_to_ipif_type;
  signal ipif_to_user     : ipif_to_user_type;

begin

  ------------------------------------------------------
  -- register definition is done in ipif_user_cfg.vhdl
  ------------------------------------------------------

  -- Register Bit <-> I/O Mapping

  -- Register 0 [0x0000]: HW_VER
  HW_VER_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_HW_VER);
  HW_VER_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_HW_VER);
  user_to_ipif.reg.data(C_REG_HW_VER)(31 downto 24)              <= x"AC";
  user_to_ipif.reg.data(C_REG_HW_VER)(23 downto 16)              <= x"01";
  user_to_ipif.reg.data(C_REG_HW_VER)(15 downto 8)               <= x"03";
  user_to_ipif.reg.data(C_REG_HW_VER)(7 downto 2)                <= "000001";
  user_to_ipif.reg.data(C_REG_HW_VER)(1 downto 0)                <= "11";

  -- Register 1 [0x0004]: REG_LAYOUT_VER
  REG_LAYOUT_VER_REG_RD_STROBE_O                                 <= ipif_to_user.reg.rd_strobe(C_REG_REG_LAYOUT_VER);
  REG_LAYOUT_VER_REG_WR_STROBE_O                                 <= ipif_to_user.reg.wr_strobe(C_REG_REG_LAYOUT_VER);
  user_to_ipif.reg.data(C_REG_REG_LAYOUT_VER)(31 downto 16)      <= x"0000";
  user_to_ipif.reg.data(C_REG_REG_LAYOUT_VER)(15 downto 0)       <= x"0000";

  -- Register 2 [0x0008]: FW_BUILD_DATE
  FW_BUILD_DATE_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_FW_BUILD_DATE);
  FW_BUILD_DATE_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_FW_BUILD_DATE);
  user_to_ipif.reg.data(C_REG_FW_BUILD_DATE)(31 downto 16)       <= FW_BUILD_YEAR_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_DATE)(15 downto 8)        <= FW_BUILD_MONTH_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_DATE)(7 downto 0)         <= FW_BUILD_DAY_I;

  -- Register 3 [0x000C]: FW_BUILD_TIME
  FW_BUILD_TIME_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_FW_BUILD_TIME);
  FW_BUILD_TIME_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_FW_BUILD_TIME);
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(31 downto 24)       <= x"00";
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(23 downto 16)       <= FW_BUILD_HOUR_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(15 downto 8)        <= FW_BUILD_MINUTE_I;
  user_to_ipif.reg.data(C_REG_FW_BUILD_TIME)(7 downto 0)         <= FW_BUILD_SECOND_I;

  -- Register 4 [0x0010]: SW_BUILD_DATE
  SW_BUILD_DATE_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_SW_BUILD_DATE);
  SW_BUILD_DATE_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_SW_BUILD_DATE);
  SW_BUILD_YEAR_O                                                <= ipif_to_user.reg.data(C_REG_SW_BUILD_DATE)(31 downto 16);
  SW_BUILD_MONTH_O                                               <= ipif_to_user.reg.data(C_REG_SW_BUILD_DATE)(15 downto 8);
  SW_BUILD_DAY_O                                                 <= ipif_to_user.reg.data(C_REG_SW_BUILD_DATE)(7 downto 0);

  -- Register 5 [0x0014]: SW_BUILD_TIME
  SW_BUILD_TIME_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_SW_BUILD_TIME);
  SW_BUILD_TIME_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_SW_BUILD_TIME);
  SW_BUILD_HOUR_O                                                <= ipif_to_user.reg.data(C_REG_SW_BUILD_TIME)(23 downto 16);
  SW_BUILD_MINUTE_O                                              <= ipif_to_user.reg.data(C_REG_SW_BUILD_TIME)(15 downto 8);
  SW_BUILD_SECOND_O                                              <= ipif_to_user.reg.data(C_REG_SW_BUILD_TIME)(7 downto 0);

  -- Register 6 [0x0018]: FW_GIT_HASH_TAG
  FW_GIT_HASH_TAG_REG_RD_STROBE_O                                <= ipif_to_user.reg.rd_strobe(C_REG_FW_GIT_HASH_TAG);
  FW_GIT_HASH_TAG_REG_WR_STROBE_O                                <= ipif_to_user.reg.wr_strobe(C_REG_FW_GIT_HASH_TAG);
  user_to_ipif.reg.data(C_REG_FW_GIT_HASH_TAG)                   <= FW_GIT_HASH_TAG_I;

  -- Register 7 [0x001C]: SW_GIT_HASH_TAG
  SW_GIT_HASH_TAG_REG_RD_STROBE_O                                <= ipif_to_user.reg.rd_strobe(C_REG_SW_GIT_HASH_TAG);
  SW_GIT_HASH_TAG_REG_WR_STROBE_O                                <= ipif_to_user.reg.wr_strobe(C_REG_SW_GIT_HASH_TAG);
  SW_GIT_HASH_TAG_O                                              <= ipif_to_user.reg.data(C_REG_SW_GIT_HASH_TAG);

  -- Register 8 [0x0020]: PROT_VER
  PROT_VER_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_PROT_VER);
  PROT_VER_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_PROT_VER);
  PROTOCOL_VERSION_O                                             <= ipif_to_user.reg.data(C_REG_PROT_VER)(7 downto 0);

  -- Register 9 [0x0024]: SN
  SN_REG_RD_STROBE_O                                             <= ipif_to_user.reg.rd_strobe(C_REG_SN);
  SN_REG_WR_STROBE_O                                             <= ipif_to_user.reg.wr_strobe(C_REG_SN);
  SERIAL_NUMBER_O                                                <= ipif_to_user.reg.data(C_REG_SN)(15 downto 0);

  -- Register 10 [0x0028]: STATUS
  STATUS_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_STATUS);
  STATUS_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_STATUS);
  user_to_ipif.reg.data(C_REG_STATUS)(5)                         <= FLASH_SEL_I;
  user_to_ipif.reg.data(C_REG_STATUS)(4)                         <= BOARD_SEL_I;
  user_to_ipif.reg.data(C_REG_STATUS)(2)                         <= SERIAL_BUSY_I;
  user_to_ipif.reg.data(C_REG_STATUS)(1)                         <= DCB_BUSY_I;
  user_to_ipif.reg.data(C_REG_STATUS)(0)                         <= SYS_BUSY_I;

  -- Register 11 [0x002C]: TEMP
  TEMP_REG_RD_STROBE_O                                           <= ipif_to_user.reg.rd_strobe(C_REG_TEMP);
  TEMP_REG_WR_STROBE_O                                           <= ipif_to_user.reg.wr_strobe(C_REG_TEMP);
  TEMPERATURE_O                                                  <= ipif_to_user.reg.data(C_REG_TEMP)(15 downto 0);

  -- Register 12 [0x0030]: PLL_LOCK
  PLL_LOCK_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_PLL_LOCK);
  PLL_LOCK_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_PLL_LOCK);
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(2)                       <= WDB_CLK_MGR_LOCK_I;
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(1)                       <= SYS_DCM_LOCK_I;
  user_to_ipif.reg.data(C_REG_PLL_LOCK)(0)                       <= LMK_PLL_LOCK_I;

  -- Register 13 [0x0034]: DCB_LOC
  DCB_LOC_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_DCB_LOC);
  DCB_LOC_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_DCB_LOC);
  CRATE_ID_O                                                     <= ipif_to_user.reg.data(C_REG_DCB_LOC)(23 downto 16);
  SLOT_ID_O                                                      <= ipif_to_user.reg.data(C_REG_DCB_LOC)(7 downto 0);

  -- Register 14 [0x0038]: CTRL
  CTRL_REG_RD_STROBE_O                                           <= ipif_to_user.reg.rd_strobe(C_REG_CTRL);
  CTRL_REG_WR_STROBE_O                                           <= ipif_to_user.reg.wr_strobe(C_REG_CTRL);
  SYNC_DELAY_O                                                   <= ipif_to_user.reg.data(C_REG_CTRL)(20 downto 16);
  DAQ_SOFT_TRIGGER_O                                             <= ipif_to_user.reg.data(C_REG_CTRL)(0);

  -- Register 15 [0x003C]: SET_CTRL
  SET_CTRL_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_SET_CTRL);
  SET_CTRL_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_SET_CTRL);
  SET_BIT_CTRL_O                                                 <= ipif_to_user.reg.data(C_REG_SET_CTRL);

  -- Register 16 [0x0040]: CLR_CTRL
  CLR_CTRL_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_CLR_CTRL);
  CLR_CTRL_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_CLR_CTRL);
  CLR_BIT_CTRL_O                                                 <= ipif_to_user.reg.data(C_REG_CLR_CTRL);

  -- Register 17 [0x0044]: CLK_CTRL
  CLK_CTRL_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_CLK_CTRL);
  CLK_CTRL_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_CLK_CTRL);
  DISTRIBUTOR_CLK_OUT_EN_O                                       <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(31 downto 12);
  DISTRIBUTOR_CLK_SRC_SEL_O                                      <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(3);
  BUS_CLK_SRC_SEL_O                                              <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(2);
  LMK_CLK_SRC_SEL_O                                              <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(1);
  EXT_CLK_IN_SEL_O                                               <= ipif_to_user.reg.data(C_REG_CLK_CTRL)(0);

  -- Register 18 [0x0048]: SET_CLK_CTRL
  SET_CLK_CTRL_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_SET_CLK_CTRL);
  SET_CLK_CTRL_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_SET_CLK_CTRL);
  SET_BIT_CLK_CTRL_O                                             <= ipif_to_user.reg.data(C_REG_SET_CLK_CTRL);

  -- Register 19 [0x004C]: CLR_CLK_CTRL
  CLR_CLK_CTRL_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_CLR_CLK_CTRL);
  CLR_CLK_CTRL_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_CLR_CLK_CTRL);
  CLR_BIT_CLK_CTRL_O                                             <= ipif_to_user.reg.data(C_REG_CLR_CLK_CTRL);

  -- Register 20 [0x0050]: COM_CTRL
  COM_CTRL_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_COM_CTRL);
  COM_CTRL_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_COM_CTRL);
  INTER_PKG_DELAY_O                                              <= ipif_to_user.reg.data(C_REG_COM_CTRL)(23 downto 0);

  -- Register 21 [0x0054]: RST
  RST_REG_RD_STROBE_O                                            <= ipif_to_user.reg.rd_strobe(C_REG_RST);
  RST_REG_WR_STROBE_O                                            <= ipif_to_user.reg.wr_strobe(C_REG_RST);
  WDB_CLK_MGR_RST_O                                              <= ipif_to_user.reg.data(C_REG_RST)(5);
  TRIGGER_MGR_RST_O                                              <= ipif_to_user.reg.data(C_REG_RST)(4);
  TR_SYNC_BPL_O                                                  <= ipif_to_user.reg.data(C_REG_RST)(3);
  LMK_SYNC_DCB_O                                                 <= ipif_to_user.reg.data(C_REG_RST)(2);
  DATA_LINK_IF_RST_O                                             <= ipif_to_user.reg.data(C_REG_RST)(1);
  RECONFIGURE_FPGA_O                                             <= ipif_to_user.reg.data(C_REG_RST)(0);

  -- Register 22 [0x0058]: APLY_CFG
  APLY_CFG_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_APLY_CFG);
  APLY_CFG_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_APLY_CFG);
  APPLY_SETTINGS_LMK_O                                           <= ipif_to_user.reg.data(C_REG_APLY_CFG)(0);

  -- Register 23 [0x005C]: LMK_0
  LMK_0_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_0);
  LMK_0_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_0);
  LMK0_RESET_O                                                   <= ipif_to_user.reg.data(C_REG_LMK_0)(31);
  LMK0_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_0)(18 downto 17);
  LMK0_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_0)(16);
  LMK0_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_0)(15 downto 8);
  LMK0_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_0)(7 downto 4);

  -- Register 24 [0x0060]: LMK_1
  LMK_1_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_1);
  LMK_1_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_1);
  LMK1_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_1)(18 downto 17);
  LMK1_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_1)(16);
  LMK1_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_1)(15 downto 8);
  LMK1_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_1)(7 downto 4);

  -- Register 25 [0x0064]: LMK_2
  LMK_2_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_2);
  LMK_2_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_2);
  LMK2_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_2)(18 downto 17);
  LMK2_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_2)(16);
  LMK2_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_2)(15 downto 8);
  LMK2_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_2)(7 downto 4);

  -- Register 26 [0x0068]: LMK_3
  LMK_3_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_3);
  LMK_3_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_3);
  LMK3_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_3)(18 downto 17);
  LMK3_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_3)(16);
  LMK3_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_3)(15 downto 8);
  LMK3_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_3)(7 downto 4);

  -- Register 27 [0x006C]: LMK_4
  LMK_4_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_4);
  LMK_4_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_4);
  LMK4_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_4)(18 downto 17);
  LMK4_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_4)(16);
  LMK4_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_4)(15 downto 8);
  LMK4_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_4)(7 downto 4);

  -- Register 28 [0x0070]: LMK_5
  LMK_5_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_5);
  LMK_5_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_5);
  LMK5_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_5)(18 downto 17);
  LMK5_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_5)(16);
  LMK5_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_5)(15 downto 8);
  LMK5_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_5)(7 downto 4);

  -- Register 29 [0x0074]: LMK_6
  LMK_6_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_6);
  LMK_6_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_6);
  LMK6_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_6)(18 downto 17);
  LMK6_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_6)(16);
  LMK6_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_6)(15 downto 8);
  LMK6_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_6)(7 downto 4);

  -- Register 30 [0x0078]: LMK_7
  LMK_7_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_7);
  LMK_7_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_7);
  LMK7_CLKOUT_MUX_O                                              <= ipif_to_user.reg.data(C_REG_LMK_7)(18 downto 17);
  LMK7_CLKOUT_EN_O                                               <= ipif_to_user.reg.data(C_REG_LMK_7)(16);
  LMK7_CLKOUT_DIV_O                                              <= ipif_to_user.reg.data(C_REG_LMK_7)(15 downto 8);
  LMK7_CLKOUT_DLY_O                                              <= ipif_to_user.reg.data(C_REG_LMK_7)(7 downto 4);

  -- Register 31 [0x007C]: LMK_8
  LMK_8_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_8);
  LMK_8_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_8);
  LMK8_PHASE_NOISE_OPT_O                                         <= ipif_to_user.reg.data(C_REG_LMK_8)(31 downto 4);

  -- Register 32 [0x0080]: LMK_9
  LMK_9_REG_RD_STROBE_O                                          <= ipif_to_user.reg.rd_strobe(C_REG_LMK_9);
  LMK_9_REG_WR_STROBE_O                                          <= ipif_to_user.reg.wr_strobe(C_REG_LMK_9);
  LMK9_VBOOST_O                                                  <= ipif_to_user.reg.data(C_REG_LMK_9)(16);

  -- Register 33 [0x0084]: LMK_11
  LMK_11_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_LMK_11);
  LMK_11_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_LMK_11);
  LMK11_DIV4_O                                                   <= ipif_to_user.reg.data(C_REG_LMK_11)(15);

  -- Register 34 [0x0088]: LMK_13
  LMK_13_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_LMK_13);
  LMK_13_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_LMK_13);
  LMK13_OSCIN_FREQ_O                                             <= ipif_to_user.reg.data(C_REG_LMK_13)(21 downto 14);
  LMK13_VCO_R4_LF_O                                              <= ipif_to_user.reg.data(C_REG_LMK_13)(13 downto 11);
  LMK13_VCO_R3_LF_O                                              <= ipif_to_user.reg.data(C_REG_LMK_13)(10 downto 8);
  LMK13_VCO_C3_C4_LF_O                                           <= ipif_to_user.reg.data(C_REG_LMK_13)(7 downto 4);

  -- Register 35 [0x008C]: LMK_14
  LMK_14_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_LMK_14);
  LMK_14_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_LMK_14);
  LMK14_EN_FOUT_O                                                <= ipif_to_user.reg.data(C_REG_LMK_14)(28);
  LMK14_EN_CLKOUT_GLOBAL_O                                       <= ipif_to_user.reg.data(C_REG_LMK_14)(27);
  LMK14_POWERDOWN_O                                              <= ipif_to_user.reg.data(C_REG_LMK_14)(26);
  LMK14_PLL_MUX_O                                                <= ipif_to_user.reg.data(C_REG_LMK_14)(23 downto 20);
  LMK14_PLL_R_O                                                  <= ipif_to_user.reg.data(C_REG_LMK_14)(19 downto 8);

  -- Register 36 [0x0090]: LMK_15
  LMK_15_REG_RD_STROBE_O                                         <= ipif_to_user.reg.rd_strobe(C_REG_LMK_15);
  LMK_15_REG_WR_STROBE_O                                         <= ipif_to_user.reg.wr_strobe(C_REG_LMK_15);
  LMK15_PLL_CP_GAIN_O                                            <= ipif_to_user.reg.data(C_REG_LMK_15)(31 downto 30);
  LMK15_VCO_DIV_O                                                <= ipif_to_user.reg.data(C_REG_LMK_15)(29 downto 26);
  LMK15_PLL_N_O                                                  <= ipif_to_user.reg.data(C_REG_LMK_15)(25 downto 8);

  -- Register 37 [0x0094]: TIME_LSB
  TIME_LSB_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_TIME_LSB);
  TIME_LSB_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_TIME_LSB);
  user_to_ipif.reg.data(C_REG_TIME_LSB)                          <= TIME_LSB_I;

  -- Register 38 [0x0098]: TIME_MSB
  TIME_MSB_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_TIME_MSB);
  TIME_MSB_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_TIME_MSB);
  user_to_ipif.reg.data(C_REG_TIME_MSB)                          <= TIME_MSB_I;

  -- Register 39 [0x009C]: TIME_LSB_SET
  TIME_LSB_SET_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_TIME_LSB_SET);
  TIME_LSB_SET_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_TIME_LSB_SET);
  TIME_LSB_SET_O                                                 <= ipif_to_user.reg.data(C_REG_TIME_LSB_SET);

  -- Register 40 [0x00A0]: TIME_MSB_SET
  TIME_MSB_SET_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_TIME_MSB_SET);
  TIME_MSB_SET_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_TIME_MSB_SET);
  TIME_MSB_SET_O                                                 <= ipif_to_user.reg.data(C_REG_TIME_MSB_SET);

  -- Register 41 [0x00A4]: EVENT_TX_RATE
  EVENT_TX_RATE_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_EVENT_TX_RATE);
  EVENT_TX_RATE_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_EVENT_TX_RATE);
  user_to_ipif.reg.data(C_REG_EVENT_TX_RATE)                     <= EVENT_TX_RATE_I;

  -- Register 42 [0x00A8]: EVENT_NR
  EVENT_NR_REG_RD_STROBE_O                                       <= ipif_to_user.reg.rd_strobe(C_REG_EVENT_NR);
  EVENT_NR_REG_WR_STROBE_O                                       <= ipif_to_user.reg.wr_strobe(C_REG_EVENT_NR);
  user_to_ipif.reg.data(C_REG_EVENT_NR)                          <= EVENT_NUMBER_I;

  -- Register 43 [0x00AC]: TRG_CFG
  TRG_CFG_REG_RD_STROBE_O                                        <= ipif_to_user.reg.rd_strobe(C_REG_TRG_CFG);
  TRG_CFG_REG_WR_STROBE_O                                        <= ipif_to_user.reg.wr_strobe(C_REG_TRG_CFG);
  EXT_TRIGGER_OUT_ENABLE_O                                       <= ipif_to_user.reg.data(C_REG_TRG_CFG)(2);
  ENABLE_AUTO_TRIGGER_O                                          <= ipif_to_user.reg.data(C_REG_TRG_CFG)(1);
  MANUAL_TRIGGER_O                                               <= ipif_to_user.reg.data(C_REG_TRG_CFG)(0);

  -- Register 44 [0x00B0]: SET_TRG_CFG
  SET_TRG_CFG_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_SET_TRG_CFG);
  SET_TRG_CFG_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_SET_TRG_CFG);
  SET_BIT_TRG_CFG_O                                              <= ipif_to_user.reg.data(C_REG_SET_TRG_CFG);

  -- Register 45 [0x00B4]: CLR_TRG_CFG
  CLR_TRG_CFG_REG_RD_STROBE_O                                    <= ipif_to_user.reg.rd_strobe(C_REG_CLR_TRG_CFG);
  CLR_TRG_CFG_REG_WR_STROBE_O                                    <= ipif_to_user.reg.wr_strobe(C_REG_CLR_TRG_CFG);
  CLR_BIT_TRG_CFG_O                                              <= ipif_to_user.reg.data(C_REG_CLR_TRG_CFG);

  -- Register 46 [0x00B8]: TRG_AUTO_PERIOD
  TRG_AUTO_PERIOD_REG_RD_STROBE_O                                <= ipif_to_user.reg.rd_strobe(C_REG_TRG_AUTO_PERIOD);
  TRG_AUTO_PERIOD_REG_WR_STROBE_O                                <= ipif_to_user.reg.wr_strobe(C_REG_TRG_AUTO_PERIOD);
  AUTO_TRIGGER_PERIOD_O                                          <= ipif_to_user.reg.data(C_REG_TRG_AUTO_PERIOD);

  -- Register 47 [0x00BC]: TRB_INFO_STAT
  TRB_INFO_STAT_REG_RD_STROBE_O                                  <= ipif_to_user.reg.rd_strobe(C_REG_TRB_INFO_STAT);
  TRB_INFO_STAT_REG_WR_STROBE_O                                  <= ipif_to_user.reg.wr_strobe(C_REG_TRB_INFO_STAT);
  user_to_ipif.reg.data(C_REG_TRB_INFO_STAT)(31)                 <= TRB_FLAG_NEW_I;
  user_to_ipif.reg.data(C_REG_TRB_INFO_STAT)(30)                 <= TRB_FLAG_PARITY_ERROR_I;
  user_to_ipif.reg.data(C_REG_TRB_INFO_STAT)(15 downto 0)        <= TRB_PARITY_ERROR_COUNT_I;

  -- Register 48 [0x00C0]: TRB_INFO_LSB
  TRB_INFO_LSB_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_TRB_INFO_LSB);
  TRB_INFO_LSB_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_TRB_INFO_LSB);
  user_to_ipif.reg.data(C_REG_TRB_INFO_LSB)                      <= TRB_INFO_LSB_I;

  -- Register 49 [0x00C4]: TRB_INFO_MSB
  TRB_INFO_MSB_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_TRB_INFO_MSB);
  TRB_INFO_MSB_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_TRB_INFO_MSB);
  user_to_ipif.reg.data(C_REG_TRB_INFO_MSB)(15 downto 0)         <= TRB_INFO_MSB_I;

  -- Register 50 [0x00C8]: LMK_MOD_FLAG
  LMK_MOD_FLAG_REG_RD_STROBE_O                                   <= ipif_to_user.reg.rd_strobe(C_REG_LMK_MOD_FLAG);
  LMK_MOD_FLAG_REG_WR_STROBE_O                                   <= ipif_to_user.reg.wr_strobe(C_REG_LMK_MOD_FLAG);
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(7)                   <= LMK_7_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(6)                   <= LMK_6_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(5)                   <= LMK_5_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(4)                   <= LMK_4_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(3)                   <= LMK_3_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(2)                   <= LMK_2_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(1)                   <= LMK_1_MOD_I;
  user_to_ipif.reg.data(C_REG_LMK_MOD_FLAG)(0)                   <= LMK_0_MOD_I;

  -- Register 51 [0x00CC]: CRC32_REG_BANK
  CRC32_REG_BANK_REG_RD_STROBE_O                                 <= ipif_to_user.reg.rd_strobe(C_REG_CRC32_REG_BANK);
  CRC32_REG_BANK_REG_WR_STROBE_O                                 <= ipif_to_user.reg.wr_strobe(C_REG_CRC32_REG_BANK);
  CRC32_REG_BANK_O                                               <= ipif_to_user.reg.data(C_REG_CRC32_REG_BANK);

--  ------------------------------------------------------
--  -- memory definition is done in ipif_user_cfg.vhdl
--  ------------------------------------------------------
--
--  user_to_ipif.mem.addr   <= core_mem_address;
--  user_to_ipif.mem.enable <= '1';
--  user_to_ipif.mem.wr_en  <= '0';
--  user_to_ipif.mem.data   <= (others => '0');

  ------------------------------------------------------------------------------
  -- instantiate AXI IP Interface
  ------------------------------------------------------------------------------

  ipif_axi_inst : entity axi_dcb_reg_bank_v1_0.ipif_axi
  generic map
  (
    -- Bus protocol parameters, do not add to or delete
    C_S_AXI_DATA_WIDTH  => C_S00_AXI_DATA_WIDTH,
    C_S_AXI_ADDR_WIDTH  => C_S00_AXI_ADDR_WIDTH,
    C_S_AXI_MIN_SIZE    => C_S00_AXI_MIN_SIZE,
    C_USE_WSTRB         => C_USE_WSTRB,
    C_BASEADDR          => C_S00_AXI_BASEADDR,
    C_HIGHADDR          => C_S00_AXI_HIGHADDR,
    C_SLV_AWIDTH        => C_S00_AXI_ADDR_WIDTH,
    C_SLV_DWIDTH        => C_S00_AXI_DATA_WIDTH,
    C_DPHASE_TIMEOUT    => C_DPHASE_TIMEOUT,
    C_FAMILY            => C_FAMILY
    --C_MEM0_BASEADDR     => C_MEM0_BASEADDR,
    --C_MEM0_HIGHADDR     => C_MEM0_HIGHADDR,
    --C_MEM1_BASEADDR     => C_MEM1_BASEADDR,
    --C_MEM1_HIGHADDR     => C_MEM1_HIGHADDR
  )
  port map
  (
    -- user ports for ip interface
    USER_TO_IPIF_I        => user_to_ipif,
    IPIF_TO_USER_O        => ipif_to_user,
    -- Bus protocol ports, do not add to or delete
    S_AXI_ACLK            => s00_axi_aclk,
    S_AXI_ARESETN         => s00_axi_aresetn,
    S_AXI_AWADDR          => s00_axi_awaddr,
    S_AXI_AWVALID         => s00_axi_awvalid,
    S_AXI_WDATA           => s00_axi_wdata,
    S_AXI_WSTRB           => s00_axi_wstrb,
    S_AXI_WVALID          => s00_axi_wvalid,
    S_AXI_BREADY          => s00_axi_bready,
    S_AXI_ARADDR          => s00_axi_araddr,
    S_AXI_ARVALID         => s00_axi_arvalid,
    S_AXI_RREADY          => s00_axi_rready,
    S_AXI_ARREADY         => s00_axi_arready,
    S_AXI_RDATA           => s00_axi_rdata,
    S_AXI_RRESP           => s00_axi_rresp,
    S_AXI_RVALID          => s00_axi_rvalid,
    S_AXI_WREADY          => s00_axi_wready,
    S_AXI_BRESP           => s00_axi_bresp,
    S_AXI_BVALID          => s00_axi_bvalid,
    S_AXI_AWREADY         => s00_axi_awready
  );

  -- Check if necessary:
  -- S_AXI_AWPROT  => s00_axi_awprot,
  -- S_AXI_ARPROT  => s00_axi_arprot,

end arch_imp;
