# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "CGN_DATA_WIDTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_TRISTATE_POLARITY" -parent ${Page_0}


}

proc update_PARAM_VALUE.CGN_DATA_WIDTH { PARAM_VALUE.CGN_DATA_WIDTH } {
	# Procedure called to update CGN_DATA_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_DATA_WIDTH { PARAM_VALUE.CGN_DATA_WIDTH } {
	# Procedure called to validate CGN_DATA_WIDTH
	return true
}

proc update_PARAM_VALUE.CGN_TRISTATE_POLARITY { PARAM_VALUE.CGN_TRISTATE_POLARITY } {
	# Procedure called to update CGN_TRISTATE_POLARITY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_TRISTATE_POLARITY { PARAM_VALUE.CGN_TRISTATE_POLARITY } {
	# Procedure called to validate CGN_TRISTATE_POLARITY
	return true
}


proc update_MODELPARAM_VALUE.CGN_TRISTATE_POLARITY { MODELPARAM_VALUE.CGN_TRISTATE_POLARITY PARAM_VALUE.CGN_TRISTATE_POLARITY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_TRISTATE_POLARITY}] ${MODELPARAM_VALUE.CGN_TRISTATE_POLARITY}
}

proc update_MODELPARAM_VALUE.CGN_DATA_WIDTH { MODELPARAM_VALUE.CGN_DATA_WIDTH PARAM_VALUE.CGN_DATA_WIDTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_DATA_WIDTH}] ${MODELPARAM_VALUE.CGN_DATA_WIDTH}
}

