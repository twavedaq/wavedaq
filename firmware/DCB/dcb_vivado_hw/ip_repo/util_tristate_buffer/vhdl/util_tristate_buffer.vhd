---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  IO Buffer
--
--  Project :  MEG
--
--  PCB  :  Data Concentrator Board
--  Part :  Xilinx Zynq XC7Z030-1FBG676C
--
--  Tool Version :  2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  13.11.2018 12:10:45
--
--  Description :  Generic tristate buffer to be insertet in vivado designs.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity util_tristate_buffer is
  generic (
    CGN_TRISTATE_POLARITY : integer   := 1; -- 1 = highlevel tristate, 0 = lowlevel tristate
    CGN_DATA_WIDTH        : integer   := 8
  );
  port (
    BUFFER_IO  : inout std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    INPUT_O    : out   std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    OUTPUT_I   : in    std_logic_vector(CGN_DATA_WIDTH-1 downto 0);
    TRISTATE_I : in    std_logic_vector(CGN_DATA_WIDTH-1 downto 0)
  );
end util_tristate_buffer;

architecture behavioral of util_tristate_buffer is


begin

  standard_tristate_buf_array : if CGN_TRISTATE_POLARITY = 0 generate
		standard_tristate_buf : for i in CGN_DATA_WIDTH-1 downto 0 generate
			BUFFER_IO(i) <= 'Z' when TRISTATE_I(i) = '0' else OUTPUT_I(i);
		end generate standard_tristate_buf;
	end generate standard_tristate_buf_array;

  inverted_tristate_buf_array : if CGN_TRISTATE_POLARITY = 1 generate
		inverted_tristate_buf : for i in CGN_DATA_WIDTH-1 downto 0 generate
		  BUFFER_IO(i) <= 'Z' when TRISTATE_I(i) = '1' else OUTPUT_I(i);
	  end generate inverted_tristate_buf;
  end generate inverted_tristate_buf_array;

  INPUT_O  <= BUFFER_IO;

end architecture behavioral;