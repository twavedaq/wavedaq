---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  LED Controler
--
--  Project :  MEG DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ XC7Z030-1FBG676C
--
--  Tool Version :  Vivado 2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  24.08.2018 12:43:00
--
--  Description :  Controls the behavior of the RGB LED on the front panel of the
--                 WaveDream2 board.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_arith.all;
use ieee.std_logic_unsigned.all;

library psi_3205_v1_00_a;
use psi_3205_v1_00_a.cdc_package.all;

entity led_ctrl is
  generic (
    CGN_ACTIVE_HIGH_HW_ERRORS : integer := 1;
    CGN_ACTIVE_LOW_HW_ERRORS  : integer := 9;
    CGN_CLK_PERIOD            : integer := 20000; -- ps
    CGN_MIN_STEADY_TIME       : integer := 50000000; -- ns
    CGN_BLINK_TIME            : integer := 500000000 -- ns
  );
  port (
    -- LED outputs
    LED_R_N_O    : out std_logic;
    LED_G_N_O    : out std_logic;
    LED_B_N_O    : out std_logic;
    -- HW state indicator
    HW_ERROR_I   : in  std_logic_vector(CGN_ACTIVE_HIGH_HW_ERRORS-1 downto 0);
    HW_ERROR_N_I : in  std_logic_vector(CGN_ACTIVE_LOW_HW_ERRORS-1  downto 0);
    HW_STATUS_I  : in  std_logic_vector(0 downto 0);
    -- SW state indicator
    SW_STATUS_I  : in  std_logic_vector(7 downto 0);
    -- clock
    LED_CLK_I    : in  std_logic
  );
end led_ctrl;

architecture behavioral of led_ctrl is

  constant C_HOLD_COUNTER_INIT     : integer := INTEGER(real(CGN_MIN_STEADY_TIME)/(real(CGN_CLK_PERIOD)/1000.0));
  constant C_HOLD_COUNTER_WIDTH    : integer := log2ceil(C_HOLD_COUNTER_INIT);
  constant C_HOLD_COUNTER_INIT_LV  : std_logic_vector := CONV_STD_LOGIC_VECTOR(C_HOLD_COUNTER_INIT, C_HOLD_COUNTER_WIDTH);
  constant C_BLINK_COUNTER_INIT    : integer := INTEGER(real(CGN_BLINK_TIME)/(real(CGN_CLK_PERIOD)/1000.0));
  constant C_BLINK_COUNTER_WIDTH   : integer := log2ceil(C_BLINK_COUNTER_INIT);
  constant C_BLINK_COUNTER_INIT_LV : std_logic_vector := CONV_STD_LOGIC_VECTOR(C_BLINK_COUNTER_INIT, C_BLINK_COUNTER_WIDTH);
  constant C_NO_HW_ERROR   : std_logic_vector(CGN_ACTIVE_HIGH_HW_ERRORS-1 downto 0) := (others=>'0');
  constant C_NO_HW_ERROR_N : std_logic_vector(CGN_ACTIVE_LOW_HW_ERRORS-1  downto 0) := (others=>'1');

  type rgb_record is
    record
      red   : std_logic;
      green : std_logic;
      blue  : std_logic;
    end record;

  constant C_ON      : std_logic  := '0';
  constant C_OFF     : std_logic  := '1';
  constant C_NONE    : rgb_record := (red=>C_OFF, green=>C_OFF, blue=>C_OFF);
  constant C_RED     : rgb_record := (red=>C_ON,  green=>C_OFF, blue=>C_OFF);
  constant C_GREEN   : rgb_record := (red=>C_OFF, green=>C_ON,  blue=>C_OFF);
  constant C_BLUE    : rgb_record := (red=>C_OFF, green=>C_OFF, blue=>C_ON );
  constant C_MAGENTA : rgb_record := (red=>C_ON,  green=>C_OFF, blue=>C_ON );
  constant C_YELLOW  : rgb_record := (red=>C_ON,  green=>C_ON,  blue=>C_OFF);
  constant C_CYAN    : rgb_record := (red=>C_OFF, green=>C_ON,  blue=>C_ON );
  constant C_WHITE   : rgb_record := (red=>C_ON,  green=>C_ON,  blue=>C_ON );

  -- SW status inidices
  constant C_SW_ERROR_IDX     : integer := 7;
  constant C_SW_MARKER_IDX    : integer := 6;
  constant C_SW_DHCP_REQ      : integer := 5;
  constant C_SW_BL_FAIL_IDX   : integer := 4;
  constant C_SW_READY_IDX     : integer := 3;
  constant C_SW_FW_UPDATE_IDX : integer := 2;
  constant C_SW_UPDATE_IDX    : integer := 1;
  constant C_SW_WDB_ACCESS    : integer := 0;
  -- FW status inidices
  constant C_HW_BUSY_IDX      : integer := 0;

  signal color               : rgb_record := C_NONE;
  signal on_beat_color       : rgb_record := C_NONE;
  signal off_beat_color      : rgb_record := C_NONE;
  signal prev_on_beat_color  : rgb_record := C_NONE;
  signal prev_off_beat_color : rgb_record := C_NONE;

  signal hold_count    : std_logic_vector(C_HOLD_COUNTER_WIDTH-1 downto 0)  := (others=>'0');
  signal blink_count   : std_logic_vector(C_BLINK_COUNTER_WIDTH-1 downto 0) := (others=>'0');

  signal color_changed : std_logic := '0';

  signal beat          : std_logic := '0';

begin

  -- output assignments
  LED_R_N_O <= color.red;
  LED_G_N_O <= color.green;
  LED_B_N_O <= color.blue;

  -- feel the beat
  color <= on_beat_color when beat = '0' else off_beat_color;

  counter : process(LED_CLK_I)
  begin
    if rising_edge(LED_CLK_I) then
      -- color hold counter
      if color_changed = '1' then
        hold_count <= C_HOLD_COUNTER_INIT_LV;
      elsif hold_count > 0 then
        hold_count <= hold_count - 1;
      end if;
      -- blink counter
      if blink_count = 0 then
        blink_count <= C_BLINK_COUNTER_INIT_LV;
        beat <= not beat;
      else
        blink_count <= blink_count - 1;
      end if;
    end if;
  end process;

  -- color selection logic
  color_select : process(LED_CLK_I)
  begin
    if rising_edge(LED_CLK_I) then
      color_changed <= '0';
      if hold_count = 0 then
        if (prev_on_beat_color /= on_beat_color) or (prev_off_beat_color /= off_beat_color) then
          color_changed <= '1';
        end if;
        prev_on_beat_color  <= on_beat_color;
        prev_off_beat_color <= off_beat_color;
        if SW_STATUS_I(C_SW_BL_FAIL_IDX) = '1' then
          on_beat_color  <= C_YELLOW;
          off_beat_color <= C_NONE;
        elsif (HW_ERROR_I /= C_NO_HW_ERROR) or (HW_ERROR_N_I /= C_NO_HW_ERROR_N) then
          on_beat_color  <= C_RED;
          off_beat_color <= C_RED;
        elsif SW_STATUS_I(C_SW_ERROR_IDX) = '1' then
          on_beat_color  <= C_RED;
          off_beat_color <= C_NONE;
        elsif SW_STATUS_I(C_SW_MARKER_IDX) = '1' then
          on_beat_color  <= C_MAGENTA;
          off_beat_color <= C_NONE;
        elsif SW_STATUS_I(C_SW_DHCP_REQ) = '1' then
          on_beat_color  <= C_CYAN;
          off_beat_color <= C_NONE;
        elsif HW_STATUS_I(C_HW_BUSY_IDX) = '0' then
          on_beat_color  <= C_BLUE;
          off_beat_color <= C_BLUE;
        elsif (SW_STATUS_I(C_SW_FW_UPDATE_IDX) = '1') or (SW_STATUS_I(C_SW_UPDATE_IDX)) = '1' then
          on_beat_color  <= C_YELLOW;
          off_beat_color <= C_GREEN;
        elsif SW_STATUS_I(C_SW_WDB_ACCESS) = '1' then
          on_beat_color  <= C_MAGENTA;
          off_beat_color <= C_MAGENTA;
        elsif SW_STATUS_I(C_SW_READY_IDX) = '1' then
          on_beat_color  <= C_GREEN;
          off_beat_color <= C_GREEN;
        else
          -- booting state
          on_beat_color  <= C_YELLOW;
          off_beat_color <= C_YELLOW;
        end if;
      end if;
    end if;
  end process;

end architecture behavioral;
