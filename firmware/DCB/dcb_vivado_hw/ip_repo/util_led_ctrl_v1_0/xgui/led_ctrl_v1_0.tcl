# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "CGN_ACTIVE_HIGH_HW_ERRORS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CGN_ACTIVE_LOW_HW_ERRORS" -parent ${Page_0}
  set CGN_BLINK_TIME [ipgui::add_param $IPINST -name "CGN_BLINK_TIME" -parent ${Page_0}]
  set_property tooltip {LED blink time in [ns]} ${CGN_BLINK_TIME}
  set CGN_CLK_PERIOD [ipgui::add_param $IPINST -name "CGN_CLK_PERIOD" -parent ${Page_0}]
  set_property tooltip {Clock period in [ps]} ${CGN_CLK_PERIOD}
  set CGN_MIN_STEADY_TIME [ipgui::add_param $IPINST -name "CGN_MIN_STEADY_TIME" -parent ${Page_0}]
  set_property tooltip {Minimum LED steady time in [ns]} ${CGN_MIN_STEADY_TIME}


}

proc update_PARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS { PARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS } {
	# Procedure called to update CGN_ACTIVE_HIGH_HW_ERRORS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS { PARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS } {
	# Procedure called to validate CGN_ACTIVE_HIGH_HW_ERRORS
	return true
}

proc update_PARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS { PARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS } {
	# Procedure called to update CGN_ACTIVE_LOW_HW_ERRORS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS { PARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS } {
	# Procedure called to validate CGN_ACTIVE_LOW_HW_ERRORS
	return true
}

proc update_PARAM_VALUE.CGN_BLINK_TIME { PARAM_VALUE.CGN_BLINK_TIME } {
	# Procedure called to update CGN_BLINK_TIME when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_BLINK_TIME { PARAM_VALUE.CGN_BLINK_TIME } {
	# Procedure called to validate CGN_BLINK_TIME
	return true
}

proc update_PARAM_VALUE.CGN_CLK_PERIOD { PARAM_VALUE.CGN_CLK_PERIOD } {
	# Procedure called to update CGN_CLK_PERIOD when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_CLK_PERIOD { PARAM_VALUE.CGN_CLK_PERIOD } {
	# Procedure called to validate CGN_CLK_PERIOD
	return true
}

proc update_PARAM_VALUE.CGN_MIN_STEADY_TIME { PARAM_VALUE.CGN_MIN_STEADY_TIME } {
	# Procedure called to update CGN_MIN_STEADY_TIME when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CGN_MIN_STEADY_TIME { PARAM_VALUE.CGN_MIN_STEADY_TIME } {
	# Procedure called to validate CGN_MIN_STEADY_TIME
	return true
}


proc update_MODELPARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS { MODELPARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS PARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS}] ${MODELPARAM_VALUE.CGN_ACTIVE_HIGH_HW_ERRORS}
}

proc update_MODELPARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS { MODELPARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS PARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS}] ${MODELPARAM_VALUE.CGN_ACTIVE_LOW_HW_ERRORS}
}

proc update_MODELPARAM_VALUE.CGN_CLK_PERIOD { MODELPARAM_VALUE.CGN_CLK_PERIOD PARAM_VALUE.CGN_CLK_PERIOD } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_CLK_PERIOD}] ${MODELPARAM_VALUE.CGN_CLK_PERIOD}
}

proc update_MODELPARAM_VALUE.CGN_MIN_STEADY_TIME { MODELPARAM_VALUE.CGN_MIN_STEADY_TIME PARAM_VALUE.CGN_MIN_STEADY_TIME } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_MIN_STEADY_TIME}] ${MODELPARAM_VALUE.CGN_MIN_STEADY_TIME}
}

proc update_MODELPARAM_VALUE.CGN_BLINK_TIME { MODELPARAM_VALUE.CGN_BLINK_TIME PARAM_VALUE.CGN_BLINK_TIME } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CGN_BLINK_TIME}] ${MODELPARAM_VALUE.CGN_BLINK_TIME}
}

