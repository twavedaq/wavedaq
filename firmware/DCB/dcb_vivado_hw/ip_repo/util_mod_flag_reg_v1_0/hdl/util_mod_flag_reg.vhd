---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  Modified Flag Register
--
--  Project :  WaveDream2
--
--  PCB  :  -
--  Part :  Xilinx Spartan6 XC6SLX100-3FGG484C
--
--  Tool Version :  14.7 (Version the code was testet with)
--
--  Author  :  Elmar Schmid (Author of generation script)
--  Created :  21.08.2014 13:28:06
--
--  Description :  Generic registers to store the information if specific bytes of
--                 a register have changed.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;

entity util_mod_flag_reg is
  generic (
    CGN_REGISTER_WIDTH : integer := 4
  );
  port (
    MODIFIED_I      : in  std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0);
    MODIFIED_FLAG_O : out std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0);
    RESET_I         : in  std_logic;
    CLK_I           : in  std_logic
  );
end util_mod_flag_reg;

architecture behavioral of util_mod_flag_reg is

  signal modified_flag : std_logic_vector(CGN_REGISTER_WIDTH-1 downto 0) := (others=>'0');

begin

  MODIFIED_FLAG_O <= modified_flag;

  process(CLK_I)
  begin
    if rising_edge(CLK_I) then
      if RESET_I = '1' then
        modified_flag <= (others=>'0');
      else
        modified_flag <= modified_flag or MODIFIED_I;
      end if;
    end if;
  end process;

end architecture behavioral;