---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  BUSY Manager
--
--  Project :  MEG DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ XC7Z030-1FBG676C
--
--  Tool Version :  Vivado 2017.4 (Version the code was testet with)
--
--  Author  :  Elmar Schmid
--  Created :  07.03.2019 15:30:00
--
--  Description :  Receives BUSY inputs from the backplane, does a logic connection
--                 to the local (DCB) BUSY and drives it to the corresponding output.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
Library UNISIM;
use UNISIM.vcomponents.all;

entity busy_manager is
  port (
    -- Internal
    --BUSY_DCB_I        : in  std_logic;
    -- Backplane
    BUSY_FROM_BPL_N_I : in  std_logic;
    BUSY_TO_BPL_N_O   : out std_logic;
    -- Front Panel Cable
    BUSY_TO_FCI_N_P_O : out std_logic;
    BUSY_TO_FCI_N_N_O : out std_logic
  );
end busy_manager;

architecture behavioral of busy_manager is

  signal busy : std_logic := '0';

begin

  busy <= BUSY_FROM_BPL_N_I;

  OBUFDS_BUSY_FCI_inst : OBUFDS
  generic map (
    IOSTANDARD => "DEFAULT",
    SLEW       => "SLOW"
  )
  port map (
    O  => BUSY_TO_FCI_N_P_O,
    OB => BUSY_TO_FCI_N_N_O,
    I  => busy
  );

  BUSY_TO_BPL_N_O <= '1';

end architecture behavioral;
