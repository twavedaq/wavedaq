
################################################################
# This is a generated script based on design: dcb
#
# Though there are limitations about the generated script,
# the main purpose of this utility is to make learning
# IP Integrator Tcl commands easier.
################################################################

namespace eval _tcl {
proc get_script_folder {} {
   set script_path [file normalize [info script]]
   set script_folder [file dirname $script_path]
   return $script_folder
}
}
variable script_folder
set script_folder [_tcl::get_script_folder]

################################################################
# Check if script is running in correct Vivado version.
################################################################
set scripts_vivado_version 2017.4
set current_vivado_version [version -short]

if { [string first $scripts_vivado_version $current_vivado_version] == -1 } {
   puts ""
   common::send_msg_id "BD_TCL-1002" "WARNING" "This script was generated using Vivado <$scripts_vivado_version> without IP versions in the create_bd_cell commands, but is now being run in <$current_vivado_version> of Vivado. There may have been major IP version changes between Vivado <$scripts_vivado_version> and <$current_vivado_version>, which could impact the parameter settings of the IPs."

}

################################################################
# START
################################################################

# To test this script, run the following commands from Vivado Tcl console:
# source dcb_script.tcl

# If there is no project opened, this script will create a
# project, but make sure you do not have an existing project
# <./myproj/project_1.xpr> in the current working folder.

set list_projs [get_projects -quiet]
if { $list_projs eq "" } {
   create_project project_1 myproj -part xc7z030fbg676-1
}


# CHANGE DESIGN NAME HERE
variable design_name
set design_name dcb

# This script was generated for a remote BD. To create a non-remote design,
# change the variable <run_remote_bd_flow> to <0>.

set run_remote_bd_flow 1
if { $run_remote_bd_flow == 1 } {
  # Set the reference directory for source file relative paths (by default 
  # the value is script directory path)
  set origin_dir ./bd

  # Use origin directory path location variable, if specified in the tcl shell
  if { [info exists ::origin_dir_loc] } {
     set origin_dir $::origin_dir_loc
  }

  set str_bd_folder [file normalize ${origin_dir}]
  set str_bd_filepath ${str_bd_folder}/${design_name}/${design_name}.bd

  # Check if remote design exists on disk
  if { [file exists $str_bd_filepath ] == 1 } {
     catch {common::send_msg_id "BD_TCL-110" "ERROR" "The remote BD file path <$str_bd_filepath> already exists!"}
     common::send_msg_id "BD_TCL-008" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0>."
     common::send_msg_id "BD_TCL-009" "INFO" "Also make sure there is no design <$design_name> existing in your current project."

     return 1
  }

  # Check if design exists in memory
  set list_existing_designs [get_bd_designs -quiet $design_name]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-111" "ERROR" "The design <$design_name> already exists in this project! Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-010" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
  }

  # Check if design exists on disk within project
  set list_existing_designs [get_files -quiet */${design_name}.bd]
  if { $list_existing_designs ne "" } {
     catch {common::send_msg_id "BD_TCL-112" "ERROR" "The design <$design_name> already exists in this project at location:
    $list_existing_designs"}
     catch {common::send_msg_id "BD_TCL-113" "ERROR" "Will not create the remote BD <$design_name> at the folder <$str_bd_folder>."}

     common::send_msg_id "BD_TCL-011" "INFO" "To create a non-remote BD, change the variable <run_remote_bd_flow> to <0> or please set a different value to variable <design_name>."

     return 1
}

  # Now can create the remote BD
  # NOTE - usage of <-dir> will create <$str_bd_folder/$design_name/$design_name.bd>
  create_bd_design -dir $str_bd_folder $design_name
} else {

  # Create regular design
  if { [catch {create_bd_design $design_name} errmsg] } {
     common::send_msg_id "BD_TCL-012" "INFO" "Please set a different value to variable <design_name>."

     return 1
}
}

current_bd_design $design_name

set bCheckIPsPassed 1
##################################################################
# CHECK IPs
##################################################################
set bCheckIPs 1
if { $bCheckIPs == 1 } {
   set list_check_ips "\ 
xilinx.com:ip:axi_quad_spi:*\
PSI:psi_3205:busy_manager:*\
xilinx.com:ip:clk_wiz:*\
psi.ch:PSI:clock_measure:*\
xilinx.com:ip:xlconstant:*\
xilinx.com:ip:gig_ethernet_pcs_pma:*\
PSI:psi_3205:led_ctrl:*\
xilinx.com:ip:processing_system7:*\
PSI:psi_3205:reset_generator:*\
PSI:psi_3205:sc_io:*\
PSI:psi_3205:tr_sync_manager:*\
PSI:psi_3205:trigger_manager:*\
xilinx.com:ip:util_ds_buf:*\
xilinx.com:ip:xlconcat:*\
xilinx.com:ip:xlslice:*\
PSI:psi_3205:axi_dcb_register_bank:*\
PSI:psi_3205:util_mod_flag_reg:*\
"

   set list_ips_missing ""
   common::send_msg_id "BD_TCL-006" "INFO" "Checking if the following IPs exist in the project's IP catalog: $list_check_ips ."

   foreach ip_vlnv $list_check_ips {
      set ip_obj [get_ipdefs -all $ip_vlnv]
      if { $ip_obj eq "" } {
         lappend list_ips_missing $ip_vlnv
      }
   }

   if { $list_ips_missing ne "" } {
      catch {common::send_msg_id "BD_TCL-115" "ERROR" "The following IPs are not found in the IP Catalog:\n  $list_ips_missing\n\nResolution: Please add the repository containing the IP(s) to the project." }
      set bCheckIPsPassed 0
   }

}

if { $bCheckIPsPassed != 1 } {
  common::send_msg_id "BD_TCL-1003" "WARNING" "Will not continue with creation of design due to the error(s) above."
  return 3
}

##################################################################
# DESIGN PROCs
##################################################################


# Hierarchical cell: register_bank_0
proc create_hier_cell_register_bank_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_register_bank_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins
  create_bd_intf_pin -mode Slave -vlnv xilinx.com:interface:aximm_rtl:1.0 S00_AXI

  # Create pins
  create_bd_pin -dir I BOARD_SELECT_N_I
  create_bd_pin -dir I BUSY_IN_N_I
  create_bd_pin -dir O BUS_CLK_SEL_O
  create_bd_pin -dir O CLK_SEL_EXT_O
  create_bd_pin -dir O CLK_SEL_O
  create_bd_pin -dir O DAQ_SOFT_TRIGGER_O
  create_bd_pin -dir I -from 47 -to 0 Din1
  create_bd_pin -dir I FLASH_SELECT_N_I
  create_bd_pin -dir I LD_I
  create_bd_pin -dir O MOSI_DIS_2_O
  create_bd_pin -dir O RECONFIGURE_FPGA_O
  create_bd_pin -dir O -from 4 -to 0 SYNC_DELAY_O
  create_bd_pin -dir I TRB_FLAG_NEW_I
  create_bd_pin -dir I TRB_FLAG_PARITY_ERROR_I
  create_bd_pin -dir I -from 15 -to 0 TRB_PARITY_ERROR_COUNT_I
  create_bd_pin -dir O TRIGGER_MGR_RST_O
  create_bd_pin -dir O TR_SYNC_BPL_O
  create_bd_pin -dir I WDB_CLK_MGR_LOCK_I
  create_bd_pin -dir O WDB_CLK_MGR_RST_O
  create_bd_pin -dir I -type clk s00_axi_aclk
  create_bd_pin -dir I -type rst s00_axi_aresetn

  # Create instance: axi_dcb_register_bank_0, and set properties
  set axi_dcb_register_bank_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:axi_dcb_register_bank axi_dcb_register_bank_0 ]

  # Create instance: util_mod_flag_lmk_ch, and set properties
  set util_mod_flag_lmk_ch [ create_bd_cell -type ip -vlnv PSI:psi_3205:util_mod_flag_reg util_mod_flag_lmk_ch ]
  set_property -dict [ list \
   CONFIG.CGN_REGISTER_WIDTH {8} \
 ] $util_mod_flag_lmk_ch

  # Create instance: xlconcat_lmk_ch_mod, and set properties
  set xlconcat_lmk_ch_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_lmk_ch_mod ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {8} \
 ] $xlconcat_lmk_ch_mod

  # Create instance: xlslice_lmk0_mod, and set properties
  set xlslice_lmk0_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk0_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {0} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk0_mod

  # Create instance: xlslice_lmk1_mod, and set properties
  set xlslice_lmk1_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk1_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {1} \
   CONFIG.DIN_TO {1} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk1_mod

  # Create instance: xlslice_lmk2_mod, and set properties
  set xlslice_lmk2_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk2_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {2} \
   CONFIG.DIN_TO {2} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk2_mod

  # Create instance: xlslice_lmk3_mod, and set properties
  set xlslice_lmk3_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk3_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {3} \
   CONFIG.DIN_TO {3} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk3_mod

  # Create instance: xlslice_lmk4_mod, and set properties
  set xlslice_lmk4_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk4_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {4} \
   CONFIG.DIN_TO {4} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk4_mod

  # Create instance: xlslice_lmk5_mod, and set properties
  set xlslice_lmk5_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk5_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {5} \
   CONFIG.DIN_TO {5} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk5_mod

  # Create instance: xlslice_lmk6_mod, and set properties
  set xlslice_lmk6_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk6_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {6} \
   CONFIG.DIN_TO {6} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk6_mod

  # Create instance: xlslice_lmk7_mod, and set properties
  set xlslice_lmk7_mod [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_lmk7_mod ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {7} \
   CONFIG.DIN_TO {7} \
   CONFIG.DIN_WIDTH {8} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_lmk7_mod

  # Create instance: xlslice_trb_info_lsbs, and set properties
  set xlslice_trb_info_lsbs [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_trb_info_lsbs ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {31} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {48} \
   CONFIG.DOUT_WIDTH {32} \
 ] $xlslice_trb_info_lsbs

  # Create instance: xlslice_trb_info_msbs, and set properties
  set xlslice_trb_info_msbs [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_trb_info_msbs ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {47} \
   CONFIG.DIN_TO {32} \
   CONFIG.DIN_WIDTH {48} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_trb_info_msbs

  # Create interface connections
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins S00_AXI] [get_bd_intf_pins axi_dcb_register_bank_0/S00_AXI]

  # Create port connections
  connect_bd_net -net BOARD_SELECT_N_I_1 [get_bd_pins BOARD_SELECT_N_I] [get_bd_pins axi_dcb_register_bank_0/BOARD_SEL_I]
  connect_bd_net -net BUSY_IN_N_I_1 [get_bd_pins BUSY_IN_N_I] [get_bd_pins axi_dcb_register_bank_0/SYS_BUSY_I]
  connect_bd_net -net FLASH_SELECT_N_I_1 [get_bd_pins FLASH_SELECT_N_I] [get_bd_pins axi_dcb_register_bank_0/FLASH_SEL_I]
  connect_bd_net -net LD_I_1 [get_bd_pins LD_I] [get_bd_pins axi_dcb_register_bank_0/LMK_PLL_LOCK_I]
  connect_bd_net -net axi_dcb_register_bank_0_BUS_CLK_SRC_SEL_O [get_bd_pins BUS_CLK_SEL_O] [get_bd_pins axi_dcb_register_bank_0/BUS_CLK_SRC_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_DAQ_SOFT_TRIGGER_O [get_bd_pins DAQ_SOFT_TRIGGER_O] [get_bd_pins axi_dcb_register_bank_0/DAQ_SOFT_TRIGGER_O]
  connect_bd_net -net axi_dcb_register_bank_0_DISTRIBUTOR_CLK_SRC_SEL_O [get_bd_pins MOSI_DIS_2_O] [get_bd_pins axi_dcb_register_bank_0/DISTRIBUTOR_CLK_SRC_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_EXT_CLK_IN_SEL_O [get_bd_pins CLK_SEL_EXT_O] [get_bd_pins axi_dcb_register_bank_0/EXT_CLK_IN_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_0_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_0_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In0]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_1_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_1_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In1]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_2_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_2_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In2]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_3_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_3_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In3]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_4_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_4_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In4]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_5_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_5_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In5]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_6_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_6_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In6]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_7_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_7_REG_WR_STROBE_O] [get_bd_pins xlconcat_lmk_ch_mod/In7]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_CLK_SRC_SEL_O [get_bd_pins CLK_SEL_O] [get_bd_pins axi_dcb_register_bank_0/LMK_CLK_SRC_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_MOD_FLAG_REG_WR_STROBE_O [get_bd_pins axi_dcb_register_bank_0/LMK_MOD_FLAG_REG_WR_STROBE_O] [get_bd_pins util_mod_flag_lmk_ch/RESET_I]
  connect_bd_net -net axi_dcb_register_bank_0_RECONFIGURE_FPGA_O [get_bd_pins RECONFIGURE_FPGA_O] [get_bd_pins axi_dcb_register_bank_0/RECONFIGURE_FPGA_O]
  connect_bd_net -net axi_dcb_register_bank_0_SYNC_DELAY_O [get_bd_pins SYNC_DELAY_O] [get_bd_pins axi_dcb_register_bank_0/SYNC_DELAY_O]
  connect_bd_net -net axi_dcb_register_bank_0_TRIGGER_MGR_RST_O [get_bd_pins TRIGGER_MGR_RST_O] [get_bd_pins axi_dcb_register_bank_0/TRIGGER_MGR_RST_O]
  connect_bd_net -net axi_dcb_register_bank_0_TR_SYNC_BPL_O [get_bd_pins TR_SYNC_BPL_O] [get_bd_pins axi_dcb_register_bank_0/TR_SYNC_BPL_O]
  connect_bd_net -net axi_dcb_register_bank_0_WDB_CLK_MGR_RST_O [get_bd_pins WDB_CLK_MGR_RST_O] [get_bd_pins axi_dcb_register_bank_0/WDB_CLK_MGR_RST_O]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins WDB_CLK_MGR_LOCK_I] [get_bd_pins axi_dcb_register_bank_0/WDB_CLK_MGR_LOCK_I]
  connect_bd_net -net trigger_manager_0_PERR_COUNT_O [get_bd_pins TRB_PARITY_ERROR_COUNT_I] [get_bd_pins axi_dcb_register_bank_0/TRB_PARITY_ERROR_COUNT_I]
  connect_bd_net -net trigger_manager_0_PERR_O [get_bd_pins TRB_FLAG_PARITY_ERROR_I] [get_bd_pins axi_dcb_register_bank_0/TRB_FLAG_PARITY_ERROR_I]
  connect_bd_net -net trigger_manager_0_TRIGGER_PDATA_O [get_bd_pins Din1] [get_bd_pins xlslice_trb_info_lsbs/Din] [get_bd_pins xlslice_trb_info_msbs/Din]
  connect_bd_net -net trigger_manager_0_VALID_O [get_bd_pins TRB_FLAG_NEW_I] [get_bd_pins axi_dcb_register_bank_0/TRB_FLAG_NEW_I]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins s00_axi_aclk] [get_bd_pins axi_dcb_register_bank_0/s00_axi_aclk] [get_bd_pins util_mod_flag_lmk_ch/CLK_I]
  connect_bd_net -net util_mod_flag_reg_0_MODIFIED_FLAG_O [get_bd_pins util_mod_flag_lmk_ch/MODIFIED_FLAG_O] [get_bd_pins xlslice_lmk0_mod/Din] [get_bd_pins xlslice_lmk1_mod/Din] [get_bd_pins xlslice_lmk2_mod/Din] [get_bd_pins xlslice_lmk3_mod/Din] [get_bd_pins xlslice_lmk4_mod/Din] [get_bd_pins xlslice_lmk5_mod/Din] [get_bd_pins xlslice_lmk6_mod/Din] [get_bd_pins xlslice_lmk7_mod/Din]
  connect_bd_net -net xlconcat_lmk_ch_mod_dout [get_bd_pins util_mod_flag_lmk_ch/MODIFIED_I] [get_bd_pins xlconcat_lmk_ch_mod/dout]
  connect_bd_net -net xlslice_lmk0_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_0_MOD_I] [get_bd_pins xlslice_lmk0_mod/Dout]
  connect_bd_net -net xlslice_lmk1_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_1_MOD_I] [get_bd_pins xlslice_lmk1_mod/Dout]
  connect_bd_net -net xlslice_lmk2_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_2_MOD_I] [get_bd_pins xlslice_lmk2_mod/Dout]
  connect_bd_net -net xlslice_lmk3_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_3_MOD_I] [get_bd_pins xlslice_lmk3_mod/Dout]
  connect_bd_net -net xlslice_lmk4_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_4_MOD_I] [get_bd_pins xlslice_lmk4_mod/Dout]
  connect_bd_net -net xlslice_lmk5_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_5_MOD_I] [get_bd_pins xlslice_lmk5_mod/Dout]
  connect_bd_net -net xlslice_lmk6_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_6_MOD_I] [get_bd_pins xlslice_lmk6_mod/Dout]
  connect_bd_net -net xlslice_lmk7_mod_Dout [get_bd_pins axi_dcb_register_bank_0/LMK_7_MOD_I] [get_bd_pins xlslice_lmk7_mod/Dout]
  connect_bd_net -net xlslice_peripheral_rst_n0_Dout [get_bd_pins s00_axi_aresetn] [get_bd_pins axi_dcb_register_bank_0/s00_axi_aresetn]
  connect_bd_net -net xlslice_trb_info_lsbs_Dout [get_bd_pins axi_dcb_register_bank_0/TRB_INFO_LSB_I] [get_bd_pins xlslice_trb_info_lsbs/Dout]
  connect_bd_net -net xlslice_trb_info_msbs_Dout [get_bd_pins axi_dcb_register_bank_0/TRB_INFO_MSB_I] [get_bd_pins xlslice_trb_info_msbs/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_const_clk_distr_ctrl
proc create_hier_cell_hier_const_clk_distr_ctrl { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_const_clk_distr_ctrl() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 CE_DIS_1_O
  create_bd_pin -dir O -from 0 -to 0 CE_DIS_2_O
  create_bd_pin -dir O -from 0 -to 0 CLK_DIS_1_O
  create_bd_pin -dir O -from 0 -to 0 CLK_DIS_2_O

  # Create instance: const_ce_dis_1, and set properties
  set const_ce_dis_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_ce_dis_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_ce_dis_1

  # Create instance: const_ce_dis_2, and set properties
  set const_ce_dis_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_ce_dis_2 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_ce_dis_2

  # Create instance: const_clk_dis_1, and set properties
  set const_clk_dis_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_clk_dis_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_clk_dis_1

  # Create instance: const_clk_dis_2, and set properties
  set const_clk_dis_2 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_clk_dis_2 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_clk_dis_2

  # Create port connections
  connect_bd_net -net const_ce_dis_1_dout [get_bd_pins CE_DIS_1_O] [get_bd_pins const_ce_dis_1/dout]
  connect_bd_net -net const_ce_dis_2_dout [get_bd_pins CE_DIS_2_O] [get_bd_pins const_ce_dis_2/dout]
  connect_bd_net -net const_clk_dis_1_dout [get_bd_pins CLK_DIS_1_O] [get_bd_pins const_clk_dis_1/dout]
  connect_bd_net -net const_clk_dis_2_dout [get_bd_pins CLK_DIS_2_O] [get_bd_pins const_clk_dis_2/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_const_bpl_dir
proc create_hier_cell_hier_const_bpl_dir { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_const_bpl_dir() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 INFO_DIR_O
  create_bd_pin -dir O -from 0 -to 0 SYNC_DIR_O
  create_bd_pin -dir O -from 0 -to 0 TRG_DIR_O

  # Create instance: const_info_dir, and set properties
  set const_info_dir [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_info_dir ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_info_dir

  # Create instance: const_sync_dir, and set properties
  set const_sync_dir [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_sync_dir ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_sync_dir

  # Create instance: const_trg_dir, and set properties
  set const_trg_dir [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_trg_dir ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_trg_dir

  # Create port connections
  connect_bd_net -net const_info_dir_dout [get_bd_pins INFO_DIR_O] [get_bd_pins const_info_dir/dout]
  connect_bd_net -net const_sync_dir_dout [get_bd_pins SYNC_DIR_O] [get_bd_pins const_sync_dir/dout]
  connect_bd_net -net const_trg_dir_dout [get_bd_pins TRG_DIR_O] [get_bd_pins const_trg_dir/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: hier_const_aux_clk
proc create_hier_cell_hier_const_aux_clk { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_hier_const_aux_clk() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 AUX_OUT_N_O
  create_bd_pin -dir O -from 0 -to 0 AUX_OUT_P_O

  # Create instance: const_aux_clk_n, and set properties
  set const_aux_clk_n [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_aux_clk_n ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_aux_clk_n

  # Create instance: const_aux_clk_p, and set properties
  set const_aux_clk_p [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_aux_clk_p ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_aux_clk_p

  # Create port connections
  connect_bd_net -net const_aux_clk_n_dout [get_bd_pins AUX_OUT_N_O] [get_bd_pins const_aux_clk_n/dout]
  connect_bd_net -net const_aux_clk_p_dout [get_bd_pins AUX_OUT_P_O] [get_bd_pins const_aux_clk_p/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: eth_constants_0
proc create_hier_cell_eth_constants_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_eth_constants_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 dout0
  create_bd_pin -dir O -from 0 -to 0 dout1
  create_bd_pin -dir O -from 0 -to 0 dout2
  create_bd_pin -dir O -from 4 -to 0 dout3
  create_bd_pin -dir O -from 15 -to 0 dout4
  create_bd_pin -dir O -from 0 -to 0 dout5
  create_bd_pin -dir O -from 0 -to 0 dout6

  # Create instance: const_eth_an_adv_cfg_vec, and set properties
  set const_eth_an_adv_cfg_vec [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_an_adv_cfg_vec ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {100001} \
   CONFIG.CONST_WIDTH {16} \
 ] $const_eth_an_adv_cfg_vec

  # Create instance: const_eth_basex_or_sgmii, and set properties
  set const_eth_basex_or_sgmii [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_basex_or_sgmii ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_basex_or_sgmii

  # Create instance: const_eth_cfg_vec, and set properties
  set const_eth_cfg_vec [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_cfg_vec ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {5} \
 ] $const_eth_cfg_vec

  # Create instance: const_eth_col, and set properties
  set const_eth_col [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_col ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_col

  # Create instance: const_eth_crs, and set properties
  set const_eth_crs [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_crs ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_crs

  # Create instance: const_eth_gp_0, and set properties
  set const_eth_gp_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_gp_0 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_gp_0

  # Create instance: const_eth_gp_1, and set properties
  set const_eth_gp_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth_gp_1 ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_eth_gp_1

  # Create port connections
  connect_bd_net -net const_eth_an_adv_cfg_vec_dout [get_bd_pins dout4] [get_bd_pins const_eth_an_adv_cfg_vec/dout]
  connect_bd_net -net const_eth_cfg_vec_dout [get_bd_pins dout3] [get_bd_pins const_eth_cfg_vec/dout]
  connect_bd_net -net const_eth_col_dout [get_bd_pins dout2] [get_bd_pins const_eth_col/dout]
  connect_bd_net -net const_eth_crs_dout [get_bd_pins dout5] [get_bd_pins const_eth_crs/dout]
  connect_bd_net -net const_eth_gp_0_dout [get_bd_pins dout0] [get_bd_pins const_eth_gp_0/dout]
  connect_bd_net -net const_eth_gp_1_dout [get_bd_pins dout1] [get_bd_pins const_eth_gp_1/dout]
  connect_bd_net -net const_eth_gp_1_dout1 [get_bd_pins dout6] [get_bd_pins const_eth_basex_or_sgmii/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: const_led_0
proc create_hier_cell_const_led_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_const_led_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 8 -to 0 dout
  create_bd_pin -dir O -from 0 -to 0 dout1
  create_bd_pin -dir O -from 0 -to 0 dout2

  # Create instance: const_led_hw_error, and set properties
  set const_led_hw_error [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_led_hw_error ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
 ] $const_led_hw_error

  # Create instance: const_led_hw_error_n, and set properties
  set const_led_hw_error_n [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_led_hw_error_n ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0b111111111} \
   CONFIG.CONST_WIDTH {9} \
 ] $const_led_hw_error_n

  # Create instance: const_led_hw_status, and set properties
  set const_led_hw_status [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_led_hw_status ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
 ] $const_led_hw_status

  # Create port connections
  connect_bd_net -net const_led_hw_error_dout [get_bd_pins dout2] [get_bd_pins const_led_hw_error/dout]
  connect_bd_net -net const_led_hw_error_n_dout [get_bd_pins dout] [get_bd_pins const_led_hw_error_n/dout]
  connect_bd_net -net const_led_hw_status_dout [get_bd_pins dout1] [get_bd_pins const_led_hw_status/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: const_dir_ext_trig_0
proc create_hier_cell_const_dir_ext_trig_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_const_dir_ext_trig_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 DIR_EXT_TRG_IN_O
  create_bd_pin -dir O -from 0 -to 0 DIR_EXT_TRG_OUT_O

  # Create instance: const_dir_ext_trig_in, and set properties
  set const_dir_ext_trig_in [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_dir_ext_trig_in ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_dir_ext_trig_in

  # Create instance: const_dir_ext_trig_out, and set properties
  set const_dir_ext_trig_out [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_dir_ext_trig_out ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_dir_ext_trig_out

  # Create port connections
  connect_bd_net -net const_dir_ext_trig_in_dout [get_bd_pins DIR_EXT_TRG_IN_O] [get_bd_pins const_dir_ext_trig_in/dout]
  connect_bd_net -net const_dir_ext_trig_out_dout [get_bd_pins DIR_EXT_TRG_OUT_O] [get_bd_pins const_dir_ext_trig_out/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: const_bpl_signals_0
proc create_hier_cell_const_bpl_signals_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_const_bpl_signals_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir O -from 0 -to 0 MSCB_DATA_O
  create_bd_pin -dir O -from 0 -to 0 MSCB_DRV_EN_O
  create_bd_pin -dir O -from 0 -to 0 SLAVE_SPI_MISO_EN_N_O
  create_bd_pin -dir O -from 0 -to 0 SLAVE_SPI_MISO_O

  # Create instance: const_mscb_data_out, and set properties
  set const_mscb_data_out [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_mscb_data_out ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_mscb_data_out

  # Create instance: const_mscb_drv_en, and set properties
  set const_mscb_drv_en [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_mscb_drv_en ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {0} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_mscb_drv_en

  # Create instance: const_slave_spi_miso, and set properties
  set const_slave_spi_miso [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_slave_spi_miso ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_slave_spi_miso

  # Create instance: const_slave_spi_miso_en_n, and set properties
  set const_slave_spi_miso_en_n [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_slave_spi_miso_en_n ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {1} \
 ] $const_slave_spi_miso_en_n

  # Create port connections
  connect_bd_net -net const_mscb_data_out_dout [get_bd_pins MSCB_DATA_O] [get_bd_pins const_mscb_data_out/dout]
  connect_bd_net -net const_mscb_drv_en_dout [get_bd_pins MSCB_DRV_EN_O] [get_bd_pins const_mscb_drv_en/dout]
  connect_bd_net -net const_slave_spi_miso_dout [get_bd_pins SLAVE_SPI_MISO_O] [get_bd_pins const_slave_spi_miso/dout]
  connect_bd_net -net const_slave_spi_miso_en_n_dout [get_bd_pins SLAVE_SPI_MISO_EN_N_O] [get_bd_pins const_slave_spi_miso_en_n/dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}

# Hierarchical cell: bus_split_spi_ss_0
proc create_hier_cell_bus_split_spi_ss_0 { parentCell nameHier } {

  variable script_folder

  if { $parentCell eq "" || $nameHier eq "" } {
     catch {common::send_msg_id "BD_TCL-102" "ERROR" "create_hier_cell_bus_split_spi_ss_0() - Empty argument(s)!"}
     return
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj

  # Create cell and set as current instance
  set hier_obj [create_bd_cell -type hier $nameHier]
  current_bd_instance $hier_obj

  # Create interface pins

  # Create pins
  create_bd_pin -dir I -from 16 -to 0 Din
  create_bd_pin -dir O -from 0 -to 0 MASTER_SPI_TCB_SS_N_O
  create_bd_pin -dir O -from 15 -to 0 MASTER_SPI_WDB_SS_N_O

  # Create instance: xlslice_spi_ss_tcb_0, and set properties
  set xlslice_spi_ss_tcb_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_spi_ss_tcb_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {16} \
   CONFIG.DIN_TO {16} \
   CONFIG.DIN_WIDTH {17} \
   CONFIG.DOUT_WIDTH {1} \
 ] $xlslice_spi_ss_tcb_0

  # Create instance: xlslice_spi_ss_wdb_0, and set properties
  set xlslice_spi_ss_wdb_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlslice xlslice_spi_ss_wdb_0 ]
  set_property -dict [ list \
   CONFIG.DIN_FROM {15} \
   CONFIG.DIN_TO {0} \
   CONFIG.DIN_WIDTH {17} \
   CONFIG.DOUT_WIDTH {16} \
 ] $xlslice_spi_ss_wdb_0

  # Create port connections
  connect_bd_net -net axi_quad_spi_to_bpl_0_ss_o [get_bd_pins Din] [get_bd_pins xlslice_spi_ss_tcb_0/Din] [get_bd_pins xlslice_spi_ss_wdb_0/Din]
  connect_bd_net -net xlslice_spi_ss_tcb_0_Dout [get_bd_pins MASTER_SPI_TCB_SS_N_O] [get_bd_pins xlslice_spi_ss_tcb_0/Dout]
  connect_bd_net -net xlslice_spi_ss_wdb_0_Dout [get_bd_pins MASTER_SPI_WDB_SS_N_O] [get_bd_pins xlslice_spi_ss_wdb_0/Dout]

  # Restore current instance
  current_bd_instance $oldCurInst
}


# Procedure to create entire design; Provide argument to make
# procedure reusable. If parentCell is "", will use root.
proc create_root_design { parentCell } {

  variable script_folder
  variable design_name

  if { $parentCell eq "" } {
     set parentCell [get_bd_cells /]
  }

  # Get object for parentCell
  set parentObj [get_bd_cells $parentCell]
  if { $parentObj == "" } {
     catch {common::send_msg_id "BD_TCL-100" "ERROR" "Unable to find parent cell <$parentCell>!"}
     return
  }

  # Make sure parentObj is hier blk
  set parentType [get_property TYPE $parentObj]
  if { $parentType ne "hier" } {
     catch {common::send_msg_id "BD_TCL-101" "ERROR" "Parent <$parentObj> has TYPE = <$parentType>. Expected to be <hier>."}
     return
  }

  # Save current instance; Restore later
  set oldCurInst [current_bd_instance .]

  # Set parent object as current
  current_bd_instance $parentObj


  # Create interface ports
  set DDR [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:ddrx_rtl:1.0 DDR ]
  set ETH0_SFP [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:sfp_rtl:1.0 ETH0_SFP ]
  set ETH1_SFP [ create_bd_intf_port -mode Master -vlnv xilinx.com:interface:sfp_rtl:1.0 ETH1_SFP ]
  set FIXED_IO [ create_bd_intf_port -mode Master -vlnv xilinx.com:display_processing_system7:fixedio_rtl:1.0 FIXED_IO ]

  # Create ports
  set AUX_OUT_N_O [ create_bd_port -dir O -from 0 -to 0 AUX_OUT_N_O ]
  set AUX_OUT_P_O [ create_bd_port -dir O -from 0 -to 0 AUX_OUT_P_O ]
  set BOARD_SELECT_N_I [ create_bd_port -dir I BOARD_SELECT_N_I ]
  set BUSY_IN_N_I [ create_bd_port -dir I BUSY_IN_N_I ]
  set BUSY_N_N_O [ create_bd_port -dir O BUSY_N_N_O ]
  set BUSY_N_P_O [ create_bd_port -dir O BUSY_N_P_O ]
  set BUSY_OUT_N_O [ create_bd_port -dir O BUSY_OUT_N_O ]
  set BUS_CLK_SEL_O [ create_bd_port -dir O BUS_CLK_SEL_O ]
  set CE_DIS_1_O [ create_bd_port -dir O -from 0 -to 0 CE_DIS_1_O ]
  set CE_DIS_2_O [ create_bd_port -dir O -from 0 -to 0 CE_DIS_2_O ]
  set CLK_DIS_1_O [ create_bd_port -dir O -from 0 -to 0 CLK_DIS_1_O ]
  set CLK_DIS_2_O [ create_bd_port -dir O -from 0 -to 0 CLK_DIS_2_O ]
  set CLK_FPGA_N_I [ create_bd_port -dir I -type clk CLK_FPGA_N_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $CLK_FPGA_N_I
  set CLK_FPGA_P_I [ create_bd_port -dir I -type clk CLK_FPGA_P_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $CLK_FPGA_P_I
  set CLK_SEL_EXT_O [ create_bd_port -dir O CLK_SEL_EXT_O ]
  set CLK_SEL_O [ create_bd_port -dir O CLK_SEL_O ]
  set DIR_EXT_TRG_IN_O [ create_bd_port -dir O -from 0 -to 0 DIR_EXT_TRG_IN_O ]
  set DIR_EXT_TRG_OUT_O [ create_bd_port -dir O -from 0 -to 0 DIR_EXT_TRG_OUT_O ]
  set EXT_TRG_IN_I [ create_bd_port -dir I EXT_TRG_IN_I ]
  set EXT_TRG_OUT_O [ create_bd_port -dir O EXT_TRG_OUT_O ]
  set FLASH_SELECT_INIT_N_O [ create_bd_port -dir O FLASH_SELECT_INIT_N_O ]
  set FS_INIT_N_I [ create_bd_port -dir I FS_INIT_N_I ]
  set GTREFCLK0_N_I [ create_bd_port -dir I GTREFCLK0_N_I ]
  set GTREFCLK0_P_I [ create_bd_port -dir I GTREFCLK0_P_I ]
  set INFO_DIR_O [ create_bd_port -dir O -from 0 -to 0 INFO_DIR_O ]
  set LD_I [ create_bd_port -dir I LD_I ]
  set LED_B_N_O [ create_bd_port -dir O LED_B_N_O ]
  set LED_G_N_O [ create_bd_port -dir O LED_G_N_O ]
  set LED_R_N_O [ create_bd_port -dir O LED_R_N_O ]
  set MASTER_SPI_CLK_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_CLK_O ]
  set MASTER_SPI_DE_N_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_DE_N_O ]
  set MASTER_SPI_MISO_I [ create_bd_port -dir I MASTER_SPI_MISO_I ]
  set MASTER_SPI_MOSI_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_MOSI_O ]
  set MASTER_SPI_TCB_SS_N_O [ create_bd_port -dir O -from 0 -to 0 MASTER_SPI_TCB_SS_N_O ]
  set MASTER_SPI_WDB_SS_N_O [ create_bd_port -dir O -from 15 -to 0 MASTER_SPI_WDB_SS_N_O ]
  set MOSI_DIS_1_O [ create_bd_port -dir O MOSI_DIS_1_O ]
  set MOSI_DIS_2_O [ create_bd_port -dir O MOSI_DIS_2_O ]
  set MSCB_DATA_I [ create_bd_port -dir I MSCB_DATA_I ]
  set MSCB_DATA_O [ create_bd_port -dir O -from 0 -to 0 MSCB_DATA_O ]
  set MSCB_DRV_EN_O [ create_bd_port -dir O -from 0 -to 0 MSCB_DRV_EN_O ]
  set RESET_FPGA_BUS_N_O [ create_bd_port -dir O RESET_FPGA_BUS_N_O ]
  set SFP_1_LOS_I [ create_bd_port -dir I SFP_1_LOS_I ]
  set SFP_1_MOD_I [ create_bd_port -dir I SFP_1_MOD_I ]
  set SFP_1_RS_O [ create_bd_port -dir O -from 1 -to 0 SFP_1_RS_O ]
  set SFP_1_TX_DISABLE_O [ create_bd_port -dir O SFP_1_TX_DISABLE_O ]
  set SFP_1_TX_FAULT_I [ create_bd_port -dir I SFP_1_TX_FAULT_I ]
  set SFP_2_LOS_I [ create_bd_port -dir I SFP_2_LOS_I ]
  set SFP_2_MOD_I [ create_bd_port -dir I SFP_2_MOD_I ]
  set SFP_2_RS_O [ create_bd_port -dir O -from 1 -to 0 SFP_2_RS_O ]
  set SFP_2_TX_DISABLE_O [ create_bd_port -dir O SFP_2_TX_DISABLE_O ]
  set SFP_2_TX_FAULT_I [ create_bd_port -dir I SFP_2_TX_FAULT_I ]
  set SLAVE_SPI_CLK_I [ create_bd_port -dir I SLAVE_SPI_CLK_I ]
  set SLAVE_SPI_MISO_EN_N_O [ create_bd_port -dir O -from 0 -to 0 SLAVE_SPI_MISO_EN_N_O ]
  set SLAVE_SPI_MISO_O [ create_bd_port -dir O -from 0 -to 0 SLAVE_SPI_MISO_O ]
  set SLAVE_SPI_MOSI_I [ create_bd_port -dir I SLAVE_SPI_MOSI_I ]
  set SPI_CS_FPGA_N_I [ create_bd_port -dir I SPI_CS_FPGA_N_I ]
  set SPI_CS_N_O [ create_bd_port -dir O SPI_CS_N_O ]
  set SPR_N_I [ create_bd_port -dir I SPR_N_I ]
  set SPR_P_I [ create_bd_port -dir I SPR_P_I ]
  set SYNC_DIR_O [ create_bd_port -dir O -from 0 -to 0 SYNC_DIR_O ]
  set SYNC_N_I [ create_bd_port -dir I SYNC_N_I ]
  set SYNC_P_I [ create_bd_port -dir I SYNC_P_I ]
  set TRG_DIR_O [ create_bd_port -dir O -from 0 -to 0 TRG_DIR_O ]
  set TRG_N_I [ create_bd_port -dir I TRG_N_I ]
  set TRG_P_I [ create_bd_port -dir I TRG_P_I ]
  set TRIGGER_O [ create_bd_port -dir O TRIGGER_O ]
  set TR_INFO_O [ create_bd_port -dir O TR_INFO_O ]
  set TR_SYNC_O [ create_bd_port -dir O TR_SYNC_O ]
  set WDB_CLK_N_I [ create_bd_port -dir I -type clk WDB_CLK_N_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $WDB_CLK_N_I
  set WDB_CLK_P_I [ create_bd_port -dir I -type clk WDB_CLK_P_I ]
  set_property -dict [ list \
   CONFIG.FREQ_HZ {80000000} \
 ] $WDB_CLK_P_I

  # Create instance: axi_interconnect_0, and set properties
  set axi_interconnect_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_interconnect axi_interconnect_0 ]
  set_property -dict [ list \
   CONFIG.ENABLE_ADVANCED_OPTIONS {1} \
   CONFIG.M03_HAS_DATA_FIFO {0} \
   CONFIG.NUM_MI {3} \
 ] $axi_interconnect_0

  # Create instance: axi_quad_spi_to_bpl_0, and set properties
  set axi_quad_spi_to_bpl_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:axi_quad_spi axi_quad_spi_to_bpl_0 ]
  set_property -dict [ list \
   CONFIG.C_FIFO_DEPTH {256} \
   CONFIG.C_NUM_SS_BITS {3} \
   CONFIG.C_SCK_RATIO {16} \
   CONFIG.C_TYPE_OF_AXI4_INTERFACE {0} \
   CONFIG.C_USE_STARTUP {0} \
   CONFIG.C_USE_STARTUP_INT {0} \
   CONFIG.C_XIP_MODE {0} \
   CONFIG.Master_mode {1} \
 ] $axi_quad_spi_to_bpl_0

  # Create instance: bus_split_spi_ss_0
  create_hier_cell_bus_split_spi_ss_0 [current_bd_instance .] bus_split_spi_ss_0

  # Create instance: busy_manager_0, and set properties
  set busy_manager_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:busy_manager busy_manager_0 ]

  # Create instance: clk_wiz_0, and set properties
  set clk_wiz_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:clk_wiz clk_wiz_0 ]
  set_property -dict [ list \
   CONFIG.CLKIN1_JITTER_PS {125.0} \
   CONFIG.CLKOUT1_JITTER {116.764} \
   CONFIG.CLKOUT1_PHASE_ERROR {101.403} \
   CONFIG.CLKOUT1_REQUESTED_OUT_FREQ {240.000} \
   CONFIG.CLK_OUT1_PORT {clk_out_240MHz} \
   CONFIG.ENABLE_CLOCK_MONITOR {false} \
   CONFIG.MMCM_CLKFBOUT_MULT_F {12.375} \
   CONFIG.MMCM_CLKIN1_PERIOD {12.500} \
   CONFIG.MMCM_CLKIN2_PERIOD {10.0} \
   CONFIG.MMCM_CLKOUT0_DIVIDE_F {4.125} \
   CONFIG.MMCM_DIVCLK_DIVIDE {1} \
   CONFIG.PRIMITIVE {MMCM} \
   CONFIG.PRIM_IN_FREQ {80.000} \
 ] $clk_wiz_0

  # Create instance: clock_measure_0, and set properties
  set clock_measure_0 [ create_bd_cell -type ip -vlnv psi.ch:PSI:clock_measure clock_measure_0 ]
  set_property -dict [ list \
   CONFIG.AxiClkFreq_g {80000000} \
   CONFIG.NumOfClocks_g {6} \
 ] $clock_measure_0

  # Create instance: const_bpl_signals_0
  create_hier_cell_const_bpl_signals_0 [current_bd_instance .] const_bpl_signals_0

  # Create instance: const_dir_ext_trig_0
  create_hier_cell_const_dir_ext_trig_0 [current_bd_instance .] const_dir_ext_trig_0

  # Create instance: const_eth0_phy_addr, and set properties
  set const_eth0_phy_addr [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth0_phy_addr ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {5} \
 ] $const_eth0_phy_addr

  # Create instance: const_eth1_phy_addr, and set properties
  set const_eth1_phy_addr [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconstant const_eth1_phy_addr ]
  set_property -dict [ list \
   CONFIG.CONST_VAL {1} \
   CONFIG.CONST_WIDTH {5} \
 ] $const_eth1_phy_addr

  # Create instance: const_led_0
  create_hier_cell_const_led_0 [current_bd_instance .] const_led_0

  # Create instance: eth_constants_0
  create_hier_cell_eth_constants_0 [current_bd_instance .] eth_constants_0

  # Create instance: gig_ethernet_pcs_pma_0, and set properties
  set gig_ethernet_pcs_pma_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:gig_ethernet_pcs_pma gig_ethernet_pcs_pma_0 ]
  set_property -dict [ list \
   CONFIG.Auto_Negotiation {true} \
   CONFIG.EMAC_IF_TEMAC {GEM} \
   CONFIG.Ext_Management_Interface {false} \
   CONFIG.RxGmiiClkSrc {TXOUTCLK} \
   CONFIG.SGMII_PHY_Mode {false} \
   CONFIG.Standard {BOTH} \
   CONFIG.SupportLevel {Include_Shared_Logic_in_Core} \
   CONFIG.TransceiverControl {false} \
 ] $gig_ethernet_pcs_pma_0

  # Create instance: gig_ethernet_pcs_pma_1, and set properties
  set gig_ethernet_pcs_pma_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:gig_ethernet_pcs_pma gig_ethernet_pcs_pma_1 ]
  set_property -dict [ list \
   CONFIG.Auto_Negotiation {true} \
   CONFIG.EMAC_IF_TEMAC {GEM} \
   CONFIG.Ext_Management_Interface {false} \
   CONFIG.RxGmiiClkSrc {TXOUTCLK} \
   CONFIG.SGMII_PHY_Mode {false} \
   CONFIG.Standard {BOTH} \
   CONFIG.SupportLevel {Include_Shared_Logic_in_Example_Design} \
   CONFIG.TransceiverControl {false} \
 ] $gig_ethernet_pcs_pma_1

  # Create instance: hier_const_aux_clk
  create_hier_cell_hier_const_aux_clk [current_bd_instance .] hier_const_aux_clk

  # Create instance: hier_const_bpl_dir
  create_hier_cell_hier_const_bpl_dir [current_bd_instance .] hier_const_bpl_dir

  # Create instance: hier_const_clk_distr_ctrl
  create_hier_cell_hier_const_clk_distr_ctrl [current_bd_instance .] hier_const_clk_distr_ctrl

  # Create instance: led_ctrl_0, and set properties
  set led_ctrl_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:led_ctrl led_ctrl_0 ]
  set_property -dict [ list \
   CONFIG.CGN_ACTIVE_HIGH_HW_ERRORS {2} \
   CONFIG.CGN_CLK_PERIOD {12500} \
 ] $led_ctrl_0

  # Create instance: ps_0, and set properties
  set ps_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:processing_system7 ps_0 ]
  set_property -dict [ list \
   CONFIG.PCW_ACT_APU_PERIPHERAL_FREQMHZ {666.666687} \
   CONFIG.PCW_ACT_CAN0_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN1_PERIPHERAL_FREQMHZ {23.8095} \
   CONFIG.PCW_ACT_CAN_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_DCI_PERIPHERAL_FREQMHZ {10.158730} \
   CONFIG.PCW_ACT_ENET0_PERIPHERAL_FREQMHZ {125.000000} \
   CONFIG.PCW_ACT_ENET1_PERIPHERAL_FREQMHZ {125.000000} \
   CONFIG.PCW_ACT_FPGA0_PERIPHERAL_FREQMHZ {50.000000} \
   CONFIG.PCW_ACT_FPGA1_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_FPGA2_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_FPGA3_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_I2C_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_PCAP_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_QSPI_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_SDIO_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_SMC_PERIPHERAL_FREQMHZ {10.000000} \
   CONFIG.PCW_ACT_SPI_PERIPHERAL_FREQMHZ {150.000000} \
   CONFIG.PCW_ACT_TPIU_PERIPHERAL_FREQMHZ {200.000000} \
   CONFIG.PCW_ACT_TTC0_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC0_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK0_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK1_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC1_CLK2_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_ACT_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_ACT_UART_PERIPHERAL_FREQMHZ {100.000000} \
   CONFIG.PCW_ACT_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_ACT_WDT_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_APU_CLK_RATIO_ENABLE {6:2:1} \
   CONFIG.PCW_APU_PERIPHERAL_FREQMHZ {666.666666} \
   CONFIG.PCW_ARMPLL_CTRL_FBDIV {40} \
   CONFIG.PCW_CAN0_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN1_GRP_CLK_ENABLE {0} \
   CONFIG.PCW_CAN1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_CAN1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_CAN_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_CAN_PERIPHERAL_VALID {0} \
   CONFIG.PCW_CLK0_FREQ {50000000} \
   CONFIG.PCW_CLK1_FREQ {200000000} \
   CONFIG.PCW_CLK2_FREQ {10000000} \
   CONFIG.PCW_CLK3_FREQ {10000000} \
   CONFIG.PCW_CPU_CPU_6X4X_MAX_RANGE {667} \
   CONFIG.PCW_CPU_CPU_PLL_FREQMHZ {1333.333} \
   CONFIG.PCW_CPU_PERIPHERAL_CLKSRC {ARM PLL} \
   CONFIG.PCW_CPU_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_CRYSTAL_PERIPHERAL_FREQMHZ {33.333333} \
   CONFIG.PCW_DCI_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR0 {15} \
   CONFIG.PCW_DCI_PERIPHERAL_DIVISOR1 {7} \
   CONFIG.PCW_DCI_PERIPHERAL_FREQMHZ {10.159} \
   CONFIG.PCW_DDRPLL_CTRL_FBDIV {32} \
   CONFIG.PCW_DDR_DDR_PLL_FREQMHZ {1066.667} \
   CONFIG.PCW_DDR_HPRLPR_QUEUE_PARTITION {HPR(0)/LPR(32)} \
   CONFIG.PCW_DDR_HPR_TO_CRITICAL_PRIORITY_LEVEL {15} \
   CONFIG.PCW_DDR_LPR_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DDR_PERIPHERAL_CLKSRC {DDR PLL} \
   CONFIG.PCW_DDR_PERIPHERAL_DIVISOR0 {2} \
   CONFIG.PCW_DDR_PORT0_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT1_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT2_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_PORT3_HPR_ENABLE {0} \
   CONFIG.PCW_DDR_RAM_BASEADDR {0x00100000} \
   CONFIG.PCW_DDR_RAM_HIGHADDR {0x3FFFFFFF} \
   CONFIG.PCW_DDR_WRITE_TO_CRITICAL_PRIORITY_LEVEL {2} \
   CONFIG.PCW_DM_WIDTH {4} \
   CONFIG.PCW_DQS_WIDTH {4} \
   CONFIG.PCW_DQ_WIDTH {32} \
   CONFIG.PCW_ENET0_ENET0_IO {EMIO} \
   CONFIG.PCW_ENET0_GRP_MDIO_ENABLE {1} \
   CONFIG.PCW_ENET0_GRP_MDIO_IO {EMIO} \
   CONFIG.PCW_ENET0_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_ENET0_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET0_RESET_ENABLE {0} \
   CONFIG.PCW_ENET1_ENET1_IO {EMIO} \
   CONFIG.PCW_ENET1_GRP_MDIO_ENABLE {1} \
   CONFIG.PCW_ENET1_GRP_MDIO_IO {EMIO} \
   CONFIG.PCW_ENET1_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_ENET1_PERIPHERAL_FREQMHZ {1000 Mbps} \
   CONFIG.PCW_ENET1_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_ENABLE {0} \
   CONFIG.PCW_ENET_RESET_POLARITY {Active Low} \
   CONFIG.PCW_EN_4K_TIMER {0} \
   CONFIG.PCW_EN_CAN0 {0} \
   CONFIG.PCW_EN_CAN1 {0} \
   CONFIG.PCW_EN_CLK0_PORT {1} \
   CONFIG.PCW_EN_CLK1_PORT {1} \
   CONFIG.PCW_EN_CLK2_PORT {0} \
   CONFIG.PCW_EN_CLK3_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG0_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG1_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG2_PORT {0} \
   CONFIG.PCW_EN_CLKTRIG3_PORT {0} \
   CONFIG.PCW_EN_DDR {1} \
   CONFIG.PCW_EN_EMIO_CAN0 {0} \
   CONFIG.PCW_EN_EMIO_CAN1 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_CD_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_ENET0 {1} \
   CONFIG.PCW_EN_EMIO_ENET1 {1} \
   CONFIG.PCW_EN_EMIO_GPIO {1} \
   CONFIG.PCW_EN_EMIO_I2C0 {0} \
   CONFIG.PCW_EN_EMIO_I2C1 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART0 {0} \
   CONFIG.PCW_EN_EMIO_MODEM_UART1 {0} \
   CONFIG.PCW_EN_EMIO_PJTAG {0} \
   CONFIG.PCW_EN_EMIO_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_SDIO1 {0} \
   CONFIG.PCW_EN_EMIO_SPI0 {0} \
   CONFIG.PCW_EN_EMIO_SPI1 {0} \
   CONFIG.PCW_EN_EMIO_SRAM_INT {0} \
   CONFIG.PCW_EN_EMIO_TRACE {0} \
   CONFIG.PCW_EN_EMIO_TTC0 {1} \
   CONFIG.PCW_EN_EMIO_TTC1 {0} \
   CONFIG.PCW_EN_EMIO_UART0 {0} \
   CONFIG.PCW_EN_EMIO_UART1 {0} \
   CONFIG.PCW_EN_EMIO_WDT {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO0 {0} \
   CONFIG.PCW_EN_EMIO_WP_SDIO1 {0} \
   CONFIG.PCW_EN_ENET0 {1} \
   CONFIG.PCW_EN_ENET1 {1} \
   CONFIG.PCW_EN_GPIO {1} \
   CONFIG.PCW_EN_I2C0 {1} \
   CONFIG.PCW_EN_I2C1 {1} \
   CONFIG.PCW_EN_MODEM_UART0 {0} \
   CONFIG.PCW_EN_MODEM_UART1 {0} \
   CONFIG.PCW_EN_PJTAG {0} \
   CONFIG.PCW_EN_PTP_ENET0 {0} \
   CONFIG.PCW_EN_PTP_ENET1 {0} \
   CONFIG.PCW_EN_QSPI {1} \
   CONFIG.PCW_EN_RST0_PORT {1} \
   CONFIG.PCW_EN_RST1_PORT {0} \
   CONFIG.PCW_EN_RST2_PORT {0} \
   CONFIG.PCW_EN_RST3_PORT {0} \
   CONFIG.PCW_EN_SDIO0 {1} \
   CONFIG.PCW_EN_SDIO1 {0} \
   CONFIG.PCW_EN_SMC {0} \
   CONFIG.PCW_EN_SPI0 {0} \
   CONFIG.PCW_EN_SPI1 {1} \
   CONFIG.PCW_EN_TRACE {0} \
   CONFIG.PCW_EN_TTC0 {1} \
   CONFIG.PCW_EN_TTC1 {0} \
   CONFIG.PCW_EN_UART0 {0} \
   CONFIG.PCW_EN_UART1 {1} \
   CONFIG.PCW_EN_USB0 {1} \
   CONFIG.PCW_EN_USB1 {0} \
   CONFIG.PCW_EN_WDT {0} \
   CONFIG.PCW_FCLK0_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR0 {6} \
   CONFIG.PCW_FCLK0_PERIPHERAL_DIVISOR1 {4} \
   CONFIG.PCW_FCLK1_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR0 {3} \
   CONFIG.PCW_FCLK1_PERIPHERAL_DIVISOR1 {2} \
   CONFIG.PCW_FCLK2_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK2_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_FCLK3_PERIPHERAL_DIVISOR1 {1} \
   CONFIG.PCW_FCLK_CLK0_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK1_BUF {TRUE} \
   CONFIG.PCW_FCLK_CLK2_BUF {FALSE} \
   CONFIG.PCW_FCLK_CLK3_BUF {FALSE} \
   CONFIG.PCW_FPGA0_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA1_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_FPGA2_PERIPHERAL_FREQMHZ {150} \
   CONFIG.PCW_FPGA3_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_FPGA_FCLK0_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK1_ENABLE {1} \
   CONFIG.PCW_FPGA_FCLK2_ENABLE {0} \
   CONFIG.PCW_FPGA_FCLK3_ENABLE {0} \
   CONFIG.PCW_GP0_EN_MODIFIABLE_TXN {1} \
   CONFIG.PCW_GP0_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP0_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GP1_EN_MODIFIABLE_TXN {1} \
   CONFIG.PCW_GP1_NUM_READ_THREADS {4} \
   CONFIG.PCW_GP1_NUM_WRITE_THREADS {4} \
   CONFIG.PCW_GPIO_EMIO_GPIO_ENABLE {1} \
   CONFIG.PCW_GPIO_EMIO_GPIO_IO {64} \
   CONFIG.PCW_GPIO_EMIO_GPIO_WIDTH {64} \
   CONFIG.PCW_GPIO_MIO_GPIO_ENABLE {1} \
   CONFIG.PCW_GPIO_MIO_GPIO_IO {MIO} \
   CONFIG.PCW_GPIO_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_I2C0_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C0_I2C0_IO {MIO 50 .. 51} \
   CONFIG.PCW_I2C0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C0_RESET_ENABLE {0} \
   CONFIG.PCW_I2C1_GRP_INT_ENABLE {0} \
   CONFIG.PCW_I2C1_I2C1_IO {MIO 48 .. 49} \
   CONFIG.PCW_I2C1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_I2C1_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_PERIPHERAL_FREQMHZ {111.111115} \
   CONFIG.PCW_I2C_RESET_ENABLE {0} \
   CONFIG.PCW_I2C_RESET_POLARITY {Active Low} \
   CONFIG.PCW_IMPORT_BOARD_PRESET {None} \
   CONFIG.PCW_INCLUDE_ACP_TRANS_CHECK {0} \
   CONFIG.PCW_IOPLL_CTRL_FBDIV {36} \
   CONFIG.PCW_IO_IO_PLL_FREQMHZ {1200.000} \
   CONFIG.PCW_IRQ_F2P_INTR {1} \
   CONFIG.PCW_MIO_0_DIRECTION {out} \
   CONFIG.PCW_MIO_0_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_0_PULLUP {enabled} \
   CONFIG.PCW_MIO_0_SLEW {slow} \
   CONFIG.PCW_MIO_10_DIRECTION {inout} \
   CONFIG.PCW_MIO_10_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_10_PULLUP {enabled} \
   CONFIG.PCW_MIO_10_SLEW {slow} \
   CONFIG.PCW_MIO_11_DIRECTION {inout} \
   CONFIG.PCW_MIO_11_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_11_PULLUP {enabled} \
   CONFIG.PCW_MIO_11_SLEW {slow} \
   CONFIG.PCW_MIO_12_DIRECTION {inout} \
   CONFIG.PCW_MIO_12_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_12_PULLUP {enabled} \
   CONFIG.PCW_MIO_12_SLEW {slow} \
   CONFIG.PCW_MIO_13_DIRECTION {inout} \
   CONFIG.PCW_MIO_13_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_13_PULLUP {enabled} \
   CONFIG.PCW_MIO_13_SLEW {slow} \
   CONFIG.PCW_MIO_14_DIRECTION {out} \
   CONFIG.PCW_MIO_14_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_14_PULLUP {enabled} \
   CONFIG.PCW_MIO_14_SLEW {slow} \
   CONFIG.PCW_MIO_15_DIRECTION {out} \
   CONFIG.PCW_MIO_15_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_15_PULLUP {enabled} \
   CONFIG.PCW_MIO_15_SLEW {slow} \
   CONFIG.PCW_MIO_16_DIRECTION {inout} \
   CONFIG.PCW_MIO_16_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_16_PULLUP {enabled} \
   CONFIG.PCW_MIO_16_SLEW {slow} \
   CONFIG.PCW_MIO_17_DIRECTION {inout} \
   CONFIG.PCW_MIO_17_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_17_PULLUP {enabled} \
   CONFIG.PCW_MIO_17_SLEW {slow} \
   CONFIG.PCW_MIO_18_DIRECTION {inout} \
   CONFIG.PCW_MIO_18_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_18_PULLUP {enabled} \
   CONFIG.PCW_MIO_18_SLEW {slow} \
   CONFIG.PCW_MIO_19_DIRECTION {inout} \
   CONFIG.PCW_MIO_19_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_19_PULLUP {enabled} \
   CONFIG.PCW_MIO_19_SLEW {slow} \
   CONFIG.PCW_MIO_1_DIRECTION {out} \
   CONFIG.PCW_MIO_1_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_1_PULLUP {enabled} \
   CONFIG.PCW_MIO_1_SLEW {slow} \
   CONFIG.PCW_MIO_20_DIRECTION {inout} \
   CONFIG.PCW_MIO_20_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_20_PULLUP {enabled} \
   CONFIG.PCW_MIO_20_SLEW {slow} \
   CONFIG.PCW_MIO_21_DIRECTION {inout} \
   CONFIG.PCW_MIO_21_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_21_PULLUP {enabled} \
   CONFIG.PCW_MIO_21_SLEW {slow} \
   CONFIG.PCW_MIO_22_DIRECTION {inout} \
   CONFIG.PCW_MIO_22_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_22_PULLUP {enabled} \
   CONFIG.PCW_MIO_22_SLEW {slow} \
   CONFIG.PCW_MIO_23_DIRECTION {inout} \
   CONFIG.PCW_MIO_23_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_23_PULLUP {enabled} \
   CONFIG.PCW_MIO_23_SLEW {slow} \
   CONFIG.PCW_MIO_24_DIRECTION {inout} \
   CONFIG.PCW_MIO_24_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_24_PULLUP {enabled} \
   CONFIG.PCW_MIO_24_SLEW {slow} \
   CONFIG.PCW_MIO_25_DIRECTION {inout} \
   CONFIG.PCW_MIO_25_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_25_PULLUP {enabled} \
   CONFIG.PCW_MIO_25_SLEW {slow} \
   CONFIG.PCW_MIO_26_DIRECTION {inout} \
   CONFIG.PCW_MIO_26_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_26_PULLUP {enabled} \
   CONFIG.PCW_MIO_26_SLEW {slow} \
   CONFIG.PCW_MIO_27_DIRECTION {inout} \
   CONFIG.PCW_MIO_27_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_27_PULLUP {enabled} \
   CONFIG.PCW_MIO_27_SLEW {slow} \
   CONFIG.PCW_MIO_28_DIRECTION {inout} \
   CONFIG.PCW_MIO_28_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_28_PULLUP {enabled} \
   CONFIG.PCW_MIO_28_SLEW {slow} \
   CONFIG.PCW_MIO_29_DIRECTION {in} \
   CONFIG.PCW_MIO_29_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_29_PULLUP {enabled} \
   CONFIG.PCW_MIO_29_SLEW {slow} \
   CONFIG.PCW_MIO_2_DIRECTION {inout} \
   CONFIG.PCW_MIO_2_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_2_PULLUP {disabled} \
   CONFIG.PCW_MIO_2_SLEW {slow} \
   CONFIG.PCW_MIO_30_DIRECTION {out} \
   CONFIG.PCW_MIO_30_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_30_PULLUP {enabled} \
   CONFIG.PCW_MIO_30_SLEW {slow} \
   CONFIG.PCW_MIO_31_DIRECTION {in} \
   CONFIG.PCW_MIO_31_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_31_PULLUP {enabled} \
   CONFIG.PCW_MIO_31_SLEW {slow} \
   CONFIG.PCW_MIO_32_DIRECTION {inout} \
   CONFIG.PCW_MIO_32_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_32_PULLUP {enabled} \
   CONFIG.PCW_MIO_32_SLEW {slow} \
   CONFIG.PCW_MIO_33_DIRECTION {inout} \
   CONFIG.PCW_MIO_33_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_33_PULLUP {enabled} \
   CONFIG.PCW_MIO_33_SLEW {slow} \
   CONFIG.PCW_MIO_34_DIRECTION {inout} \
   CONFIG.PCW_MIO_34_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_34_PULLUP {enabled} \
   CONFIG.PCW_MIO_34_SLEW {slow} \
   CONFIG.PCW_MIO_35_DIRECTION {inout} \
   CONFIG.PCW_MIO_35_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_35_PULLUP {enabled} \
   CONFIG.PCW_MIO_35_SLEW {slow} \
   CONFIG.PCW_MIO_36_DIRECTION {in} \
   CONFIG.PCW_MIO_36_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_36_PULLUP {enabled} \
   CONFIG.PCW_MIO_36_SLEW {slow} \
   CONFIG.PCW_MIO_37_DIRECTION {inout} \
   CONFIG.PCW_MIO_37_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_37_PULLUP {enabled} \
   CONFIG.PCW_MIO_37_SLEW {slow} \
   CONFIG.PCW_MIO_38_DIRECTION {inout} \
   CONFIG.PCW_MIO_38_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_38_PULLUP {enabled} \
   CONFIG.PCW_MIO_38_SLEW {slow} \
   CONFIG.PCW_MIO_39_DIRECTION {inout} \
   CONFIG.PCW_MIO_39_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_39_PULLUP {enabled} \
   CONFIG.PCW_MIO_39_SLEW {slow} \
   CONFIG.PCW_MIO_3_DIRECTION {inout} \
   CONFIG.PCW_MIO_3_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_3_PULLUP {disabled} \
   CONFIG.PCW_MIO_3_SLEW {slow} \
   CONFIG.PCW_MIO_40_DIRECTION {inout} \
   CONFIG.PCW_MIO_40_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_40_PULLUP {enabled} \
   CONFIG.PCW_MIO_40_SLEW {slow} \
   CONFIG.PCW_MIO_41_DIRECTION {inout} \
   CONFIG.PCW_MIO_41_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_41_PULLUP {enabled} \
   CONFIG.PCW_MIO_41_SLEW {slow} \
   CONFIG.PCW_MIO_42_DIRECTION {inout} \
   CONFIG.PCW_MIO_42_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_42_PULLUP {enabled} \
   CONFIG.PCW_MIO_42_SLEW {slow} \
   CONFIG.PCW_MIO_43_DIRECTION {inout} \
   CONFIG.PCW_MIO_43_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_43_PULLUP {enabled} \
   CONFIG.PCW_MIO_43_SLEW {slow} \
   CONFIG.PCW_MIO_44_DIRECTION {inout} \
   CONFIG.PCW_MIO_44_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_44_PULLUP {enabled} \
   CONFIG.PCW_MIO_44_SLEW {slow} \
   CONFIG.PCW_MIO_45_DIRECTION {inout} \
   CONFIG.PCW_MIO_45_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_45_PULLUP {enabled} \
   CONFIG.PCW_MIO_45_SLEW {slow} \
   CONFIG.PCW_MIO_46_DIRECTION {inout} \
   CONFIG.PCW_MIO_46_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_46_PULLUP {enabled} \
   CONFIG.PCW_MIO_46_SLEW {slow} \
   CONFIG.PCW_MIO_47_DIRECTION {inout} \
   CONFIG.PCW_MIO_47_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_47_PULLUP {enabled} \
   CONFIG.PCW_MIO_47_SLEW {slow} \
   CONFIG.PCW_MIO_48_DIRECTION {inout} \
   CONFIG.PCW_MIO_48_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_48_PULLUP {disabled} \
   CONFIG.PCW_MIO_48_SLEW {slow} \
   CONFIG.PCW_MIO_49_DIRECTION {inout} \
   CONFIG.PCW_MIO_49_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_49_PULLUP {disabled} \
   CONFIG.PCW_MIO_49_SLEW {slow} \
   CONFIG.PCW_MIO_4_DIRECTION {inout} \
   CONFIG.PCW_MIO_4_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_4_PULLUP {disabled} \
   CONFIG.PCW_MIO_4_SLEW {slow} \
   CONFIG.PCW_MIO_50_DIRECTION {inout} \
   CONFIG.PCW_MIO_50_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_50_PULLUP {disabled} \
   CONFIG.PCW_MIO_50_SLEW {slow} \
   CONFIG.PCW_MIO_51_DIRECTION {inout} \
   CONFIG.PCW_MIO_51_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_51_PULLUP {disabled} \
   CONFIG.PCW_MIO_51_SLEW {slow} \
   CONFIG.PCW_MIO_52_DIRECTION {out} \
   CONFIG.PCW_MIO_52_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_52_PULLUP {enabled} \
   CONFIG.PCW_MIO_52_SLEW {slow} \
   CONFIG.PCW_MIO_53_DIRECTION {in} \
   CONFIG.PCW_MIO_53_IOTYPE {LVCMOS 1.8V} \
   CONFIG.PCW_MIO_53_PULLUP {enabled} \
   CONFIG.PCW_MIO_53_SLEW {slow} \
   CONFIG.PCW_MIO_5_DIRECTION {inout} \
   CONFIG.PCW_MIO_5_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_5_PULLUP {disabled} \
   CONFIG.PCW_MIO_5_SLEW {slow} \
   CONFIG.PCW_MIO_6_DIRECTION {out} \
   CONFIG.PCW_MIO_6_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_6_PULLUP {disabled} \
   CONFIG.PCW_MIO_6_SLEW {slow} \
   CONFIG.PCW_MIO_7_DIRECTION {out} \
   CONFIG.PCW_MIO_7_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_7_PULLUP {disabled} \
   CONFIG.PCW_MIO_7_SLEW {slow} \
   CONFIG.PCW_MIO_8_DIRECTION {out} \
   CONFIG.PCW_MIO_8_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_8_PULLUP {disabled} \
   CONFIG.PCW_MIO_8_SLEW {slow} \
   CONFIG.PCW_MIO_9_DIRECTION {inout} \
   CONFIG.PCW_MIO_9_IOTYPE {LVCMOS 3.3V} \
   CONFIG.PCW_MIO_9_PULLUP {enabled} \
   CONFIG.PCW_MIO_9_SLEW {slow} \
   CONFIG.PCW_MIO_PRIMITIVE {54} \
   CONFIG.PCW_MIO_TREE_PERIPHERALS {USB Reset#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#Quad SPI Flash#GPIO#Quad SPI Flash#GPIO#SPI 1#SPI 1#SPI 1#SPI 1#SPI 1#SPI 1#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#GPIO#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#USB 0#SD 0#SD 0#SD 0#SD 0#SD 0#SD 0#GPIO#GPIO#I2C 1#I2C 1#I2C 0#I2C 0#UART 1#UART 1} \
   CONFIG.PCW_MIO_TREE_SIGNALS {reset#qspi0_ss_b#qspi0_io[0]#qspi0_io[1]#qspi0_io[2]#qspi0_io[3]/HOLD_B#qspi0_sclk#gpio[7]#qspi_fbclk#gpio[9]#mosi#miso#sclk#ss[0]#ss[1]#ss[2]#gpio[16]#gpio[17]#gpio[18]#gpio[19]#gpio[20]#gpio[21]#gpio[22]#gpio[23]#gpio[24]#gpio[25]#gpio[26]#gpio[27]#data[4]#dir#stp#nxt#data[0]#data[1]#data[2]#data[3]#clk#data[5]#data[6]#data[7]#clk#cmd#data[0]#data[1]#data[2]#data[3]#gpio[46]#gpio[47]#scl#sda#scl#sda#tx#rx} \
   CONFIG.PCW_M_AXI_GP0_ENABLE_STATIC_REMAP {0} \
   CONFIG.PCW_M_AXI_GP0_ID_WIDTH {12} \
   CONFIG.PCW_M_AXI_GP0_SUPPORT_NARROW_BURST {0} \
   CONFIG.PCW_M_AXI_GP0_THREAD_ID_WIDTH {12} \
   CONFIG.PCW_NAND_CYCLES_T_AR {1} \
   CONFIG.PCW_NAND_CYCLES_T_CLR {1} \
   CONFIG.PCW_NAND_CYCLES_T_RC {11} \
   CONFIG.PCW_NAND_CYCLES_T_REA {1} \
   CONFIG.PCW_NAND_CYCLES_T_RR {1} \
   CONFIG.PCW_NAND_CYCLES_T_WC {11} \
   CONFIG.PCW_NAND_CYCLES_T_WP {1} \
   CONFIG.PCW_NAND_GRP_D8_ENABLE {0} \
   CONFIG.PCW_NAND_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_CS0_T_PC {1} \
   CONFIG.PCW_NOR_CS0_T_RC {11} \
   CONFIG.PCW_NOR_CS0_T_TR {1} \
   CONFIG.PCW_NOR_CS0_T_WC {11} \
   CONFIG.PCW_NOR_CS0_T_WP {1} \
   CONFIG.PCW_NOR_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_CS1_T_PC {1} \
   CONFIG.PCW_NOR_CS1_T_RC {11} \
   CONFIG.PCW_NOR_CS1_T_TR {1} \
   CONFIG.PCW_NOR_CS1_T_WC {11} \
   CONFIG.PCW_NOR_CS1_T_WP {1} \
   CONFIG.PCW_NOR_CS1_WE_TIME {0} \
   CONFIG.PCW_NOR_GRP_A25_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS0_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_CS1_ENABLE {0} \
   CONFIG.PCW_NOR_GRP_SRAM_INT_ENABLE {0} \
   CONFIG.PCW_NOR_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_NOR_SRAM_CS0_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_RC {11} \
   CONFIG.PCW_NOR_SRAM_CS0_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WC {11} \
   CONFIG.PCW_NOR_SRAM_CS0_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS0_WE_TIME {0} \
   CONFIG.PCW_NOR_SRAM_CS1_T_CEOE {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_PC {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_RC {11} \
   CONFIG.PCW_NOR_SRAM_CS1_T_TR {1} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WC {11} \
   CONFIG.PCW_NOR_SRAM_CS1_T_WP {1} \
   CONFIG.PCW_NOR_SRAM_CS1_WE_TIME {0} \
   CONFIG.PCW_OVERRIDE_BASIC_CLOCK {0} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY0 {0.101} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY1 {0.097} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY2 {0.102} \
   CONFIG.PCW_PACKAGE_DDR_BOARD_DELAY3 {0.106} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_0 {0.005} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_1 {0.016} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_2 {0.001} \
   CONFIG.PCW_PACKAGE_DDR_DQS_TO_CLK_DELAY_3 {-0.009} \
   CONFIG.PCW_PACKAGE_NAME {fbg676} \
   CONFIG.PCW_PCAP_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_PCAP_PERIPHERAL_DIVISOR0 {6} \
   CONFIG.PCW_PCAP_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_PERIPHERAL_BOARD_PRESET {None} \
   CONFIG.PCW_PJTAG_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_PLL_BYPASSMODE_ENABLE {0} \
   CONFIG.PCW_PRESET_BANK0_VOLTAGE {LVCMOS 3.3V} \
   CONFIG.PCW_PRESET_BANK1_VOLTAGE {LVCMOS 1.8V} \
   CONFIG.PCW_PS7_SI_REV {PRODUCTION} \
   CONFIG.PCW_QSPI_GRP_FBCLK_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_FBCLK_IO {MIO 8} \
   CONFIG.PCW_QSPI_GRP_IO1_ENABLE {0} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_ENABLE {1} \
   CONFIG.PCW_QSPI_GRP_SINGLE_SS_IO {MIO 1 .. 6} \
   CONFIG.PCW_QSPI_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_QSPI_INTERNAL_HIGHADDRESS {0xFDFFFFFF} \
   CONFIG.PCW_QSPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_QSPI_PERIPHERAL_DIVISOR0 {6} \
   CONFIG.PCW_QSPI_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_QSPI_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_QSPI_QSPI_IO {MIO 1 .. 6} \
   CONFIG.PCW_SD0_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD0_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SD0_SD0_IO {MIO 40 .. 45} \
   CONFIG.PCW_SD1_GRP_CD_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_POW_ENABLE {0} \
   CONFIG.PCW_SD1_GRP_WP_ENABLE {0} \
   CONFIG.PCW_SD1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SDIO0_BASEADDR {0xE0100000} \
   CONFIG.PCW_SDIO0_HIGHADDR {0xE0100FFF} \
   CONFIG.PCW_SDIO_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SDIO_PERIPHERAL_DIVISOR0 {12} \
   CONFIG.PCW_SDIO_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_SDIO_PERIPHERAL_VALID {1} \
   CONFIG.PCW_SINGLE_QSPI_DATA_MODE {x4} \
   CONFIG.PCW_SMC_CYCLE_T0 {NA} \
   CONFIG.PCW_SMC_CYCLE_T1 {NA} \
   CONFIG.PCW_SMC_CYCLE_T2 {NA} \
   CONFIG.PCW_SMC_CYCLE_T3 {NA} \
   CONFIG.PCW_SMC_CYCLE_T4 {NA} \
   CONFIG.PCW_SMC_CYCLE_T5 {NA} \
   CONFIG.PCW_SMC_CYCLE_T6 {NA} \
   CONFIG.PCW_SMC_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SMC_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_SMC_PERIPHERAL_VALID {0} \
   CONFIG.PCW_SPI0_GRP_SS0_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS0_IO {<Select>} \
   CONFIG.PCW_SPI0_GRP_SS1_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS1_IO {<Select>} \
   CONFIG.PCW_SPI0_GRP_SS2_ENABLE {0} \
   CONFIG.PCW_SPI0_GRP_SS2_IO {<Select>} \
   CONFIG.PCW_SPI0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_SPI0_SPI0_IO {<Select>} \
   CONFIG.PCW_SPI1_GRP_SS0_ENABLE {1} \
   CONFIG.PCW_SPI1_GRP_SS0_IO {MIO 13} \
   CONFIG.PCW_SPI1_GRP_SS1_ENABLE {1} \
   CONFIG.PCW_SPI1_GRP_SS1_IO {MIO 14} \
   CONFIG.PCW_SPI1_GRP_SS2_ENABLE {1} \
   CONFIG.PCW_SPI1_GRP_SS2_IO {MIO 15} \
   CONFIG.PCW_SPI1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_SPI1_SPI1_IO {MIO 10 .. 15} \
   CONFIG.PCW_SPI_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_SPI_PERIPHERAL_DIVISOR0 {8} \
   CONFIG.PCW_SPI_PERIPHERAL_FREQMHZ {150} \
   CONFIG.PCW_SPI_PERIPHERAL_VALID {1} \
   CONFIG.PCW_TPIU_PERIPHERAL_CLKSRC {External} \
   CONFIG.PCW_TPIU_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TPIU_PERIPHERAL_FREQMHZ {200} \
   CONFIG.PCW_TRACE_GRP_16BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_2BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_32BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_4BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_GRP_8BIT_ENABLE {0} \
   CONFIG.PCW_TRACE_INTERNAL_WIDTH {2} \
   CONFIG.PCW_TRACE_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC0_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_TTC0_TTC0_IO {EMIO} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK0_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK1_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_TTC1_CLK2_PERIPHERAL_FREQMHZ {133.333333} \
   CONFIG.PCW_TTC1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_TTC_PERIPHERAL_FREQMHZ {50} \
   CONFIG.PCW_UART0_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART0_GRP_FULL_IO {<Select>} \
   CONFIG.PCW_UART0_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_UART0_UART0_IO {<Select>} \
   CONFIG.PCW_UART1_BASEADDR {0xE0001000} \
   CONFIG.PCW_UART1_BAUD_RATE {115200} \
   CONFIG.PCW_UART1_GRP_FULL_ENABLE {0} \
   CONFIG.PCW_UART1_HIGHADDR {0xE0001FFF} \
   CONFIG.PCW_UART1_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_UART1_UART1_IO {MIO 52 .. 53} \
   CONFIG.PCW_UART_PERIPHERAL_CLKSRC {IO PLL} \
   CONFIG.PCW_UART_PERIPHERAL_DIVISOR0 {12} \
   CONFIG.PCW_UART_PERIPHERAL_FREQMHZ {100} \
   CONFIG.PCW_UART_PERIPHERAL_VALID {1} \
   CONFIG.PCW_UIPARAM_ACT_DDR_FREQ_MHZ {533.333374} \
   CONFIG.PCW_UIPARAM_DDR_ADV_ENABLE {0} \
   CONFIG.PCW_UIPARAM_DDR_AL {0} \
   CONFIG.PCW_UIPARAM_DDR_BANK_ADDR_COUNT {3} \
   CONFIG.PCW_UIPARAM_DDR_BL {8} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY0 {0.300} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY1 {0.300} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY2 {0.360} \
   CONFIG.PCW_UIPARAM_DDR_BOARD_DELAY3 {0.360} \
   CONFIG.PCW_UIPARAM_DDR_BUS_WIDTH {32 Bit} \
   CONFIG.PCW_UIPARAM_DDR_CL {7} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PACKAGE_LENGTH {102.799} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_CLOCK_STOP_EN {0} \
   CONFIG.PCW_UIPARAM_DDR_COL_ADDR_COUNT {10} \
   CONFIG.PCW_UIPARAM_DDR_CWL {6} \
   CONFIG.PCW_UIPARAM_DDR_DEVICE_CAPACITY {4096 MBits} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PACKAGE_LENGTH {97.8785} \
   CONFIG.PCW_UIPARAM_DDR_DQS_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PACKAGE_LENGTH {86.7455} \
   CONFIG.PCW_UIPARAM_DDR_DQS_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PACKAGE_LENGTH {102.1345} \
   CONFIG.PCW_UIPARAM_DDR_DQS_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PACKAGE_LENGTH {111.4085} \
   CONFIG.PCW_UIPARAM_DDR_DQS_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_0 {0.090} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_1 {0.080} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_2 {0.160} \
   CONFIG.PCW_UIPARAM_DDR_DQS_TO_CLK_DELAY_3 {0.170} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PACKAGE_LENGTH {99.127} \
   CONFIG.PCW_UIPARAM_DDR_DQ_0_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PACKAGE_LENGTH {91.501} \
   CONFIG.PCW_UIPARAM_DDR_DQ_1_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PACKAGE_LENGTH {100.241} \
   CONFIG.PCW_UIPARAM_DDR_DQ_2_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_LENGTH_MM {0} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PACKAGE_LENGTH {110.0845} \
   CONFIG.PCW_UIPARAM_DDR_DQ_3_PROPOGATION_DELAY {160} \
   CONFIG.PCW_UIPARAM_DDR_DRAM_WIDTH {16 Bits} \
   CONFIG.PCW_UIPARAM_DDR_ECC {Disabled} \
   CONFIG.PCW_UIPARAM_DDR_ENABLE {1} \
   CONFIG.PCW_UIPARAM_DDR_FREQ_MHZ {533.333333} \
   CONFIG.PCW_UIPARAM_DDR_HIGH_TEMP {Normal (0-85)} \
   CONFIG.PCW_UIPARAM_DDR_MEMORY_TYPE {DDR 3} \
   CONFIG.PCW_UIPARAM_DDR_PARTNO {MT41J256M16 RE-125} \
   CONFIG.PCW_UIPARAM_DDR_ROW_ADDR_COUNT {15} \
   CONFIG.PCW_UIPARAM_DDR_SPEED_BIN {DDR3_1066F} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_DATA_EYE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_READ_GATE {1} \
   CONFIG.PCW_UIPARAM_DDR_TRAIN_WRITE_LEVEL {1} \
   CONFIG.PCW_UIPARAM_DDR_T_FAW {40.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RAS_MIN {35.0} \
   CONFIG.PCW_UIPARAM_DDR_T_RC {48.91} \
   CONFIG.PCW_UIPARAM_DDR_T_RCD {7} \
   CONFIG.PCW_UIPARAM_DDR_T_RP {7} \
   CONFIG.PCW_UIPARAM_DDR_USE_INTERNAL_VREF {0} \
   CONFIG.PCW_UIPARAM_GENERATE_SUMMARY {NA} \
   CONFIG.PCW_USB0_PERIPHERAL_ENABLE {1} \
   CONFIG.PCW_USB0_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB0_RESET_ENABLE {1} \
   CONFIG.PCW_USB0_RESET_IO {MIO 0} \
   CONFIG.PCW_USB0_USB0_IO {MIO 28 .. 39} \
   CONFIG.PCW_USB1_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_USB1_PERIPHERAL_FREQMHZ {60} \
   CONFIG.PCW_USB1_RESET_ENABLE {0} \
   CONFIG.PCW_USB_RESET_ENABLE {1} \
   CONFIG.PCW_USB_RESET_POLARITY {Active Low} \
   CONFIG.PCW_USB_RESET_SELECT {Share reset pin} \
   CONFIG.PCW_USE_AXI_FABRIC_IDLE {0} \
   CONFIG.PCW_USE_AXI_NONSECURE {0} \
   CONFIG.PCW_USE_CORESIGHT {0} \
   CONFIG.PCW_USE_CROSS_TRIGGER {0} \
   CONFIG.PCW_USE_CR_FABRIC {1} \
   CONFIG.PCW_USE_DDR_BYPASS {0} \
   CONFIG.PCW_USE_DEBUG {0} \
   CONFIG.PCW_USE_DMA0 {0} \
   CONFIG.PCW_USE_DMA1 {0} \
   CONFIG.PCW_USE_DMA2 {0} \
   CONFIG.PCW_USE_DMA3 {0} \
   CONFIG.PCW_USE_EXPANDED_IOP {0} \
   CONFIG.PCW_USE_FABRIC_INTERRUPT {1} \
   CONFIG.PCW_USE_HIGH_OCM {0} \
   CONFIG.PCW_USE_M_AXI_GP0 {1} \
   CONFIG.PCW_USE_M_AXI_GP1 {0} \
   CONFIG.PCW_USE_PROC_EVENT_BUS {0} \
   CONFIG.PCW_USE_PS_SLCR_REGISTERS {0} \
   CONFIG.PCW_USE_S_AXI_ACP {0} \
   CONFIG.PCW_USE_S_AXI_GP0 {0} \
   CONFIG.PCW_USE_S_AXI_GP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP0 {0} \
   CONFIG.PCW_USE_S_AXI_HP1 {0} \
   CONFIG.PCW_USE_S_AXI_HP2 {0} \
   CONFIG.PCW_USE_S_AXI_HP3 {0} \
   CONFIG.PCW_USE_TRACE {0} \
   CONFIG.PCW_VALUE_SILVERSION {3} \
   CONFIG.PCW_WDT_PERIPHERAL_CLKSRC {CPU_1X} \
   CONFIG.PCW_WDT_PERIPHERAL_DIVISOR0 {1} \
   CONFIG.PCW_WDT_PERIPHERAL_ENABLE {0} \
   CONFIG.PCW_WDT_PERIPHERAL_FREQMHZ {133.333333} \
 ] $ps_0

  # Create instance: register_bank_0
  create_hier_cell_register_bank_0 [current_bd_instance .] register_bank_0

  # Create instance: reset_generator_0, and set properties
  set reset_generator_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:reset_generator reset_generator_0 ]
  set_property -dict [ list \
   CONFIG.CGN_INPUT_RESET_POLARITY {0} \
   CONFIG.CGN_NR_OF_BUS_RST {1} \
   CONFIG.CGN_NR_OF_BUS_RST_N {1} \
   CONFIG.CGN_NR_OF_PERIPHERAL_RST {1} \
   CONFIG.CGN_NR_OF_PERIPHERAL_RST_N {1} \
 ] $reset_generator_0

  # Create instance: sc_io_0, and set properties
  set sc_io_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:sc_io sc_io_0 ]

  # Create instance: tr_sync_manager_0, and set properties
  set tr_sync_manager_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:tr_sync_manager tr_sync_manager_0 ]

  # Create instance: trigger_manager_0, and set properties
  set trigger_manager_0 [ create_bd_cell -type ip -vlnv PSI:psi_3205:trigger_manager trigger_manager_0 ]

  # Create instance: util_ds_buf_fpga_clk, and set properties
  set util_ds_buf_fpga_clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_fpga_clk ]

  # Create instance: util_ds_buf_wdb_clk, and set properties
  set util_ds_buf_wdb_clk [ create_bd_cell -type ip -vlnv xilinx.com:ip:util_ds_buf util_ds_buf_wdb_clk ]

  # Create instance: xlconcat_0, and set properties
  set xlconcat_0 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_0 ]

  # Create instance: xlconcat_1, and set properties
  set xlconcat_1 [ create_bd_cell -type ip -vlnv xilinx.com:ip:xlconcat xlconcat_1 ]
  set_property -dict [ list \
   CONFIG.NUM_PORTS {6} \
 ] $xlconcat_1

  # Create interface connections
  connect_bd_intf_net -intf_net S00_AXI_1 [get_bd_intf_pins axi_interconnect_0/S00_AXI] [get_bd_intf_pins ps_0/M_AXI_GP0]
  connect_bd_intf_net -intf_net S00_AXI_2 [get_bd_intf_pins axi_interconnect_0/M00_AXI] [get_bd_intf_pins register_bank_0/S00_AXI]
  connect_bd_intf_net -intf_net axi_interconnect_0_M01_AXI [get_bd_intf_pins axi_interconnect_0/M01_AXI] [get_bd_intf_pins axi_quad_spi_to_bpl_0/AXI_LITE]
  connect_bd_intf_net -intf_net axi_interconnect_0_M02_AXI [get_bd_intf_pins axi_interconnect_0/M02_AXI] [get_bd_intf_pins clock_measure_0/s00_axi]
  connect_bd_intf_net -intf_net gig_ethernet_pcs_pma_0_sfp [get_bd_intf_ports ETH0_SFP] [get_bd_intf_pins gig_ethernet_pcs_pma_0/sfp]
  connect_bd_intf_net -intf_net gig_ethernet_pcs_pma_1_sfp [get_bd_intf_ports ETH1_SFP] [get_bd_intf_pins gig_ethernet_pcs_pma_1/sfp]
  connect_bd_intf_net -intf_net ps_0_DDR [get_bd_intf_ports DDR] [get_bd_intf_pins ps_0/DDR]
  connect_bd_intf_net -intf_net ps_0_FIXED_IO [get_bd_intf_ports FIXED_IO] [get_bd_intf_pins ps_0/FIXED_IO]

  # Create port connections
  connect_bd_net -net BOARD_SELECT_N_I_1 [get_bd_ports BOARD_SELECT_N_I] [get_bd_pins sc_io_0/BPL_BOARD_SELECT_N_I]
  connect_bd_net -net BUSY_IN_N_I_1 [get_bd_ports BUSY_IN_N_I] [get_bd_pins busy_manager_0/BUSY_FROM_BPL_N_I] [get_bd_pins register_bank_0/BUSY_IN_N_I]
  connect_bd_net -net CLK_FPGA_N_I_1 [get_bd_ports CLK_FPGA_N_I] [get_bd_pins util_ds_buf_fpga_clk/IBUF_DS_N]
  connect_bd_net -net CLK_FPGA_P_I_1 [get_bd_ports CLK_FPGA_P_I] [get_bd_pins util_ds_buf_fpga_clk/IBUF_DS_P]
  connect_bd_net -net Din_1 [get_bd_pins bus_split_spi_ss_0/Din] [get_bd_pins sc_io_0/BPL_BOARD_SELECT_N_O]
  connect_bd_net -net EXT_CLK_N_I_1 [get_bd_ports WDB_CLK_N_I] [get_bd_pins util_ds_buf_wdb_clk/IBUF_DS_N]
  connect_bd_net -net EXT_CLK_P_I_1 [get_bd_ports WDB_CLK_P_I] [get_bd_pins util_ds_buf_wdb_clk/IBUF_DS_P]
  connect_bd_net -net EXT_TRIG_IN_I_1 [get_bd_ports EXT_TRG_IN_I] [get_bd_pins trigger_manager_0/TRIGGER_MCX_I]
  connect_bd_net -net FS_INIT_N_I_1 [get_bd_ports FS_INIT_N_I] [get_bd_pins sc_io_0/BPL_FS_INIT_N_I]
  connect_bd_net -net GTREFCLK0_N_I_1 [get_bd_ports GTREFCLK0_N_I] [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_n]
  connect_bd_net -net GTREFCLK0_P_I_1 [get_bd_ports GTREFCLK0_P_I] [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_p]
  connect_bd_net -net LD_I_1 [get_bd_ports LD_I] [get_bd_pins register_bank_0/LD_I]
  connect_bd_net -net MASTER_SPI_MISO_I_1 [get_bd_ports MASTER_SPI_MISO_I] [get_bd_pins axi_quad_spi_to_bpl_0/io1_i]
  connect_bd_net -net SFP_1_LOS_I_1 [get_bd_ports SFP_1_LOS_I] [get_bd_pins sc_io_0/SFP_1_LOS_I]
  connect_bd_net -net SFP_1_MOD_I_1 [get_bd_ports SFP_1_MOD_I] [get_bd_pins sc_io_0/SFP_1_MOD_I]
  connect_bd_net -net SFP_1_TX_FAULT_I_1 [get_bd_ports SFP_1_TX_FAULT_I] [get_bd_pins sc_io_0/SFP_1_TX_FAULT_I]
  connect_bd_net -net SFP_2_LOS_I_1 [get_bd_ports SFP_2_LOS_I] [get_bd_pins sc_io_0/SFP_2_LOS_I]
  connect_bd_net -net SFP_2_MOD_I_1 [get_bd_ports SFP_2_MOD_I] [get_bd_pins sc_io_0/SFP_2_MOD_I]
  connect_bd_net -net SFP_2_TX_FAULT_I_1 [get_bd_ports SFP_2_TX_FAULT_I] [get_bd_pins sc_io_0/SFP_2_TX_FAULT_I]
  connect_bd_net -net SPI_CS_FPGA_N_I_1 [get_bd_ports SPI_CS_FPGA_N_I] [get_bd_pins sc_io_0/BPL_SPI_CS_FPGA_N_I]
  connect_bd_net -net SPR_N_I_1 [get_bd_ports SPR_N_I] [get_bd_pins trigger_manager_0/TRIGGER_SDATA_N_I]
  connect_bd_net -net SPR_P_I_1 [get_bd_ports SPR_P_I] [get_bd_pins trigger_manager_0/TRIGGER_SDATA_P_I]
  connect_bd_net -net SYNC_N_I_1 [get_bd_ports SYNC_N_I] [get_bd_pins tr_sync_manager_0/TR_SYNC_FCI_N_I]
  connect_bd_net -net SYNC_P_I_1 [get_bd_ports SYNC_P_I] [get_bd_pins tr_sync_manager_0/TR_SYNC_FCI_P_I]
  connect_bd_net -net TRG_N_I_1 [get_bd_ports TRG_N_I] [get_bd_pins trigger_manager_0/TRIGGER_FCI_N_I]
  connect_bd_net -net TRG_P_I_1 [get_bd_ports TRG_P_I] [get_bd_pins trigger_manager_0/TRIGGER_FCI_P_I]
  connect_bd_net -net axi_dcb_register_bank_0_DAQ_SOFT_TRIGGER_O [get_bd_pins register_bank_0/DAQ_SOFT_TRIGGER_O] [get_bd_pins trigger_manager_0/TRIGGER_PS_I]
  connect_bd_net -net axi_dcb_register_bank_0_LMK_CLK_SRC_SEL_O [get_bd_ports CLK_SEL_O] [get_bd_pins register_bank_0/CLK_SEL_O]
  connect_bd_net -net axi_dcb_register_bank_0_SYNC_DELAY_O [get_bd_pins register_bank_0/SYNC_DELAY_O] [get_bd_pins tr_sync_manager_0/DELAY_I]
  connect_bd_net -net axi_dcb_register_bank_0_TRIGGER_MGR_RST_O [get_bd_pins register_bank_0/TRIGGER_MGR_RST_O] [get_bd_pins trigger_manager_0/RST_I]
  connect_bd_net -net axi_dcb_register_bank_0_TR_SYNC_BPL_O [get_bd_pins register_bank_0/TR_SYNC_BPL_O] [get_bd_pins tr_sync_manager_0/TR_SYNC_PS_I]
  connect_bd_net -net axi_dcb_register_bank_0_WDB_CLK_MGR_RST_O [get_bd_pins clk_wiz_0/reset] [get_bd_pins register_bank_0/WDB_CLK_MGR_RST_O]
  connect_bd_net -net axi_quad_spi_to_bpl_0_io0_o [get_bd_ports MASTER_SPI_MOSI_O] [get_bd_pins axi_quad_spi_to_bpl_0/io0_o]
  connect_bd_net -net axi_quad_spi_to_bpl_0_ip2intc_irpt [get_bd_pins axi_quad_spi_to_bpl_0/ip2intc_irpt] [get_bd_pins ps_0/IRQ_F2P]
  connect_bd_net -net axi_quad_spi_to_bpl_0_sck_o [get_bd_ports MASTER_SPI_CLK_O] [get_bd_pins axi_quad_spi_to_bpl_0/sck_o]
  connect_bd_net -net axi_quad_spi_to_bpl_0_ss_o [get_bd_pins axi_quad_spi_to_bpl_0/ss_o] [get_bd_pins sc_io_0/SPI_CS_N_I]
  connect_bd_net -net busy_manager_0_BUSY_TO_BPL_N_O [get_bd_ports BUSY_OUT_N_O] [get_bd_pins busy_manager_0/BUSY_TO_BPL_N_O]
  connect_bd_net -net busy_manager_0_BUSY_TO_FCI_N_O [get_bd_ports BUSY_N_N_O] [get_bd_pins busy_manager_0/BUSY_TO_FCI_N_N_O]
  connect_bd_net -net busy_manager_0_BUSY_TO_FCI_P_O [get_bd_ports BUSY_N_P_O] [get_bd_pins busy_manager_0/BUSY_TO_FCI_N_P_O]
  connect_bd_net -net clk_wiz_0_clk_out_240MHz [get_bd_pins clk_wiz_0/clk_out_240MHz] [get_bd_pins tr_sync_manager_0/DLY_CLK_I]
  connect_bd_net -net clk_wiz_0_locked [get_bd_pins clk_wiz_0/locked] [get_bd_pins register_bank_0/WDB_CLK_MGR_LOCK_I]
  connect_bd_net -net const_aux_clk_n_dout [get_bd_ports AUX_OUT_N_O] [get_bd_pins hier_const_aux_clk/AUX_OUT_N_O]
  connect_bd_net -net const_aux_clk_p_dout [get_bd_ports AUX_OUT_P_O] [get_bd_pins hier_const_aux_clk/AUX_OUT_P_O]
  connect_bd_net -net const_ce_dis_1_dout [get_bd_ports CE_DIS_1_O] [get_bd_pins hier_const_clk_distr_ctrl/CE_DIS_1_O]
  connect_bd_net -net const_ce_dis_2_dout [get_bd_ports CE_DIS_2_O] [get_bd_pins hier_const_clk_distr_ctrl/CE_DIS_2_O]
  connect_bd_net -net const_clk_dis_1_dout [get_bd_ports CLK_DIS_1_O] [get_bd_pins hier_const_clk_distr_ctrl/CLK_DIS_1_O]
  connect_bd_net -net const_clk_dis_2_dout [get_bd_ports CLK_DIS_2_O] [get_bd_pins hier_const_clk_distr_ctrl/CLK_DIS_2_O]
  connect_bd_net -net const_dir_ext_trig_in_dout [get_bd_ports DIR_EXT_TRG_IN_O] [get_bd_pins const_dir_ext_trig_0/DIR_EXT_TRG_IN_O]
  connect_bd_net -net const_dir_ext_trig_out_dout [get_bd_ports DIR_EXT_TRG_OUT_O] [get_bd_pins const_dir_ext_trig_0/DIR_EXT_TRG_OUT_O]
  connect_bd_net -net const_eth0_phy_addr1_dout [get_bd_pins const_eth1_phy_addr/dout] [get_bd_pins gig_ethernet_pcs_pma_1/phyaddr]
  connect_bd_net -net const_eth0_phy_addr_dout [get_bd_pins const_eth0_phy_addr/dout] [get_bd_pins gig_ethernet_pcs_pma_0/phyaddr]
  connect_bd_net -net const_eth_an_adv_cfg_vec_dout [get_bd_pins eth_constants_0/dout4] [get_bd_pins gig_ethernet_pcs_pma_0/an_adv_config_vector] [get_bd_pins gig_ethernet_pcs_pma_1/an_adv_config_vector]
  connect_bd_net -net const_eth_cfg_vec_dout [get_bd_pins eth_constants_0/dout3] [get_bd_pins gig_ethernet_pcs_pma_0/configuration_vector] [get_bd_pins gig_ethernet_pcs_pma_1/configuration_vector]
  connect_bd_net -net const_eth_col_dout [get_bd_pins eth_constants_0/dout2] [get_bd_pins ps_0/ENET0_GMII_COL] [get_bd_pins ps_0/ENET1_GMII_COL]
  connect_bd_net -net const_eth_crs_dout [get_bd_pins eth_constants_0/dout5] [get_bd_pins ps_0/ENET0_GMII_CRS] [get_bd_pins ps_0/ENET1_GMII_CRS]
  connect_bd_net -net const_eth_gp_0_dout [get_bd_pins eth_constants_0/dout0] [get_bd_pins gig_ethernet_pcs_pma_0/an_adv_config_val] [get_bd_pins gig_ethernet_pcs_pma_0/an_restart_config] [get_bd_pins gig_ethernet_pcs_pma_0/configuration_valid] [get_bd_pins gig_ethernet_pcs_pma_1/an_adv_config_val] [get_bd_pins gig_ethernet_pcs_pma_1/an_restart_config] [get_bd_pins gig_ethernet_pcs_pma_1/configuration_valid]
  connect_bd_net -net const_eth_signal_detect_dout [get_bd_pins eth_constants_0/dout1] [get_bd_pins gig_ethernet_pcs_pma_0/signal_detect] [get_bd_pins gig_ethernet_pcs_pma_1/signal_detect]
  connect_bd_net -net const_info_dir_dout [get_bd_ports INFO_DIR_O] [get_bd_pins hier_const_bpl_dir/INFO_DIR_O]
  connect_bd_net -net const_led_hw_error_n_dout [get_bd_pins const_led_0/dout] [get_bd_pins led_ctrl_0/HW_ERROR_N_I]
  connect_bd_net -net const_led_hw_status_dout [get_bd_pins const_led_0/dout1] [get_bd_pins led_ctrl_0/HW_STATUS_I]
  connect_bd_net -net const_mscb_data_out_dout [get_bd_ports MSCB_DATA_O] [get_bd_pins const_bpl_signals_0/MSCB_DATA_O]
  connect_bd_net -net const_mscb_drv_en_dout [get_bd_ports MSCB_DRV_EN_O] [get_bd_pins const_bpl_signals_0/MSCB_DRV_EN_O]
  connect_bd_net -net const_slave_spi_miso_dout [get_bd_ports SLAVE_SPI_MISO_O] [get_bd_pins const_bpl_signals_0/SLAVE_SPI_MISO_O]
  connect_bd_net -net const_slave_spi_miso_en_n_dout [get_bd_ports SLAVE_SPI_MISO_EN_N_O] [get_bd_pins const_bpl_signals_0/SLAVE_SPI_MISO_EN_N_O]
  connect_bd_net -net const_sync_dir_dout [get_bd_ports SYNC_DIR_O] [get_bd_pins hier_const_bpl_dir/SYNC_DIR_O]
  connect_bd_net -net const_trg_dir_dout [get_bd_ports TRG_DIR_O] [get_bd_pins hier_const_bpl_dir/TRG_DIR_O]
  connect_bd_net -net eth_constants_0_dout6 [get_bd_pins eth_constants_0/dout6] [get_bd_pins gig_ethernet_pcs_pma_0/basex_or_sgmii] [get_bd_pins gig_ethernet_pcs_pma_1/basex_or_sgmii]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rx_dv [get_bd_pins gig_ethernet_pcs_pma_0/gmii_rx_dv] [get_bd_pins ps_0/ENET0_GMII_RX_DV]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rx_er [get_bd_pins gig_ethernet_pcs_pma_0/gmii_rx_er] [get_bd_pins ps_0/ENET0_GMII_RX_ER]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gmii_rxd [get_bd_pins gig_ethernet_pcs_pma_0/gmii_rxd] [get_bd_pins ps_0/ENET0_GMII_RXD]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gt0_qplloutclk_out [get_bd_pins gig_ethernet_pcs_pma_0/gt0_qplloutclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/gt0_qplloutclk_in]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gt0_qplloutrefclk_out [get_bd_pins gig_ethernet_pcs_pma_0/gt0_qplloutrefclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/gt0_qplloutrefclk_in]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gtrefclk_bufg_out [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_bufg_out] [get_bd_pins gig_ethernet_pcs_pma_1/gtrefclk_bufg] [get_bd_pins xlconcat_1/In0]
  connect_bd_net -net gig_ethernet_pcs_pma_0_gtrefclk_out [get_bd_pins gig_ethernet_pcs_pma_0/gtrefclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/gtrefclk]
  connect_bd_net -net gig_ethernet_pcs_pma_0_mdio_o [get_bd_pins gig_ethernet_pcs_pma_0/mdio_o] [get_bd_pins ps_0/ENET0_MDIO_I]
  connect_bd_net -net gig_ethernet_pcs_pma_0_mmcm_locked_out [get_bd_pins gig_ethernet_pcs_pma_0/mmcm_locked_out] [get_bd_pins gig_ethernet_pcs_pma_1/mmcm_locked]
  connect_bd_net -net gig_ethernet_pcs_pma_0_pma_reset_out [get_bd_pins gig_ethernet_pcs_pma_0/pma_reset_out] [get_bd_pins gig_ethernet_pcs_pma_1/pma_reset]
  connect_bd_net -net gig_ethernet_pcs_pma_0_sgmii_clk_r [get_bd_pins gig_ethernet_pcs_pma_0/sgmii_clk_r] [get_bd_pins ps_0/ENET0_GMII_RX_CLK] [get_bd_pins ps_0/ENET0_GMII_TX_CLK] [get_bd_pins xlconcat_1/In3]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk2_out [get_bd_pins gig_ethernet_pcs_pma_0/userclk2_out] [get_bd_pins gig_ethernet_pcs_pma_1/userclk2] [get_bd_pins xlconcat_1/In2]
  connect_bd_net -net gig_ethernet_pcs_pma_0_userclk_out [get_bd_pins gig_ethernet_pcs_pma_0/userclk_out] [get_bd_pins gig_ethernet_pcs_pma_1/rxuserclk] [get_bd_pins gig_ethernet_pcs_pma_1/rxuserclk2] [get_bd_pins gig_ethernet_pcs_pma_1/userclk] [get_bd_pins xlconcat_1/In1]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rx_dv [get_bd_pins gig_ethernet_pcs_pma_1/gmii_rx_dv] [get_bd_pins ps_0/ENET1_GMII_RX_DV]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rx_er [get_bd_pins gig_ethernet_pcs_pma_1/gmii_rx_er] [get_bd_pins ps_0/ENET1_GMII_RX_ER]
  connect_bd_net -net gig_ethernet_pcs_pma_1_gmii_rxd [get_bd_pins gig_ethernet_pcs_pma_1/gmii_rxd] [get_bd_pins ps_0/ENET1_GMII_RXD]
  connect_bd_net -net gig_ethernet_pcs_pma_1_mdio_o [get_bd_pins gig_ethernet_pcs_pma_1/mdio_o] [get_bd_pins ps_0/ENET1_MDIO_I]
  connect_bd_net -net gig_ethernet_pcs_pma_1_sgmii_clk_r [get_bd_pins gig_ethernet_pcs_pma_1/sgmii_clk_r] [get_bd_pins ps_0/ENET1_GMII_RX_CLK] [get_bd_pins ps_0/ENET1_GMII_TX_CLK] [get_bd_pins xlconcat_1/In4]
  connect_bd_net -net led_ctrl_0_LED_B_N_O [get_bd_ports LED_B_N_O] [get_bd_pins led_ctrl_0/LED_B_N_O]
  connect_bd_net -net led_ctrl_0_LED_G_N_O [get_bd_ports LED_G_N_O] [get_bd_pins led_ctrl_0/LED_G_N_O]
  connect_bd_net -net led_ctrl_0_LED_R_N_O [get_bd_ports LED_R_N_O] [get_bd_pins led_ctrl_0/LED_R_N_O]
  connect_bd_net -net ps_0_ENET0_GMII_TXD [get_bd_pins gig_ethernet_pcs_pma_0/gmii_txd] [get_bd_pins ps_0/ENET0_GMII_TXD]
  connect_bd_net -net ps_0_ENET0_GMII_TX_EN [get_bd_pins gig_ethernet_pcs_pma_0/gmii_tx_en] [get_bd_pins ps_0/ENET0_GMII_TX_EN]
  connect_bd_net -net ps_0_ENET0_GMII_TX_ER [get_bd_pins gig_ethernet_pcs_pma_0/gmii_tx_er] [get_bd_pins ps_0/ENET0_GMII_TX_ER]
  connect_bd_net -net ps_0_ENET0_MDIO_MDC [get_bd_pins gig_ethernet_pcs_pma_0/mdc] [get_bd_pins ps_0/ENET0_MDIO_MDC]
  connect_bd_net -net ps_0_ENET0_MDIO_O [get_bd_pins gig_ethernet_pcs_pma_0/mdio_i] [get_bd_pins ps_0/ENET0_MDIO_O]
  connect_bd_net -net ps_0_ENET1_GMII_TXD [get_bd_pins gig_ethernet_pcs_pma_1/gmii_txd] [get_bd_pins ps_0/ENET1_GMII_TXD]
  connect_bd_net -net ps_0_ENET1_GMII_TX_EN [get_bd_pins gig_ethernet_pcs_pma_1/gmii_tx_en] [get_bd_pins ps_0/ENET1_GMII_TX_EN]
  connect_bd_net -net ps_0_ENET1_GMII_TX_ER [get_bd_pins gig_ethernet_pcs_pma_1/gmii_tx_er] [get_bd_pins ps_0/ENET1_GMII_TX_ER]
  connect_bd_net -net ps_0_ENET1_MDIO_MDC [get_bd_pins gig_ethernet_pcs_pma_1/mdc] [get_bd_pins ps_0/ENET1_MDIO_MDC]
  connect_bd_net -net ps_0_ENET1_MDIO_O [get_bd_pins gig_ethernet_pcs_pma_1/mdio_i] [get_bd_pins ps_0/ENET1_MDIO_O]
  connect_bd_net -net ps_0_FCLK_CLK0 [get_bd_pins axi_interconnect_0/ACLK] [get_bd_pins axi_interconnect_0/S00_ACLK] [get_bd_pins ps_0/FCLK_CLK0] [get_bd_pins ps_0/M_AXI_GP0_ACLK] [get_bd_pins reset_generator_0/BUS_RST_CLK_I] [get_bd_pins reset_generator_0/BUS_RST_N_CLK_I] [get_bd_pins reset_generator_0/REF_CLK_I] [get_bd_pins sc_io_0/CLK_I]
  connect_bd_net -net ps_0_FCLK_CLK1 [get_bd_pins gig_ethernet_pcs_pma_0/independent_clock_bufg] [get_bd_pins gig_ethernet_pcs_pma_1/independent_clock_bufg] [get_bd_pins ps_0/FCLK_CLK1]
  connect_bd_net -net ps_0_FCLK_RESET0_N [get_bd_pins ps_0/FCLK_RESET0_N] [get_bd_pins reset_generator_0/RST_I]
  connect_bd_net -net ps_0_GPIO_O [get_bd_pins ps_0/GPIO_O] [get_bd_pins sc_io_0/GPIO_EMIO_I]
  connect_bd_net -net register_bank_0_BUS_CLK_SEL_O [get_bd_ports BUS_CLK_SEL_O] [get_bd_pins register_bank_0/BUS_CLK_SEL_O]
  connect_bd_net -net register_bank_0_CLK_SEL_EXT_O [get_bd_ports CLK_SEL_EXT_O] [get_bd_pins register_bank_0/CLK_SEL_EXT_O]
  connect_bd_net -net register_bank_0_MOSI_DIS_2_O [get_bd_ports MOSI_DIS_1_O] [get_bd_ports MOSI_DIS_2_O] [get_bd_pins register_bank_0/MOSI_DIS_2_O]
  connect_bd_net -net register_bank_0_RECONFIGURE_FPGA_O [get_bd_pins register_bank_0/RECONFIGURE_FPGA_O] [get_bd_pins sc_io_0/FPGA_RESET_I]
  connect_bd_net -net reset_generator_0_BUS_RST_N_O [get_bd_pins axi_interconnect_0/ARESETN] [get_bd_pins axi_interconnect_0/S00_ARESETN] [get_bd_pins reset_generator_0/BUS_RST_N_O]
  connect_bd_net -net sc_io_0_BPL_FS_INIT_N_O [get_bd_ports FLASH_SELECT_INIT_N_O] [get_bd_pins sc_io_0/BPL_FS_INIT_N_O]
  connect_bd_net -net sc_io_0_BPL_MASTER_SPI_DE_N_O [get_bd_ports MASTER_SPI_DE_N_O] [get_bd_pins sc_io_0/BPL_MASTER_SPI_DE_N_O]
  connect_bd_net -net sc_io_0_BPL_SPI_CS_N_O [get_bd_ports SPI_CS_N_O] [get_bd_pins sc_io_0/BPL_SPI_CS_N_O]
  connect_bd_net -net sc_io_0_ETH0_RESET_O [get_bd_pins gig_ethernet_pcs_pma_0/reset] [get_bd_pins sc_io_0/ETH0_RESET_O] [get_bd_pins xlconcat_0/In0]
  connect_bd_net -net sc_io_0_ETH1_RESET_O [get_bd_pins gig_ethernet_pcs_pma_1/reset] [get_bd_pins sc_io_0/ETH1_RESET_O] [get_bd_pins xlconcat_0/In1]
  connect_bd_net -net sc_io_0_FPGA_RESET_N_O [get_bd_ports RESET_FPGA_BUS_N_O] [get_bd_pins sc_io_0/FPGA_RESET_N_O]
  connect_bd_net -net sc_io_0_GPIO_EMIO_O [get_bd_pins ps_0/GPIO_I] [get_bd_pins sc_io_0/GPIO_EMIO_O]
  connect_bd_net -net sc_io_0_SFP_1_RS_O [get_bd_ports SFP_1_RS_O] [get_bd_pins sc_io_0/SFP_1_RS_O]
  connect_bd_net -net sc_io_0_SFP_1_TX_DISABLE_O [get_bd_ports SFP_1_TX_DISABLE_O] [get_bd_pins sc_io_0/SFP_1_TX_DISABLE_O]
  connect_bd_net -net sc_io_0_SFP_2_RS_O [get_bd_ports SFP_2_RS_O] [get_bd_pins sc_io_0/SFP_2_RS_O]
  connect_bd_net -net sc_io_0_SFP_2_TX_DISABLE_O [get_bd_ports SFP_2_TX_DISABLE_O] [get_bd_pins sc_io_0/SFP_2_TX_DISABLE_O]
  connect_bd_net -net sc_io_0_SW_STATE_O [get_bd_pins led_ctrl_0/SW_STATUS_I] [get_bd_pins sc_io_0/SW_STATE_O]
  connect_bd_net -net tr_sync_manager_0_TR_SYNC_BPL_O [get_bd_ports TR_SYNC_O] [get_bd_pins tr_sync_manager_0/TR_SYNC_BPL_O]
  connect_bd_net -net trigger_manager_0_PERR_COUNT_O [get_bd_pins register_bank_0/TRB_PARITY_ERROR_COUNT_I] [get_bd_pins trigger_manager_0/PERR_COUNT_O]
  connect_bd_net -net trigger_manager_0_PERR_O [get_bd_pins register_bank_0/TRB_FLAG_PARITY_ERROR_I] [get_bd_pins trigger_manager_0/PERR_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_BPL_O [get_bd_ports TRIGGER_O] [get_bd_pins trigger_manager_0/TRIGGER_BPL_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_MCX_O [get_bd_ports EXT_TRG_OUT_O] [get_bd_pins trigger_manager_0/TRIGGER_MCX_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_PDATA_O [get_bd_pins register_bank_0/Din1] [get_bd_pins trigger_manager_0/TRIGGER_PDATA_O]
  connect_bd_net -net trigger_manager_0_TRIGGER_SDATA_O [get_bd_ports TR_INFO_O] [get_bd_pins trigger_manager_0/TRIGGER_SDATA_O]
  connect_bd_net -net trigger_manager_0_VALID_O [get_bd_pins register_bank_0/TRB_FLAG_NEW_I] [get_bd_pins trigger_manager_0/VALID_O]
  connect_bd_net -net util_ds_buf_0_IBUF_OUT [get_bd_pins axi_interconnect_0/M00_ACLK] [get_bd_pins axi_interconnect_0/M01_ACLK] [get_bd_pins axi_interconnect_0/M02_ACLK] [get_bd_pins axi_quad_spi_to_bpl_0/ext_spi_clk] [get_bd_pins axi_quad_spi_to_bpl_0/s_axi_aclk] [get_bd_pins clock_measure_0/s00_axi_aclk] [get_bd_pins led_ctrl_0/LED_CLK_I] [get_bd_pins register_bank_0/s00_axi_aclk] [get_bd_pins reset_generator_0/PERIPHERAL_RST_CLK_I] [get_bd_pins reset_generator_0/PERIPHERAL_RST_N_CLK_I] [get_bd_pins util_ds_buf_fpga_clk/IBUF_OUT] [get_bd_pins xlconcat_1/In5]
  connect_bd_net -net util_ds_buf_ext_clk_IBUF_OUT [get_bd_pins clk_wiz_0/clk_in1] [get_bd_pins tr_sync_manager_0/WDB_CLK_I] [get_bd_pins trigger_manager_0/WDB_CLK_I] [get_bd_pins util_ds_buf_wdb_clk/IBUF_OUT]
  connect_bd_net -net xlconcat_0_dout [get_bd_pins led_ctrl_0/HW_ERROR_I] [get_bd_pins xlconcat_0/dout]
  connect_bd_net -net xlconcat_1_dout [get_bd_pins clock_measure_0/Clocks] [get_bd_pins xlconcat_1/dout]
  connect_bd_net -net xlslice_peripheral_rst_n0_Dout [get_bd_pins axi_interconnect_0/M00_ARESETN] [get_bd_pins axi_interconnect_0/M01_ARESETN] [get_bd_pins axi_interconnect_0/M02_ARESETN] [get_bd_pins axi_quad_spi_to_bpl_0/s_axi_aresetn] [get_bd_pins clock_measure_0/s00_axi_aresetn] [get_bd_pins register_bank_0/s00_axi_aresetn] [get_bd_pins reset_generator_0/PERIPHERAL_RST_N_O]
  connect_bd_net -net xlslice_spi_ss_tcb_0_Dout [get_bd_ports MASTER_SPI_TCB_SS_N_O] [get_bd_pins bus_split_spi_ss_0/MASTER_SPI_TCB_SS_N_O]
  connect_bd_net -net xlslice_spi_ss_wdb_0_Dout [get_bd_ports MASTER_SPI_WDB_SS_N_O] [get_bd_pins bus_split_spi_ss_0/MASTER_SPI_WDB_SS_N_O]

  # Create address segments
  create_bd_addr_seg -range 0x00010000 -offset 0x44000000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs register_bank_0/axi_dcb_register_bank_0/S00_AXI/S00_AXI_reg] SEG_axi_dcb_register_bank_0_S00_AXI_reg
  create_bd_addr_seg -range 0x00010000 -offset 0x41E00000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs axi_quad_spi_to_bpl_0/AXI_LITE/Reg] SEG_axi_quad_spi_to_bpl_0_Reg
  create_bd_addr_seg -range 0x00010000 -offset 0x43C00000 [get_bd_addr_spaces ps_0/Data] [get_bd_addr_segs clock_measure_0/s00_axi/reg0] SEG_clock_measure_0_reg0


  # Restore current instance
  current_bd_instance $oldCurInst

  save_bd_design
}
# End of create_root_design()


##################################################################
# MAIN FLOW
##################################################################

create_root_design ""


