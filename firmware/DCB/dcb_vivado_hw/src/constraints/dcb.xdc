create_clock -period 12.5 [get_ports CLK_FPGA_P_I]
create_clock -period 12.5 [get_ports WDB_CLK_P_I]
#create_clock -period 12.5 [get_ports EXT_CLK_P_I]
create_clock -period 8.0 [get_ports GTREFCLK0_P_I]

set_clock_groups -asynchronous -group [get_clocks clk_fpga_0] -group [get_clocks {dcb_i/util_ds_buf_wdb_clk/U0/IBUF_OUT[0]}]
set_clock_groups -asynchronous -group [get_clocks clk_fpga_0] -group [get_clocks clk_out_240MHz_dcb_clk_wiz_0_0]
set_clock_groups -asynchronous -group [get_clocks {dcb_i/util_ds_buf_wdb_clk/U0/IBUF_OUT[0]}] -group [get_clocks clk_out_240MHz_dcb_clk_wiz_0_0]

set_false_path -from [get_pins {*/crate_ctrl_if_0/U0/crate_ctrl_if_v1_0_S00_AXI_inst/slv_reg2_reg[*]/C}] -to [get_pins {*/crate_ctrl_if_0/U0/crate_ctrl_if_v1_0_S00_AXI_inst/BPL_SPI_M_ss_n_reg[*]/D}]

set_property PACKAGE_PIN L5       [get_ports CLK_FPGA_P_I]; # PCB Net: CLK_FPGA_P
set_property IOSTANDARD  LVDS     [get_ports CLK_FPGA_P_I];
set_property PACKAGE_PIN L4       [get_ports CLK_FPGA_N_I]; # PCB Net: CLK_FPGA_N
set_property IOSTANDARD  LVDS     [get_ports CLK_FPGA_N_I];

#set_property PACKAGE_PIN G7       [get_ports EXT_CLK_P_I]; # PCB Net: IN_EXT_CLK_P
#set_property IOSTANDARD  LVDS     [get_ports EXT_CLK_P_I];
#set_property PACKAGE_PIN F7       [get_ports EXT_CLK_N_I]; # PCB Net: IN_EXT_CLK_N
#set_property IOSTANDARD  LVDS     [get_ports EXT_CLK_N_I];

set_property PACKAGE_PIN AC13      [get_ports WDB_CLK_P_I]; # PCB Net: FPGA_CLK_P
set_property IOSTANDARD  LVDS_25   [get_ports WDB_CLK_P_I];
set_property PACKAGE_PIN AD13      [get_ports WDB_CLK_N_I]; # PCB Net: FPGA_CLK_N
set_property IOSTANDARD  LVDS_25   [get_ports WDB_CLK_N_I];

set_property PACKAGE_PIN AC19     [get_ports LED_R_N_O]; # PCB Net: LED_RED
set_property IOSTANDARD  LVCMOS33 [get_ports LED_R_N_O];
set_property PACKAGE_PIN AA20     [get_ports LED_B_N_O]; # PCB Net: LED_BLUE
set_property IOSTANDARD  LVCMOS33 [get_ports LED_B_N_O];
set_property PACKAGE_PIN AB20     [get_ports LED_G_N_O]; # PCB Net: LED_GREEN
set_property IOSTANDARD  LVCMOS33 [get_ports LED_G_N_O];


# FCI Connector
set_property PACKAGE_PIN AC17     [get_ports TRG_P_I]; # PCB Net: TRG_P hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports TRG_P_I];
set_property PULLDOWN true        [get_ports TRG_P_I];
set_property PACKAGE_PIN AC16     [get_ports TRG_N_I]; # PCB Net: TRG_N hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports TRG_N_I];
set_property PULLUP true          [get_ports TRG_N_I];
set_property PACKAGE_PIN Y16      [get_ports SYNC_P_I]; # PCB Net: SYNC_P hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports SYNC_P_I];
set_property PULLDOWN true        [get_ports SYNC_P_I];
set_property PACKAGE_PIN Y15      [get_ports SYNC_N_I]; # PCB Net: SYNC_N hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports SYNC_N_I];
set_property PULLUP true          [get_ports SYNC_N_I];
set_property PACKAGE_PIN AB17     [get_ports BUSY_N_P_O]; # PCB Net: BUSY_P   # AD20 in DCB_A
set_property IOSTANDARD  LVDS_25  [get_ports BUSY_N_P_O];
set_property PULLDOWN true        [get_ports BUSY_N_P_O];
set_property PACKAGE_PIN AB16     [get_ports BUSY_N_N_O]; # PCB Net: BUSY_N   # AD21 in DCB_A
set_property IOSTANDARD  LVDS_25  [get_ports BUSY_N_N_O];
set_property PULLUP true          [get_ports BUSY_N_N_O];
set_property PACKAGE_PIN W16      [get_ports SPR_P_I]; # PCB Net: SPR_P hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports SPR_P_I];
set_property PULLDOWN true        [get_ports SPR_P_I];
set_property PACKAGE_PIN W15      [get_ports SPR_N_I]; # PCB Net: SPR_N hardware diffterm
set_property IOSTANDARD  LVDS_25  [get_ports SPR_N_I];
set_property PULLUP true          [get_ports SPR_N_I];

#set_property PACKAGE_PIN Y17      [get_ports SPR1_P_I]; # PCB Net: SPR1_P hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR1_P_I];
#set_property PULLDOWN true        [get_ports SPR1_P_I];
#set_property PACKAGE_PIN AA17     [get_ports SPR1_N_I]; # PCB Net: SPR1_N hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR1_N_I];
#set_property PULLUP true          [get_ports SPR1_N_I];
#set_property PACKAGE_PIN AA15     [get_ports SPR2_P_I]; # PCB Net: SPR2_P hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR2_P_I];
#set_property PULLDOWN true        [get_ports SPR2_P_I];
#set_property PACKAGE_PIN AA14     [get_ports SPR2_N_I]; # PCB Net: SPR2_N hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR2_N_I];
#set_property PULLUP true          [get_ports SPR2_N_I];
#set_property PACKAGE_PIN AB15     [get_ports SPR3_P_I]; # PCB Net: SPR3_P hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR3_P_I];
#set_property PULLDOWN true        [get_ports SPR3_P_I];
#set_property PACKAGE_PIN AB14     [get_ports SPR3_N_I]; # PCB Net: SPR3_N hardware diffterm
#set_property IOSTANDARD  LVDS_25  [get_ports SPR3_N_I];
#set_property PULLUP true          [get_ports SPR3_N_I];

# Trigger interface
# Bank 12 (2.5V)
set_property PACKAGE_PIN Y12      [get_ports TR_SYNC_O]; # PCB Net: TR_SYNC
set_property IOSTANDARD LVCMOS25  [get_ports TR_SYNC_O];
set_property PACKAGE_PIN AD25     [get_ports SYNC_DIR_O]; # PCB Net: SYNC_DIR
set_property IOSTANDARD LVCMOS33  [get_ports SYNC_DIR_O];
set_property PACKAGE_PIN AB12     [get_ports TRIGGER_O]; # PCB Net: TRIGGER
set_property IOSTANDARD LVCMOS25  [get_ports TRIGGER_O];
set_property PACKAGE_PIN AC24     [get_ports TRG_DIR_O]; # PCB Net: TRG_DIR
set_property IOSTANDARD LVCMOS33  [get_ports TRG_DIR_O];
set_property PACKAGE_PIN AA12     [get_ports TR_INFO_O]; # PCB Net: TR_INFO
set_property IOSTANDARD LVCMOS25  [get_ports TR_INFO_O];
set_property PACKAGE_PIN AD26     [get_ports INFO_DIR_O]; # PCB Net: INFO_DIR
set_property IOSTANDARD LVCMOS33  [get_ports INFO_DIR_O];


# Configuration control
# Bank 12 (2.5V)
set_property PACKAGE_PIN AB10     [get_ports RESET_FPGA_BUS_N_O]; # PCB Net: RESET_FPGA_BUS
set_property IOSTANDARD  LVCMOS25 [get_ports RESET_FPGA_BUS_N_O];
set_property PULLUP true          [get_ports RESET_FPGA_BUS_N_O];
set_property IOB true             [get_ports RESET_FPGA_BUS_N_O];
set_property PACKAGE_PIN AD15     [get_ports FLASH_SELECT_INIT_N_O]; # PCB Net: INIT_BUS
set_property IOSTANDARD LVCMOS25  [get_ports FLASH_SELECT_INIT_N_O];
set_property PULLUP true          [get_ports FLASH_SELECT_INIT_N_O];
set_property IOB true             [get_ports FLASH_SELECT_INIT_N_O];
set_property PACKAGE_PIN AA10     [get_ports SPI_CS_N_O]; # PCB Net: FS_BUS
set_property IOSTANDARD LVCMOS25  [get_ports SPI_CS_N_O];
set_property PULLUP true          [get_ports SPI_CS_N_O];
set_property IOB true             [get_ports SPI_CS_N_O];
# Bank 13 (3.3V)
set_property PACKAGE_PIN AB25     [get_ports FS_INIT_N_I]; # PCB Net: INIT_FPGA
set_property IOSTANDARD LVCMOS33  [get_ports FS_INIT_N_I];
set_property PACKAGE_PIN AB26     [get_ports BOARD_SELECT_N_I]; # PCB Net: BS_FPGA
set_property IOSTANDARD LVCMOS33  [get_ports BOARD_SELECT_N_I];


#set_property PACKAGE_PIN AF25     [get_ports SYNC_LMK_N_O]; # PCB Net: SYNC_LMK
#set_property IOSTANDARD LVCMOS33  [get_ports SYNC_LMK_N_O];
set_property PACKAGE_PIN AF24     [get_ports LD_I]; # PCB Net: LD
set_property IOSTANDARD LVCMOS33  [get_ports LD_I];
set_property PACKAGE_PIN AF18     [get_ports BUSY_IN_N_I]; # PCB Net: BUSY_IN
set_property IOSTANDARD LVCMOS33  [get_ports BUSY_IN_N_I];
set_property PACKAGE_PIN AE18     [get_ports BUSY_OUT_N_O]; # PCB Net: BUSY_OUT
set_property IOSTANDARD LVCMOS33  [get_ports BUSY_OUT_N_O];
set_property PULLUP true          [get_ports BUSY_OUT_N_O];
#set_property PACKAGE_PIN AD18     [get_ports ATTENTION_IN_N_I]; # PCB Net: ATTENTION_IN
#set_property IOSTANDARD LVCMOS33  [get_ports ATTENTION_IN_N_I];
#set_property PACKAGE_PIN AC18     [get_ports ATTENTION_OUT_N_O]; # PCB Net: ATTENTION_OUT
#set_property IOSTANDARD LVCMOS33  [get_ports ATTENTION_OUT_N_O];
set_property PACKAGE_PIN AD24     [get_ports CLK_SEL_O]; # PCB Net: CLK_SEL
set_property IOSTANDARD LVCMOS33  [get_ports CLK_SEL_O];
set_property PACKAGE_PIN AA23     [get_ports CLK_SEL_EXT_O]; # PCB Net: CLK_SEL_EXT
set_property IOSTANDARD LVCMOS33  [get_ports CLK_SEL_EXT_O];


# SFP Control
# SFP 2
set_property PACKAGE_PIN AD20     [get_ports SFP_2_RS_O[1]]; # PCB Net: SFP_2_RS1
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_RS_O[1]];
set_property PACKAGE_PIN AD21     [get_ports SFP_2_RS_O[0]]; # PCB Net: SFP_2_RS0
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_RS_O[0]];
set_property PACKAGE_PIN W18      [get_ports SFP_2_TX_DISABLE_O]; # PCB Net: SFP_2_TX_DISABLE
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_TX_DISABLE_O];
set_property PACKAGE_PIN V18      [get_ports SFP_2_TX_FAULT_I]; # PCB Net: SFP_2_TX_FAULT
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_TX_FAULT_I];
set_property PACKAGE_PIN Y18      [get_ports SFP_2_MOD_I]; # PCB Net: SFP_2_MOD
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_MOD_I];
set_property PACKAGE_PIN AA18     [get_ports SFP_2_LOS_I]; # PCB Net: SFP_2_LOS
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_2_LOS_I];
# SFP 1
set_property PACKAGE_PIN AC21     [get_ports SFP_1_RS_O[1]]; # PCB Net: SFP_1_RS1
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_RS_O[1]];
set_property PACKAGE_PIN AC22     [get_ports SFP_1_RS_O[0]]; # PCB Net: SFP_1_RS0
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_RS_O[0]];
set_property PACKAGE_PIN W19      [get_ports SFP_1_TX_DISABLE_O]; # PCB Net: SFP_1_TX_DISABLE
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_TX_DISABLE_O];
set_property PACKAGE_PIN V19      [get_ports SFP_1_TX_FAULT_I]; # PCB Net: SFP_1_TX_FAULT
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_TX_FAULT_I];
set_property PACKAGE_PIN W20      [get_ports SFP_1_MOD_I]; # PCB Net: SFP_1_MOD
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_MOD_I];
set_property PACKAGE_PIN Y20      [get_ports SFP_1_LOS_I]; # PCB Net: SFP_1_LOS
set_property IOSTANDARD  LVCMOS33 [get_ports SFP_1_LOS_I];

# Ethernet
# Refclocks
set_property PACKAGE_PIN R6       [get_ports GTREFCLK0_P_I]; # PCB Net: -
set_property PACKAGE_PIN R5       [get_ports GTREFCLK0_N_I]; # PCB Net: -
#set_property PACKAGE_PIN U6       [get_ports GTREFCLK1_P_I]; # PCB Net: -
#set_property PACKAGE_PIN U5       [get_ports GTREFCLK1_N_I]; # PCB Net: -
# ETH0
set_property PACKAGE_PIN AB4      [get_ports ETH0_SFP_rxp];  # PCB Net: SFP_1_RX_P
set_property PACKAGE_PIN AB3      [get_ports ETH0_SFP_rxn];  # PCB Net: SFP_1_RX_N
set_property PACKAGE_PIN AA2      [get_ports ETH0_SFP_txp];  # PCB Net: SFP_1_TX_P
set_property PACKAGE_PIN AA1      [get_ports ETH0_SFP_txn];  # PCB Net: SFP_1_TX_N
# ETH1
set_property PACKAGE_PIN Y4       [get_ports ETH1_SFP_rxp];  # PCB Net: SFP_2_RX_P
set_property PACKAGE_PIN Y3       [get_ports ETH1_SFP_rxn];  # PCB Net: SFP_2_RX_N
set_property PACKAGE_PIN W2       [get_ports ETH1_SFP_txp];  # PCB Net: SFP_2_TX_P
set_property PACKAGE_PIN W1       [get_ports ETH1_SFP_txn];  # PCB Net: SFP_2_TX_N


# Clock Distributor Interface (Bank 34, 1.8V)
set_property PACKAGE_PIN J10      [get_ports CLK_DIS_1_O]; # PCB Net: CLK_DIS_1
set_property IOSTANDARD  LVCMOS18 [get_ports CLK_DIS_1_O];
set_property PACKAGE_PIN H7       [get_ports CLK_DIS_2_O]; # PCB Net: CLK_DIS_2
set_property IOSTANDARD  LVCMOS18 [get_ports CLK_DIS_2_O];
set_property PACKAGE_PIN H6       [get_ports CE_DIS_1_O]; # PCB Net: CE_DIS_1
set_property IOSTANDARD  LVCMOS18 [get_ports CE_DIS_1_O];
set_property PACKAGE_PIN J9       [get_ports CE_DIS_2_O]; # PCB Net: CE_DIS_2
set_property IOSTANDARD  LVCMOS18 [get_ports CE_DIS_2_O];
set_property PACKAGE_PIN J8       [get_ports MOSI_DIS_1_O]; # PCB Net: MOSI_DIS_1
set_property IOSTANDARD  LVCMOS18 [get_ports MOSI_DIS_1_O];
set_property PACKAGE_PIN H8       [get_ports MOSI_DIS_2_O]; # PCB Net: MOSI_DIS_2
set_property IOSTANDARD  LVCMOS18 [get_ports MOSI_DIS_2_O];


# MCX Connectors
set_property PACKAGE_PIN AF20     [get_ports EXT_TRG_IN_I];      # PCB Net: EXT_TRG_IN
set_property IOSTANDARD  LVCMOS33 [get_ports EXT_TRG_IN_I];
set_property PULLDOWN true        [get_ports EXT_TRG_IN_I];
set_property PACKAGE_PIN AE20     [get_ports DIR_EXT_TRG_IN_O];  # PCB Net: DIR_EXT_TRG_IN
set_property IOSTANDARD  LVCMOS33 [get_ports DIR_EXT_TRG_IN_O];
set_property PACKAGE_PIN AF19     [get_ports EXT_TRG_OUT_O];     # PCB Net: EXT_TRG_OUT
set_property IOSTANDARD  LVCMOS33 [get_ports EXT_TRG_OUT_O];
set_property PACKAGE_PIN AD19     [get_ports DIR_EXT_TRG_OUT_O]; # PCB Net: DIR_EXT_TRG_OUT
set_property IOSTANDARD  LVCMOS33 [get_ports DIR_EXT_TRG_OUT_O];

#MSCB TO BACKPLANE
set_property PACKAGE_PIN AF22    [get_ports MSCB_DATA_I]; # MSCB_RO
set_property IOSTANDARD LVCMOS33 [get_ports MSCB_DATA_I];
set_property PACKAGE_PIN AB21    [get_ports MSCB_DATA_O]; # MSCB_DI
set_property IOSTANDARD LVCMOS33 [get_ports MSCB_DATA_O];
set_property PACKAGE_PIN AE21    [get_ports MSCB_DRV_EN_O]; # MSCB_DE
set_property IOSTANDARD LVCMOS33 [get_ports MSCB_DRV_EN_O];

#SPI TO BACKPLANE (IF MASTER)
set_property PACKAGE_PIN AE22    [get_ports MASTER_SPI_MOSI_O]; # MASTER_SPI_MOSI
set_property IOSTANDARD LVCMOS33 [get_ports MASTER_SPI_MOSI_O];
set_property PACKAGE_PIN AB22    [get_ports MASTER_SPI_MISO_I]; # MASTER_SPI_MISO
set_property IOSTANDARD LVCMOS33 [get_ports MASTER_SPI_MISO_I];
set_property PACKAGE_PIN AF23    [get_ports MASTER_SPI_CLK_O]; # MASTER_SPI_CLK
set_property IOSTANDARD LVCMOS33 [get_ports MASTER_SPI_CLK_O];
set_property PACKAGE_PIN AE23     [get_ports MASTER_SPI_DE_N_O]; # MASTER_SPI_DE
set_property IOSTANDARD LVCMOS33  [get_ports MASTER_SPI_DE_N_O];
set_property PULLDOWN true        [get_ports MASTER_SPI_DE_N_O];

#SPI FROM BACKPLANE (IF SALVE)
set_property PACKAGE_PIN AD23    [get_ports SLAVE_SPI_MOSI_I]; # MOSI_FPGA
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_MOSI_I];
set_property PACKAGE_PIN AE25    [get_ports SPI_CS_FPGA_N_I]; # SPI_CS_FPGA
set_property IOSTANDARD LVCMOS33 [get_ports SPI_CS_FPGA_N_I];
set_property PACKAGE_PIN AE26    [get_ports SLAVE_SPI_CLK_I];
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_CLK_I];
set_property PACKAGE_PIN AB24    [get_ports SLAVE_SPI_MISO_O]; # SPI_MISO
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_MISO_O];
set_property PACKAGE_PIN AC26    [get_ports SLAVE_SPI_MISO_EN_N_O]; # MISO_EN
set_property IOSTANDARD LVCMOS33 [get_ports SLAVE_SPI_MISO_EN_N_O];
set_property PULLDOWN true       [get_ports SLAVE_SPI_MISO_EN_N_O];

#Board selects WDB and TCB
set_property PACKAGE_PIN AF15    [get_ports MASTER_SPI_WDB_SS_N_O[0]];  # BS_0
set_property PACKAGE_PIN AE17    [get_ports MASTER_SPI_WDB_SS_N_O[1]];  # BS_1
set_property PACKAGE_PIN AE15    [get_ports MASTER_SPI_WDB_SS_N_O[2]];  # BS_2
set_property PACKAGE_PIN AF17    [get_ports MASTER_SPI_WDB_SS_N_O[3]];  # BS_3
set_property PACKAGE_PIN AF14    [get_ports MASTER_SPI_WDB_SS_N_O[4]];  # BS_4
set_property PACKAGE_PIN AF13    [get_ports MASTER_SPI_WDB_SS_N_O[5]];  # BS_5
set_property PACKAGE_PIN AE10    [get_ports MASTER_SPI_WDB_SS_N_O[6]];  # BS_6
set_property PACKAGE_PIN AF10    [get_ports MASTER_SPI_WDB_SS_N_O[7]];  # BS_7
set_property PACKAGE_PIN AE11    [get_ports MASTER_SPI_WDB_SS_N_O[8]];  # BS_8
set_property PACKAGE_PIN AE13    [get_ports MASTER_SPI_WDB_SS_N_O[9]];  # BS_9
set_property PACKAGE_PIN AE12    [get_ports MASTER_SPI_WDB_SS_N_O[10]]; # BS_10
set_property PACKAGE_PIN AF12    [get_ports MASTER_SPI_WDB_SS_N_O[11]]; # BS_11
set_property PACKAGE_PIN AD10    [get_ports MASTER_SPI_WDB_SS_N_O[12]]; # BS_12
set_property PACKAGE_PIN AC12    [get_ports MASTER_SPI_WDB_SS_N_O[13]]; # BS_13
set_property PACKAGE_PIN AC11    [get_ports MASTER_SPI_WDB_SS_N_O[14]]; # BS_14
set_property PACKAGE_PIN AD11    [get_ports MASTER_SPI_WDB_SS_N_O[15]]; # BS_15
set_property PACKAGE_PIN AE16    [get_ports MASTER_SPI_TCB_SS_N_O];     # BS_TCB
set_property IOSTANDARD LVCMOS25 [get_ports MASTER_SPI_WDB_SS_N_O[*]];
set_property IOB true            [get_ports MASTER_SPI_WDB_SS_N_O[*]];
set_property PULLUP true         [get_ports MASTER_SPI_WDB_SS_N_O[*]];
set_property IOSTANDARD LVCMOS25 [get_ports MASTER_SPI_TCB_SS_N_O];
set_property IOB true            [get_ports MASTER_SPI_TCB_SS_N_O];
set_property PULLUP true         [get_ports MASTER_SPI_TCB_SS_N_O];



# CPLD interface
#set_property PACKAGE_PIN AC23    [get_ports CPLD_FPGA_D_I]; # CPLD_DIO
#set_property IOSTANDARD LVCMOS25 [get_ports CPLD_FPGA_D_I];
#set_property PACKAGE_PIN AB19    [get_ports FPGA_CPLD_CE_O]; # CPLD_CE
#set_property IOSTANDARD LVCMOS25 [get_ports FPGA_CPLD_CE_O];
#set_property PACKAGE_PIN AA19    [get_ports FPGA_CPLD_CLK_O]; # CPLD_CLK
#set_property IOSTANDARD LVCMOS25 [get_ports FPGA_CPLD_CLK_O];



# Miscellaneous
#BUS clock - Bank12 (2.5V)
set_property PACKAGE_PIN AA13    [get_ports BUS_CLK_SEL_O]; # BUS_CLK_SEL
set_property IOSTANDARD LVCMOS25 [get_ports BUS_CLK_SEL_O];
set_property PACKAGE_PIN W13     [get_ports AUX_OUT_P_O]; # AUX_OUT_P
set_property IOSTANDARD LVCMOS25 [get_ports AUX_OUT_P_O];
set_property PACKAGE_PIN Y13     [get_ports AUX_OUT_N_O]; # AUX_OUT_N
set_property IOSTANDARD LVCMOS25 [get_ports AUX_OUT_N_O];
#Bank 13 (3.3V)
#set_property PACKAGE_PIN AA22    [get_ports SIG_TP68_O]; # SIG_TP68
#set_property IOSTANDARD LVCMOS33 [get_ports SIG_TP68_O];
