#! /usr/bin/env python
if __name__ == "__main__":
  import sys, os, csv
  #import re
  from datetime import datetime
  import openpyxl
  #import openpyxl.cell
  from openpyxl.utils import get_column_letter, column_index_from_string
#  from openpyxl.cell import get_column_letter, column_index_from_string
  from shutil import copyfile

  ### Usage:
  ###
  ### csv2regconst.py <control xlsx path>
  ###
  ### <control csv path> is the path to the control register .csv file to be processed.
  ###
  ### <status csv path> is the path to the status register .csv file to be processed.
  ###
  ### <csv path> is the path to save the .vhd file.

  ###############################################################
  ###
  ### This script generates a VHDL file
  ### containing constans with register indices
  ### for a register bank based on a csv from
  ### Excel.
  ###
  ###############################################################


###################################################################################################
# File and Path Checks/Settings
###################################################################################################

  ### Checking input file parameters
  if os.path.isabs(sys.argv[1]):
    path = sys.argv[1]
  else:
    path = os.path.abspath('.')+os.sep+sys.argv[1]

  indir = os.path.dirname(path)
  infile = os.path.basename(path)

  outdir = indir + os.sep + "script_output"
  if not os.path.isdir(outdir):
    os.makedirs(outdir)

  vhd_pkg_outfile = "ipif_user_cfg.vhd"
  vhd_outfile     = "axi_dcb_register_bank_v1_0.vhd"
  c_outfile       = "register_map_dcb.c"
  h_outfile       = "register_map_dcb.h"
  py_outfile      = "dcb_regs.py"
  class_outfile   = "DCBReg.h"
  css_outfile     = "dcb_register_map.css"
  html_outfile    = "dcb_register_map.html"

  # Paths relative to GIT root directory
  ip_dir      = "firmware/DCB/dcb_vivado_hw/ip_repo/axi_dcb_register_bank_1.0"
  ip_dir      = ip_dir.replace('/', os.sep)
  ip_vhdl_dir = ip_dir + os.sep + "hdl"
  ip_doc_dir  = ip_dir + os.sep + "doc"

  c_h_dir_arm = "firmware/DCB/dcb_xsdk_workspace/dcb_app_sw/app_dcb_ctrl/src"
  c_h_dir_arm = c_h_dir_arm.replace('/', os.sep)
  c_h_dir_lin = "software/dcb/app/src"
  c_h_dir_lin = c_h_dir_lin.replace('/', os.sep)
  c_h_dir_wds = "software/include"
  c_h_dir_wds = c_h_dir_wds.replace('/', os.sep)

  py_dir = "software/scripts"
  py_dir = py_dir.replace('/', os.sep)

  class_dir = "software/include"
  class_dir = class_dir.replace('/', os.sep)

###################################################################################################
# Generic constants
###################################################################################################

  bits_per_reg  = 32

  extra_name_len = 12

###################################################################################################
# Helper Functions
###################################################################################################

  def underscore_to_camelcase(name):
    return ''.join(w.capitalize() for w in name.split('_'))

  max_reg_name_len  = 0
  max_port_name_len = 0
  max_func_name_len = 0

  def reg_designator(reg):
    return "DCB_REG_%s"%reg[i_reg_name]
  def calc_reg_des_max_len():
    return max_reg_name_len + 8

  def bit_reg_designator(port):
    return "DCB_%s_REG"%port[i_port_name]
  def calc_bit_reg_des_max_len():
    return max_port_name_len + 8

  def bit_msk_designator(port):
    return "DCB_%s_MASK"%port[i_port_name]
  def calc_bit_msk_des_max_len():
    return max_port_name_len + 9

  def bit_ofs_designator(port):
    return "DCB_%s_OFS"%port[i_port_name]
  def calc_bit_ofs_des_max_len():
    return max_port_name_len + 8

  def bit_const_designator(port):
    return "DCB_%s_CONST"%port[i_port_name]
  def calc_bit_const_des_max_len():
    return max_port_name_len + 10

  def find_vcs_root(test, dirs=(".git",), default=None):
      import os
      prev, test = None, os.path.abspath(test)
      while prev != test:
          if any(os.path.isdir(os.path.join(test, d)) for d in dirs):
              return test
          prev, test = test, os.path.abspath(os.path.join(test, os.pardir))
      return default

  def copy_file_to_target(src, dst):
    src_dir  = os.path.dirname(src)
    src_file = os.path.basename(src)
    dst_dir  = os.path.dirname(dst)
    dst_file = os.path.basename(dst)
    if not os.path.exists(dst_dir):
      create_folder = raw_input("Destination path does not exist. Shall I creat it? ([y]/n): ")
      if (not create_folder) or (create_folder == 'y') or (create_folder == "yes"):
        os.makedirs(dst_dir)
      else:
        return
    copyfile(src, dst)
    print "From %s\nto   %s"%(src, dst)

###################################################################################################
# Headers, Comments and Constant Code
###################################################################################################

  vhd_pkg_header_start = """---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  ipif_user_cfg.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)\n"""

  vhd_pkg_header_date = "--  Created :  " + datetime.now().strftime('%d.%m.%Y %H:%M:%S')

  vhd_pkg_header_end = """\n--
--  Description :  Mapping of the register content of WaveDream2.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------\n"""

  vhd_pkg_header = vhd_pkg_header_start + vhd_pkg_header_date + vhd_pkg_header_end

  vhd_pkg_content_start = """library ieee;
use ieee.std_logic_1164.all;
use ieee.std_logic_unsigned.all;

--------------------------------------------------------------------------------
-- Package header
--------------------------------------------------------------------------------

package ipif_user_cfg is

  ---------------------------------------------------------------------------
  -- General Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_WIDTH         : integer := 32;
  constant  C_SLV_AWIDTH        : integer := 32;
  constant  C_SLV_DWIDTH        : integer := 32;

  ---------------------------------------------------------------------------
  -- Register Definitions
  ---------------------------------------------------------------------------

  constant  C_REG_UNDEFINED     : std_logic_vector(C_REG_WIDTH-1 downto 0) := X"DEADBEEF";
  constant  C_DEFAULT_ZERO      : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_BIT_ALL        : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '1');
  constant  C_WR_BIT_NONE       : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');
  constant  C_WR_PULSE_NONE     : std_logic_vector(C_REG_WIDTH-1 downto 0) := (others => '0');

  constant  C_WR_EXT_NO         : integer := 0;
  constant  C_WR_EXT_YES        : integer := 1;

  constant  C_REG_SELF          : integer := -1;
  constant  C_REGISTER_REG_READ : boolean := false;


  ---------------------------------------------------------------------------
  -- register description
  ---------------------------------------------------------------------------

  -- Define special register types, default is C_REG_RW, possible types:
  -- C_REG_RW  : Read / Write register
  -- C_REG_R_W : Seperate Read (from Input) and Write (to output)
  -- C_REG_RO  : Read Only register
  -- C_REG_WR  : WRite register,               reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_SET : SET bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- C_BIT_CLR : CLR bits in another register, reg_rd(<num>) may be set by user or defaults to C_REG_UNDEFINED
  -- set as record (<reg_type>, <reg_num>, <default>, <writable mask>),
  --   e.g. C_REG_CTRL_SET => (C_BIT_SET, C_REG_CTRL)
  --   <reg_num> only used for C_BIT_SET and C_BIT_CLR, otherwise set to C_REG_SELF

  type  reg_type is (C_REG_RW, C_REG_R_W, C_REG_RO, C_REG_WR, C_BIT_SET, C_BIT_CLR);

  type  reg_descr_record is
        record
          typ     : reg_type;
          reg     : integer;
          def     : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_msk  : std_logic_vector(C_REG_WIDTH-1 downto 0);
          pls_msk : std_logic_vector(C_REG_WIDTH-1 downto 0);
          wr_ext  : integer;
        end record;

  type  reg_descr_type is array (natural range <>) of reg_descr_record;

  constant  C_REG_DESCR_DEFAULT : reg_descr_type(0 to 1) :=
  (        --  type         set/clr num       default           writable mask    write pulse only   external overwrite
    others => (C_REG_RW,    C_REG_SELF,       C_DEFAULT_ZERO,   C_WR_BIT_ALL,    C_WR_PULSE_NONE,   C_WR_EXT_NO)
  );

  ---------------------------------------------------------------------------
  -- User register definition
  ---------------------------------------------------------------------------

"""

  vhd_pkg_content_end = """
  ---------------------------------------------------------------------------
  -- register user interface signals
  ---------------------------------------------------------------------------

  type  reg_array is array (natural range <>) of std_logic_vector(C_REG_WIDTH-1 downto 0);

  type  user_to_reg_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_ext           :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  type  reg_to_user_type is
        record
          data             :  reg_array(0 to C_NUM_REG-1);
          wr_strobe        :  std_logic_vector(0 to C_NUM_REG-1);
          rd_strobe        :  std_logic_vector(0 to C_NUM_REG-1);
        end record;

  -------------------------------------------------------------------------------------------
  -- User memory definition
  -------------------------------------------------------------------------------------------

  constant  C_MEM_DEPTH                  : integer              := 9;
  constant  C_NUM_MEM                    : integer              := 0;


  type  user_to_mem_type is
        record
          enable  : std_logic;
          wr_en   : std_logic;
          addr    : std_logic_vector(8 downto 0);
          data    : std_logic_vector(31 downto 0);
        end record;


  type  mem_to_user_type is
        record
          data    : std_logic_vector(31 downto 0);
        end record;

  ---------------------------------------------------------------------------
  -- User interface signals to top level
  ---------------------------------------------------------------------------

  constant  C_NUM_CS : integer  := (1 + C_NUM_MEM);

  type  user_to_ipif_type is
        record
          reg    : user_to_reg_type;
          mem    : user_to_mem_type;
        end record;


  type  ipif_to_user_type is
        record
          rst    : std_logic;
          reg    : reg_to_user_type;
          mem    : mem_to_user_type;
        end record;


end;


--------------------------------------------------------------------------------
-- Package body
--------------------------------------------------------------------------------

package body ipif_user_cfg is

end package body;
"""

  vhd_header_start = """---------------------------------------------------------------------------------------
--  Paul Scherrer Institut
---------------------------------------------------------------------------------------
--
--  Unit :  axi_dcb_register_bank_v1_0.vhd
--
--  Project :  WDAQ - DCB
--
--  PCB  :  -
--  Part :  Xilinx ZYNQ7000 XC2Z030-FGB676C
--
--  Tool Version :  Vivaldo 2017.4 (Version the code was testet with)
--
--  Author  :  TG32, SE32(Author of generation script)\n"""

  vhd_header_date = "--  Created :  " + datetime.now().strftime('%d.%m.%Y %H:%M:%S')

  vhd_header_end = """\n--
--  Description :  Toplevel of the MEGII DCB register bank.
--
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------\n"""

  vhd_header = vhd_header_start + vhd_header_date + vhd_header_end

  vhd_entity_start = """library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

library axi_dcb_reg_bank_v1_0;
use axi_dcb_reg_bank_v1_0.ipif_user_cfg.all;
use axi_dcb_reg_bank_v1_0.ipif_axi;

entity axi_dcb_register_bank_v1_0 is
  generic (
    -- Parameters of Axi Slave Bus Interface S00_AXI
    C_S00_AXI_DATA_WIDTH  : integer           := 32;
    C_S00_AXI_ADDR_WIDTH  : integer           := 32;
    C_S00_AXI_BASEADDR    : std_logic_vector := X"FFFFFFFF";
    C_S00_AXI_HIGHADDR    : std_logic_vector := X"00000000";
    C_S00_AXI_MIN_SIZE    : std_logic_vector := X"FFFFFFFF";
    -- Users parameters
    C_USE_WSTRB           : integer          := 0;
    C_DPHASE_TIMEOUT      : integer          := 8;
    C_FAMILY              : string           := "virtex6"
    --C_MEM0_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM0_HIGHADDR       : std_logic_vector := X"00000000";
    --C_MEM1_BASEADDR       : std_logic_vector := X"FFFFFFFF";
    --C_MEM1_HIGHADDR       : std_logic_vector := X"00000000"
  );
  port (
    -- -------------------------------------------
    -- User Ports                               --
    -- -------------------------------------------"""

  vhd_entity_end = """\n    -- -------------------------------------------
    -- Ports of Axi Slave Bus Interface S00_AXI --
    -- -------------------------------------------
    s00_axi_aclk     : in std_logic;
    s00_axi_aresetn  : in std_logic;
    s00_axi_awaddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_awprot   : in std_logic_vector(2 downto 0);
    s00_axi_awvalid  : in std_logic;
    s00_axi_awready  : out std_logic;
    s00_axi_wdata    : in std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_wstrb    : in std_logic_vector((C_S00_AXI_DATA_WIDTH/8)-1 downto 0);
    s00_axi_wvalid   : in std_logic;
    s00_axi_wready   : out std_logic;
    s00_axi_bresp    : out std_logic_vector(1 downto 0);
    s00_axi_bvalid   : out std_logic;
    s00_axi_bready   : in std_logic;
    s00_axi_araddr   : in std_logic_vector(C_S00_AXI_ADDR_WIDTH-1 downto 0);
    s00_axi_arprot   : in std_logic_vector(2 downto 0);
    s00_axi_arvalid  : in std_logic;
    s00_axi_arready  : out std_logic;
    s00_axi_rdata    : out std_logic_vector(C_S00_AXI_DATA_WIDTH-1 downto 0);
    s00_axi_rresp    : out std_logic_vector(1 downto 0);
    s00_axi_rvalid   : out std_logic;
    s00_axi_rready   : in std_logic
  );
end axi_dcb_register_bank_v1_0;\n\n"""

  vhd_architecture_start = """architecture arch_imp of axi_dcb_register_bank_v1_0 is

  ---------------------------------------------------------------------------
  -- interface signal definitions
  ---------------------------------------------------------------------------
  signal user_to_ipif     : user_to_ipif_type;
  signal ipif_to_user     : ipif_to_user_type;

begin

  ------------------------------------------------------
  -- register definition is done in ipif_user_cfg.vhdl
  ------------------------------------------------------

  -- Register Bit <-> I/O Mapping\n"""

  vhd_architecture_end = """\n--  ------------------------------------------------------
--  -- memory definition is done in ipif_user_cfg.vhdl
--  ------------------------------------------------------
--
--  user_to_ipif.mem.addr   <= core_mem_address;
--  user_to_ipif.mem.enable <= '1';
--  user_to_ipif.mem.wr_en  <= '0';
--  user_to_ipif.mem.data   <= (others => '0');

  ------------------------------------------------------------------------------
  -- instantiate AXI IP Interface
  ------------------------------------------------------------------------------

  ipif_axi_inst : entity axi_dcb_reg_bank_v1_0.ipif_axi
  generic map
  (
    -- Bus protocol parameters, do not add to or delete
    C_S_AXI_DATA_WIDTH  => C_S00_AXI_DATA_WIDTH,
    C_S_AXI_ADDR_WIDTH  => C_S00_AXI_ADDR_WIDTH,
    C_S_AXI_MIN_SIZE    => C_S00_AXI_MIN_SIZE,
    C_USE_WSTRB         => C_USE_WSTRB,
    C_BASEADDR          => C_S00_AXI_BASEADDR,
    C_HIGHADDR          => C_S00_AXI_HIGHADDR,
    C_SLV_AWIDTH        => C_S00_AXI_ADDR_WIDTH,
    C_SLV_DWIDTH        => C_S00_AXI_DATA_WIDTH,
    C_DPHASE_TIMEOUT    => C_DPHASE_TIMEOUT,
    C_FAMILY            => C_FAMILY
    --C_MEM0_BASEADDR     => C_MEM0_BASEADDR,
    --C_MEM0_HIGHADDR     => C_MEM0_HIGHADDR,
    --C_MEM1_BASEADDR     => C_MEM1_BASEADDR,
    --C_MEM1_HIGHADDR     => C_MEM1_HIGHADDR
  )
  port map
  (
    -- user ports for ip interface
    USER_TO_IPIF_I        => user_to_ipif,
    IPIF_TO_USER_O        => ipif_to_user,
    -- Bus protocol ports, do not add to or delete
    S_AXI_ACLK            => s00_axi_aclk,
    S_AXI_ARESETN         => s00_axi_aresetn,
    S_AXI_AWADDR          => s00_axi_awaddr,
    S_AXI_AWVALID         => s00_axi_awvalid,
    S_AXI_WDATA           => s00_axi_wdata,
    S_AXI_WSTRB           => s00_axi_wstrb,
    S_AXI_WVALID          => s00_axi_wvalid,
    S_AXI_BREADY          => s00_axi_bready,
    S_AXI_ARADDR          => s00_axi_araddr,
    S_AXI_ARVALID         => s00_axi_arvalid,
    S_AXI_RREADY          => s00_axi_rready,
    S_AXI_ARREADY         => s00_axi_arready,
    S_AXI_RDATA           => s00_axi_rdata,
    S_AXI_RRESP           => s00_axi_rresp,
    S_AXI_RVALID          => s00_axi_rvalid,
    S_AXI_WREADY          => s00_axi_wready,
    S_AXI_BRESP           => s00_axi_bresp,
    S_AXI_BVALID          => s00_axi_bvalid,
    S_AXI_AWREADY         => s00_axi_awready
  );

  -- Check if necessary:
  -- S_AXI_AWPROT  => s00_axi_awprot,
  -- S_AXI_ARPROT  => s00_axi_arprot,

end arch_imp;\n"""

  c_h_header_start = """/*-------------------------------------------------------------------------------------
 *  Paul Scherrer Institut
 *-------------------------------------------------------------------------------------
 *
 *  Project :  MEGII - DCB
 *
 *  Author  :  schmid_e (Author of generation script)\n"""
  c_h_header_date = " *  Created :  " + datetime.now().strftime('%d.%m.%Y %H:%M:%S')
  c_h_header_end = """\n *
 *  Description :  Register map definitions.
 *
 *-------------------------------------------------------------------------------------
 *-------------------------------------------------------------------------------------
 */\n\n"""

  h_header_def_incl = """#ifndef __REGISTER_MAP_DCB_H__
#define __REGISTER_MAP_DCB_H__

#ifndef DCB_DONT_INCLUDE_REG_ACCESS_VARS
#include "drv_axi_dcb_reg_bank.h"
#endif

/******************************************************************************/
/* definitions                                                                */
/******************************************************************************/\n\n"""

  c_header_incl = """#include "register_map_dcb.h"\n\n"""

  h_header = c_h_header_start + c_h_header_date + c_h_header_end + h_header_def_incl
  c_header = c_h_header_start + c_h_header_date + c_h_header_end + c_header_incl

  c_restore = """\n/******************************************************************************/
/* Register Restore                                                          */
/******************************************************************************/\n\n"""

  c_default = """\n/******************************************************************************/
/* Register Defaults                                                          */
/******************************************************************************/\n\n"""

  h_end = """\n/******************************************************************************/

#endif /* __REGISTER_MAP_DCB_H__ */

/******************************************************************************/\n"""

  c_reg_mapping = """\n/******************************************************************************/
/* Register Mapping                                                           */
/******************************************************************************/\n\n"""

  c_bit_mapping = """\n/******************************************************************************/
/* Bit Mapping                                                                */
/******************************************************************************/\n\n"""

  c_end = """\n/******************************************************************************/\n"""

  py_header_start = """# #####################################################################################
#  Paul Scherrer Institut
# #####################################################################################
#
#  Project :  MEGII - DCB
#
#  Author  :  schmid_e (Author of generation script)\n"""
  py_header_date = "#  Created :  " + datetime.now().strftime('%d.%m.%Y %H:%M:%S')
  py_header_end = """\n#
#  Description :  Register map definitions.
#
# #####################################################################################
# #####################################################################################

# ###############################################################################
# # definitions                                                                ##
# ###############################################################################\n\n"""

  py_header = py_header_start + py_header_date + py_header_end

  py_end = """\n# #############################################################################
# #############################################################################\n"""

  class_header_start = """//
//  DCBReg.h
//
//  MEGII - DCB Register Access Class
//
//  This file is generated automatically, please do not edit!
//\n"""
  class_header_date = "// Created :  " + datetime.now().strftime('%d.%m.%Y %H:%M:%S')
  class_header_end = """\n//\n

#ifndef __DCBREG_H__
#define __DCBREG_H__

#include "register_map_dcb.h"

//--------------------------------------------------------------------

class DCBREG {

private:
   // virtual bit functions, must be implemented in derived class
   virtual unsigned int BitExtract(unsigned int reg, unsigned int mask, unsigned int ofs) = 0;
   virtual void SetRegMask(unsigned int reg, unsigned int mask, unsigned int ofs, unsigned int v) = 0;

public:

   // constructor
   DCBREG(){};\n\n"""

  class_header = class_header_start + class_header_date + class_header_end

  class_end = """\n};\n\n//--------------------------------------------------------------------

#endif /* defined(__DCBREG_H__) */\n"""

  html_header = """<!DOCTYPE html>
<!-- Created :   %s -->
<html>

<head>
  <link href="dcb_register_map.css" rel="stylesheet" type="text/css">
  <title>DCB Register Map</title>
</head>

<body>
  <h1>DCB Register Map</h1>
  Generated : %s<br><br>
"""%(datetime.now().strftime('%d.%m.%Y %H:%M:%S'), datetime.now().strftime('%d.%m.%Y %H:%M:%S'))

  html_end = """
</body>

</html>
"""

  css_content = """/* Created :   %s */
table {
  border-collapse: collapse;
  border: none;
}

th, td {
  padding-top: 3px;
  padding-bottom: 3px;
  padding-left: 10px;
  padding-right: 10px;
  border: 1px solid black;
}

tr.reg_header {
  font-weight: bold;
  background: rgb(150,180,215);
  border: 3px solid black;
}

tr.reg_name {
  font-weight: bold;
  background: rgb(150,180,215);
  border: 3px solid black;
}

td.reg_table_title, tr.reg_table_title {
  background: rgb(255,255,255);
  padding: 0px;
  border: none;
}

td.back_to_overview, tr.back_to_overview {
  background: rgb(255,255,255);
  padding: 0px;
  border: none;
  border-top: 3px solid black;
}

tbody tr.reg_list:nth-child(even) {
  background: rgb(185,205,230);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

tbody tr.reg_list:nth-child(odd) {
  background: rgb(150,180,215);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

tbody tr.reg_content:nth-child(even) {
  background: rgb(170,255,255);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

tbody tr.reg_content:nth-child(odd) {
  background: rgb(220,255,255);
  border-left:  3px solid black;
  border-right: 3px solid black;
}

a:link {
  color: rgb(0,0,0);
  text-decoration: none;
}

a:visited {
  color: rgb(20,20,20);
}

a:hover {
  color: rgb(235,235,235);
}

a:active {
  color: rgb(255,255,0);
}
"""%datetime.now().strftime('%d.%m.%Y %H:%M:%S')

  ip_name = "axi_dcb_register_bank_v1_0"

  #print vhd_pkg_header + vhd_entity_start + "\n\n" + vhd_entity_end + "\n\n" + vhd_architecture_end

  # Indices of Register List
  i_reg_num        = 0
  i_reg_offset     = 1
  i_reg_hw_sw      = 2
  i_reg_spi_wr     = 3
  i_reg_restore    = 4
  i_reg_default    = 5
  i_reg_name       = 6
  i_reg_comment    = 7
  i_reg_func       = 8
  i_reg_param      = 9
  i_reg_wr_mask    = 10
  i_reg_pulse_mask = 11
  reg_item_size    = 12

  # Indices of Port List
  i_port_reg_nr        =  0
  i_port_name          =  1
  i_port_name_pfx      =  2
  i_port_dir           =  3
  i_port_vec           =  4
  i_port_range_hi      =  5
  i_port_range_lo      =  6
  i_port_rw            =  7
  i_port_pulse         =  8
  i_port_default       =  9
  i_port_default_valid = 10
  i_port_comment       = 11
  port_item_size       = 12

  ### Reading Register Information
  print "Reading " + indir + os.sep + infile

  portlist   = []
  reglist    = []

  reg_names  = []
  port_names = []

  #in_file = file(indir+os.sep+infile,'r')
  excel_wb = openpyxl.load_workbook(indir+os.sep+infile, data_only=True)
  # data_only means reading the results of formulas rather than the formula itself
  register_sheet = excel_wb.get_sheet_by_name('DCB Registers')

  print "Generating register list\n"

  header_found = 0

  for row in register_sheet['A2':'N2']:
    for current_cell in row:
      if current_cell.value == '#':
        header_found = 1
        i_col_reg_num = current_cell.column-1
      elif current_cell.value == 'Offset_dec':
        i_col_offset  = current_cell.column-1
      elif current_cell.value == 'HW/SW':
        i_col_hw_sw   = current_cell.column-1
      elif current_cell.value == 'R/W':
        i_col_rw      = current_cell.column-1
      elif current_cell.value == 'Pulse':
        i_col_pulse   = current_cell.column-1
      elif current_cell.value == 'SPI WR':
        i_col_spi_wr  = current_cell.column-1
      elif current_cell.value == 'Restore':
        i_col_restore = current_cell.column-1
      elif current_cell.value == 'Bit':
        i_col_range   = current_cell.column-1
      elif current_cell.value == 'Default':
        i_col_default = current_cell.column-1
      elif current_cell.value == 'Name':
        i_col_name    = current_cell.column-1
      elif current_cell.value == 'Comment':
        i_col_comment = current_cell.column-1
      elif current_cell.value == 'SW Function':
        i_col_func    = current_cell.column-1
      elif current_cell.value == 'SW Parameter':
        i_col_param   = current_cell.column-1
    if header_found == 1:
      break;

  # Collect register and port information
  prev_reg_num = -1;
  for row in register_sheet['A3':'N2000']:
    if row[i_col_reg_num].value != None:
      # Collect Register Information and add Register in Reglist
      if row[i_col_reg_num].value != prev_reg_num+1:
        sys.exit("Error: register numbering incorrect")
      else:
        prev_reg_num = row[i_col_reg_num].value
      reg_item = [None] * reg_item_size
      reg_item[i_reg_num]        = row[i_col_reg_num].value
      reg_item[i_reg_offset]     = "0x%04X" % row[i_col_offset].value
      reg_item[i_reg_hw_sw]      = row[i_col_hw_sw].value
      reg_item[i_reg_default]    = row[i_col_default].value
      reg_item[i_reg_name]       = row[i_col_name].value
      reg_item[i_reg_comment]    = str(row[i_col_comment].value)
      reg_item[i_reg_func]       = str(row[i_col_func].value)
      reg_item[i_reg_param]      = str(row[i_col_param].value)
      reg_item[i_reg_wr_mask]    = 0x00000000
      reg_item[i_reg_pulse_mask] = 0x00000000
      if row[i_col_spi_wr].value != None:
        reg_item[i_reg_spi_wr]   = row[i_col_spi_wr].value
      else:
        reg_item[i_reg_spi_wr]   = ''
      if row[i_col_restore].value != None:
        reg_item[i_reg_restore]  = row[i_col_restore].value
      else:
        reg_item[i_reg_restore]  = ''
      reglist.append(reg_item)
      reg_names.append(row[i_col_name].value)
      if max_func_name_len < (len(str(row[i_col_func].value))):
        max_func_name_len = len(row[i_col_func].value)
      if max_reg_name_len < (len(row[i_col_name].value)):
        max_reg_name_len = len(row[i_col_name].value)
    elif row[i_col_range].value != None:
      reg_nr = len(reglist)-1
      # Collect Port Information and add Port in Portlist
      port_item = [None] * port_item_size
      port_item[i_port_reg_nr]          = reg_nr
      port_item[i_port_name]            = row[i_col_name].value.upper()
      port_item[i_port_rw]              = row[i_col_rw].value.upper()
      port_item[i_port_comment]         = str(row[i_col_comment].value)
      bit_range = str(row[i_col_range].value).split(':',1)
      range_hi = int(bit_range[0])
      if len(bit_range)>1:
        range_lo = int(bit_range[1])
      else:
        range_lo = range_hi
      port_item[i_port_range_hi]        = range_hi
      port_item[i_port_range_lo]        = range_lo
      if "W" in row[i_col_rw].value.upper():
        port_item[i_port_name_pfx]      = "_O"
        port_item[i_port_dir]           = "out"
        writable_mask = (2**(range_hi-range_lo+1)-1) << range_lo
        reglist[reg_nr][i_reg_wr_mask] = reglist[reg_nr][i_reg_wr_mask] | writable_mask
      else:
        port_item[i_port_name_pfx]      = "_I"
        port_item[i_port_dir]           = "in "
      if max_port_name_len < (len(row[i_col_name].value)):
        max_port_name_len = len(row[i_col_name].value)
      if len(bit_range)>1:
        port_item[i_port_vec]           = "std_logic_vector"
      else:
        port_item[i_port_vec]           = "std_logic"
      if row[i_col_pulse].value != None:
        port_item[i_port_pulse]         = row[i_col_pulse].value.upper()
        pulse_mask = (2**(range_hi-range_lo+1)-1) << range_lo
        reglist[reg_nr][i_reg_pulse_mask] = reglist[reg_nr][i_reg_pulse_mask] | pulse_mask
      else:
        port_item[i_port_pulse]         = ''
      if row[i_col_default].value != None:
        port_item[i_port_default]       = int(row[i_col_default].value,0)
        port_item[i_port_default_valid] = True
      else:
        port_item[i_port_default]       = 0
        port_item[i_port_default_valid] = False
      portlist.append(port_item)
      if (row[i_col_name].value).lower() != "reserved":
        port_names.append(row[i_col_name].value)

  # check for duplicate register names
  dupl_set = set()
  duplicate_names = set(x for x in reg_names if x in dupl_set or dupl_set.add(x))
  if duplicate_names:
    print "\n!!! ERROR !!!   Duplicate register names found:\n"
    print "\n".join(str(i) for i in duplicate_names)
    print "\n"
    print "\nQUITTING SCRIPT\n\n"
    quit()
  # check for duplicate bit/port names
  dupl_set = set()
  duplicate_names = set(x for x in port_names if x in dupl_set or dupl_set.add(x))
  if duplicate_names:
    print "\n!!! ERROR !!!   Duplicate bit/port names found:\n"
    print "\n".join(str(i) for i in duplicate_names)
    print "\n"
    print "\nQUITTING SCRIPT\n\n"
    quit()

  reg_count = len(reglist)

  # calculate maximum lengths
  reg_des_max_len       = calc_reg_des_max_len()
  bit_reg_des_max_len   = calc_bit_reg_des_max_len()
  bit_msk_des_max_len   = calc_bit_msk_des_max_len()
  bit_ofs_des_max_len   = calc_bit_ofs_des_max_len()
  bit_const_des_max_len = calc_bit_const_des_max_len()

#  ### create ip directory structure
#  def gen_ip_dirs():
#    print "Creating ip_dir structure..."
#    if not os.path.exists(ip_dir):
#      os.makedirs(ip_dir)
#    if not os.path.exists(ip_vhdl_dir):
#      os.makedirs(ip_vhdl_dir)
#    if not os.path.exists(ip_data_dir):
#      os.makedirs(ip_data_dir)


  # ###### vhd pkg file functions ###########################################################################################

  def write_vhd_pkg_reg_numbers(out_file):
    outline = "\n  -- register numbers\n"
    out_file.write(outline)
    for reg in reglist:
      reg_name = '{0:<{max_reg_name_len}}'.format(reg[i_reg_name], max_reg_name_len=max_reg_name_len)
      outline = "  constant C_REG_%s : integer := %3d;\n"%(reg_name, reg[i_reg_num])
      out_file.write(outline)

    const_name = '{0:<{max_reg_name_len}}'.format("NUM_REG", max_reg_name_len=max_reg_name_len+4)
    outline = "\n  constant C_%s : integer := %3d;\n"%(const_name, reg_count)
    out_file.write(outline)

    #-- not yet used
    #  constant  C_REG_CTRL_PULSE_MASK   : std_logic_vector(C_REG_WIDTH-1 downto 0) := x"000000FF";

  def write_vhd_pkg_reg_config(out_file):
    outline = "\n  -- register description\n"
    out_file.write(outline)
    outline = "  constant  C_REG_DESCR     : reg_descr_type(0 to C_NUM_REG-1) :=\n"
    out_file.write(outline)
    outline = "  (                         --  type          "
    outline += '{0:<{max_reg_name_len}}'.format("set/clr num", max_reg_name_len=max_reg_name_len+7)
    outline += "default           writable mask    write pulse only   external write\n"
    out_file.write(outline)
    for reg in reglist:
      reg_name = '{0:<{max_reg_name_len}}'.format(reg[i_reg_name], max_reg_name_len=max_reg_name_len)
      outline = "    C_REG_%s => (" % reg_name

      if reg[i_reg_wr_mask] and not("SW" in reg[i_reg_hw_sw]):
        if reg_name.startswith( 'SET_' ):
          outline += "C_BIT_SET,    "
        elif reg_name.startswith( 'CLR_' ):
          outline += "C_BIT_CLR,    "
        else:
          outline += "C_REG_RW,     "
      else:
        outline += "C_REG_RO,     "

      if reg_name.startswith( 'SET_' ):
        ref_reg = reg[i_reg_name][4:]
        ref_reg = '{0:<{max_reg_name_len}}'.format(ref_reg + ',', max_reg_name_len=max_reg_name_len+1)
      elif reg_name.startswith( 'CLR_' ):
        ref_reg = reg[i_reg_name][4:]
        ref_reg = '{0:<{max_reg_name_len}}'.format(ref_reg + ',', max_reg_name_len=max_reg_name_len+1)
      else:
        ref_reg = '{0:<{max_reg_name_len}}'.format("SELF,", max_reg_name_len=max_reg_name_len+1)
      outline += "C_REG_%s" % ref_reg
      if "SW" in reg[i_reg_hw_sw]:
        outline += "X\"DEADBEEF\",      "
      else:
        outline += "X\"%s\",      " % reg[i_reg_default][2:]
      if "SW" in reg[i_reg_hw_sw]:
        outline += "X\"00000000\",     "
      else:
        outline += "X\"%08X\",     " % reg[i_reg_wr_mask]
      outline += "X\"%08X\",       " % reg[i_reg_pulse_mask]
      if reg[i_reg_spi_wr] == '':
        outline += "C_WR_EXT_NO "
      else:
        outline += "C_WR_EXT_YES"
      outline += "),\n"
      out_file.write(outline)

    # "others" line
    outline  = "    "
    outline += '{0:<{max_reg_name_len}}'.format("others", max_reg_name_len=max_reg_name_len+6)
    outline += " => ("
    outline += "C_REG_RW,     "
    outline += '{0:<{max_reg_name_len}}'.format("C_REG_SELF,", max_reg_name_len=max_reg_name_len+7)
    outline += "X\"00000000\",      "
    outline += "X\"FFFFFFFF\",     "
    outline += "X\"00000000\",       "
    outline += "C_WR_EXT_NO )\n"
    out_file.write(outline)
    # terminate configuration table
    outline = "  );\n"
    out_file.write(outline)

  # #########################################################################################################################

  # ###### vhd file functions ###############################################################################################

  # vhd entity
  def write_vhd_entity(out_file):
    # write all ports with comments per register
    out_file.write(vhd_entity_start)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_reg_nr]:
        current_reg = port[i_port_reg_nr]
        reg_index  += 1
        outline = "\n    -- Register %s [%s]: %s\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name])
        out_file.write(outline)
        if "HW" in reglist[reg_index][i_reg_hw_sw]:
          # Generate one read/write strobe port pair for each register
          reg_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name] + "_REG_RD_STROBE_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : out std_logic;\n"%(reg_name)
          out_file.write(outline)
          reg_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name] + "_REG_WR_STROBE_O", max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : out std_logic;\n"%(reg_name)
          out_file.write(outline)
        else:
          outline = "    -- '--> Pure software register => no ports available\n"
          out_file.write(outline)
      if "RESERVED" not in port[i_port_name]:
        # Omit registers for reserved bits
        if ("HW" in reglist[reg_index][i_reg_hw_sw]) and not (port[i_port_default_valid] and port[i_port_rw] == "R"):
          # Only generate IOs for hardware registers
          # Omit inputs if there is a "Default" value for read-only registers (constant register value defined in register table)
          port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+port[i_port_name_pfx], max_port_name_len=max_port_name_len+extra_name_len)
          outline = "    %s : %s "%(port_name, port[i_port_dir])
          port_vec = "%s"%(port[i_port_vec])
          if port[i_port_range_hi] != port[i_port_range_lo]:
            port_vec = port_vec + "(%s downto 0)"%(port[i_port_range_hi]-port[i_port_range_lo])
          port_vec = '{0:<34}'.format(port_vec + ";")
          outline += port_vec + "\n"
          out_file.write(outline)
    out_file.write(vhd_entity_end)

  # vhd signal mappings
  def write_vhd_signal_mappings(out_file):
    extra_assign_target_name_len = extra_name_len + 27
    out_file.write(vhd_architecture_start)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      # Register comment and strobe signals
      if current_reg != port[i_port_reg_nr]:
        current_reg = port[i_port_reg_nr]
        reg_index += 1
        outline = "\n  -- Register %s [%s]: %s\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name])
        out_file.write(outline)
        if "HW" in reglist[reg_index][i_reg_hw_sw]:
          port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_REG_RD_STROBE_O", max_port_name_len=max_port_name_len+extra_assign_target_name_len)
          outline = "  %s <= ipif_to_user.reg.rd_strobe(C_REG_%s);\n" % (port_name, reglist[reg_index][i_reg_name])
          out_file.write(outline)
          port_name = '{0:<{max_port_name_len}}'.format(reglist[reg_index][i_reg_name]+"_REG_WR_STROBE_O", max_port_name_len=max_port_name_len+extra_assign_target_name_len)
          outline = "  %s <= ipif_to_user.reg.wr_strobe(C_REG_%s);\n" % (port_name, reglist[reg_index][i_reg_name])
          out_file.write(outline)
        else:
          outline = "  -- '--> Pure software register => no ports assigned\n"
          out_file.write(outline)
      # Port assignment
      if ("RESERVED" not in port[i_port_name]) and ("HW" in reglist[reg_index][i_reg_hw_sw]):
        if port[i_port_range_hi] != port[i_port_range_lo]:
          if (port[i_port_range_hi] == bits_per_reg-1) and (port[i_port_range_lo] == 0):
            port_range = ""
          else:
            port_range = "(%s downto %s)" % (port[i_port_range_hi], port[i_port_range_lo])
        else:
          port_range = "(%s)" % (port[i_port_range_hi])
        if "W" in port[i_port_rw]:
          # Case control (RW) port: assign register content to output
          port_name = '{0:<{max_port_name_len}}'.format(port[i_port_name]+port[i_port_name_pfx], max_port_name_len=max_port_name_len+extra_assign_target_name_len)
          outline = "  %s <= ipif_to_user.reg.data(C_REG_%s)%s;\n" % (port_name, reglist[reg_index][i_reg_name], port_range)
          out_file.write(outline)
        else:
          # Case status (R) port: assign input or constant to register content
          # generate constant
          port_initial = port[i_port_default]
          nr_of_bits = port[i_port_range_hi]-port[i_port_range_lo]+1
          if port[i_port_range_hi] == port[i_port_range_lo]:
            port_initial = "\'%d\'" % port_initial
          else:
            if nr_of_bits % 4 != 0:
              port_initial = '\"{0:0{bitlen}b}\"'.format(port_initial, bitlen = nr_of_bits)
            else:
              port_initial = 'x\"{0:0{hexlen}X}\"'.format(port_initial, hexlen = nr_of_bits/4)
          if "in" in port[i_port_dir]:
            # Actual assignment (R)
            signal_name = "user_to_ipif.reg.data(C_REG_%s)%s" % (reglist[reg_index][i_reg_name], port_range)
            signal_name = '{0:<{max_port_name_len}}'.format(signal_name, max_port_name_len=max_port_name_len+extra_assign_target_name_len)
            outline = "  %s" % (signal_name)
            if port[i_port_default_valid]:
              # Assign constant
              outline += " <= %s;\n"%port_initial
            else:
              # Assign port
              outline += " <= %s;\n"%(port[i_port_name]+port[i_port_name_pfx])
            out_file.write(outline)
    out_file.write(vhd_architecture_end)

  # #########################################################################################################################

  # ###### h file functions #################################################################################################

  def bit_mask(port):
    bit_mask_pattern = ""
    for i in range(31,-1,-1):
      if i in range(port[i_port_range_lo],port[i_port_range_hi]+1):
        bit_mask_pattern = bit_mask_pattern + '1'
      else:
        bit_mask_pattern = bit_mask_pattern + '0'
    bit_mask_pattern = "0x" + "{0:0>8X}".format(int(bit_mask_pattern, 2))
    return bit_mask_pattern

  def write_h_reg_offsets(out_file):
    outline = "/*\n * Register Offsets\n */\n\n"
    out_file.write(outline)
    last_reg = 0
    for register in reglist:
      last_reg = int(register[i_reg_num])
      reg_name = '{0:<{max_reg_name_len}}'.format(reg_designator(register), max_reg_name_len=reg_des_max_len+extra_name_len)
      outline = "#define %s   %s\n"%(reg_name, register[i_reg_offset])
      out_file.write(outline)

  def write_h_bit_parameters(out_file):
    outline = "\n/*\n * Bit Positions\n */"
    out_file.write(outline)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_reg_nr]:
        current_reg = port[i_port_reg_nr]
        reg_index += 1
        outline = "\n\n/* ****** Register %s [%s]: %s - %s (Default: %s) ****** */\n\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
        out_file.write(outline)
      if ("RESERVED" not in port[i_port_name]):
        outline = "/* %s - %s */\n"%(port[i_port_name], port[i_port_comment])
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_reg_designator(port), max_port_name_len=bit_reg_des_max_len+1)
        port_reg  = '{0:>{max_port_name_len}}'.format(reg_designator(reglist[reg_index]), max_port_name_len=reg_des_max_len)
        outline = "#define %s   %s\n"%(port_name, port_reg)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_msk_designator(port), max_port_name_len=bit_msk_des_max_len)
        bit_pattern = bit_mask(port)
        bit_pattern = '{0:>{max_port_name_len}}'.format(bit_pattern, max_port_name_len=reg_des_max_len)
        outline = "#define %s   %s\n"%(port_name, bit_pattern)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_ofs_designator(port), max_port_name_len=bit_ofs_des_max_len+1)
        port_ofs  = '{0:>{max_port_name_len}}'.format(port[i_port_range_lo], max_port_name_len=reg_des_max_len)
        outline = "#define %s   %s\n"%(port_name, port_ofs)
        out_file.write(outline)
        if ( port[i_port_default_valid] and ("W" not in port[i_port_rw]) ):
          hex_len = (port[i_port_range_hi]-port[i_port_range_lo]+3)/4
          port_name = '{0:<{max_port_name_len}}'.format(bit_const_designator(port), max_port_name_len=bit_const_des_max_len-1)
          port_const = "0x{0:0{hex_len}X}".format(port[i_port_default], hex_len=hex_len)
          port_const = '{0:>{max_port_name_len}}'.format(port_const, max_port_name_len=reg_des_max_len)
          outline = "#define %s   %s\n"%(port_name, port_const)
          out_file.write(outline)
        outline = "\n"
        out_file.write(outline)

  def write_h_total_nr_of_regs(out_file):
    outline = "\n/*\n * Number of Registers\n */\n\n"
    out_file.write(outline)
    outline = "#define NR_OF_REGS          %d\n\n" % reg_count
    out_file.write(outline)

  def write_h_register_access_declarations(out_file):
    outline  = "typedef unsigned int (*dcb_reg_func)(unsigned int, unsigned int, unsigned int, unsigned int);\n\n"
    outline += "typedef struct\n"
    outline += "{\n"
    outline += "  dcb_reg_func  func;\n"
    outline += "  unsigned int par;\n"
    outline += "} dcb_reg_func_type;\n\n"
    outline += "extern const dcb_reg_func_type  dcb_reg_func_list[];\n\n"
    out_file.write(outline)

  def write_c_register_access_functions(out_file):
    outline = "\nconst dcb_reg_func_type  dcb_reg_func_list[] = {\n"
    out_file.write(outline)
    for register in reglist:
      outline = '{0:<{max_func_name_len}}'.format("  { %s"%(register[i_reg_func]), max_func_name_len=max_func_name_len+4)
      outline += " , %s }"%(register[i_reg_param])
      if register[i_reg_num] != reg_count-1:
        outline += ','
      else:
        outline += ' '
      outline += "   /* register %d [%s]: %s */\n"%(register[i_reg_num], register[i_reg_offset], register[i_reg_name])
      out_file.write(outline)
    outline = "};\n\n"
    out_file.write(outline)

  def write_h_register_mapping_declarations(out_file):
    outline = "\n"
    outline += "typedef struct {\n"
    outline += "  const char * name;\n"
    outline += "  const unsigned int reg;\n"
    outline += "  const unsigned int read_only;\n"
    outline += "} dcb_reg_entry_type;\n\n\n"
    outline += "typedef struct {\n"
    outline += "  const char * name;\n"
    outline += "  const unsigned int reg;\n"
    outline += "  const unsigned int mask;\n"
    outline += "  const unsigned int ofs;\n"
    outline += "} dcb_bit_group_entry_type;\n\n"
    out_file.write(outline)
    outline  = "extern const dcb_reg_entry_type  dcb_reg_list[];\n"
    outline += "extern const dcb_bit_group_entry_type  dcb_bit_group_list[];\n"
    outline += "extern const unsigned char reg_restore[];\n"
    outline += "extern const unsigned int reg_default[];\n\n"
    out_file.write(outline)
    outline  = "#define DCB_WRITABLE_REG     0\n"
    outline += "#define DCB_READONLY_REG     1\n\n"
    outline += "#define DCB_DONT_TOUCH_REG   0\n"
    outline += "#define DCB_RESTORE_REG      1\n\n"
    out_file.write(outline)

  def write_c_mapping_lists(out_file):
    out_file.write(c_reg_mapping)
    outline = "const dcb_reg_entry_type  dcb_reg_list[] = {\n"
    out_file.write(outline)
    for register in reglist:
      outline = '{0:<{max_reg_name_len}}'.format("  { \"%s\""%(register[i_reg_name]), max_reg_name_len=max_reg_name_len+6)
      outline += '{0:<{max_reg_name_len}}'.format(" , %s"%(reg_designator(register)), max_reg_name_len=reg_des_max_len+3)
      if register[i_reg_wr_mask]:
        outline += ", DCB_WRITABLE_REG"
      else:
        outline += ", DCB_READONLY_REG"
      outline += " },\n"
      out_file.write(outline)
    outline = '{0:<{max_reg_name_len}}'.format("  { (const char*)0", max_reg_name_len=max_reg_name_len+6)
    outline += '{0:<{max_reg_name_len}}'.format(" , 0", max_reg_name_len=reg_des_max_len+3)
    outline += ", 0               "
    outline += " }\n};\n"
    out_file.write(outline)

    # list with bit mappings
    out_file.write(c_bit_mapping)
    outline = "const dcb_bit_group_entry_type  dcb_bit_group_list[] = {\n"
    out_file.write(outline)
    for port in portlist:
      if ("RESERVED" not in port[i_port_name]):
        outline =  '{0:<{max_port_name_len}}'.format("  { \"%s\""%(port[i_port_name]), max_port_name_len=max_port_name_len+6)
        outline += '{0:<{max_port_name_len}}'.format(" , %s"%(bit_reg_designator(port)), max_port_name_len=bit_reg_des_max_len+3)
        outline += '{0:<{max_port_name_len}}'.format(" , %s"%(bit_msk_designator(port)), max_port_name_len=bit_msk_des_max_len+3)
        outline += '{0:<{max_port_name_len}}'.format(" , %s"%(bit_ofs_designator(port)), max_port_name_len=bit_ofs_des_max_len+3)
        outline += " },\n"
        out_file.write(outline)
    outline =  '{0:<{max_port_name_len}}'.format("  { (const char*)0", max_port_name_len=max_port_name_len+6)
    outline += '{0:<{max_port_name_len}}'.format(" , 0", max_port_name_len=bit_reg_des_max_len+3)
    outline += '{0:<{max_port_name_len}}'.format(" , 0", max_port_name_len=bit_msk_des_max_len+3)
    outline += '{0:<{max_port_name_len}}'.format(" , 0", max_port_name_len=bit_ofs_des_max_len+3)
    outline += " }\n};\n"
    out_file.write(outline)

  def write_c_register_restore(out_file):
    out_file.write(c_restore)
    for register in reglist:
      if reglist.index(register) == 0:
        outline = "const unsigned char reg_restore[] = {\n"
      else:
        outline = ",   /* Offset %s */\n"%reg_offset
      reg_offset = register[i_reg_offset]
      out_file.write(outline)
      if register[i_reg_restore] == "":
        outline = "  DCB_DONT_TOUCH_REG"
      else:
        outline = "  DCB_RESTORE_REG   "
      out_file.write(outline)
    outline = "    /* Offset %s */\n};\n\n"%reg_offset
    out_file.write(outline)

  def write_c_register_defaults(out_file):
    out_file.write(c_default)
    for register in reglist:
      if "CRC32" not in register[i_reg_name]:
        if reglist.index(register) == 0:
          outline = "const unsigned int reg_default[] = {\n"
        else:
          outline = ",   /* Offset %s */\n"%reg_offset
        reg_offset = register[i_reg_offset]
        out_file.write(outline)
        outline = "  %s"%(register[i_reg_default])
        out_file.write(outline)
    outline = "    /* Offset %s */\n};\n\n"%reg_offset
    out_file.write(outline)

  # #########################################################################################################################

  # ###### py file functions ################################################################################################

  def write_py_reg_offsets(out_file):
    outline = "#\n# Register Offsets\n#\n\n"
    out_file.write(outline)
    for register in reglist:
      reg_name = '{0:<{max_reg_name_len}}'.format(reg_designator(register), max_reg_name_len=reg_des_max_len+extra_name_len)
      outline = "%s   = %s\n"%(reg_name, register[i_reg_offset])
      out_file.write(outline)

  def write_py_bit_parameters(out_file):
    outline = "\n#\n# Bit Positions\n#"
    out_file.write(outline)

    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_reg_nr]:
        current_reg = port[i_port_reg_nr]
        reg_index += 1
        outline += "\n# ****** Register %s [%s]: %s - %s (Default: %s) ******\n\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
        out_file.write(outline)
      if ("RESERVED" not in port[i_port_name]):
        outline = "# %s - %s\n"%(port[i_port_name], port[i_port_comment].replace("\n","\n# "))
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_reg_designator(port), max_port_name_len=bit_reg_des_max_len+1)
        port_reg  = '{0:>{max_port_name_len}}'.format(reg_designator(reglist[reg_index]), max_port_name_len=reg_des_max_len)
        outline = "%s   = %s\n"%(port_name, port_reg)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_msk_designator(port), max_port_name_len=bit_msk_des_max_len)
        bit_pattern = bit_mask(port)
        bit_pattern = '{0:>{max_port_name_len}}'.format(bit_pattern, max_port_name_len=max_port_name_len+2)
        outline = "%s   = %s\n"%(port_name, bit_pattern)
        out_file.write(outline)
        port_name = '{0:<{max_port_name_len}}'.format(bit_ofs_designator(port), max_port_name_len=bit_ofs_des_max_len+1)
        port_ofs  = '{0:>{max_port_name_len}}'.format(port[i_port_range_lo], max_port_name_len=max_port_name_len+2)
        outline = "%s   = %s\n"%(port_name, port_ofs)
        out_file.write(outline)
        if ( port[i_port_default_valid] and ("W" not in port[i_port_rw]) ):
          hex_len = (port[i_port_range_hi]-port[i_port_range_lo]+4)/4
          port_name = '{0:<{max_port_name_len}}'.format(bit_const_designator(port), max_port_name_len=bit_const_des_max_len+1)
          port_const = "0x{0:0{hex_len}X}".format(port[i_port_default], hex_len=hex_len)
          port_const = '{0:>{max_port_name_len}}'.format(port_const, max_port_name_len=reg_des_max_len)
          outline = "%s   %s\n"%(port_name, port_const)
          out_file.write(outline)
        outline = "\n"
        out_file.write(outline)

  def write_py_total_nr_of_regs(out_file):
    outline = "\n\n# Number of Registers\n\n"
    out_file.write(outline)
    outline = "REG_NR_OF_REGS          = %d\n"%reg_count
    out_file.write(outline)

  def write_py_mapping_lists(out_file):
    # write list with register name-offset mapping
    outline = "\n\n\n# List of register names, offsets and writable info\n"
    out_file.write(outline)
    outline  = "\nDCB_WRITABLE_REG = False\n"
    outline += "DCB_READONLY_REG = True\n"
    out_file.write(outline)
    outline = "\ndcb_reg_list = (\n"
    out_file.write(outline)
    for register in reglist:
      outline = '{0:<{max_reg_name_len}}'.format("  ( \"%s\""%(register[i_reg_name]), max_reg_name_len=max_reg_name_len+6)
      outline += '{0:<{max_reg_name_len}}'.format(" , %s"%(reg_designator(register)), max_reg_name_len=reg_des_max_len+3)
      if register[i_reg_wr_mask]:
        outline += ", DCB_WRITABLE_REG"
      else:
        outline += ", DCB_READONLY_REG"
      outline += " ),\n"
      out_file.write(outline)
    outline = ")\n"
    out_file.write(outline)
    # write list with bit mappings
    outline = "\n\n\n# List of bit names and parameters\n"
    out_file.write(outline)
    outline = "\ndcb_bit_group_list = (\n"
    out_file.write(outline)
    for port in portlist:
      if ("RESERVED" not in port[i_port_name]):
        outline =  '{0:<{max_port_name_len}}'.format("  ( \"%s\""%(port[i_port_name]), max_port_name_len=max_port_name_len+6)
        outline += '{0:<{max_port_name_len}}'.format(" , %s"%(bit_reg_designator(port)), max_port_name_len=bit_reg_des_max_len+3)
        outline += '{0:<{max_port_name_len}}'.format(" , %s"%(bit_msk_designator(port)), max_port_name_len=bit_msk_des_max_len+3)
        outline += '{0:<{max_port_name_len}}'.format(" , %s"%(bit_ofs_designator(port)), max_port_name_len=bit_ofs_des_max_len+3)
        outline += " ),\n"
        out_file.write(outline)
    outline = ")\n"
    out_file.write(outline)

  def write_py_register_restore(out_file):
    outline = "\n\n\n#\n# Register Restore\n#\n\n"
    out_file.write(outline)
    outline = "DCB_DONT_TOUCH_REG = False\n"
    out_file.write(outline)
    outline = "DCB_RESTORE_REG    = True\n\n"
    out_file.write(outline)
    for register in reglist:
      if reglist.index(register) == 0:
        outline = "reg_restore = ("
      else:
        outline = ",   # Offset %s \n               " % reg_offset
      reg_offset = register[i_reg_offset]
      out_file.write(outline)
      if register[i_reg_restore] == "":
        outline = "DCB_DONT_TOUCH_REG"
      else:
        outline = "DCB_RESTORE_REG   "
      out_file.write(outline)
    outline = ")   # Offset %s \n"%reg_offset
    out_file.write(outline)

  def write_py_register_defaults(out_file):
    outline = "\n\n\n#\n# Register Defaults\n#\n\n"
    out_file.write(outline)
    for register in reglist:
      if "CRC32" not in register[i_reg_name]:
        if reglist.index(register) == 0:
          outline =    "ctrl_reg_default = ("
        else:
          outline = ",   # Offset %s \n                    "%reg_offset
        reg_offset = register[i_reg_offset]
        out_file.write(outline)
        outline = "%s"%(register[i_reg_default])
        out_file.write(outline)
    outline = ")   # Offset %s \n\n"%reg_offset
    out_file.write(outline)

  # #########################################################################################################################

  # ###### class file functions #############################################################################################

  def gen_class_get_function(port):
    name = underscore_to_camelcase(port[i_port_name])
    func_str = "   unsigned int Get" + name + "() { return BitExtract("
    func_str = func_str + bit_reg_designator(port) + ", "
    func_str = func_str + bit_msk_designator(port) + ", "
    func_str = func_str + bit_ofs_designator(port) + ");"
    func_str = func_str + " };\n"
    return func_str

  def gen_class_set_function(port):
    name = underscore_to_camelcase(port[i_port_name])
    func_str = "   void         Set" + name + "(unsigned int value) { SetRegMask("
    func_str = func_str + bit_reg_designator(port) + ", "
    func_str = func_str + bit_msk_designator(port) + ", "
    func_str = func_str + bit_ofs_designator(port) + ", value);"
    func_str = func_str + " };\n"
    return func_str

  def write_class_functions(out_file):
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_reg_nr]:
        current_reg = port[i_port_reg_nr]
        reg_index += 1
        outline = "\n\n   ////// ------ Register %s [%s]: %s - %s (Default: %s) ------ //////\n\n"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_offset], reglist[reg_index][i_reg_name], reglist[reg_index][i_reg_comment], reglist[reg_index][i_reg_default])
        out_file.write(outline)
      if "RESERVED" not in port[i_port_name]:
        outline = "   // %s: %s - %s\n"%(bit_mask(port), port[i_port_name], port[i_port_comment].replace("\n","\n   // "))
        out_file.write(outline)
        outline = gen_class_get_function(port)
        out_file.write(outline)
        if "W" in port[i_port_rw]:
          outline = gen_class_set_function(port) + "\n"
          out_file.write(outline)

  # #########################################################################################################################

  # ###### html file ########################################################################################################

  def write_html_reg_list(out_file):
    outline = "  <table>\n    <tbody>\n"
    out_file.write(outline)
    outline = "      <tr class=\"reg_header\">\n        <th>#</th>\n        <th>Offset</th>\n        <th>HW/SW</th>\n        <th>SPI WR</th>\n        <th>Restore</th>\n        <th>Default</th>\n        <th>Name</th>\n        <th>Comment</th>\n      </tr>\n"
    out_file.write(outline)
    last_reg = 0
    for register in reglist:
      outline = "      <tr class=\"reg_list\">\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:right\" ><a href=\"#register_%d\" id=\"reg_list_%d\">%d</a></td>\n"%(register[i_reg_num], register[i_reg_num], register[i_reg_num])
      out_file.write(outline)
      outline = "        <td style=\"text-align:right\" ><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_offset])
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_hw_sw])
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_spi_wr])
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_restore])
      out_file.write(outline)
      outline = "        <td style=\"text-align:right\" ><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_default])
      out_file.write(outline)
      outline = "        <td style=\"text-align:left\"  ><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_name])
      out_file.write(outline)
      outline = "        <td style=\"text-align:left\"  ><a href=\"#register_%d\">%s</a></td>\n"%(register[i_reg_num], register[i_reg_comment])
      out_file.write(outline)
      outline = "      </tr>\n"
      out_file.write(outline)
    outline = "      <tr class=\"back_to_overview\">"
    out_file.write(outline)
    outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#register_overview\">Registers Top</a></a></td>"
    out_file.write(outline)
    outline = "      </tr>\n    </tbody>\n  </table>"
    out_file.write(outline)

  def write_html_reg_contents(out_file):
    outline = "  <table>\n    <tbody>\n"
    out_file.write(outline)
    current_reg = -1
    reg_index = -1
    for port in portlist:
      if current_reg != port[i_port_reg_nr]:
        if current_reg != -1:
          outline = "      <tr class=\"back_to_overview\">\n"
          out_file.write(outline)
          outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#reg_list_%d\">Register Overview</a></td>\n      </tr>\n"%(reglist[reg_index][i_reg_num])
          out_file.write(outline)
        current_reg = port[i_port_reg_nr]
        reg_index += 1
        outline = "      <tr class=\"reg_table_title\">\n"
        out_file.write(outline)
        outline = "        <td class=\"reg_table_title\" colspan=\"7\">"
        out_file.write(outline)
        outline = "<a id=\"register_%s\">"%(reglist[reg_index][i_reg_num])
        out_file.write(outline)
        outline = "<h3>Register %d: %s</h3>"%(reglist[reg_index][i_reg_num], reglist[reg_index][i_reg_name])
        out_file.write(outline)
        outline = "</a></td>\n      </tr>\n"
        out_file.write(outline)
        outline = "      <tr class=\"reg_header\">\n        <th>#</th>\n        <th>Offset</th>\n        <th>HW/SW</th>\n        <th>SPI WR</th>\n        <th>Restore</th>\n        <th>R/W</th>\n        <th>Pulse</th>\n        <th>Bit</th>\n        <th>Initial</th>\n        <th>Name</th>\n        <th>Comment</th>\n      </tr>\n"
        out_file.write(outline)
        outline = "      <tr class=\"reg_name\">\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\">%s</td>\n"%(reglist[reg_index][i_reg_num])
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" >%s</td>\n"%(reglist[reg_index][i_reg_offset])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\">%s</td>\n"%(reglist[reg_index][i_reg_hw_sw])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\">%s</td>\n"%(reglist[reg_index][i_reg_spi_wr])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\">%s</td>\n"%(reglist[reg_index][i_reg_restore])
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"></td>\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"></td>\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:center\"></td>\n"
        out_file.write(outline)
        outline = "        <td style=\"text-align:right\" >%s</td>\n"%(reglist[reg_index][i_reg_default])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  >%s</td>\n"%(reglist[reg_index][i_reg_name])
        out_file.write(outline)
        outline = "        <td style=\"text-align:left\"  >%s</td>\n"%(reglist[reg_index][i_reg_comment])
        out_file.write(outline)
        outline = "      </tr>\n"
        out_file.write(outline)
#      if "out" in port[i_port_dir]:
      outline = "      <tr class=\"reg_content\">\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"></td>\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:right\" ></td>\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:right\" ></td>\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:right\" ></td>\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"></td>\n"
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"  >%s</td>\n"%(port[i_port_rw])
      out_file.write(outline)
      outline = "        <td style=\"text-align:center\"  >%s</td>\n"%(port[i_port_pulse])
      out_file.write(outline)
      if port[i_port_range_hi] == port[i_port_range_lo]:
        outline = "        <td style=\"text-align:center\">%d</td>\n"%(port[i_port_range_hi])
      else:
        outline = "        <td style=\"text-align:center\">%d:%d</td>\n"%(port[i_port_range_hi], port[i_port_range_lo])
      out_file.write(outline)
      port_initial = port[i_port_default]
      nr_of_bits = port[i_port_range_hi]-port[i_port_range_lo]+1
      if "RESERVED" in port[i_port_name] and port[i_port_default] == 0:
        port_initial = "0"
      elif nr_of_bits % 4 != 0:
        port_initial = '0b{0:0{bitlen}b}'.format(port_initial, bitlen = nr_of_bits)
      else:
        port_initial = '0x{0:0{hexlen}X}'.format(port_initial, hexlen = nr_of_bits/4)
      outline = "        <td style=\"text-align:right\" >%s</td>\n"%port_initial
      out_file.write(outline)
      outline = "        <td style=\"text-align:left\"  >%s</td>\n"%(port[i_port_name])
      out_file.write(outline)
      if "RESERVED" in port[i_port_name]:
        outline = "        <td style=\"text-align:left\"  ></td>\n"
      else:
        outline = "        <td style=\"text-align:left\"  >%s</td>\n"%("<br>".join(port[i_port_comment].split("\n")))
      out_file.write(outline)
      outline = "      </tr>\n"
      out_file.write(outline)
    outline = "      <tr class=\"back_to_overview\">\n"
    out_file.write(outline)
    outline = "        <td class=\"back_to_overview\" colspan=\"7\"><a href=\"#reg_list_%d\">Register Overview</a></td>\n      </tr>\n"%(reglist[reg_index][i_reg_num])
    out_file.write(outline)
    outline = "\n    </tbody>\n"
    out_file.write(outline)
    outline = "\n  </table>\n"
    out_file.write(outline)

  # #########################################################################################################################

  def write_vhd_pkg():
    outfile = vhd_pkg_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(vhd_pkg_header)
    out_file.write(vhd_pkg_content_start)

    write_vhd_pkg_reg_numbers(out_file)
    write_vhd_pkg_reg_config(out_file)

    out_file.write(vhd_pkg_content_end)
    out_file.close()

  def write_vhd():
    outfile = vhd_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(vhd_header)

    write_vhd_entity(out_file)

    write_vhd_signal_mappings(out_file)

    out_file.close()

  def write_c():
    outfile = c_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(c_header)

    outline = "#ifndef DCB_DONT_INCLUDE_REG_ACCESS_VARS\n\n"
    out_file.write(outline)

    write_c_register_access_functions(out_file)

    outline = "#endif /* DCB_DONT_INCLUDE_REG_ACCESS_VARS */\n\n"
    out_file.write(outline)

    outline = "#ifndef DCB_DONT_INCLUDE_VARS\n"
    out_file.write(outline)

    write_c_mapping_lists(out_file)
    write_c_register_restore(out_file)
    write_c_register_defaults(out_file)

    outline = "#endif /* DCB_DONT_INCLUDE_VARS */\n\n"
    out_file.write(outline)

    out_file.write(c_end)
    out_file.close()

  def write_h():
    outfile = h_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(h_header)

    write_h_reg_offsets(out_file)
    write_h_bit_parameters(out_file)
    write_h_total_nr_of_regs(out_file)

    outline = "#ifndef DCB_DONT_INCLUDE_REG_ACCESS_VARS\n\n"
    out_file.write(outline)

    write_h_register_access_declarations(out_file)

    outline = "#endif /* DCB_DONT_INCLUDE_REG_ACCESS_VARS */\n\n"
    out_file.write(outline)

    outline = "#ifndef DCB_DONT_INCLUDE_VARS\n"
    out_file.write(outline)

    write_h_register_mapping_declarations(out_file)

    outline = "#endif /* DCB_DONT_INCLUDE_VARS */\n\n"
    out_file.write(outline)

    out_file.write(h_end)
    out_file.close()

  def write_py():
    outfile = py_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(py_header)

    write_py_reg_offsets(out_file)
    write_py_bit_parameters(out_file)
    write_py_total_nr_of_regs(out_file)
    write_py_mapping_lists(out_file)
    write_py_register_restore(out_file)
    write_py_register_defaults(out_file)

    out_file.write(py_end)
    out_file.close()

  def write_class():
    outfile = class_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(class_header)

    write_class_functions(out_file)

    out_file.write(class_end)
    out_file.close()

  def write_html():
    outfile = html_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(html_header)

    write_html_reg_list(out_file)

    outline = "  <br>\n  <br>\n  <hr>\n  <hr>\n  <h2>Register Contents</h2>\n"
    out_file.write(outline)

    write_html_reg_contents(out_file)

    outline = "\n  <br>\n  Generated :   %s\n\n</body>\n\n</html>\n"%datetime.now().strftime('%d.%m.%Y %H:%M:%S')
    out_file.write(outline)

    out_file.close()

  def write_css():
    outfile = css_outfile
    out_file = file(outdir+os.sep+outfile,'w')
    out_file.write(css_content)
    out_file.close()

  # #########################################################################################################################

  print "Writing temporary output files:"
  print "Writing " + outdir + os.sep + vhd_pkg_outfile +"..."
  write_vhd_pkg()
  print "Writing " + outdir + os.sep + vhd_outfile +"..."
  write_vhd()
  print "Writing " + outdir + os.sep + css_outfile + "..."
  write_css()
  print "Writing " + outdir + os.sep + html_outfile + "..."
  write_html()
  print "Writing " + outdir + os.sep + c_outfile + "..."
  write_c()
  print "Writing " + outdir + os.sep + py_outfile + "..."
  write_h()
  print "Writing " + outdir + os.sep + py_outfile + "..."
  write_py()
  print "Writing " + outdir + os.sep + class_outfile + "..."
  write_class()

  # Copy files to target directories?
  do_file_copy = raw_input("\nCopy files to target directories? ([y]/n): ")
  if (not do_file_copy) or (do_file_copy == 'y') or (do_file_copy == "yes"):
    do_file_copy = 'y'
  else:
    do_file_copy = 'n'

  # Copy files to target directories!
  if do_file_copy == 'y':
    print "\n"
    #git_root = find_vcs_root(os.getcwd()) # to find git root with current working directory
    git_root = find_vcs_root(indir)
    if git_root == None:
      sys.exit("Error: GIT root not found.\n       Make sure .xlsx source is in a WD GIT repository directory")
    else:
      print "Performing filecopy to target directories (GIT root = %s):"%(git_root)
      print "Copy IP files ..."
      print "---\n%s :"%(vhd_pkg_outfile)
      copy_file_to_target(outdir + os.sep + vhd_pkg_outfile,  git_root + os.sep + ip_vhdl_dir + os.sep + vhd_pkg_outfile)
      print "---\n%s :"%(vhd_outfile)
      copy_file_to_target(outdir + os.sep + vhd_outfile,  git_root + os.sep + ip_vhdl_dir + os.sep + vhd_outfile)
      print "---\n%s :"%(css_outfile)
      copy_file_to_target(outdir + os.sep + css_outfile,  git_root + os.sep + ip_doc_dir + os.sep + css_outfile)
      print "---\n%s :"%(html_outfile)
      copy_file_to_target(outdir + os.sep + html_outfile, git_root + os.sep + ip_doc_dir + os.sep + html_outfile)
      print "---\n%s :"%(c_outfile)
      copy_file_to_target(outdir + os.sep + c_outfile, git_root + os.sep + c_h_dir_arm + os.sep + c_outfile)
      print "---\n%s :"%(h_outfile)
      copy_file_to_target(outdir + os.sep + h_outfile, git_root + os.sep + c_h_dir_arm + os.sep + h_outfile)
      #print "\nCopy baremetal ARM softwre files ..."
      print "\nCopy Linux softwre files ..."
      print "---\n%s :"%(c_outfile)
      copy_file_to_target(outdir + os.sep + c_outfile, git_root + os.sep + c_h_dir_lin + os.sep + c_outfile)
      print "---\n%s :"%(h_outfile)
      copy_file_to_target(outdir + os.sep + h_outfile, git_root + os.sep + c_h_dir_lin + os.sep + h_outfile)
      print "\nCopy external host softwre files ..."
      print "---\n%s :"%(c_outfile)
      copy_file_to_target(outdir + os.sep + c_outfile, git_root + os.sep + c_h_dir_wds + os.sep + c_outfile)
      print "---\n%s :"%(h_outfile)
      copy_file_to_target(outdir + os.sep + h_outfile, git_root + os.sep + c_h_dir_wds + os.sep + h_outfile)
      print "---\n%s :"%(class_outfile)
      copy_file_to_target(outdir + os.sep + class_outfile, git_root + os.sep + class_dir + os.sep + class_outfile)
      print "\nCopy python script files ..."
      print "---\n%s :"%(py_outfile)
      copy_file_to_target(outdir + os.sep + py_outfile, git_root + os.sep + py_dir + os.sep + py_outfile)
      print "\nFilecopy completed !!!\n"
      print "\n--> Remember to update the IP in the block design <--\n"
  else:
    print "Target files not updated"
